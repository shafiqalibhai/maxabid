<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction AJAX Callback Main
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
//require('includes/application_top.php');
  require('includes/application_mini_top.php');

  $output_result_array = array();
  tep_auction_expire();

  switch($action) {
    case 'initialize':
      $output_result_array = array(
        'period' => 1000,
      );
      tep_auction_format_callback($output_result_array, true, true);
      break;

    case 'set_auction':
      $auctions_id = isset($_POST['auction_id'])?(int)$_POST['auction_id']:0;
      if( !$auctions_id || !isset($_POST['bid_value']) ) {
        exit();
      }
      break;

    case 'browse_auctions':
      $check_auctions_array = ( isset($_POST['auction_bid']) && is_array($_POST['auction_bid']))?$_POST['auction_bid']:array();

      $request_array = array();
      foreach($check_auctions_array as $key => $value) {
        $request_array[(int)$key] = tep_db_prepare_input($value);
      }

      if( isset($_GET['auctions_group_id']) ) {

        $new_array = tep_auction_get_group_differences( (int)$_GET['auctions_group_id'], $request_array);
        foreach($new_array as $key => $last_modified ) {
          $output_result_array[$key]['new'] = $last_modified;
        }
      }

      if( empty($request_array) ) {
        tep_auction_format_callback($output_result_array, true, true);
      }
      break;

    default:
      exit();
      break;
  }

  if( $action == 'browse_auctions' ) {

    if( isset($_GET['auctions_group_id']) ) {
      $history_array = array();
      $history_query_raw = "select auctions_id, customers_nickname from " . TABLE_AUCTIONS_HISTORY . " where auctions_id in (" . implode(',', array_keys($request_array)) . ") group by auctions_id";
      tep_query_to_array($history_query_raw, $history_array, 'auctions_id');
      foreach($history_array as $key => $auction) {
        if( empty($auction['customers_nickname']) ) {
          $output_result_array[$key]['note'] = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_END_NULL_LABEL . '</span>';
        } else {
          $output_result_array[$key]['note'] = '<span class="heavy mark_blue">' . sprintf(TEXT_INFO_AUCTION_END_WINNER_LABEL, $auction['customers_nickname']) . '</span>';
        }
        $output_result_array[$key]['note2'] = '<span class="font_large" style="color: #000;">' . TEXT_INFO_AUCTION_ENDED . '</span>';
        $output_result_array[$key]['background'] = '#CCCCCC';
        $output_result_array[$key]['ended'] = $key;

        if( !(time()%4) ) {
          tep_auction_display_growl_end($key, false, false);
        }
        unset($request_array[$key]);
      }

      if( empty($request_array) ) {
        tep_auction_format_callback($output_result_array, true, true);
      }
    }

    $bids_array = array();
    $bids_query_raw = "select auctions_id, bid_price, last_modified, customers_id, customers_nickname, signature from " . TABLE_AUCTIONS_BID . " where auctions_id in (" . implode(',', array_keys($request_array)) . ")";

    tep_query_to_array($bids_query_raw, $bids_array, 'auctions_id');

    if( empty($bids_array) ) {
      tep_auction_format_callback($output_result_array, true, true);
    }

    $result_array = array();
    $auctions_array = array();
    $auctions_query_raw = "select auctions_id, auctions_name, auctions_type, auctions_overbid, bid_step, max_bids, bids_left, countdown_bids, countdown_timer, auto_timer, date_start, date_expire, start_pause, end_pause, status_id, live_off from " . TABLE_AUCTIONS . " where status_id = '1' and auctions_id in (" . implode(',', array_keys($request_array)) . ")";
    tep_query_to_array($auctions_query_raw, $auctions_array, 'auctions_id');

    foreach($auctions_array as $key => $auction) {
      $action = 'browse_auctions';

      $output_result_array[$key]['note2'] = '';

      if( isset($bids_array[$key]) && $bids_array[$key]['signature'] != $request_array[$key] ) {
        $result_array[$key] = $auction;
      }

      $remaining = -1;
      $restart = false;
      if( isset($bids_array[$key]) ) {
        $pause = tep_auction_check_pause($auction, $restart, $remaining, $bids_array[$key]['last_modified']);
      } else {
        $pause = tep_auction_check_pause($auction, $restart, $remaining);
      }

      if( $pause ) {
        if($remaining > 0 && $remaining <= 10 ) {
          $output_result_array[$key]['note2'] = sprintf(ERROR_AUCTION_SECS_LABEL, '<br /><span class="font_large" style="color: #000;">' . $remaining . ' Secs</span>');
          //$output_result_array[$key]['background'] = '#FF9999';
        } else {
          $output_result_array[$key]['note2'] = sprintf(ERROR_AUCTION_PAUSED_LABEL, '<br /><span class="font_large" style="color: #000;">' . $auction['end_pause'] . '</span>');
        }
        continue;
      }

      $pause = tep_auction_check_start($auction);
      if( strlen($pause) ) {
        $tmp_array = explode(' ', tep_date_time_short($pause));
        $output_result_array[$key]['note2'] = sprintf(ERROR_AUCTION_WILL_START_LABEL, ' ' . $tmp_array[0] . '<br /><span class="font_large" style="color: #000;">' . $tmp_array[1] . '</span>');
        $check_time = strtotime($pause) - strtotime(date('H:i:s'));
        if( $check_time > 0 && $check_time < 60 ) {
          //$output_result_array[$key]['background'] = '#FF9999';
          if( $check_time <= 10 ) {
            $pause = substr($pause, -2);
            $output_result_array[$key]['note2'] = sprintf(ERROR_AUCTION_SECS_LABEL, '<br /><span class="font_large" style="color: #000;">' . $check_time . ' Secs</span>');
          }
        }
        continue;
      }

      $auto_timer = true;

      if( !$auction['countdown_bids'] ) {
        //$output_result_array[$key]['note2'] = '<img src="' . DIR_WS_IMAGES . 'no_countdown_timer.png' . '" border="0" class="nobg" />';
      }

      if( $auction['max_bids'] ) {
        $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$key . "'");
        $check_array = tep_db_fetch_array($check_query);

        if( $check_array['total'] >= $auction['max_bids'] ) {
          $action = 'end_auction';
        } elseif( $auction['bids_left'] ) {

          $bids_left = $auction['max_bids'] - $check_array['total'];
          $output_result_array[$key]['bids_left'] = '<span class="heavy mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_REMAIN, $bids_left) . '</span>';
        }

      } elseif($auction['countdown_bids']) {
        $check_query = tep_db_query("select bid_count, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$key . "'");
        if( !tep_db_num_rows($check_query) ) {
          $check_array = array(
            'bid_count' => 0,
            'last_modfied' => date('Y-m-d H:i:s'),
          );
        } else {
          $check_array = tep_db_fetch_array($check_query);
        }

        $index_array = tep_get_countdown_bids_index($auction, $check_array['bid_count']);

        //if( $check_array['bid_count'] >= $auction['countdown_bids'] ) {
        if( $check_array['bid_count'] >= $index_array['countdown_bids'] ) {
          $last_time = tep_mysql_to_time_stamp($check_array['last_modified']);
          //$offset_time = tep_mysql_to_time_stamp($auction['countdown_timer']);
          $offset_time = tep_mysql_to_time_stamp($index_array['countdown_timer']);
          $expire_time = tep_mysql_to_time_stamp(date('Y-m-d H:i:s'));

          if( $last_time+$offset_time < $expire_time ) {
            $output_result_array[$key]['note2'] = TEXT_INFO_AUCTION_ENDED;
            $output_result_array[$key]['background'] = '#CCCCCC';
            $action = 'end_auction';
          } else {
            $red_timer = false;

            $timer_auction = $last_time+$offset_time - $expire_time;
            $timer_string = gmstrftime("%H:%M:%S", $timer_auction);

            if( $timer_auction >= 0 && $timer_auction < 60 ) {
              $check_index = substr($index_array['countdown_timer'], -2);
              $timer_string = substr($timer_string, -2);

              if( $check_index <= 5 && $timer_auction <= 3 ) {
                $red_timer = true;
              } elseif( $check_index <= 10 && $timer_auction <= 5 ) {
                $red_timer = true;
              } elseif( $check_index > 10 && $timer_auction <= 10 ) {
                $red_timer = true;
              }

              if( $timer_auction < 10 ) {
                $timer_string = substr($timer_string, -1);
              }
              $timer_string .= ' Secs';
              if( $red_timer ) {
                $timer_string = '<span class="mark_red">' . $timer_string . '</span>';
              }
            }
            $action = 'timer_auction';
          }
        } elseif($index_array['countdown_bids']) {
          $action = 'timer_fixed';
          $timer_string = $index_array['countdown_timer'];
        }
      }

      if( $auto_timer && $auction['auto_timer'] && $action == 'browse_auctions' && $auction['live_off'] != '0000-00-00 00:00:00') {
        $expire_time = tep_mysql_to_time_stamp($auction['live_off'], false);
        $current_time = tep_mysql_to_time_stamp(date('Y-m-d H:i:s'), false);
        if( $expire_time < $current_time ) {
          $output_result_array[$key]['note2'] = TEXT_INFO_AUCTION_ENDED;
          $output_result_array[$key]['background'] = '#CCCCCC';
          $action = 'end_auction';
        } else {
          //$timer_string = tep_date_time_short($auction['live_off']);
          //$action = 'timer_auto';
        }
      }

      $auctions_id = $key;
      switch($action) {
        case 'end_auction':
          switch( $auction['auctions_type'] ) {
            case 0:
              require_once(DIR_WS_MODULES . 'auction_penny.php');
              unset($result_array[$key]);
              unset($bids_array[$key]);
              break;
            case 1:
              require_once(DIR_WS_MODULES . 'auction_lowest_unique.php');
              unset($result_array[$key]);
              unset($bids_array[$key]);
              break;
            case 2:
              require_once(DIR_WS_MODULES . 'auction_normal.php');
              unset($result_array[$key]);
              unset($bids_array[$key]);
              break;
            default:
              exit();
          }
          break;
        case 'timer_auction':
          $output_result_array[$key]['note2'] = sprintf(TEXT_INFO_AUCTION_EXPIRES, '<br /><span class="font_large" style="color: #000;">' . $timer_string . '</span>');
          //$output_result_array[$key]['background'] = '#FFFF77';
          break;
        case 'timer_fixed':
          if($index_array['countdown_bids']>1) {
           $output_result_array[$key]['note2'] = sprintf(TEXT_INFO_AUCTION_TIMER_PRESENT_X_BID, '<br /><span class="font_large nobg" style="color: #000;">' . $timer_string . '</span>');
        }
        else {
           $output_result_array[$key]['note2'] = sprintf(TEXT_INFO_AUCTION_TIMER_PRESENT_FIRST_BID, '<br /><span class="font_large nobg" style="color: #000;">' . $timer_string . '</span>');
        }
          break;
        case 'timer_auto':
          $output_result_array[$key]['note2'] = sprintf(TEXT_INFO_AUCTION_TIMER_AUTO, '<br /><span class="nobg" style="color: #000;">' . $timer_string . '</span>');
          break;
        default: 
          break;
      }
    }

    if( empty($result_array) ) {
      tep_auction_format_callback($output_result_array, true, true);
    }

    foreach($result_array as $key => $auction ) {
      if( !isset($bids_array[$key]) ) continue;

      $output_result_array[$key]['bg_flash'] = 1;

      switch( $auction['auctions_type'] ) {
        case 0:
          $nickname = $bids_array[$key]['customers_nickname'];
          //$nick_key = md5($nickname);
          $output_result_array[$key]['backup'] = $bids_array[$key]['signature'];
          if( $sticky_poll ) {
            $output_result_array[$key]['growl'][] = '<span class="heavy mark_green">' . sprintf(TEXT_INFO_BID_NEW_PRICE, $bids_array[$key]['bid_price'], $auction['auctions_name'], $nickname) . '</span>';
          }

          if( $customer_id ) {
            $output_result_array[$key]['note'] = sprintf(TEXT_INFO_LAST_LEADER, tep_truncate_string($nickname));
          } else {
            $output_result_array[$key]['note'] = '<span class="mark_red">' . ERROR_CUSTOMER_MUST_LOGIN . '</span>';
          }
          $output_result_array[$key]['input'] = tep_round($bids_array[$key]['bid_price']+(1/100),2);
          $output_result_array[$key]['bid_display'] = tep_round($bids_array[$key]['bid_price']+0.01,2);
          break;
        case 1:
          //$output_result_array[$key]['input'] = $bids_array[$key]['bid_price'];

          //$lowest_query = tep_db_query("select customers_id from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' group by bid_price having count(*) = 1 order by bid_price limit 1");
          $lowest_query = tep_db_query("select customers_id, count(bid_price) as winning from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' group by bid_price order by winning, bid_price, date_added limit 1");
          $lowest_array = tep_db_fetch_array($lowest_query);

          $current_query = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS .  " where customers_id = '" . (int)$lowest_array['customers_id'] . "'");
          $current_array = tep_db_fetch_array($current_query);

          $nickname = $current_array['customers_nickname'];
          //$nickname = $bids_array[$key]['customers_nickname'];

          $output_result_array[$key]['backup'] = $bids_array[$key]['signature'];

          if( $sticky_poll ) {
            $output_result_array[$key]['growl'][] = '<span class="heavy mark_green">' . sprintf(TEXT_INFO_BID_NEW, $auction['auctions_name'], $bids_array[$key]['customers_nickname']) . '</span>';
          }

          if( $customer_id ) {
            $output_result_array[$key]['note'] = sprintf(TEXT_INFO_LAST_BID, tep_truncate_string($nickname));
          } else {
            $output_result_array[$key]['note'] = '<span class="mark_red">' . ERROR_CUSTOMER_MUST_LOGIN . '</span>';
          }
          break;
        case 2:
          $nickname = $bids_array[$key]['customers_nickname'];
          $output_result_array[$key]['backup'] = $bids_array[$key]['signature'];
          if( $sticky_poll ) {
            $output_result_array[$key]['growl'][] = '<span class="heavy mark_green">' . sprintf(TEXT_INFO_BID_NEW_PRICE, $bids_array[$key]['bid_price'], $auction['auctions_name'], $nickname) . '</span>';
          }
          if( $customer_id ) {
            $output_result_array[$key]['note'] = sprintf(TEXT_INFO_LAST_LEADER, tep_truncate_string($nickname));
          } else {
            $output_result_array[$key]['note'] = '<span class="mark_red">' . ERROR_CUSTOMER_MUST_LOGIN . '</span>';
          }
          $output_result_array[$key]['input'] = tep_round($bids_array[$key]['bid_price']+(1/100),2);
          break;
        default:
          break;
      }
    }
    tep_auction_format_callback($output_result_array, true, false);

  } elseif( $action == 'set_auction' ) {

    if( !$customer_id ) {
      $output_result_array[$auctions_id]['note'] = ERROR_CUSTOMER_MUST_LOGIN;
      tep_auction_format_callback($output_result_array, true, true);
    }

    $auction_query = tep_db_query("select auctions_id, auctions_name, auctions_type, auctions_overbid, cap_price, start_price, bid_step, max_bids, bids_left, countdown_bids, countdown_timer, date_start, date_expire, start_pause, end_pause, status_id from " . TABLE_AUCTIONS . " where status_id = '1' and auctions_id = '" . (int)$auctions_id . "'");
    if( !tep_db_num_rows($auction_query) ) {
      $output_result_array[$auctions_id]['note'] = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_ENDED . '</span>';
      $output_result_array[$auctions_id]['background'] = '#CCCCCC';
      $output_result_array[$auctions_id]['growl'][] = ERROR_INVALID_AUCTION;
      tep_auction_format_callback($output_result_array, true, true);
    }
    $auction = tep_db_fetch_array($auction_query);

    $cbids_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $cbids_array = tep_db_fetch_array($cbids_query);
    if( $cbids_array['customers_bids']-$auction['bid_step'] <= 0 ) {
      $output_result_array[$auctions_id]['growl'][] = ERROR_NO_BIDS;
      tep_auction_format_callback($output_result_array, true, true);
    }

    $remaining = -1;
    $restart = false;
    $pause = tep_auction_check_pause($auction, $restart, $remaining);
    if( $pause ) {
      $output_result_array[$auctions_id]['growl'][] = '<spam class="heavy mark_red">' . sprintf(ERROR_AUCTION_PAUSED, $auction['auctions_name'], tep_date_time_short($restart) ) . '</span>';
      tep_auction_format_callback($output_result_array, true, true);
    }

    $pause = tep_auction_check_start($auction);
    if( strlen($pause) ) {
      $output_result_array[$auctions_id]['growl'][] = '<spam class="heavy mark_red">' . sprintf(ERROR_AUCTION_WILL_START, $auction['auctions_name'], tep_date_time_short($pause) ) . '</span>';
      tep_auction_format_callback($output_result_array, true, true);
    }

    if( $auction['max_bids'] ) {
      $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( $check_array['total'] >= $auction['max_bids'] ) {
        $action = 'end_auction';
      }
    } elseif($auction['countdown_bids']) {

      $check_query = tep_db_query("select bid_count, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auctions_id . "'");
      $check_array = tep_db_fetch_array($check_query);

      $index_array = tep_get_countdown_bids_index($auction, $check_array['bid_count']);

      //if( $check_array['bid_count'] >= $auction['countdown_bids'] ) {
      if( $check_array['bid_count'] >= $index_array['countdown_bids'] ) {

//$output_result_array[0]['growl'][] = tep_get_dump($index_array);
//tep_auction_format_callback($output_result_array, true, true);

        $last_time = strtotime($check_array['last_modified']);
        $expire_time = time();
        //$time_array = explode(':', $auction['countdown_timer']);
        $time_array = explode(':', $index_array['countdown_timer']);
        $offset_time = gmmktime( (int)$time_array[0], (int)$time_array[1], (int)$time_array[2], 1, 1, 1970);
        //$offset_time = gmmktime( 0, 0, 0, 1, 1, 1970);

        if( $last_time+$offset_time < $expire_time ) {
          $action = 'end_auction';
        }
      }
    }

    switch( $auction['auctions_type'] ) {
      case 0:
        require_once(DIR_WS_MODULES . 'auction_penny.php');
        break;
      case 1:
        require_once(DIR_WS_MODULES . 'auction_lowest_unique.php');
        break;
      case 2:
        require_once(DIR_WS_MODULES . 'auction_normal.php');
        break;
      default:
        exit();
    }
    tep_auction_format_callback($output_result_array, true, false);
  }
?>
<?php require('includes/application_mini_bottom.php'); ?>
