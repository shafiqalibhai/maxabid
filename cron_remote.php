<?php
      ini_set('error_reporting', E_ALL);
      ini_set('display_errors', 1);

  function sendToHost($host,$method,$path,$data='',$cookies_array='', $useragent=0,$secure=0) {
    $buf = '';

    // Supply a default method of GET if the one passed was empty
    if (empty($method)) {
      $method = 'GET';
    }
    $method = strtoupper($method);
    if ($secure) {
      $fp = fsockopen('ssl://'.$host, 443, $errno, $errstr, 30);
    } else {
      $fp = fsockopen($host, 80, $errno, $errstr, 10);
    }
    if( !$fp || get_resource_type($fp) != 'stream' ) {
      $buf = 'error: ' . $errno . '<br />string: ' . $errstr;
      return $buf;
    }
    if ($method == 'GET' && !empty($data) ) {
      $path .= '?' . $data;
    }
    $path = '/' . $path;


    if( is_array($cookies_array) && count($cookies_array) ) {
      $cookie_string = 'Cookie: ';
      foreach($cookies_array as $key => $value ) {
        $cookie_string .= $key . '=' .  $value . '; ';
      }
      $cookie_string = substr($cookie_string, 0, -2);
      $cookie_string .= "\r\n";
    }

    $output = 
"$method $path HTTP/1.1\r\n" . 
"Host: $host\r\n";
//"Accept-Encoding: gzip,deflate\r\n";

    if( isset($cookie_string) ) {
      $output .= $cookie_string;
    }

    if($method == 'POST') {
      $output .= 
"Content-type: application/x-www-form-urlencoded\r\n";
    }

    $output .= 
"Content-length: " . strlen($data) . "\r\n" .
"Connection: close\r\n\r\n" . 
$data;

    fwrite($fp, $output);

    while (!feof($fp)) {
      $buf .= fread($fp,4096);
    }

    fclose($fp);
//    $buf = strip_tags($buf);
//die(var_dump($output));
    return $buf;
  } 

  set_time_limit(0);
  $count=0;

  $random_domain = 'www.maxabid.co.uk';
  //$random_domain = 'www.example.com';
  //$buf = sendToHost($random_domain, 'GET', 'maxabid2/terminate_auctions.php', '');
  //$buf = sendToHost($random_domain, 'GET', 'maxabid2/notify_auctions.php', '');
  $buf = '';
  $buf .= sendToHost($random_domain, 'GET', 'terminate_auctions.php', '') . '<br /><br />';
  $buf .= sendToHost($random_domain, 'GET', 'notify_auctions.php', '');

  //$buf = sendToHost($random_domain, 'GET', 'notify_currencies.php', '');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><title>PHP Cron Script</title></head>
<body>
<p><?php echo $buf; ?></p>
</body>
</html>
