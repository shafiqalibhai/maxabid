<?php
/*
  $Id: logoff.php,v 1.13 2003/06/05 23:28:24 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Logoff page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGOFF);

  //$breadcrumb->add(NAVBAR_TITLE);
  $breadcrumb->add(TEXT_LOGIN, tep_href_link(FILENAME_LOGIN, '', 'SSL'));

  $navigation->clear_snapshot();
  tep_session_destroy();
  $session_started = false;
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
      <div><?php echo TEXT_MAIN; ?></div>
<?php
  $html_lines_array = array();
  $html_lines_array[] = '<div class="rspacer ralign"><a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE . '</a></div>' . "\n";
?>
<?php require('includes/objects/html_content_bottom.php'); ?>
<?php require('includes/objects/html_end.php'); ?>
