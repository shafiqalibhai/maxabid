<?php
/*
  $Id: checkout_shipping_address.php,v 1.15 2003/06/09 23:03:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Checkout Shipping address page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }

  // needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SHIPPING_ADDRESS);

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
  if ($order->content_type == 'virtual') {
    if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
    $shipping = false;
    if (!tep_session_is_registered('sendto')) tep_session_register('sendto');
    $sendto = false;
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
  }

  $error = false;
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch($action) {
    case 'submit': 
      // process the selected shipping destination
      if (isset($_POST['address'])) {
        $reset_shipping = false;
        if (tep_session_is_registered('sendto')) {
          if ($sendto != $_POST['address']) {
            if (tep_session_is_registered('shipping')) {
              $reset_shipping = true;
            }
          }
        } else {
          tep_session_register('sendto');
        }

        $sendto = (int)$_POST['address'];

        $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
        $check_address = tep_db_fetch_array($check_address_query);

        if ($check_address['total'] == '1') {
          if ($reset_shipping == true) tep_session_unregister('shipping');
          tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
        } else {
          tep_session_unregister('sendto');
        }
      } else {
        if (!tep_session_is_registered('sendto')) tep_session_register('sendto');
        $sendto = $customer_default_address_id;

        tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
      }
      break;
    default:
      break;
  }

// if no shipping destination address was selected, use their own address as default
  if (!tep_session_is_registered('sendto')) {
    $sendto = $customer_default_address_id;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_SHIPPING_ADDRESS, '', 'SSL'));

  $addresses_count = tep_count_customer_address_book_entries();
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_address.address[0]) {
    document.checkout_address.address[buttonSelect].checked=true;
  } else {
    document.checkout_address.address.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}

//--></script>
<?php require(DIR_WS_INCLUDES . 'form_check.js.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div><h1><?php echo HEADING_TITLE; ?></h1></div>
      <div class="heavy"><?php echo TABLE_HEADING_SHIPPING_ADDRESS; ?></div>
      <div class="bounder">
        <div class="floater hpad">
          <div class="vpad"><?php echo TEXT_SELECTED_SHIPPING_DESTINATION; ?></div>
        </div>
        <div class="floatend hpad">
          <div class="bounder">
            <div class="heavy floater calign"><?php echo TITLE_SHIPPING_ADDRESS . '<br />' . tep_image(DIR_WS_IMAGES . 'arrow_south_east.gif'); ?></div>
            <div class="floater hpad"><?php echo tep_address_label($customer_id, $sendto, true, ' ', '<br />'); ?></div>
          </div>
        </div>
      </div>
<?php
  if ($addresses_count > 1) {
?>
      <div class="bounder tspacer"><?php echo tep_draw_form('checkout_shipping_address', tep_href_link($g_script, 'action=submit', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="heavy"><?php echo TABLE_HEADING_ADDRESS_BOOK_ENTRIES; ?></div>
        <div class="bounder">
          <div class="floater quarter3"><?php echo TEXT_SELECT_OTHER_SHIPPING_DESTINATION; ?></div>
          <div class="floatend quarter calign"><?php echo '<b>' . TITLE_PLEASE_SELECT . '</b><br />' . tep_image(DIR_WS_IMAGES . 'arrow_east_south.gif'); ?></div>
<?php
    $radio_buttons = 0;

    $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "'");
    while ($addresses = tep_db_fetch_array($addresses_query)) {
      $format_id = tep_get_address_format_id($addresses['country_id']);

      if ($addresses['address_book_id'] == $sendto) {
        $selected_class = 'moduleRowSelected';
      } else {
        $selected_class = 'moduleRow';
      }
?>
          <div class="bounder <?php echo $selected_class; ?>">
            <div class="floater quarter3">
               <div class="heavy hpad"><?php echo tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></div>
               <div class="hpad"><?php echo tep_address_format($format_id, $addresses, true, ' ', ', '); ?></div>
            </div>
            <div class="floatend quarter calign"><?php echo tep_draw_radio_field('address', $addresses['address_book_id'], ($addresses['address_book_id'] == $sendto)); ?></div>
          </div>
<?php
      $radio_buttons++;
    }
?>
        </div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ADD_ADDRESS . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php
  } else {
?>
      <div class="bounder tspacer">
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_ADD_ADDRESS . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '" class="mbutton2">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>
      </div>
<?php
  }
?>
      <div class="bounder bspacer"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%" align="right"><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            </tr>
          </table></td>
          <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
          <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_DELIVERY; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
        </tr>
      </table></div>
<?php require('includes/objects/html_end.php'); ?>
