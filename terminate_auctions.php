<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Terminate Past Auctions Module
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  ini_set('error_reporting', E_ALL);
  ini_set('display_errors', 1);

  define('MAX_SEND_REMINDERS', 100);
  define('TERMINATE_AUCTION_THRESHOLD', 2592000);

  set_time_limit(0);

//-MS- Email Templates added
  $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='auction_removed'");
  if( !tep_db_num_rows($mail_query) ) {
    tep_exit();
  }
  $email_array = tep_db_fetch_array($mail_query);
//-MS- Email Templates added EOM
  $check_array = array();
  $check_query_raw = "select ah.*  from " . TABLE_CUSTOMERS_BASKET . " cb, " . TABLE_AUCTIONS_HISTORY . " ah where ah.customers_id=cb.customers_id and cb.auctions_history_id != '0' and (unix_timestamp(now()) - unix_timestamp(ah.completed)) > " . TERMINATE_AUCTION_THRESHOLD . " group by ah.customers_id limit " . MAX_SEND_REMINDERS;
  tep_query_to_array($check_query_raw, $check_array, 'customers_id');

  foreach ($check_array as $key => $val ) {

    $customers_query = tep_db_query("select concat(customers_firstname, ' ', customers_lastname) as customers_name, customers_email_address, customers_telephone from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$key . "'");
    if( !tep_db_num_rows($customers_query) ) {
      continue;
    }
    $customers_array = tep_db_fetch_array($customers_query);

//-MS- Email Templates added
    $right_col = tep_email_column(DEFAULT_EMAIL_ABSTRACT_ZONE_ID);
    $temp_array = array(
      'CUSTOMER_NAME' => $customers_array['customers_name'],
      'STORE_NAME' => STORE_OWNER,
      'ACCOUNT_URL' => '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL', false) . '">' . tep_href_link(FILENAME_LOGIN, '', 'SSL', false)  . '</a>',
      'RIGHT_COL' => $right_col,
    );
    $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
    tep_mail($customers_array['customers_name'], $customers_array['customers_email_address'], $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
//-MS- Email Templates added EOM
    tep_db_query("delete from " . TABLE_AUCTIONS_HISTORY . " where auctions_id='" . (int)$val['auctions_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where auctions_history_id='" . (int)$val['auctions_id'] . "'");
  }
  tep_exit();
