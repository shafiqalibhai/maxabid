<?php
/*
  $Id: account_password.php,v 1.1 2003/05/19 19:55:45 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Password forgotten page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Removed tables, using CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_PASSWORD);

  switch($action) {
    case 'process':
      $error = false;
      $password_current = isset($_POST['password_current'])?tep_db_prepare_input($_POST['password_current']):'';
      $password_new = isset($_POST['password_new'])?tep_db_prepare_input($_POST['password_new']):'';
      $password_confirmation = isset($_POST['password_confirmation'])?tep_db_prepare_input($_POST['password_confirmation']):'';

      if (strlen($password_current) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;
        $messageStack->add('account_password', ENTRY_PASSWORD_CURRENT_ERROR);
      } elseif (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;
        $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR);
      } elseif ($password_new != $password_confirmation) {
        $error = true;
        $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
      }

      if ($error == false) {
        $check_customer_query = tep_db_query("select customers_password from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
        $check_customer = tep_db_fetch_array($check_customer_query);

        if (tep_validate_password($password_current, $check_customer['customers_password'])) {
          tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password_new) . "' where customers_id = '" . (int)$customer_id . "'");
          tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

          $messageStack->add_session('account', SUCCESS_PASSWORD_UPDATED, 'success');
          tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
        } else {
          $error = true;
          $messageStack->add('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
        }
      }
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  require('includes/objects/html_body_header.php');
?>
      <div><?php echo tep_draw_form('account_password', tep_href_link($g_script, 'action=process', 'SSL')); ?><fieldset><legend><?php echo MY_PASSWORD_TITLE; ?></legend>
        <div class="halfer floater">
          <div class="vpad"><label for="log_current" class="halfer floater"><?php echo ENTRY_PASSWORD_CURRENT; ?></label><?php echo tep_draw_password_field('password_current', '', 'id="log_current"'); ?></div>
          <div class="vpad"><label for="log_new" class="halfer floater"><?php echo ENTRY_PASSWORD_NEW; ?></label><?php echo tep_draw_password_field('password_new', '', 'id="log_new"'); ?></div>
          <div class="vpad"><label for="log_confirm" class="halfer floater"><?php echo ENTRY_PASSWORD_CONFIRMATION; ?></label><?php echo tep_draw_password_field('password_confirmation', '', 'id="log_confirm"'); ?></div>
        </div>
        <div class="halfer floater">
          <div class="lspacer"><?php echo TEXT_MAIN; ?></div>
        </div>
        <div class="cleaner wider ralign"><?php echo tep_image_submit('button_confirm.gif', IMAGE_BUTTON_CONFIRM); ?></div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
