<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Auctions Main Script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $auction_status_array = array(
    array('id' => 3, 'text' => 'Stacked'),
    array('id' => 0, 'text' => 'Coming Soon'),
    array('id' => 1, 'text' => 'Live Auction'),
    array('id' => 2, 'text' => 'Ended Auction'),
  );
  $auction_status_array2 = array(
    '3' => 'Stacked',
    '0' => 'Coming Soon',
    '1' => 'Live Auction',
    '2' => 'Ended Auction',
  );

  $auction_types_array = array(
    array('id' => 99, 'text' => 'Select the auction type'),
    array('id' => 0, 'text' => 'Penny Auction'),
    array('id' => 1, 'text' => 'Lowest Unique Bid'),
    array('id' => 2, 'text' => 'Normal Auction'),
  );
  $auction_types_array2 = array(
          '99' => 'Select the auction type',
          '0' => 'Penny Auction',
          '1' => 'Lowest Unique Bid',
          '2' => 'Normal Auction');

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  if (isset($_POST['update_auction_x']) || isset($_POST['update_auction_y'])) $action='update_confirm';

  $aID = isset($_GET['aID'])?(int)$_GET['aID']:0;
  $agID = isset($_GET['agID'])?(int)$_GET['agID']:0;

  switch ($action) {
    case 'delete_confirm':
      $aID = isset($_POST['auctions_id'])?(int)$_POST['auctions_id']:0;
      if( empty($aID) ) {
        $messageStack->add_session(ERROR_AUCTION_INVALID);
        tep_redirect(tep_href_link($g_script));
      }

      $check_query = tep_db_query("select auctions_image from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
      if( !tep_db_num_rows($check_query) ) {
        $messageStack->add_session(ERROR_AUCTION_INVALID);
        tep_redirect(tep_href_link($g_script));
      }

      $check_array = tep_db_fetch_array($check_query);
      if( !empty($check_array['auctions_image']) && is_file(DIR_FS_CATALOG_IMAGES . $check_array['auctions_image']) ) {
        @unlink(DIR_FS_CATALOG_IMAGES . $check_array['auctions_image']);
      }

      $check_query_raw = "select auctions_image from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "'";
      $check_array = array();
      tep_query_to_array($check_query_raw, $check_array);
      for($i=0, $j=count($check_array); $i<$j; $i++) {
        @unlink(DIR_FS_CATALOG_IMAGES . $check_array['auctions_image']);
      }
      clearstatcache();

      tep_db_query("delete from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");

      tep_db_query("delete from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_SEO_TO_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
      tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_org like '%auctions_id=" . (int)$aID . "'");

      $messageStack->add_session(WARNING_AUCTION_DELETED, 'warning');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) ));
      break;

    case 'insert_confirm':
      $products_id = isset($_POST['products_id'])?(int)$_POST['products_id']:0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $messageStack->add_session(ERROR_AUCTION_INVALID_PRODUCT);
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) ));
      }

      $image_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id  = '" . (int)$products_id . "'");
      if( tep_db_num_rows($image_query) ) {
        $image_array = tep_db_fetch_array($image_query);     
      } else {
        $image_array['products_image'] = 'image_not_available.jpg';
      }
    case 'update_confirm':
      $agID = isset($_POST['auctions_group_id'])?(int)$_POST['auctions_group_id']:0;
      $auctions_type = (int)$_POST['auctions_type'];
      $auctions_overbid = isset($_POST['auctions_overbid'])?1:0;
      $bids_added = isset($_POST['bids_added'])?1:0;
      $bids_left = isset($_POST['bids_left'])?1:0;

      if( $action == 'update_confirm' ) {
        $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
        $check_array = tep_db_fetch_array($check_query);
        if( !$check_array['total'] ) {
          $messageStack->add_session(ERROR_AUCTION_INVALID);
          tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) ));
        }
      }

      $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$agID . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $check_query = tep_db_query("select auctions_group_id from " . TABLE_AUCTIONS_GROUP . " limit 1");
        if( !tep_db_num_rows($check_query) ) {
          $messageStack->add_session(ERROR_AUCTION_CREATE_GROUP_FIRST);
          tep_redirect(tep_href_link(FILENAME_AUCTIONS_GROUP, 'action=new'));
        }
        $check_array = tep_db_fetch_array($check_query);
        $agID = $check_array['auctions_group_id'];
      }

      if( $auctions_type == 1 ) {
        $auctions_overbid = 1;
      }

      $countdown_array = array(); 
      if( !empty($_POST['countdown_bids']) && is_array($_POST['countdown_bids']) ) {
        for( $i=0, $j=count($_POST['countdown_bids']); $i<$j; $i++) {
          if( !empty($_POST['countdown_timer'][$i]) ) {
             $countdown_array[(int)$_POST['countdown_bids'][$i]] = tep_db_prepare_input($_POST['countdown_timer'][$i]);
          }

          
          //echo $_POST['countdown_bids'][$i];
        }
      }
      //$end_price = $countdown_array[5];
      //unset($countdown_array[5]);
      
      $countdown_bids_string = $countdown_timer_string = '';
      if( count($countdown_array) ) {
        $countdown_bids_string = implode(',',array_keys($countdown_array));
        $countdown_timer_string = implode(',',array_values($countdown_array));
      }
      //$countdown_bids_string .= $end_price*100;
      //$countdown_timer_string .= '1';
      $auction_array = array(
        'auctions_group_id' => (int)$agID,
        'auctions_name' => tep_db_prepare_input($_POST['auctions_name']),
        'auctions_description' => tep_db_prepare_input($_POST['auctions_description']),
        'products_id' => (int)$_POST['products_id'],
        'auctions_overbid' => (int)$auctions_overbid,
        'auctions_type' => (int)$auctions_type,
        'status_id' => (int)$_POST['status_id'],
        'bid_step' => (int)$_POST['bid_step'],
        'max_bids' => (int)$_POST['max_bids'],
        //'countdown_bids' => (int)$_POST['countdown_bids'],
        //'countdown_timer' => tep_db_prepare_input($_POST['countdown_timer']),
        'countdown_bids' => $countdown_bids_string,
        'countdown_timer' => $countdown_timer_string,
        'auto_timer' => max( (int)$_POST['auto_timer'], 0),
        'sort_id' => (int)$_POST['sort_id'],
        'date_start' => tep_db_prepare_input($_POST['date_start']),
        'date_expire' => tep_db_prepare_input($_POST['date_expire']),
        'start_pause' => tep_db_prepare_input($_POST['start_pause']),
        'end_pause' => tep_db_prepare_input($_POST['end_pause']),
        'cap_price' => tep_db_prepare_input($_POST['cap_price']),
        'start_price' => tep_db_prepare_input($_POST['start_price']),
        'shipping_cost' => tep_db_prepare_input($_POST['shipping_cost']),
        'bids_added' => $bids_added,
        'bids_left' => $bids_left,
        'live_off' => tep_db_prepare_input($_POST['live_off']),
        'end_price' => tep_db_prepare_input($_POST['end_price']),
      );

      if( $auction_array['auctions_type'] == 1 && $auction_array['cap_price'] <= 0 ) {
        $products_query = tep_db_query("select products_price from " . TABLE_PRODUCTS . " where products_id  = '" . (int)$auction_array['products_id'] . "'");
        $products_array = tep_db_fetch_array($products_query);
        $auction_array['cap_price'] = $products_array['products_price'];
      }

      if( $auction_array['countdown_bids'] && (!$auction_array['countdown_timer'] || $auction_array['countdown_timer'] == '00:00:00') ) {
        $auction_array['countdown_bids'] = 0;
      }

      if( $auction_array['max_bids'] && $auction_array['countdown_bids'] > $auction_array['max_bids'] ) {
        $auction_array['max_bids'] = 0;
      }

      $auction_array['date_start'] .= ' ' . tep_db_prepare_input($_POST['date_start_time']);
      $auction_array['date_expire'] .= ' ' . tep_db_prepare_input($_POST['date_expire_time']);

      $start = strtotime($auction_array['date_start']);
      $end = strtotime($auction_array['date_expire']);

      if( $end && $start >= $end ) {
        $auction_array['date_expire'] = '';
      }

      if( isset($_FILES['auctions_image']) && !empty($_FILES['auctions_image']['name']) ) {
        $cImage = new upload('auctions_image', DIR_FS_CATALOG_IMAGES);
        if( $cImage->c_result ) {
          $auction_array['auctions_image'] = $cImage->filename;
        }
      }
/*
      if( isset($_POST['auctions_overbid']) && (int)$_POST['auctions_overbid'] > 0 ) {
        $sql_data_array['auctions_overbid'] = (int)$_POST['auctions_overbid'];
      }
      if( isset($_POST['bid_step']) && (int)$_POST['bid_step'] >= 1 ) {
        $sql_data_array['bid_step'] = (int)$_POST['bid_step'];
      }
      if( isset($_POST['max_bids']) && (int)$_POST['max_bids'] > 0 ) {
        $sql_data_array['max_bids'] = (int)$_POST['max_bids'];
      }
      if( isset($_POST['countdown_bids']) && (int)$_POST['countdown_bids'] > 0 ) {
        $sql_data_array['countdown_bids'] = (int)$_POST['countdown_bids'];
      }

      if( isset($_POST['start_price']) && (float)$_POST['start_price'] > 0 ) {
        $sql_data_array['start_price'] = (float)$_POST['start_price'];
      }

      if( isset($_POST['shipping_cost']) && (float)$_POST['shipping_cost'] > 0 ) {
        $sql_data_array['shipping_cost'] = (float)$_POST['shipping_cost'];
      }

      $sql_data_array['date_expire'] = isset($_POST['date_expire'])?tep_db_prepare_input($_POST['date_expire']):null;
      $sql_data_array['start_pause'] = isset($_POST['start_pause'])?tep_db_prepare_input($_POST['start_pause']):null;
      $sql_data_array['end_pause'] = isset($_POST['end_pause'])?tep_db_prepare_input($_POST['end_pause']):null;
      $sql_data_array['sort_id'] = isset($_POST['sort_id'])?(int)$_POST['sort_id']:100;
*/
      if($action == 'insert_confirm') {
        if($auction_array['status_id'] == 1 && $auction_array['auto_timer'] ) {
          $off_time = date('Y-m-d H:i:s', strtotime('+ ' . $auction_array['auto_timer'] . 'days' ) );
          $auction_array['live_off'] = $off_time;
        }

        tep_db_perform(TABLE_AUCTIONS, $auction_array);
        $aID = tep_db_insert_id();
        if( !isset($auction_array['auctions_image']) && is_file(DIR_FS_CATALOG_IMAGES . $image_array['products_image']) ) {
          $tmp_array = explode('.', $image_array['products_image']);
          $dst_image =  $tmp_array[0] . '-' . $aID . '.' . $tmp_array[count($tmp_array)-1];

          $src = DIR_FS_CATALOG_IMAGES . $image_array['products_image'];
          $dst = DIR_FS_CATALOG_IMAGES . $dst_image;
          copy($src, $dst);
          tep_db_query("update " . TABLE_AUCTIONS . " set auctions_image = '" . tep_db_input($dst_image) . "' where auctions_id = '" . (int)$aID . "'");
        }
        $messageStack->add_session(SUCCESS_AUCTION_CREATED, 'success');
      } elseif ($action == 'update_confirm') {

        if( $auction_array['status_id'] == 1 ) {
          tep_db_query("delete from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$aID . "'");
        }

        if( $auction_array['status_id'] == 0 || $auction_array['status_id'] == 2 || !$auction_array['auto_timer'] ) {
          $auction_array['live_off'] = 0;
        }

        tep_db_perform(TABLE_AUCTIONS, $auction_array, 'update', "auctions_id = '" . (int)$aID . "'");
        $messageStack->add_session(SUCCESS_AUCTION_UPDATED, 'success');
      }
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) . 'aID=' . $aID ));

      break;

    case 'insert_products_confirm':
      $products_array = isset($_POST['pc_id'])?$_POST['pc_id']:array();

      if( empty($products_array) || !is_array($products_array) ) {
        $messageStack->add_session(ERROR_AUCTION_PRODUCTS_TICK);
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=select_products'));
      }
      foreach( $products_array as $key => $value ) {
        $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " where products_id = '" . (int)$key . "'");
        $check_array = tep_db_fetch_array($check_query);
        if( !$check_array['total'] ) {
          $messageStack->add_session(ERROR_AUCTION_PRODUCTS_INVALID);
          tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=select_products'));
        }

        $info_query = tep_db_query("select p.products_image, pd.products_name, pd.products_description from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id=pd.products_id and p.products_id= '" . (int)$key . "' and pd.language_id = '" . (int)$languages_id . "' order by products_name");
        $info_array = tep_db_fetch_array($info_query);
        //$products_name = TEXT_INFO_AUCTION_PRODUCT_PREFIX . ' ' . $info_array['products_name'];
        //$info_array['products_name'] = $products_name;

        $sql_data_array = array(
          'products_id' => (int)$key,
          'auctions_group_id' => (int)$agID,
          'auctions_name' => $info_array['products_name'],
          'auctions_description' => $info_array['products_description'],
          'auto_timer' => AUCTION_AUTO_TIMER,
          'live_off' => '0000-00-00 00:00:00',
          //'auctions_image' => $info_array['products_image'],
        );

        tep_db_perform(TABLE_AUCTIONS, $sql_data_array);
        $aID = tep_db_insert_id();

        $sql_data_array = array(
          'auctions_name' => substr($info_array['products_name'], 0, 50) . ' (#' . $aID . ')',
          'auctions_type' => '99',
          'status_id' => '3',
        );

        $image_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id  = '" . (int)$key . "'");
        if( tep_db_num_rows($image_query) ) {
          $image_array = tep_db_fetch_array($image_query);     
        } else {
          $image_array['products_image'] = 'image_not_available.jpg';
        }

        if( is_file(DIR_FS_CATALOG_IMAGES . $image_array['products_image']) ) {
          $src = DIR_FS_CATALOG_IMAGES . $image_array['products_image'];
          $dst = DIR_FS_CATALOG_IMAGES . $image_array['products_image'] . '-' . $aID;
          copy($src, $dst);
          $sql_data_array['auctions_image'] = $image_array['products_image'] . '-' . $aID;
        }
        tep_db_perform(TABLE_AUCTIONS, $sql_data_array, 'update', "auctions_id = '" . (int)$aID . "'");

      }
      $messageStack->add_session(SUCCESS_AUCTION_PRODUCTS_ENTERED);
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      break;

    case 'new':
    case 'edit':
      $check_query = tep_db_query("select * from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
      if( !tep_db_num_rows($check_query) ) {
        $action = 'new';
        $auction_array = array(
          'auctions_id' => $aID,
          'auctions_group_id' => 0,
          'products_id' => 0,
          'auctions_name' => '',
          'auctions_description' => '',
          'auctions_overbid' => 0,
          'auctions_type' => '',
          'status_id' => 0,
          'bid_step' => 1,
          'max_bids' => 0,
          'countdown_bids' => 0,
          'countdown_timer' => 0,
          'auto_timer' => AUCTION_AUTO_TIMER,
          'sort_id' => 0,
          'date_start' => 0,
          'date_start_time' => 0,
          'date_expire' => 0,
          'date_expire_time' => 0,
          'start_pause' => 0,
          'end_pause' => 0,
          'cap_price' => 0,
          'start_price' => 0,
          'shipping_cost' => 0,
          'bids_added' => 0,
          'bids_left' => 0,
          'live_off' => '0000-00-00 00:00:00',
        );
      } else {
        $auction_array = tep_db_fetch_array($check_query);

        $tmp_array = explode(' ', $auction_array['date_start']);
        $auction_array['date_start'] = $tmp_array[0];
        $auction_array['date_start_time'] = isset($tmp_array[1])?$tmp_array[1]:'';

        $tmp_array = explode(' ', $auction_array['date_expire']);
        $auction_array['date_expire'] = $tmp_array[0];
        $auction_array['date_expire_time'] = isset($tmp_array[1])?$tmp_array[1]:'';
      }
      break;
    default:
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/timepick/jquery-ui-timepicker.css" />
<script type="text/javascript" src="includes/javascript/jquery/jquery.js"></script>
<script type="text/javascript" src="includes/javascript/jquery/jquery.dump.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/ui/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/timepick/jquery.ui.timepicker.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/general.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery.validate.js"></script>
<?php
  if( $action == 'edit' || $action == 'new' ) {
    $mce_str = 'auctions_description'; // YOURCODEHERE // Comma separated list of textarea names
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    include "includes/javascript/tiny_mce/general2.php";
  }
?>
<script>
$.validator.setDefaults({
	submitHandler: function() { 
  if($('[name="auctions_type"]').val() == 99) {
    alert('Please select the auction type');
    return false;
  }
  $(form).submit(); 
  }
});
$(document).ready(function() {
if($('[name="status_id"]').val() == 3) {
$("[name='edit_auction'] :input").attr("disabled", true);
$("[name='status_id']").removeAttr("disabled");
$('[name="auctions_type"]').val('99');
$('[name="update_auction"]').attr('disabled', 'disabled');
}

$('[name="status_id"]').change(function () {
  if($(this).val() != 3) {
    $("[name='edit_auction'] :input").removeAttr("disabled");
  }
  else {
    $("[name='edit_auction'] :input").attr("disabled", true);
$("[name='status_id']").removeAttr("disabled");
  }                        
  if($(this).val() == 1 && $('#auction_date_expire').val() != ' ') {
    $('#auction_date_expire').val('<?php print date('Y-m-d', strtotime('+ ' . AUCTION_AUTO_TIMER . 'days' ) ); ?>');
    $('#auction_date_expire_time').val('<?php print date('H:i', strtotime('+ ' . AUCTION_AUTO_TIMER . 'days' ) ); ?>');
  }
});

$('[name="auctions_type"]').change(function () {
//alert($('[name="status_id"]').val());
if($('[name="status_id"]').val() != 3) { 
  if($(this).val() == 1) {
    lub_type_auction();
  }
  else if($(this).val() == 0) {
    penny_type_auction();
  }
}
else {
  //$("[name='edit_auction'] :input").attr("disabled", true);
}    
});
	// validate the form when it is submitted
	$('[name="edit_auction"]').validate({
  
  });
  
  $('input#end_price').keyup(function() {
    
    $('#countdown_bids_4').val(parseInt($(this).val()*100));
    $('#auction_countdown_timer4').val('00:00:01');
  });  
});
function penny_type_auction() {
//$('[name="edit_auction"]')[0].reset();
$('[name="update_auction"]').removeAttr('disabled');
$('[name="auctions_overbid"]').prop('checked', true);
$('[name="bids_left"]').prop('checked', false);
$('[name="bids_left"]').hide();
$('[name="bid_step"]').val('1');
$('[name="max_bids"]').val('');
$('[name="max_bids"]').hide();
$('[name="countdown_bids[]"]').show();
$('[name="countdown_timer[]"]').show();
$('[name="bids_timer"]').show();
$('[name="bids_label"]').show();
$('#end_price').show();
$('[name="bids_input"]').show();
$('[name="bids_input_tr"]').show();
$('[name="bids_label_tr"]').show();
$('[name="pause_tr"]').show();
$('#countdown_bids_0').val('1');
$('#auction_countdown_timer0').val('00:00:59');
$('[name="cap_price"]').val('');
$('[name="start_price"]').val('0.00');
$('[name="shipping_cost"]').val('4.99');
}

function lub_type_auction() {
$('#end_price').hide();
$('[name="update_auction"]').removeAttr('disabled');
$('[name="auctions_overbid"]').prop('checked', true);
$('[name="bids_left"]').prop('checked', true);
$('[name="bid_step"]').val('1');
$('[name="max_bids"]').val('100');
$('[name="countdown_bids[]"]').hide();
$('[name="countdown_timer[]"]').hide();
$('[name="bids_timer"]').hide();
$('[name="bids_label"]').hide();
$('[name="bids_input"]').hide();
$('[name="bids_input_tr"]').hide();
$('[name="bids_label_tr"]').hide();
$('[name="pause_tr"]').hide();
$('[name="cap_price"]').val('9.99');
$('[name="start_price"]').val('0.01');
$('[name="shipping_cost"]').val('2.99');
}
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  $auction_groups_array = array();
  $auction_groups_array[0] = array('id' => 0, 'text' => TEXT_INFO_GROUP_ALL);
  $auction_groups_query_raw = "select auctions_group_id as id, auctions_group_name as text from " . TABLE_AUCTIONS_GROUP . " order by auctions_group_name";
  tep_query_to_array($auction_groups_query_raw, $auction_groups_array, 'id');
  //array_unshift($auction_groups_array, array('id' => 0, 'text' => TEXT_INFO_GROUP_ALL));

  if( $action == 'edit' || $action == 'new') {
    $title = ($action=='new')?HEADING_TITLE_NEW:HEADING_TITLE_EDIT;

    $buttons = array(
      '<a href="' . tep_href_link(FILENAME_AUCTIONS, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>',
    );

    if( $action == 'edit' ) {
      $buttons[] = tep_image_submit('button_update.gif',IMAGE_UPDATE, 'name="update_auction"');
    } else {
      $buttons[] = tep_image_submit('button_insert.gif',IMAGE_INSERT);
    }
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo $title; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form("edit_auction", $g_script, tep_get_all_get_params(array('action', 'aID')) . 'action=insert_confirm&aID=' . $aID, 'post', 'enctype="multipart/form-data"'); ?><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
              <tr>
                <td class="main"><?php echo TEXT_INFO_AUCTION_NAME; ?></td>
                <td><?php echo tep_draw_input_field('auctions_name', $auction_array['auctions_name']); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_INFO_AUCTION_IMAGE; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_file_field('auctions_image'); ?></td>
<?php
    if( !empty($auction_array['auctions_image']) ) {
?>
                    <td class="rpad"><?php echo $auction_array['auctions_image']; ?></td>
<?php
    }
?>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_INFO_PRODUCT_ID; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_input_field('products_id', $auction_array['products_id']); ?></td>
<?php
    $product_name = tep_get_products_name($auction_array['products_id']);
    if( !empty($product_name) ) {
?>
                    <td class="main"><?php echo TEXT_INFO_PRODUCT_NAME . '&nbsp;<b>' . $product_name . '</b>'; ?></td>
<?php
    }
?>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_STATE; ?></td>
                <td><?php echo tep_draw_pull_down_menu('status_id', array_values($auction_status_array), $auction_array['status_id']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_GROUP_SELECT; ?></td>
                <td><?php echo tep_draw_pull_down_menu('auctions_group_id', array_values($auction_groups_array), $auction_array['auctions_group_id']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_TYPE; ?></td>
                <td><?php echo tep_draw_pull_down_menu('auctions_type', array_values($auction_types_array), $auction_array['auctions_type']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_BIDS_ADDED; ?></td>
                <td><?php echo tep_draw_checkbox_field('bids_added', 'on', $auction_array['bids_added']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_BIDS_LEFT; ?></td>
                <td><?php echo tep_draw_checkbox_field('bids_left', 'on', $auction_array['bids_left']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_OVERBID; ?></td>
                <td><?php echo tep_draw_checkbox_field('auctions_overbid', 'on', $auction_array['auctions_overbid']?true:false); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_BID_STEP; ?></td>
                <td><?php echo tep_draw_input_field('bid_step', $auction_array['bid_step']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_MAX_BIDS; ?></td>
                <td><?php echo tep_draw_input_field('max_bids', $auction_array['max_bids']); ?></td>
              </tr>

<?php
    $countdown_bids_array = explode(',', $auction_array['countdown_bids']);
    $j = count($countdown_bids_array);

    for($i=0; $i<5; $i++) {
      if( !isset($countdown_bids_array[$i]) ) {
        $countdown_bids_array[$i] = 0;
      }
    }

    $countdown_timer_array = explode(',', $auction_array['countdown_timer']);
    for($i=0; $i<5; $i++) {
      if( !isset($countdown_timer_array[$i]) ) {
        $countdown_timer_array[$i] = 0;
      }
    }
?>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr name='bids_timer'>
                <td class="heavy" colspan="2"><?php echo TEXT_INFO_AUCTION_BIDS_TIMERS; ?></td>
              </tr>
<?php
    for( $i=0; $i<5; $i++ ) {
?>
              <tr name='bids_label_tr'>
                <td class="lpad" name='bids_label'><?php echo TEXT_INFO_AUCTION_COUNTDOWN_BIDS; ?></td>
                <td><?php echo tep_draw_input_field('countdown_bids[]', $countdown_bids_array[$i], 'id=countdown_bids_'.$i); ?></td>
              </tr>
              <tr name='bids_label_tr'>
                <td class="lpad" name='bids_input'><?php echo TEXT_INFO_AUCTION_COUNTDOWN_TIMER; ?></td>
                <td><?php echo tep_draw_input_field('countdown_timer[]', $countdown_timer_array[$i], 'id="auction_countdown_timer' . $i . '"'); ?></td>
              </tr>
<?php
    }
?>
<tr name='bids_label_tr'>
                <td class="lpad" name='bids_label'><?php echo 'End price'; ?></td>
                <td><?php echo tep_draw_input_field('end_price', $auction_array['end_price'], 'id=end_price'); ?></td>
              </tr>
              <tr><td colspan="2">&nbsp;</td></tr>

<?php
/*
              <tr>
                <td class="lpad" colspan="2"><div class="bounder add_field_section">
                  <div class="add_field">
                    <div class="vlinepad heavy floater quarter"><?php echo TEXT_INFO_AUCTION_COUNTDOWN_BIDS; ?></div>
                    <div class="vlinepad heavy floater quarter3"><?php echo tep_draw_input_field('countdown_bids', $auction_array['countdown_bids']); ?></div>
                    <div class="vlinepad heavy floater quarter"><?php echo TEXT_INFO_AUCTION_COUNTDOWN_TIMER; ?></div>
                    <div class="vlinepad heavy floater quarter3"><?php echo tep_draw_input_field('countdown_timer', $auction_array['countdown_timer'], 'class="auction_countdown_timer"'); ?></div>
                  </div>
                </div></td>
              </tr>

                  <tr>
                    <td><?php echo TEXT_INFO_AUCTION_COUNTDOWN_BIDS; ?></td>
                    <td><?php echo tep_draw_input_field('countdown_bids', $auction_array['countdown_bids']); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo TEXT_INFO_AUCTION_COUNTDOWN_TIMER; ?></td>
                    <td><?php echo tep_draw_input_field('countdown_timer', $auction_array['countdown_timer'], 'id="auction_countdown_timer"'); ?></td>
                  </tr>
                </table></td>
              </tr>
*/
?>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_CAP_PRICE; ?></td>
                <td><?php echo tep_draw_input_field('cap_price', $auction_array['cap_price']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_START_PRICE; ?></td>
                <td><?php echo tep_draw_input_field('start_price', $auction_array['start_price']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_SHIPPING; ?></td>
                <td><?php echo tep_draw_input_field('shipping_cost', $auction_array['shipping_cost']); ?></td>
              </tr>
              <tr name='pause_tr'>
                <td><?php echo TEXT_INFO_AUCTION_START_PAUSE; ?></td>
                <td><?php echo tep_draw_input_field('start_pause', $auction_array['start_pause'], 'id="auction_start_pause"'); ?></td>
              </tr>
              <tr name='pause_tr'>
                <td><?php echo TEXT_INFO_AUCTION_END_PAUSE; ?></td>
                <td><?php echo tep_draw_input_field('end_pause', $auction_array['end_pause'], 'id="auction_end_pause"'); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_DATE_START; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_input_field('date_start', $auction_array['date_start'], 'id="auction_date_start"'); ?></td>
                    <td><?php echo tep_draw_input_field('date_start_time', $auction_array['date_start_time'], 'id="auction_date_start_time"'); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_DATE_EXPIRE; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_input_field('date_expire', $auction_array['date_expire'], 'id="auction_date_expire"'); ?></td>
                    <td><?php echo tep_draw_input_field('date_expire_time', $auction_array['date_expire_time'], 'id="auction_date_expire_time"'); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_AUTO_TIMER; ?></td>
                <td><?php echo tep_draw_input_field('auto_timer', $auction_array['auto_timer'], 'id="auction_auto_timer"'); ?>&nbsp;&nbsp;<?php echo tep_draw_input_field('live_off', $auction_array['live_off']); ?></td>
              </tr>
              <tr><td colspan="2">&nbsp;</td></tr>
              <tr>
                <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?php echo TEXT_INFO_AUCTION_DESCRIPTION; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo tep_draw_textarea_field('auctions_description', 'hard', 41, 5, $auction_array['auctions_description']); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_SORT_ORDER; ?></td>
                <td><?php echo tep_draw_input_field('sort_id', $auction_array['sort_id']); ?></td>
              </tr>
            </table></td>
          </tr>

          <tr>
            <td class="formAreaRow"><?php echo implode('', $buttons); ?></td>
          </tr>
        </table></form></td>
      </tr>

<?php
  } elseif( $action == 'select_products') {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE_SELECT_PRODUCTS; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form("select_products", $g_script, tep_get_all_get_params(array('action')) . 'action=insert_products_confirm'); ?><table width="100%" border="0" cellspacing="1" cellpadding="3">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th class="calign" width="40"><?php echo TABLE_HEADING_SELECT; ?></th>
                <th width="40"><?php echo TABLE_HEADING_ID; ?></th>
                <th><?php echo TABLE_HEADING_TITLE; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_INSTANCES; ?></th>
              </tr>
<?php
    $buttons = array(
      '<a href="' . tep_href_link(FILENAME_AUCTIONS, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>',
      tep_image_submit('button_insert.gif',IMAGE_INSERT),
    );

    $products_query_raw = "select pd.products_id, pd.products_name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id=pd.products_id and p.products_status='1' and p.products_display='1' and pd.language_id = '" . (int)$languages_id . "' order by products_name";
    $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);
    $products_array = array();
    tep_query_to_array($products_query_raw, $products_array);
    for( $i=0, $j=count($products_array); $i<$j; $i++) {
      $product = $products_array[$i];
      $instances_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where products_id = '" . (int)$product['products_id'] . "'");
      $instances_array = tep_db_fetch_array($instances_query);

      $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where products_id = '" . (int)$product['products_id'] . "'");
      $tiers_array = tep_db_fetch_array($tiers_query);
?>
              <tr class="dataTableRow">
                <td class="calign"><?php echo tep_draw_checkbox_field('pc_id['.$product['products_id'].']', 'on', false); ?></td>
                <td><?php echo $product['products_id']; ?></td>
                <td><?php echo $product['products_name']; ?></td>
                <td class="calign"><?php echo $instances_array['total'] . '/' . $tiers_array['total']; ?></td>
              </tr>
<?php
    }
?>
            </table></td>
          </tr>
          <tr>
            <td class="formAreaRow"><?php echo implode('', $buttons); ?></td>
          </tr>
        </table></form></td>
      </tr>
      <tr>
        <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
            <td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
          </tr>
        </table></td>
      </tr>

<?php
  } else {
    $buttons = array(
      '<a href="' . tep_href_link(FILENAME_AUCTIONS, tep_get_all_get_params(array('action')) . 'action=select_products') . '">' . tep_image_button('button_new_product.gif', IMAGE_INSERT) . '</a>',
      '<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) . 'action=new') . '">' . tep_image_button('button_new.gif', IMAGE_NEW) . '</a>',
    );
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td class="formArea" width="100%"><?php echo tep_draw_form("group_filter", $g_script, tep_get_all_get_params(array('action')), 'get'); ?><table border="0" cellspacing="1" cellpadding="3">
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_GROUP_FILTER . tep_draw_hidden_field('action', 'list'); ?></td>
            <td><?php echo tep_draw_pull_down_menu('agID', array_values($auction_groups_array), $agID, 'onchange="this.form.submit();"'); ?></td>
          </tr>
        </table></form></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th><?php echo TABLE_HEADING_ID; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_IMAGE; ?></th>
                <th><?php echo TABLE_HEADING_TITLE; ?></th>
                <th><?php echo TABLE_HEADING_PRODUCT; ?></th>
<?php
    if( !$agID ) {
?>
                <th><?php echo TABLE_HEADING_GROUP; ?></th>
<?php
    }
?>
                <th><?php echo TABLE_HEADING_TYPE; ?></th>
                <th><?php echo TABLE_HEADING_STATUS; ?></th>
                <th><?php echo TABLE_HEADING_TIERS; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_ACTION; ?></th>
              </tr>
<?php
    $generic_count = 0;
    $rows = 0;
    if( !$agID ) {
      $auctions_query_raw = "select auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_type, status_id from " . TABLE_AUCTIONS . " order by sort_id, auctions_name";
    } else {
      $auctions_query_raw = "select auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_type, status_id from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$agID . "' and status_id != 2 order by status_id desc, sort_id, auctions_name";
    }

    $auctions_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $auctions_query_raw, $auctions_query_numrows);
    $auctions_query = tep_db_query($auctions_query_raw);

    while( $auctions_array = tep_db_fetch_array($auctions_query) ) {

      if( ( empty($aID) && !isset($aInfo) ) || ( !empty($aID) && ($aID == $auctions_array['auctions_id'])) && (substr($action, 0, 3) != 'new')) {
        $aInfo = new objectInfo($auctions_array);
      }
      $generic_count++;
      $rows++;

      if (isset($aInfo) && is_object($aInfo) && ($auctions_array['auctions_id'] == $aInfo->auctions_id) ) {
        $class = 'dataTableRowSelected';
      } else {
        $class = 'dataTableRow';
      }

      if( !$auctions_array['auctions_group_id'] ) {
        $class = 'dataTableRowHigh';
      }
      echo '              <tr class="' . $class . '">' . "\n";
?>
                <td><?php echo $auctions_array['auctions_id']; ?></td>
                <td class="calign"><?php echo tep_image(DIR_WS_CATALOG_IMAGES . $auctions_array['auctions_image'], $auctions_array['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT); ?></td>
                <td><?php echo $auctions_array['auctions_name']; ?></td>
                <td><?php echo tep_get_products_name($auctions_array['products_id']); ?></td>
<?php
      if( !$agID ) {
?>
                <td><?php echo $auction_groups_array[$auctions_array['auctions_group_id']]['text']; ?></td>
<?php
      }
?>
                <td><?php echo $auction_types_array2[$auctions_array['auctions_type']]; ?></td>
                <td><?php echo $auction_status_array2[$auctions_array['status_id']]; ?></td>
                <td class="calign">
<?php
      $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . $auctions_array['auctions_id'] . "'");
      $tiers_array = tep_db_fetch_array($tiers_query);
      echo $tiers_array['total'];
?>
                </td>
                <td class="calign">
<?php 
      if (isset($aInfo) && is_object($aInfo) && ($auctions_array['auctions_id'] == $aInfo->auctions_id)) { 
        echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); 
      } else { 
        echo '<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) . 'aID=' . $auctions_array['auctions_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
      } 
?>
                &nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="9"><table border="0" width="100%" cellpadding="0"cellspacing="2">
                  <tr>
                    <td class="smallText"><?php echo $auctions_split->display_count($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                    <td class="smallText ralign"><?php echo $auctions_split->display_links($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="formAreaRow ralign" colspan="9"><?php echo implode('', $buttons); ?></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {
/*
      case 'new':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_AUCTION_NEW . '</b>');

        $contents = array('form' => tep_draw_form('new_group', $g_script, 'action=insert_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id));
        $contents[] = array('text' => TEXT_INFO_AUCTION_NEW_INTRO);
        $contents[] = array('text' => TEXT_INFO_AUCTION_NAME . '<br />' . tep_draw_input_field('auctions_name'));
        $contents[] = array('text' => TEXT_INFO_AUCTION_DESC . '<br />' . tep_draw_textarea_field('auctions_description', 'hard', 30, 5, ''));

        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT . '</b>');

        $contents = array('form' => tep_draw_form('edit_group', $g_script, 'action=update_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
        $contents[] = array('text' => TEXT_INFO_GROUP_NAME . '<br />' . tep_draw_input_field('auctions_name', $aInfo->auctions_name));
        $contents[] = array('text' => TEXT_INFO_GROUP_DESC . '<br />' . tep_draw_textarea_field('auctions_description', 'hard', 30, 5, $aInfo->auctions_description));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
*/
      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE . '</b>');

        $contents = array('form' => tep_draw_form('delete_auction', $g_script, 'action=delete_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id));
        $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
        $contents[] = array('text' => '<br><b>' . $aInfo->auctions_name . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      default:
        if( isset($aInfo) && is_object($aInfo) ) { // generic_text info box contents
          $extra_query = tep_db_query("select bid_step, max_bids, countdown_bids, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost from "  . TABLE_AUCTIONS . " where auctions_id = '" . $aInfo->auctions_id . "'");
          $extra_array = tep_db_fetch_array($extra_query);
          $heading[] = array('text' => '<b>' . $aInfo->auctions_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
          $contents[] = array('text' => '<br />');
          $contents[] = array('text' => TEXT_INFO_AUCTION_NAME . '<br /><b>' . $aInfo->auctions_name . '</b>');
          $contents[] = array('text' => TEXT_INFO_AUCTION_TYPE . '<br /><b>' . $auction_types_array[$aInfo->auctions_type]['text'] . '</b>');
          $product_name = tep_get_products_name($aInfo->products_id);
          if( !empty($products_name) ) {
            $contents[] = array('text' => TEXT_INFO_PRODUCT_NAME . '<br /><b>' . $product_name . '</b>');
          }
          if( $aInfo->status_id == 1 ) {
            $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . $aInfo->auctions_id . "'");
            $check_array = tep_db_fetch_array($check_query);
            $contents[] = array('text' => TEXT_INFO_AUCTION_BIDS_ENTERED . '&nbsp;<b>' . $check_array['total'] . '</b>');
            $contents[] = array('text' => TEXT_INFO_AUCTION_BIDS_VALUE . '&nbsp;<b>' . $currencies->format($check_array['total']*($extra_array['bid_step']/100)) . '</b>');
          } elseif( $aInfo->status_id == 2 ) {
            $check_query = tep_db_query("select bid_count from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . $aInfo->auctions_id . "'");
            $check_array = tep_db_fetch_array($check_query);
            $contents[] = array('text' => TEXT_INFO_AUCTION_BIDS_ENTERED . '&nbsp;<b>' . $check_array['bid_count'] . '</b>');
            $contents[] = array('text' => TEXT_INFO_AUCTION_BIDS_VALUE . '&nbsp;<b>' . $currencies->format($check_array['bid_count']*($extra_array['bid_step']/100)) . '</b>');
          }

          //$contents[] = array('text' => $aInfo->auctions_description);
          //$contents[] = array('text' => $aInfo->auctions_description);
          //$contents[] = array('text' => $aInfo->auctions_description);

        } else { // create generic_text dummy info
          $heading[] = array('text' => '<b>' . TEXT_INFO_NO_AUCTIONS . '</b>');
          $contents[] = array('text' => TEXT_INFO_ZERO_AUCTIONS);
        }
        break;

    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }

?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<div><script language="javascript" type="text/javascript">

  $('#auction_countdown_timer0, #auction_countdown_timer1, #auction_countdown_timer2, #auction_countdown_timer3, #auction_countdown_timer4').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true,
    defaultTime: '00:00:00',
    rows: 6,
    showSeconds: true,
    minutes: {interval: 1, ends: 59},
    seconds: {interval: 1, ends: 59}
  });

  $('#auction_start_pause').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });

  $('#auction_end_pause').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });

  $('#auction_auto_timer').timepicker({
    showDays: true,
    showHours: false,
    showMinutes: false,
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: false
  });


  $('#auction_date_start').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $('#auction_date_start_time').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });

  $('#auction_date_expire').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $('#auction_date_expire_time').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });

//  var jqWrap = general;
//  jqWrap.launch();

</script></div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
