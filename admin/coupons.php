<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Coupons Management script
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('LOCAL_SPLIT_RESULTS', 100);
  require('includes/application_top.php');
 //include('includes/form_check.js.php');
 require('includes/ajax.js.php');
 require('includes/ajax.php');
  require_once(DIR_WS_CLASSES . 'coupons.php');
  $cCoupons = new coupons;

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  $cID = isset($_GET['cID'])?(int)$_GET['cID']:0;

  if (isset($_POST['reset_x']) || isset($_POST['reset_y'])) $action='reset';
  if (isset($_POST['delete_x']) || isset($_POST['delete_y'])) $action='delete';

  // +Country-State Selector
// Adapted from functions in catalog/includes/general.php and html_output.php for Country-State Selector
// Returns an array with countries
// TABLES: countries
  function css_get_countries($countries_id = '', $with_iso_codes = false) {
    $countries_array = array();
    if (tep_not_null($countries_id)) {
      if ($with_iso_codes == true) {
        $countries = tep_db_query("select countries_name, countries_iso_code_2, countries_iso_code_3 from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$countries_id . "' order by countries_name");
        $countries_values = tep_db_fetch_array($countries);
        $countries_array = array('countries_name' => $countries_values['countries_name'],
                                 'countries_iso_code_2' => $countries_values['countries_iso_code_2'],
                                 'countries_iso_code_3' => $countries_values['countries_iso_code_3']);
      } else {
        $countries = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$countries_id . "'");
        $countries_values = tep_db_fetch_array($countries);
        $countries_array = array('countries_name' => $countries_values['countries_name']);
      }
    } else {
      $countries = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " order by countries_name");
      while ($countries_values = tep_db_fetch_array($countries)) {
        $countries_array[] = array('countries_id' => $countries_values['countries_id'],
                                   'countries_name' => $countries_values['countries_name']);
      }
    }

    return $countries_array;
  }

////
// Creates a pull-down list of countries
  function css_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array();

    $countries = css_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
    }

    return tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }
 // -Country-State Selector 
  
  switch ($action) {
    case 'setflag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_COUPONS, $sql_data_array, 'update', 'coupons_id=' . (int)$cID);
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'cID', 'flag')) . 'cID=' . $cID ));
      break;

    case 'insert':
      if( !isset($_POST['name']) || empty($_POST['name']) || !isset($_POST['amount']) || empty($_POST['amount']) ) {
        $messageStack->add_session(ERROR_INVALID_PARAMETERS_SPECIFIED, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'flag')) ));
      }

      $name = tep_db_prepare_input($_POST['name']);
      $check_query = tep_db_query("select count(*) as total from " . TABLE_COUPONS . " where coupons_name = '" . tep_db_input($name) . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( $check_array['total'] ) {
        $messageStack->add_session(ERROR_COUPON_EXISTS, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'flag')) ));
      }

      $sql_data_array = array(
        'coupons_name' => $name,
        'coupons_type' => (isset($_POST['type']) && $cCoupons->is_valid_type($_POST['type']) )?(int)$_POST['type']:1,
        'coupons_usage' => isset($_POST['usage'])?(int)$_POST['usage']:0,
        'coupons_max_usage' => isset($_POST['max_usage'])?(int)$_POST['max_usage']:0,
        'coupons_amount' => isset($_POST['amount'])?tep_round($_POST['amount'],2):0,
        'date_start' => isset($_POST['start'])?tep_db_prepare_input($_POST['start']):'null',
        'date_end' => isset($_POST['end'])?tep_db_prepare_input($_POST['end']):'null',
      );

      $sql_data_array['date_start'] .= ' ' . tep_db_prepare_input($_POST['start_time']);
      $sql_data_array['date_end'] .= ' ' . tep_db_prepare_input($_POST['end_time']);

      tep_db_perform(TABLE_COUPONS, $sql_data_array, 'insert');
      $messageStack->add_session(SUCCESS_NEW_COUPON, 'success');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'cID')) . 'cID=' . $cID ));
      break;

    case 'update':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      }

      foreach ($_POST['mark'] as $key => $val) {
        $sql_data_array = array(
          'coupons_name' => tep_db_prepare_input($_POST['name'][$key]),
          'coupons_type' => ($cCoupons->is_valid_type($_POST['type'][$key]))?(int)$_POST['type'][$key]:1,
          'coupons_usage' => (int)$_POST['usage'][$key],
          'coupons_max_usage' => (int)$_POST['max_usage'][$key],
          'coupons_amount' => tep_round($_POST['amount'][$key],2),
          'date_start' => tep_db_prepare_input($_POST['start'][$key]),
          'date_end' => tep_db_prepare_input($_POST['end'][$key]),
        );


        $sql_data_array['date_start'] .= ' ' . tep_db_prepare_input($_POST['start_time'][$key]);
        $sql_data_array['date_end'] .= ' ' . tep_db_prepare_input($_POST['end_time'][$key]);

        tep_db_perform(TABLE_COUPONS, $sql_data_array, 'update', "coupons_id= '" . (int)$key . "'");
        $messageStack->add_session(SUCCESS_COUPONS_UPDATED, 'success');
      }
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      break;

    case 'edit':
      $check_query = tep_db_query("select count(*) as total from " . TABLE_COUPONS . " where coupons_id = '" . (int)$cID . "'");
      $check_array = tep_db_fetch_array($check_query);

      if( !$check_array['total'] ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'cID')) ));
      }
      break;
    case 'update_confirm':
      $cID = isset($_POST['coupons_id'])?(int)$_POST['coupons_id']:0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_COUPONS . " where coupons_id = '" . (int)$cID . "'");
      $check_array = tep_db_fetch_array($check_query);

      if( !$check_array['total'] ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'cID')) ));
      }

      $sql_data_array = array(
        'coupons_message' => tep_db_prepare_input($_POST['message'])
      );
      tep_db_perform(TABLE_COUPONS, $sql_data_array, 'update', "coupons_id= '" . (int)$cID . "'");
      $messageStack->add_session(SUCCESS_COUPONS_UPDATED, 'success');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      break;

    case 'delete_confirm':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
      }
      foreach( $_POST['mark'] as $key => $val ) {
        tep_db_query("delete from " . TABLE_COUPONS . " where coupons_id= '" . (int)$key . "'");
        tep_db_query("delete from " . TABLE_COUPONS_REDEEM . " where coupons_id= '" . (int)$key . "'");
      }
      break;
    case 'delete':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || empty($_POST['mark']) ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
        tep_redirect(tep_href_link($g_script));
      }
      break;
    case 'reset':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) ) {
        $messageStack->add_session(ERROR_NOTHING_SELECTED, 'error');
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      }

      foreach( $_POST['mark'] as $key => $val ) {
        tep_db_query("update " . TABLE_COUPONS . " set last_customer_id=0 where coupons_id = '" . (int)$key . "'");
      }
      $messageStack->add_session(SUCCESS_COUPONS_RESET, 'success');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action')) ));
      break;
    default:
      break;
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/timepick/jquery-ui-timepicker.css" />
<script type="text/javascript" src="includes/javascript/jquery/jquery.js"></script>
<script type="text/javascript" src="includes/javascript/jquery/jquery.base64.js"></script>
<script type="text/javascript" src="includes/javascript/jquery/jquery.unserialize.js"></script>
<script type="text/javascript" src="includes/javascript/jquery/jquery.dump.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/ui/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/timepick/jquery.ui.timepicker.js"></script>
<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />
<script language="javascript" type="text/javascript" src="includes/javascript/general.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/coupon_emails.js"></script>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
  
  function refresh_form1() {
   //form_name.refresh.value = 'true';
   //form_name.submit();
   return true;
   }
</script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
<?php
  if( $action == 'delete') {
?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_DELETE_TITLE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_DELETE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('delete', $g_script, tep_get_all_get_params(array('action')) . 'action=delete_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
              </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $fields_query = tep_db_query("select coupons_id, coupons_name from " . TABLE_COUPONS . " where coupons_id = '" . (int)$key . "'");
      if( tep_db_num_rows($fields_query) ) { 
        $fields_array = tep_db_fetch_array($fields_query);
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
                <td class="dataTableContent"><?php echo '<b>' . $fields_array['coupons_name'] . '</b>' . tep_draw_hidden_field('mark[' . $fields_array['coupons_id'] . ']', $fields_array['coupons_id']); ?></td>
              </tr>
<?php
      }
    }
?>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) ) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>

<?php
  } else {
?>
        <td width="75%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php 
    echo HEADING_TITLE;
?>
                </td>
              </tr>
            </table></td>
          </tr>
<?php
    $coupons_types_array = tep_array_to_pull_down($cCoupons->types);
    $coupons_query_raw = "select * from " . TABLE_COUPONS . " order by coupons_name";
    $coupons_split = new splitPageResults($_GET['page'], LOCAL_SPLIT_RESULTS, $coupons_query_raw, $coupons_query_numrows);
    $total_items = array();
    tep_query_to_array($coupons_query_raw, $total_items, 'coupons_id');
    if( count($total_items) ) {
?>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
          </tr>

          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $coupons_split->display_count($coupons_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText" align="right"><?php echo $coupons_split->display_links($coupons_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>

          <tr>
            <td class="formArea"><?php echo tep_draw_form('coupons_form', $g_script, 'action=update&cID=' . $cID, 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent calign"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.coupons_form,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_AMOUNT; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TYPE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_USAGE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_MAX_USAGE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_DATE_START; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TIME_START; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_DATE_END; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TIME_END; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php

      foreach($total_items as $key => $coupon) {

        if( (!$cID || ($cID > 0 && ($cID == $coupon['coupons_id']))) && !isset($cInfo) ) {
          $cInfo = new objectInfo($coupon);
          $cID = $cInfo->coupons_id;
        }

        if (isset($cInfo) && is_object($cInfo) && ($coupon['coupons_id'] == $cInfo->coupons_id)) {
          echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
        } else {
          echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
        }
        $start_array = explode(' ', $coupon['date_start']);
        $end_array = explode(' ', $coupon['date_end']);
?>
                <td class="calign"><?php echo tep_draw_checkbox_field('mark['.$coupon['coupons_id'].']', 1) ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$coupon['coupons_id'].']', $coupon['coupons_name']); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('amount['.$coupon['coupons_id'].']', $coupon['coupons_amount'], 'size="8" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_pull_down_menu('type['.$coupon['coupons_id'].']', $coupons_types_array, $coupon['coupons_type']); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('usage['.$coupon['coupons_id'].']', $coupon['coupons_usage'], 'size="3" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('max_usage['.$coupon['coupons_id'].']', $coupon['coupons_max_usage'], 'size="3" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('start['.$coupon['coupons_id'].']', $start_array[0], 'class="date_start calign" size="10"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('start_time['.$coupon['coupons_id'].']', $start_array[1], 'class="time_start calign" size="8"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('end['.$coupon['coupons_id'].']', $end_array[0], 'class="date_end calign" size="10"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('end_time['.$coupon['coupons_id'].']', $end_array[1], 'class="time_end calign" size="8"'); ?></td>
                <td class="dataTableContent calign">
<?php
        if( $coupon['status_id'] == 1 ) {
          echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link($g_script, 'action=setflag&flag=0&cID=' . $coupon['coupons_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
        } else {
          echo '<a href="' . tep_href_link($g_script, 'action=setflag&flag=1&cID=' . $coupon['coupons_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
        }
?>
                </td>
                <td class="dataTableContent" align="center">
<?php 
        if (isset($cInfo) && is_object($cInfo) && ($coupon['coupons_id'] == $cInfo->coupons_id)) { 
          echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); 
        } else { 
          echo '<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('action', 'cID') ) . 'cID=' . $coupon['coupons_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
        } 
?>
                </td>
              </tr>
<?php
      } 
?>
              <tr>
                <td colspan="12"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="12"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE, 'name="update"') . ' ' . tep_image_submit('button_delete.gif', IMAGE_DELETE, 'name="delete"') . ' ' . tep_image_submit('button_reset.gif', IMAGE_RESET, 'name="reset"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $coupons_split->display_count($coupons_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText" align="right"><?php echo $coupons_split->display_links($coupons_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
<?php
    }
?>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php 
    echo HEADING_INSERT_TITLE; 
?>
                </td>
              </tr>
            </table></td>
          </tr>

          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("add_field", $g_script, 'action=insert', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_AMOUNT; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TYPE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_USAGE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_MAX_USAGE; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_DATE_START; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TIME_START; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_DATE_END; ?></td>
                <td class="dataTableHeadingContent calign"><?php echo TABLE_HEADING_TIME_END; ?></td>
              </tr>
              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('amount', '', 'size="8" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_pull_down_menu('type', $coupons_types_array); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('usage', '', 'size="3" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('max_usage', '', 'size="3" class="calign"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('start', '', 'class="date_start calign" size="10"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('start_time', '', 'class="time_start calign" size="8"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('end', '', 'class="date_end calign" size="10"'); ?></td>
                <td class="dataTableContent calign"><?php echo tep_draw_input_field('end_time', '', 'class="time_end calign" size="8"'); ?></td>
              </tr>
              <tr>
                <td colspan="9"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="9">
                <td class="dataTableHeadingContent"><?php echo tep_image_submit('button_insert.gif', IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>
<?php
    $heading = array();
    $contents = array();

    switch($action) {
      case 'edit':
        $heading[] = array('text' => '<b>' . sprintf(TEXT_INFO_HEADING_EDIT, $cInfo->coupons_name) . '</b>');

        $contents = array('form' => tep_draw_form('edit', $g_script, 'action=update_confirm') . tep_draw_hidden_field('coupons_id', $cInfo->coupons_id));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
        $contents[] = array('text' => TEXT_INFO_COUPONS_DESC . '<br />' . tep_draw_textarea_field('message', 'hard', 30, 5, $cInfo->coupons_message));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link($g_script, 'cID=' . $cInfo->coupons_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      default:
        if (isset($cInfo) && is_object($cInfo)) {
        $mail_query = tep_db_query("select customers_id, customers_email_address, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " order by customers_lastname");
    while($customers_values = tep_db_fetch_array($mail_query)) {
      $customers[] = array('id' => $customers_values['customers_id'],
                           'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
    }
    $customer_dropdown = tep_draw_pull_down_menu('customers_email_address', $customers, (isset($_GET['customer']) ? $_GET['customer'] : ''), 'id="customers_email_address"');            
          
      $country_dropdown = css_get_country_list('entry_country_id',  $cInfo->entry_country_id,'onChange="return refresh_form1();"');    
       // +Country-State Selector
				//$country_dropdown = ajax_get_zones_html($entry['entry_country_id'],($entry['entry_zone_id']==0 ? $entry['entry_state'] : $entry['entry_zone_id']),false);
				// -Country-State Selector   
          
          $heading[] = array('text' => '<b>' . $cInfo->coupons_name . '</b>');
          $check_query = tep_db_query("select count(*) as total from " . TABLE_COUPONS_REDEEM . " where coupons_id = '" . (int)$cInfo->coupons_id . "'");
          $check_array = tep_db_fetch_array($check_query);
          $contents[] = array('text' => '<br>' . sprintf(TEXT_INFO_COUPONS_USED, '<b>' . $check_array['total'] . '</b>') );
          //var_dump($cInfo);
          $contents[] = array(
            'align' => 'center', 
            'text' => $customer_dropdown . '<a id="sms_prompt" href="' . tep_href_link($g_script, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->coupons_id . '&action=sms') . '">' . tep_image_button('button_send.gif', IMAGE_SEND) . '</a>&nbsp;<a id="email_coupon" href="' . tep_href_link('mail.php', tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->coupons_id . '') . '">' . tep_image_button('button_send_mail.gif', 'Send Email') . '</a>&nbsp;<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->coupons_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a>'
          );
        }
        break;
    }
    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
      </tr>
    </table></td>
<?php
  }
?>
      </tr>
    </table></td>

<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<div><script language="javascript" type="text/javascript">
  $('.date_start').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });
  $('.date_end').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd'
  });

  $('.time_start, .time_end').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true,
    defaultTime: '00:00:00',
    rows: 6,
    showSeconds: true,
    minutes: {interval: 1, ends: 59},
    seconds: {interval: 1, ends: 59}
  });

  $('.time_start').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });

  $('.time_end').timepicker({
    showPeriod: false,
    showPeriodLabels: false,
    showLeadingZero: true
  });
</script></div>
<?php 
  if (isset($cInfo) && is_object($cInfo)) {
?>
<div><script language="javascript" type="text/javascript">
  var jqWrap = coupon_emails;
  jqWrap.baseURL = "<?php echo tep_href_link(FILENAME_AJAX_CONTROL, 'module=coupons_to_customers&strings=coupons.php&cID=' . $cInfo->coupons_id); ?>";
  jqWrap.launch();
</script></div>
<?php
  }
?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
