<?php
/*
  $Id: mail.php,v 1.8 2002/01/18 17:28:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Send Email To Customers');

define('TEXT_CUSTOMER', 'Customer:');
define('TEXT_CUSTOMER_LIST', 'Customer List:');
define('TEXT_CUSTOMER_BLANK', 'Other:');
define('TEXT_SUBJECT', 'Subject:');
define('TEXT_FROM', 'From:');
define('TEXT_MESSAGE', 'Message:');
define('TEXT_SELECT_CUSTOMER', 'Select Customer');
define('TEXT_ALL_CUSTOMERS', 'All Customers');
define('TEXT_NEWSLETTER_CUSTOMERS', 'To All Newsletter Subscribers');
define('TEXT_ATTACH_FILE', 'Attach Files:');
define('TEXT_ATTACHMENTS', 'Attachments:');

define('NOTICE_EMAIL_SENT_TO', 'Email sent to: %s');
define('ERROR_NO_CUSTOMER_SELECTED', 'No customer has been selected');
define('ERROR_NO_SUBJECT', 'Subject Line is empty');
define('ERROR_FILE_UPLOAD', 'Could not upload file %s');
?>
