<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// SEO_G Exclusion scripts
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'SEO-G Exclude');
define('HEADING_TITLE2', 'Insert Script Entries');

define('TABLE_HEADING_FILENAME', 'Filename');
define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_PARAMETERS', 'Parameters');
define('TEXT_INSERT', 'Insert script entry into the exclusion list');
define('TEXT_UPDATE', 'Update selected script entries');
define('TEXT_DELETE', 'Delete selected script entries');

define('TEXT_DISPLAY_NUMBER_OF_SCRIPTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> scripts)');

define('TEXT_INFO_DELETE', 'Are you sure you want to remove these script entries? <br>Note: No files are removed only the exclusion database table for the SEO-G is updated');
define('TEXT_INFO_MAIN', 'Select the script entries to update or delete from the following list.');
define('TEXT_INFO_MAIN2', 'This operation simply inserts script entries to the database. These entries are used by the SEO-G on the catalog end to signal whether URLs for the script should be processed or not. No files are modified. Included entries are not processed by SEO-G');
?>
