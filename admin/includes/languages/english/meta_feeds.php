<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Feeds for the Admin end
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
//define('TITLE', 'META-G Feeds Generator');
define('HEADING_TITLE', 'META-G Feeds Generator');
define('TEXT_CREATE_PRODUCTS', 'Click Start to begin generating the base feed. <br>This scipt will parse the products tables in the database and will associate them with the stored SEO-G urls. Make sure the products SEO-G urls are present. If in doubt, use the all products page first from the catalog end.<br />Click cancel at anytime to abort this process.<br />Existing base feed map file will be replaced.<br /><b>Default step is 10 seconds for 1000 products.</b>');
define('TEXT_GENERATE', 'Commence Feed Generation')
?>
