<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Types for the Abstract Zones component for osCommerce Admin
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

define('HEADING_TITLE', 'Service Specifics');
define('HEADING_ABSTRACT_TYPES_ADD', 'Add a New Abstract Type');
define('HEADING_ABSTRACT_TYPES_UPDATE', 'Update Abstract Types');

define('TABLE_HEADING_ABSTRACT_NAME', 'Name');
define('TABLE_HEADING_ABSTRACT_CLASS', 'Script');
define('TABLE_HEADING_ABSTRACT_TABLE', 'Table');
define('TABLE_HEADING_SORT_ORDER', 'Sort Order');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_SELECT', 'Select');

define('IMAGE_ADD_FIELD', 'Add new field');
define('IMAGE_UPDATE_FIELDS', 'Update fields');
define('IMAGE_REMOVE_FIELDS', 'Remove selected fields');