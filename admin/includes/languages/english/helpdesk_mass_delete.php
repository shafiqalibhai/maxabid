<?php
/*
  $Id: helpdesk.php,v 1.5 2005/08/16 20:56:39 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'HelpDesk Mass Delete');

define('TABLE_HEADING_TICKET', 'Ticket');
define('TABLE_HEADING_SUBJECT', 'Subject');
define('TABLE_HEADING_SENDER', 'Sender');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_PRIORITY', 'Priority');
define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_EMAIL', 'E-mail');
define('TABLE_HEADING_DATE', 'Date');

define('TEXT_TICKET_NUMBER', 'Ticket #:');
define('TEXT_FROM_NAME', 'From (Name):');
define('TEXT_FROM_EMAIL_ADDRESS', 'From (E-Mail Address):');
define('TEXT_TO_NAME', 'To (Name):');
define('TEXT_TO_EMAIL_ADDRESS', 'To (E-Mail Address):');
define('TEXT_SUBJECT', 'Subject:');
define('TEXT_BODY', 'Body:');
define('TEXT_TO', 'To:');
define('TEXT_FROM', 'From:');
define('TEXT_DATE', 'Date:');

define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this entry?');
define('TEXT_DELETE_WHOLE_THREAD', 'Remove whole thread');

define('TEXT_SEND_INTRO', 'Send the E-Mail written below?');

define('TEXT_UPDATE_INTRO', 'Update the E-Mail written below?');

define('TEXT_ALL_STATUSES', 'All Statuses');
define('TEXT_ALL_PRIORITIES', 'All Priorities');
define('TEXT_ALL_DEPARTMENTS', 'All Departments');
define('TEXT_ALL_ENTRIES', 'All Entries');
define('TEXT_ONLY_NEW_ENTRIES', 'Only New Entries');

define('TEXT_STATUS', 'Status:');
define('TEXT_PRIORITY', 'Priority:');
define('TEXT_DEPARTMENT', 'Department:');
define('TEXT_ENTRIES', 'Entries:');


define('SUCCESS_ENTRY_UPDATED', 'Success: Entry successfully updated.');
define('SUCCESS_REPLY_PROCESSED', 'Success: Reply successfully processed.');
define('SUCCESS_WHOLE_THREAD_REMOVED', 'Success: Whole thread successfully removed.');
define('SUCCESS_ENTRY_REMOVED', 'Success: Entry successfully removed.');
define('SUCCESS_TICKET_UPDATED', 'Success: Ticket has been successfully updated.');
define('SUCCESS_COMMENT_UPDATED', 'Success: Comment has been successfully updated.');

define('ERROR_TICKET_DOES_NOT_EXIST', 'Error: Ticket does not exist.');
?>
