<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2012 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Coupons Manager strings
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Coupons Manager');
define('HEADING_INSERT_TITLE', 'Insert New Coupon');
define('HEADING_DELETE_TITLE', 'Delete Coupons');

define('TABLE_HEADING_NAME', 'Coupon Name');
define('TABLE_HEADING_CUSTOMERS_NAME', 'Customer');
define('TABLE_HEADING_CUSTOMERS_EMAIL', 'E-Mail');
define('TABLE_HEADING_CUSTOMERS_SMS', 'SMS');
define('TABLE_HEADING_AMOUNT', 'Amount');
define('TABLE_HEADING_TYPE', 'Type');
define('TABLE_HEADING_USAGE', 'Uses/Client');
define('TABLE_HEADING_MAX_USAGE', 'Max Uses');
define('TABLE_HEADING_DATE_START', 'Date Start');
define('TABLE_HEADING_DATE_END', 'Date End');
define('TABLE_HEADING_TIME_START', 'Time Start');
define('TABLE_HEADING_TIME_END', 'Time End');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_HEADING_EDIT', 'Coupon %s');
define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_COUPONS_DESC', 'Coupon Message');

define('TEXT_INFO_INSERT', 'Enter the coupon name and coupon parameters and hit insert to create a new coupon. The coupon name must be unique');
define('TEXT_INFO_DELETE', 'Are you sure you want to delete these coupons?');
define('TEXT_INFO_UPDATE', 'Tick the checkboxes on the left and change the content of the coupons. Then hit update to modify the selected coupons');

define('TEXT_INFO_COUPONS_USED', 'Coupon used %s times');
define('TEXT_INFO_COUPON_SMS_TO_CUSTOMERS', 'SMS to customers with Coupon: [%s]');

define('BUTTON_SEND_SELECTED', 'Send Selected');
define('TEXT_INFO_BUTTON_SEND_SELECTED', 'Send SMS to selected customers');
define('BUTTON_SEND_ALL', 'Send To All');
define('TEXT_INFO_BUTTON_SEND_ALL', 'Send SMS to all customers with a cellphone');
define('BUTTON_SEND_RESUME', 'Resume Sending');
define('TEXT_INFO_BUTTON_SEND_RESUME', 'Resume SMS sending to customers');

define('BUTTON_BACK', 'Back');
define('TEXT_INFO_BUTTON_BACK_LISTING', 'Back to the customers list');
define('TEXT_INFO_AVAILABLE_CREDITS', 'Available Credits %s');

define('ERROR_INVALID_PARAMETERS_SPECIFIED', 'Invalid Coupon parameters specified. Name must be unique and amount valid');
define('ERROR_COUPON_EXISTS', 'Coupon already exists. Coupon Names must be unique');
define('ERROR_TITLE_INVALID_COUPON', 'Invalid Coupon');
define('ERROR_INVALID_COUPON', 'The coupon you specified is invalid. Make sure you are using a valid coupon for this function');
define('ERROR_NOTHING_SELECTED', 'Nothing was selected. Tick the checkboxes on the left to select entries');
define('ERROR_TITLE_NOTHING_SELECTED', 'Nothing was selected');
define('SUCCESS_NEW_COUPON', 'New Coupon Created');
define('SUCCESS_COUPONS_UPDATED', 'Selected Coupons Updated');
define('SUCCESS_COUPONS_RESET', 'Selected Coupons Reset for customers sending SMS');
