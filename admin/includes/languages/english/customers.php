<?php
/*
  $Id: customers.php,v 1.12 2002/01/12 18:46:27 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Customers');
define('HEADING_TITLE_SEARCH', 'Search:');

define('TABLE_HEADING_ID', 'CID');
define('TABLE_HEADING_FIRSTNAME', 'First Name');
define('TABLE_HEADING_LASTNAME', 'Last Name');
define('TABLE_HEADING_ACCOUNT_CREATED', 'Account Created');
define('TABLE_HEADING_ACCOUNT_LAST_LOGON', 'Last Login');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_DATE_ACCOUNT_CREATED', 'Account Created:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_DATE_LAST_LOGON', 'Last Logon:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Number of Logons:');
define('TEXT_INFO_COUNTRY', 'Country:');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Number of Reviews:');
define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this customer?');
define('TEXT_DELETE_REVIEWS', 'Delete %s review(s)');
define('TEXT_INFO_HEADING_DELETE_CUSTOMER', 'Delete Customer');
define('TYPE_BELOW', 'Type below');
define('PLEASE_SELECT', 'Select One');

//-MS- Active Countries support
define('HEADING_ADDRESS_BOOK', 'Address Book Entries &raquo; %s');
define('CATEGORY_PASSWORD', 'Account Password <span class="smallText">(for existing accounts leave fields blank to ignore)</span>');
define('ENTRY_PASSWORD', 'Password:');
define('ENTRY_PASSWORD_CONFIRM', 'Password Confirm:');
define('ENTRY_ACCOUNT_NOTIFY', 'Notify Customer:');
define('ENTRY_ACCOUNT_POINTS', 'Bids Purchased:');
define('EMAIL_SUBJECT1', 'Welcome to ' . STORE_NAME);
define('EMAIL_SUBJECT2', 'Account changes from ' . STORE_NAME . ' Administrator');
define('EMAIL_GREET_NONE', 'Dear %s' . "\n\n" . 'Please make a note of the following account details:' . "\n\n");
define('EMAIL_WELCOME', 'We welcome you to <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'You can now take part in the <b>various services</b> we have to offer you. Some of these services include:' . "\n\n" . '<li><b>Permanent Cart</b> - Any products added to your online cart remain there until you remove them, or check them out.' . "\n" . '<li><b>Address Book</b> - We can now deliver your products to another address other than yours! This is perfect to send birthday gifts direct to the birthday-person themselves.' . "\n" . '<li><b>Order History</b> - View your history of purchases that you have made with us.' . "\n" . '<li><b>Products Reviews</b> - Share your opinions on products with our other customers.' . "\n\n");
define('EMAIL_CONTACT', 'For help with any of our online services, please email the store-owner: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '<b>Note:</b> This email address was given to us by one of our customers. If you did not signup to be a member, please send an email to ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
define('TEXT_INFO_NUMBER_OF_ENTRIES', 'Address Book Entries');
define('ENTRY_DEFAULT_ADDRESS', 'Default Address Book Entry');
define('TEXT_INFO_ADDRESS_BOOK_DELETE', 'Do you want to delete this address book entry?');
define('TABLE_CELL_ADDRESS_BOOK_DETAILS', 'Details:');
define('HEADING_ADDRESS_BOOK_DELETE', 'Address Book Removal for %s');
define('ENTRY_PASSWORD_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_PASSWORD_MIN_LENGTH . ' chars</span>');
define('ENTRY_PASSWORD_CONFIRM_ERROR', '&nbsp;<span class="errorText">min ' . ENTRY_PASSWORD_MIN_LENGTH . ' chars</span>');
//-MS- Active Countries support EOM
?>
