<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Group Fields page language strings
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

define('HEADING_TITLE', 'New Group of Fields');
define('HEADING_TITLE_OPTION', 'New Option for: %s');
define('HEADING_TITLE_VALUE', 'New Value for:  %s&nbsp;&raquo;&nbsp;%s');
define('HEADING_GROUP_UPDATE', 'Update Group Fields');
define('HEADING_GROUP_OPTIONS_UPDATE', 'Options for Group: %s');
define('HEADING_GROUP_VALUES_UPDATE', 'Values for: %s&nbsp;&raquo;&nbsp;%s');
define('HEADING_GROUP_DELETE', 'Entire Group Removal Confirmation');
define('HEADING_GROUP_OPTIONS_DELETE', 'Entire Option Removal Confirmation');
define('HEADING_GROUP_VALUES_DELETE', 'Values Removal Confirmation');

define('TABLE_HEADING_NEW_GROUP', 'New Group');
define('TABLE_HEADING_NAME', 'Name');
define('TABLE_HEADING_DESCRIPTION', 'Description');
define('TABLE_HEADING_TYPE', 'HTML Type for values');
define('TABLE_HEADING_LAYOUT', 'Layout');
define('TABLE_HEADING_LIMITER', 'Limiter');
define('TABLE_HEADING_STOCK', 'Stock');
define('TABLE_HEADING_PRICE', 'Price');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_ORDER', 'Order');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_INSERT', 
       'Enter a new Group with a name, description and sort order. Enabled Groups can chosen when editing products. ' . 
       'The <b>name</b> box must be populated and defines the Group name/title that appears on the catalog end. The <b>sort order</b> defines how the Groups are ordered when editing products and when viewing the catalog pages.'
       );

define('TEXT_INFO_INSERT_OPTION', 
       'Enter a new Option for the current Group with a name and sort order. ' . 
       'Tick the associated boxes to control the visibility of fields. Select the HTML Values type via the drop-down list. ' . 
       'The <b>sort order</b> defines how the Group Options are ordered when viewing the catalog pages.'
       );

define('TEXT_INFO_INSERT_VALUE', 
       'Enter a new Value for the current Group-Option with a name and sort order. Image, Price and Stock visibility are subject to the Options settings. ' . 
       'The <b>sort order</b> defines how the Values are ordered when viewing the catalog pages.'
       );

define('TEXT_INFO_UPDATE', 'Select the Group Fields to update. Use the tick boxes on the left to mark the items for update. Status lamps are operating separately. Use the information icons to select a Group then click the Details button on the right to switch to the Options Menu for the selected Group.');
define('TEXT_INFO_UPDATE_OPTION', 'Select the Group Options to update. Use the tick boxes on the left to mark the items for update. Status lamps are operating separately. . Use the information icons to select a Group-Option, then click the Details button on the right to switch to the Values Menu for the selected Option.');
define('TEXT_INFO_UPDATE_VALUE', 'Select the Group Values to update. Use the tick boxes on the left to mark the items for update. Status lamps are operating separately. Shown columns are subject to the filtered options, for price, image and stock columns');
define('TEXT_INFO_DELETE_OPTION', 'The following options will be deleted:');
define('TEXT_INFO_DELETE_VALUE', 'The following values will be deleted:');
define('TEXT_INFO_NUMBER_OPTIONS', 'Defined Options:');
define('TEXT_INFO_NUMBER_VALUES', 'Defined Values:');
define('TEXT_INFO_IMAGE_NOT_SET', '<em><b>Image Not Set</b></em>');