<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Generic Text script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Generic Entries');
define('HEADING_TITLE_SEARCH', 'Search:');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_GENERIC', 'Generic Text');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_EDIT', 'Edit');

define('TEXT_NEW_GENERIC', 'New Entry');
define('TEXT_GENERIC', 'Generic Text:');

define('TEXT_NO_GENERIC', 'Please insert a new entry.');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_EDIT', 'Edit');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE_GENERIC', 'Delete Entry');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');

define('TEXT_DELETE_GENERIC_INTRO', 'Are you sure you want to permanently delete this entry?');

define('TEXT_GENERIC_STATUS', 'Status:');
define('TEXT_GENERIC_NAME', 'Title:');
define('TEXT_GENERIC_DESCRIPTION', 'Description:');

define('TEXT_COPY_AS_DUPLICATE', 'Duplicate entry');
define('TEXT_GENERIC_AVAILABLE', 'Enabled');
define('TEXT_GENERIC_NOT_AVAILABLE', 'Disabled');
define('EMPTY_GENERIC', 'No Entries');

define('IMAGE_NEW_GENERIC_TEXT', 'Create a new generic text entry');

//-MS- SEO-G Added
define('TEXT_SEO_NAME', 'SEO-G Name:');
define('TEXT_METAG', 'META-G Tags');
define('TEXT_META_TITLE', 'META Title:');
define('TEXT_META_KEYWORDS', 'META Keywords: (separate with comas, do not leave spaces)');
define('TEXT_META_TEXT', 'META Description:');
//-MS- SEO-G Added EOM
?>
