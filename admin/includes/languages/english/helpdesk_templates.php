<?php
/*
  $Id: helpdesk_templates.php,v 1.5 2005/08/16 20:56:39 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'HelpDesk');

define('TABLE_HEADING_TEMPLATES', 'Templates');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_TEMPLATE', 'Template:');

define('TEXT_TEMPLATE_TITLE', 'Title:');
define('TEXT_TEMPLATE_BODY', 'Body:');

define('TEXT_INSERT_TEMPLATE', 'Save the template written below?');
define('TEXT_UPDATE_TEMPLATE', 'Update the template written below?');

define('TEXT_INFO_HEADING_DELETE_TEMPLATE', 'Delete Template');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this helpdesk template?');
?>
