<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Product Filters page language strings
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

define('HEADING_TITLE', 'New Product Filter');
define('HEADING_FILTERS_UPDATE', 'Update Filters');
define('HEADING_FILTERS_OPTIONS_UPDATE', 'Update Options for Filter: %s&nbsp;&raquo;&nbsp;%s');
define('HEADING_FILTERS_DELETE', 'Filter Removal Confirmation');
define('HEADING_FILTERS_OPTIONS_DELETE', 'Filter Options Removal Confirmation');
define('SUBHEADING_TITLE', 'Add a new Option for Filter: %s&nbsp;&raquo;&nbsp;%s');

define('HEADING_TITLE_SUB', 'New Option for Filter: %s&nbsp;&raquo;&nbsp;%s');

define('TABLE_HEADING_NEW_FILTER', 'New Filter');
define('TABLE_HEADING_FILTERS', 'Filter Name');
define('TABLE_HEADING_DB_NAME', 'DBase Column');
define('TABLE_HEADING_PARAMETER', 'Parameter');
define('TABLE_HEADING_NAME', 'Name');
define('TABLE_HEADING_LAYOUT', 'Layout');
define('TABLE_HEADING_WIDTH', 'Width');
define('TABLE_HEADING_ORDER', 'Order');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_INSERT', 
       'Enter a new filter with a name, database column and sort order. Enabled Filters with valid options can chosen when editing products. ' . 
       'Tick the associated boxes to control the visibility of fields.' . 
       'The <b>parameter</b> box must be populated and defines the GET parameter for the filter. The <b>sort order</b> defines how the filters are ordered when editing products and when viewing the catalog pages. The width symbolizes the dbase column size (valid values 1-9).'
       );

define('TEXT_INFO_INSERT_SUB', 
       'Enter a new option for the current filter with a name and sort order. Enabled Options can then be selected when editing products. ' . 
       'Tick the associated boxes to control the visibility of fields.' . 
       'The <b>sort order</b> defines how the filter options are ordered when editing products and when viewing the catalog pages.'
       );

define('TEXT_INFO_UPDATE', 'Select the filters to update. Use the tick boxes on the left to mark the items for update. Status lamps are operating separately. <br><b>Warning: Altering - specifically reducing - the width of the dbase column may lead to data loss of the listed column values in the products table.</b>');
define('TEXT_INFO_UPDATE_SUB', 'Select the filter options to update. Use the tick boxes on the left to mark the items for update. Status lamps are operating separately.<br /><em><b>Note: A single "Invalid" option is present with each filter set and should not be removed.</b></em>');
define('TEXT_INFO_DELETE_SUB', 'The following options will be deleted:');
define('TEXT_INFO_NUMBER_FILTERS', 'Defined Options:');