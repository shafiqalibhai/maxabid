<?php
/*
  $Id: geo_zones.php,v 1.7 2003/05/05 20:45:15 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Tax Zones');

define('TABLE_HEADING_COUNTRY', 'Country');
define('TABLE_HEADING_COUNTRY_ZONE', 'Zone');
define('TABLE_HEADING_TAX_ZONES', 'Tax Zones');
define('TABLE_HEADING_ACTION', 'Action');


define('TEXT_INFO_HEADING_NEW_ZONE', 'New Zone');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Please enter the new zone information');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Edit Zone');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Delete Zone');
define('TEXT_INFO_DELETE_ZONE_INTRO', 'Are you sure you want to delete this zone?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'New Sub Zone');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Please enter the new sub zone information');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Edit Sub Zone');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Delete Sub Zone');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', 'Are you sure you want to delete this sub zone?');

define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_ZONE_NAME', 'Zone Name:');
define('TEXT_INFO_NUMBER_ZONES', 'Number of Zones:');
define('TEXT_INFO_ZONE_DESCRIPTION', 'Description:');
define('TEXT_INFO_COUNTRY', 'Country:');
define('TEXT_INFO_COUNTRY_ZONE', 'Zone:');
define('TYPE_BELOW', 'All Zones');
define('PLEASE_SELECT', 'All Zones');
define('TEXT_ALL_COUNTRIES', 'All Countries');

//-MS- Added Multi-Zones support
define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_MODE', 'Mode');
define('TABLE_HEADING_ZONE_CODE', 'Zone Code');
define('TABLE_HEADING_ZONE_NAME', 'Zone Name');
define('TEXT_SELECT_MULTICOUNTRIES', 'Select the countries to insert from the following list.<br>Mode Notes: \'Expand to Zones\' will expand and insert individual zones from a country. \'All Zones\' Will insert a single entry covering all zones of a specific country');
define('TEXT_SELECT_MULTIZONES', 'Select the zones from the form below to insert them into this tax zone.');
define('TEXT_DELETE_MULTIZONE_CONFIRM', 'The following zones will be deleted from <b>%s</b> described as: %s');
define('TEXT_DELETE_MULTIZONE', 'Delete Selected Zones');
define('TEXT_SWITCH_ACTIVE_COUNTRIES', 'Switch to Active Countries Mode');
define('TEXT_SWITCH_ACTIVE_ZONES', 'Switch to Zones Mode of the selected Country');
//-MS- Added Multi-Zones support EOM
?>
