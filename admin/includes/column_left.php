<?php
/*
  $Id: column_left.php,v 1.15 2002/01/11 05:03:25 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
?>

  <tr>
    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0" class="innerLeft">

<?php
  if (tep_admin_check_boxes('administrator.php') == true) {
    require(DIR_WS_BOXES . 'administrator.php');
  } 
  if (tep_admin_check_boxes('configuration.php') == true) {
    require(DIR_WS_BOXES . 'configuration.php');
  } 
  if (tep_admin_check_boxes('catalog.php') == true) {
    require(DIR_WS_BOXES . 'catalog.php');
  } 
  if (tep_admin_check_boxes('auctions.php') == true) {
    require(DIR_WS_BOXES . 'auctions.php');
  } 
  if (tep_admin_check_boxes('customers.php') == true) {
    require(DIR_WS_BOXES . 'customers.php');
  } 
  if (tep_admin_check_boxes('helpdesk.php') == true) {
    require(DIR_WS_BOXES . 'helpdesk.php');
  } 
  if (tep_admin_check_boxes('abstract_zones.php') == true) {
    require(DIR_WS_BOXES . 'abstract_zones.php');
  } 
  if (tep_admin_check_boxes('seo_g.php') == true) {
    require(DIR_WS_BOXES . 'seo_g.php');
  } 
  if (tep_admin_check_boxes('meta_g.php') == true) {
    require(DIR_WS_BOXES . 'meta_g.php');
  } 
  if (tep_admin_check_boxes('cache.php') == true) {
    require(DIR_WS_BOXES . 'cache.php');
  } 
  if (tep_admin_check_boxes('modules.php') == true) {
    require(DIR_WS_BOXES . 'modules.php');
  } 
  if (tep_admin_check_boxes('taxes.php') == true) {
    require(DIR_WS_BOXES . 'taxes.php');
  } 
  if (tep_admin_check_boxes('localization.php') == true) {
    require(DIR_WS_BOXES . 'localization.php');
  } 
  if (tep_admin_check_boxes('reports.php') == true) {
    require(DIR_WS_BOXES . 'reports.php');
  } 
  if (tep_admin_check_boxes('tools.php') == true) {
    require(DIR_WS_BOXES . 'tools.php');
  }
//Admin end
?>
    </table></td>
  </tr>