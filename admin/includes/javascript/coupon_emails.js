/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: NuMetrics Installation via Ajax
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var coupon_emails = {
  baseURL:        false,
  title:          false,
  strings:        false,
  baseURL:        false,
  module:         false,

  install: function() {
    coupon_emails.current_progress = 0;

    //$('#coupon_emails_bar').progressbar("enable");
    coupon_emails.timer = setInterval(function() {
    $("#coupon_emails_bar .ui-widget-header").css({'background': '#DFEFFF'});

      var href = coupon_emails.baseURL+coupon_emails.get_parameters;
      var parameters = 'numetrics_ajax=1&numetrics_progress='+coupon_emails.current_progress;

      if( !coupon_emails.busy && coupon_emails.current_progress != 100 && coupon_emails.current_progress >= 0 ) {
        coupon_emails.busy = true;
        coupon_emails.send_request(href, parameters);
      } else if(coupon_emails.busy) {
        return;
      } else if(coupon_emails.timer) {
        clearInterval(coupon_emails.timer);
        coupon_emails.timer = false;
        return;
      }
    }, 500);
    //$.fancybox.close();
  },

  send_request: function(href, parameters) {
    $.ajax({
      type: 'POST',
      url: href,
      data: parameters,
      dataType: 'html',
      complete: function(msg){
      },
      success: function(msg) {
        coupon_emails.current_progress = parseInt(msg, 10);

        if( coupon_emails.current_progress >= 0 ) {
          //$("#coupon_emails_bar .ui-progressbar-value").animate({width: coupon_emails.current_progress+'%'}, 'slow');
          $('#coupon_emails_bar').progressbar( "option", "value", coupon_emails.current_progress );
          //$('#coupon_emails_bar').progressbar({ value: coupon_emails.current_progress });
        } else {
        }
        coupon_emails.busy = false;
      }
    });
  },

  progress_change: function(e, ui) {

    $bar_text = $('#numetrics_complete');
    if( coupon_emails.current_progress > 0 && coupon_emails.current_progress < 100 ) {
      $bar_text.text('Installing '+coupon_emails.current_progress+'%');
    } else if(coupon_emails.current_progress == 100) {
      $bar_text.text('Database Complete - Installing Files');

      var href = coupon_emails.baseURL+coupon_emails.get_parameters;
      var parameters = 'numetrics_ajax=1&numetrics_progress=100';

      $.ajax({
        type: 'POST',
        url: href,
        data: parameters,
        dataType: 'html',
        complete: function(msg){
        },
        success: function(msg) {
          if( msg.length ) {
            alert(msg);
          } else {

            $.fancybox.close();
            $(location).attr('href',coupon_emails.baseURL+coupon_emails.complete_parameters);
          }
        }
      });

    }
  },

  launch: function() {
    
    $('#sms_prompt').live('click', function(e) {
      e.preventDefault();
      coupon_emails.button_selector = $this = $(this);
      var url = coupon_emails.baseURL;
      //console.log(url);
      general.send_request(url).success( function(msg) {
        $.fancybox(msg['data'], {
         'orig'          : $this,
         'autoScale'     : true,
         'transitionIn'  : 'elastic',
         'transitionOut' : 'elastic',
         'title'         : msg['title']
        });

      });

    });

    $('.sms_coupons').live('click', function(e) {
      e.preventDefault();
      var $form = $(this).closest('form');
      var params = $form.serialize();
      var customer = $('#customers_email_address').val();
      var url = $(this).attr('href') + '&mark=' + customer;
      //console.log(url);
      general.send_request(url, params).success( function(msg) {

        $.fancybox(msg['data'], {
         'orig'          : coupon_emails.button_selector,
         'autoScale'     : false,
         'transitionIn'  : 'elastic',
         'transitionOut' : 'elastic',
         'title'         : msg['title']
        });

      });

    });

  }
}
