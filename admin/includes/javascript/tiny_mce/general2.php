<?php
$config['general.engine'] = 'GoogleSpell';
?>
<script language="javascript" type="text/javascript">
// Configuration file for tinyMCE editor: general
tinyMCE.init({
  mode : "exact",
  elements: "<?php echo $mce_str ?>",
  language: "en",
  theme : "advanced",
  skin : "o2k7",

  plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
  file_browser_callback : "ajaxfilemanager",
  theme_advanced_buttons1_add_before : "",
  theme_advanced_buttons1_add : "fontselect,fontsizeselect",
  theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
  theme_advanced_buttons2_add : "separator,insertdate,inserttime,separator,forecolor,backcolor",
  theme_advanced_buttons3_add_before : "tablecontrols,separator",
  theme_advanced_buttons3_add : "iespell,spellchecker,flash,advhr,separator,print,separator,preview,separator,fullscreen",

  fullscreen_settings : {
  theme_advanced_path_location : "top"},
  //document_base_url : "<?php echo HTTP_SERVER.DIR_WS_CATALOG ?>",
  document_base_url : "http://maxabid.co.uk",
  //content_css : "<?php echo HTTP_SERVER.DIR_WS_CATALOG ?>stylesheet.css",
  //external_image_list_url : "tinymce_images.js.php",
        width : "100%",
        height : "460",
        //theme_advanced_disable : "image",
  theme_advanced_toolbar_location : "top",
  theme_advanced_toolbar_align : "left",
  theme_advanced_path_location : "bottom",
  extended_valid_elements : "a[name|href|target|title|onclick],img[class|style|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
});
function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_ADMIN; ?>includes/javascript/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_ADMIN; ?>includes/javascript/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
</script>

