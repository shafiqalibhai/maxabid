/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2012 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: General Support Function for the I-Metrics CMS
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce MS2.2, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var general = {
  period: 1000,
  cookies: [],

  cookie_init: function(cookie_name) {
    if( document.cookie != 'undefined' ) {
      var cookies_array = document.cookie.split(';');
      for( var i in cookies_array) {
        var parts = cookies_array[i].split('=');
        general.cookies[parts[0]] = parts[1];
      }
    }
  },

  cookie_get: function( name ) {
    var result = '';
    if( typeof general.cookies[name] != 'undefined' ) {
      result = general.cookies[name];
    }
    return result;
  },

  cookie_set: function( name, value ) {
    general.cookies[name] = value;
  },

  cookie_update: function() {
    result = '';
    for( var i in cookies ) {
      result += i+'='+cookies[i]+';';
    }
    document.cookie = result;
  },

  stop_timer: function(timer) {
    if( typeof timer == 'undefined' || !timer ) return false;
    clearTimeout(timer);
    return true;
  },

  start_timer: function(callback, period) {
    if( typeof period == 'undefined' || !period ) {
      period = general.period;
    }
    return setTimeout(callback, period);
  },

  show_overlay: function() {
    var fancy_css = {
      'display': 'inline',
      'opacity': 0.5
    }
    $('#fancybox-overlay').css(fancy_css);
  },

  hide_overlay: function() {
    $('#fancybox-overlay').css('display', 'none');
  },

  send_request: function(href, parameters, show_loader) {
    var type = 'POST';

    if( typeof parameters == 'undefined' || !parameters.length ) {
      parameters = '';
    }

    if( typeof show_loader == 'undefined' || show_loader ) {
      show_loader = true;
    }

    return $.ajax({
      type: type,
      url: href,
      data: parameters,
      dataType: 'html',
      beforeSend: function(msg, settings) {
        if( show_loader ) {
          $.fancybox.showActivity();
          general.show_overlay();
        }
        return true;
      }, 
      dataFilter: function(msg, type) {
//alert($.dump(msg));
        var data_array = [];
        if( msg.length ) {
          msg = $.base64Decode(msg);
          data_array = $.unserialize(msg);
        } else {
          data_array['error'] = 'empty result';
        }
        return data_array;
      },
      complete: function(msg){},
      success: function(msg) {
        if( show_loader ) {
          $.fancybox.hideActivity();
          general.hide_overlay();
        }
      }
    });
  },

  notice: function(msg, $sel, title) {
    var options = {
     'autoScale'     : true,
     'transitionIn'  : 'elastic',
     'transitionOut' : 'elastic'
    };
    
    if( typeof $sel != 'undefined' && $sel.length ) {
      options.orig = $sel;
    }
    if( typeof title != 'undefined' && title.length ) {
      options.title = title;
    } else {
      options.title = 'Notice';
    }
    $.fancybox(msg, options);
  },

  launch: function() {
/*
    $('input').click(function(e) {
      e.stopPropagation();
    });
*/
    $('.toggler').click(function(e) {
      e.preventDefault();
      $selector = $(this).next();
      var current = $selector.css('display');
      current = current.toLowerCase();
      if( current == 'none' || current == '') {
        $selector.css('display', '');
      } else {
        $selector.css('display', 'none');
      }
    });

    $('select.change_submit').change(function() {
      var $parent = $(this).closest("form");
      $parent.submit();
    });

    // checkboxes multi-select
    //$('form a.page_select').live("click", function(e) {
    $('form a.page_select').click(function(e) {
      e.preventDefault();

      var id = $(this).attr('href').substr(1);
      var status = -1;
      var $parent = $(this).closest("form");
      var checkboxes_array = $parent.find('input:checkbox');

      for(var i=0; i < checkboxes_array.length; i++) {
        var check = checkboxes_array[i].name.split('[');
        if( check[0] != id ) continue;

        if( status < 0 ) {
          status = $(checkboxes_array[i]).is(':checked');
          status = status?false:true;
        }
        $(checkboxes_array[i]).attr('checked', status);        
      }
    });

    // inputs multi-select
    $('form a.input_select').click(function(e) {
      e.preventDefault();

      var id = $(this).attr('href').substr(1);
      var text = -1;
      var $parent = $(this).closest("form");
      var inputs_array = $parent.find('input:not(input[type=hidden])');

      for(var i=0; i<inputs_array.length; i++) {

        var check = inputs_array[i].name.split('[');
        if( check[0] != id ) continue;
        if( text < 0 ) {
          text = $(inputs_array[i]).val();
        }
        $(inputs_array[i]).val(text);
      }
    });

    // combos multi-select
    $('form a.combo_select').click(function(e) {
      e.preventDefault();

      var id = $(this).attr('href').substr(1);
      var index = -1;
      var $parent = $(this).closest("form");
      var selects_array = $parent.find('select');

      for(var i=0; i<selects_array.length; i++) {

        var check = selects_array[i].name.split('[');
        if( check[0] != id ) continue;
        if( index < 0 ) {
          index = $(selects_array[i]).val();
        }
        $(selects_array[i]).val(index);
      }
    });

    $('form input[type=checkbox]').live("click", function(e) {
    //$('form input[type=checkbox]').click(function(e) {

      if( !e.shiftKey && !e.ctrlKey ) {
        return;
      }

      var base = this.name.split('[');
      base = base[0];

      var status = false;
      var $parent = $(this).closest("form");
      var checkboxes_array = $parent.find('input[type=checkbox]');
      if( checkboxes_array.length < 8 ) return;

      if( $(this).is(':checked') ) {
        status = true;
      }

      if( e.shiftKey ) {
        for(var i=0; i<checkboxes_array.length; i++) {
          var tmp = checkboxes_array[i].name.split('[');
          if( tmp[0] != base ) continue;

          if( checkboxes_array[i].name == this.name ) {
            break;
          }
        }
        for(; i<checkboxes_array.length; i++) {
          var tmp = checkboxes_array[i].name.split('[');
          if( tmp[0] != base ) continue;

          $(checkboxes_array[i]).attr('checked', status);
        }
      } else {
        for(var i=checkboxes_array.length-1; i>=0; i--) {
          var tmp = checkboxes_array[i].name.split('[');
          if( tmp[0] != base ) continue;

          if( checkboxes_array[i].name == this.name ) {
            break;
          }
        }
        for(; i>=0; i--) {
          var tmp = checkboxes_array[i].name.split('[');
          if( tmp[0] != base ) continue;

          $(checkboxes_array[i]).attr('checked', status);
        }
      }
    });

    $('.add_button').click(function() {
      var parent = $.find('.add_field_section');
      if( !parent.length ) return true;

      var $field = $(parent).find('.add_field').eq(0);

      var content = $field.html().replace(new RegExp( "\\*", "g" ), '');
      $field.append(content);
      return false;
    });

    $('.auction_countdown_timer').live('click', function(e) {

      $(this).timepicker({
        showPeriod: false,
        showPeriodLabels: false,
        showLeadingZero: true,
        defaultTime: '00:00:00',
        rows: 6,
        showSeconds: true,
        minutes: {interval: 1, ends: 59},
        seconds: {interval: 1, ends: 59}
      });
    });

    $('.row_link').click(function(e) {
      if(e.target.nodeName != 'TD'){
        return true;
      }
      $(location).attr('href',$(this).attr('href'));
    });
  }
}
