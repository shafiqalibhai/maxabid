/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: Auctions Tiers support
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var auctions_tier = {

  auctionsURL: false,
  productsURL: false,

  launch: function() {

    $('#button_auction_tier').click(function() {
      var url = auctions_tier.auctionsURL;

      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'html',
        complete: function(msg){
        },
        success: function(msg) {

          var title = this.innerHTML;

          $.fancybox({
            'type'          : 'html',
            'content'       : msg,
            'autoScale'     : true,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic',
            'title'         : 'Select an Auction'
          });


        }
      });

      return false;
    });

    $('#button_product_tier').click(function() {
      var url = auctions_tier.productsURL;

      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'html',
        complete: function(msg){
        },
        success: function(msg) {

          var title = this.innerHTML;

          $.fancybox({
            'type'          : 'html',
            'content'       : msg,
            'autoScale'     : true,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic',
            'title'         : 'Select a Product'
          });


        }
      });

      return false;
    });

    $('.auctions_tier_row').live('click', function(e){
      var index = $(this).attr('attr');
      $(':input[name=auctions_id]').val(index);
      $.fancybox.close();
      e.preventDefault();
    });

    $('.products_tier_row').live('click', function(e){
      var index = $(this).attr('attr');
      $(':input[name=products_id]').val(index);
      $.fancybox.close();
      e.preventDefault();
    });
  }
}
