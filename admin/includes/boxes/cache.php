<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Cache Manager for osC Admin
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_CACHE,
                     'link'  => tep_href_link(FILENAME_CACHE_CONFIG, 'selected_box=cache'));

  if ($selected_box == 'cache') {
      $contents[] = array('text' => tep_admin_files_boxes(FILENAME_CACHE_CONFIG, BOX_CACHE_CONFIG) .
                                    //tep_admin_files_boxes(FILENAME_CACHE_SQL, BOX_CACHE_SQL) .
                                    tep_admin_files_boxes(FILENAME_CACHE_HTML, BOX_CACHE_HTML) .
                                    tep_admin_files_boxes(FILENAME_CACHE_REPORTS, BOX_CACHE_REPORTS)
                         );
  }
  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
