<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Zones component for Admin
// Controls relationships among products, categories and attributes etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_ABSTRACT_ZONES,
                     'link'  => tep_href_link(FILENAME_ABSTRACT_ZONES_CONFIG, 'selected_box=abstract_config'));

  if ($selected_box == 'abstract_config') {
    $contents[] = array('text'  => tep_admin_files_boxes(FILENAME_ABSTRACT_ZONES_CONFIG, BOX_ABSTRACT_CONFIG) .
                                   tep_admin_files_boxes(FILENAME_ABSTRACT_TYPES, BOX_ABSTRACT_TYPES) .
                                   tep_admin_files_boxes(FILENAME_ABSTRACT_ZONES, BOX_ABSTRACT_ZONES) .
                                   tep_admin_files_boxes(FILENAME_GENERIC_TEXT, BOX_ABSTRACT_GENERIC_TEXT)
                       );
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
