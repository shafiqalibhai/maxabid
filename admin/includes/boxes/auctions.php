<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Auctions component for Admin
// Controls relationships among products, categories and attributes etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_AUCTIONS,
                     'link'  => tep_href_link(FILENAME_AUCTIONS_GROUP, 'selected_box=auctions'));

  if ($selected_box == 'auctions') {
    $box_string = '';
    $auction_groups_array = array();
    $auction_groups_query_raw = "select auctions_group_id as id, auctions_group_name as text from " . TABLE_AUCTIONS_GROUP . " order by auctions_group_name";
    tep_query_to_array($auction_groups_query_raw, $auction_groups_array, 'id');
    foreach($auction_groups_array as $key => $value) {
      $tmp_array = explode(' ', $value['text']);
      $box_string .= '<a href="' . tep_href_link(FILENAME_AUCTIONS, 'agID=' . $key) . '"><b>' . implode(' ', $tmp_array) . '</b></a><br />';
    }



    $contents[] = array(
      'text'  => tep_admin_files_boxes(FILENAME_AUCTIONS_GROUP, BOX_AUCTIONS_GROUP) . 
                 tep_admin_files_boxes(FILENAME_AUCTIONS, BOX_AUCTIONS) . $box_string . 
                 tep_admin_files_boxes(FILENAME_AUCTIONS_TIER, BOX_AUCTIONS_TIER) . 
                 tep_admin_files_boxes(FILENAME_AUCTIONS_CONFIG, BOX_AUCTIONS_CONFIG)
    );
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
