<?php
/*
  $Id: tools.php,v 1.21 2003/07/09 01:18:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
?>
<!-- tools //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_TOOLS,
                     'link'  => tep_href_link(FILENAME_WHOS_ONLINE, 'selected_box=tools'));

  if ($selected_box == 'tools') {
    $contents[] = array('text' =>
                                  tep_admin_files_boxes(FILENAME_ADMIN_EMAIL_TEMPLATES, BOX_TOOLS_EMAIL_TEMPLATES) .
                                  tep_admin_files_boxes(FILENAME_TESTIMONIALS, BOX_TOOLS_TESTIMONIALS) .
                                  tep_admin_files_boxes(FILENAME_TOTAL_CONFIGURATION, BOX_TOOLS_TOTAL_CONFIGURATION) .
                                  tep_admin_files_boxes(FILENAME_BACKUP, BOX_TOOLS_BACKUP) .
                                  tep_admin_files_boxes(FILENAME_DEFINE_LANGUAGE, BOX_TOOLS_DEFINE_LANGUAGE) .
                                  tep_admin_files_boxes(FILENAME_MAIL, BOX_TOOLS_MAIL) .
                                  tep_admin_files_boxes(FILENAME_NEWSLETTERS, BOX_TOOLS_NEWSLETTER_MANAGER) .
                                  tep_admin_files_boxes(FILENAME_SERVER_INFO, BOX_TOOLS_SERVER_INFO) .
                                  tep_admin_files_boxes(FILENAME_WHOS_ONLINE, BOX_TOOLS_WHOS_ONLINE) .
                                  tep_admin_files_boxes(FILENAME_SEND_SMS, BOX_TOOLS_SEND_SMS)
                       );
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- tools_eof //-->
