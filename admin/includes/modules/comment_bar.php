<?php
/*
 $Id: comment_bar.php,v 4.0 2005/02/16 Skeedo.com Enterprises Exp $
 
 Admin Comments Toolbar 4.0 by Skeedo.com Enterprises osc@skeedo.com

  - Update to multilangual: Ben Zukrel   Date:2005/02/16

 osCommerce, Open Source E-Commerce Solutions
 http://www.oscommerce.com

 Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Modifications by Asymmetrics
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Rewritten code to use dynamic text entries.
// Removed language dependencies.
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
<style type="text/css">
.cbutton { font-family: Verdana; font-size: 10px; padding: 3; margin: 2; background-color: #D1E4FF; border-bottom: 2px solid #003366; border-right: 1px solid #003366; border-top: 1px solid #003366; border-left: 1px solid #003366; cursor: pointer;}
</style>
<script language="javascript"><!--
  function updateComment(obj, statusnum) {
    textareas = document.getElementsByTagName('textarea');
    myTextarea = textareas.item(0);
    for( $i=0; $i<10; $i++ ) {
      tmp_index = obj.indexOf('[%s');
      if( tmp_index != -1 ) {
        tmp_replace = obj.substring(tmp_index+3, obj.length);
        tmp_index2 = obj.indexOf(']');
        if( tmp_index2 != -1 ) {
          sub_replace = obj.substring(tmp_index+3, tmp_index2);
          result = get_input(sub_replace);
          sub_replace = obj.substring(tmp_index2+1, obj.length);
          first_seg = obj.substring(0, tmp_index);
          obj = first_seg+result+sub_replace;
        }
      } else {
        break;
      }
    }
    myTextarea.value = obj;

    if( statusnum != '') {
      selector = document.getElementById('orders_status');
      selector.value = statusnum;
    }
    return false;           
  }

  function killbox() {
    var box = document.getElementsByTagName('textarea');
    var killbox = box.item(0);
    killbox.value = '';
    return false;
  }

  function get_input(user_input) {
    result = prompt(user_input); 
    return result;
  }
//--></script>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText"><?php echo '<a href="' . tep_href_link(FILENAME_ORDERS_COMMENTS) . '"><font color="#0033CC"><b>click here to edit the comment entries</b></font></a>'; ?></td>
              </tr>
              <tr>
                <td>
<?php
  $buttons_query = tep_db_query("select orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id from " . TABLE_ORDERS_COMMENTS . " where status_id='1'");
  while($buttons_array = tep_db_fetch_array($buttons_query)) {
    //$buttons_array['orders_comments_text'] = preg_replace("/\r\n|\r|\n/", '<br />', $buttons_array['orders_comments_text']);
    $buttons_array['orders_comments_text'] = preg_replace("/\r\n|\r|\n/", "\\n", $buttons_array['orders_comments_text']);

//-MS- Tracking numbers support added
//    if( isset($order) && is_object($order) && isset($order->products) && is_array($order->products) && isset($order->products[0]['tracking']) && tep_not_null($order->products[0]['tracking']) ) {
//      $buttons_array['orders_comments_text'] = str_replace('[%sTracking No]', $order->products[0]['tracking'], $buttons_array['orders_comments_text']);
//      $buttons_array['orders_comments_text'] = str_replace('[%sTracking Number]', $order->products[0]['tracking'], $buttons_array['orders_comments_text']);
//    }
//-MS- Tracking numbers support added EOM
    echo '<button class="cbutton" onclick="return updateComment(\'' . tep_db_input($buttons_array['orders_comments_text']) . '\',\'' .  $buttons_array['orders_status_id'] . '\'); return false;">' . $buttons_array['orders_comments_title'] . '</button>&nbsp;';
  }
?>
                  <button class="cbutton" onclick="return killbox();"><?php echo 'Reset'; ?></button>&nbsp;
                </td>
              </tr>
            </table>

