<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2012 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Coupons To Customers Module
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce MS2.2, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  //include_once(DIR_WS_LANGUAGES . $language . '/coupons.php');

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  $cID = isset($_GET['cID'])?(int)$_GET['cID']:0;
  $coupon_query = tep_db_query("select coupons_name, coupons_message, last_customer_id from " . TABLE_COUPONS . " where coupons_id = '" . (int)$cID . "'");

  if( !tep_db_num_rows($coupon_query) ) {
    $result_array = array(
      'title' => ERROR_TITLE_INVALID_COUPON,
      'data' => ERROR_INVALID_COUPON,
    );
    $result = tep_encode_serialize($result_array);
    echo $result;
    tep_exit();
  }

  $coupon_array = tep_db_fetch_array($coupon_query);
  require(DIR_WS_CLASSES . 'xml_core.php');
  require(DIR_WS_CLASSES . 'sms_text.php');

  switch($action) {
    case 'send_selected':
      $buttons = array(
        '<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) ) . '" title="' . TEXT_INFO_BUTTON_BACK_LISTING . '">' . BUTTON_BACK . '</a>',
      );

      if( !isset($_GET['mark']) ) {
        $result_array = array(
          'title' => $coupon_array['coupons_name'] . '&nbsp;-&nbsp' . ERROR_TITLE_NOTHING_SELECTED,
          'data' => '<div class="bounder">' . ERROR_NOTHING_SELECTED . '</div><div>' . implode('', $buttons) . '</div>',
        );
        $result = tep_encode_serialize($result_array);
        echo $result;
        tep_exit();
      }

      $customers_array = $_GET['mark'];
      $customers_query_raw = "select customers_email_address, customers_fax from " . TABLE_CUSTOMERS . " where customers_id in (" . $customers_array . ") and customers_fax != ''";
      $customers_array = array();
      tep_query_to_array($customers_query_raw, $customers_array);
      $cSMS = new sms_text();

      $sms_message = $coupon_array['coupons_message'] . ' ' . $coupon_array['coupons_name'];

      $result_array = array();
      ob_start();
?>
            <div class="highter"><table class="tabledata">
<?php
      for( $i=0, $j=count($customers_array); $i<$j; $i++ ) {
        $xml_array = $cSMS->send_message($sms_message, $customers_array[$i]['customers_fax']);
        if( !$i ) {
          $rowclass = 'dataTableHeadingRow';
          echo '<tr class="' . $rowclass . '">' . "\n";
          echo '<th>' . TABLE_HEADING_CUSTOMERS_EMAIL . '</th>' . "\n";
          foreach($xml_array as $key => $value) {
            echo '<th>' . $key . '</th>' . "\n";
          }
          echo '</tr>' . "\n";
        }

        $rowclass = ($i%2)?'dataTableRow':'dataTableRowAlt';
        echo '<tr class="' . $rowclass . '">' . "\n";
        echo '<td>' . $customers_array[$i]['customers_email_address'] . '</td>' . "\n";
        foreach($xml_array as $key => $value) {
          if( is_array($value) ) {
            $value = implode(',', $value);
          }
          echo '<td>' . $value . '</td>' . "\n";
        }
        echo '</tr>' . "\n";
      }
?>
            </table><div class="tmargin"><?php echo implode('', $buttons); ?></div></div>
<?php

      $contents = ob_get_contents();
      ob_end_clean();

      $result_array = array(
        'title' => $coupon_array['coupons_name'],
        'data' => $contents,
      );
      $result = tep_encode_serialize($result_array);
      echo $result;

      break;
    case 'send_all':
      $buttons = array(
        '<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) ) . '" title="' . TEXT_INFO_BUTTON_BACK_LISTING . '">' . BUTTON_BACK . '</a>',
      );

      $customers_query_raw = "select customers_email_address, customers_fax from " . TABLE_CUSTOMERS . " where customers_fax != ''";
      $customers_array = array();
      tep_query_to_array($customers_query_raw, $customers_array);
      $cSMS = new sms_text();

      $sms_message = $coupon_array['coupons_message'] . $coupon_array['coupons_name'];

      $result_array = array();
      ob_start();
?>
            <div class="highter"><table class="tabledata">
<?php
      for( $i=0, $j=count($customers_array); $i<$j; $i++ ) {
        $xml_array = $cSMS->send_message($sms_message, $customers_array[$i]['customers_fax']);
        if( !$i ) {
          $rowclass = 'dataTableHeadingRow';
          echo '<tr class="' . $rowclass . '">' . "\n";
          echo '<th>' . TABLE_HEADING_CUSTOMERS_EMAIL . '</th>' . "\n";
          foreach($xml_array as $key => $value) {
            echo '<th>' . $key . '</th>' . "\n";
          }
          echo '</tr>' . "\n";
        }

        $rowclass = ($i%2)?'dataTableRow':'dataTableRowAlt';
        echo '<tr class="' . $rowclass . '">' . "\n";
        echo '<td>' . $customers_array[$i]['customers_email_address'] . '</td>' . "\n";
        foreach($xml_array as $key => $value) {
          if( is_array($value) ) {
            $value = implode(',', $value);
          }
          echo '<td>' . $value . '</td>' . "\n";
        }
        echo '</tr>' . "\n";
      }
?>
            </table><div class="tmargin"><?php echo implode('', $buttons); ?></div></div>
<?php

      $contents = ob_get_contents();
      ob_end_clean();

      $result_array = array(
        'title' => $coupon_array['coupons_name'],
        'data' => $contents,
      );
      $result = tep_encode_serialize($result_array);
      echo $result;
      break;
    case 'send_resume':
      $buttons = array(
        '<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) ) . '" title="' . TEXT_INFO_BUTTON_BACK_LISTING . '">' . BUTTON_BACK . '</a>',
      );

      $customers_array = array();
      $customers_query_raw = "select customers_id, customers_email_address, customers_fax from " . TABLE_CUSTOMERS . " where customers_id > '" . (int)$coupon_array['last_customer_id'] . "' and customers_fax != '' limit 50";
      tep_query_to_array($customers_query_raw, $customers_array);

      $cSMS = new sms_text();
      $sms_message = $coupon_array['coupons_name'];

      $result_array = array();
      ob_start();
?>
            <div class="highter"><table class="tabledata">
<?php
      for( $i=0, $j=count($customers_array); $i<$j; $i++ ) {
        $xml_array = $cSMS->send_message($sms_message, $customers_array[$i]['customers_fax']);
        if( !$i ) {
          $rowclass = 'dataTableHeadingRow';
          echo '<tr class="' . $rowclass . '">' . "\n";
          echo '<th>' . TABLE_HEADING_CUSTOMERS_EMAIL . '</th>' . "\n";
          foreach($xml_array as $key => $value) {
            echo '<th>' . $key . '</th>' . "\n";
          }
          echo '</tr>' . "\n";
        }

        $rowclass = ($i%2)?'dataTableRow':'dataTableRowAlt';
        echo '<tr class="' . $rowclass . '">' . "\n";
        echo '<td>' . $customers_array[$i]['customers_email_address'] . '</td>' . "\n";
        foreach($xml_array as $key => $value) {
          if( is_array($value) ) {
            $value = implode(',', $value);
          }
          echo '<td>' . $value . '</td>' . "\n";
        }
        echo '</tr>' . "\n";
      }
      if( $j ) {
        tep_db_query("update " . TABLE_COUPONS . " set last_customer_id = '" . (int)$customers_array[$j-1]['customers_id'] . "' where coupons_id = '" . (int)$cID . "'");
      }
?>
            </table><div class="tmargin"><?php echo implode('', $buttons); ?></div></div>
<?php

      $contents = ob_get_contents();
      ob_end_clean();

      $result_array = array(
        'title' => $coupon_array['coupons_name'],
        'data' => $contents,
      );
      $result = tep_encode_serialize($result_array);
      echo $result;
      break;

    default:
      ob_start();

/*
      $number_array = array(
        array('id' => 0, 'text' => TEXT_DEFAULT),
        array('id' => 1, 'text' => '1'),
        array('id' => 2, 'text' => '2'),
        array('id' => 3, 'text' => '3'),
      );

      $period_array = array(
        array('id' => 0, 'text' => TEXT_DEFAULT),
        array('id' => 'day', 'text' => 'Days'),
        array('id' => 'week', 'text' => 'Weeks'),
        array('id' => 'month', 'text' => 'Months'),
      );

      $customer_array = array(
        array('id' => 0, 'text' => TEXT_DEFAULT),
        array('id' => '10', 'text' => '10 Customers'),
        array('id' => '50', 'text' => '50 Customers'),
        array('id' => '100', 'text' => '100 Customers'),
        array('id' => '200', 'text' => '200 Customers'),
      );

      $type_array = array(
        array('id' => 0, 'text' => TEXT_DEFAULT),
        array('id' => 'max-bids', 'text' => 'Most Bids'),
        array('id' => 'max-wins', 'text' => 'Most Wins'),
        array('id' => 'most-logins', 'text' => 'Most Logins'),
        array('id' => 'non-logins', 'text' => 'Non Logins'),
      );

      $selection = '';
      $selection .= '<table class="tabledata">';
      $selection .= '<tr class="dataTableHeadingRow"><th>Select Number</th><th>Select Period</th><th>Select Customers</th><th>Select Type</th>';
      $selection .= '<tr><td>' . tep_draw_pull_down_menu('number_select', $number_array) . '</td><td>' . tep_draw_pull_down_menu('period_select', $period_array) . '</td><td>' . tep_draw_pull_down_menu('customer_select', $customer_array) . '</td><td>' . tep_draw_pull_down_menu('type_select', $type_array) . '</td><tr>';
      $selection .= '</table>';
      $selection .= '<div class="vpad"><a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=send_selected') . '" title="' . TEXT_INFO_BUTTON_SEND_SELECTED . '">' . BUTTON_SEND_SELECTED . '</a></div>';
*/
      $buttons = array(
        //'<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=send_selected') . '" title="' . TEXT_INFO_BUTTON_SEND_SELECTED . '">' . BUTTON_SEND_SELECTED . '</a>',
        '<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=send_all') . '"  title="' . TEXT_INFO_BUTTON_SEND_ALL . '">' . BUTTON_SEND_ALL . '</a>',
        '<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=send_selected') . '"  title="' . TEXT_INFO_BUTTON_SEND_ALL . '"> Send Selected</a>'
        //'<a class="textbutton sms_coupons" href="' . tep_href_link($g_script, tep_get_all_get_params(array('action')) . 'action=send_resume') . '"  title="' . TEXT_INFO_BUTTON_SEND_RESUME . '">' . BUTTON_SEND_RESUME . '</a>',
      );
      $cSMS = new sms_text();
      $credits = $cSMS->get_available_credit();
?>
        <div class="heavy"><?php echo sprintf(TEXT_INFO_AVAILABLE_CREDITS, $credits); ?></div>
        <div><?php echo tep_draw_form("sms_coupon", $g_script); ?>
<?php
/*
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th class="calign" width="5%"><?php echo '<a href="javascript:void(0)" onclick="copy_checkboxes(document.coupons_form,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></th>
                <th><?php echo TABLE_HEADING_CUSTOMERS_NAME; ?></th>
                <th><?php echo TABLE_HEADING_CUSTOMERS_EMAIL; ?></th>
                <th><?php echo TABLE_HEADING_CUSTOMERS_SMS; ?></th>
              </tr>
<?php
      $customers_query_raw = "select customers_id, customers_email_address, concat(customers_firstname, ' ', customers_lastname) as customers_name, customers_fax from " . TABLE_CUSTOMERS . " where customers_fax != '' order by customers_lastname";
      $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $customers_query_raw, $customers_query_numrows);
      $customers_array = array();
      tep_query_to_array($customers_query_raw, $customers_array, 'customers_id');

      $rows = 0;
      foreach( $customers_array as $key => $customer ) {
        $rows++;
        $rowclass = ($rows%2)?'dataTableRow':'dataTableRowAlt';
?>
              <tr class="<?php echo $rowclass . ' auctions_tier_row'; ?>" attr="<?php echo $customer['auctions_id']; ?>">
                <td width="5%"><?php echo tep_draw_checkbox_field('mark['.$customer['customers_id'].']', 1) ?></td>
                <td><?php echo $customer['customers_name']; ?></td>
                <td><?php echo $customer['customers_email_address']; ?></td>
                <td><?php echo $customer['customers_fax']; ?></td>
              </tr>
<?php
      }
?>
            </table></td>
          </tr>
          <tr>
            <td><table class="tabledata">
              <tr>
                <td class="smallText"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText ralign"><?php echo $customers_split->display_links_ajax(5, 'strings=coupons&module=coupons_to_customers'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table>
*/
?>
        <div class="tmargin tpad" style="width: 300px; height: 20px;"><?php echo implode('', $buttons); ?></div></form></div>
<?php
      $contents = ob_get_contents();
      ob_end_clean();
      $result_array = array(
        'title' => sprintf(TEXT_INFO_COUPON_SMS_TO_CUSTOMERS, $coupon_array['coupons_name']),
        'data' => $contents,
      );
      $result = tep_encode_serialize($result_array);
      echo $result;
      break;
  }
?>
