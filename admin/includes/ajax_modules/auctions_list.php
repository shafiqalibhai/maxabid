<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Common Index Rendering Module
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $auction_status_array = array(
    array('id' => 0, 'text' => 'Coming Auction'),
    array('id' => 1, 'text' => 'Live Auction'),
    array('id' => 2, 'text' => 'Ended Auction'),
  );

  $auction_types_array = array(
    array('id' => 0, 'text' => 'Penny Auction'),
    array('id' => 1, 'text' => 'Lowest Unique Bid'),
    array('id' => 2, 'text' => 'Normal Auction'),
  );
?>
        <div><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th><?php echo TABLE_HEADING_ID; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_IMAGE; ?></th>
                <th><?php echo TABLE_HEADING_TITLE; ?></th>
                <th><?php echo TABLE_HEADING_TYPE; ?></th>
                <th><?php echo TABLE_HEADING_STATUS; ?></th>
              </tr>
<?php
  $auctions_query_raw = "select auctions_id, auctions_name, auctions_image, auctions_type, status_id from " . TABLE_AUCTIONS . " where status_id < 2 order by sort_id, auctions_name";
  $auctions_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $auctions_query_raw, $auctions_query_numrows);
  $auctions_array = array();
  tep_query_to_array($auctions_query_raw, $auctions_array, 'auctions_id');

  $rows = 0;
  foreach( $auctions_array as $key => $auction ) {
    $rows++;
    $rowclass = ($rows%2)?'dataTableRow':'dataTableRowAlt';
?>
              <tr class="<?php echo $rowclass . ' auctions_tier_row'; ?>" attr="<?php echo $auction['auctions_id']; ?>">
                <td><?php echo $auction['auctions_id']; ?></td>
                <td class="calign"><?php echo tep_image(DIR_WS_CATALOG_IMAGES . $auction['auctions_image'], $auction['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT); ?></td>
                <td><?php echo $auction['auctions_name']; ?></td>
                <td><?php echo $auction_types_array[$auction['auctions_type']]['text']; ?></td>
                <td><?php echo $auction_status_array[$auction['status_id']]['text']; ?></td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td><table class="tabledata">
              <tr>
                <td class="smallText"><?php echo $auctions_split->display_count($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText ralign"><?php echo $auctions_split->display_links($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></div>
