<?php
/*
  $Id: filenames.php,v 1.1 2003/06/20 00:18:30 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// define the filenames used in the project
  define('FILENAME_BACKUP', 'backup.php');
  define('FILENAME_BANNER_MANAGER', 'banner_manager.php');
  define('FILENAME_BANNER_STATISTICS', 'banner_statistics.php');
  define('FILENAME_CATALOG_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
  define('FILENAME_CATEGORIES', 'categories.php');
  define('FILENAME_CONFIGURATION', 'configuration.php');
  define('FILENAME_COUNTRIES', 'countries.php');
  define('FILENAME_CURRENCIES', 'currencies.php');
  define('FILENAME_CUSTOMERS', 'customers.php');
  define('FILENAME_DEFAULT', 'index.php');
  define('FILENAME_DEFINE_LANGUAGE', 'define_language.php');
//  define('FILENAME_FILE_MANAGER', 'file_manager.php');
  define('FILENAME_GEO_ZONES', 'geo_zones.php');
  define('FILENAME_LANGUAGES', 'languages.php');
  define('FILENAME_MAIL', 'mail.php');
  define('FILENAME_MANUFACTURERS', 'manufacturers.php');
  define('FILENAME_MODULES', 'modules.php');
  define('FILENAME_NEWSLETTERS', 'newsletters.php');
  define('FILENAME_ORDERS', 'orders.php');
  define('FILENAME_ORDERS_INVOICE', 'invoice.php');
  define('FILENAME_ORDERS_PACKINGSLIP', 'packingslip.php');
  define('FILENAME_ORDERS_STATUS', 'orders_status.php');
  define('FILENAME_POPUP_IMAGE', 'popup_image.php');
  define('FILENAME_PRODUCTS_ATTRIBUTES', 'products_attributes.php');
  define('FILENAME_PRODUCTS_EXPECTED', 'products_expected.php');
  define('FILENAME_REVIEWS', 'reviews.php');
  define('FILENAME_SERVER_INFO', 'server_info.php');
  define('FILENAME_SHIPPING_MODULES', 'shipping_modules.php');
  define('FILENAME_SPECIALS', 'specials.php');
  define('FILENAME_STATS_CUSTOMERS', 'stats_customers.php');
  define('FILENAME_STATS_PRODUCTS_PURCHASED', 'stats_products_purchased.php');
  define('FILENAME_STATS_PRODUCTS_VIEWED', 'stats_products_viewed.php');
  define('FILENAME_TAX_CLASSES', 'tax_classes.php');
  define('FILENAME_TAX_RATES', 'tax_rates.php');
  define('FILENAME_WHOS_ONLINE', 'whos_online.php');
  define('FILENAME_SEND_SMS', 'send_sms.php');
  define('FILENAME_ZONES', 'zones.php');

//-MS- Added Abstract Zones
  define('FILENAME_ABSTRACT_ZONES','abstract_zones.php');
  define('FILENAME_ABSTRACT_ZONES_CONFIG','abstract_zones_config.php');
  define('FILENAME_ABSTRACT_TYPES','abstract_types.php');
//-MS- Added Abstract Zones EOM

//-MS- Generic Text Added
  define('FILENAME_GENERIC_TEXT', 'generic_text.php');
//-MS- Generic Text Added EOM

//-MS- Added Email Templates
  define('FILENAME_ADMIN_EMAIL_TEMPLATES', 'admin_email_templates.php');
//-MS- Added Email Templates EOM

//-MS- Added Numeric Ranges
  define('FILENAME_NUMERIC_RANGES', 'numeric_ranges.php');
//-MS- Added Numeric Ranges EOM

//-MS- Total configuration Added
  define('FILENAME_TOTAL_CONFIGURATION', 'total_configuration.php');
//-MS- Total configuration Added EOM

//-MS- SEO-G Added
  define('FILENAME_SEO_G','seo_g.php');
  define('FILENAME_SEO_ZONES','seo_zones.php');
  define('FILENAME_SEO_ZONES_CONFIG','seo_zones_config.php');
  define('FILENAME_SEO_TYPES','seo_types.php');
  define('FILENAME_SEO_REPORTS','seo_reports.php');
  define('FILENAME_SEO_EXCLUDE','seo_exclude.php');
  define('FILENAME_SEO_REDIRECTS','seo_redirects.php');
//-MS- SEO-G Added EOM

//-MS- XML Support Added
  define('FILENAME_XML_CORE','xml_core.php');
  define('FILENAME_XML_GOOGLE_SITEMAP','xml_google_sitemap.php');
//-MS- XML Support Added EOM

//-MS- META-G Added
  define('FILENAME_META_G','meta_g.php');
  define('FILENAME_META_ZONES_CONFIG','meta_zones_config.php');
  define('FILENAME_META_ZONES','meta_zones.php');
  define('FILENAME_META_TYPES','meta_types.php');
  define('FILENAME_META_REPORTS','meta_reports.php');
  define('FILENAME_META_SCRIPTS','meta_scripts.php');
  define('FILENAME_META_LEXICO','meta_lexico.php');
  define('FILENAME_META_EXCLUDE','meta_exclude.php');
  define('FILENAME_META_FEEDS','meta_feeds.php');
//-MS- META-G Added EOM

//-MS- Cache Manager Added
define('FILENAME_CACHE_CONFIG','cache_config.php');
define('FILENAME_CACHE_SQL','cache_sql.php');
define('FILENAME_CACHE_HTML','cache_html.php');
define('FILENAME_CACHE_REPORTS','cache_reports.php');
//-MS- Cache Manager Added EOM

//-MS- Add Quick Add Products
  define('FILENAME_QUICK_ADD_PRODUCTS', 'quick_add_products.php');
//-MS- Add Quick Add Products EOM

//-MS- Multiple Products Manager 2.0
  define('FILENAME_PRODUCTS_MULTI', 'products_multi.php');
//-MS- Multiple Products Manager 2.0 EOM

//-MS- extra images added
  define('FILENAME_PRODUCTS_EXTRA_IMAGES','products_extra_images.php');
//-MS- extra images added EOM

//-MS- Testimonials Added
  define('FILENAME_TESTIMONIALS', 'testimonials.php');
//-MS- Testimonials Added EOM

//-MS- Shout Text Added
  define('FILENAME_GSHOUT_TEXT', 'gshout_text.php');
//-MS- Shout Text Added EOM

//-MS- HelpDesc Added
  define('FILENAME_HELPDESK', 'helpdesk.php');
  define('FILENAME_HELPDESK_LOG', 'helpdesk_log.php');
  define('FILENAME_HELPDESK_NEW_EMAIL', 'helpdesk_new_email.php');
  define('FILENAME_HELPDESK_DEPARTMENTS', 'helpdesk_departments.php');
  define('FILENAME_HELPDESK_PRIORITIES', 'helpdesk_priorities.php');
  define('FILENAME_HELPDESK_STATUS', 'helpdesk_status.php');
  define('FILENAME_HELPDESK_TEMPLATES', 'helpdesk_templates.php');
  define('FILENAME_HELPDESK_POP3', 'helpdesk_pop3.php');
  define('FILENAME_HELPDESK_MASS_DELETE', 'helpdesk_mass_delete.php');
//-MS- HelpDesc Added EOM

//-MS- Added Order Creation
  define('FILENAME_CREATE_ORDER_PROCESS', 'create_order_process.php');
  define('FILENAME_CREATE_ORDER', 'create_order.php');
  define('FILENAME_EDIT_ORDERS', 'edit_orders.php');
//-MS- Added Order Creation EOM

//-MS- Added Repository Text
  define('FILENAME_GREP_TEXT', 'grep_text.php');
//-MS- Added Repository Text EOM

//-MS- Testimonials Added
  define('FILENAME_TESTIMONIALS_MANAGER', 'testimonials_manager.php');
//-MS- Testimonials Added EOM

//-MS- Added Products Filters
  define('FILENAME_PRODUCTS_FILTERS', 'products_filters.php');
//-MS- Added Products Filters EOM

//-MS- Added Products Filters
  define('FILENAME_GROUP_FIELDS', 'group_fields.php');
//-MS- Added Products Filters EOM

//-MS- Orders to generate invoices Added
  define('FILENAME_PDF_ORDER_INVOICE', 'pdf_order_invoice.php');
//-MS- Orders to generate invoices Added EOM

//-MS- Orders Comments Added
  define('FILENAME_ORDERS_COMMENTS', 'orders_comments.php');
  define('FILENAME_ORDERS_EDIT', 'edit_orders.php');
//-MS- Orders Comments Added EOM

//-MS- Admin begin
  define('FILENAME_ADMIN_ACCOUNT', 'admin_account.php');
  define('FILENAME_ADMIN_FILES', 'admin_files.php');
  define('FILENAME_ADMIN_MEMBERS', 'admin_members.php');  
  Define('FILENAME_FORBIDEN', 'forbiden.php');
  define('FILENAME_LOGIN', 'login.php');
  define('FILENAME_LOGOFF', 'logoff.php');
  define('FILENAME_PASSWORD_FORGOTTEN', 'password_forgotten.php');
//-MS- Admin end

//-MS- Explain Queries Report Added
  define('FILENAME_STATS_EXPLAIN_QUERIES', 'stats_explain_queries.php');
//-MS- Explain Queries Report Added EOM

//-MS- Other Configuration Added
  define('FILENAME_OTHER_CONFIG', 'other_config.php');
//-MS- Other Configuration Added EOM

  define('FILENAME_AJAX_CONTROL', 'ajax_control.php');

  define('FILENAME_AUCTIONS', 'auctions.php');
  define('FILENAME_AUCTIONS_GROUP', 'auctions_group.php');
  define('FILENAME_AUCTIONS_TIER', 'auctions_tier.php');
  define('FILENAME_AUCTIONS_CONFIG', 'auctions_config.php');

  define('FILENAME_COUPONS', 'coupons.php');
  define('FILENAME_SEND_SMS', 'coupons.php');     
?>
