<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Accessories Zones class for osCommerce Admin
// This is a Bridge for the Abstract Zones
// Controls relationships of categories.
// Featuring:
// - Multi-Categories instant selection
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class categories_zones extends abstract_zones {

// class constructor
    function categories_zones() {
      $this->m_mcpage = isset($_GET['mcpage'])?$_GET['mcpage']:'';
      parent::abstract_zones();
    }

    function process_action() {
      switch( $this->m_action ) {
        case 'update_multizone':
          $result = parent::validate_array_selection('pc_id');
          return $this->update_multizone();
        case 'multi_insert':
          return $this->multi_insert();
        case 'multi_categories':
          return $this->multi_categories();
        case 'deleteconfirm_multizone':
          $result = parent::validate_array_selection('categories_id'); 
          return $this->deleteconfirm_multizone();
        case 'deleteconfirm_zone':
          return $this->deleteconfirm_zone();
        default:
          return parent::process_action(); 
          break;
      }
    }

    function update_multizone() {
      foreach ($_POST['pc_id'] as $pc_id=>$val) {
        $sql_data_array = array(
                                'sort_id' => (int)$_POST['sort'][$pc_id],
                                'status_id' => (int)$_POST['status'][$pc_id]
                               );

        tep_db_perform(TABLE_CATEGORIES_ZONES_TO_CATEGORIES, $sql_data_array, 'update', "abstract_zone_id = '" . (int)$this->m_zID . "' and categories_id = '" . (int)$pc_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function multi_insert() {
      $check_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where categories_id = '0' and abstract_zone_id = '" . (int)$this->m_zID . "'");
      if( tep_db_num_rows($check_query) ) {
        $messageStack->add_session(ERROR_CANNOT_ADD_MORE, 'error');
        tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . '&action=list'));
        break;
      }
      tep_db_query("delete from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      $sql_data_array = array(
                              'abstract_zone_id' => (int)$this->m_zID,
                              'categories_id' => '0'
                             );
      tep_db_perform(TABLE_CATEGORIES_ZONES_TO_CATEGORIES, $sql_data_array);
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . '&action=list'));
    }


    function multi_categories() {
      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');
      $check_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where categories_id = '0' and abstract_zone_id = '" . (int)$this->m_zID . "'");
      if( tep_db_num_rows($check_query) > 0 ) {
        $messageStack->add_session(ERROR_CANNOT_ADD_MORE, 'error');
        tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
        break;
      }

      switch( $multi_form ) {
        case 'multi_categories':
          if( !is_array($_POST['categories_id'])) break;
          foreach ($_POST['categories_id'] as $category_id=>$val) {
            $check_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              continue;
            } else {
              $sql_data_array = array(
                                      'abstract_zone_id' => (int)$this->m_zID,
                                      'categories_id' => (int)$category_id
                                     );
              tep_db_perform(TABLE_CATEGORIES_ZONES_TO_CATEGORIES, $sql_data_array);
            }
          }
          tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      for($i=0, $j=count($_POST['categories_id']); $i<$j; $i++ ) {
        $categories_id = $_POST['categories_id'][$i];
        tep_db_query("delete from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where abstract_zone_id = '" . (int)$this->m_zID . "' and categories_id = '" . (int)$categories_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function deleteconfirm_zone() {
      tep_db_query("delete from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      parent::deleteconfirm_zone();
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES));
    }

    function display_html() {
      switch( $this->m_action ) {
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_categories':
          $result = $this->display_multi_categories();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

// Categories/Products List
    function display_list() {
      global $languages_id;
      $categories_array = $this->get_category_tree_paths();
      $html_string = '';
      $rows = 0;
      $zones_query_raw = "select cz2c.categories_id, if(cz2c.categories_id, cd.categories_name, 'All Categories') as final_categories_name, cz2c.status_id, cz2c.sort_id from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " cz2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (cz2c.categories_id=cd.categories_id) where cd.language_id= '" . (int)$languages_id . "' and cz2c.abstract_zone_id = '" . (int)$this->m_zID . "' order by cz2c.status_id desc, cz2c.sort_id";
      $zones_split = new splitPageResults($this->m_spage, ABSTRACT_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows, 'cz2c.categories_id');
      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_ABSTRACT_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n";

        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl,\'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="center"><a href="javascript:void(0)" onClick="copy_radios(document.rl, \'status\')" title="Replicate Status from the first entry to subsequent entries" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_STATUS . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="center"><a href="javascript:void(0)" onClick="copy_inputs(document.rl, \'order\')" title="Replicate Order from the first entry to subsequent entries" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SORT . '</span></a></td>' . "\n" . 
        '          </tr>' . "\n";
        $zones_query = tep_db_query($zones_query_raw);
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $product_categories_string = $this->get_categories_string($zones['categories_id']);
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $zones['categories_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . (($zones['categories_id']) ? $product_categories_string : TEXT_ALL_CATEGORIES) . '</td>' . "\n" . 
          '            <td class="dataTableContent"><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
          '              <tr>'  . "\n" . 
          '                <td class="dataTableContent">On</td>' . "\n" . 
          '                <td class="dataTableContent">' . tep_draw_radio_field('status[' . $zones['categories_id'] . ']', '1', false, $zones['status_id']) . '</td>' . "\n" . 
          '                <td>' . tep_draw_separator('pixel_trans.gif', '4', '1') . '</td>' . "\n" . 
          '                <td class="dataTableContent">Off</td>' . "\n" . 
          '                <td class="dataTableContent">' . tep_draw_radio_field('status[' . $zones['categories_id'] . ']', '0', false, $zones['status_id']) . '</td>' . "\n" . 
          '              </tr>'  . "\n" . 
          '            </table></td>'  . "\n" . 
          '            <td class="dataTableContent" align="center">' . tep_draw_input_field('sort[' . $zones['categories_id'] . ']', $zones['sort_id'], 'style="width: 50px"') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, ABSTRACT_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_CATEGORIES_PRODUCTS) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <td nowrap><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_categories') . '">' . tep_image_button('button_categories.gif', TEXT_SWITCH_CATEGORIES) . '</a></td>' . "\n" . 
        '              <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '              </table></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }

    function display_multi_categories() {
      global $languages_id;

      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTICATEGORIES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_categories', 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mc,\'categories_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      $categories_query_raw = "select cd.categories_id, cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . (int)$languages_id . "' order by cd.categories_name";
      $categories_split = new splitPageResults($this->m_mcpage, ABSTRACT_PAGE_SPLIT, $categories_query_raw, $zones_query_numrows, 'cd.categories_id');
      $categories_query = tep_db_query($categories_query_raw);
      $bCheck = false;
      while ($categories = tep_db_fetch_array($categories_query)) {
        $product_categories_string = $this->get_categories_string($categories['categories_id']);
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . tep_draw_checkbox_field('categories_id[' . $categories['categories_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $product_categories_string . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_categories') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $categories_split->display_count($zones_query_numrows, ABSTRACT_PAGE_SPLIT, $this->m_mcpage, TEXT_DISPLAY_NUMBER_OF_CATEGORIES_PRODUCTS) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $categories_split->display_links($zones_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mcpage, tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_categories', 'mcpage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }


    function display_delete_multizone() {
      global $languages_id;
      $html_string = '';
      $attr_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      $attr_array = tep_db_fetch_array($attr_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $attr_array['abstract_zone_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";
      $rows = 0;
      foreach ($_POST['pc_id'] as $categories_id => $val) {
        $delete_query = tep_db_query("select cz2c.categories_id, if(cz2c.categories_id, cd.categories_name, 'All Categories') as final_categories_name, cz2c.status_id, cz2c.sort_id from " . TABLE_CATEGORIES_ZONES_TO_CATEGORIES . " cz2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (cz2c.categories_id=cd.categories_id) where cz2c.categories_id = '" . (int)$categories_id . "' and cd.language_id= '" . (int)$languages_id . "' and cz2c.abstract_zone_id = '" . (int)$this->m_zID . "' order by cz2c.status_id desc, cz2c.sort_id");
        if( $delete_array = tep_db_fetch_array($delete_query) ) {
          $product_categories_string = $this->get_categories_string($delete_array['categories_id']);
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_hidden_field('categories_id[]', $delete_array['categories_id']) . $product_categories_string . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_hidden_field('products_id[]', $delete_array['products_id']) . $delete_array['final_products_name'] . '</td>' . "\n" . 
          '          </tr>' . "\n";
        }
      }
      if( count($_POST['pc_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="4"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }
  }
?>