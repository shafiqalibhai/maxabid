<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// PDF Order Invoice Custom Class
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('PDF_TO_MM_FACTOR',0.2626);
define('PDF_MAX_IMAGE_WIDTH',400);

  class pdf_order_invoice extends FPDF {
    var $headerset, $footerset, $columns_fits_array, $columns_array, $columns_align_array, $order_title;

    function pdf_order_invoice($orientation='P',$unit='mm',$format='A4') {
      parent::FPDF($orientation, $unit, $format);
      $this->reset();
    }


    function _beginpage($orientation) {
      $this->page++;
      if(!isset($this->pages[$this->page]) ) // solved the problem of overwriting a page, if it already exists
        $this->pages[$this->page]='';
      $this->state=2;
      $this->x=$this->lMargin;
      $this->y=$this->tMargin;
      $this->lasth=0;
      $this->FontFamily='Arial';
      //Page orientation
      if(!$orientation) {
        $orientation=$this->DefOrientation;
      } else {
        $orientation=strtoupper($orientation{0});
        if($orientation!=$this->DefOrientation)
          $this->OrientationChanges[$this->page]=true;
      }
      if($orientation!=$this->CurOrientation) {
        //Change orientation
        if($orientation=='P') {
          $this->wPt=$this->fwPt;
          $this->hPt=$this->fhPt;
          $this->w=$this->fw;
          $this->h=$this->fh;
        } else {
          $this->wPt=$this->fhPt;
          $this->hPt=$this->fwPt;
          $this->w=$this->fh;
          $this->h=$this->fw;
        }
        $this->PageBreakTrigger=$this->h-$this->bMargin;
        $this->CurOrientation=$orientation;
      }
    }


    function Header() {
      //$date = strftime('%A, %d %B %Y');
      $date = gmdate("F jS Y");
      $this->SetY($this->tMargin);
      //Store Name
      $this->SetFont('helvetica','B',18);
      $this->SetLineWidth(0);
      //$w=$this->GetStringWidth(STORE_NAME)+6;
      $w=$this->GetStringWidth($this->title)+6;
      $this->SetTextColor(0x3F, 0x3F, 0x3F);
      //$this->Cell($w,8,STORE_NAME,0,0,'C');
      $this->Cell($w,8,$this->title,0,0,'C');

      $this->SetY($this->y+22);
    }

    function Footer() {
      // Check if footer for this page already exists
      if(!isset($this->footerset[$this->page]) || !$this->footerset[$this->page]) {
        //$this->SetY(-15);
        $this->SetY(-$this->bMargin+$this->GetLineHeight() );
        //Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        //$x = $this->GetX();
        $y = $this->GetY();
        $this->Line($this->lMargin,$y, $this->w-$this->rMargin, $y);
        $this->SetFont('Verdana','B',10);
        $this->SetY($y+2);
        $this->SetFillColor(0xF0, 0xFF, 0xF0);
        $this->SetTextColor(0x3F, 0x3F, 0x3F);
        //$date = strftime(DATE_FORMAT_LONG);
        $date = gmdate("F jS Y");

        $this->Cell(0, $this->GetLineHeight(), STORE_NAME . ' - ' . 'Invoice File Generated on: ' . $date . ' ',0,0,'L',1);
        $this->Cell(0, $this->GetLineHeight(), 'Page: '.$this->PageNo().'/{nb}',0,0,'R',1);
        // set footerset
        $this->footerset[$this->page] = 1;
        $this->SetFont('Verdana','',$this->FontSizePt);
        $this->SetTextColor(0, 0, 0);
      }
    }

    function set_order_title($title) {
      $this->order_title = $title;
    }

    function invoice_order( $data_array ) {
      $this->setY($this->tMargin);
      $this->AddPage();

      $startpage = $currpage = $maxpage = $this->page;
      $this->SetFont('Arial','B',12);
      $this->SetTextColor(0, 0, 0);
      $l = $this->lMargin;
      $h = $this->GetY();

      $this->Line($l,$h,$this->ehw+$l,$h);
      $this->SetY($this->y+$this->FontSizePt);

      foreach($data_array as $key => $value ) {
        if( $key == 'order_title' )
          $this->set_order_title($value);
        $this->MultiCell($this->ehw, $this->FontSizePt+2, $value, 0, 'L');
      }
      $this->Ln();
      if($this->page > $maxpage)
        $maxpage = $this->page;

      $this->page = $maxpage;

//      $this->Line($l,$h,$fullwidth+$l,$h);
    }

    function draw_products_cols() {
      $this->SetFont('Arial','B',$this->FontSizePt);
      $this->SetTextColor(0, 0, 0x3F);
      $this->SetFillColor(0xF0, 0xFF, 0xF0);

      $col_count = count($this->columns_array);
      $line_height = $this->GetLineHeight()+4;
      $lOffset = $this->lMargin;
      $h = $this->GetY();
      $lh = $h + (2*$this->y+$line_height)/3;

      //$this->Line($lOffset,$h,$this->ehw+$lOffset,$h);

      $this->SetY($this->y+$line_height/3);
      //$this->Line($lOffset,$h,$lOffset,$lh);

      for($i=0, $j=count($this->columns_array); $i<$j; $i++) {
        $this->SetXY($lOffset, $h);
        $width = $this->ehw*((int)$this->columns_fits_array[$i]/100);
        $this->column_fits_array[$i] = $width;
        $this->MultiCell($this->column_fits_array[$i], $line_height, $this->columns_array[$i], 1, $this->columns_align_array[$i], 1);
        $lOffset += $this->column_fits_array[$i];
        //$this->Line($lOffset,$h,$lOffset,$lh);
      }
      $h = $this->GetY();
      $this->Line($this->lMargin,$h,$this->ehw+$this->lMargin,$h);
      $this->SetXY($this->lMargin, $h+1);
    }

    function invoice_products_cols($columns) {
      $this->columns_array = array();
      $this->columns_fits_array = array();
      $this->columns_align_array = array();

      $this->columns_array = array_values($columns['data']);
      $this->columns_fits_array = array_values($columns['width']);
      $this->columns_align_array = array_values($columns['align']);

      $this->draw_products_cols();
    }

    function invoice_products( $data_array ) {
      $this->SetFont('Arial','',$this->FontSizePt);
      $this->SetTextColor(0x00, 0x00, 0x00);
      $this->SetFillColor(0xFF, 0xFF, 0xFF);

      $lOffset = $this->lMargin;
      $line_height = $this->GetLineHeight()+4;
      $multiplier = 1;

      $i = 0;
      foreach($data_array as $index => $content ) {
        if( $index == 'image' ) {
          $image = HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $content;
          $xy = $this->calculate_image_space($image);
          $image_lines = ($xy[1]/$line_height)+1;
          if( $multiplier < $image_lines ) {
            $multiplier = $image_lines;
          }
        } else {

          $cell_width = $this->GetStringWidth($content, true);
          $sub_lines = explode("\r\n", $content);
          if( $multiplier < count($sub_lines) ) {
            $multiplier = count($sub_lines);
          }
/*
          if( ($cell_width/$multiplier) > $this->column_fits_array[$i] && $multiplier < ($cell_width/$this->column_fits_array[$i]) ) {
            $multiplier = $cell_width/$this->column_fits_array[$i];
          }
*/
        }
        $i++;
      }

      $cell_height = $multiplier*$line_height;

      if($this->y+$cell_height>$this->PageBreakTrigger) {
        $this->setY($this->tMargin);
        $this->AddPage();
        $l = $this->lMargin;
        $h = $this->GetY();
        $this->Line($l,$h,$this->ehw+$l,$h);
        $this->SetY($this->y+$this->FontSizePt);
        $this->Ln();
        $content = $this->order_title . ' - Continues from page ' . ($this->PageNo()-1);
        $this->SetFont('Arial','B',$this->FontSizePt);
        $this->SetTextColor(0xEF, 0x00, 0x00);
        $this->SetFillColor(0xFF, 0xFF, 0xFF);
//        $this->MultiCell($this->ehw, $line_height, $content, 0, $this->columns_align_array[$i], 0);
        $this->MultiCell($this->ehw, $line_height, $content, 0, 'L', 0);
        $this->Line($l,$h,$this->ehw+$l,$h);
        $this->SetY($this->y+$this->FontSizePt);
        $this->draw_products_cols();
        $this->SetFont('Arial','',$this->FontSizePt);
        $this->SetTextColor(0x00, 0x00, 0x00);
        $this->SetFillColor(0xFF, 0xFF, 0xFF);
      }

      $h = $this->GetY();
      $lh = $h + $cell_height;

      $i=0;
      foreach($data_array as $index => $content ) {
        $this->SetXY($lOffset, $h);

        if( $index == 'image' ) {
          $image = HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $content;
          $xy = $this->calculate_image_space($image);
          $content = '';
        } elseif($index == 'quantity' && $content > 1) {
          $this->SetFont('Arial','B',$this->FontSizePt);
          $this->SetTextColor(0xEF, 0x00, 0x00);
          $this->SetFillColor(0xFF, 0xEF, 0xEF);
        }

        $this->MultiCell($this->column_fits_array[$i], $line_height, $content, 0, $this->columns_align_array[$i], 1, $cell_height);
        if( $index == 'image' ) {
          $this->Image($image, $lOffset+$this->column_fits_array[$i]/4, $h+4, $xy[0], $xy[1]);
        } elseif($index == 'quantity' && $content > 1) {
          $this->SetFont('Arial','',$this->FontSizePt);
          $this->SetTextColor(0x00, 0x00, 0x00);
          $this->SetFillColor(0xFF, 0xFF, 0xFF);
        }
        $this->Line($lOffset,$h,$lOffset,$lh);

        $lOffset += $this->column_fits_array[$i];
        $i++;

        $this->Line($lOffset,$h,$lOffset,$lh);
      }

      $h = $this->GetY();
      $this->Line($this->lMargin,$h,$this->ehw+$this->lMargin,$h);
      $this->SetXY($this->lMargin, $h+1);
    }

    function reset() {
      $this->footerset = array();
      $this->columns_array = array();
      $this->columns_fits_array = array();
      $this->columns_align_array = array();
    }

    function calculate_image_space($image) {
      $image = str_replace(' ', '%20', $image);
      $heightwidth=@getimagesize($image);

      $data_array = array();
      if( !isset($heightwidth[0]) || !isset($heightwidth[1]) ) {
        $data_array[0]=PDF_MAX_IMAGE_WIDTH;
        $data_array[1]=PDF_MAX_IMAGE_WIDTH;
        return $data_array;
      }       

      $factor = $heightwidth[0]/$heightwidth[1];

      if( $heightwidth[0] > PDF_MAX_IMAGE_WIDTH || $heightwidth[1] > PDF_MAX_IMAGE_WIDTH ) {
        $rx = $heightwidth[0] / PDF_MAX_IMAGE_WIDTH; 
        $ry = $heightwidth[1] / PDF_MAX_IMAGE_WIDTH; 
        $adjust = PDF_MAX_IMAGE_WIDTH*PDF_TO_MM_FACTOR;
        if ($rx < $ry) { 
          $data_array[0] = intval($adjust * $factor);
          $data_array[1] = $adjust;
        } else { 
          $data_array[0] = $adjust;
          $data_array[1] = intval($adjust / $factor); 
        } 
      } else {
        $data_array[0]=$heightwidth[1]*PDF_TO_MM_FACTOR;
        $data_array[1]=$data_array[0]/$factor;
      }
      return $data_array;
    }
  }
?>