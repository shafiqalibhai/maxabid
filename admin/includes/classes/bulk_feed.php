<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Google Bulk Feed Class for Admin.
// Support class to generate bulk feed from the database recorde
// - Added progressive buildup controls (10/18/2007)
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class bulk_feed extends xml_core {
    function bulk_feed($type='rss20', $insert_header=true) {
      $head_count = $loc_count = 0;
      parent::xml_core();

      if( !$insert_header ) return;

      $this->insert_raw_entry('<?xml version="1.0" encoding="UTF-8"?>');
      $header = '';
      switch ($type) {
        case 'rss20':
          $header = '<rss version="2.0"';
          break;
        default:
          $header = '<rss version="2.0"';
          break;
      }
      $header .= ' xmlns:g="http://base.google.com/ns/1.0">';
      $this->insert_raw_entry($header);
    }

    function build_channel($title, $desc, $link=HTTP_CATALOG_SERVER, $channel='') {
      if( tep_not_null($channel)) 
        $this->insert_entry('channel ' . $channel);
      else 
        $this->insert_entry('channel');

      $this->insert_closed_entry('title', $title);
      $this->insert_closed_entry('description', $desc);
      $this->insert_closed_entry('link', $link);
    }

    function build_products($next=0, $limit=1000, &$result_array, &$max_limit) {
      global $currency;
      if( !$limit ) {
        $limit = '';
      } else {
        $limit = ' limit ' . $limit;
      }

      $products_query = tep_db_query("select mp.products_id, mp.meta_title, mp.meta_text, p.products_price, p.products_quantity, p.products_image, s2p.seo_name from " . TABLE_META_PRODUCTS . " mp left join " . TABLE_SEO_TO_PRODUCTS . " s2p on (s2p.products_id=mp.products_id) left join " . TABLE_PRODUCTS . " p on (p.products_id=mp.products_id) where p.products_id > '" . (int)$next . "' and p.products_display='1' order by p.products_id" . $limit);
      $max_limit = tep_db_num_rows($products_query);
      $result = 0;
      while($products_array = tep_db_fetch_array($products_query) ) {
        $check_query = tep_db_query("select seo_url_get, date_added from " . TABLE_SEO_URL . " where seo_url_org like '%product_info.php%' and seo_url_get like '%" . tep_db_input(tep_db_prepare_input($products_array['seo_name'])) . ".%' limit 1");
        if( $check_array = tep_db_fetch_array($check_query) ) {

          $specials_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$products_array['products_id'] . "'");
          if($specials_array = tep_db_fetch_array($specials_query) ) {
            $products_array['products_price'] = $specials_array['specials_new_products_price'];
          }

          $this->insert_entry('item');
          $this->insert_closed_entry('title', htmlspecialchars(utf8_encode($products_array['meta_title'])));
          $this->insert_closed_entry('description', htmlspecialchars(utf8_encode($products_array['meta_text'])));
          $this->insert_closed_entry('guid', $products_array['products_id']);
          $this->insert_closed_entry('g:quantity', $products_array['products_quantity']);
          $this->insert_closed_entry('g:price', $products_array['products_price']);
          $this->insert_closed_entry('g:currency', DEFAULT_CURRENCY);
          if( tep_not_null($products_array['products_image']) ) {
            $this->insert_closed_entry('g:image_link', HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $products_array['products_image']);
          }
          $this->insert_closed_entry('g:condition', 'new');
          $this->insert_closed_entry('link', $check_array['seo_url_get']);
          $this->insert_entry('item', true);
          $result_array[] = $check_array;
        }
        $result = $products_array['products_id'];
      }
      return $result;
    }

    function close_channel() {
       $this->insert_entry('channel', true);
    }

    function close_feed() {
       $this->insert_entry('rss', true);
    }
  }
?>