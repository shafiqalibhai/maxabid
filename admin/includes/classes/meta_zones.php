<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// MEATA-G Zones root class for osCommerce Admin
// Controls relationships among products, categories, customers etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class meta_zones {
   var $m_zID, $m_zpage, $m_saction, $m_action, $m_zInfo, $m_sID, $m_spage;
// class constructor
    function meta_zones() {
      $this->m_zID = isset($_GET['zID'])?$_GET['zID']:'';
      $this->m_zpage = isset($_GET['zpage'])?$_GET['zpage']:'';
      $this->m_saction = isset($_GET['saction'])?$_GET['saction']:'';
      $this->m_action = isset($_GET['action'])?$_GET['action']:'';
      $this->m_sID = isset($_GET['sID'])?$_GET['sID']:'';
      $this->m_spage = isset($_GET['spage'])?$_GET['spage']:'';
    }

    function validate_array_selection($entity, $action='list') {
      global $messageStack;
      if( !isset($_POST[$entity]) || !is_array($_POST[$entity]) || !count($_POST[$entity]) ) {
        $messageStack->add_session(WARNING_NOTHING_SELECTED, 'warning');
        tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=' . $action));
      }
    }

    function create_safe_string($string, $separator=META_DEFAULT_WORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }

      //$string = preg_replace('/\s\s+/', ' ', trim($string));
      $string = trim($string);
	  $string = preg_replace("/[^0-9a-z\-\$]+/i", $separator, strtolower($string));
      $string = trim($string, $separator);
      $string = str_replace($separator . $separator . $separator, $separator, $string);
      $string = str_replace($separator . $separator, $separator, $string);
      if(META_DEFAULT_WORD_LENGTH > 1) {
        $words_array = explode($separator, $string);
        if( is_array($words_array) ) {
          for($i=0, $j=count($words_array); $i<$j; $i++) {
            if(strlen($words_array[$i]) < META_DEFAULT_WORD_LENGTH) {
              unset($words_array[$i]);
            }
          }
          if(count($words_array))
            $string = implode($separator, $words_array);
        }
      }
      return $string;
    }

    function create_keywords_array($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }
      $string = str_replace(META_DEFAULT_WORDS_SEPARATOR, $separator, $string);
      $string = $this->create_safe_string($string, $separator);
      $keywords_array = explode($separator, $string, META_MAX_KEYWORDS);
      return $keywords_array;
    }

    function create_keywords_string($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }

      $string = $this->strip_all_tags($string);
      $keywords_array = $this->create_keywords_array($string, $separator);
      $string = implode(',',$keywords_array);
      return $string;
    }

    function create_keywords_lexico($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }

      $string = $this->strip_all_tags($string);
      $string = $this->create_safe_string($string);
      $phrases_array = explode($separator, $string);
      $keywords_array = array();
      $index = 0;
      foreach($phrases_array as $key => $value) {
        if( $index > META_MAX_KEYWORDS) break;
        if( is_numeric($value) ) continue;
        if( strlen($value) <= META_DEFAULT_WORD_LENGTH) continue;
        $check_query = tep_db_query("select meta_exclude_key from " . TABLE_META_EXCLUDE . " where meta_exclude_text like '%" . tep_db_input($value) . "%' and meta_exclude_status='1'");
        if( tep_db_num_rows($check_query) ) continue;

        $process = false;
        if( META_USE_LEXICO == 'true' ) {
          $check_query = tep_db_query("select meta_lexico_key as id, meta_lexico_text as text from " . TABLE_META_LEXICO . " where meta_lexico_text like '%" . tep_db_input(tep_db_prepare_input($value)) . "%' and meta_lexico_status='1' order by sort_id");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $tmp_string = $this->create_safe_string($check_array['text']);
            $md5_key = md5($tmp_string);
            $keywords_array[$check_array['id']] = $tmp_string;
            $process = true;
            $index++;
          }
        }
        if( !$process && META_USE_GENERATOR == 'true') {
          $tmp_string = $this->create_safe_string($value);
          $md5_key = md5($tmp_string);
          $keywords_array[$md5_key] = $tmp_string;
          $index++;
        }
      }
      $string = implode(',',$keywords_array);
      return $string;
    }

    function create_safe_description($string, $max_length = META_MAX_DESCRIPTION, $extension = '') {
      $string = $this->strip_all_tags($string);
      $string = $this->truncate_string($string, $max_length, $extension);
      return $string;
    }

// $string        = "Truncates a string at a certain position without &raquo;cutting&laquo; words into senseless pieces.";
// $maxlength    = 75;
// $extension    = " ...";
// This Will output:
// "Truncates a string at a certain position without ?cutting? ..."
// Usage: echo truncate_string ($string, $maxlength, $extension);
    function truncate_string($string, $maxlength=META_MAX_DESCRIPTION, $extension='', $cutmarker = '.') {
      // Set the replacement for the "string break" in the wordwrap function
      //$cutmarker = "**cut_here**";

      // Checking if the given string is longer than $maxlength
      if (strlen($string) > $maxlength) {
        // Using wordwrap() to set the cutmarker
        // NOTE: wordwrap (PHP 4 >= 4.0.2, PHP 5)
        $string = wordwrap($string, $maxlength, $cutmarker);

        // Exploding the string at the cutmarker, set by wordwrap()
        $string = explode($cutmarker, $string);

        // Adding $extension to the first value of the array $string, returned by explode()
        $string = $string[0] . $extension;
      }
      // returning $string
      return $string;
    }


    function strip_all_tags($string) {
      // tam have number of cars the string
      $tam=strlen($string);

      // newstring will be returned
      $newstring="";
      $tag=0;
      /* tag = 0 => copy car from string to newstring
         tag > 0 => don't copy. Find one or mor tag '<' and
            need to find '>'. If we find 3 '<' need to find
            all 3 '>'
      */

      /* I am C programm. seek in a string is natural for me
          and more efficient

          Problem: copy a string to another string is more
          efficient but use more memory!!!
      */
      for ($i=0; $i < $tam; $i++) {

        /* If I find one '<', $tag++ and continue whithout copy*/
        if ($string{$i} == '<'){
            $tag++;
            continue;
        }

        /* if I find '>', decrease $tag and continue */
        if ($string{$i} == '>') {
          if ($tag){
            $tag--;
          }
        /* $tag never be negative. If string is "<b>test</b>>" (error, of course)
            $tag stop in 0
        */
          continue;
        }

        /* if $tag is 0, can copy */
        if ($tag == 0){
          $newstring .= $string{$i}; // simple copy, only car
        }
      }
      return $newstring;
    } 


    function generate($offset=0) {
      return false;
/*
      if($offset > 0 ) {

      }

      $offset += META_SPLIT_GENERATE;
      $params = 'generate_id=' . $offset;
      return $params;
*/
    }

    function get_category_path($categories_id) {
      $categories = array();
      $categories[] = $categories_id;
      $this->get_parent_categories($categories, $categories_id);
      $categories = array_reverse($categories);
      $path = implode('_', $categories);
      if (substr($path, 0, 1) == '_') {
        $path = substr($path, 1);
      }
      return $path;
    }

//-MS- ported from catalog\includes\functions\general.php
// Get path
////
// Recursively go through the categories and retreive all parent categories IDs
// TABLES: categories
    function get_parent_categories(&$categories, $categories_id) {
      $parent_categories_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
      while ($parent_categories = tep_db_fetch_array($parent_categories_query)) {
        if ($parent_categories['parent_id'] == 0) return true;
        $categories[sizeof($categories)] = $parent_categories['parent_id'];
        if ($parent_categories['parent_id'] != $categories_id) {
          $this->get_parent_categories($categories, $parent_categories['parent_id']);
        }
      }
    }

//-MS- ported from catalog\admin\includes\functions\general.php
// Modified path generation
    function generate_category_path($id, $from = 'category', $categories_array = '', $index = 0, $force_top = true) {
      global $languages_id;
      if (!is_array($categories_array)) $categories_array = array();

      if ($from == 'product') {
        $categories_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$id . "' order by categories_id");
        while ($categories = tep_db_fetch_array($categories_query)) {
          if ($categories['categories_id'] == '0') {
            if( $force_top ) {
              $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
            }
          } else {
            $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where c.categories_id = '" . (int)$categories['categories_id'] . "' and cd.language_id = '" . (int)$languages_id . "'");
            $category = tep_db_fetch_array($category_query);
            if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) 
              $categories_array = $this->generate_category_path($category['parent_id'], 'category', $categories_array, $index, $force_top);
            $categories_array[$index][] = array('id' => $categories['categories_id'], 'text' => $category['categories_name']);
          }
          $index++;
        }
      } elseif ($from == 'category') {

        $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where c.categories_id = '" . (int)$id . "' and cd.language_id = '" . (int)$languages_id . "'");
        $category = tep_db_fetch_array($category_query);

        if( (int)$category['parent_id'] != 0 ) 
          $categories_array = $this->generate_category_path($category['parent_id'], 'category', $categories_array, $index, $force_top);

        if( $force_top || ( tep_not_null($id) && tep_not_null($category['categories_name']) ) ) {
          $categories_array[$index][] = array('id' => $id, 'text' => $category['categories_name']);
        }

      }
      return $categories_array;
    }
//-MS- Modified path generation EOM

//-MS- ported from catalog\admin\includes\functions\articles.php
// Modified topic generation
    function generate_topic_path($id, $from = 'topic', $topics_array = '', $index = 0, $force_top=true) {
      global $languages_id;

      if (!is_array($topics_array)) $topics_array = array();

      if ($from == 'article') {
        $topics_query = tep_db_query("select topics_id from " . TABLE_ARTICLES_TO_TOPICS . " where articles_id = '" . (int)$id . "' order by topics_id");
        while ($topics = tep_db_fetch_array($topics_query)) {
          if($topics['topics_id'] == '0') {
            if( $force_top ) {
              $topics_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
            }
          } else {
            $topic_query = tep_db_query("select cd.topics_name, c.parent_id from " . TABLE_TOPICS . " c left join " . TABLE_TOPICS_DESCRIPTION . " cd on (c.topics_id=cd.topics_id)  where c.topics_id = '" . (int)$topics['topics_id'] . "' and c.topics_id = cd.topics_id and cd.language_id = '" . (int)$languages_id . "'");
            $topic = tep_db_fetch_array($topic_query);
            if ( (tep_not_null($topic['parent_id'])) && ($topic['parent_id'] != '0') ) 
              $topics_array = $this->generate_topic_path($topic['parent_id'], 'topic', $topics_array, $index, $force_top);
            $topics_array[$index][] = array('id' => $topics['topics_id'], 'text' => $topic['topics_name']);
          }
          $index++;
        }
      } elseif ($from == 'topic') {
        $topic_query = tep_db_query("select cd.topics_name, c.parent_id from " . TABLE_TOPICS . " c left join " . TABLE_TOPICS_DESCRIPTION . " cd on (c.topics_id=cd.topics_id) where c.topics_id = '" . (int)$id . "' and cd.language_id = '" . (int)$languages_id . "'");
        $topic = tep_db_fetch_array($topic_query);

        if( (int)$topic['parent_id'] != 0 ) 
          $topics_array = $this->generate_topic_path($topic['parent_id'], 'topic', $topics_array, $index, $force_top);

        if( $force_top || ( tep_not_null($id) && tep_not_null($topic['topics_name']) ) ) {
          $topics_array[$index][] = array('id' => $id, 'text' => $topic['topics_name']);
        }
      }
      return $topics_array;
    }
//-MS- Modified topic generation EOM

    function process_action() {
      switch( $this->m_action ) {
        default:
          break;
      }
    }

    function process_saction() {
      switch( $this->m_saction ) {
        default:
          break;
      }
    }

    function display_html() {
      $html_string = '';

      if (!$this->m_action) {
        $html_string = $this->display_default();
      }
      return $html_string;
    }

    function display_default() {
      $html_string = '';
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_META_ZONES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent" align="right">' . TABLE_HEADING_ACTION . '&nbsp;</td>' . "\n" . 
      '            </tr>' . "\n";
      $zones_query_raw = "select at.meta_types_id, at.meta_types_name, at.meta_types_class from " . TABLE_META_TYPES . " at where meta_types_status='1' order by at.sort_order";
      $zones_split = new splitPageResults($this->m_zpage, META_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows);
      $zones_query = tep_db_query($zones_query_raw);
      while ($zones = tep_db_fetch_array($zones_query)) {

        if( (!tep_not_null($this->m_zID) || (tep_not_null($this->m_zID) && ($this->m_zID == $zones['meta_types_id']))) && !isset($this->m_zInfo) && (substr($this->m_action, 0, 3) != 'new')) {
          $this->m_zInfo = new objectInfo($zones);
          $this->m_zID = $this->m_zInfo->meta_types_id;
        }
        if (isset($this->m_zInfo) && is_object($this->m_zInfo) && ($zones['meta_types_id'] == $this->m_zInfo->meta_types_id)) {
          $html_string .= 
          '          <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->meta_types_id . '&action=list') . '\'">' . "\n";
        } else {
          $html_string .= 
          '          <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['meta_types_id']) . '\'">' . "\n";
        }
        $html_string .= 
        '              <td class="dataTableContent"><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['meta_types_id'] . '&action=list') . '">' . tep_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>&nbsp;' . $zones['meta_types_name'] . '</td>' . "\n" . 
        '              <td class="dataTableContent" align="right">';
        if (isset($this->m_zInfo) && is_object($this->m_zInfo) && ($zones['meta_types_id'] == $this->m_zInfo->meta_types_id) && tep_not_null($this->m_zID) ) { 
          $html_string .= tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); 
        } else { 
          $html_string .= '<a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['meta_types_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
        } 
        $html_string .= '&nbsp;</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td class="smallText">' . $zones_split->display_count($zones_query_numrows, META_PAGE_SPLIT, $this->m_zpage, TEXT_DISPLAY_NUMBER_OF_META_ZONES) . '</td>' . "\n" . 
      '                    <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, META_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_zpage, '', 'zpage') . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }

    function display_right_box() {
      $html_string = '';
      switch( $this->m_action ) {
        case 'list':
          break;
        default:
          if (isset($this->m_zInfo) && is_object($this->m_zInfo) && tep_not_null($this->m_zID) ) {
            $heading[] = array('text' => '<b>' . $this->m_zInfo->meta_types_name . '</b>');

            //$contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->meta_types_id . '&action=validate') . '">' . tep_image_button('button_validate.gif', 'Validate Entries for this type') . '</a> <a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->meta_types_id . '&action=list') . '">' . tep_image_button('button_details.gif', IMAGE_DETAILS) . '</a>');
            $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->meta_types_id . '&action=list') . '">' . tep_image_button('button_details.gif', IMAGE_DETAILS) . '</a>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_TYPE . '<br><b>' . $this->m_zInfo->meta_types_name . '</b>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_CLASS . '<br><b>' . $this->m_zInfo->meta_types_class . '.php</b>');
          }
          break;
      }

      if( isset($heading) && isset($contents) && tep_not_null($heading) && tep_not_null($contents) ) {
        $html_string .= '            <td width="25%" valign="top">' . "\n";
        $box = new box;
        $html_string .= $box->infoBox($heading, $contents);
        $html_string .= '            </td>' . "\n";
      }
      return $html_string;
    }

    function display_bottom() {
       return '';
    }
  }
?>
