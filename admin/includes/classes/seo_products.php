<?php
/*
//----------------------------------------------------------------------------
//-------------- SEO-G by Asymmetrics (Renegade Edition) ---------------------
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// SEO-G Products class for Admin
// This is a Bridge for SEO-G
// Processes products generates products seo urls.
// Featuring:
// - Products Listings SEO URLs
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class seo_products extends seo_zones {
    var $error_array;

// class constructor
    function seo_products() {
      $this->m_ssID = isset($_GET['ssID'])?$_GET['ssID']:'';
      $this->m_mcpage = isset($_GET['mcpage'])?$_GET['mcpage']:'';
      $this->m_mppage = isset($_GET['mppage'])?$_GET['mppage']:'';
      parent::seo_zones();
    }

    function generate_name($products_id, $separator=SEO_DEFAULT_WORDS_SEPARATOR) {
      global $languages_id;

      $string = '';
      $name_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$products_id . "' and language_id = '" . (int)$languages_id . "'");
      if( !tep_db_num_rows($name_query) )
        return $string;

      $names_array = tep_db_fetch_array($name_query);
      $string =  $this->create_safe_string($names_array['products_name'], $separator);
      return $this->adapt_lexico($string, $separator);
    }

    function get_categories_string($categories_id) {
      $product_categories = $this->generate_category_path($categories_id, 'category', '', 0, false);
      $product_categories_string = '';
      for ($n = sizeof($product_categories), $i=$n-1; $i >= 0; $i--) {
        $category_path = '';
        for ($k = sizeof($product_categories[$i]), $j=$k-1; $j >= 0; $j--) {
          $category_path .= '<b>' . $product_categories[$i][$j]['text'] . '</b>&nbsp;&laquo;&nbsp;';
        }
        $product_categories_string .= $category_path;
      }

      if( strlen($product_categories_string) > 19 )
        $product_categories_string = substr($product_categories_string, 0, -19);
      return $product_categories_string;
    }

    function get_category_tree_paths() {
      global $languages_id;
      //$categories_query = tep_db_query("select distinct p2c.categories_id as id, cd.categories_name as text from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (cd.categories_id=p2c.categories_id) where cd.language_id = '" . (int)$languages_id . "' order by p2c.categories_id, cd.categories_name");
      $categories_query = tep_db_query("select categories_id as id, categories_name as text from " . TABLE_CATEGORIES_DESCRIPTION . " where language_id = '" . (int)$languages_id . "' order by categories_name");
      $categories_array = array();

      while( $categories = tep_db_fetch_array($categories_query) ) {
        $product_categories = $this->generate_category_path($categories['id'], 'category', '', 0, false);
        $product_categories_string = '';
        for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
          $category_path = '';
          for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
            $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&raquo;&nbsp;';
          }
          $product_categories_string .= $category_path;
        }
        $product_categories_string = substr($product_categories_string, 0, -19);
        $categories_array[$product_categories_string] = array(
                                   'id' => $categories['id'],
                                   'text' => $product_categories_string
                                 );
      }
      ksort($categories_array, SORT_STRING);
      $categories_array = array_values($categories_array);
      return $categories_array;
    }

    function process_action() {
      switch( $this->m_action ) {
        case 'validate':
          return $this->validate();
        case 'validate_confirm':
          return $this->validate_confirm();
        case 'update_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->update_multizone();
        case 'multi_products':
          return $this->multi_products();
        case 'multi_categories':
          return $this->multi_categories();
        case 'deleteconfirm_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->deleteconfirm_multizone();
        case 'delete_multizone':
          $result = parent::validate_array_selection('pc_id'); 
        default:
          return parent::process_action(); 
          break;
      }
    }
/*
    function validate() {
      global $languages_id;
      $this->error_array = array();

      // First pass check for redundant entries in the seo table
      for($pos=1;; $pos++) {
        $seo_query_raw = "select products_id, seo_name as name from " . TABLE_SEO_TO_PRODUCTS . " order by products_id";
        $seo_split = new splitPageResults($pos, SEO_PAGE_SPLIT, $seo_query_raw, $seo_query_numrows);
        if(!$seo_query_numrows) break;

        $total_items = array();
        tep_query_to_array($seo_query_raw, $total_items);
        for($i=0, $j=count($total_items); $i<$j; $i++ ) {
          $check_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$total_items[$i]['products_id'] . "' and language_id = '" . (int)$languages_id . "'");
          if( !tep_db_num_rows($check_query) ) {
            $total_items[$i]['missing_id'] = -1;
            $this->error_array[] = $total_items[$i];
          }
        }
        if( count($this->error_array) >= SEO_PAGE_SPLIT ) {
          return $this->error_array;
        }
      }

      // Second pass check for missing products from seo table
      for($pos=1;; $pos++) {
        $products_query_raw = "select products_id, products_name as name from " . TABLE_PRODUCTS_DESCRIPTION . " where language_id = '" . (int)$languages_id . "'";
        $products_split = new splitPageResults($pos, SEO_PAGE_SPLIT, $products_query_raw, $products_query_numrows);
        if(!$products_query_numrows) break;

        $total_items = array();
        tep_query_to_array($products_query_raw, $total_items);
        for($i=0, $j=count($total_items); $i<$j; $i++ ) {
          $check_query = tep_db_query("select products_id from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$total_items[$i]['products_id'] . "'");
          if( !tep_db_num_rows($check_query) ) {
            $total_items[$i]['missing_id'] = 0;
            $this->error_array[] = $total_items[$i];
          }
        }
        if( count($this->error_array) >= SEO_PAGE_SPLIT ) {
          return $this->error_array;
        }
      }
      return $this->error_array;
    }
*/


/*
    function validate() {
      global $languages_id;
      $this->error_array = array();
      // First pass check for missing products from seo table
      //$check_query = tep_db_query("select pd.products_id, pd.products_name as name, if(s2p.products_id, s2p.products_id, 0) as missing_id from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_SEO_TO_PRODUCTS . " s2p on ((s2p.products_id = if(s2p.products_id,pd.products_id,0))) where pd.language_id = '" . (int)$languages_id . "' order by pd.products_id desc");
      $check_query = tep_db_query("select pd.products_id, pd.products_name as name, if(s2p.products_id, s2p.products_id, 0) as missing_id from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_SEO_TO_PRODUCTS . " s2p on ((s2p.products_id = if(s2p.products_id,pd.products_id,0))) where pd.language_id = '" . (int)$languages_id . "' order by pd.products_id desc limit 100");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( !$check_array['missing_id'] ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= SEO_PAGE_SPLIT )
          break;
      }
      // Second pass check for redundant entries in the seo table
      $check_query = tep_db_query("select s2p.products_id, if(pd.products_id, pd.products_name, s2p.seo_name) as name, if(pd.products_id, pd.products_id, -1) as missing_id from " . TABLE_SEO_TO_PRODUCTS . " s2p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on ((s2p.products_id = if(pd.products_id,pd.products_id,0)) and (pd.language_id = if(pd.products_id, '" . (int)$languages_id . "', 0))) order by s2p.products_id desc");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( $check_array['missing_id'] == -1 ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= SEO_PAGE_SPLIT )
          break;
      }
      return $this->error_array;
    }
*/

    function validate() {
      global $languages_id;
      $this->error_array = array();
      // First pass check for missing products from seo table
      $check_query = tep_db_query("select pd.products_id, pd.products_name as name, '0' as missing_id from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_SEO_TO_PRODUCTS . " s2p on (pd.products_id = s2p.products_id) where s2p.products_id is null and pd.language_id = '" . (int)$languages_id . "' order by pd.products_id desc limit " . SEO_PAGE_SPLIT);
      while( $check_array = tep_db_fetch_array($check_query) ) {
        $this->error_array[] = $check_array;
      }
      // Second pass check for redundant entries in the seo table
      $check_query = tep_db_query("select s2p.products_id, s2p.seo_name as name, '-1' as missing_id from " . TABLE_SEO_TO_PRODUCTS . " s2p left join " . TABLE_PRODUCTS . " p on (p.products_id = s2p.products_id) where p.products_id is null order by s2p.products_id desc limit " . SEO_PAGE_SPLIT);
      while( $check_array = tep_db_fetch_array($check_query) ) {
        $this->error_array[] = $check_array;
      }
    }


    function validate_confirm() {
      foreach($_POST['pc_id'] as $products_id => $val) {
        if( $_POST['missing'][$products_id] == -1 ) {
          tep_db_query("delete from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
        } elseif( $_POST['missing'][$products_id] == 0 ) {
          $seo_name = $this->generate_name($products_id);
          $sql_data_array = array(
                                  'products_id' => (int)$products_id,
                                  'seo_name' => tep_db_prepare_input($seo_name),
                                  );
          tep_db_perform(TABLE_SEO_TO_PRODUCTS, $sql_data_array, 'insert');
        }
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=validate'));
    }

    function update_multizone() {
      if( !isset($_POST['pc_id']) || !is_array($_POST['pc_id']) ) {
        tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
      }

      foreach($_POST['pc_id'] as $products_id => $val) {
        if( SEO_PROXIMITY_CLEANUP == 'true' ) {
          $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $check_name = $check_array['seo_name'];
            tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_get like '%" . tep_db_input(tep_db_prepare_input($check_name)) . "%'");
          }
        }
        $seo_name = $this->create_safe_string($_POST['name'][$products_id]);
        $sql_data_array = array(
                                'seo_name' => tep_db_prepare_input($seo_name)
                               );

        tep_db_perform(TABLE_SEO_TO_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function multi_products() {
      if( isset($_GET['categories_list']) ) {
        $categories_id = tep_db_prepare_input($_GET['categories_list']);
        tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action', 'sID', 'categories_list')) . 'action=multi_products&sID=' . $categories_id));
      }
    }


    function multi_categories() {
      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');

      switch( $multi_form ) {
        case 'multi_categories':
          if( !is_array($_POST['categories_id'])) break;
          $tmp_array = array();
          foreach ($_POST['categories_id'] as $category_id=>$val) {
            $multi_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
            while( $multi = tep_db_fetch_array($multi_query) ) {
              $check_query = tep_db_query("select products_id from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$multi['products_id'] . "'");
              if( tep_db_num_rows($check_query) )
                continue;

              $seo_name = $this->generate_name($multi['products_id']);
              if( !isset($tmp_array[$seo_name]) ) {
                $tmp_array[$seo_name] = 1;
              } else {
                $tmp_array[$seo_name]++;
                $seo_name .= $tmp_array[$seo_name];
              }

              $sql_data_array = array(
                                      'products_id' => (int)$multi['products_id'],
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform(TABLE_SEO_TO_PRODUCTS, $sql_data_array, 'insert');
            }
          }
          tep_db_query("optimize table " . TABLE_SEO_TO_PRODUCTS . "");
          tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
          break;
        case 'multi_products':
          $categories_id = tep_db_prepare_input($_POST['categories_list']);
          if( !is_array($_POST['products_id'])) break;
          $tmp_array = array();
          foreach($_POST['products_id'] as $products_id=>$val) {
            $check_query = tep_db_query("select products_id from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
            if( tep_db_num_rows($check_query) )
              continue;

            $seo_name = $this->generate_name($products_id);
            if( !isset($tmp_array[$seo_name]) ) {
              $tmp_array[$seo_name] = 1;
            } else {
              $tmp_array[$seo_name]++;
              $seo_name .= $tmp_array[$seo_name];
            }

            $sql_data_array = array(
                                    'products_id' => (int)$products_id,
                                    'seo_name' => tep_db_prepare_input($seo_name),
                                    );
            tep_db_perform(TABLE_SEO_TO_PRODUCTS, $sql_data_array);
          }
          tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action', 'sID')) . 'action=list&sID=' . $categories_id));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      for($i=0, $j=count($_POST['pc_id']); $i<$j; $i++ ) {
        $products_id = $_POST['pc_id'][$i];
        if( SEO_PROXIMITY_CLEANUP == 'true' ) {
          $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $check_name = $check_array['seo_name'];
            tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_get like '%" . tep_db_input(tep_db_prepare_input($check_name)) . "%'");
          }
        }
        tep_db_query("delete from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function display_html() {
      switch( $this->m_action ) {
        case 'validate':
          $result = $this->display_validation();
          break;
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_categories':
          $result = $this->display_multi_categories();
          break;
        case 'multi_products':
          $result = $this->display_multi_products();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

    function display_validation() {
      $html_string = '';
      $html_string .= 
      '      <tr>' . "\n" . 
      '        <td><hr /></td>' . "\n" . 
      '      </tr>' . "\n";
      if( count($this->error_array) ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowHighBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Product present in the products table but not present in the SEO-G table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '1') . '</td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowImpactBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Product present in the SEO-G table but it is not present in the products table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n" .
        '      <tr>' . "\n" . 
        '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '      </tr>' . "\n";
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_SEO_ZONES, 'action=validate_confirm&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent" width="40"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_ID . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_COMMENT . '</td>' . "\n" . 
        '          </tr>' . "\n";
        for($i=0, $j=count($this->error_array); $i<$j; $i++ ) {
          $row_class = ($this->error_array[$i]['missing_id'])?'dataTableRowImpact':'dataTableRowHigh';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $this->error_array[$i]['products_id'] . ']', 'on', false ) . tep_draw_hidden_field('missing[' . $this->error_array[$i]['products_id'] . ']', $this->error_array[$i]['missing_id']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['products_id'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['name'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . (($this->error_array[$i]['missing_id'])?'Missing from Products':'Missing from SEO-G') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        $html_string .= 
        '          <tr>' . "\n" . 
        '            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <tr>' . "\n" . 
        '                <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_fix_errors.gif', 'Fix Listed Errors') . '</td>' . "\n" . 
        '              </tr>' . "\n" . 
        '            </table></td>' . "\n" . 
        '          </tr>' . "\n" .
        '        </table></form></td>' . "\n" . 
        '      </tr>' . "\n";
      } else {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td class="smallText">' . 'No Errors Found' . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n";
      }
      return $html_string;
    }

// Categories/Products List
    function display_list() {
      global $languages_id;
      $categories_array = $this->get_category_tree_paths();
      $html_string = '';
      $rows = 0;
      //$zones_query_raw = "select s2p.products_id, s2p.seo_types_id, if(pd.products_id, pd.products_name, 'N/A') as final_name, s2p.seo_name from " . TABLE_SEO_TO_PRODUCTS . " s2p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on ((s2p.products_id = if(pd.products_id,pd.products_id,0)) and (pd.language_id = if(pd.products_id, '" . (int)$languages_id . "', 0))) where s2p.seo_types_id = '" . (int)$this->m_zID . "' order by pd.products_name";
      $zones_query_raw = "select products_id, seo_name from " . TABLE_SEO_TO_PRODUCTS . " order by seo_name";

      $zones_split = new splitPageResults($this->m_spage, SEO_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows);
      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_SEO_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n";

        $zones_query = tep_db_query($zones_query_raw);
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="center">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="center">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
        '          </tr>' . "\n";
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $products_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$zones['products_id'] . "' and language_id = '" . (int)$languages_id . "'");
          if( $products_array = tep_db_fetch_array($products_query) ) {
            $final_name = '<a href="' . tep_href_link(FILENAME_CATEGORIES, 'pID=' . $zones['products_id'] . '&action=new_product') . '"><b>' . $products_array['products_name'] . '</b></a>';
          } else {
            $final_name = TEXT_INFO_NA;
          }
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $zones['products_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent" align="center">' . tep_draw_input_field('name[' . $zones['products_id'] . ']', $zones['seo_name'], 'style="width: 300px"') . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $final_name . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, SEO_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '                <tr>' . "\n" . 
        '                  <td nowrap><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_categories') . '">' . tep_image_button('button_categories.gif', TEXT_SWITCH_CATEGORIES) . '</a></td>' . "\n" . 
        '                  <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '                </tr>' . "\n" . 
        '              </table></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><hr /></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td>' . tep_draw_form('mz', FILENAME_SEO_ZONES, '', 'get') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '                <tr>' . "\n" . 
        '                  <td nowrap class="smallText"><b>Select Products from:</b></td>' . "\n" . 
        '                  <td nowrap>' . tep_draw_pull_down_menu('categories_list', $categories_array, (isset($this->m_sID)?$this->m_sID:'')) . '</td>' . "\n" . 
        '                  <td nowrap>' . tep_draw_hidden_field('action', 'multi_products') . tep_draw_hidden_field('zID', $this->m_zID) . tep_draw_hidden_field('zpage', $this->m_zpage) . tep_draw_hidden_field('spage', $this->m_spage) . tep_image_submit('button_submit.gif', TEXT_SWITCH_PRODUCTS) . '</td>' . "\n" . 
        '                  <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '                </tr>' . "\n" . 
        '              </table></form></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }


    function display_multi_categories() {
      global $languages_id;
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTICATEGORIES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_categories', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mc, \'categories_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      $categories_query_raw = "select distinct p2c.categories_id, cd.categories_name from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (p2c.categories_id=cd.categories_id) where cd.language_id = '" . (int)$languages_id . "' order by cd.categories_name";
      $categories_split = new splitPageResults($this->m_mcpage, SEO_PAGE_SPLIT, $categories_query_raw, $categories_query_numrows, 'p2c.categories_id');
      $categories_query = tep_db_query($categories_query_raw);
      $bCheck = false;
      while ($categories = tep_db_fetch_array($categories_query)) {
        $product_categories_string = $this->get_categories_string($categories['categories_id']);
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . tep_draw_checkbox_field('categories_id[' . $categories['categories_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $product_categories_string . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_categories') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $categories_split->display_count($categories_query_numrows, SEO_PAGE_SPLIT, $this->m_mcpage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $categories_split->display_links($categories_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mcpage, tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_categories', 'mcpage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }


    function display_multi_products() {
      global $languages_id;
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIZONES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' .  tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' .  tep_draw_form('mp', FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_categories', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mp, \'products_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";

      $rows = 0;
      $zones_query_raw = "select distinct p2c.products_id, p2c.categories_id, pd.products_name, if(s2p.products_id, '1', '0') as checkbox from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p2c.products_id) left join " . TABLE_SEO_TO_PRODUCTS . " s2p on (s2p.products_id=p2c.products_id) where pd.language_id= '" . (int)$languages_id . "' and p2c.categories_id = '" . (int)$this->m_sID . "' order by pd.products_name";
      $zones_split = new splitPageResults($this->m_mppage, SEO_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows, 'p2c.products_id');
      $zones_query = tep_db_query($zones_query_raw);
      $bCheck = false;
      while ($zones = tep_db_fetch_array($zones_query)) {
        $bCheck = ($zones['checkbox'] == '1')?true:false;
        $product_categories_string = $this->get_categories_string($zones['categories_id']);
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        if($bCheck)
          $row_class = 'dataTableRowGreen';

        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . ($bCheck?'Included':tep_draw_checkbox_field('products_id[' . $zones['products_id'] . ']')) . '</td>' . "\n" . 
//        '              <td class="dataTableContent">' . tep_draw_checkbox_field('products_id[' . $zones['products_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $product_categories_string . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $zones['products_name'] . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action', 'mppage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_products') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . tep_draw_hidden_field('multi_form', 'multi_products') . tep_draw_hidden_field('categories_list', $this->m_sID) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '         </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, SEO_PAGE_SPLIT, $this->m_mppage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mppage, tep_get_all_get_params(array('action', 'mppage')) . 'action=multi_products', 'mppage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }


    function display_delete_multizone() {
      global $languages_id;

      if( !isset($_POST['pc_id']) || !is_array($_POST['pc_id']) ) {
        return '';
      }

      $html_string = '';
      $attr_query = tep_db_query("select seo_types_name from " . TABLE_SEO_TYPES . " where seo_types_id = '" . (int)$this->m_zID . "'");
      $attr_array = tep_db_fetch_array($attr_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $attr_array['seo_types_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";
      $rows = 0;

      foreach($_POST['pc_id'] as $key => $value) {
        //$delete_query = tep_db_query("select s2p.seo_types_id, s2p.products_id, pd.products_name as final_name from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_SEO_TO_PRODUCTS . " s2p on (s2p.products_id = p2c.products_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p2c.products_id=pd.products_id) where s2p.products_id = '" . (int)$products_id . "' and pd.language_id = '" .(int)$languages_id . "' and s2p.seo_types_id = '" . (int)$this->m_zID . "' order by pd.products_name");
        $delete_query = tep_db_query("select products_name as final_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$key . "' and language_id = '" .(int)$languages_id . "'");
        if( tep_db_num_rows($delete_query) ) {
          $delete_array = tep_db_fetch_array($delete_query);
        } else {
          $delete_array = array('final_name' => 'N/A - ' . $key);
        }

        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .= 
        '          <tr class="' . $row_class . '">' . "\n" . 
        '            <td class="dataTableContent">' . tep_draw_hidden_field('pc_id[]', $key) . $delete_array['final_name'] . '</td>' . "\n" . 
        '          </tr>' . "\n";

      }
      if( count($_POST['pc_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="3"><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }
  }
?>