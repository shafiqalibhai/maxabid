<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Coupons support class
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class coupons {

    function coupons() {
      $this->cache = array();
      $this->types = array(
//        1 => 'Fixed Amount on Sub-Total',
//        2 => 'Percentage on Sub-Total',
//        3 => 'Fixed Amount on Grand Total',
//        4 => 'Percentage on Grand Total',
//        5 => 'Fixed on Shipping Cost',
//        6 => 'Percentage on Shipping Cost',
//        7 => 'Fixed for some products',
//        8 => 'Percentage on some products',
//        9 => 'Fixed on some categories',
//        10 => 'Percentage on some categories',
//        11 => 'Fixed excluding products',
//        12 => 'Percentage excluding products',
//        13 => 'Fixed excluding categories',
//        14 => 'Percentage excluding categories',
        15 => 'Earn Extra Bids',
      );
    }

    function get( $name ) {
      if( empty($name) ) return array();

      if( isset($this->cache[$name]) ) {
        return $this->cache[$name];
      }

      $this->cache[$name] = array();
      $coupons_query = tep_db_query("select * from " . TABLE_COUPONS . " where coupons_name = '" . tep_db_input($name) . "'");
      if( tep_db_num_rows($coupons_query) ) {
        $this->cache[$name] = tep_db_fetch_array($coupons_query);
      }
      return $this->cache[$name];
    }

    function get_amount( $name ) {
      global $order;

      $amount = 0;

      if( !isset($order) || !is_object($order) ) return $amount;

      $coupons_array = $this->get($name);
      if( empty($coupons_array) || !$coupons_array['coupons_amount'] || !is_valid_type($coupons_array['coupons_type']) ) return $amount;

      $amount = $coupons_array['coupons_amount'];

      switch($coupons_type) {
        case 1:
          if( $order->info['subtotal'] < $amount ) {
            $amount = $order->info['subtotal'];
          }
          break;
        case 2:
          $amount = $order->info['subtotal']*($amount/100);
          break;
        default:
          break;
      }
      $amount = tep_round($amount,2);
      return $amount;
    }

    function is_valid_type($type_id) {
      return isset($this->types[$type_id]);
    }

    function check( $name, $customers_id=0 ) {
      $coupons_array = $this->get($name);
      if( empty($coupons_array) || !$coupons_array['status_id'] ) return false;

      $now = tep_mysql_to_time_stamp(date('Y-m-d H:i:s'));
      $start = tep_mysql_to_time_stamp($coupons_array['date_start']);
      $end = tep_mysql_to_time_stamp($coupons_array['date_end']);

      if( $start && $now < $start ) return false;
      if( $end && $now >= $end ) return false;

      $usage = $this->get_usage($coupons_array['coupons_id'], $customers_id);
      if( $coupons_array['coupons_usage'] && $usage >= $coupons_array['coupons_usage'] ) return false;

      $max_usage = $this->get_usage($coupons_array['coupons_id']);
      if( $coupons_array['coupons_max_usage'] && $max_usage >= $coupons_array['coupons_max_usage'] ) return false;

      switch($coupons_type) {
        default:
          break;
      }
      return true;
    }

    function register_coupon($name) {
      global $redeem_coupon_id;

      $coupons_array = $this->get($name);
      if( empty($coupons_array) ) return false;
      if( !$this->check($name) ) return false;

      if( !tep_session_is_registered('redeem_coupon_id') ) {
        tep_session_register('redeem_coupon_id');
      }
      $redeem_coupon_id = $coupons_array['coupons_id'];
      return true;
    }

    function update( $coupons_id, $customers_id=0 ) {
      $sql_data_array = array(
        'coupons_id' => (int)$coupons_id,
        'customers_id' => (int)$customers_id,
        'date_added' => date('Y-m-d H:i:s'),
      );
      tep_db_perform(TABLE_COUPONS_REDEEM, $sql_data_array);
    }

    function get_usage( $coupons_id, $customers_id=0) {
      $coupons_query_raw = "select count(*) as total from " . TABLE_COUPONS_REDEEM . " where coupons_id = '" . (int)$coupons_id . "'";
      if( $customers_id ) {
        $coupons_query_raw .= " and customers_id = '" . (int)$customers_id . "'";
      }
      $coupons_query = tep_db_query($coupons_query_raw);
      $coupons_array = tep_db_fetch_array($coupons_query);
      return $coupons_array['total'];
    }
  }
?>
