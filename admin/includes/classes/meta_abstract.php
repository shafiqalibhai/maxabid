<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Abstract Zones class for Admin
// This is a Bridge for META-G
// Processes Abstract Zones generates meta-tag segments.
// Featuring:
// - Multi-Abstract Zones Listings with Meta-Tags
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class meta_abstract extends meta_zones {
    var $error_array;

// class constructor
    function meta_abstract() {
      $this->m_ssID = isset($_GET['ssID'])?$_GET['ssID']:'';
      $this->m_mcpage = isset($_GET['mcpage'])?$_GET['mcpage']:'';
      $this->m_mppage = isset($_GET['mppage'])?$_GET['mppage']:'';
      parent::meta_zones();
    }

    function generate_name($abstract_zone_id) {
      $name = '';
      $name_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$abstract_zone_id . "'");
      if( $names_array = tep_db_fetch_array($name_query) ) {
        $name = $names_array['abstract_zone_name'];
        $name =  $this->create_safe_string($name);
      }
      return $name;
    }

    function generate_lexico($index=0) {
      $abstract_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . "");
      while( $abstract_array = tep_db_fetch_array($abstract_query) ) {

        $phrase = $this->create_safe_string($abstract_array['abstract_zone_name']);
        $md5_key = md5($phrase);
        $check_query = tep_db_query("select count(*) as total from " . TABLE_META_LEXICO . " where meta_lexico_key = '" . tep_db_input(tep_db_prepare_input($md5_key)) . "'");
        $check_array = tep_db_fetch_array($check_query);
        if( !$check_array['total'] ) {
          $sql_data_array = array(
                                  'meta_lexico_key' => tep_db_prepare_input($md5_key),
                                  'meta_lexico_text' => tep_db_prepare_input($phrase)
                                 );
          tep_db_perform(TABLE_META_LEXICO, $sql_data_array);
        }
      }
    }

    function get_zone_names($zone_id) {
      $keywords_array = array();

      $types_query = tep_db_query("select abstract_types_class, abstract_types_table from " . TABLE_ABSTRACT_TYPES . " azt left join " . TABLE_ABSTRACT_ZONES . " az on (az.abstract_types_id=azt.abstract_types_id) where az.abstract_zone_id = '" . (int)$zone_id . "'");
      if( !tep_db_num_rows($types_query) )
        return $keywords_array;
      $types_array = tep_db_fetch_array($types_query);

      switch($types_array['abstract_types_class']) {
        case 'products_zones':
        case 'products_control_zones':
        case 'accessory_zones':
          $keywords_array = $this->get_group_products($zone_id, $types_array['abstract_types_table']);
          break;
          break;
        case 'attributes_zones':
        case 'weak_attributes_zones':
          break;
        case 'generic_zones':
          $keywords_array = $this->get_group_text($zone_id, $types_array['abstract_types_table']);
          break;
        default:
          return $keywords_array;

      }
      return $keywords_array;
    }

    function get_group_text($zone_id, $table) {
      global $languages_id;

      $products_array = array();
      $text_array = array();

      $tables_array = explode(',', $table);
      if( !is_array($tables_array) ) {
        return $text_array;
      }

      if( isset($tables_array[1]) ) {
        $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) left join " . tep_db_input(tep_db_prepare_input($tables_array[1])) . " gtp on (gtp.products_id=p.products_id) where gtp.abstract_zone_id = '" . (int)$zone_id . "' and p.products_status = '1' and pd.language_id = '" . (int)$languages_id . "'");
        while( $product = tep_db_fetch_array($products_query) ) {
          $text_array[$product['products_id']] = $this->create_safe_string($product['products_name']);
        }
      }

      $text_query = tep_db_query("select gtd.gtext_id, gt.gtext_title from " . TABLE_GTEXT . " gt left join " . tep_db_prepare_input(tep_db_input($tables_array[0])) . " gtd on (gtd.gtext_id=gt.gtext_id) where gtd.abstract_zone_id = '" . (int)$zone_id . "' and gt.status='1' order by gtd.sequence_order");
      while($text = tep_db_fetch_array($text_query) ) { 
        $text_array['txt_' . $text['gtext_id']] = $this->create_safe_string($text['gtext_title']);
        if( count($text_array) > META_MAX_KEYWORDS ) {
          break;
        }
      }
      return $text_array;
    }


    function get_group_products($zone_id, $table) {
      global $languages_id;

      $products_array = array();
      $zone_query = tep_db_query("select products_id, categories_id from " . tep_db_prepare_input(tep_db_input($table)) . " where abstract_zone_id = '" . (int)$zone_id . "' and status_id='1' order by sort_id");

      while( $zone = tep_db_fetch_array($zone_query) ) {
        if( !$zone['categories_id'] && !$zone['products_id'] ) {
          return $products_array;
        }
        if( !$zone['products_id'] ) {
          $key_array = array();
          $this->set_key_products($key_array, $zone['categories_id']);
          if( count($key_array) ) {
            $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) where p.products_display = '1' and p.products_id in (" . implode(',', $key_array) . ") and pd.language_id = '" . (int)$languages_id . "' order by p.products_status desc");
            while( $product = tep_db_fetch_array($products_query) ) {
              $products_array[$product['products_id']] = $this->create_safe_string($product['products_name']);
            }
          }
        } else {
          $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) where p.products_id = '" . (int)$zone['products_id'] . "' and p.products_display = '1' and pd.language_id = '" . (int)$languages_id . "'");
          if( $product = tep_db_fetch_array($products_query) ) {
            $products_array[$zone['products_id']] = $this->create_safe_string($product['products_name']);
          }
        }
        if( count($products_array) > META_MAX_KEYWORDS ) {
          break;
        }
      }
      return $products_array;
    }


    function set_key_products(&$key_array, $categories_id) {
      $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
      while($products = tep_db_fetch_array($products_query) ) {
        $key_array[$products['products_id']] = $products['products_id'];
      }
    }


    function process_action() {
      switch( $this->m_action ) {
        case 'validate':
          return $this->validate();
        case 'validate_confirm':
          return $this->validate_confirm();
        case 'update_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->update_multizone();
        case 'insert_multientries':
          $result = parent::validate_array_selection('pc_id', 'multi_entries'); 
          return $this->insert_multientries();
        case 'deleteconfirm_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->deleteconfirm_multizone();
        case 'delete_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          break;
        default:
          return parent::process_action(); 
          break;
      }
    }

    function validate() {
      $this->error_array = array();
      // First pass check for missing products from seo table
      $check_query = tep_db_query("select pd.abstract_zone_id, pd.abstract_zone_name as name, if(s2p.abstract_zone_id, s2p.abstract_zone_id, 0) as missing_id from " . TABLE_ABSTRACT_ZONES . " pd left join " . TABLE_META_ABSTRACT . " s2p on ((s2p.abstract_zone_id = if(s2p.abstract_zone_id,pd.abstract_zone_id,0))) order by pd.abstract_zone_id desc");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( !$check_array['missing_id'] ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= META_PAGE_SPLIT )
          break;
      }
      // Second pass check for redundant entries in the seo table
      $check_query = tep_db_query("select s2p.abstract_zone_id, if(pd.abstract_zone_id, pd.abstract_zone_name, s2p.meta_name) as name, if(pd.abstract_zone_id, pd.abstract_zone_id, -1) as missing_id from " . TABLE_META_ABSTRACT . " s2p left join " . TABLE_ABSTRACT_ZONES . " pd on (s2p.abstract_zone_id = if(pd.abstract_zone_id,pd.abstract_zone_id,0)) order by s2p.abstract_zone_id desc");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( $check_array['missing_id'] == -1 ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= META_PAGE_SPLIT )
          break;
      }
      return $this->error_array;
    }

    function validate_confirm() {
      foreach($_POST['pc_id'] as $abstract_zone_id => $val) {
        if( $_POST['missing'][$abstract_zone_id] == -1 ) {
          tep_db_query("delete from " . TABLE_META_ABSTRACT . " where meta_types_id = '" . (int)$this->m_zID . "' and abstract_zone_id = '" . (int)$abstract_zone_id . "'");
        } elseif( $_POST['missing'][$abstract_zone_id] == 0 ) {
          $meta_name = $this->generate_name($abstract_zone_id);
          $sql_data_array = array(
                                  'meta_types_id' => (int)$this->m_zID,
                                  'abstract_zone_id' => (int)$abstract_zone_id,
                                  'meta_name' => tep_db_prepare_input($meta_name),
                                  );
          tep_db_perform(TABLE_META_ABSTRACT, $sql_data_array, 'insert');
        }
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=validate'));
    }

    function update_multizone() {
      foreach($_POST['pc_id'] as $abstract_zone_id => $val) {

        $meta_title = $_POST['title'][$abstract_zone_id];
        $meta_keywords = $_POST['keywords'][$abstract_zone_id];
        $meta_text = $_POST['text'][$abstract_zone_id];

        $sql_data_array = array(
                                'meta_title' => tep_db_prepare_input($meta_title),
                                'meta_keywords' => tep_db_prepare_input($meta_keywords),
                                'meta_text' => tep_db_prepare_input($meta_text)
                               );

        tep_db_perform(TABLE_META_ABSTRACT, $sql_data_array, 'update', "abstract_zone_id = '" . (int)$abstract_zone_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }


    function insert_multientries() {
      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');

      switch( $multi_form ) {
        case 'multi_entries':
          $tmp_array = array();
          foreach ($_POST['pc_id'] as $abstract_id=>$val) {
            $multi_query = tep_db_query("select abstract_zone_id, abstract_zone_name, abstract_zone_desc from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$abstract_id . "'");
            while( $multi = tep_db_fetch_array($multi_query) ) {
              $check_query = tep_db_query("select abstract_zone_id from " . TABLE_META_ABSTRACT . " where abstract_zone_id = '" . (int)$multi['abstract_zone_id'] . "'");
              if( tep_db_num_rows($check_query) )
                continue;

              $meta_name = $this->create_safe_string($multi['abstract_zone_name']);
              $keywords_array = $this->get_zone_names($multi['abstract_zone_id']);
              
              if( strlen($meta_text) < META_MAX_DESCRIPTION ) {
                $meta_text .= $this->create_safe_description($multi['abstract_zone_desc']) . ' ';
              }

              if( count($keywords_array) ) {
                $meta_keywords = implode(',',$keywords_array);
              } else {
                $meta_keywords = $meta_name;
              }
              if( strlen($meta_text) ) {
                $meta_text = substr($meta_text, 0, -1);
              } else {
                $meta_text = $meta_name;
              }

              $sql_data_array = array(
                                      'abstract_zone_id' => (int)$abstract_id,
                                      'meta_title' => tep_db_prepare_input($meta_name),
                                      'meta_keywords' => tep_db_prepare_input($meta_keywords),
                                      'meta_text' => tep_db_prepare_input($meta_text)
                                     );
              tep_db_perform(TABLE_META_ABSTRACT, $sql_data_array, 'insert');

            }
          }
          tep_db_query("optimize table " . TABLE_META_ABSTRACT . "");
          tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      for($i=0, $j=count($_POST['pc_id']); $i<$j; $i++ ) {
        $abstract_zone_id = $_POST['pc_id'][$i];
        tep_db_query("delete from " . TABLE_META_ABSTRACT . " where abstract_zone_id = '" . (int)$abstract_zone_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }


    function display_html() {
      switch( $this->m_action ) {
        case 'validate':
          $result = $this->display_validation();
          break;
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_entries':
          $result = $this->display_multi_entries();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

    function display_validation() {
      $html_string = '';
      $html_string .= 
      '      <tr>' . "\n" . 
      '        <td><hr /></td>' . "\n" . 
      '      </tr>' . "\n";
      if( count($this->error_array) ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowHighBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Zone present in the abstract zones table but not present in the META-G table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '1') . '</td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowImpactBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Zone present in the META-G table but it is not present in the Abstract Zones table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n" .
        '      <tr>' . "\n" . 
        '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '      </tr>' . "\n";
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_META_ZONES, 'action=validate_confirm&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent" width="40"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_ID . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_COMMENT . '</td>' . "\n" . 
        '          </tr>' . "\n";
        for($i=0, $j=count($this->error_array); $i<$j; $i++ ) {
          $row_class = ($this->error_array[$i]['missing_id'])?'dataTableRowImpact':'dataTableRowHigh';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $this->error_array[$i]['abstract_zone_id'] . ']', 'on', false ) . tep_draw_hidden_field('missing[' . $this->error_array[$i]['abstract_zone_id'] . ']', $this->error_array[$i]['missing_id']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['abstract_zone_id'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['name'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . (($this->error_array[$i]['missing_id'])?'Missing from Products':'Missing from META-G') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        $html_string .= 
        '          <tr>' . "\n" . 
        '            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <tr>' . "\n" . 
        '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_fix_errors.gif', 'Fix Listed Errors') . '</td>' . "\n" . 
        '              </tr>' . "\n" . 
        '            </table></td>' . "\n" . 
        '          </tr>' . "\n" .
        '        </table></form></td>' . "\n" . 
        '      </tr>' . "\n";
      } else {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td class="smallText">' . 'No Errors Found' . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n";
      }
      return $html_string;
    }

// Categories/Products List
    function display_list() {
      //$abstract_array = $this->get_category_tree_paths();
      $html_string = '';
      $rows = 0;
      $zones_query_raw = "select * from " . TABLE_META_ABSTRACT . " order by meta_title, abstract_zone_id";
      $zones_split = new splitPageResults($this->m_spage, META_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows);

      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_META_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n";

        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent" valign="top"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_TITLE_PRODUCT . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_KEYWORDS . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_DESCRIPTION . '</td>' . "\n" . 
        '          </tr>' . "\n";
        $zones_query = tep_db_query($zones_query_raw);
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $products_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$zones['abstract_zone_id'] . "'");
          if( $products_array = tep_db_fetch_array($products_query) ) {
            $final_name = '<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zID=' . $zones['abstract_zone_id'] . '&action=list') . '"><b>' . $products_array['abstract_zone_name'] . '</b></a>';
          } else {
            $final_name = '<b>N/A</b>';
          }
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $zones['abstract_zone_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent">Zone:&nbsp;' . $final_name . '<br />' . tep_draw_input_field('title[' . $zones['abstract_zone_id'] . ']', $zones['meta_title'], 'style="width: 300px"') . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_textarea_field('keywords[' . $zones['abstract_zone_id'] . ']', 'soft', '40','2', $zones['meta_keywords']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_textarea_field('text[' . $zones['abstract_zone_id'] . ']', 'soft', '40','2', $zones['meta_text']) . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, META_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, META_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '                <tr>' . "\n" . 
        '                  <td nowrap><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> <a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_entries') . '">' . tep_image_button('button_zones.gif', TEXT_SWITCH_ZONES) . '</a></td>' . "\n" . 
        '                  <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '                </tr>' . "\n" . 
        '              </table></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><hr /></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }


    function display_multi_entries() {
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIZONES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=insert_multientries', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mc, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ZONES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_TYPES . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      //$abstract_query_raw = "select abstract_zone_id, abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " order by abstract_zone_name";
      $abstract_query_raw = "select az.abstract_zone_id, az.abstract_zone_name, azt.abstract_types_name, if(mm.abstract_zone_id, '1', '0') as checkbox from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " azt on (az.abstract_types_id=azt.abstract_types_id) left join " . TABLE_META_ABSTRACT . " mm on ((mm.abstract_zone_id = if(mm.abstract_zone_id, az.abstract_zone_id,0))) order by az.abstract_zone_id, az.abstract_zone_name";
      $abstract_split = new splitPageResults($this->m_mcpage, META_PAGE_SPLIT, $abstract_query_raw, $abstract_query_numrows, 'az.abstract_zone_id');
      $abstract_query = tep_db_query($abstract_query_raw);
      $bCheck = false;
      while ($abstract = tep_db_fetch_array($abstract_query)) {
        $bCheck = ($abstract['checkbox'] == '1')?true:false;
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        if($bCheck)
          $row_class = 'dataTableRowGreen';

        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . ($bCheck?'Included':tep_draw_checkbox_field('pc_id[' . $abstract['abstract_zone_id'] . ']')) . '</td>' . "\n" . 
        '              <td class="dataTableContent"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zID=' . $abstract['abstract_zone_id'] . '&action=list') . '"><b>' . $abstract['abstract_zone_name'] . '</b></a></td>' . "\n" . 
        '              <td class="dataTableContent">' . $abstract['abstract_types_name'] . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_entries') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $abstract_split->display_count($abstract_query_numrows, META_PAGE_SPLIT, $this->m_mcpage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $abstract_split->display_links($abstract_query_numrows, META_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mcpage, tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_entries', 'mcpage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }

    function display_delete_multizone() {
      if( !isset($_POST['pc_id']) || !is_array($_POST['pc_id']) ) {
        return '';
      }

      $html_string = '';
      $attr_query = tep_db_query("select meta_types_name from " . TABLE_META_TYPES . " where meta_types_id = '" . (int)$this->m_zID . "'");
      $attr_array = tep_db_fetch_array($attr_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $attr_array['meta_types_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";
      $rows = 0;

      foreach($_POST['pc_id'] as $key => $value) {
        $abstract_zone_id = $key;
        $delete_query = tep_db_query("select abstract_zone_id, meta_title from " . TABLE_META_ABSTRACT . " where abstract_zone_id = '" . (int)$key . "' order by meta_title, abstract_zone_id");
        if( $delete_array = tep_db_fetch_array($delete_query) ) {
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_hidden_field('pc_id[]', $delete_array['abstract_zone_id']) . $delete_array['meta_title'] . '</td>' . "\n" . 
          '          </tr>' . "\n";
        }
      }
      if( count($_POST['pc_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="3"><a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }
  }
?>