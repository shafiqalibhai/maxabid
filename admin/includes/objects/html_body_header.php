<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Common html body header section
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
?>
<!-- body_text //-->
    <td width="100%" valign="top">
<?php
  if( isset($body_form) ) {
    echo $body_form;
  }
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if( !isset($heading_row) ) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right">
<?php
    if(defined('HEADING_IMAGE') ) {
      echo tep_image(DIR_WS_IMAGES . HEADING_IMAGE, HEADING_TITLE, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); 
    } else {
      echo '&nbsp;';
    }
?>
            </td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>