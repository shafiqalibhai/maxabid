<?php
/*-----------------------------------------------------------------------------*\
#################################################################################
#	Script name: admin/stats_explain_queries.php
#	Version: v1.0
#
#	Copyright (C) 2005 Bobby Easland
#	Internet moniker: Chemo
#	Contact: chemo@mesoimpact.com
#
#	This script is free software; you can redistribute it and/or
#	modify it under the terms of the GNU General Public License
#	as published by the Free Software Foundation; either version 2
#	of the License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	Script is intended to be used with:
#	osCommerce, Open Source E-Commerce Solutions
#	http://www.oscommerce.com
#	Copyright (c) 2003 osCommerce
#
#   Version: 1.1
#   Modified by Mark Samios
#   - Modified explain_queries table columns to fix problem with dbase backup
#
################################################################################
\*-----------------------------------------------------------------------------*/

  require('includes/application_top.php');
  
// Set it false initially
$run_it = false;
  
// function used to download the CVS export
function explain_download($filecontents, $filename)
{	
	header('Content-Type: application/octet-stream');
	header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
	header('Content-Disposition: attachment; filename="' . $filename . '"');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	echo $filecontents;	
	exit();
}

if ( isset($_POST['truncate']) && $_POST['truncate'] == 'true' ) {
	tep_db_query('TRUNCATE TABLE `explain_queries`','db_link',true);
}

if ( isset($_POST['analyze']) && $_POST['analyze'] == 'true' ) {
	tep_db_query('ANALYZE TABLE `explain_queries`','db_link',true);
}

$limit = ( (isset($_GET['limit'])) ? (int)$_GET['limit'] : 400);
$offset = ( (isset($_GET['offset'])) ? (int)$_GET['offset'] : 0);

if ( isset($_GET['script']) && tep_not_null($_GET['script']) ){
	$type = 'script';
	$_query_raw = "SELECT `explain_md5query` , `explain_query` , `explain_time` , `explain_script` , `explain_request_string` , `explain_table` , `explain_type` , `explain_possible_keys` , `explain_key` , `explain_key_len` , `explain_ref` , `explain_rows` , `explain_extra` , `explain_comment`, avg(explain_time) as average, count(explain_md5query) as num_records, min(explain_time) as min, max(explain_time) as max from explain_queries where explain_script='".$_GET['script']."' GROUP BY explain_md5query ORDER BY average DESC limit $offset, $limit";
	$run_it = true;
}

if (isset($_GET['query']) && tep_not_null($_GET['query']) ){
	$type = 'query';
	$_query_raw = "SELECT `explain_md5query` , `explain_query` , `explain_time`, `explain_script` , `explain_request_string` , `explain_table` , `explain_type` , `explain_possible_keys` , `explain_key` , `explain_key_len` , `explain_ref` , `explain_rows` , `explain_extra` , `explain_comment`, avg(explain_time) as average, count(explain_md5query) as num_records, min(explain_time) as min, max(explain_time) as max from explain_queries WHERE explain_md5query='".$_GET['query']."' GROUP BY explain_script ORDER BY average DESC limit $offset, $limit";
	$run_it = true;
}

if (isset($_POST['format'])){
	switch($_POST['format']){
		case 'html':
		 	$html_out = true;
		break;
		case 'cvs':
		default:
			$cvsfields = array('explain_time','explain_script','explain_request_string','explain_table','explain_type','explain_possible_keys','explain_key','explain_key_len','explain_ref','explain_rows','explain_extra','explain_comment','explain_query');
			$cvsoutput = implode("\t", $cvsfields) . "\n"; 
			$cvs_out = true;
		break;
	} # End switch
}

if ($run_it){
 	$_query = tep_db_query($_query_raw, 'db_link', true);

	while ($temp = tep_db_fetch_array($_query)){
		if (tep_not_null($temp['explain_extra'])) $temp['explain_extra'] = '<font color="#990000">'. $temp['explain_extra'] . '</font>';
		if (tep_not_null($temp['explain_comment'])) $temp['explain_comment'] = '<font color="#990000">'. $temp['explain_comment'] . '</font>';
		$_result[]=$temp;
		if( isset($cvs_out) ){
			foreach ($cvsfields as $index => $field){
				$temp_cvs[] = $temp[$field];
			} # End foreach
			$cvsoutput .= implode("\t", $temp_cvs)."\n";
			unset($temp_cvs);
		} # End if
	} # End while
} # End if

// This is a CVS download so let them have it 
if ( isset($cvs_out) ){
	explain_download(rtrim($cvsoutput), str_replace('.php', '.txt', $_GET['script']));
}

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title>Explain Queries v1.0 - by Chemo</title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php
// if this is an html export start the buffer to swallow the header and column left
if (isset($html_out) ) ob_start(); 
require(DIR_WS_INCLUDES . 'header.php'); 
?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top">
	<table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
	<!-- left_navigation //-->
	<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
	<!-- left_navigation_eof //-->
    </table>
	<br>
<!-- truncate and analyze button //-->
<div align="center">
	<form action="" method="post">
		<input type="hidden" name="truncate" value="true">
		<input type="submit" value="Truncate Table">
	</form>
</div>
	<br>
<div align="center">
	<form action="" method="post">
		<input type="hidden" name="analyze" value="true">
		<input type="submit" value="Analyze Table">
	</form>
</div>
<!--  truncate and analyze button end //-->	
	</td>
<!-- body_text //-->
    <td width="100%" valign="top">
	<!--  page heading and export format forum //-->
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
    	<tr>
        	<td class="pageHeading" align="left">Explain Queries Report v1.0 <a href="http://forums.oscommerce.com/index.php?showuser=9196">- by Chemo</a></td>
          	<td align="right">
				<form action="<?php echo $_SERVER['PHP_SELF'] . '?' .tep_get_all_get_params(); ?>" method="post">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
					<tr>
						<td class="dataTableContent" align="right"><b>Export Format</b></td><td>&nbsp;</td>
					</tr>
					<tr>
						<td class="dataTableContent" align="right">CVS <input type="radio" name="format" value="cvs" checked>HTML <input type="radio" name="format" value="html"></td>
						<td align="right"><input type="submit" value="Export"></td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
	</table>
	<!-- page heading and export format end //-->
	<!-- script drop down menu filter and limit - offset //-->
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
		<td class="dataTableContent">
		<form action="" method="get">
<?php
/* Query for total scripts, number of unique queries, and total queries stored for the script */
$pages_query = tep_db_query('select explain_script, count(distinct explain_md5query) as count, count(*) as total from explain_queries group by explain_script','db_link',true);
/* Start the drop down menu */
$page_menu = '<select name="script">';
while ($temp = tep_db_fetch_array($pages_query)){
	$pages_indexed[] = $temp['explain_script'];
	$selected = ( ( isset($_GET['script']) && $_GET['script'] == $temp['explain_script']) ? ' selected' : '' );
    $page_menu .= '<option value="' . $temp['explain_script'] . '"'.$selected.'>' . $temp['explain_script'] . ' ('.$temp['count'].' / '. $temp['total'].')</option>';
}
$page_menu .= '</select>';
echo $page_menu;
?>
&nbsp;Limit to&nbsp;<input type="text" size="3" maxlength="3" name="limit" value="<?php echo ( (isset($_GET['limit'])) ? $_GET['limit'] : '400'); ?>">
&nbsp;rows starting with row#&nbsp;<input type="text" size="3" maxlength="3" name="offset" value="<?php echo ( (isset($_GET['offset'])) ? $_GET['offset'] : '0'); ?>">
			<input type="submit">
			</form>
			</td>
		</tr>
	</table>
	<!-- script drop down filter and limit - offset end //-->
	<br>
<?php 
if ( isset($html_out) ) {
	/* End the buffer if this is an html export */
	ob_end_clean(); 
	/* Give a link back to the page so they won't be stranded */
	echo '<p><a href="'.$_SERVER['PHP_SELF'].'">Back to Explain Queries</a><p>';
}
?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td valign="top" class="dataTableContent">
				<?php if(isset($_result) && is_array($_result)) echo sizeof($_result); ?> results returned for: <b><?php echo $type . '=' . $_GET[$type]; ?></b>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
		<td valign="top">			
<?php
/* If we have a result set to work with let's make some pretty tables */
if (isset($_result)) {
    $time_total = array('min' => 0, 'average' => 0, 'max' => 0 );
	$i = $offset+1;
	/* Loop it */
	foreach($_result as $data){
      $time_total['min'] += $data['min'];
      $time_total['average'] += $data['average'];
      $time_total['max'] += $data['max'];
?>
	<!-- data table for query <?php echo $i; ?> //-->
	<table border="1" width="100%" cellspacing="0" cellpadding="3">
		<tr class="dataTableHeadingRow">
			<td class="dataTableHeadingContent"><font color="#990000">Query#&nbsp;<?php echo $i; ?> =></font>&nbsp <a href="<?php echo basename($_SERVER['PHP_SELF']).'?query='.$data['explain_md5query'] ?>"><?php echo $data['explain_query']; ?></a></td>
		</tr>
	</table>
	<table border="1" width="100%" cellspacing="0" cellpadding="2">
		<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
			<td class="dataTableContent" align="left" valign="top">Time (ms): <?php echo number_format($data['min'], 3) .' - <b>'.number_format($data['average'], 3).'</b> - '.number_format($data['max'], 3); ?></td>
			<td class="dataTableContent" align="left" valign="top"># Records: <b><?php echo $data['num_records']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Script: <b><?php echo $data['explain_script']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Request String: <b><?php echo $data['explain_request_string']; ?></b></td>
		</tr>
		<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
			<td class="dataTableContent" align="left" valign="top">Table: <b><?php echo $data['explain_table']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Type: <b><?php echo $data['explain_type']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Possible / Used Key: <b><?php echo $data['explain_possible_keys'].' / '.$data['explain_key']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Key Len: <b><?php echo $data['explain_key_len']; ?></b></td>
		</tr>
		<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
			<td class="dataTableContent" align="left" valign="top">Rows: <b><?php echo $data['explain_rows']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Ref: <b><?php echo $data['explain_ref']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Extra: <b><?php echo $data['explain_extra']; ?></b></td>
			<td class="dataTableContent" align="left" valign="top">Comment: <b><?php echo $data['explain_comment']; ?></b></td>
		</tr>
	</table>
	<!-- end data table for query <?php echo $i; ?> //-->
	<br>
<?php
$i++;
	}
?>
	<table border="1" width="100%" cellspacing="0" cellpadding="2">
		<tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">
			<td class="dataTableContent" align="left" valign="top">Total Time (ms): <?php echo number_format($time_total['min'], 3) .' - <b>'.number_format($time_total['average'], 3).'</b> - '.number_format($time_total['max'], 3); ?></td>
        </tr>
	</table>
<?php
}
/* unset the array since it may be rather large */
unset($_result);
?>			
			</td>
		</tr>
	</table>
			</td>
		</tr>
	</table>
<!-- body_text_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>