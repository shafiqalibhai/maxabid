<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Friendly strings for Images Generator
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

/*
  $extra_query = tep_db_query("select products_extra_fields_id from " . TABLE_PRODUCTS_EXTRA_FIELDS . " where products_extra_fields_status = '1' and stock_status='1' order by products_extra_fields_order");
  while($extra = tep_db_fetch_array($extra_query) ) {
    $check_query = tep_db_query("select p2pef.products_extra_fields_id, p2pef.products_id, p2pef.products_extra_fields_value from " . TABLE_PRODUCTS_TO_PRODUCTS_EXTRA_FIELDS . " p2pef where p2pef.products_extra_fields_id = '" . (int)$extra['products_extra_fields_id'] . "'");
    while($check_array = tep_db_fetch_array($check_query) ) {
      $values_array = explode(',',$check_array['products_extra_fields_value']);
      $new_stock_array = array();

      for($i=0, $j=count($values_array); $i<$j; $i++ ) {
        if( tep_not_null($values_array[$i]) ) {
          $new_stock_array[$i] = '1';
        } else {
          $new_stock_array[$i] = '0';
        }
      }

      $new_stock = implode(',',$new_stock_array);
      tep_db_query("update " .  TABLE_PRODUCTS_TO_PRODUCTS_EXTRA_FIELDS . " set products_extra_fields_stock = '" . tep_db_input(tep_db_prepare_input($new_stock)) . "' where products_extra_fields_id = '" . (int)$extra['products_extra_fields_id'] . "' and products_id = '" . (int)$check_array['products_id'] . "'");
      echo 'Updated Products-ID: ' . $check_array['products_id'] . '<br />';
    }
  }
*/
  $extra_query = tep_db_query("select products_extra_fields_id from " . TABLE_PRODUCTS_EXTRA_FIELDS . " where products_extra_fields_status = '1' and price_status='1' order by products_extra_fields_order");
  while($extra = tep_db_fetch_array($extra_query) ) {
    $check_query = tep_db_query("select p2pef.products_extra_fields_id, p2pef.products_id, p2pef.products_extra_fields_value from " . TABLE_PRODUCTS_TO_PRODUCTS_EXTRA_FIELDS . " p2pef where p2pef.products_extra_fields_id = '" . (int)$extra['products_extra_fields_id'] . "'");
    while($check_array = tep_db_fetch_array($check_query) ) {
      $values_array = explode(',',$check_array['products_extra_fields_value']);
      $new_price_array = array();

      for($i=0, $j=count($values_array); $i<$j; $i++ ) {
        $new_price_array[$i] = '0';
      }

      $new_price = implode(',',$new_price_array);
      tep_db_query("update " .  TABLE_PRODUCTS_TO_PRODUCTS_EXTRA_FIELDS . " set products_extra_fields_price = '" . tep_db_input(tep_db_prepare_input($new_price)) . "' where products_extra_fields_id = '" . (int)$extra['products_extra_fields_id'] . "' and products_id = '" . (int)$check_array['products_id'] . "'");
      echo 'Updated Products-ID: ' . $check_array['products_id'] . '<br />';
    }
  }

?>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
