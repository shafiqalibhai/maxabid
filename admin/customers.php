<?php
/*
  $Id: customers.php,v 1.82 2003/06/30 13:54:14 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Modifications by Asymmetrics.
// Last Update : March 11th 2007
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// http://www.asymmetrics.com
// Support for active countries added
// Details
// - Added New Account creation
// - Added Countries/Zones filtering
// - Added email notification option
// - Enforce zones with countries
// - Added password change for accounts.
// - Added address book entries insert/update support
// - Implemented different state check by ID
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  $clear_query = tep_db_query("select distinct customers_id from " . TABLE_CUSTOMERS_BASKET . " where (unix_timestamp(now()) - unix_timestamp(customers_basket_date_added)) > "  . GARBAGE_CUSTOMERS_BASKET_TIMEOUT);
  while($clear_array = tep_db_fetch_array($clear_query) ) {
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$clear_array['customers_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$clear_array['customers_id'] . "'");
  }

  //-MS- Garbage Collector Added
  $clear_query = tep_db_query("select customers_info_id from " . TABLE_CUSTOMERS_INFO . " where (unix_timestamp(now()) - unix_timestamp(customers_info_date_of_last_logon)) > "  . GARBAGE_CUSTOMERS_TIMEOUT);
  $clear_array = array();
  while($clear = tep_db_fetch_array($clear_query) ) {
    $clear_array[] = $clear;
    if( count($clear_array) > 1000 )
      break;
  }
  tep_db_free_result($clear_query);

  foreach($clear_array as $key => $value) {
    $reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    while ($reviews = tep_db_fetch_array($reviews_query)) {
      tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews['reviews_id'] . "'");
    }
    tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_REVIEWS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");

    //tep_db_query("delete from " . TABLE_WISHLIST . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    //tep_db_query("delete from " . TABLE_WISHLIST_ATTRIBUTES . " where customers_id = '" . (int)$value['customers_info_id'] . "'");
    //tep_db_query("delete from " . TABLE_WISHLIST_FIELDS . " where customers_id = '" . (int)$value['customers_info_id'] . "'");

    $check_query = tep_db_query("select session_id from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$value['customers_info_id'] . "'");
    while($check_array = tep_db_fetch_array($check_query) ) {
      tep_db_query("delete from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input(tep_db_prepare_input($check_array['session_id'])) . "'");
    }

    tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$value['customers_info_id'] . "'");
  }
  unset($clear_array);
  //-MS- Garbage Collector Added EOM


  $action = (isset($_GET['action']) ? $_GET['action'] : '');

//-MS- Active Countries
  $country = (isset($_POST['entry_country_id']))?tep_db_prepare_input($_POST['entry_country_id']):((int)STORE_COUNTRY);
  $country_flag = (isset($_POST['country_old']) && ($country != $_POST['country_old']))?true:false;
//-MS- Active Countries EOM

  $error = false;
  $processed = false;

//-MS- Active Countries Support Added
  switch ($action) {
    case 'insert':
      $customers_gender = '';
      $customers_firstname = tep_db_prepare_input($_POST['customers_firstname']);
      $customers_lastname = tep_db_prepare_input($_POST['customers_lastname']);
      $customers_email_address = tep_db_prepare_input($_POST['customers_email_address']);
      $customers_telephone = tep_db_prepare_input($_POST['customers_telephone']);
      $customers_fax = tep_db_prepare_input($_POST['customers_fax']);
      $customers_bids = (int)$_POST['customers_bids'];
      $customers_newsletter = isset($_POST['customers_newsletter'])?1:0;

      $password = tep_db_prepare_input($_POST['password']);
      $confirmation = tep_db_prepare_input($_POST['confirmation']);
      $customers_dob = (ACCOUNT_DOB == 'true')?tep_db_prepare_input($_POST['customers_dob']):'';

      $entry_street_address = tep_db_prepare_input($_POST['entry_street_address']);
      $entry_suburb = tep_db_prepare_input($_POST['entry_suburb']);
      $entry_postcode = tep_db_prepare_input($_POST['entry_postcode']);
      $entry_city = tep_db_prepare_input($_POST['entry_city']);
      $entry_country_id = tep_db_prepare_input($_POST['entry_country_id']);

      $entry_company = tep_db_prepare_input($_POST['entry_company']);

      if (ACCOUNT_GENDER == 'true') {
        if( !isset($_POST['customers_gender']) || !tep_not_null($_POST['customers_gender']) ) {
          $error = true;
          $entry_gender_error = true;
        } else {
          $customers_gender = tep_db_prepare_input($_POST['customers_gender']);
          $entry_gender_error = false;
        }
      }

      $entry_password_error = false;
      $entry_password_confirm_error = false;

      if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;
        $entry_password_error = true;
        $entry_password_confirm_error = true;
      } elseif ($password != $confirmation) {
        $error = true;
        $entry_password_error = true;
        $entry_password_confirm_error = true;
      }

      if (isset($_POST['entry_zone_id'])) 
        $entry_zone_id = tep_db_prepare_input($_POST['entry_zone_id']);
      else
        $error = true;

      if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_firstname_error = true;
      } else {
        $entry_firstname_error = false;
      }

      if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_lastname_error = true;
      } else {
        $entry_lastname_error = false;
      }

      if (ACCOUNT_DOB == 'true') {
        if (checkdate(substr(tep_date_raw($customers_dob), 4, 2), substr(tep_date_raw($customers_dob), 6, 2), substr(tep_date_raw($customers_dob), 0, 4))) {
          $entry_date_of_birth_error = false;
        } else {
          $error = true;
          $entry_date_of_birth_error = true;
        }
      }

      if (strlen($customers_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_email_address_error = true;
      } else {
        $entry_email_address_error = false;
      }

      if (!tep_validate_email($customers_email_address)) {
        $error = true;
        $entry_email_address_check_error = true;
      } else {
        $entry_email_address_check_error = false;
      }

      if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_street_address_error = true;
      } else {
        $entry_street_address_error = false;
      }

      if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $entry_post_code_error = true;
      } else {
        $entry_post_code_error = false;
      }

      if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;
        $entry_city_error = true;
      } else {
        $entry_city_error = false;
      }

      if ($entry_country_id == false) {
        $error = true;
        $entry_country_error = true;
      } else {
        $entry_country_error = false;
      }

      if ($entry_country_error == true) {
        $entry_state_error = true;
      } else {
        $zone_id = 0;
        $entry_state_error = false;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "'");
        $check_value = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check_value['total'] > 0);
        if ($entry_state_has_zones == true) {
          $zone_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_id = '" . tep_db_input($entry_zone_id) . "'");
          if (tep_db_num_rows($zone_query) == 1) {
            $zone_values = tep_db_fetch_array($zone_query);
            $entry_zone_id = $zone_values['zone_id'];
            $entry_zone_name = $zone_values['zone_name'];
          } else {
            $error = true;
            $entry_state_error = true;
          }
        } else {
          $error = true;
          $entry_state_error = true;
        }
      }

      if (strlen($customers_telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $entry_telephone_error = true;
      } else {
        $entry_telephone_error = false;
      }

      $check_email = tep_db_query("select customers_email_address from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($customers_email_address) . "'");
      if (tep_db_num_rows($check_email)) {
        $error = true;
        $entry_email_address_exists = true;
      } else {
        $entry_email_address_exists = false;
      }

      if(!$error && !$country_flag) {

        $sql_data_array = array(
          'customers_firstname' => $customers_firstname,
          'customers_lastname' => $customers_lastname,
          'customers_email_address' => $customers_email_address,
          'customers_telephone' => $customers_telephone,
          'customers_fax' => $customers_fax,
          'customers_bids' => $customers_bids,                      
          'customers_newsletter' => $customers_newsletter,
          'customers_password' => tep_encrypt_password($password)
        );

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $customers_gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($customers_dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
        $customers_id = tep_db_insert_id();

        $sql_data_array = array(
          'customers_info_id' => $customers_id,
          'customers_info_date_of_last_logon' => '',
          'customers_info_number_of_logons' => '0',
          'customers_info_date_account_created' => 'now()',
          'customers_info_date_account_last_modified' => 'now()',
          'global_product_notifications' => '0'
        );

        tep_db_perform(TABLE_CUSTOMERS_INFO, $sql_data_array);

        if ($entry_zone_id > 0) $entry_state = '';

        $sql_data_array = array(
          'customers_id' => $customers_id,
          'entry_gender' => $customers_gender,
          'entry_firstname' => $customers_firstname,
          'entry_lastname' => $customers_lastname,
          'entry_street_address' => $entry_street_address,
          'entry_postcode' => $entry_postcode,
          'entry_city' => $entry_city,
          'entry_country_id' => $entry_country_id
        );

        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $entry_company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;

        $sql_data_array['entry_zone_id'] = $entry_zone_id;
        $sql_data_array['entry_state'] = '';

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
        $address_book_id = tep_db_insert_id();

        $sql_data_array = array(
                                'customers_default_address_id' => $address_book_id
                               );

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
        if( isset($_POST['email_notify']) ) {
          $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$entry_country_id . "'");
          $state_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_id = '" . (int)$entry_zone_id . "'");
          $customers_country = tep_db_fetch_array($country_query);
          $customers_state = tep_db_fetch_array($state_query);
          $customers_name = $customers_firstname . ' ' . $customers_lastname;
//-MS- Email Templates added
          $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='create_account_admin'");
          if( $email_array = tep_db_fetch_array($mail_query) ) {
            $customer_name = $customers_firstname . ' ' . $customers_lastname;
            $email_address = $customers_email_address;
            $temp_array = array(
              'CUSTOMER_NAME' => $customer_name,
              'CUSTOMER_FIRSTNAME' => $customers_firstname,
              'CUSTOMER_LASTNAME' => $customers_lastname,
              'CUSTOMER_EMAIL' => $email_address,
              'CUSTOMER_ADDRESS_LINE1' => $entry_street_address,
              'CUSTOMER_ADDRESS_LINE2' => $entry_suburb,
              'CUSTOMER_CITY' => $entry_city,
              'CUSTOMER_STATE' => $customers_state['zone_name'],
              'CUSTOMER_COUNTRY' => $customers_country['countries_name'],
              'CUSTOMER_ZIPCODE' => $entry_postcode,
              'CUSTOMER_TELEPHONE' => $customers_telephone,
              'CUSTOMER_PASSWORD' => $_POST['password'],
              'STORE_NAME' => STORE_NAME,
              'STORE_EMAIL_ADDRESS' => STORE_OWNER_EMAIL_ADDRESS
            );
            $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
            tep_mail($customer_name, $email_address, $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
          } else {
//-MS- Email Templates added EOM
            $email = 'Name: ' . $customers_name .  "\n" . 
                     'Email: ' . $customers_email_address .  "\n" . 
                     'Telephone: ' . $customers_telephone .  "\n" . 
                     'Address: ' . $entry_street_address . "\n" . 
                     'City: ' . $entry_city . "\n" . 
                     'State: ' . $entry_zone_name . "\n" . 
                     'Country: ' . $customers_country['countries_name'] . "\n" . 
                     'Zipcode: ' . $entry_postcode . "\n\n" . 
                     'Password: ' . $password . "\n\n";
                    
            $email .= EMAIL_TEXT . EMAIL_CONTACT;
            tep_mail($customers_name, $customers_email_address, EMAIL_SUBJECT1, $email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
          }
        }

        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_id));

      } else if ($error == true) {
        $cInfo = new objectInfo($_POST);
        if (ACCOUNT_GENDER == 'true') {
          $cInfo->customers_gender = $customers_gender;
        }
        $processed = true;
      }
      break;
    case 'update':
      $customers_id = tep_db_prepare_input($_GET['cID']);
      $customers_firstname = tep_db_prepare_input($_POST['customers_firstname']);
      $customers_lastname = tep_db_prepare_input($_POST['customers_lastname']);
      $customers_email_address = tep_db_prepare_input($_POST['customers_email_address']);
      $customers_telephone = tep_db_prepare_input($_POST['customers_telephone']);
      $customers_fax = tep_db_prepare_input($_POST['customers_fax']);
      $customers_bids = (int)$_POST['customers_bids'];
      $customers_newsletter = isset($_POST['customers_newsletter'])?1:0;

      $password = tep_db_prepare_input($_POST['password']);
      $confirmation = tep_db_prepare_input($_POST['confirmation']);

      $customers_dob = (isset($_POST['customers_dob']))?tep_db_prepare_input($_POST['customers_dob']):'';

      $default_address_id = tep_db_prepare_input($_POST['default_address_id']);
      $entry_street_address = tep_db_prepare_input($_POST['entry_street_address']);
      $entry_suburb = tep_db_prepare_input($_POST['entry_suburb']);
      $entry_postcode = tep_db_prepare_input($_POST['entry_postcode']);
      $entry_city = tep_db_prepare_input($_POST['entry_city']);
      $entry_country_id = tep_db_prepare_input($_POST['entry_country_id']);

      $entry_company = (isset($_POST['entry_company']))?tep_db_prepare_input($_POST['entry_company']):'';
      if (isset($_POST['entry_zone_id'])) $entry_zone_id = tep_db_prepare_input($_POST['entry_zone_id']);

      $entry_password_error = false;
      $entry_password_confirm_error = false;
      if (ACCOUNT_GENDER == 'true') {
        if( !isset($_POST['customers_gender']) || !tep_not_null($_POST['customers_gender']) ) {
          $error = true;
          $entry_gender_error = true;
          $customers_gender = '';
        } else {
          $customers_gender = tep_db_prepare_input($_POST['customers_gender']);
          $entry_gender_error = false;
        }
      }

      if (strlen($customers_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_firstname_error = true;
      } else {
        $entry_firstname_error = false;
      }

      if (strlen($customers_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $entry_lastname_error = true;
      } else {
        $entry_lastname_error = false;
      }

      if (ACCOUNT_DOB == 'true') {
        if (checkdate(substr(tep_date_raw($customers_dob), 4, 2), substr(tep_date_raw($customers_dob), 6, 2), substr(tep_date_raw($customers_dob), 0, 4))) {
          $entry_date_of_birth_error = false;
        } else {
          $error = true;
          $entry_date_of_birth_error = true;
        }
      }

      if (strlen($customers_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_email_address_error = true;
      } else {
        $entry_email_address_error = false;
      }

      if (!tep_validate_email($customers_email_address)) {
        $error = true;
        $entry_email_address_check_error = true;
      } else {
        $entry_email_address_check_error = false;
      }

      if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;
        $entry_street_address_error = true;
      } else {
        $entry_street_address_error = false;
      }

      if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $entry_post_code_error = true;
      } else {
        $entry_post_code_error = false;
      }

      if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;
        $entry_city_error = true;
      } else {
        $entry_city_error = false;
      }

      if ($entry_country_id == false) {
        $error = true;
        $entry_country_error = true;
      } else {
        $entry_country_error = false;
      }

      $entry_zone_name = '';
      if ($entry_country_error == true) {
        $entry_state_error = true;
      } else {
        $zone_id = 0;
        $entry_state_error = false;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "'");
        $check_value = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check_value['total'] > 0);
        if ($entry_state_has_zones == true) {
          //$zone_query = tep_db_query("select zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_name = '" . tep_db_input($entry_state) . "'");
          $zone_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_id = '" . (int)$entry_zone_id . "'");
          if (tep_db_num_rows($zone_query) == 1) {
            $zone_values = tep_db_fetch_array($zone_query);
            $entry_zone_name = $zone_values['zone_name'];
            $entry_zone_id = $zone_values['zone_id'];
          } else {
            $error = true;
            $entry_state_error = true;
          }
        } else {
          if ($entry_state == false) {
            $error = true;
            $entry_state_error = true;
          }
        }
      }


      if (strlen($customers_telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $entry_telephone_error = true;
      } else {
        $entry_telephone_error = false;
      }

      $check_email = tep_db_query("select customers_email_address from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($customers_email_address) . "' and customers_id != '" . (int)$customers_id . "'");
      if (tep_db_num_rows($check_email)) {
        $error = true;
        $entry_email_address_exists = true;
      } else {
        $entry_email_address_exists = false;
      }

      if(!$error && !$country_flag) {

        $sql_data_array = array(
          'customers_firstname' => $customers_firstname,
          'customers_lastname' => $customers_lastname,
          'customers_email_address' => $customers_email_address,
          'customers_telephone' => $customers_telephone,
          'customers_fax' => $customers_fax,
          'customers_bids' => $customers_bids,
          'customers_newsletter' => $customers_newsletter,
        );

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $customers_gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($customers_dob);


        if (strlen($password) >= ENTRY_PASSWORD_MIN_LENGTH && $password == $confirmation) {
          $sql_data_array['customers_password'] = tep_encrypt_password($password);
        }


        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");

        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customers_id . "'");

        if ($entry_zone_id > 0) $entry_state = '';

        $sql_data_array = array('entry_firstname' => $customers_firstname,
                                'entry_lastname' => $customers_lastname,
                                'entry_street_address' => $entry_street_address,
                                'entry_postcode' => $entry_postcode,
                                'entry_city' => $entry_city,
                                'entry_country_id' => $entry_country_id);

        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $entry_company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;

        if ($entry_zone_id > 0) {
          $sql_data_array['entry_zone_id'] = $entry_zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $messageStack->add_session('No Zones for this country. Setup at least one zone first', 'error');
          tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=edit'));
        }

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "' and address_book_id = '" . (int)$default_address_id . "'");

        if( isset($_POST['email_notify']) ) {
          $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$entry_country_id . "'");
          $customers_country = tep_db_fetch_array($country_query);
          $customers_name = $customers_firstname . ' ' . $customers_lastname;
          $email = 'Name: ' . $customers_name .  "\n" . 
                   'Email: ' . $customers_email_address .  "\n" . 
                   'Telephone: ' . $customers_telephone .  "\n" . 
                   'Address: ' . $entry_street_address . "\n" . 
                   'City: ' . $entry_city . "\n" . 
                   'State: ' . $entry_zone_name . "\n" . 
                   'Country: ' . $customers_country['countries_name'] . "\n" . 
                   'Zipcode: ' . $entry_postcode . "\n\n";

          if (strlen($password) >= ENTRY_PASSWORD_MIN_LENGTH && $password == $confirmation) {
            $email .= 'Password: ' . $password . "\n\n";
          }
          $email .= EMAIL_TEXT . EMAIL_CONTACT;
          tep_mail($customers_name, $customers_email_address, EMAIL_SUBJECT2, $email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
        }

        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $customers_id));

      } else if ($error == true) {
        $cInfo = new objectInfo($_POST);
        $cInfo->customers_default_address_id = $cInfo->default_address_id;
        if (ACCOUNT_GENDER == 'true') {
          $cInfo->customers_gender = $customers_gender;
        }
        $processed = true;
      }

      break;
    case 'deleteconfirm':
      $customers_id = $_GET['cID'];

      if (isset($_POST['delete_reviews']) && ($_POST['delete_reviews'] == 'on')) {
        $reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
        while ($reviews = tep_db_fetch_array($reviews_query)) {
          tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$reviews['reviews_id'] . "'");
        }

        tep_db_query("delete from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers_id . "'");
      } else {
        tep_db_query("update " . TABLE_REVIEWS . " set customers_id = null where customers_id = '" . (int)$customers_id . "'");
      }
      tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customers_id . "'");
      tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customers_id . "'");

      //tep_db_query("delete from " . TABLE_WISHLIST . " where customers_id = '" . (int)$customers_id . "'");
      //tep_db_query("delete from " . TABLE_WISHLIST_ATTRIBUTES . " where customers_id = '" . (int)$customers_id . "'");
      //tep_db_query("delete from " . TABLE_WISHLIST_FIELDS . " where customers_id = '" . (int)$customers_id . "'");

      $check_query = tep_db_query("select session_id from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$customers_id . "'");
      while($check_array = tep_db_fetch_array($check_query) ) {
        tep_db_query("delete from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input(tep_db_prepare_input($check_array['session_id'])) . "'");
      }
      tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$customers_id . "'");

      tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action'))));
      break;

    case 'insert_entry':
      if( $country_flag ) {
        $action = 'ab_edit';
        break;
      }

      $customers_id = tep_db_prepare_input($_GET['cID']);
      $entry_firstname = tep_db_prepare_input($_POST['entry_firstname']);
      $entry_lastname = tep_db_prepare_input($_POST['entry_lastname']);
      $entry_gender = tep_db_prepare_input($_POST['entry_gender']);
      $entry_street_address = tep_db_prepare_input($_POST['entry_street_address']);
      $entry_suburb = tep_db_prepare_input($_POST['entry_suburb']);
      $entry_postcode = tep_db_prepare_input($_POST['entry_postcode']);
      $entry_city = tep_db_prepare_input($_POST['entry_city']);
      $entry_country_id = tep_db_prepare_input($_POST['entry_country_id']);
      $entry_company = tep_db_prepare_input($_POST['entry_company']);

      if (isset($_POST['entry_zone_id'])) {
        $entry_zone_id = tep_db_prepare_input($_POST['entry_zone_id']);
      } else {
        $messageStack->add_session('Invalid Zone Selected', 'error');
        $error = true;
      }

      if (strlen($entry_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $messageStack->add_session('Check First Name Minimum Length', 'error');
        $error = true;
        $entry_firstname_error = true;
      } else {
        $entry_firstname_error = false;
      }

      if (strlen($entry_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $messageStack->add_session('Check Last Name Minimum Length', 'error');
        $error = true;
        $entry_lastname_error = true;
      } else {
        $entry_lastname_error = false;
      }

      if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $messageStack->add_session('Check Street Address Minimum Length', 'error');
        $error = true;
        $entry_street_address_error = true;
      } else {
        $entry_street_address_error = false;
      }

      if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $messageStack->add_session('Check Postcode Minimum Length', 'error');
        $error = true;
        $entry_post_code_error = true;
      } else {
        $entry_post_code_error = false;
      }

      if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
        $messageStack->add_session('Check City Minimum Length', 'error');
        $error = true;
        $entry_city_error = true;
      } else {
        $entry_city_error = false;
      }

      if ($entry_country_id == false) {
        $messageStack->add_session('Invalid Country Selected', 'error');
        $error = true;
        $entry_country_error = true;
      } else {
        $entry_country_error = false;
      }


      if ($entry_country_error == true) {
        $entry_state_error = true;
      } else {
        $zone_id = 0;
        $entry_state_error = false;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "'");
        $check_value = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check_value['total'] > 0);
        if ($entry_state_has_zones == true) {
          $zone_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_id = '" . tep_db_input($entry_zone_id) . "'");
          if (tep_db_num_rows($zone_query) == 1) {
            $zone_values = tep_db_fetch_array($zone_query);
            $entry_zone_id = $zone_values['zone_id'];
            $entry_zone_name = $zone_values['zone_name'];
          } else {
            $error = true;
            $entry_state_error = true;
          }
        } else {
          $error = true;
          $entry_state_error = true;
        }
      }

      if( !$error && !$country_flag) {
        $sql_data_array = array(
          'customers_id' => $customers_id,
          'entry_firstname' => $entry_firstname,
          'entry_lastname' => $entry_lastname,
          'entry_street_address' => $entry_street_address,
          'entry_postcode' => $entry_postcode,
          'entry_city' => $entry_city,
          'entry_country_id' => $entry_country_id
        );

        if ($entry_zone_id > 0) $entry_state = '';
        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $entry_gender;
        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $entry_company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;

        $sql_data_array['entry_zone_id'] = $entry_zone_id;
        $sql_data_array['entry_state'] = '';

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
        $address_book_id = tep_db_insert_id();

        if( isset($_POST['default_address_id']) ) {
          $sql_data_array = array(
                                  'customers_default_address_id' => tep_db_prepare_input($address_book_id)
                                 );

          tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
        }
        $messageStack->add_session('New Address Book Entry added', 'success');
        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action', 'abID')) . 'action=ab_edit&abID=' . $address_book_id));
      }
      tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action', 'abID')) . 'action=ab_edit'));
      break;
    case 'update_entry':
      if( $country_flag ) {
        $action = 'ab_edit';
        break;
      }
      $customers_id = tep_db_prepare_input($_GET['cID']);
      $address_book_id = tep_db_prepare_input($_POST['address_book_id']);
      $entry_firstname = tep_db_prepare_input($_POST['entry_firstname']);
      $entry_lastname = tep_db_prepare_input($_POST['entry_lastname']);
      $entry_gender = tep_db_prepare_input($_POST['entry_gender']);
      $entry_street_address = tep_db_prepare_input($_POST['entry_street_address']);
      $entry_suburb = tep_db_prepare_input($_POST['entry_suburb']);
      $entry_postcode = tep_db_prepare_input($_POST['entry_postcode']);
      $entry_city = tep_db_prepare_input($_POST['entry_city']);
      $entry_country_id = tep_db_prepare_input($_POST['entry_country_id']);
      $entry_company = tep_db_prepare_input($_POST['entry_company']);

      if (isset($_POST['entry_zone_id'])) $entry_zone_id = tep_db_prepare_input($_POST['entry_zone_id']);

      if (strlen($entry_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add_session('Check First Name Minimum Length', 'error');
        $entry_firstname_error = true;
      } else {
        $entry_firstname_error = false;
      }

      if (strlen($entry_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $messageStack->add_session('Check Last Name Minimum Length', 'error');
        $error = true;
        $entry_lastname_error = true;
      } else {
        $entry_lastname_error = false;
      }


      if (strlen($entry_street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $messageStack->add_session('Check Street Address Minimum Length', 'error');
        $error = true;
        $entry_street_address_error = true;
      } else {
        $entry_street_address_error = false;
      }

      if (strlen($entry_postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $messageStack->add_session('Check PostCode Minimum Length', 'error');
        $entry_post_code_error = true;
      } else {
        $entry_post_code_error = false;
      }

      if (strlen($entry_city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;
        $entry_city_error = true;
      } else {
        $entry_city_error = false;
      }

      if ($entry_country_id == false) {
        $messageStack->add_session('Invalid Country Selected', 'error');
        $error = true;
        $entry_country_error = true;
      } else {
        $entry_country_error = false;
      }

      if ($entry_country_error == true) {
        $entry_state_error = true;
      } else {
        $zone_id = 0;
        $entry_state_error = false;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "'");
        $check_value = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check_value['total'] > 0);
        if ($entry_state_has_zones == true) {
          $zone_query = tep_db_query("select zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$entry_country_id . "' and zone_id = '" . tep_db_input($entry_zone_id) . "'");
          if (tep_db_num_rows($zone_query) == 1) {
            $zone_values = tep_db_fetch_array($zone_query);
            $entry_zone_id = $zone_values['zone_id'];
          } else {
            $error = true;
            $entry_state_error = true;
          }
        } else {
          if ($entry_state == false) {
            $error = true;
            $entry_state_error = true;
          }
        }
      }

      if( !$error && !$country_flag) {
        $sql_data_array = array('entry_firstname' => $entry_firstname,
                                'entry_lastname' => $entry_lastname,
                                'entry_street_address' => $entry_street_address,
                                'entry_postcode' => $entry_postcode,
                                'entry_city' => $entry_city,
                                'entry_country_id' => $entry_country_id);

        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $entry_gender;
        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $entry_company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $entry_suburb;


        if ($entry_zone_id > 0) {
          $sql_data_array['entry_zone_id'] = $entry_zone_id;
          $sql_data_array['entry_state'] = '';
        } else {
          $messageStack->add_session('No Zones for this country. Setup at least one zone first', 'error');
          tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=ab_edit'));
        }

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "' and address_book_id = '" . (int)$address_book_id . "'");

        if( isset($_POST['default_address_id']) ) {
          $sql_data_array = array(
                                  'customers_default_address_id' => tep_db_prepare_input($_POST['address_book_id'])
                                 );

          tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
        }
        $messageStack->add_session('Address Book Entry updated', 'success');
      }
      tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=ab_edit'));
      break;
    case 'deleteconfirm_entry':
      $customers_id = tep_db_prepare_input($_GET['cID']);
      $address_book_id = tep_db_prepare_input($_POST['address_book_id']);
      // Check if more that one address book entries is available for this customer
      $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "' and address_book_id <> '" . (int)$address_book_id . "'");
      $check_count = tep_db_num_rows($check_query);
      if( !$check_count ) {
        $messageStack->add_session('Cannot remove. At least one address book entry must be present with each customer', 'error');
        tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('abID', 'action', 'apage')) . 'action=ab_edit&abID=' . $address_book_id));
      }

      $check_array = array();
      while($check_array[] = tep_db_fetch_array($check_query) );
      array_pop($check_array);

      // Check if the default address book id was just removed and adjust customers table
      $default_query = tep_db_query("select customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
      $default_entry = tep_db_fetch_array($default_query);
      if( $default_entry['customers_default_address_id'] == $address_book_id ) {
        $sql_data_array = array(
                                'customers_default_address_id' => $check_array[0]['address_book_id']
                               );
        $messageStack->add_session('Default Address Book entry changed for this customer', 'warning');
        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customers_id . "'");
      }
      tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "' and address_book_id = '" . (int)$address_book_id . "'");
      $messageStack->add_session('Address Book entry removed', 'success');
      tep_redirect(tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('abID', 'action', 'apage')) . 'action=ab_edit&abID=' . $check_array[0]['address_book_id']));
      break;
    default:
      $default = 1;
      break;
  }

  if( isset($default) || ($country_flag && $action == 'ab_edit') ) {
    if( isset($_GET['apage']) && isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
      $address_book_query = tep_db_query("select * from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_GET['cID'] . "'");
      $page_index = 1;
      while( ($address_book = tep_db_fetch_array($address_book_query)) && $page_index != $_GET['apage'] ) {
        $page_index++;
      }
      $abInfo = new objectInfo($address_book);
      if( !isset($_POST['entry_country_id']) )
        $country = $abInfo->entry_country_id;

    } elseif( isset($_GET['abID']) && tep_not_null($_GET['abID']) && isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
      $address_book_query = tep_db_query("select * from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_GET['cID'] . "'");
      $page_index = 1;
      while( ($address_book = tep_db_fetch_array($address_book_query)) && $address_book['address_book_id'] != $_GET['abID'] ) {
        $page_index++;
      }

      $abInfo = new objectInfo($address_book);
      if( !isset($_POST['entry_country_id']) )
        $country = $abInfo->entry_country_id;
    } elseif( isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
      $customers_query = tep_db_query("select c.customers_id, c.customers_gender, c.customers_firstname, c.customers_lastname, c.customers_dob, c.customers_email_address, a.entry_company, a.entry_street_address, a.entry_suburb, a.entry_postcode, a.entry_city, a.entry_state, a.entry_zone_id, a.entry_country_id, c.customers_telephone, c.customers_fax, c.customers_bids, c.customers_newsletter, c.customers_default_address_id from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_default_address_id = a.address_book_id where a.customers_id = c.customers_id and c.customers_id = '" . (int)$_GET['cID'] . "'");
      $customers = tep_db_fetch_array($customers_query);
      $cInfo = new objectInfo($customers);
      if( !isset($_POST['entry_country_id']) )
        $country = $cInfo->entry_country_id;
    }
  }
//-MS- Active Countries Support Added EOM

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<?php
  if ($action == 'edit' || $action == 'update') {
?>
<script language="javascript"><!--

function check_form() {
  var error = 0;
  var error_message = "<?php echo JS_ERROR; ?>";

  var customers_firstname = document.customers.customers_firstname.value;
  var customers_lastname = document.customers.customers_lastname.value;
<?php if (ACCOUNT_COMPANY == 'true') echo 'var entry_company = document.customers.entry_company.value;' . "\n"; ?>
<?php if (ACCOUNT_DOB == 'true') echo 'var customers_dob = document.customers.customers_dob.value;' . "\n"; ?>
  var customers_email_address = document.customers.customers_email_address.value;
  var entry_street_address = document.customers.entry_street_address.value;
  var entry_postcode = document.customers.entry_postcode.value;
  var entry_city = document.customers.entry_city.value;
  var customers_telephone = document.customers.customers_telephone.value;

<?php if (ACCOUNT_GENDER == 'true') { ?>
  if (document.customers.customers_gender[0].checked || document.customers.customers_gender[1].checked) {
  } else {
    error_message = error_message + "<?php echo JS_GENDER; ?>";
    error = 1;
  }
<?php } ?>

  if (customers_firstname == "" || customers_firstname.length < <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_FIRST_NAME; ?>";
    error = 1;
  }

  if (customers_lastname == "" || customers_lastname.length < <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_LAST_NAME; ?>";
    error = 1;
  }

<?php if (ACCOUNT_DOB == 'true') { ?>
  if (customers_dob == "" || customers_dob.length < <?php echo ENTRY_DOB_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_DOB; ?>";
    error = 1;
  }
<?php } ?>

  if (customers_email_address == "" || customers_email_address.length < <?php echo ENTRY_EMAIL_ADDRESS_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_EMAIL_ADDRESS; ?>";
    error = 1;
  }

  if (entry_street_address == "" || entry_street_address.length < <?php echo ENTRY_STREET_ADDRESS_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_ADDRESS; ?>";
    error = 1;
  }

  if (entry_postcode == "" || entry_postcode.length < <?php echo ENTRY_POSTCODE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_POST_CODE; ?>";
    error = 1;
  }

  if (entry_city == "" || entry_city.length < <?php echo ENTRY_CITY_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_CITY; ?>";
    error = 1;
  }

<?php
/*
  if (ACCOUNT_STATE == 'true') {
?>
  if (document.customers.elements['entry_state'].type != "hidden") {
    if (document.customers.entry_state.value == '' || document.customers.entry_state.value.length < <?php echo ENTRY_STATE_MIN_LENGTH; ?> ) {
       error_message = error_message + "<?php echo JS_STATE; ?>";
       error = 1;
    }
  }
<?php
  }
*/
?>

  if (document.customers.elements['entry_country_id'].type != "hidden") {
    if (document.customers.entry_country_id.value == 0) {
      error_message = error_message + "<?php echo JS_COUNTRY; ?>";
      error = 1;
    }
  }

  if (customers_telephone == "" || customers_telephone.length < <?php echo ENTRY_TELEPHONE_MIN_LENGTH; ?>) {
    error_message = error_message + "<?php echo JS_TELEPHONE; ?>";
    error = 1;
  }

  if (error == 1) {
    alert(error_message);
    return false;
  } else {
    return true;
  }
}
//--></script>
<?php
  }
?>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ($action == 'edit' || $action == 'update' || $action == 'insert') {

    if($action == 'edit' && !isset($cInfo) ) {
      $customers = array(
        'customers_firstname' => '',
        'customers_lastname' => '',
        'customers_email_address' => '',
        'customers_telephone' => '',
        'customers_fax' => '',
        'customers_bids' => 0,
        'customers_newsletter' => '',
        'customers_password' => '',
        'customers_gender' => '',
        'customers_dob' => '',
        'customers_firstname' => '',
        'customers_lastname' => '',
        'default_address_book_id' => '',
        'entry_gender' => '',
        'entry_suburb' => '',
        'entry_company' => '',
        'entry_street_address' => '',
        'entry_postcode' => '',
        'entry_city' => '',
        'entry_zone_id' => '',
        'entry_country_id' => $country
      );
      $cInfo = new objectInfo($customers);
    }
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('customers', FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=' . ((isset($_GET['cID']) && tep_not_null($_GET['cID']))?'update':'insert'), 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_PERSONAL; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo (isset($_GET['cID']) && tep_not_null($_GET['cID']))?tep_draw_hidden_field('default_address_id', $cInfo->customers_default_address_id):''; ?><table border="0" cellspacing="2" cellpadding="2">
<?php
    if (ACCOUNT_GENDER == 'true') {
?>
              <tr>
                <td class="main"><?php echo ENTRY_GENDER; ?></td>
                <td class="main">
<?php
      if ($error == true && $entry_gender_error == true) {
        echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . ENTRY_GENDER_ERROR;
      } else {
        echo tep_draw_radio_field('customers_gender', 'm', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('customers_gender', 'f', false, $cInfo->customers_gender) . '&nbsp;&nbsp;' . FEMALE;
      }
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td class="main"><?php echo ENTRY_FIRST_NAME; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_firstname_error == true) {
      echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32"') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
    } else {
      echo tep_draw_input_field('customers_firstname', $cInfo->customers_firstname, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_LAST_NAME; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_lastname_error == true) {
      echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
    } else {
      echo tep_draw_input_field('customers_lastname', $cInfo->customers_lastname, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
<?php
    if (ACCOUNT_DOB == 'true') {
?>
              <tr>
                <td class="main"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
                <td class="main">

<?php
      if ($error == true && $entry_date_of_birth_error == true) {
        echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10"') . '&nbsp;' . ENTRY_DATE_OF_BIRTH_ERROR;
      } else {
        echo tep_draw_input_field('customers_dob', tep_date_short($cInfo->customers_dob), 'maxlength="10"', true);
      }
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td class="main"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_email_address_error == true) {
      echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR;
    } elseif ($error == true && $entry_email_address_check_error == true) {
      echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
    } elseif ($error == true && $entry_email_address_exists == true) {
      echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
    } else {
      echo tep_draw_input_field('customers_email_address', $cInfo->customers_email_address, 'maxlength="96"', true);
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
<?php
    if (ACCOUNT_COMPANY == 'true') {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_COMPANY; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_COMPANY; ?></td>
                <td class="main">
<?php
      echo tep_draw_input_field('entry_company', $cInfo->entry_company, 'maxlength="32"');
?>
                </td>
              </tr>
            </table></td>
          </tr>
<?php
    }
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_ADDRESS; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                <td class="main">
<?php
  if ($error == true && $entry_street_address_error == true) {
    echo tep_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64"') . '&nbsp;' . ENTRY_STREET_ADDRESS_ERROR;
  } else {
    echo tep_draw_input_field('entry_street_address', $cInfo->entry_street_address, 'maxlength="64"', true);
  }
?>
                </td>
              </tr>
<?php
    if (ACCOUNT_SUBURB == 'true') {
?>
              <tr>
                <td class="main"><?php echo ENTRY_SUBURB; ?></td>
                <td class="main">
<?php
      echo tep_draw_input_field('entry_suburb', $cInfo->entry_suburb, 'maxlength="32"');
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td class="main"><?php echo ENTRY_POST_CODE; ?></td>
                <td class="main">
<?php
  if ($error == true && $entry_post_code_error == true) {
    echo tep_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="8"') . '&nbsp;' . ENTRY_POST_CODE_ERROR;
  } else {
    echo tep_draw_input_field('entry_postcode', $cInfo->entry_postcode, 'maxlength="8"', true);
  }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_CITY; ?></td>
                <td class="main">
<?php
  if ($error == true && $entry_city_error == true) {
    echo tep_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32"') . '&nbsp;' . ENTRY_CITY_ERROR;
  } else {
    echo tep_draw_input_field('entry_city', $cInfo->entry_city, 'maxlength="32"', true);
  }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_STATE; ?></td>
                <td class="main">
<?php
      $zones_array = array();
      $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . tep_db_input($country) . "' order by zone_name");
      while ($zones_values = tep_db_fetch_array($zones_query)) {
        $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
      }
      echo tep_draw_pull_down_menu('entry_zone_id', $zones_array, $cInfo->entry_zone_id);


?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_COUNTRY; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_country_error == true) {
      echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $country, 'onChange="this.form.submit();"') . tep_draw_hidden_field('country_old', $country) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
    } else {
      echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $country, 'onChange="this.form.submit();"') . tep_draw_hidden_field('country_old', $country);
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_CONTACT; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_telephone_error == true) {
      echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32"') . '&nbsp;' . ENTRY_TELEPHONE_NUMBER_ERROR;
    } else {
      echo tep_draw_input_field('customers_telephone', $cInfo->customers_telephone, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_FAX_NUMBER; ?></td>
                <td class="main">
<?php
    echo tep_draw_input_field('customers_fax', $cInfo->customers_fax, 'maxlength="32"');
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>

          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_PASSWORD; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_PASSWORD; ?></td>
                <td class="main">
<?php
    if($action != 'update' && $error == true && $entry_password_error == true) {
      echo tep_draw_input_field('password', (isset($_POST['password']))?$_POST['password']:'', 'maxlength="32"') . '&nbsp;' . ENTRY_PASSWORD_ERROR;
    } else {
      echo tep_draw_input_field('password', (isset($_POST['password']))?$_POST['password']:'', 'maxlength="32"', (isset($_GET['cID']) && tep_not_null($_GET['cID']))?false:true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_PASSWORD_CONFIRM; ?></td>
                <td class="main">
<?php
    if($action != 'update' && $error == true && $entry_password_confirm_error == true) {
      echo tep_draw_input_field('confirmation', (isset($_POST['password']))?$_POST['confirmation']:'', 'maxlength="32"') . '&nbsp;' . ENTRY_PASSWORD_CONFIRM_ERROR;
    } else {
      echo tep_draw_input_field('confirmation', (isset($_POST['password']))?$_POST['confirmation']:'', 'maxlength="32"', (isset($_GET['cID']) && tep_not_null($_GET['cID']))?false:true);
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>

          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_OPTIONS; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_ACCOUNT_POINTS; ?></td>
                <td class="main">
<?php
    echo tep_draw_input_field('customers_bids', $cInfo->customers_bids, 'maxlength="32"');
?>
                </td>
              </tr>

              <tr>
                <td class="main"><?php echo ENTRY_NEWSLETTER; ?></td>
                <td class="main">
<?php
    echo tep_draw_checkbox_field('customers_newsletter', 'on', ((isset($cInfo->customers_newsletter) && $cInfo->customers_newsletter)?true:false));
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_ACCOUNT_NOTIFY; ?></td>
                <td class="main">
<?php
    echo tep_draw_checkbox_field('email_notify', 'on', false);
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
<?php
    if( isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
?>
            <td align="right" class="main"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
    } else {
?>
            <td align="right" class="main"><?php echo tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
<?php
    }
?>
          </tr>
        </table></form></td>
      </tr>
<?php
//-MS- Active Countries support Added
  } elseif( $action == 'ab_edit' && isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
    $customers_query = tep_db_query("select CONCAT(customers_firstname, ' ', customers_lastname) as fullname, customers_default_address_id as default_address_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$_GET['cID'] . "'");
    $customers_array = tep_db_fetch_array($customers_query);
    $new_flag = false;
    if( isset($_GET['abID']) && tep_not_null($_GET['abID']) ) {
      $address_book_query_raw = "select * from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_GET['cID'] . "'";
      $address_book_split = new splitPageResults($page_index, 1, $address_book_query_raw, $address_book_query_numrows, 'address_book_id');
      $address_book_query = tep_db_query($address_book_query_raw);
      $address_book = tep_db_fetch_array($address_book_query);
    } elseif( isset($abInfo) && is_object($abInfo) ) {
      $address_book_query_raw = "select * from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$_GET['cID'] . "'";
      $address_book_split = new splitPageResults($page_index, 1, $address_book_query_raw, $address_book_query_numrows, 'address_book_id');
      $address_book_query = tep_db_query($address_book_query_raw);
      $address_book = tep_db_fetch_array($address_book_query);
    } else {
      $address_book = array(
                            'customers_id' => (int)$_GET['cID'],
                            'address_book_id' => '',
                            'entry_gender' => '',
                            'entry_suburb' => '',
                            'entry_company' => '',
                            'entry_firstname' => '',
                            'entry_lastname' => '',
                            'entry_street_address' => '',
                            'entry_postcode' => '',
                            'entry_city' => '',
                            'entry_zone_id' => '',
                            'entry_country_id' => $country
                           );
      $new_flag = true;
    }

    $abInfo = new objectInfo($address_book);
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo sprintf(HEADING_ADDRESS_BOOK, $customers_array['fullname']); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    if( !$new_flag ) {
?>
      <tr>
        <td class="smalltext"><b><?php echo 'Address Book ID:&nbsp;' . $abInfo->address_book_id; ?></b></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText" valign="top"><?php echo $address_book_split->display_count($address_book_query_numrows, 1, $page_index, TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
            <td class="smallText" align="right"><?php echo $address_book_split->display_links($address_book_query_numrows, 1, MAX_DISPLAY_PAGE_LINKS, $page_index, tep_get_all_get_params(array('apage')), 'apage' ); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    }
?>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('address_book', FILENAME_CUSTOMERS, tep_get_all_get_params(array('action', 'abID')) . 'action=' . ($new_flag?'insert_entry':('update_entry&abID=' . $abInfo->address_book_id)), 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_PERSONAL; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
<?php
    if (ACCOUNT_GENDER == 'true') {
?>
              <tr>
                <td class="main"><?php echo ENTRY_GENDER; ?></td>
                <td class="main">
<?php
      if ($error == true && $entry_gender_error == true) {
        echo tep_draw_radio_field('entry_gender', 'm', false, $abInfo->entry_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('entry_gender', 'f', false, $abInfo->entry_gender) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . ENTRY_GENDER_ERROR;
      } else {
        echo tep_draw_radio_field('entry_gender', 'm', false, $abInfo->entry_gender) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('entry_gender', 'f', false, $abInfo->entry_gender) . '&nbsp;&nbsp;' . FEMALE;
      }
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td class="main"><?php echo ENTRY_FIRST_NAME; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_firstname_error == true) {
      echo tep_draw_input_field('entry_firstname', $abInfo->entry_firstname, 'maxlength="32"') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
    } else {
      echo tep_draw_input_field('entry_firstname', $abInfo->entry_firstname, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_LAST_NAME; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_lastname_error == true) {
      echo tep_draw_input_field('entry_lastname', $abInfo->entry_lastname, 'maxlength="32"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
    } else {
      echo tep_draw_input_field('entry_lastname', $abInfo->entry_lastname, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
<?php
    if (ACCOUNT_COMPANY == 'true') {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_COMPANY; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_COMPANY; ?></td>
                <td class="main">
<?php
      echo tep_draw_input_field('entry_company', $abInfo->entry_company, 'maxlength="32"');
?>
                </td>
              </tr>
            </table></td>
          </tr>
<?php
    }
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="formAreaTitle"><?php echo CATEGORY_ADDRESS; ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_street_address_error == true) {
      echo tep_draw_input_field('entry_street_address', $abInfo->entry_street_address, 'maxlength="64"') . '&nbsp;' . ENTRY_STREET_ADDRESS_ERROR;
    } else {
      echo tep_draw_input_field('entry_street_address', $abInfo->entry_street_address, 'maxlength="64"', true);
    }
?>
                </td>
              </tr>
<?php
    if (ACCOUNT_SUBURB == 'true') {
?>
              <tr>
                <td class="main"><?php echo ENTRY_SUBURB; ?></td>
                <td class="main">
<?php
      echo tep_draw_input_field('entry_suburb', $abInfo->entry_suburb, 'maxlength="32"');
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td class="main"><?php echo ENTRY_POST_CODE; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_post_code_error == true) {
      echo tep_draw_input_field('entry_postcode', $abInfo->entry_postcode, 'maxlength="8"') . '&nbsp;' . ENTRY_POST_CODE_ERROR;
    } else {
      echo tep_draw_input_field('entry_postcode', $abInfo->entry_postcode, 'maxlength="8"', true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_CITY; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_city_error == true) {
      echo tep_draw_input_field('entry_city', $abInfo->entry_city, 'maxlength="32"') . '&nbsp;' . ENTRY_CITY_ERROR;
    } else {
      echo tep_draw_input_field('entry_city', $abInfo->entry_city, 'maxlength="32"', true);
    }
?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_STATE; ?></td>
                <td class="main">
<?php
    $zones_array = array();
    $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . tep_db_input($country) . "' order by zone_name");
    while ($zones_values = tep_db_fetch_array($zones_query)) {
      $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
    }
    echo tep_draw_pull_down_menu('entry_zone_id', $zones_array, (isset($_GET['abID']) && tep_not_null($_GET['abID']))?$abInfo->entry_zone_id:$zones_array[0]['id']);


?>
                </td>
              </tr>
              <tr>
                <td class="main"><?php echo ENTRY_COUNTRY; ?></td>
                <td class="main">
<?php
    if ($error == true && $entry_country_error == true) {
      echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $country, 'onChange="this.form.submit();"') . tep_draw_hidden_field('country_old', $country) . '&nbsp;' . ENTRY_COUNTRY_ERROR;
    } else {
      echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries(), $country, 'onChange="this.form.submit();"') . tep_draw_hidden_field('country_old', $country);
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
          </tr>
          <tr>
            <td class="formArea"><table border="0" cellspacing="2" cellpadding="2">
              <tr>
                <td class="main"><?php echo ENTRY_DEFAULT_ADDRESS; ?></td>
                <td class="main"><?php echo tep_draw_checkbox_field('default_address_id', 'on', ($abInfo->address_book_id==$customers_array['default_address_id'])?true:false) . tep_draw_hidden_field('address_book_id', $abInfo->address_book_id); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    if( !$new_flag ) {
?>
            <td align="right" class="main"><?php echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action', 'apage', 'abID'))) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=ab_delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a> '; ?></td>
<?php
    } else {
?>
            <td align="right" class="main"><?php echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action', 'apage', 'abID'))) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_insert.gif', IMAGE_INSERT); ?></td>
<?php
    }
?>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
    if( !$new_flag ) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText" valign="top"><?php echo $address_book_split->display_count($address_book_query_numrows, 1, $page_index, TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
            <td class="smallText" align="right"><?php echo $address_book_split->display_links($address_book_query_numrows, 1, MAX_DISPLAY_PAGE_LINKS, $page_index, tep_get_all_get_params(array('apage')), 'apage' ); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    }

  } elseif( $action == 'ab_delete' && isset($_GET['abID']) && tep_not_null($_GET['abID']) && isset($_GET['cID']) && tep_not_null($_GET['cID']) ) {
    $customers_query = tep_db_query("select CONCAT(customers_firstname, ' ', customers_lastname) as fullname, customers_default_address_id as default_address_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$_GET['cID'] . "'");
    $customers_array = tep_db_fetch_array($customers_query);
?>

      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo sprintf(HEADING_ADDRESS_BOOK_DELETE, $customers_array['fullname']); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('delete_address_entry', FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_entry'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><?php echo TEXT_INFO_ADDRESS_BOOK_DELETE; ?></td>
          </tr>
          <tr>
            <td><hr /></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="30%"><?php echo 'Address Book Entry:'; ?></td>
                    <td><?php echo $abInfo->address_book_id . tep_draw_hidden_field('address_book_id', $abInfo->address_book_id); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo 'Entry Name:'; ?></td>
                    <td><?php echo $abInfo->entry_firstname . ' ' . $abInfo->entry_lastname; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo 'Street Address:'; ?></td>
                    <td><?php echo $abInfo->entry_street_address; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo 'Post Code:'; ?></td>
                    <td><?php echo $abInfo->entry_postcode; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo 'State:'; ?></td>
                    <td><?php echo tep_get_zone_name($abInfo->entry_country_id, $abInfo->entry_zone_id, ''); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo 'Country:'; ?></td>
                    <td><?php echo tep_get_country_name($abInfo->entry_country_id); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="main"><?php echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('action')) . 'action=ab_edit') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM); ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
//-MS- Active Countries support Added EOM
?>

<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr><?php echo tep_draw_form('search', FILENAME_CUSTOMERS, '', 'get'); ?>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td class="smallText" align="right"><?php echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search') . tep_draw_hidden_field(tep_session_name(),tep_session_id()); ?></td>
          </form></tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="formArea" valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ID; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LASTNAME; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRSTNAME; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACCOUNT_CREATED; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACCOUNT_LAST_LOGON; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                  </tr>
<?php
    $search = '';
    if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
      $keywords = tep_db_input(tep_db_prepare_input($_GET['search']));
      $search = "where c.customers_lastname like '%" . $keywords . "%' or c.customers_firstname like '%" . $keywords . "%' or c.customers_email_address like '%" . $keywords . "%'";
    } elseif (isset($_GET['search_phone']) && tep_not_null($_GET['search_phone'])) {
      $keywords = tep_db_input(tep_db_prepare_input($_GET['search_phone']));
      $search = "where c.customers_telephone like '%" . $keywords . "%'";
    }
//-MS- Clear the $cInfo object
    if( isset($cInfo) )
      unset($cInfo);

    //$customers_query_raw = "select c.customers_id, c.customers_lastname, c.customers_firstname, c.customers_email_address, a.entry_country_id, c.customers_default_address_id from " . TABLE_CUSTOMERS . " c left join " . TABLE_ADDRESS_BOOK . " a on c.customers_id = a.customers_id and c.customers_default_address_id = a.address_book_id " . $search . " order by c.customers_lastname, c.customers_firstname";
    $customers_query_raw = "select c.customers_id, c.customers_lastname, c.customers_firstname, c.customers_email_address, c.customers_default_address_id from " . TABLE_CUSTOMERS . " c " . $search . " order by c.customers_lastname, c.customers_firstname";
    $customers_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $customers_query_raw, $customers_query_numrows);
    $customers_query = tep_db_query($customers_query_raw);
    while ($customers = tep_db_fetch_array($customers_query)) {
      $info_query = tep_db_query("select customers_info_date_account_created as date_account_created, customers_info_date_account_last_modified as date_account_last_modified, customers_info_date_of_last_logon as date_last_logon, customers_info_number_of_logons as number_of_logons from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . $customers['customers_id'] . "'");
      $info = tep_db_fetch_array($info_query);

      $address_book_query = tep_db_query("select entry_country_id from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$customers['customers_default_address_id'] . "'");
      $address_book_array = tep_db_fetch_array($address_book_query);

      if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $customers['customers_id']))) && !isset($cInfo)) {
        $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$address_book_array['entry_country_id'] . "'");
        $country = tep_db_fetch_array($country_query);

        $reviews_query = tep_db_query("select count(*) as number_of_reviews from " . TABLE_REVIEWS . " where customers_id = '" . (int)$customers['customers_id'] . "'");
        $reviews = tep_db_fetch_array($reviews_query);

        $address_book_query = tep_db_query("select count(*) as number_of_entries from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers['customers_id'] . "'");
        $address_book = tep_db_fetch_array($address_book_query);

        $customer_info = array_merge($country, $info, $reviews, $address_book);

        $cInfo_array = array_merge($customers, $customer_info);
        $cInfo = new objectInfo($cInfo_array);
      }

      if (isset($cInfo) && is_object($cInfo) && ($customers['customers_id'] == $cInfo->customers_id)) {
        echo '          <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '          <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '\'">' . "\n";
      }
?>
                    <td class="dataTableContent"><?php echo $customers['customers_id']; ?></td>
                    <td class="dataTableContent"><?php echo $customers['customers_lastname']; ?></td>
                    <td class="dataTableContent"><?php echo $customers['customers_firstname']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_date_short($info['date_account_created']); ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_date_short($info['date_last_logon']); ?></td>
                    <td class="dataTableContent" align="right">
<?php 
      if (isset($cInfo) && is_object($cInfo) && ($customers['customers_id'] == $cInfo->customers_id)) { 
        echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); 
      } else { 
        echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID')) . 'cID=' . $customers['customers_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
      }
      echo '&nbsp;';
?>
                    </td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="7"><hr class="formAreaBorder" /></td>
                  </tr>

                  <tr>
                    <td colspan="7"><?php if (!$action) echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, 'page=' . $_GET['page'] . '&action=edit') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
                  </tr>

                  <tr>
                    <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText" valign="top"><?php echo $customers_split->display_count($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CUSTOMERS); ?></td>
                        <td class="smallText" align="right"><?php echo $customers_split->display_links($customers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'cID'))); ?></td>
                      </tr>
<?php
    if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
?>
                      <tr>
                        <td align="right" colspan="2"><?php echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS) . '">' . tep_image_button('button_reset.gif', IMAGE_RESET) . '</a>'; ?></td>
                      </tr>
<?php
    }
?>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'confirm':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_CUSTOMER . '</b>');

      $contents = array('form' => tep_draw_form('customers', FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_DELETE_INTRO . '<br><br><b>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</b>');
      if (isset($cInfo->number_of_reviews) && ($cInfo->number_of_reviews) > 0) $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('delete_reviews', 'on', true) . ' ' . sprintf(TEXT_DELETE_REVIEWS, $cInfo->number_of_reviews));
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
      if (isset($cInfo) && is_object($cInfo)) {
        $heading[] = array('text' => '<b>' . $cInfo->customers_firstname . ' ' . $cInfo->customers_lastname . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=confirm') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS, 'cID=' . $cInfo->customers_id) . '">' . tep_image_button('button_orders.gif', IMAGE_ORDERS) . '</a> <a href="' . tep_href_link(FILENAME_MAIL, 'selected_box=tools&customer=' . $cInfo->customers_email_address) . '">' . tep_image_button('button_email.gif', IMAGE_EMAIL) . '</a> <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'action=ab_edit&cID=' . $cInfo->customers_id) . '">' . tep_image_button('button_insert_address.gif', 'Insert an Address Book Entry for this customer') . '</a> <a href="' . tep_href_link(FILENAME_CUSTOMERS, tep_get_all_get_params(array('cID', 'action')) . 'cID=' . $cInfo->customers_id . '&action=ab_edit&abID=' . $cInfo->customers_default_address_id) . '">' . tep_image_button('button_edit_address.gif', 'Edit Address Book Entries for this customer') . '</a>');
        $contents[] = array('text' => '<br>' . TEXT_DATE_ACCOUNT_CREATED . ' ' . tep_date_short($cInfo->date_account_created));
        $contents[] = array('text' => '<br>' . TEXT_DATE_ACCOUNT_LAST_MODIFIED . ' ' . tep_date_short($cInfo->date_account_last_modified));
        $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_LAST_LOGON . ' '  . tep_date_short($cInfo->date_last_logon));
        $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_OF_LOGONS . ' ' . $cInfo->number_of_logons);
        $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY . ' ' . $cInfo->countries_name);
        $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_OF_REVIEWS . ' ' . $cInfo->number_of_reviews);
        $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_OF_ENTRIES . ' ' . $cInfo->number_of_entries);
      }
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
