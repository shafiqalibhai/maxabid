# osCommerce, Open Source E-Commerce Solutions
# http://www.oscommerce.com
#
# Database Backup For Maxabid
# Copyright (c) 2013 Maxabid
#
# Database: maxabidc1_123
# Database Server: 10.168.1.60
#
# Backup Date: 03/11/2013 18:40:02

drop table if exists abstract_types;
create table abstract_types (
  abstract_types_id int(11) not null auto_increment,
  abstract_types_name varchar(64) not null ,
  abstract_types_class varchar(64) not null ,
  abstract_types_table varchar(255) not null ,
  abstract_types_status tinyint(1) default '1' not null ,
  sort_order int(3) ,
  PRIMARY KEY (abstract_types_id)
);

insert into abstract_types (abstract_types_id, abstract_types_name, abstract_types_class, abstract_types_table, abstract_types_status, sort_order) values ('1', 'Product Groups', 'products_zones', 'products_zones_to_categories', '1', '1');
insert into abstract_types (abstract_types_id, abstract_types_name, abstract_types_class, abstract_types_table, abstract_types_status, sort_order) values ('3', 'Page Sequencer', 'generic_zones', 'gtext_to_display, gtext_to_products', '1', '3');
drop table if exists abstract_zones;
create table abstract_zones (
  abstract_zone_id int(11) not null auto_increment,
  abstract_types_id int(11) default '0' not null ,
  abstract_zone_name varchar(64) not null ,
  abstract_zone_desc text ,
  date_added datetime ,
  last_modified datetime ,
  PRIMARY KEY (abstract_zone_id, abstract_types_id)
);

insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('6', '1', 'Upcoming Prizes', 'Upcoming Prizes', '2010-03-10 18:39:23', '2011-11-11 16:42:03');
insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('7', '3', 'Menu Zones', 'Menu Zones', '2010-03-10 18:41:57', NULL);
insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('8', '3', 'Footer Entries', 'Text entries will appear with the footer', '2010-03-10 19:27:01', NULL);
insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('9', '3', 'Header Quicks', 'Image/Links to display below the main header', '2011-07-25 15:54:05', NULL);
insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('10', '3', 'Information Entries', 'Information Entries', '2011-07-26 14:24:50', NULL);
insert into abstract_zones (abstract_zone_id, abstract_types_id, abstract_zone_name, abstract_zone_desc, date_added, last_modified) values ('11', '3', 'Frequently Asked Questions', 'Frequently Asked Questions', '2011-07-26 14:37:27', NULL);
drop table if exists address_book;
create table address_book (
  address_book_id int(11) not null auto_increment,
  customers_id int(11) not null ,
  entry_gender char(1) not null ,
  entry_company varchar(32) ,
  entry_firstname varchar(32) not null ,
  entry_lastname varchar(32) not null ,
  entry_street_address varchar(64) not null ,
  entry_suburb varchar(32) ,
  entry_postcode varchar(10) not null ,
  entry_city varchar(32) not null ,
  entry_state varchar(32) ,
  entry_country_id int(11) default '0' not null ,
  entry_zone_id int(11) default '0' not null ,
  PRIMARY KEY (address_book_id),
  KEY idx_address_book_customers_id (customers_id)
);

insert into address_book (address_book_id, customers_id, entry_gender, entry_company, entry_firstname, entry_lastname, entry_street_address, entry_suburb, entry_postcode, entry_city, entry_state, entry_country_id, entry_zone_id) values ('9', '7', '', NULL, 'AA', 'YY', 'None Street', '', 'e6123', 'London', '', '222', '3618');
insert into address_book (address_book_id, customers_id, entry_gender, entry_company, entry_firstname, entry_lastname, entry_street_address, entry_suburb, entry_postcode, entry_city, entry_state, entry_country_id, entry_zone_id) values ('10', '8', '', NULL, 'harry', 'houdini', 'fsdfsdf', '', 'dfdsfdsfsd', 'dfsdfsdfsdf', '', '222', '3578');
insert into address_book (address_book_id, customers_id, entry_gender, entry_company, entry_firstname, entry_lastname, entry_street_address, entry_suburb, entry_postcode, entry_city, entry_state, entry_country_id, entry_zone_id) values ('13', '11', '', NULL, 'Mark', 'Tester', '5th street', '', 'SM1 4TP', 'London', '', '222', '3618');
drop table if exists address_format;
create table address_format (
  address_format_id int(11) not null auto_increment,
  address_format varchar(128) not null ,
  address_summary varchar(48) not null ,
  PRIMARY KEY (address_format_id)
);

insert into address_format (address_format_id, address_format, address_summary) values ('1', '$firstname $lastname$cr$streets$cr$city, $postcode$cr$statecomma$country', '$city / $country');
insert into address_format (address_format_id, address_format, address_summary) values ('2', '$firstname $lastname$cr$streets$cr$city, $state    $postcode$cr$country', '$city, $state / $country');
insert into address_format (address_format_id, address_format, address_summary) values ('3', '$firstname $lastname$cr$streets$cr$city$cr$postcode - $statecomma$country', '$state / $country');
insert into address_format (address_format_id, address_format, address_summary) values ('4', '$firstname $lastname$cr$streets$cr$city ($postcode)$cr$country', '$postcode / $country');
insert into address_format (address_format_id, address_format, address_summary) values ('5', '$firstname $lastname$cr$streets$cr$postcode $city$cr$country', '$city / $country');
drop table if exists admin;
create table admin (
  admin_id int(11) not null auto_increment,
  admin_groups_id int(11) ,
  admin_firstname varchar(32) not null ,
  admin_lastname varchar(32) ,
  admin_email_address varchar(96) not null ,
  admin_password varchar(40) not null ,
  admin_created datetime ,
  admin_modified datetime default '0000-00-00 00:00:00' not null ,
  admin_logdate datetime ,
  admin_lognum int(11) default '0' not null ,
  PRIMARY KEY (admin_id),
  UNIQUE admin_email_address (admin_email_address)
);

insert into admin (admin_id, admin_groups_id, admin_firstname, admin_lastname, admin_email_address, admin_password, admin_created, admin_modified, admin_logdate, admin_lognum) values ('1', '1', 'Test', 'Admin', 'support@maxabid.co.uk', '745080d4a269769bdb6be01aaf5c411a:65', '2008-04-04 12:07:44', '2013-10-05 10:34:19', '2013-11-03 16:38:36', '379');
drop table if exists admin_files;
create table admin_files (
  admin_files_id int(11) not null auto_increment,
  admin_files_name varchar(64) not null ,
  admin_files_is_boxes tinyint(5) default '0' not null ,
  admin_files_to_boxes int(11) default '0' not null ,
  admin_groups_id set('1','2') default '1' not null ,
  PRIMARY KEY (admin_files_id)
);

insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('1', 'administrator.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('2', 'configuration.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('3', 'catalog.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('4', 'modules.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('5', 'customers.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('6', 'taxes.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('7', 'localization.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('8', 'reports.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('9', 'tools.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('10', 'admin_members.php', '0', '1', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('11', 'admin_files.php', '0', '1', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('12', 'configuration.php', '0', '2', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('13', 'categories.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('14', 'products_attributes.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('15', 'manufacturers.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('16', 'reviews.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('17', 'specials.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('18', 'products_expected.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('19', 'modules.php', '0', '4', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('20', 'customers.php', '0', '5', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('21', 'orders.php', '0', '5', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('22', 'countries.php', '0', '6', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('23', 'zones.php', '0', '6', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('24', 'geo_zones.php', '0', '6', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('25', 'tax_classes.php', '0', '6', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('26', 'tax_rates.php', '0', '6', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('27', 'currencies.php', '0', '7', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('28', 'languages.php', '0', '7', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('29', 'orders_status.php', '0', '7', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('30', 'stats_products_viewed.php', '0', '8', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('31', 'stats_products_purchased.php', '0', '8', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('32', 'stats_customers.php', '0', '8', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('34', 'banner_manager.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('35', 'cache.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('36', 'define_language.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('37', 'file_manager.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('38', 'mail.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('39', 'newsletters.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('40', 'server_info.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('41', 'whos_online.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('42', 'banner_statistics.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('43', 'seo_g.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('44', 'returns.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('45', 'meta_g.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('46', 'cache.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('47', 'abstract_zones.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('48', 'abstract_types.php', '0', '47', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('49', 'abstract_zones.php', '0', '47', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('50', 'abstract_zones_config.php', '0', '47', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('51', 'generic_text.php', '0', '47', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('52', 'cache_config.php', '0', '46', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('53', 'cache_html.php', '0', '46', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('54', 'cache_reports.php', '0', '46', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('56', 'products_extra_images.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('57', 'products_filters.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('58', 'products_multi.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('60', 'orders_comments.php', '0', '5', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('61', 'edit_orders.php', '0', '5', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('73', 'meta_exclude.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('74', 'meta_feeds.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('75', 'meta_lexico.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('76', 'meta_types.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('77', 'meta_zones.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('78', 'meta_zones_config.php', '0', '45', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('79', 'other_config.php', '0', '2', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('81', 'seo_exclude.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('82', 'seo_redirects.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('83', 'seo_reports.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('84', 'seo_types.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('85', 'seo_zones.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('86', 'seo_zones_config.php', '0', '43', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('87', 'total_configuration.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('89', 'admin_email_templates.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('90', 'helpdesk.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('91', 'helpdesk.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('92', 'helpdesk_departments.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('93', 'helpdesk_log.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('94', 'helpdesk_mass_delete.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('95', 'helpdesk_pop3.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('96', 'helpdesk_new_email.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('97', 'helpdesk_priorities.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('98', 'helpdesk_status.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('99', 'helpdesk_templates.php', '0', '90', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('100', 'group_fields.php', '0', '3', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('104', 'auctions.php', '1', '0', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('105', 'auctions.php', '0', '104', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('106', 'auctions_group.php', '0', '104', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('107', 'auctions_tier.php', '0', '104', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('108', 'ajax_control.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('109', 'coupons.php', '0', '5', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('110', 'auctions_config.php', '0', '104', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('111', 'backup.php', '0', '9', '1');
insert into admin_files (admin_files_id, admin_files_name, admin_files_is_boxes, admin_files_to_boxes, admin_groups_id) values ('112', 'backup.php', '0', '9', '1');
drop table if exists admin_groups;
create table admin_groups (
  admin_groups_id int(11) not null auto_increment,
  admin_groups_name varchar(64) ,
  PRIMARY KEY (admin_groups_id),
  UNIQUE admin_groups_name (admin_groups_name)
);

insert into admin_groups (admin_groups_id, admin_groups_name) values ('2', 'Customer Relations');
insert into admin_groups (admin_groups_id, admin_groups_name) values ('1', 'Top Administrator');
drop table if exists auctions;
create table auctions (
  auctions_id int(11) not null auto_increment,
  auctions_group_id int(11) not null ,
  products_id int(11) not null ,
  auctions_name varchar(64) not null ,
  auctions_image varchar(255) not null ,
  auctions_description text ,
  auctions_type int(3) default '0' not null ,
  auctions_overbid tinyint(1) default '0' not null ,
  status_id int(3) default '0' ,
  bid_step int(3) default '1' ,
  max_bids int(3) default '0' ,
  countdown_bids varchar(64) not null ,
  countdown_timer varchar(64) not null ,
  auto_timer int(11) not null ,
  sort_id int(3) ,
  date_start datetime ,
  date_expire datetime ,
  start_pause time ,
  end_pause time ,
  cap_price decimal(15,2) default '0.00' not null ,
  start_price decimal(15,4) default '0.0000' not null ,
  shipping_cost decimal(15,4) default '0.0000' not null ,
  bids_added tinyint(1) default '0' not null ,
  bids_left tinyint(1) default '0' not null ,
  live_off datetime not null ,
  PRIMARY KEY (auctions_id),
  KEY idx_auctions_group_id (auctions_group_id),
  KEY idx_auctions_name (auctions_name),
  KEY idx_auction_status (status_id),
  KEY idx_date_expire (date_expire)
);

insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('1', '5', '95', '10 Bids-1', 'argosgiftcard2.jpg', '', '1', '1', '2', '1', '0', '1', '00:00:59', '28', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '10.00', '0.0000', '4.9900', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('2', '1', '96', '100 Bids-2', 'giftcard-3.jpg', '', '1', '1', '2', '1', '0', '2', '00:00:30', '28', '2', '2012-05-01 00:00:00', '2012-05-09 00:00:00', '08:00:00', '16:25:00', '100.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('3', '1', '94', 'Bundle-1-3', 'game2.jpg', '<p>A unique offer for the bundle1  product</p>', '0', '1', '2', '3', '8', '', '', '28', '3', '2011-09-18 15:45:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '9.7700', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('4', '5', '99', '500 Bids-4', 'sample-champagne.jpg', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>500 
Bids&nbsp;</strong></span></p>', '0', '1', '2', '1', '0', '5', '00:00:59', '28', '0', '2011-09-17 00:00:00', '0000-00-00 00:00:00', '21:15:00', '21:20:00', '0.00', '0.0000', '4.8800', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('22', '1', '94', 'Bundle-1-22', 'game1.jpg-22', '<p>bundle1</p>', '1', '1', '2', '1', '20', '', '', '28', '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '3.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('23', '3', '95', '10 Bids-23', 'bids10-image.jpg-23', '<p><span style=\"background-color: # 6666cc; color: #cc9999; font-size: xx- l arge;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>
<p>&nbsp;</p>
<p><span style=\"color: #000000;\"><strong>The winner of this auction will receive 10 Bids added to their Bids Balance :)</strong></span></p>
<p><span><strong>The winner of this auction will receive 10 Bids added to their Bids Balance :)</strong></span></p>
<div><span>
<p style=\"font-weight: bold;\"><span><strong>The winner of this auction will receive 10 Bids added to their Bids Balance :)</strong></span></p>
</span></div>
<p>��</p>
<p><span style=\"background-color: #ff9900;\"><strong>The winner of this auction will receive 10 Bids added to their Bids Balance :)</strong></span></p>
<div><span><strong>
<p><span style=\"background-color: #993366; color: #ffffff;\"><strong>The winner of this auction will receive 10 Bids added to their Bids Balance :)</strong></span></p>
<div><span><strong><br /></strong></span></div>
<br /></strong></span></div>', '0', '1', '2', '4', '0', '88', '00:04:50', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '8.7700', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('24', '3', '97', '1000 Bids-24', 'bids1000-image.jpg-24', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx- 
large;\">&nbsp;<strong>1000 B
ids&nbsp;</strong></span></p>�', '0', '1', '2', '5', '0', '5', '00:00:59', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '23:50:00', '23:55:00', '0.00', '0.0000', '5.1100', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('25', '1', '99', '500 Bids-25', 'bids500-image.jpg-25', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '50', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '500.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('26', '1', '93', 'Bundle2-26', 'giftcard-1.jpg', '<p>Bundle2</p>', '1', '1', '2', '1', '0', '5', '00:00:30', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '2.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('27', '1', '95', '10 Bids-27', 'bids10-image.jpg-27', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '1', '2', '1', '5', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('28', '2', '98', '50 Bids-28', 'bids50-image.jpg-28', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '1', '1', '1', '1', '30', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '50.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('29', '2', '99', '500 Bids-29', 'bids500-image.jpg-29', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '1', '1', '1', '1', '0', '20', '00:00:15', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '500.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('30', '2', '93', 'Bundle2-30', 'giftcard-2.jpg', '<p>Bundle2</p>', '1', '1', '2', '1', '0', '7', '00:00:15', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '2.00', '0.0000', '1.2300', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('31', '2', '97', '1000 Bids-31', 'bids1000-image.jpg-31', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>1000 
Bids&nbsp;</strong></span></p>', '1', '1', '2', '5', '99', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '1000.00', '0.0000', '0.0000', '0', '1', '2013-06-03 17:23:45');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('32', '3', '95', '10 Bids-32', 'bids10-image.jpg-32', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '2', '1', '2', '1', '0', '10', '00:00:10', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '13.99', '10.0000', '0.0500', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('33', '3', '93', 'Bundle2-33', 'game2.jpg', '<p>Bundle2</p>', '2', '1', '2', '1', '0', '5', '00:00:15', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '30.00', '20.0000', '5.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('34', '4', '95', '10 Bids-34', 'bids10-image.jpg-34', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>10 
Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '20', '', '', '28', '0', '0000-00-00 00:00:00', '2012-05-23 13:00:00', '00:00:00', '00:00:00', '10.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('35', '4', '96', '100 Bids-35', 'bids100-image.jpg-35', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>100 
Bids&nbsp;</strong></span></p>', '0', '1', '1', '1', '20', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '2.9900', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('36', '4', '97', '1000 Bids-36', 'bids1000-image.jpg-36', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx- large;\">&nbsp;<strong>1000  Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '0', '1,2,3', '20,00:00:59,7', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10:00:00', '14:00:00', '1000.00', '0.0100', '9.1100', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('37', '4', '98', '50 Bids-37', 'bids50-image.jpg-37', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '0', '1', '2', '2', '20', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('38', '6', '96', '100 Bids-38', 'bids100-image.jpg-38', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '2', '1', '2', '1', '10', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('39', '6', '97', '1000 Bids-39', 'bids1000-image.jpg-39', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '2', '1', '1', '1', '30', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('40', '6', '98', '50 Bids-40', 'bids50-image.jpg-40', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '1', '1', '1', '1', '50', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '50.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('41', '6', '99', '500 Bids-41', 'bids500-image.jpg-41', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '1', '1', '1', '1', '35', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '500.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('42', '5', '96', '100 Bids-42', 'bids100-image.jpg-42', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>100 
Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '30', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '100.00', '0.0000', '0.0000', '0', '1', '2013-05-23 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('43', '5', '97', '1000 Bids-43', 'bids1000-image.jpg-43', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '2', '1', '2', '1', '30', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('44', '5', '95', '10 Bids (#44)', 'bids10-image.jpg-44', '<p><span style=\"background-color: # 
6666cc; color: #cc9999; font-size: xx- l 
arge;\">&nbsp;<strong>10 Bi 
ds&nbsp;</strong></span></p>
<', '0', '1', '1', '1', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('45', '1', '95', '10 Bids (#45)', 'bids10-image.jpg-45', '<p><span style=\"background-color: #    6666cc; color: #cc9999; font-size: xx-  l    arge;\">&nbsp;<strong>10 B i d  s&nbsp;</strong></span></p>
<p>&amp;</p>', '0', '0', '2', '10', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '17:45:00', '17:50:00', '0.00', '0.0000', '0.0000', '0', '0', '2013-06-22 20:42:26');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('46', '4', '99', '500 Bids (#46)', 'bids500-image.jpg-46', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>500 
Bids&nbsp;</strong></span></p>', '0', '0', '1', '10000', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('47', '4', '97', '1000 Bids (#47)', 'bids1000-image.jpg-47', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '0', '0', '2', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-07-07 20:53:43');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('48', '1', '93', 'Bundle2 (#48)', '', '<p>Bundle2</p>', '0', '0', '2', '7', '0', '5', '00:00:59', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '12.9900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('49', '1', '98', '50 Bids (#49)', 'bids50-image.jpg-49', '<p><span style=\"background-color: #   
6666cc; color: #cc9999; font-size: xx- l   
arge;\">&nbsp;<strong>50 Bi d 
s&nbsp;</strong></span></p><p
>', '0', '0', '2', '3', '0', '2', '00:00:17', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '8.9900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('50', '3', '99', '500 Bids (#50)', 'bids500-image.jpg-50', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>500 
Bids&nbsp;</strong></span></p>', '0', '0', '1', '22', '0', '99', '00:50:00', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '58.1000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('51', '3', '98', '50 Bids (#51)', 'bids50-image.jpg-51', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx- large;\">&nbsp;<strong>50  Bids&nbsp;</strong></span></p>', '0', '0', '2', '9', '0', '6', '23:59:59', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '154.2800', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('53', '1', '96', '100 Bids (#53)', 'bids100-image.jpg-53', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '0', '1', '2', '8', '3', '3', '00:00:10', '28', '0', '2013-04-01 00:00:00', '2013-04-25 11:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '9.9800', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('54', '1', '94', 'Bundle-1 (#54)', 'game1.jpg-54', '<p>bundle1</p>', '0', '0', '2', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-05-20 21:04:38');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('55', '1', '95', '10 Bids (#55)', 'bids10-image.jpg-55', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '0', '2', '1', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '0', '2013-04-24 21:04:38');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('56', '1', '96', '100 Bids (#56)', 'bids100-image.jpg-56', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '0', '0', '2', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-05-23 16:40:08');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('57', '1', '98', '50 Bids (#57)', 'bids50-image.jpg-57', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '0', '0', '1', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-06-22 20:42:25');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('58', '1', '94', 'Bundle-1 (#58)', 'game1.jpg-58', '<p>bundle1</p>', '0', '0', '1', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-06-22 20:42:25');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('59', '9', '97', '1000 Bids (#59)', 'bids1000-image.jpg-59', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '0', '1', '2', '16', '0', '3', '00:00:10', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '567.8900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('60', '9', '98', '50 Bids (#60)', 'bids50-image.jpg-60', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '0', '1', '2', '41', '0', '6', '00:00:17', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '9.9100', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('61', '9', '96', '100 Bids (#61)', 'bids100-image.jpg-61', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '0', '1', '2', '1', '3', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('62', '9', '95', '10 Bids (#62)', 'bids10-image.jpg-62', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '1', '2', '12', '13', '2,5', '00:00:10,00:00:05', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('63', '9', '99', '50 Bids (#64)', 'bids50-image.jpg-63', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '0', '1', '2', '5', '0', '3', '00:00:10', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '1.2300', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('64', '9', '95', '10 Bids (#64)', 'bids10-image.jpg-64', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '1', '2', '4', '0', '5,2', '00:00:19,00:00:07', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('65', '10', '95', '10 Bids (#65)', 'bids10-image.jpg-65', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '1', '2', '3', '0', '3,7', '00:00:55,00:00:08', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('66', '9', '96', '100 Bids (#66)', 'bids100-image.jpg-66', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '0', '1', '2', '9', '0', '5,6', '02:00:03,00:00:15', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('67', '9', '99', '500 Bids (#67)', 'bids500-image.jpg-67', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '0', '1', '2', '2', '0', '1,5,9,14', '00:00:30,07:00:00,00:25:02,00:00:50', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '1.0900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('68', '9', '99', '500 Bids (#68)', 'bids500-image.jpg-68', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '0', '1', '2', '7', '0', '2,4,6,8', '06:00:00,00:00:53,00:04:00,00:00:05', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '9.7700', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('69', '7', '95', '10 Bids (#69)', 'bids10-image.jpg-69', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '1', '1', '2', '4', '25', '', '', '28', '0', '0000-00-00 00:00:00', '2013-04-24 09:00:00', '00:00:00', '00:00:00', '10.00', '0.0000', '2.9900', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('70', '7', '96', '100 Bids (#70)', 'bids100-image.jpg-70', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>
<p>&nbsp;</p>
<p><span style=\"font-family: \'comic sans ms\', sans-serif; font-size: small;\"><em><span style=\"background-color: #ffffff; color: #00ff00;\"><strong>biggin up the bids for all the Maxabidders :)</strong></span></em></span></p>', '0', '1', '2', '3', '0', '2', '00:00:59', '28', '0', '2013-04-07 05:00:00', '2013-04-18 08:40:00', '00:00:00', '00:00:00', '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('71', '7', '99', '500 Bids (#71)', 'bids500-image.jpg-71', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '0', '2', '5', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '499.99', '0.0100', '0.0800', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('72', '7', '97', '1000 Bids (#72)', 'bids1000-image.jpg-72', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '1', '1', '1', '1', '100', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '1000.00', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('73', '7', '95', '10 Bids (#73)', 'bids10-image.jpg-73', '<p><span style=\"background-color: 
\#6666cc; color: #cc9999; font-size: xx-
large;\">&nbsp;<strong>10 
Bids&nbsp;</strong></span></p>', '1', '1', '2', '4', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '10.00', '0.0000', '0.0000', '0', '1', '2013-05-22 13:44:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('74', '3', '99', '500 Bids (#74)', 'bids500-image.jpg-74', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '0', '0', '2', '1', '0', '', '', '28', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '2013-07-07 20:43:41');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('75', '8', '97', '1000 Bids (#75)', 'bids1000-image.jpg-75', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>
<p>&nbsp;</p>
<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\"><strong><span style=\"background-color: #ffffff; color: #00ff00;\">tooooooooooo</span><br /></strong></span></p>', '1', '1', '1', '1', '0', '', '', '3', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '14.99', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('76', '6', '98', '50 Bids (#76)', 'bids50-image.jpg-76', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>
<p>&nbsp;</p>
<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\"><strong><span style=\"color: #ff00ff; background-color: #ffffff;\">innit</span><br /></strong></span></p>', '1', '1', '2', '1', '99', '', '', '5', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '7.44', '0.0000', '0.0000', '0', '1', '2013-04-28 22:05:30');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('77', '8', '99', '500 Bids (#77)', 'bids500-image.jpg-77', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '0', '0', '1', '1', '0', '', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '19.99', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('78', '7', '95', '10 Bids (#78)', 'bids10-image.jpg-78', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '0', '0', '1', '1', '0', '', '', '0', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('79', '9', '99', '500 Bids (#79)', 'bids500-image.jpg-79', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '0', '0', '1', '1', '0', '', '', '0', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('80', '10', '94', 'Bundle-1 (#80)', 'game1.jpg-80', '<p>bundle1</p>', '1', '1', '2', '1', '5', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '9.99', '0.0100', '7.7700', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('81', '10', '96', '100 Bids (#81)', 'bids100-image.jpg-81', '<p><span style=\"background-color: 
\#    6666cc; color: #cc9999; font-size: 
xx-  l    arge;\">&nbsp;<strong>100  
B    ids&nbsp;</strong></span></p>', '0', '1', '2', '1', '0', '5', '00:10:00', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.08', '0.0100', '2.9900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('82', '10', '100', '250 Bids (#82)', '', '', '0', '1', '1', '1', '0', '', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0000', '19.2500', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('83', '10', '101', '750 Bids (#83)', '', '', '0', '1', '1', '1', '0', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '0.00', '0.0100', '4.9900', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('84', '7', '93', 'Bundle2 (#84)', '', '<p>Bundle2</p>', '0', '0', '1', '1', '0', '', '', '0', NULL, NULL, NULL, NULL, NULL, '0.00', '0.0000', '0.0000', '0', '0', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('86', '1', '95', '10 Bids (#86)', 'bids10-image.jpg-86', '<p><span style=\"background-color: # 6666cc; color: #cc9999; font-size: xx- l arge;\">&nbsp;<strong>10  B ids&nbsp;</strong></span></p>
<p>﷯</p>', '1', '1', '2', '6', '4', '', '', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '9.99', '0.0000', '0.0000', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('89', '1', '101', '750 Bids (#89)', '', '', '1', '1', '2', '3', '4', '', '', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '9.99', '0.0100', '2.9900', '0', '1', '2013-06-24 13:54:09');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('90', '1', '95', '10 Bids (#90)', 'bids10-image.jpg-90', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '1', '1', '2', '1', '0', '3', '00:00:05', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '9.99', '0.0100', '2.8500', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('91', '1', '100', '250 Bids (#91)', '', '', '1', '1', '2', '19', '0', '2,3,6', '00:00:45,00:02:00,00:00:02', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '9.99', '0.0100', '77.2100', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('92', '1', '101', '750 Bids (#92)', '', '', '1', '1', '2', '1', '0', '1', '45 secs', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '12.77', '0.0100', '6.1100', '0', '1', '0000-00-00 00:00:00');
insert into auctions (auctions_id, auctions_group_id, products_id, auctions_name, auctions_image, auctions_description, auctions_type, auctions_overbid, status_id, bid_step, max_bids, countdown_bids, countdown_timer, auto_timer, sort_id, date_start, date_expire, start_pause, end_pause, cap_price, start_price, shipping_cost, bids_added, bids_left, live_off) values ('93', '1', '101', '750 Bids (#93)', '', '', '1', '1', '2', '1', '0', '1', '00:00:45', '28', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '00:00:00', '00:00:00', '99.99', '0.0100', '9.6600', '0', '1', '2013-07-07 21:06:30');
drop table if exists auctions_bid;
create table auctions_bid (
  auctions_id int(11) not null ,
  customers_id int(11) not null ,
  customers_nickname varchar(64) not null ,
  signature varchar(32) not null ,
  bid_count int(11) default '1' not null ,
  bid_price decimal(15,2) default '0.00' not null ,
  last_modified datetime ,
  PRIMARY KEY (auctions_id),
  KEY idx_customers_id (customers_id)
);

insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('28', '7', 'Totally One', '56d2d2af0660aa05bafb034aefe996f2', '8', '0.77', '2012-01-18 00:41:28');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('29', '11', 'marktester1', 'c792036fd523c28637ab4b32a665c7ab', '3', '14.00', '2013-05-01 20:19:28');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('35', '7', 'Totally One', '51417061a314dadb678edc646de30789', '4', '0.05', '2013-04-04 09:43:56');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('39', '5', 'The Tester', '98ce680d0aff430e398d43debde3bd4e', '3', '0.03', '2012-05-08 09:14:03');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('40', '7', 'Totally One', '392a1eb395f54bf3c6fed0df72e6cd2e', '2', '8.77', '2012-05-02 10:11:01');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('44', '11', 'marktester1', '0fe267b4887ae937b9227b73a67cf87c', '6', '0.07', '2013-05-05 21:23:16');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('46', '7', 'Totally One', '2f1ef879ebe290f242af5747457d6659', '1', '0.01', '2012-04-13 10:19:55');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('50', '7', 'Totally One', '394f92bfabbcd73570178c47963896dd', '1', '0.02', '2012-05-09 19:25:29');
insert into auctions_bid (auctions_id, customers_id, customers_nickname, signature, bid_count, bid_price, last_modified) values ('77', '7', 'Totally One', 'cd810626550254e839c8b78baaa4ebb5', '1', '0.01', '2013-05-13 09:56:25');
drop table if exists auctions_customer;
create table auctions_customer (
  auctions_id int(11) not null ,
  customers_id int(11) not null ,
  bid_price decimal(15,2) default '0.00' not null ,
  date_added datetime not null ,
  KEY idx_auctions_id (auctions_id),
  KEY idx_customers_id (customers_id),
  KEY idx_date_added (date_added)
);

insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('39', '7', '0.01', '2012-01-14 23:07:43');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('39', '7', '0.02', '2012-01-14 23:07:46');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('40', '7', '1.33', '2012-01-17 23:45:33');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.01', '2012-01-18 00:10:36');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.05', '2012-01-18 00:18:54');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.09', '2012-01-18 00:20:13');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.02', '2012-01-18 00:20:32');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.88', '2012-01-18 00:22:38');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.34', '2012-01-18 00:27:08');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '1.00', '2012-01-18 00:39:50');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('28', '7', '0.77', '2012-01-18 00:41:28');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('29', '7', '0.01', '2012-01-18 00:41:49');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('46', '7', '0.01', '2012-04-13 10:19:55');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('29', '7', '1.01', '2012-04-13 10:20:50');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('40', '7', '8.77', '2012-05-02 10:11:01');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('39', '5', '0.03', '2012-05-08 09:14:03');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '7', '0.02', '2012-05-08 10:56:21');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('50', '7', '0.02', '2012-05-09 19:25:29');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('35', '7', '0.02', '2013-04-04 09:43:46');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('35', '7', '0.03', '2013-04-04 09:43:50');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('35', '7', '0.04', '2013-04-04 09:43:53');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('35', '7', '0.05', '2013-04-04 09:43:56');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('29', '11', '14.00', '2013-05-01 20:19:28');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '7', '0.03', '2013-05-02 22:16:19');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '11', '0.04', '2013-05-05 21:19:53');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '11', '0.05', '2013-05-05 21:19:56');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '11', '0.06', '2013-05-05 21:19:57');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('44', '11', '0.07', '2013-05-05 21:23:16');
insert into auctions_customer (auctions_id, customers_id, bid_price, date_added) values ('77', '7', '0.01', '2013-05-13 09:56:25');
drop table if exists auctions_group;
create table auctions_group (
  auctions_group_id int(11) not null auto_increment,
  auctions_group_name varchar(64) not null ,
  auctions_group_desc text ,
  sort_order int(3) ,
  PRIMARY KEY (auctions_group_id)
);

insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('1', 'Featured', 'Featured Auctions Description
Optional', '0');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('2', 'Cash', 'Cash Prizes Auctions', '4');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('3', 'Rapid Timer', 'Rapid Timer Auctions', '1');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('4', 'Bundles', 'Auctions Bundles', '6');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('5', 'Multi-Winner', '', '3');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('6', 'Charity', '', '9');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('7', 'Bids Pack', 'Bids Pack Auctions', '2');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('8', 'Vouchers', 'Gift Certificate and Vouchers 
Auctions', '5');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('9', 'For Him', '\"For Him\" Auctions', '8');
insert into auctions_group (auctions_group_id, auctions_group_name, auctions_group_desc, sort_order) values ('10', 'For Her', '\"For Her\" Auctions', '7');
drop table if exists auctions_history;
create table auctions_history (
  auto_id int(11) not null auto_increment,
  auctions_id int(11) default '0' not null ,
  products_id int(11) default '0' not null ,
  customers_id int(11) default '0' not null ,
  auctions_name varchar(64) not null ,
  auctions_image varchar(255) not null ,
  products_name varchar(64) not null ,
  customers_nickname varchar(64) not null ,
  prize_stored tinyint(1) default '0' not null ,
  auctions_price decimal(15,4) default '0.0000' not null ,
  shipping_cost decimal(15,4) default '0.0000' not null ,
  bid_count int(11) default '0' not null ,
  winner_bids int(11) default '0' not null ,
  customer_count int(11) default '0' not null ,
  started datetime ,
  completed datetime ,
  last_notified datetime not null ,
  PRIMARY KEY (auto_id),
  KEY idx_auctions_id (auctions_id),
  KEY idx_products_id (products_id),
  KEY idx_customers_id (customers_id)
);

insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('44', '4', '99', '7', '500 Bids-4', 'sample-champagne.jpg', '500 Bids', 'Totally One', '0', '0.0900', '4.8800', '9', '4', '3', '2011-09-17 00:00:00', '2011-11-04 02:58:10', '0000-00-00 00:00:00');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('45', '4', '98', '8', '500 Bids-4', 'sample-port.jpg', '50 Bids', 'Harry Houdini', '0', '0.0800', '0.0000', '9', '4', '3', '2011-09-17 00:00:00', '2011-11-04 02:58:10', '0000-00-00 00:00:00');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('46', '4', '95', '7', '500 Bids-4', 'sample-ocean-8.jpg', '10 Bids', 'Totally One', '0', '0.0700', '0.0000', '9', '4', '3', '2011-09-17 00:00:00', '2011-11-04 02:58:10', '0000-00-00 00:00:00');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('49', '27', '95', '7', '10 Bids-27', 'bids10-image.jpg-27', '10 Bids', 'Totally One', '0', '0.0500', '0.0000', '5', '2', '2', '0000-00-00 00:00:00', '2012-01-14 21:32:31', '2012-01-14 21:32:31');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('50', '43', '97', '7', '1000 Bids-43', 'bids1000-image.jpg-43', '1000 Bids', 'Totally One', '0', '0.3000', '0.0000', '30', '30', '1', '0000-00-00 00:00:00', '2012-01-18 21:57:08', '2012-01-18 21:57:08');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('51', '43', '96', '7', '1000 Bids-43', 'bids100-image-t43-1.jpg', '100 Bids', 'Totally One', '0', '0.2900', '0.0000', '30', '30', '1', '0000-00-00 00:00:00', '2012-01-18 21:57:08', '2012-01-18 21:57:08');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('52', '37', '98', '7', '50 Bids-37', 'bids50-image.jpg-37', '50 Bids', 'Totally One', '0', '0.2000', '0.0000', '20', '20', '1', '0000-00-00 00:00:00', '2012-01-19 23:04:15', '2012-01-19 23:04:15');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('55', '33', '93', '7', 'Bundle2-33', 'game2.jpg', 'Bundle2', 'Totally One', '0', '25.0900', '5.0000', '12', '12', '1', '0000-00-00 00:00:00', '2012-04-13 10:27:35', '2012-04-13 10:27:35');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('56', '3', '94', '5', 'Bundle-1-3', 'game2.jpg', 'Bundle-1', 'The Tester', '0', '0.0900', '9.7700', '8', '8', '1', '2011-09-18 15:45:00', '2012-04-14 15:32:45', '2012-04-14 15:32:45');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('59', '26', '93', '7', 'Bundle2-26', 'giftcard-1.jpg', 'Bundle2', 'Totally One', '0', '1.4400', '0.0000', '5', '5', '1', '0000-00-00 00:00:00', '2012-04-14 19:30:19', '2012-04-14 19:30:19');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('60', '24', '97', '7', '1000 Bids-24', 'bids1000-image.jpg-24', '1000 Bids', 'Totally One', '0', '0.0500', '5.1100', '5', '5', '1', '0000-00-00 00:00:00', '2012-04-17 15:57:33', '2012-04-17 15:57:33');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('61', '1', '95', '7', '10 Bids-1', 'argosgiftcard2.jpg', '10 Bids', 'Totally One', '0', '1.9900', '4.9900', '3', '3', '1', '0000-00-00 00:00:00', '2012-04-19 20:23:43', '2012-04-19 20:23:43');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('62', '1', '94', '7', '10 Bids-1', 'prepaid-mastercard-25.jpg', 'Bundle-1', 'Totally One', '0', '5.1000', '0.0000', '3', '3', '1', '0000-00-00 00:00:00', '2012-04-19 20:23:43', '2012-04-19 20:23:43');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('63', '1', '97', '7', '10 Bids-1', 'prepaid-mastercard-150.jpg', '1000 Bids', 'Totally One', '0', '9.0700', '55.5500', '3', '3', '1', '0000-00-00 00:00:00', '2012-04-19 20:23:43', '2012-04-19 20:23:43');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('64', '2', '96', '5', '100 Bids-2', 'giftcard-3.jpg', '100 Bids', 'The Tester', '0', '0.0100', '0.0000', '2', '1', '2', '2012-05-01 00:00:00', '2012-05-02 16:38:59', '2012-05-02 16:38:59');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('65', '2', '96', '7', '100 Bids-2', 'mastercard-1.png', '100 Bids', 'Totally One', '0', '4.2200', '0.0000', '2', '1', '2', '2012-05-01 00:00:00', '2012-05-02 16:38:59', '2012-05-02 16:38:59');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('66', '25', '99', '9', '500 Bids-25', 'bids500-image.jpg-25', '500 Bids', 'The Gambler', '0', '0.0400', '0.0000', '50', '17', '4', '0000-00-00 00:00:00', '2012-05-04 17:56:28', '2012-05-04 17:56:28');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('67', '22', '94', '7', 'Bundle-1-22', 'game1.jpg-22', 'Bundle-1', 'Totally One', '0', '0.0100', '0.0000', '20', '13', '2', '0000-00-00 00:00:00', '2012-09-25 12:14:47', '2012-09-25 12:14:47');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('68', '23', '95', '10', '10 Bids-23', 'bids10-image.jpg-23', '10 Bids', 'khalidqasim', '0', '1.7800', '8.7700', '91', '27', '4', '0000-00-00 00:00:00', '2012-09-26 18:29:45', '2012-09-26 18:29:45');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('69', '51', '98', '10', '50 Bids (#51)', 'bids50-image.jpg-51', '50 Bids', 'khalidqasim', '0', '0.0700', '154.2800', '7', '4', '2', '0000-00-00 00:00:00', '2012-09-27 23:54:30', '2012-09-27 23:54:30');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('70', '59', '97', '7', '1000 Bids (#59)', 'bids1000-image.jpg-59', '1000 Bids', 'Totally One', '0', '0.0500', '567.8900', '5', '5', '1', '0000-00-00 00:00:00', '2013-04-03 18:05:57', '2013-04-03 18:05:57');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('71', '60', '98', '7', '50 Bids (#60)', 'bids50-image.jpg-60', '50 Bids', 'Totally One', '0', '0.0700', '9.9100', '7', '7', '1', '0000-00-00 00:00:00', '2013-04-03 18:09:39', '2013-04-03 18:09:39');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('72', '61', '96', '7', '100 Bids (#61)', 'bids100-image.jpg-61', '100 Bids', 'Totally One', '0', '0.0400', '0.0000', '3', '3', '1', '0000-00-00 00:00:00', '2013-04-03 18:12:16', '2013-04-03 18:12:16');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('73', '62', '95', '7', '10 Bids (#62)', 'bids10-image.jpg-62', '10 Bids', 'Totally One', '0', '0.1400', '0.0000', '13', '13', '1', '0000-00-00 00:00:00', '2013-04-03 18:16:15', '2013-04-03 18:16:15');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('74', '65', '95', '7', '10 Bids (#65)', 'bids10-image.jpg-65', '10 Bids', 'Totally One', '0', '0.0800', '0.0000', '7', '7', '1', '0000-00-00 00:00:00', '2013-04-03 18:27:29', '2013-04-03 18:27:29');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('75', '64', '95', '7', '10 Bids (#64)', 'bids10-image.jpg-64', '10 Bids', 'Totally One', '0', '0.1200', '0.0000', '11', '11', '1', '0000-00-00 00:00:00', '2013-04-03 18:29:44', '2013-04-03 18:29:44');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('76', '66', '96', '7', '100 Bids (#66)', 'bids100-image.jpg-66', '100 Bids', 'Totally One', '0', '0.0700', '0.0000', '6', '6', '1', '0000-00-00 00:00:00', '2013-04-03 18:32:20', '2013-04-03 18:32:20');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('77', '67', '99', '7', '500 Bids (#67)', 'bids500-image.jpg-67', '500 Bids', 'Totally One', '0', '0.1700', '1.0900', '16', '16', '1', '0000-00-00 00:00:00', '2013-04-03 18:38:26', '2013-04-03 18:38:26');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('78', '68', '99', '7', '500 Bids (#68)', 'bids500-image.jpg-68', '500 Bids', 'Totally One', '0', '0.0500', '9.7700', '5', '5', '1', '0000-00-00 00:00:00', '2013-04-04 18:25:00', '2013-04-04 18:25:00');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('79', '49', '98', '11', '50 Bids (#49)', 'bids50-image.jpg-49', '50 Bids', 'marktester1', '0', '0.0200', '8.9900', '2', '1', '2', '0000-00-00 00:00:00', '2013-04-22 21:04:38', '2013-04-22 21:04:38');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('80', '38', '96', '7', '100 Bids-38', 'bids100-image.jpg-38', '100 Bids', 'Totally One', '0', '0.1000', '0.0000', '10', '9', '2', '0000-00-00 00:00:00', '2013-04-23 22:05:30', '2013-04-23 22:05:30');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('81', '69', '95', '7', '10 Bids (#69)', 'bids10-image.jpg-69', '10 Bids', 'Totally One', '0', '10.0000', '2.9900', '1', '1', '1', '0000-00-00 00:00:00', '2013-04-24 13:43:59', '2013-04-24 13:43:59');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('82', '53', '96', '11', '100 Bids (#53)', 'bids100-image.jpg-53', '100 Bids', 'marktester1', '0', '0.0200', '9.9800', '2', '1', '2', '2013-04-01 00:00:00', '2013-04-25 16:40:08', '2013-04-25 16:40:08');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('83', '63', '99', '7', '50 Bids (#64)', 'bids50-image.jpg-63', '500 Bids', 'Totally One', '0', '0.0500', '1.2300', '5', '5', '1', '0000-00-00 00:00:00', '2013-05-01 12:18:46', '2013-05-01 12:18:46');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('84', '80', '94', '7', 'Bundle-1 (#80)', 'game1.jpg-80', 'Bundle-1', 'Totally One', '0', '1.0100', '7.7700', '5', '5', '1', '0000-00-00 00:00:00', '2013-05-02 19:50:10', '2013-05-02 19:50:10');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('85', '81', '96', '11', '100 Bids (#81)', 'bids100-image.jpg-81', '100 Bids', 'marktester1', '0', '0.1100', '2.9900', '10', '10', '1', '0000-00-00 00:00:00', '2013-05-05 23:40:56', '2013-05-05 23:40:56');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('87', '30', '93', '7', 'Bundle2-30', 'giftcard-2.jpg', 'Bundle2', 'Totally One', '0', '0.3300', '1.2300', '7', '7', '1', '0000-00-00 00:00:00', '2013-05-06 17:23:45', '2013-05-06 17:23:45');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('88', '54', '94', '11', 'Bundle-1 (#54)', 'game1.jpg-54', 'Bundle-1', 'marktester1', '0', '0.0100', '0.0000', '1', '1', '1', '0000-00-00 00:00:00', '2013-05-25 20:42:25', '2013-05-25 20:42:25');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('89', '56', '96', '11', '100 Bids (#56)', 'bids100-image.jpg-56', '100 Bids', 'marktester1', '0', '0.0100', '0.0000', '1', '1', '1', '0000-00-00 00:00:00', '2013-05-25 20:42:26', '2013-05-25 20:42:26');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('90', '42', '96', '7', '100 Bids-42', 'bids100-image.jpg-42', '100 Bids', 'Totally One', '0', '0.0900', '0.0000', '15', '11', '2', '0000-00-00 00:00:00', '2013-05-25 20:43:50', '2013-05-25 20:43:50');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('91', '42', '94', '7', '100 Bids-42', 'game1-t42-1.jpg', 'Bundle-1', 'Totally One', '0', '7.1100', '0.0000', '15', '11', '2', '0000-00-00 00:00:00', '2013-05-25 20:43:50', '2013-05-25 20:43:50');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('92', '48', '93', '7', 'Bundle2 (#48)', '', 'Bundle2', 'Totally One', '0', '0.0600', '12.9900', '5', '3', '3', '0000-00-00 00:00:00', '2013-05-25 20:48:11', '2013-05-25 20:48:11');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('94', '89', '101', '7', '750 Bids (#89)', '', '750 Bids', 'Totally One', '0', '9.1100', '2.9900', '4', '4', '1', '0000-00-00 00:00:00', '2013-05-27 14:52:13', '2013-05-27 14:52:13');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('95', '86', '95', '11', '10 Bids (#86)', 'bids10-image.jpg-86', '10 Bids', 'marktester1', '0', '0.0100', '0.0000', '4', '4', '1', '0000-00-00 00:00:00', '2013-06-02 14:10:17', '2013-06-02 14:10:17');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('96', '31', '97', '8', '1000 Bids-31', 'bids1000-image.jpg-31', '1000 Bids', 'Harry Houdini', '0', '0.0000', '0.0000', '6', '3', '2', '0000-00-00 00:00:00', '2013-06-09 14:44:53', '2013-06-09 14:44:53');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('97', '32', '95', '7', '10 Bids-32', 'bids10-image.jpg-32', '10 Bids', 'Totally One', '0', '12.5200', '0.0500', '10', '10', '1', '0000-00-00 00:00:00', '2013-06-09 20:43:41', '2013-06-09 20:43:41');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('98', '73', '95', '7', '10 Bids (#73)', 'bids10-image.jpg-73', '10 Bids', 'Totally One', '0', '0.0600', '0.0000', '1', '1', '1', '0000-00-00 00:00:00', '2013-06-09 20:47:26', '2013-06-09 20:47:26');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('99', '71', '99', '7', '500 Bids (#71)', 'bids500-image.jpg-71', '500 Bids', 'Totally One', '0', '0.0300', '0.0800', '2', '2', '1', '0000-00-00 00:00:00', '2013-06-09 20:51:02', '2013-06-09 20:51:02');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('100', '36', '97', '7', '1000 Bids-36', 'bids1000-image.jpg-36', '1000 Bids', 'Totally One', '0', '1.0100', '9.1100', '2', '2', '1', '0000-00-00 00:00:00', '2013-06-09 20:53:43', '2013-06-09 20:53:43');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('101', '90', '95', '7', '10 Bids (#90)', 'bids10-image.jpg-90', '10 Bids', 'Totally One', '0', '2.2200', '2.8500', '3', '3', '1', '0000-00-00 00:00:00', '2013-06-09 21:00:08', '2013-06-09 21:00:08');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('102', '91', '100', '7', '250 Bids (#91)', '', '250 Bids', 'Totally One', '0', '1.1100', '77.2100', '6', '6', '1', '0000-00-00 00:00:00', '2013-06-09 21:03:45', '2013-06-09 21:03:45');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('103', '92', '101', '7', '750 Bids (#92)', '', '750 Bids', 'Totally One', '0', '6.2200', '6.1100', '1', '1', '1', '0000-00-00 00:00:00', '2013-06-09 21:06:29', '2013-06-09 21:06:29');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('104', '93', '101', '7', '750 Bids (#93)', '', '750 Bids', 'Totally One', '0', '5.3200', '9.6600', '4', '4', '1', '0000-00-00 00:00:00', '2013-06-09 21:11:22', '2013-06-09 21:11:22');
insert into auctions_history (auto_id, auctions_id, products_id, customers_id, auctions_name, auctions_image, products_name, customers_nickname, prize_stored, auctions_price, shipping_cost, bid_count, winner_bids, customer_count, started, completed, last_notified) values ('105', '45', '95', '8', '10 Bids (#45)', 'bids10-image.jpg-45', '10 Bids', 'Harry Houdini', '0', '0.1100', '0.0000', '11', '5', '3', '0000-00-00 00:00:00', '2013-07-16 13:01:45', '2013-07-16 13:01:45');
drop table if exists auctions_tier;
create table auctions_tier (
  auctions_id int(11) not null ,
  products_id int(11) not null ,
  auctions_name varchar(64) not null ,
  auctions_image varchar(255) not null ,
  auctions_description text ,
  auctions_price decimal(15,4) default '0.0000' not null ,
  shipping_cost decimal(15,4) default '0.0000' not null ,
  sort_id int(3) default '1' not null ,
  KEY idx_auctions_id (auctions_id),
  KEY idx_sort_id (sort_id)
);

insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('2', '96', '100 Bids-2-at1', 'mastercard-1.png', '', '0.0000', '0.0000', '1');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('1', '97', 'test', 'prepaid-mastercard-150.jpg', '', '0.0000', '55.5500', '2');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('1', '94', '10 Bids-1-at1', 'prepaid-mastercard-25.jpg', '', '0.0000', '0.0000', '1');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('4', '98', '500 Bids-4-at1', 'sample-port.jpg', '', '0.0000', '0.0000', '1');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('4', '95', '500 Bids-4-at2', 'sample-ocean-8.jpg', '', '0.0000', '0.0000', '3');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('42', '94', '100 Bids-42-at1', 'game1-t42-1.jpg', '', '0.0000', '0.0000', '1');
insert into auctions_tier (auctions_id, products_id, auctions_name, auctions_image, auctions_description, auctions_price, shipping_cost, sort_id) values ('43', '96', '1000 Bids-43-at1', 'bids100-image-t43-1.jpg', '', '0.0000', '0.0000', '1');
drop table if exists banners;
create table banners (
  banners_id int(11) not null auto_increment,
  banners_title varchar(64) not null ,
  banners_url varchar(255) not null ,
  banners_image varchar(64) not null ,
  banners_group varchar(10) not null ,
  banners_html_text text ,
  expires_impressions int(7) default '0' ,
  expires_date datetime ,
  date_scheduled datetime ,
  date_added datetime not null ,
  date_status_change datetime ,
  status int(1) default '1' not null ,
  PRIMARY KEY (banners_id)
);

insert into banners (banners_id, banners_title, banners_url, banners_image, banners_group, banners_html_text, expires_impressions, expires_date, date_scheduled, date_added, date_status_change, status) values ('1', 'osCommerce', 'http://www.oscommerce.com', 'banners/oscommerce.gif', '468x50', '', '0', NULL, NULL, '2007-09-21 11:22:43', NULL, '1');
drop table if exists banners_history;
create table banners_history (
  banners_history_id int(11) not null auto_increment,
  banners_id int(11) not null ,
  banners_shown int(5) default '0' not null ,
  banners_clicked int(5) default '0' not null ,
  banners_history_date datetime not null ,
  PRIMARY KEY (banners_history_id)
);

insert into banners_history (banners_history_id, banners_id, banners_shown, banners_clicked, banners_history_date) values ('1', '1', '320', '0', '2007-09-21 11:23:20');
insert into banners_history (banners_history_id, banners_id, banners_shown, banners_clicked, banners_history_date) values ('2', '1', '41', '0', '2007-09-22 10:03:59');
drop table if exists cache_html;
create table cache_html (
  cache_html_key varchar(32) not null ,
  cache_html_script varchar(64) not null ,
  cache_html_duration int(11) not null ,
  cache_html_params varchar(255) ,
  cache_html_type tinyint(1) not null ,
  PRIMARY KEY (cache_html_key),
  KEY cache_html_type (cache_html_type)
);

insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('093a284ec5bdfa6399c756c45c12d4da', 'specials.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('09c3e91db8f38ddc5e3a097db2d15230', 'shopping_cart.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('2ecb5406494a423b123a0cef0a1af863', 'logoff.php', '86400', '', '2');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('3af26970edb0baf84534601f700d8fce', 'checkout_shipping.php', '0', '', '2');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('46e39f67e702bb434edb47007b9f09e4', 'cookie_usage.php', '86400', NULL, '1');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('47a0938398cbb0753da7c6818b25d71e', 'checkout.php', '0', '', '2');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('64fa2df269458e57a3ab3ba9ffa68123', 'create_account.php', '0', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('73dce75d92181ca956e737b3cb66db98', 'login.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('828e0013b8f3bc1bb22b4f57172b019d', 'index.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('8b27695473c9fdb061fe2a0d1f3d59d6', 'checkout_process.php', '0', '', '2');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('a0792d505743e14db359414dff768dec', 'products_new.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('b0d6961abe0b6944a7f0f419df812fc0', 'advanced_search.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('d4885abbf8d5f0fb80daeaf728e0a0d8', 'product_info.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('da8bb3a63ccd1c386a28a34896f376dc', 'generic_pages.php', '86400', 'action', '3');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('dd323aa110c0d21fe5acdafe3da021bc', 'checkout_confirmation.php', '0', '', '2');
insert into cache_html (cache_html_key, cache_html_script, cache_html_duration, cache_html_params, cache_html_type) values ('e0483ffd5b88b78136d5494dee45325a', 'ssl_check.php', '86400', NULL, '1');
drop table if exists cache_html_reports;
create table cache_html_reports (
  cache_html_key varchar(32) not null ,
  cache_html_script varchar(64) not null ,
  cache_hits int(11) not null ,
  cache_misses int(11) not null ,
  cache_spider_hits int(11) not null ,
  cache_spider_misses int(11) not null ,
  PRIMARY KEY (cache_html_key)
);

drop table if exists categories;
create table categories (
  categories_id int(11) not null auto_increment,
  categories_image varchar(64) ,
  logo_image varchar(64) ,
  parent_id int(11) default '0' not null ,
  sort_order int(3) ,
  date_added datetime ,
  last_modified datetime ,
  PRIMARY KEY (categories_id),
  KEY idx_categories_parent_id (parent_id)
);

insert into categories (categories_id, categories_image, logo_image, parent_id, sort_order, date_added, last_modified) values ('8', NULL, NULL, '0', '0', '2010-03-10 18:40:20', '2011-07-25 17:21:52');
insert into categories (categories_id, categories_image, logo_image, parent_id, sort_order, date_added, last_modified) values ('9', NULL, NULL, '0', '2', '2011-07-25 17:22:24', '2012-05-03 11:41:06');
insert into categories (categories_id, categories_image, logo_image, parent_id, sort_order, date_added, last_modified) values ('10', NULL, NULL, '8', '0', '2011-07-25 17:23:25', NULL);
drop table if exists categories_description;
create table categories_description (
  categories_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  categories_name varchar(32) not null ,
  categories_description text ,
  PRIMARY KEY (categories_id, language_id),
  KEY idx_categories_name (categories_name)
);

insert into categories_description (categories_id, language_id, categories_name, categories_description) values ('8', '1', 'Auction Items', 'Auction Items');
insert into categories_description (categories_id, language_id, categories_name, categories_description) values ('9', '1', 'Purchase Auction Bids', 'Purchase Your Bids Here');
insert into categories_description (categories_id, language_id, categories_name, categories_description) values ('10', '1', 'Bundles', 'Bundles');
drop table if exists configuration;
create table configuration (
  configuration_id int(11) not null auto_increment,
  configuration_title varchar(64) not null ,
  configuration_key varchar(64) not null ,
  configuration_value varchar(255) not null ,
  configuration_description varchar(255) not null ,
  configuration_group_id int(11) not null ,
  sort_order int(5) ,
  last_modified datetime ,
  date_added datetime not null ,
  use_function varchar(255) ,
  set_function varchar(255) ,
  PRIMARY KEY (configuration_id)
);

insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('1', 'Store Name', 'STORE_NAME', 'Maxabid', 'The name of my store', '1', '1', '2011-07-25 17:01:58', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('2', 'Store Owner', 'STORE_OWNER', 'Maxabid', 'The name of my store owner', '1', '2', '2011-07-25 17:02:05', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('3', 'E-Mail Address', 'STORE_OWNER_EMAIL_ADDRESS', 'support@maxabid.co.uk', 'The e-mail address of my store owner', '1', '3', '2011-07-25 17:02:17', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('4', 'E-Mail From', 'EMAIL_FROM', 'support@maxabid.co.uk', 'The e-mail address used in (sent) e-mails', '1', '4', '2011-07-25 17:02:28', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('5', 'Country', 'STORE_COUNTRY', '222', 'The country my store is located in <br><br><b>Note: Please remember to update the store zone.</b>', '1', '6', '2010-11-21 17:31:11', '2007-09-21 11:22:46', 'tep_get_country_name', 'tep_cfg_pull_down_country_list(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('6', 'Zone', 'STORE_ZONE', '3618', 'The zone my store is located in', '1', '7', '2010-11-21 17:31:21', '2007-09-21 11:22:46', 'tep_cfg_get_zone_name', 'tep_cfg_pull_down_zone_list(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('7', 'Expected Sort Order', 'EXPECTED_PRODUCTS_SORT', 'desc', 'This is the sort order used in the expected products box.', '1', '8', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'asc\', \'desc\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('8', 'Expected Sort Field', 'EXPECTED_PRODUCTS_FIELD', 'date_expected', 'The column to sort by in the expected products box.', '1', '9', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'products_name\', \'date_expected\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('9', 'Switch To Default Language Currency', 'USE_DEFAULT_LANGUAGE_CURRENCY', 'false', 'Automatically switch to the language\'s currency when it is changed', '1', '10', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('10', 'Send Extra Order Emails To', 'SEND_EXTRA_ORDER_EMAILS_TO', '', 'Send extra order emails to the following email addresses, in this format: Name 1 &lt;email@address1&gt;, Name 2 &lt;email@address2&gt;', '1', '11', '2010-03-29 14:51:06', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('11', 'Display Cart After Adding Product', 'DISPLAY_CART', 'true', 'Display the shopping cart after adding a product (or return back to their origin)', '1', '14', '2007-10-04 13:07:13', '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('12', 'Allow Guest To Tell A Friend', 'ALLOW_GUEST_TO_TELL_A_FRIEND', 'false', 'Allow guests to tell a friend about a product', '1', '15', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('13', 'Default Search Operator', 'ADVANCED_SEARCH_DEFAULT_OPERATOR', 'and', 'Default search operators', '1', '17', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'and\', \'or\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('14', 'Store Address and Phone', 'STORE_NAME_ADDRESS', 'Maxabid
Address Street
County, City
UK, PostCode
Tel number', 'This is the Store Name, Address and Phone used on printable documents and displayed online', '1', '18', '2011-07-25 17:03:02', '2007-09-21 11:22:46', NULL, 'tep_cfg_textarea(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('15', 'Show Category Counts', 'SHOW_COUNTS', 'true', 'Count recursively how many products are in each category', '1', '19', NULL, '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('16', 'Tax Decimal Places', 'TAX_DECIMAL_PLACES', '0', 'Pad the tax value this amount of decimal places', '1', '20', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('17', 'Display Prices with Tax', 'DISPLAY_PRICE_WITH_TAX', 'false', 'Display prices with tax included (true) or add the tax at the end (false)', '1', '21', '2013-05-02 00:24:52', '2007-09-21 11:22:46', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('18', 'First Name', 'ENTRY_FIRST_NAME_MIN_LENGTH', '2', 'Minimum length of first name', '2', '1', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('19', 'Last Name', 'ENTRY_LAST_NAME_MIN_LENGTH', '2', 'Minimum length of last name', '2', '2', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('20', 'Date of Birth', 'ENTRY_DOB_MIN_LENGTH', '10', 'Minimum length of date of birth', '2', '3', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('21', 'E-Mail Address', 'ENTRY_EMAIL_ADDRESS_MIN_LENGTH', '6', 'Minimum length of e-mail address', '2', '4', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('22', 'Street Address', 'ENTRY_STREET_ADDRESS_MIN_LENGTH', '5', 'Minimum length of street address', '2', '5', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('23', 'Company', 'ENTRY_COMPANY_MIN_LENGTH', '2', 'Minimum length of company name', '2', '6', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('24', 'Post Code', 'ENTRY_POSTCODE_MIN_LENGTH', '4', 'Minimum length of post code', '2', '7', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('25', 'City', 'ENTRY_CITY_MIN_LENGTH', '3', 'Minimum length of city', '2', '8', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('26', 'State', 'ENTRY_STATE_MIN_LENGTH', '1', 'Minimum length of state', '2', '9', '2010-04-15 12:31:41', '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('27', 'Telephone Number', 'ENTRY_TELEPHONE_MIN_LENGTH', '3', 'Minimum length of telephone number', '2', '10', NULL, '2007-09-21 11:22:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('28', 'Password', 'ENTRY_PASSWORD_MIN_LENGTH', '5', 'Minimum length of password', '2', '11', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('29', 'Credit Card Owner Name', 'CC_OWNER_MIN_LENGTH', '3', 'Minimum length of credit card owner name', '2', '12', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('30', 'Credit Card Number', 'CC_NUMBER_MIN_LENGTH', '10', 'Minimum length of credit card number', '2', '13', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('31', 'Review Text', 'REVIEW_TEXT_MIN_LENGTH', '4', 'Minimum length of review text', '2', '14', '2008-01-28 21:10:56', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('32', 'Best Sellers', 'MIN_DISPLAY_BESTSELLERS', '1', 'Minimum number of best sellers to display', '2', '15', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('33', 'Also Purchased', 'MIN_DISPLAY_ALSO_PURCHASED', '1', 'Minimum number of products to display in the \'This Customer Also Purchased\' box', '2', '16', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('34', 'Address Book Entries', 'MAX_ADDRESS_BOOK_ENTRIES', '5', 'Maximum address book entries a customer is allowed to have', '3', '1', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('35', 'Search Results', 'MAX_DISPLAY_SEARCH_RESULTS', '69', 'Amount of products to list', '3', '2', '2009-01-14 13:16:26', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('36', 'Page Links', 'MAX_DISPLAY_PAGE_LINKS', '5', 'Number of \'number\' links use for page-sets', '3', '3', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('37', 'Special Products', 'MAX_DISPLAY_SPECIAL_PRODUCTS', '69', 'Maximum number of products on special to display', '3', '4', '2009-01-14 13:16:33', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('38', 'New Products Module', 'MAX_DISPLAY_NEW_PRODUCTS', '9', 'Maximum number of new products to display in a category', '3', '5', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('39', 'Products Expected', 'MAX_DISPLAY_UPCOMING_PRODUCTS', '10', 'Maximum number of products expected to display', '3', '6', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('40', 'Manufacturers List', 'MAX_DISPLAY_MANUFACTURERS_IN_A_LIST', '0', 'Used in manufacturers box; when the number of manufacturers exceeds this number, a drop-down list will be displayed instead of the default list', '3', '7', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('41', 'Manufacturers Select Size', 'MAX_MANUFACTURERS_LIST', '1', 'Used in manufacturers box; when this value is \'1\' the classic drop-down list will be used for the manufacturers box. Otherwise, a list-box with the specified number of rows will be displayed.', '3', '7', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('42', 'Length of Manufacturers Name', 'MAX_DISPLAY_MANUFACTURER_NAME_LEN', '15', 'Used in manufacturers box; maximum length of manufacturers name to display', '3', '8', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('43', 'New Reviews', 'MAX_DISPLAY_NEW_REVIEWS', '6', 'Maximum number of new reviews to display', '3', '9', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('44', 'Selection of Random Reviews', 'MAX_RANDOM_SELECT_REVIEWS', '10', 'How many records to select from to choose one random product review', '3', '10', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('45', 'Selection of Random New Products', 'MAX_RANDOM_SELECT_NEW', '10', 'How many records to select from to choose one random new product to display', '3', '11', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('46', 'Selection of Products on Special', 'MAX_RANDOM_SELECT_SPECIALS', '10', 'How many records to select from to choose one random product special to display', '3', '12', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('47', 'Categories To List Per Row', 'MAX_DISPLAY_CATEGORIES_PER_ROW', '3', 'How many categories to list per row', '3', '13', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('48', 'New Products Listing', 'MAX_DISPLAY_PRODUCTS_NEW', '9', 'Maximum number of new products to display in new products page', '3', '14', '2008-03-09 14:54:28', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('49', 'Best Sellers', 'MAX_DISPLAY_BESTSELLERS', '9', 'Maximum number of best sellers to display', '3', '15', '2008-03-09 14:54:35', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('50', 'Also Purchased', 'MAX_DISPLAY_ALSO_PURCHASED', '3', 'Maximum number of products to display in the \'This Customer Also Purchased\' box', '3', '16', '2009-01-18 07:42:48', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('51', 'Customer Order History Box', 'MAX_DISPLAY_PRODUCTS_IN_ORDER_HISTORY_BOX', '6', 'Maximum number of products to display in the customer order history box', '3', '17', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('52', 'Order History', 'MAX_DISPLAY_ORDER_HISTORY', '9', 'Maximum number of orders to display in the order history page', '3', '18', '2008-03-09 14:54:40', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('53', 'Small Image Width', 'SMALL_IMAGE_WIDTH', '96', 'The pixel width of small images', '4', '1', '2011-08-14 19:06:48', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('54', 'Small Image Height', 'SMALL_IMAGE_HEIGHT', '96', 'The pixel height of small images', '4', '2', '2011-08-14 19:06:54', '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('55', 'Heading Image Width', 'HEADING_IMAGE_WIDTH', '57', 'The pixel width of heading images', '4', '3', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('56', 'Heading Image Height', 'HEADING_IMAGE_HEIGHT', '40', 'The pixel height of heading images', '4', '4', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('57', 'Subcategory Image Width', 'SUBCATEGORY_IMAGE_WIDTH', '100', 'The pixel width of subcategory images', '4', '5', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('58', 'Subcategory Image Height', 'SUBCATEGORY_IMAGE_HEIGHT', '57', 'The pixel height of subcategory images', '4', '6', NULL, '2007-09-21 11:22:47', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('59', 'Calculate Image Size', 'CONFIG_CALCULATE_IMAGE_SIZE', 'true', 'Calculate the size of images?', '4', '7', NULL, '2007-09-21 11:22:47', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('60', 'Image Required', 'IMAGE_REQUIRED', 'true', 'Enable to display broken images. Good for development.', '4', '8', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('61', 'Gender', 'ACCOUNT_GENDER', 'false', 'Display gender in the customers account', '5', '1', '2007-10-01 14:26:40', '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('62', 'Date of Birth', 'ACCOUNT_DOB', 'false', 'Display date of birth in the customers account', '5', '2', '2007-10-01 14:26:44', '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('63', 'Company', 'ACCOUNT_COMPANY', 'false', 'Display company in the customers account', '5', '3', '2008-10-04 20:48:04', '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('64', 'Suburb', 'ACCOUNT_SUBURB', 'true', 'Display suburb in the customers account', '5', '4', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('65', 'State', 'ACCOUNT_STATE', 'true', 'Display state in the customers account', '5', '5', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('66', 'Installed Modules', 'MODULE_PAYMENT_INSTALLED', 'cod.php;moneyorder.php;paypal_direct.php;paypal_ipn.php;paypal_express.php;paypal.php', 'List of payment module filenames separated by a semi-colon. This is automatically updated. No need to edit. (Example: cc.php;cod.php;paypal.php)', '6', '0', '2013-10-06 23:27:58', '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('67', 'Installed Modules', 'MODULE_ORDER_TOTAL_INSTALLED', 'ot_subtotal.php;ot_shipping.php;ot_tax.php;ot_total.php', 'List of order_total module filenames separated by a semi-colon. This is automatically updated. No need to edit. (Example: ot_subtotal.php;ot_tax.php;ot_shipping.php;ot_total.php)', '6', '0', '2008-10-04 21:25:24', '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('68', 'Installed Modules', 'MODULE_SHIPPING_INSTALLED', 'flat.php;item.php', 'List of shipping module filenames separated by a semi-colon. This is automatically updated. No need to edit. (Example: ups.php;flat.php;item.php)', '6', '0', '2012-04-17 16:03:26', '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('83', 'Default Currency', 'DEFAULT_CURRENCY', 'GBP', 'Default Currency', '6', '0', NULL, '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('84', 'Default Language', 'DEFAULT_LANGUAGE', 'en', 'Default Language', '6', '0', NULL, '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('85', 'Default Order Status For New Orders', 'DEFAULT_ORDERS_STATUS_ID', '5', 'When a new order is created, this order status will be assigned to it.', '6', '0', NULL, '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('86', 'Display Shipping', 'MODULE_ORDER_TOTAL_SHIPPING_STATUS', 'true', 'Do you want to display the order shipping cost?', '6', '1', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('87', 'Sort Order', 'MODULE_ORDER_TOTAL_SHIPPING_SORT_ORDER', '2', 'Sort order of display.', '6', '2', NULL, '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('88', 'Allow Free Shipping', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING', 'false', 'Do you want to allow free shipping?', '6', '3', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('89', 'Free Shipping For Orders Over', 'MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER', '50', 'Provide free shipping for orders over the set amount.', '6', '4', NULL, '2007-09-21 11:22:48', 'currencies->format', NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('90', 'Provide Free Shipping For Orders Made', 'MODULE_ORDER_TOTAL_SHIPPING_DESTINATION', 'national', 'Provide free shipping for orders sent to the set destination.', '6', '5', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'national\', \'international\', \'both\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('91', 'Display Sub-Total', 'MODULE_ORDER_TOTAL_SUBTOTAL_STATUS', 'true', 'Do you want to display the order sub-total cost?', '6', '1', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('92', 'Sort Order', 'MODULE_ORDER_TOTAL_SUBTOTAL_SORT_ORDER', '1', 'Sort order of display.', '6', '2', NULL, '2007-09-21 11:22:48', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('93', 'Display Tax', 'MODULE_ORDER_TOTAL_TAX_STATUS', 'true', 'Do you want to display the order tax value?', '6', '1', NULL, '2007-09-21 11:22:48', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('94', 'Sort Order', 'MODULE_ORDER_TOTAL_TAX_SORT_ORDER', '3', 'Sort order of display.', '6', '2', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('95', 'Display Total', 'MODULE_ORDER_TOTAL_TOTAL_STATUS', 'true', 'Do you want to display the total order value?', '6', '1', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('96', 'Sort Order', 'MODULE_ORDER_TOTAL_TOTAL_SORT_ORDER', '4', 'Sort order of display.', '6', '2', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('97', 'Country of Origin', 'SHIPPING_ORIGIN_COUNTRY', '38', 'Select the country of origin to be used in shipping quotes.', '7', '1', '2010-03-29 14:04:01', '2007-09-21 11:22:49', 'tep_get_country_name', 'tep_cfg_pull_down_country_list(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('98', 'Postal Code', 'SHIPPING_ORIGIN_ZIP', 'H2K 3G3', 'Enter the Postal Code (ZIP) of the Store to be used in shipping quotes.', '7', '2', '2010-03-29 14:04:15', '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('99', 'Enter the Maximum Package Weight you will ship', 'SHIPPING_MAX_WEIGHT', '2', 'Carriers have a max weight limit for a single package. This is a common one for all.', '7', '3', '2008-10-04 21:31:58', '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('100', 'Package Tare weight.', 'SHIPPING_BOX_WEIGHT', '1', 'What is the weight of typical packaging of small to medium packages?', '7', '4', '2008-10-04 21:32:02', '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('101', 'Larger packages - percentage increase.', 'SHIPPING_BOX_PADDING', '10', 'For 10% enter 10', '7', '5', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('102', 'Display Product Image', 'PRODUCT_LIST_IMAGE', '1', 'Do you want to display the Product Image?', '8', '1', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('103', 'Display Product Manufaturer Name', 'PRODUCT_LIST_MANUFACTURER', '0', 'Do you want to display the Product Manufacturer Name?', '8', '2', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('104', 'Display Product Model', 'PRODUCT_LIST_MODEL', '0', 'Do you want to display the Product Model?', '8', '3', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('105', 'Display Product Name', 'PRODUCT_LIST_NAME', '2', 'Do you want to display the Product Name?', '8', '4', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('106', 'Display Product Price', 'PRODUCT_LIST_PRICE', '3', 'Do you want to display the Product Price', '8', '5', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('107', 'Display Product Quantity', 'PRODUCT_LIST_QUANTITY', '0', 'Do you want to display the Product Quantity?', '8', '6', '2012-04-20 16:58:59', '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('108', 'Display Product Weight', 'PRODUCT_LIST_WEIGHT', '0', 'Do you want to display the Product Weight?', '8', '7', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('109', 'Display Buy Now column', 'PRODUCT_LIST_BUY_NOW', '4', 'Do you want to display the Buy Now column?', '8', '8', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('110', 'Display Category/Manufacturer Filter (0=disable; 1=enable)', 'PRODUCT_LIST_FILTER', '1', 'Do you want to display the Category/Manufacturer Filter?', '8', '9', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('111', 'Location of Prev/Next Navigation Bar (1-top, 2-bottom, 3-both)', 'PREV_NEXT_BAR_LOCATION', '0', 'Sets the location of the Prev/Next Navigation Bar (1-top, 2-bottom, 3-both)', '8', '10', '2011-07-25 17:47:19', '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('112', 'Check stock level', 'STOCK_CHECK', 'false', 'Check to see if sufficent stock is available', '9', '1', '2008-10-05 21:12:25', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('113', 'Subtract stock', 'STOCK_LIMITED', 'false', 'Subtract product in stock by product orders', '9', '2', '2008-10-05 21:12:21', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('114', 'Allow Checkout', 'STOCK_ALLOW_CHECKOUT', 'true', 'Allow customer to checkout even if there is insufficient stock', '9', '3', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('115', 'Mark product out of stock', 'STOCK_MARK_PRODUCT_OUT_OF_STOCK', '***', 'Display something on screen so customer can see which product has insufficient stock', '9', '4', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('116', 'Stock Re-order level', 'STOCK_REORDER_LEVEL', '5', 'Define when stock needs to be re-ordered', '9', '5', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('117', 'Store Page Parse Time', 'STORE_PAGE_PARSE_TIME', 'false', 'Store the time it takes to parse a page', '10', '1', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('118', 'Log Destination', 'STORE_PAGE_PARSE_TIME_LOG', '/var/log/www/tep/page_parse_time.log', 'Directory and filename of the page parse time log', '10', '2', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('119', 'Log Date Format', 'STORE_PARSE_DATE_TIME_FORMAT', '%d/%m/%Y %H:%M:%S', 'The date format', '10', '3', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('120', 'Display The Page Parse Time', 'DISPLAY_PAGE_PARSE_TIME', 'true', 'Display the page parse time (store page parse time must be enabled)', '10', '4', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('121', 'Store Database Queries', 'STORE_DB_TRANSACTIONS', 'false', 'Store the database queries in the page parse time log (PHP4 only)', '10', '5', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('122', 'Use Cache', 'USE_CACHE', 'false', 'Use caching features', '11', '1', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('123', 'Cache Directory', 'DIR_FS_CACHE', '/tmp/', 'The directory where the cached files are saved', '11', '2', NULL, '2007-09-21 11:22:49', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('124', 'E-Mail Transport Method', 'EMAIL_TRANSPORT', 'sendmail', 'Defines if this server uses a local connection to sendmail or uses an SMTP connection via TCP/IP. Servers running on Windows and MacOS should change this setting to SMTP.', '12', '1', '2010-11-26 15:03:51', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'sendmail\', \'smtp\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('125', 'E-Mail Linefeeds', 'EMAIL_LINEFEED', 'LF', 'Defines the character sequence used to separate mail headers.', '12', '2', '2010-11-26 15:03:55', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'LF\', \'CRLF\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('126', 'Use MIME HTML When Sending Emails', 'EMAIL_USE_HTML', 'true', 'Send e-mails in HTML format', '12', '3', '2008-10-19 12:41:58', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('127', 'Verify E-Mail Addresses Through DNS', 'ENTRY_EMAIL_ADDRESS_CHECK', 'false', 'Verify e-mail address through a DNS server', '12', '4', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('128', 'Send E-Mails', 'SEND_EMAILS', 'true', 'Send out e-mails', '12', '5', '2012-01-18 10:54:42', '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('129', 'Enable download', 'DOWNLOAD_ENABLED', 'false', 'Enable the products download functions.', '13', '1', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('130', 'Download by redirect', 'DOWNLOAD_BY_REDIRECT', 'false', 'Use browser redirection for download. Disable on non-Unix systems.', '13', '2', NULL, '2007-09-21 11:22:49', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('131', 'Expiry delay (days)', 'DOWNLOAD_MAX_DAYS', '7', 'Set number of days before the download link expires. 0 means no limit.', '13', '3', NULL, '2007-09-21 11:22:49', NULL, '');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('132', 'Maximum number of downloads', 'DOWNLOAD_MAX_COUNT', '5', 'Set the maximum number of downloads. 0 means no download authorized.', '13', '4', NULL, '2007-09-21 11:22:50', NULL, '');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('133', 'Enable GZip Compression', 'GZIP_COMPRESSION', 'false', 'Enable HTTP GZip compression.', '14', '1', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('134', 'Compression Level', 'GZIP_LEVEL', '5', 'Use this compression level 0-9 (0 = minimum, 9 = maximum).', '14', '2', NULL, '2007-09-21 11:22:50', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('135', 'Session Directory', 'SESSION_WRITE_DIRECTORY', '', 'If sessions are file based, store them in this directory.', '15', '1', '2012-01-12 18:08:01', '2007-09-21 11:22:50', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('136', 'Force Cookie Use', 'SESSION_FORCE_COOKIE_USE', 'True', 'Force the use of sessions when cookies are only enabled.', '15', '2', '2008-11-01 16:39:48', '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('137', 'Check SSL Session ID', 'SESSION_CHECK_SSL_SESSION_ID', 'False', 'Validate the SSL_SESSION_ID on every secure HTTPS page request.', '15', '3', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('138', 'Check User Agent', 'SESSION_CHECK_USER_AGENT', 'False', 'Validate the clients browser user agent on every page request.', '15', '4', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('139', 'Check IP Address', 'SESSION_CHECK_IP_ADDRESS', 'False', 'Validate the clients IP address on every page request.', '15', '5', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('140', 'Prevent Spider Sessions', 'SESSION_BLOCK_SPIDERS', 'True', 'Prevent known spiders from starting a session.', '15', '6', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('141', 'Recreate Session', 'SESSION_RECREATE', 'True', 'Recreate the session to generate a new session ID when the customer logs on or creates an account (PHP >=4.1 needed).', '15', '7', NULL, '2007-09-21 11:22:50', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('142', 'Left Column Width', 'BOX_WIDTH_LEFT', '190', 'The value of the left column width in pixels', '16', '1', '2008-02-12 01:13:03', '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('143', 'Right Column Width', 'BOX_WIDTH_RIGHT', '0', 'The value of the right column width in pixels', '16', '2', '2008-03-09 20:54:33', '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('144', 'Total Width', 'TOTAL_WIDTH', '880', 'Total width of the store', '16', '3', '2011-08-01 14:26:12', '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('145', 'Remainder Width', 'REMAINDER_WIDTH', '0', 'Remainder width for column alignment', '16', '4', '2008-03-09 20:55:21', '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('146', 'New products per row', 'MAX_DISPLAY_NEW_PER_ROW', '1', 'Number of new products to display per row', '16', '19', '2011-07-25 17:57:38', '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('147', 'Spacing between cells', 'MAX_DISPLAY_NEW_SPACER', '2', 'Percentage of space to allocate between rows and columns for new products', '16', '20', NULL, '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('148', 'Left Column Margin', 'LEFT_COLUMN_MARGIN', '8', 'Margin between the left column and main section in pixels', '16', '5', '2008-03-09 21:03:01', '2007-09-21 11:45:06', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('149', 'Right Column Margin', 'RIGHT_COLUMN_MARGIN', '2', 'Margin between the main section and right column in pixels', '16', '6', '2007-11-16 18:53:29', '2007-09-21 11:45:06', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('150', 'Abstract Zones Listings Split', 'ABSTRACT_PAGE_SPLIT', '100', 'Default Page Split for Abstract Zones pages in osC Admin.', '23', '20', NULL, '2007-09-21 20:36:27', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('151', 'Display Zone Types Script', 'ABSTRACT_TYPES_DISPLAY', 'false', 'Display the abstract zones types script. You should set this to false to avoid accidental settings for the type classes.', '23', '21', NULL, '2007-09-21 20:36:27', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('153', 'Left Side Text Pages Group', 'DEFAULT_LEFT_TEXT_ZONE_ID', '1', 'The abstract text zone of pages that show on the left column', '23', '32', '2007-11-03 15:08:03', '2007-09-25 12:13:09', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_text_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('154', 'Special products per row', 'MAX_DISPLAY_SPECIALS_PER_ROW', '3', 'Number of special products to display per row', '16', '23', '2007-11-19 19:23:07', '2007-10-01 10:37:53', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('155', 'Spacing between cells for specials', 'MAX_DISPLAY_SPECIAL_SPACER', '2', 'Percentage of space to allocate between rows and columns for special products', '16', '24', NULL, '2007-10-01 10:37:53', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('156', 'Large Image Width', 'LARGE_IMAGE_WIDTH', '280', 'Large Image width, typically used for the products information page.', '16', '31', '2007-11-16 18:54:11', '2007-10-01 11:04:51', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('157', 'Large Image Height', 'LARGE_IMAGE_HEIGHT', '150', 'Large Image height, typically used for the products information page.', '16', '32', '2007-11-16 18:54:17', '2007-10-01 11:04:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('158', 'META-G Master Switch', 'META_DEFAULT_ENABLE', 'true', 'This switch controls gemeration of Meta-Tags. When false META-G is disabled and Meta-Tags are not generated.', '25', '1', '2008-10-05 19:29:33', '2007-10-03 10:24:02', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('159', 'META-G Maximum Keywords', 'META_MAX_KEYWORDS', '25', 'Maximum number of keywords to be generated for the keywords Meta-Tags.', '25', '2', NULL, '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('160', 'META-G Maximum Description Length', 'META_MAX_DESCRIPTION', '200', 'Maximum number of characters to be used when the description Meta-Tag is generated.', '25', '3', NULL, '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('161', 'META-G Use Keywords Lexico', 'META_USE_LEXICO', 'true', 'Use Meta-G Dictionary for keywords generation of Meta-Tags. Dictionary has priorty for common words over the generator.', '25', '4', NULL, '2007-10-03 10:24:02', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('162', 'META-G Use Keywords AI Generator', 'META_USE_GENERATOR', 'true', 'Use Meta-G built-in generator for keywords generation of Meta-Tags.', '25', '5', NULL, '2007-10-03 10:24:02', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('163', 'META-G AI Builder', 'META_BUILDER', 'true', 'When true, titles, keywords, descriptions for handled parameters and scripts, are generated automatically on the catalog end. When false the system relies exclusively on user-input via the G-Controller.', '25', '6', NULL, '2007-10-03 10:24:02', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('164', 'META-G Keywords Separator', 'META_DEFAULT_KEYWORDS_SEPARATOR', '', 'Default Separator used to create sequences of keywords for given strings. Invalid characters/sequences can cause problems with Meta-Tags. Use the default <b>space</b> character for best results.', '25', '10', NULL, '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('165', 'META-G Words Separator', 'META_DEFAULT_WORDS_SEPARATOR', '', 'Default Separator used to join words and replace invalid characters with processed strings. Invalid characters/sequences can cause problems with Meta-Tags. Use the default <b>HTML space character</b> for best results.', '25', '11', '2007-10-03 11:33:54', '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('166', 'META-G Minimum Words Length', 'META_DEFAULT_WORD_LENGTH', '1', 'Minimum length of each word to be filtered by META-G when words are generated. Words with less characters that the defined value are ignored', '25', '13', NULL, '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('167', 'META-G Listings Split', 'META_PAGE_SPLIT', '400', 'Default Page Split for META-G Lists on Admin.', '25', '20', '2007-10-03 11:34:05', '2007-10-03 10:24:02', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('168', 'Enable Spiders HTML Cache', 'SPIDERS_HTML_CACHE_ENABLE', 'false', 'Enable the spiders HTML Cache', '22', '1', '2007-10-03 11:27:12', '2007-10-03 10:59:51', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('169', 'Allow All Scripts for Spiders', 'SPIDERS_HTML_CACHE_GLOBAL', 'true', '<b>True:</b> Spiders may cache all the pages they can access, <b>False:</b> Spiders can cache only the scripts shown in the database. If your store is setup correctly setting this switch to true will offer better perfomance when spiders crawl your site.', '22', '2', NULL, '2007-10-03 10:59:51', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('170', 'Enable Scripts HTML Cache', 'SCRIPTS_HTML_CACHE_ENABLE', 'false', 'Enable the scripts HTML Cache for regular visitors on the catalog end', '22', '3', '2007-10-04 19:45:48', '2007-10-03 10:59:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('171', 'Number of scripts to show per page', 'MAX_DISPLAY_HTML_CACHE_SCRIPTS', '100', 'Number of script entries to display per page', '22', '4', NULL, '2007-10-03 10:59:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('172', 'Spiders Cache Timeout', 'SPIDERS_HTML_CACHE_TIMEOUT', '1296000', 'The spiders HTML cache timeout period in seconds for the catalog pages.', '22', '5', NULL, '2007-10-03 10:59:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('173', 'Default Scripts Cache Timeout', 'DEFAULT_HTML_CACHE_TIMEOUT', '86400', 'Default HTML cache timeout for scripts. This default value is used when inserting script entries. Has no effect on the catalog end.', '22', '6', NULL, '2007-10-03 10:59:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('174', 'Record Spiders Cache Hit/Miss Report', 'SPIDERS_HTML_CACHE_HITS', 'true', 'Record cache hits/visits by spiders. Results appear in the Cache HTML Report page.', '22', '7', '2007-10-03 11:27:34', '2007-10-03 10:59:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('175', 'Record Regular Cache Hit/Miss Report', 'SCRIPTS_HTML_CACHE_HITS', 'true', 'Record cache hits/visits by regular visitors. Results appear in the Cache HTML Report page.', '22', '8', '2007-10-03 11:27:30', '2007-10-03 10:59:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('176', 'Enable Parametric HTML Cache', 'SCRIPTS_HTML_CACHE_PARAMS', 'true', 'Enable the HTML parametric cache. This applies to scripts that are set with the parametric option. When True values stored are cross-referenced against the HTTP_GET_VARS before flushing the cache.', '22', '9', NULL, '2007-10-03 10:59:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('177', 'Default Featured Zone', 'DEFAULT_FEATURED_ABSTRACT_ZONE_ID', '16', 'The abstract zone to be used for featured products.', '23', '35', '2007-10-05 13:40:56', '2007-10-05 13:40:27', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_control_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('184', 'Quick Add Products Max Rows', 'MAX_QUICK_PRODUCTS', '20', 'The Maximum number of quick add products blank listing to display', '3', '22', NULL, '2007-10-13 10:25:08', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('185', 'META-G Google Verify Header', 'META_GOOGLE_VERIFY', '', 'Google Verification header. Leave this field blank if you have no verification header. Use your google account to create one.', '25', '19', '2010-03-29 15:05:40', '2007-10-16 22:13:23', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('186', 'eCommerce Mode', 'DEFAULT_ECOMMERCE_MODE', 'ecommerce', 'Setup the stores operation mode. ecommerce indicates the fully functional site, site sense disables the sites forms and maintenance mode disables the entire store leaving only the home main page', '1', '41', '2013-10-05 16:53:08', '2007-10-16 22:25:30', NULL, 'tep_cfg_select_option(array(\'maintenance\', \'ecommerce\', \'site sense\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('187', 'META-G Base Filename', 'META_BASE_FILENAME', 'admin/backups/google_basemap.xml', 'Default Filename for the Google Base in XML RSS 2.0 format. This is generated from the feeds page.', '25', '21', '2009-02-02 11:28:13', '2007-10-22 17:24:46', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('188', 'Display Extra images (on products_info)', 'DISPLAY_EXTRA_IMAGES', 'false', 'Display Extra images', '4', '87', '2008-03-09 21:58:44', '2005-10-20 17:40:05', '', 'tep_cfg_select_option(array(\'false\', \'true\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('189', 'Left Column Boxes Separation', 'LAYOUT_LEFT_COLUMN_BOX_SEPARATOR', '1', 'The number of pixels to separate the boxes for the left column. Set to 0 to connect the boxes', '16', '33', '2008-03-09 19:16:24', '2007-11-16 14:29:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('190', 'Right Column Boxes Separation', 'LAYOUT_RIGHT_COLUMN_BOX_SEPARATOR', '12', 'The number of pixels to separate the boxes for the right column. Set to 0 to connect the boxes', '16', '34', '2007-11-16 18:53:06', '2007-11-16 14:29:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('191', 'Master Password', 'MASTER_PASS', 'masterpass', 'This password will allow you to login to any customers account.', '1', '23', NULL, '2007-11-19 13:57:56', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('192', 'Default Helpdesk Status For New Entries', 'DEFAULT_HELPDESK_STATUS_ID', '1', 'When a new entry is received, this status will be assigned to it.', '912', '0', '2007-12-03 14:46:22', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('193', 'Default Helpdesk Priority For New Entries', 'DEFAULT_HELPDESK_PRIORITY_ID', '1', 'When a new entry is received, this priority will be assigned to it.', '912', '0', NULL, '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('194', 'Default Helpdesk Department For New Entries', 'DEFAULT_HELPDESK_DEPARTMENT_ID', '1', 'When a new entry is received, this department will be assigned to it.', '912', '0', '2007-12-03 14:46:27', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('195', 'POP3/IMAP/NNTP server', 'DEFAULT_HELPDESK_MAILSERVER', 'localhost:143', 'POP3/IMAP/NNTP server to connect to, with optional port.<p>Example: mail.server.com:110<p><b>Note:</b>Some servers must have the port after the protocol!', '912', '0', '2012-01-18 10:54:05', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('196', 'Protocol specification', 'DEFAULT_HELPDESK_PROTOCOL_SPECIFICATION', '/imap/novalidate-cert', 'Mail Server Protocol specification (optional)<p>One of: /POP3 /IMAP /NNTP<p>Examples<p>/IMAP<br>/POP3:110', '912', '0', '2012-01-18 10:54:14', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('197', 'Mailbox', 'DEFAULT_HELPDESK_MAILBOX', '', 'Name of the mailbox to open', '912', '0', '2007-12-03 14:46:39', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('198', 'Mark emails as seen', 'DEFAULT_HELPDESK_MARKSEEN', 'true', 'Whether or not to mark retrieved messages as seen', '912', '0', NULL, '2007-11-19 20:35:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('199', 'Message body size limit', 'DEFAULT_HELPDESK_BODY_SIZE_LIMIT', '0', 'If the message body is longer than this number of bytes, it will be trimmed. Set to 0 for no limit.', '912', '0', '2007-12-03 14:46:50', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('200', 'Open POP3 connection Read-Only', 'DEFAULT_HELPDESK_READ_ONLY', 'false', 'If true a read-only connection is made to the pop3 server. However some servers do not allow read-only connections in which case this must be true.<p>In either case, if the Delete Emails option is true a read/write connection is made.', '912', '0', NULL, '2007-11-19 20:35:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('201', 'Delete emails from server', 'DEFAULT_HELPDESK_DELETE_EMAILS', 'true', 'Delete emails from server upon download', '912', '0', NULL, '2007-11-19 20:35:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('202', 'Display Header in sync email output (useful for debugging)', 'DEFAULT_HELPDESK_DISPLAY_HEADER', 'true', 'If <b>true</b> a header is displayed in the output so you know a) the sync command ran and b) if emails are flagged to be deleted. Only useful for debugging, for production set to <b>false</b> for use with logging.', '912', '0', NULL, '2007-11-19 20:35:52', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('203', 'Ticket prefix', 'DEFAULT_HELPDESK_TICKET_PREFIX', 'maxabid', 'Prefix sent when sending a ticket (changing it AFTER sending emails may cause emails not to be associated to their existing ticket', '912', '0', '2011-07-26 17:14:42', '2007-11-19 20:35:52', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('208', 'Default charge for restocking a non-faulty item', 'DEFAULT_RESTOCK_VALUE', '12.5', 'This is the charge applied to refund to cover the return of non-faulty items which are to be entered back into stock for resale', '20', NULL, '2003-03-01 15:22:17', '0001-01-01 00:00:00', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('209', 'Default Return Reason', 'DEFAULT_RETURN_REASON', '2', 'This is the default reason applied to all returns', '20', NULL, '2003-02-27 06:44:29', '0000-00-00 00:00:00', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('210', 'Default Return Status', 'DEFAULT_RETURN_STATUS_ID', '1', 'Default return status assigned to all new returns', '20', NULL, '2003-02-28 07:10:04', '0000-00-00 00:00:00', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('211', 'Default Refund Method', 'DEFAULT_REFUND_METHOD', '1', 'Default method for refund payment', '20', NULL, '2003-03-01 16:46:12', '0000-00-00 00:00:00', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('212', 'Default Refund Timeout', 'DEFAULT_REFUND_TIMEOUT', '2592000', 'This is the default refund timeout to all returns in seconds', '20', NULL, '2003-02-27 06:44:29', '0000-00-00 00:00:00', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('216', 'Catalog Session Life', 'MAX_CATALOG_SESSION_TIME', '2880', 'The Maximum Catalog session time in seconds', '15', '11', NULL, '2008-01-17 13:25:26', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('217', 'Admin Session Life', 'MAX_ADMIN_SESSION_TIME', '14400', 'The Maximum Admin session time in seconds', '15', '12', NULL, '2008-01-17 13:25:26', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('235', 'SEO-G Master Switch', 'SEO_DEFAULT_ENABLE', 'true', 'This switch controls the SEO-G urls. When false the SEO-G is disabled and SEO-G URLs are not generated. However incoming hits of SEO-G URLs will still be converted to osC original ones to keep your store functional.', '24', '1', '2012-01-14 14:04:26', '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('236', 'SEO-G Extension', 'SEO_DEFAULT_EXTENSION', '.html', 'Default Extension to be appended with the SEO-G urls. Note your <b>.htaccess</b> file should be modified to match this setting.', '24', '2', '2012-09-26 16:50:06', '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('237', 'SEO-G Error Header', 'SEO_DEFAULT_ERROR_HEADER', '301', 'Default Header to be set by SEO-G when a page is not found. Used for inactive urls that match the SEO-G extension.', '24', '3', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('238', 'SEO-G Redirect', 'SEO_DEFAULT_ERROR_REDIRECT', '', 'Default Redirect Page to be loaded when the header is set to a temp or permanent redirect (Like 301).', '24', '4', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('239', 'SEO-G Strict Validation', 'SEO_STRICT_VALIDATION', 'false', 'When true all parameters passed must contain 2 arguments (eg:products_id=1) and invalid parameters will not propagate.', '24', '5', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('240', 'SEO-G Auto Names Builder', 'SEO_AUTO_BUILDER', 'true', 'When true, seo names for handled parameters, are generated automatically on the catalog end. When false the system relies exclusively on user-input via the G-Controller to generate names.', '24', '6', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('241', 'SEO-G Continuous Check', 'SEO_CONTINUOUS_CHECK', 'false', 'When <b>false</b>, SEO-G checks a passed osC URL against the stored ones, leading to a faster response. When <b>true</b> SEO-G will process every osC URL request and response will be slower but SEO-G name updates will be instant.', '24', '7', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('242', 'SEO-G Process Secure Pages', 'SEO_PROCESS_SSL', 'false', 'When <b>false</b>, SEO-G does not process secure pages. When <b>true</b> SEO-G will generate links for secure pages.', '24', '8', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('243', 'SEO-G Redirection Table', 'SEO_REDIRECT_TABLE', 'true', 'When <b>true</b> SEO-G uses the redirection table and/or auto-redirect methods. If a match is found, a redirect is issued according to the redirection type. <b>false</b> Redirections are disabled.', '24', '9', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('244', 'SEO-G Safe Mode', 'SEO_DEFAULT_SAFE_MODE', 'true', 'Controls if SEO-G urls are generated on non-handled parameters. When false SEO-G generates friendly urls regardless of parameters passed with the links. When true generates links for the handled parameters only. (ex: products_id, cpath etc).', '24', '10', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('245', 'SEO-G Parts Separator', 'SEO_DEFAULT_PARTS_SEPARATOR', '-', 'Default Separator used to join SEO-G url sections. Invalid characters/sequences can cause problems with URLs. Use <b>-</b> or <b>_</b> for best results.', '24', '15', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('246', 'SEO-G Words Separator', 'SEO_DEFAULT_WORDS_SEPARATOR', '-', 'Default Separator used to join words and replace invalid characters with processed strings. Invalid characters/sequences can cause problems with URLs. Use <b>-</b> or <b>_</b> for best results.', '24', '16', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('247', 'SEO-G Inner Separator', 'SEO_DEFAULT_INNER_SEPARATOR', '-', 'Default Separator used to join paths (Like Categories and Sub-Categories). Invalid characters/sequences can cause problems with URLs. Use <b>-</b> or <b>_</b> for best results.', '24', '17', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('248', 'SEO-G Minimum Words Length', 'SEO_DEFAULT_WORD_LENGTH', '1', 'Minimum length of each word to be filtered by SEO-G when names are generated. Words with less characters that the defined value are ignored', '24', '18', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('249', 'SEO-G Listings Split', 'SEO_PAGE_SPLIT', '100', 'Default Page Split for SEO-G Lists in osC Admin.', '24', '20', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('250', 'SEO-G Sitemap Filename', 'SEO_SITEMAP_FILENAME', 'gsitemap.xml', 'Default Filename for the Google Sitemap in XML format. This is generated from the reports page. Do not include the .gz extension (appended automatically).', '24', '28', '2010-03-29 15:06:11', '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('251', 'SEO-G XML Compress', 'SEO_DEFAULT_COMPRESS', 'true', 'Default compress behavior of the XML maps generated by SEO-G. When true XML map files are compressed.', '24', '29', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('252', 'SEO-G Use META-G Exclusion', 'SEO_METAG_EXCLUSION', 'true', 'Use the META-G Exclusion list when generating friendly URLs. When false, SEO-G uses its internal default mechanism to generate the link names. (Set to true only if META-G is installed)', '24', '31', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('253', 'SEO-G Use META-G Lexico', 'SEO_METAG_INCLUSION', 'false', 'Use the META-G Lexico when generating friendly URLs to adapt and prioritize words for the URL structure. When false, the META-G lexico is not processed. (Set to true only if META-G is installed)', '24', '32', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('254', 'SEO-G META-G Lexico Phrase Limit', 'SEO_METAG_INCLUSION_LIMIT', '1', 'The number of phrases to extract from the META-G Lexico and to replace each component of the generated friendly URL.', '24', '33', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('255', 'SEO-G Proximity Redirect and Cleanup', 'SEO_PROXIMITY_CLEANUP', 'true', 'When <b>true</b>, attempts to synchronize the reports table and typed-in links. Entries that match the name listed under the G-Controller are removed from the reports table and typed links are scanned for the nearest match before a redirect.', '24', '35', NULL, '2008-03-23 16:11:57', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('256', 'SEO-G Proximity Threshold', 'SEO_PROXIMITY_THRESHOLD', '3', 'The number of characters to allow when approximating a URL for redirection. Higher values result in better proximity but may void redirection in which case the default redirect page is used.', '24', '36', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('257', 'SEO-G Periodic Refresh', 'SEO_PERIODIC_REFRESH', '259200', 'Period to refresh urls in seconds. Updates the recorded urls periodically triggering the continuous check mode.', '24', '40', NULL, '2008-03-23 16:11:57', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('258', 'SEO-G Cache Switch', 'SEO_CACHE_ENABLE', 'true', 'Enable the SEO-G Cache. When Enabled the SEO-G stores and utilizes the SEO URL keys via a separate table leading to an increased performance.', '24', '44', NULL, '2008-03-23 16:11:58', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('259', 'SEO-G Cache Refresh', 'SEO_CACHE_REFRESH', '604800', 'Period to maintain the SEO-G cache contents in seconds.', '24', '45', NULL, '2008-03-23 16:11:58', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('261', 'Help Desk Page Listing', 'MAX_DISPLAY_ADMIN_HELP_DESK', '35', 'Maximum number of help desk entries to display per page', '33', '1', NULL, '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('262', 'Reviews Listing', 'MAX_DISPLAY_ADMIN_REVIEWS', '35', 'Maximum number of reviews entries to display per page', '33', '2', NULL, '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('263', 'Customer Page Listing', 'MAX_DISPLAY_ADMIN_CUSTOMERS', '35', 'Maximum number of customer entries to display per page', '33', '3', NULL, '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('264', 'Order Page Listing', 'MAX_DISPLAY_ADMIN_ORDERS', '35', 'Maximum number of order entries to display per page', '33', '4', NULL, '2007-01-11 12:16:33', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('265', 'Past Orders Life', 'GARBAGE_ORDERS_TIMEOUT', '15552000', 'Orders to be deleted when older than the predefined time in seconds', '15', '20', NULL, '2008-10-04 21:20:05', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('266', 'Idle Customers Life', 'GARBAGE_CUSTOMERS_TIMEOUT', '15552000', 'Customer accounts to be deleted when older than the predefined time from their last logon (in seconds)', '15', '21', NULL, '2008-10-04 21:20:05', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('267', 'Idle Customers Validation', 'GARBAGE_CUSTOMERS_VALID_TIMEOUT', '172800', 'Delete New Customer accounts when not validated and older than the predefined period (in seconds)', '15', '22', NULL, '2008-10-04 21:20:05', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('268', 'Customers Basket Auto-Cleanup', 'GARBAGE_CUSTOMERS_BASKET_TIMEOUT', '1209600', 'Delete Items from logged-on customers basket those added for longer than the specified period (in seconds)', '15', '23', NULL, '2008-10-04 21:20:05', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('270', 'Enable Check/Money Order Module', 'MODULE_PAYMENT_MONEYORDER_STATUS', 'False', 'Do you want to accept Check/Money Order payments?', '6', '1', NULL, '2008-10-04 21:24:45', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('271', 'Make Payable to:', 'MODULE_PAYMENT_MONEYORDER_PAYTO', 'Joe Doe', 'Who should payments be made payable to?', '6', '1', NULL, '2008-10-04 21:24:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('272', 'Sort order of display.', 'MODULE_PAYMENT_MONEYORDER_SORT_ORDER', '2', 'Sort order of display. Lowest is displayed first.', '6', '0', NULL, '2008-10-04 21:24:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('273', 'Payment Zone', 'MODULE_PAYMENT_MONEYORDER_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', NULL, '2008-10-04 21:24:45', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('274', 'Set Order Status', 'MODULE_PAYMENT_MONEYORDER_ORDER_STATUS_ID', '0', 'Set the status of orders made with this payment module to this value', '6', '0', NULL, '2008-10-04 21:24:45', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('275', 'Similar Image Width', 'SIMILAR_IMAGE_WIDTH', '175', 'Image Width in pixels to be used primarily for the similar products.', '4', '16', NULL, '2008-10-05 14:04:43', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('276', 'Similar Image Height', 'SIMILAR_IMAGE_HEIGHT', '120', 'Image Height in pixels to be used primarily for the similar products.', '4', '17', NULL, '2008-10-05 14:04:43', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('277', 'Whos Online Timeout', 'MAX_WHOS_ONLINE_TIME', '2880', 'Max time in seconds to maintain the whos online information', '15', '13', '2008-04-05 07:50:06', '2008-04-05 07:46:14', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('278', 'Shout Box Entries Life', 'GARBAGE_GSHOUT_TIMEOUT', '1209600', 'Shout Entries to be deleted when older than the predefined period in seconds', '15', '24', NULL, '2008-04-08 09:24:18', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('279', 'Paypal', 'INFO_EXCHANGE_PAYPAL', 'test', 'Last Paypal Order processed', '99', '6', '2008-11-01 16:40:31', '2008-04-29 07:41:19', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('280', 'Credit Card', 'INFO_EXCHANGE_CC', '123', 'Last CC Order processed', '99', '6', '2008-11-01 16:40:10', '2008-04-29 07:41:19', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('281', 'PHP Session Callback Handling', 'SESSION_PHP_HANDLING', 'false', 'When <b>true</b> the system relies on PHP to call the session handlers. When <b>false</b> the system calls the handlers and serializes the session.', '15', '1', '2012-01-14 21:03:06', '2008-06-12 07:46:41', NULL, 'tep_cfg_select_option(array(\'true\',\'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('287', 'Menu Text Pages Group', 'DEFAULT_MENU_TEXT_ZONE_ID', '11', 'The text zone of pages that show on the menu', '23', '31', '2011-11-11 16:35:57', '2008-10-12 16:05:45', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_text_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('288', 'Footer Text Pages Group', 'DEFAULT_FOOTER_TEXT_ZONE_ID', '8', 'The text zone of pages that show on the footer', '23', '32', '2010-03-10 19:27:46', '2008-10-12 16:05:45', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_text_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('289', 'Footer Right Text Pages Group', 'DEFAULT_FOOTER_TEXT_ZONE3_ID', '1', 'The text zone of pages that show on the footer right', '23', '33', NULL, '2008-10-12 16:05:45', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_text_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('290', 'SEO-G Path Cascade Level', 'SEO_PATH_CASCADE', '1', 'Depth level SEO-G will preserve with paths. For example with categories a value of 2 will preserve the 2 bottom levels from the categories hierarchy. Adjust it based on the path structure of your store.', '24', '50', NULL, '2008-12-03 17:53:37', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('291', 'SEO-G Path Reverse', 'SEO_PATH_REVERSE', 'true', 'When <b>true</b> reverses the path components of the generated URL. The deepest level of the path becomes the current and SEO-G works the path hierarchy towards the top path level.', '24', '51', NULL, '2008-12-03 17:53:37', NULL, 'tep_cfg_select_option(array(\'true\', \'false\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('292', 'Default Email Column Zone', 'DEFAULT_EMAIL_ABSTRACT_ZONE_ID', '4', 'The abstract zone to be used for the email templates.', '23', '36', '2009-01-12 14:21:02', '2009-01-12 14:17:40', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_product_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('293', 'Thumbnailer Postfix', 'FLY_THUMB_POSTFIX', '-thumb-', 'The string that is appended after the thumbnail image name products main image width', '4', '16', NULL, '2009-01-12 14:38:15', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('294', 'Thumbnailer Folder', 'FLY_THUMB_FOLDER', 'images/thumbs/', 'The folder to store the images. It is an offset from the web-root of the domain.', '4', '17', NULL, '2009-01-12 14:38:15', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('295', 'Default Front Page Products Zone', 'DEFAULT_FRONT_ABSTRACT_ZONE_ID', '6', 'The abstract zone to be used for the products collection with the home page', '23', '33', '2010-03-10 18:44:56', '2009-01-20 14:25:44', 'tep_get_abstract_zone_name', 'tep_cfg_pull_down_product_zones(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('296', 'SEO-G Update Timeout', 'SEO_UPDATE_TIMOUT', '2013-11-03 16:46:45', 'Read-Only Timout for Updates - <b>Do not Modify</b>', '24', '52', NULL, '2009-01-26 05:42:18', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('297', 'Attachments Folder', 'HELPDESK_ATTACHMENTS_FOLDER', 'attachments/', 'Folder used to store attachments from incoming emails.', '912', '32', NULL, '2009-06-20 12:31:44', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('298', 'Default Front Page', 'DEFAULT_FRONT_PAGE_ID', '6', 'Default abstract zone for the home page', '23', '10', '2011-08-01 21:39:35', '2010-03-09 16:53:04', 'tep_get_gtext_title', 'tep_cfg_pull_down_gtext_entries(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('305', 'Contact us text', 'GTEXT_CONTACT_ID', '1', 'Default text entry for the contact us page', '23', '36', NULL, '2010-03-29 14:59:04', 'tep_get_gtext_title', 'tep_cfg_pull_down_gtext_entries(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('321', 'Enable Flat Shipping', 'MODULE_SHIPPING_FLAT_STATUS', 'True', 'Do you want to offer flat rate shipping?', '6', '0', NULL, '2011-08-01 08:41:08', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('322', 'Shipping Cost', 'MODULE_SHIPPING_FLAT_COST', '0.00', 'The shipping cost for all orders using this shipping method.', '6', '0', NULL, '2011-08-01 08:41:08', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('323', 'Tax Class', 'MODULE_SHIPPING_FLAT_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '0', NULL, '2011-08-01 08:41:08', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('324', 'Shipping Zone', 'MODULE_SHIPPING_FLAT_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '0', NULL, '2011-08-01 08:41:08', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('325', 'Sort Order', 'MODULE_SHIPPING_FLAT_SORT_ORDER', '1', 'Sort order of display.', '6', '0', NULL, '2011-08-01 08:41:08', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('326', 'Enable PayPal IPN Module', 'MODULE_PAYMENT_PAYPAL_IPN_STATUS', 'True', 'Do you want to accept PayPal IPN payments?', '6', '1', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('327', 'Gateway Server', 'MODULE_PAYMENT_PAYPAL_IPN_GATEWAY_SERVER', 'Live', 'Use the testing (sandbox) or live gateway server for transactions?', '6', '2', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'Testing\',\'Live\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('328', 'Sort order of display.', 'MODULE_PAYMENT_PAYPAL_IPN_SORT_ORDER', '4', 'Sort order of display. Lowest is displayed first.', '6', '3', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('329', 'Force shipping address?', 'MODULE_PAYMENT_PAYPAL_IPN_SHIPPING', 'False', 'If TRUE the address details for the PayPal Seller Protection Policy are sent but customers without a PayPal account must re-enter their details. If set to FALSE order is not eligible for Seller Protection but customers without acount will have their addre', '6', '4', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('330', 'E-Mail Address', 'MODULE_PAYMENT_PAYPAL_IPN_ID', 'paypal@maxabid.co.uk', 'The e-mail address to use for the PayPal IPN service', '6', '5', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('331', 'Transaction Currency', 'MODULE_PAYMENT_PAYPAL_IPN_CURRENCY', 'Only GBP', 'The currency to use for transactions', '6', '10', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'Selected Currency\',\'Only AUD\',\'Only CAD\',\'Only CHF\',\'Only CZK\',\'Only DKK\',\'Only EUR\',\'Only GBP\',\'Only HKD\',\'Only HUF\',\'Only JPY\',\'Only NOK\',\'Only NZD\',\'Only PLN\',\'Only SEK\',\'Only SGD\',\'Only USD\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('332', 'Payment Zone', 'MODULE_PAYMENT_PAYPAL_IPN_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '11', NULL, '2011-08-01 08:42:13', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('333', 'Set Preparing Order Status', 'MODULE_PAYMENT_PAYPAL_IPN_PREPARE_ORDER_STATUS_ID', '6', 'Set the status of prepared orders made with this payment module to this value', '6', '12', NULL, '2011-08-01 08:42:13', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('334', 'Set PayPal Acknowledged Order Status', 'MODULE_PAYMENT_PAYPAL_IPN_ORDER_STATUS_ID', '6', 'Set the status of orders made with this payment module to this value', '6', '13', NULL, '2011-08-01 08:42:13', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('335', 'Set PayPal Completed Order Status', 'MODULE_PAYMENT_PAYPAL_IPN_COMP_ORDER_STATUS_ID', '5', 'Set the status of orders which are confirmed as paid (completed) to this value', '6', '13', NULL, '2011-08-01 08:42:13', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('336', 'Transaction Type', 'MODULE_PAYMENT_PAYPAL_IPN_TRANSACTION_TYPE', 'Aggregate', 'Send individual items to PayPal or aggregate all as one total item?', '6', '14', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'Per Item\',\'Aggregate\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('337', 'Move tax to total amount', 'MOVE_TAX_TO_TOTAL_AMOUNT', 'True', 'Do you want to move the tax to the total amount? If true PayPal will allways show the total amount including tax. (needs Aggregate instead of Per Item to function)', '6', '15', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('338', 'Page Style', 'MODULE_PAYMENT_PAYPAL_IPN_PAGE_STYLE', '', 'The page style to use for the transaction procedure (defined at your PayPal Profile page)', '6', '20', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('339', 'Debug E-Mail Address', 'MODULE_PAYMENT_PAYPAL_IPN_DEBUG_EMAIL', 'paypal@maxabid.co.uk', 'All parameters of an Invalid IPN notification will be sent to this email address if one is entered.', '6', '21', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('340', 'Enable Encrypted Web Payments', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_STATUS', 'False', 'Do you want to enable Encrypted Web Payments?', '6', '30', NULL, '2011-08-01 08:42:13', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('341', 'Your Private Key', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_PRIVATE_KEY', '', 'The location of your Private Key to use for signing the data. (*.pem)', '6', '31', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('342', 'Your Public Certificate', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_PUBLIC_KEY', '/var/sites/m/maxabid.co.uk/public_html/ext/modules/payment/paypal_cert_pem', 'The location of your Public Certificate to use for signing the data. (*.pem)', '6', '32', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('343', 'PayPals Public Certificate', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_PAYPAL_KEY', '', 'The location of the PayPal Public Certificate for encrypting the data.', '6', '33', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('344', 'Your PayPal Public Certificate ID', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_CERT_ID', '', 'The Certificate ID to use from your PayPal Encrypted Payment Settings Profile.', '6', '34', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('345', 'Working Directory', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_WORKING_DIRECTORY', '/tmp/', 'The working directory to use for temporary files. (trailing slash needed)', '6', '35', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('346', 'OpenSSL Location', 'MODULE_PAYMENT_PAYPAL_IPN_EWP_OPENSSL', '/usr/bin/openssl', 'The location of the openssl binary file.', '6', '36', NULL, '2011-08-01 08:42:13', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('347', 'Enable PayPal Direct', 'MODULE_PAYMENT_PAYPAL_DIRECT_STATUS', 'True', 'Do you want to accept PayPal Direct payments?', '6', '1', NULL, '2011-09-27 14:59:39', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('348', 'API Username', 'MODULE_PAYMENT_PAYPAL_DIRECT_API_USERNAME', 'jedi4london-facilitator_api1.hotmail.com', 'The username to use for the PayPal API service.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('349', 'API Password', 'MODULE_PAYMENT_PAYPAL_DIRECT_API_PASSWORD', '1381006244', 'The password to use for the PayPal API service.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('350', 'API Signature', 'MODULE_PAYMENT_PAYPAL_DIRECT_API_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AN1Eqa6AyYey8Yvr1pOom0oDnJ.D', 'The signature to use for the PayPal API service.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('351', 'Transaction Server', 'MODULE_PAYMENT_PAYPAL_DIRECT_TRANSACTION_SERVER', 'Sandbox', 'Use the live or testing (sandbox) gateway server to process transactions?', '6', '0', NULL, '2011-09-27 14:59:39', NULL, 'tep_cfg_select_option(array(\'Live\', \'Sandbox\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('352', 'Transaction Method', 'MODULE_PAYMENT_PAYPAL_DIRECT_TRANSACTION_METHOD', 'Authorization', 'The processing method to use for each transaction.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, 'tep_cfg_select_option(array(\'Authorization\', \'Sale\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('353', 'Card Acceptance Page', 'MODULE_PAYMENT_PAYPAL_DIRECT_CARD_INPUT_PAGE', 'Payment', 'The location to accept card information. Either on the Checkout Confirmation page or the Checkout Payment page.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, 'tep_cfg_select_option(array(\'Confirmation\', \'Payment\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('354', 'Payment Zone', 'MODULE_PAYMENT_PAYPAL_DIRECT_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', NULL, '2011-09-27 14:59:39', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('355', 'Sort order of display.', 'MODULE_PAYMENT_PAYPAL_DIRECT_SORT_ORDER', '3', 'Sort order of display. Lowest is displayed first.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('356', 'Set Order Status', 'MODULE_PAYMENT_PAYPAL_DIRECT_ORDER_STATUS_ID', '5', 'Set the status of orders made with this payment module to this value.', '6', '0', NULL, '2011-09-27 14:59:39', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('357', 'cURL Program Location', 'MODULE_PAYMENT_PAYPAL_DIRECT_CURL', '/usr/bin/curl', 'The location to the cURL program application.', '6', '0', NULL, '2011-09-27 14:59:39', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('358', 'SMS Mode', 'SMS_TEXT_MODE', 'live', 'Default SMS Mode for sending messages', '30', '1', '2013-11-01 20:55:54', '2012-02-27 11:13:11', NULL, 'tep_cfg_select_option(array(\'test\', \'live\'),');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('359', 'SMS Username', 'SMS_TEXT_USERNAME', '46777', 'The account username provided by Text Marketer', '30', '2', '2013-11-01 20:42:09', '2012-02-27 11:13:11', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('360', 'SMS Password', 'SMS_TEXT_PASSWORD', '49969', 'The account password provided by Text Marketer', '30', '3', '2013-11-01 20:42:20', '2012-02-27 11:13:11', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('361', 'SMS Originator', 'SMS_TEXT_STORE_NUMBER', 'Maxabid', 'The originator number that sends the SMS messages', '30', '4', '2012-04-17 19:51:05', '2012-02-27 11:13:11', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('362', 'SMS E-Mail', 'SMS_TEXT_STORE_EMAIL', 'sms@maxabid.co.uk', 'The store e-mail to handle the SMS messages if applicable', '30', '5', NULL, '2012-02-27 11:13:11', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('363', 'Enable Item Shipping', 'MODULE_SHIPPING_ITEM_STATUS', 'True', 'Do you want to offer per item rate shipping?', '6', '0', NULL, '2012-04-17 16:03:25', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('364', 'Shipping Cost', 'MODULE_SHIPPING_ITEM_COST', '0.01', 'The shipping cost will be multiplied by the number of items in an order that uses this shipping method.', '6', '0', NULL, '2012-04-17 16:03:25', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('365', 'Handling Fee', 'MODULE_SHIPPING_ITEM_HANDLING', '0', 'Handling fee for this shipping method.', '6', '0', NULL, '2012-04-17 16:03:25', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('366', 'Tax Class', 'MODULE_SHIPPING_ITEM_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '0', NULL, '2012-04-17 16:03:25', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('367', 'Shipping Zone', 'MODULE_SHIPPING_ITEM_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '0', NULL, '2012-04-17 16:03:25', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('368', 'Sort Order', 'MODULE_SHIPPING_ITEM_SORT_ORDER', '0', 'Sort order of display.', '6', '0', NULL, '2012-04-17 16:03:25', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('369', 'Footer Page', 'GTEXT_FOOTER_INFO_ID', '35', 'Default text entry for the footer', '23', '52', '2012-05-04 11:52:52', '2012-05-04 11:51:36', 'tep_get_gtext_title', 'tep_cfg_pull_down_gtext_entries(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('370', 'Enable Cash On Delivery Module', 'MODULE_PAYMENT_COD_STATUS', 'True', 'Do you want to accept Cash On Delevery payments?', '6', '1', NULL, '2012-09-26 17:05:28', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('371', 'Payment Zone', 'MODULE_PAYMENT_COD_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', NULL, '2012-09-26 17:05:28', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('372', 'Sort order of display.', 'MODULE_PAYMENT_COD_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', NULL, '2012-09-26 17:05:28', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('373', 'Set Order Status', 'MODULE_PAYMENT_COD_ORDER_STATUS_ID', '0', 'Set the status of orders made with this payment module to this value', '6', '0', NULL, '2012-09-26 17:05:28', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('374', 'Default Auto-Expire Timer for Auctions', 'AUCTION_AUTO_TIMER', '28', 'Default value to automatically expire auctions after the defined days', '26', '2', NULL, '2013-04-22 20:33:40', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('391', 'Enable PayPal Express Checkout', 'MODULE_PAYMENT_PAYPAL_EXPRESS_STATUS', 'False', 'Do you want to accept PayPal Express Checkout payments?', '6', '1', NULL, '2013-10-05 21:14:45', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('392', 'API Username', 'MODULE_PAYMENT_PAYPAL_EXPRESS_API_USERNAME', 'support_api1.maxabid.com', 'The username to use for the PayPal API service', '6', '0', NULL, '2013-10-05 21:14:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('393', 'API Password', 'MODULE_PAYMENT_PAYPAL_EXPRESS_API_PASSWORD', 'UVVV3QT8E7Q773PL', 'The password to use for the PayPal API service', '6', '0', NULL, '2013-10-05 21:14:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('394', 'API Signature', 'MODULE_PAYMENT_PAYPAL_EXPRESS_API_SIGNATURE', 'A--8MSCLabuvN8L.-MHjxC9uypBtAkI7IOQ0pWNF1PN9ZURea.s9-mZD', 'The signature to use for the PayPal API service', '6', '0', NULL, '2013-10-05 21:14:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('395', 'Transaction Server', 'MODULE_PAYMENT_PAYPAL_EXPRESS_TRANSACTION_SERVER', 'Live', 'Use the live or testing (sandbox) gateway server to process transactions?', '6', '0', NULL, '2013-10-05 21:14:45', NULL, 'tep_cfg_select_option(array(\'Live\', \'Sandbox\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('396', 'Transaction Method', 'MODULE_PAYMENT_PAYPAL_EXPRESS_TRANSACTION_METHOD', 'Sale', 'The processing method to use for each transaction', '6', '0', NULL, '2013-10-05 21:14:45', NULL, 'tep_cfg_select_option(array(\'Authorization\', \'Sale\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('397', 'Payment Zone', 'MODULE_PAYMENT_PAYPAL_EXPRESS_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone', '6', '2', NULL, '2013-10-05 21:14:45', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('398', 'Sort order of display', 'MODULE_PAYMENT_PAYPAL_EXPRESS_SORT_ORDER', '5', 'Sort order of display. Lowest is displayed first', '6', '0', NULL, '2013-10-05 21:14:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('399', 'Set Order Status', 'MODULE_PAYMENT_PAYPAL_EXPRESS_ORDER_STATUS_ID', '5', 'Set the status of orders made with this payment module to this value', '6', '0', NULL, '2013-10-05 21:14:45', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('400', 'cURL Program Location', 'MODULE_PAYMENT_PAYPAL_EXPRESS_CURL', '/usr/bin/curl', 'The location to the cURL program application', '6', '0', NULL, '2013-10-05 21:14:45', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('401', 'Enable PayPal Module', 'MODULE_PAYMENT_PAYPAL_STATUS', 'True', 'Do you want to accept PayPal payments?', '6', '3', NULL, '2013-10-06 22:19:18', NULL, 'tep_cfg_select_option(array(\'True\', \'False\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('402', 'E-Mail Address', 'MODULE_PAYMENT_PAYPAL_ID', 'paypal@maxabid.co.uk', 'The e-mail address to use for the PayPal service', '6', '4', NULL, '2013-10-06 22:19:18', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('403', 'Transaction Currency', 'MODULE_PAYMENT_PAYPAL_CURRENCY', 'Only GBP', 'The currency to use for credit card transactions', '6', '6', NULL, '2013-10-06 22:19:18', NULL, 'tep_cfg_select_option(array(\'Selected Currency\',\'Only USD\',\'Only CAD\',\'Only EUR\',\'Only GBP\',\'Only JPY\'), ');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('404', 'Sort order of display.', 'MODULE_PAYMENT_PAYPAL_SORT_ORDER', '6', 'Sort order of display. Lowest is displayed first.', '6', '0', NULL, '2013-10-06 22:19:18', NULL, NULL);
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('405', 'Payment Zone', 'MODULE_PAYMENT_PAYPAL_ZONE', '0', 'If a zone is selected, only enable this payment method for that zone.', '6', '2', NULL, '2013-10-06 22:19:18', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(');
insert into configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) values ('406', 'Set Order Status', 'MODULE_PAYMENT_PAYPAL_ORDER_STATUS_ID', '0', 'Set the status of orders made with this payment module to this value', '6', '0', NULL, '2013-10-06 22:19:18', 'tep_get_order_status_name', 'tep_cfg_pull_down_order_statuses(');
drop table if exists configuration_group;
create table configuration_group (
  configuration_group_id int(11) not null auto_increment,
  configuration_group_title varchar(64) not null ,
  configuration_group_description varchar(255) not null ,
  sort_order int(5) ,
  visible int(1) default '1' ,
  PRIMARY KEY (configuration_group_id)
);

insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('1', 'My Store', 'General information about my store', '1', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('2', 'Minimum Values', 'The minimum values for functions / data', '2', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('3', 'Maximum Values', 'The maximum values for functions / data', '3', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('4', 'Images', 'Image parameters', '4', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('5', 'Customer Details', 'Customer account configuration', '5', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('6', 'Module Options', 'Hidden from configuration', '6', '0');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('7', 'Shipping/Packaging', 'Shipping options available at my store', '7', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('8', 'Product Listing', 'Product Listing    configuration options', '8', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('9', 'Stock', 'Stock configuration options', '9', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('10', 'Logging', 'Logging configuration options', '10', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('11', 'Cache', 'Caching configuration options', '11', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('12', 'E-Mail Options', 'General setting for E-Mail transport and HTML E-Mails', '12', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('13', 'Download', 'Downloadable products options', '13', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('14', 'GZip Compression', 'GZip compression options', '14', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('15', 'Sessions', 'Session options', '15', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('16', 'Store Layout', 'Layout Controls', '16', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('20', 'RMA', 'Configure RMA', '16', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('30', 'SMS Configuration', 'The configuration setting sending SMS messages with the Text Marketer', '30', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('912', 'Helpdesk', 'Helpdesk Configuration Group', '20', '1');
insert into configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) values ('913', 'Admin Values', 'Configuration Values for the administrator', '31', '1');
drop table if exists countries;
create table countries (
  countries_id int(11) not null auto_increment,
  countries_name varchar(64) not null ,
  countries_iso_code_2 char(2) not null ,
  countries_iso_code_3 char(3) not null ,
  address_format_id int(11) not null ,
  status_id tinyint(1) default '1' not null ,
  pay_status_id tinyint(1) default '1' not null ,
  ship_status_id tinyint(1) default '1' not null ,
  sort_id int(3) default '999' not null ,
  PRIMARY KEY (countries_id),
  KEY IDX_COUNTRIES_NAME (countries_name)
);

insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('1', 'Afghanistan', 'AF', 'AFG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('2', 'Albania', 'AL', 'ALB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('3', 'Algeria', 'DZ', 'DZA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('4', 'American Samoa', 'AS', 'ASM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('5', 'Andorra', 'AD', 'AND', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('6', 'Angola', 'AO', 'AGO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('7', 'Anguilla', 'AI', 'AIA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('8', 'Antarctica', 'AQ', 'ATA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('9', 'Antigua and Barbuda', 'AG', 'ATG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('10', 'Argentina', 'AR', 'ARG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('11', 'Armenia', 'AM', 'ARM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('12', 'Aruba', 'AW', 'ABW', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('13', 'Australia', 'AU', 'AUS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('14', 'Austria', 'AT', 'AUT', '5', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('15', 'Azerbaijan', 'AZ', 'AZE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('16', 'Bahamas', 'BS', 'BHS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('17', 'Bahrain', 'BH', 'BHR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('18', 'Bangladesh', 'BD', 'BGD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('19', 'Barbados', 'BB', 'BRB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('20', 'Belarus', 'BY', 'BLR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('21', 'Belgium', 'BE', 'BEL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('22', 'Belize', 'BZ', 'BLZ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('23', 'Benin', 'BJ', 'BEN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('24', 'Bermuda', 'BM', 'BMU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('25', 'Bhutan', 'BT', 'BTN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('26', 'Bolivia', 'BO', 'BOL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('27', 'Bosnia and Herzegowina', 'BA', 'BIH', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('28', 'Botswana', 'BW', 'BWA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('29', 'Bouvet Island', 'BV', 'BVT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('30', 'Brazil', 'BR', 'BRA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('31', 'British Indian Ocean Territory', 'IO', 'IOT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('32', 'Brunei Darussalam', 'BN', 'BRN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('33', 'Bulgaria', 'BG', 'BGR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('34', 'Burkina Faso', 'BF', 'BFA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('35', 'Burundi', 'BI', 'BDI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('36', 'Cambodia', 'KH', 'KHM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('37', 'Cameroon', 'CM', 'CMR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('38', 'Canada', 'CA', 'CAN', '1', '0', '1', '0', '102');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('39', 'Cape Verde', 'CV', 'CPV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('40', 'Cayman Islands', 'KY', 'CYM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('41', 'Central African Republic', 'CF', 'CAF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('42', 'Chad', 'TD', 'TCD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('43', 'Chile', 'CL', 'CHL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('44', 'China', 'CN', 'CHN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('45', 'Christmas Island', 'CX', 'CXR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('46', 'Cocos (Keeling) Islands', 'CC', 'CCK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('47', 'Colombia', 'CO', 'COL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('48', 'Comoros', 'KM', 'COM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('49', 'Congo', 'CG', 'COG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('50', 'Cook Islands', 'CK', 'COK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('51', 'Costa Rica', 'CR', 'CRI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('52', 'Cote D\'Ivoire', 'CI', 'CIV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('53', 'Croatia', 'HR', 'HRV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('54', 'Cuba', 'CU', 'CUB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('55', 'Cyprus', 'CY', 'CYP', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('56', 'Czech Republic', 'CZ', 'CZE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('57', 'Denmark', 'DK', 'DNK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('58', 'Djibouti', 'DJ', 'DJI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('59', 'Dominica', 'DM', 'DMA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('60', 'Dominican Republic', 'DO', 'DOM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('61', 'East Timor', 'TP', 'TMP', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('62', 'Ecuador', 'EC', 'ECU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('63', 'Egypt', 'EG', 'EGY', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('64', 'El Salvador', 'SV', 'SLV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('65', 'Equatorial Guinea', 'GQ', 'GNQ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('66', 'Eritrea', 'ER', 'ERI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('67', 'Estonia', 'EE', 'EST', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('68', 'Ethiopia', 'ET', 'ETH', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('69', 'Falkland Islands (Malvinas)', 'FK', 'FLK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('70', 'Faroe Islands', 'FO', 'FRO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('71', 'Fiji', 'FJ', 'FJI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('72', 'Finland', 'FI', 'FIN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('73', 'France', 'FR', 'FRA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('74', 'France, Metropolitan', 'FX', 'FXX', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('75', 'French Guiana', 'GF', 'GUF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('76', 'French Polynesia', 'PF', 'PYF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('77', 'French Southern Territories', 'TF', 'ATF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('78', 'Gabon', 'GA', 'GAB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('79', 'Gambia', 'GM', 'GMB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('80', 'Georgia', 'GE', 'GEO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('81', 'Germany', 'DE', 'DEU', '5', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('82', 'Ghana', 'GH', 'GHA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('83', 'Gibraltar', 'GI', 'GIB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('84', 'Greece', 'GR', 'GRC', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('85', 'Greenland', 'GL', 'GRL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('86', 'Grenada', 'GD', 'GRD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('87', 'Guadeloupe', 'GP', 'GLP', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('88', 'Guam', 'GU', 'GUM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('89', 'Guatemala', 'GT', 'GTM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('90', 'Guinea', 'GN', 'GIN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('91', 'Guinea-bissau', 'GW', 'GNB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('92', 'Guyana', 'GY', 'GUY', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('93', 'Haiti', 'HT', 'HTI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('94', 'Heard and Mc Donald Islands', 'HM', 'HMD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('95', 'Honduras', 'HN', 'HND', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('96', 'Hong Kong', 'HK', 'HKG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('97', 'Hungary', 'HU', 'HUN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('98', 'Iceland', 'IS', 'ISL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('99', 'India', 'IN', 'IND', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('100', 'Indonesia', 'ID', 'IDN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('101', 'Iran (Islamic Republic of)', 'IR', 'IRN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('102', 'Iraq', 'IQ', 'IRQ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('103', 'Ireland', 'IE', 'IRL', '1', '0', '0', '0', '2');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('104', 'Israel', 'IL', 'ISR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('105', 'Italy', 'IT', 'ITA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('106', 'Jamaica', 'JM', 'JAM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('107', 'Japan', 'JP', 'JPN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('108', 'Jordan', 'JO', 'JOR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('109', 'Kazakhstan', 'KZ', 'KAZ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('110', 'Kenya', 'KE', 'KEN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('111', 'Kiribati', 'KI', 'KIR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('112', 'Korea, Democratic People\'s Republic of', 'KP', 'PRK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('113', 'Korea, Republic of', 'KR', 'KOR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('114', 'Kuwait', 'KW', 'KWT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('115', 'Kyrgyzstan', 'KG', 'KGZ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('116', 'Lao People\'s Democratic Republic', 'LA', 'LAO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('117', 'Latvia', 'LV', 'LVA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('118', 'Lebanon', 'LB', 'LBN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('119', 'Lesotho', 'LS', 'LSO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('120', 'Liberia', 'LR', 'LBR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('121', 'Libyan Arab Jamahiriya', 'LY', 'LBY', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('122', 'Liechtenstein', 'LI', 'LIE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('123', 'Lithuania', 'LT', 'LTU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('124', 'Luxembourg', 'LU', 'LUX', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('125', 'Macau', 'MO', 'MAC', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('126', 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('127', 'Madagascar', 'MG', 'MDG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('128', 'Malawi', 'MW', 'MWI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('129', 'Malaysia', 'MY', 'MYS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('130', 'Maldives', 'MV', 'MDV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('131', 'Mali', 'ML', 'MLI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('132', 'Malta', 'MT', 'MLT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('133', 'Marshall Islands', 'MH', 'MHL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('134', 'Martinique', 'MQ', 'MTQ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('135', 'Mauritania', 'MR', 'MRT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('136', 'Mauritius', 'MU', 'MUS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('137', 'Mayotte', 'YT', 'MYT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('138', 'Mexico', 'MX', 'MEX', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('139', 'Micronesia, Federated States of', 'FM', 'FSM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('140', 'Moldova, Republic of', 'MD', 'MDA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('141', 'Monaco', 'MC', 'MCO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('142', 'Mongolia', 'MN', 'MNG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('143', 'Montserrat', 'MS', 'MSR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('144', 'Morocco', 'MA', 'MAR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('145', 'Mozambique', 'MZ', 'MOZ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('146', 'Myanmar', 'MM', 'MMR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('147', 'Namibia', 'NA', 'NAM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('148', 'Nauru', 'NR', 'NRU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('149', 'Nepal', 'NP', 'NPL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('150', 'Netherlands', 'NL', 'NLD', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('151', 'Netherlands Antilles', 'AN', 'ANT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('152', 'New Caledonia', 'NC', 'NCL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('153', 'New Zealand', 'NZ', 'NZL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('154', 'Nicaragua', 'NI', 'NIC', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('155', 'Niger', 'NE', 'NER', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('156', 'Nigeria', 'NG', 'NGA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('157', 'Niue', 'NU', 'NIU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('158', 'Norfolk Island', 'NF', 'NFK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('159', 'Northern Mariana Islands', 'MP', 'MNP', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('160', 'Norway', 'NO', 'NOR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('161', 'Oman', 'OM', 'OMN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('162', 'Pakistan', 'PK', 'PAK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('163', 'Palau', 'PW', 'PLW', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('164', 'Panama', 'PA', 'PAN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('165', 'Papua New Guinea', 'PG', 'PNG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('166', 'Paraguay', 'PY', 'PRY', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('167', 'Peru', 'PE', 'PER', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('168', 'Philippines', 'PH', 'PHL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('169', 'Pitcairn', 'PN', 'PCN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('170', 'Poland', 'PL', 'POL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('171', 'Portugal', 'PT', 'PRT', '1', '0', '0', '0', '3');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('172', 'Puerto Rico', 'PR', 'PRI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('173', 'Qatar', 'QA', 'QAT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('174', 'Reunion', 'RE', 'REU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('175', 'Romania', 'RO', 'ROM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('176', 'Russian Federation', 'RU', 'RUS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('177', 'Rwanda', 'RW', 'RWA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('178', 'Saint Kitts and Nevis', 'KN', 'KNA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('179', 'Saint Lucia', 'LC', 'LCA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('180', 'Saint Vincent and the Grenadines', 'VC', 'VCT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('181', 'Samoa', 'WS', 'WSM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('182', 'San Marino', 'SM', 'SMR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('183', 'Sao Tome and Principe', 'ST', 'STP', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('184', 'Saudi Arabia', 'SA', 'SAU', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('185', 'Senegal', 'SN', 'SEN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('186', 'Seychelles', 'SC', 'SYC', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('187', 'Sierra Leone', 'SL', 'SLE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('188', 'Singapore', 'SG', 'SGP', '4', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('189', 'Slovakia (Slovak Republic)', 'SK', 'SVK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('190', 'Slovenia', 'SI', 'SVN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('191', 'Solomon Islands', 'SB', 'SLB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('192', 'Somalia', 'SO', 'SOM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('193', 'South Africa', 'ZA', 'ZAF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('194', 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('195', 'Spain', 'ES', 'ESP', '3', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('196', 'Sri Lanka', 'LK', 'LKA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('197', 'St. Helena', 'SH', 'SHN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('198', 'St. Pierre and Miquelon', 'PM', 'SPM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('199', 'Sudan', 'SD', 'SDN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('200', 'Suriname', 'SR', 'SUR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('201', 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('202', 'Swaziland', 'SZ', 'SWZ', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('203', 'Sweden', 'SE', 'SWE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('204', 'Switzerland', 'CH', 'CHE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('205', 'Syrian Arab Republic', 'SY', 'SYR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('206', 'Taiwan', 'TW', 'TWN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('207', 'Tajikistan', 'TJ', 'TJK', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('208', 'Tanzania, United Republic of', 'TZ', 'TZA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('209', 'Thailand', 'TH', 'THA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('210', 'Togo', 'TG', 'TGO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('211', 'Tokelau', 'TK', 'TKL', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('212', 'Tonga', 'TO', 'TON', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('213', 'Trinidad and Tobago', 'TT', 'TTO', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('214', 'Tunisia', 'TN', 'TUN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('215', 'Turkey', 'TR', 'TUR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('216', 'Turkmenistan', 'TM', 'TKM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('217', 'Turks and Caicos Islands', 'TC', 'TCA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('218', 'Tuvalu', 'TV', 'TUV', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('219', 'Uganda', 'UG', 'UGA', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('220', 'Ukraine', 'UA', 'UKR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('221', 'United Arab Emirates', 'AE', 'ARE', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('222', 'United Kingdom', 'GB', 'GBR', '1', '1', '1', '1', '100');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('223', 'United States', 'US', 'USA', '2', '0', '1', '1', '101');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('224', 'United States Minor Outlying Islands', 'UM', 'UMI', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('225', 'Uruguay', 'UY', 'URY', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('226', 'Uzbekistan', 'UZ', 'UZB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('227', 'Vanuatu', 'VU', 'VUT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('228', 'Vatican City State (Holy See)', 'VA', 'VAT', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('229', 'Venezuela', 'VE', 'VEN', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('230', 'Viet Nam', 'VN', 'VNM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('231', 'Virgin Islands (British)', 'VG', 'VGB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('232', 'Virgin Islands (U.S.)', 'VI', 'VIR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('233', 'Wallis and Futuna Islands', 'WF', 'WLF', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('234', 'Western Sahara', 'EH', 'ESH', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('235', 'Yemen', 'YE', 'YEM', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('236', 'Yugoslavia', 'YU', 'YUG', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('237', 'Zaire', 'ZR', 'ZAR', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('238', 'Zambia', 'ZM', 'ZMB', '1', '0', '0', '0', '999');
insert into countries (countries_id, countries_name, countries_iso_code_2, countries_iso_code_3, address_format_id, status_id, pay_status_id, ship_status_id, sort_id) values ('239', 'Zimbabwe', 'ZW', 'ZWE', '1', '0', '0', '0', '999');
drop table if exists coupons;
create table coupons (
  coupons_id int(11) not null auto_increment,
  coupons_type int(11) default '0' not null ,
  coupons_name varchar(64) not null ,
  coupons_message varchar(255) not null ,
  coupons_usage int(3) default '0' not null ,
  coupons_max_usage int(3) default '0' not null ,
  coupons_amount decimal(15,4) default '0.0000' not null ,
  last_customer_id int(11) default '0' not null ,
  date_start datetime ,
  date_end datetime ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (coupons_id),
  KEY idx_coupons_name (coupons_name),
  KEY idx_status_id (status_id),
  KEY idx_last_customer_id (last_customer_id)
);

insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('1', '15', 'Test Coupon', '', '50', '50', '20.0000', '0', '2012-04-19 00:00:00', '2012-04-25 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('2', '15', '123aA', 'Test Message for 123aA', '2', '2', '14.0000', '0', '2012-04-17 00:00:00', '0000-00-00 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('3', '15', 'mark1234', '', '1', '1', '1.0000', '0', '2012-04-23 00:00:00', '2012-04-30 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('4', '15', 'Mark1', '', '1', '1', '1.0000', '0', '2012-04-21 00:00:00', '2012-04-24 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('5', '15', 'abcde999', 'bb iuy', '1', '1', '77.0000', '0', '2012-12-17 01:12:00', '2012-12-18 00:17:27', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('6', '15', 'thanx444', '', '1', '1', '9.0000', '0', '2012-12-16 00:00:00', '2012-12-17 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('7', '15', 'help111', '', '1', '1', '7.0000', '0', '2012-12-16 00:00:00', '2012-12-17 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('8', '15', 'from9', 'nice', '1', '1', '11.0000', '0', '2012-12-16 00:00:00', '2012-12-17 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('9', '15', 'wassup', 'gg wake me up', '1', '1', '3.0000', '0', '2013-11-02 00:00:00', '2013-11-26 00:00:00', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('10', '15', 'latest news', 'checking it out', '1', '1', '15.0000', '0', '2013-11-05 03:08:00', '2013-11-25 00:45:52', '1');
insert into coupons (coupons_id, coupons_type, coupons_name, coupons_message, coupons_usage, coupons_max_usage, coupons_amount, last_customer_id, date_start, date_end, status_id) values ('11', '15', 'teenja panja', 'go for it yaara', '4', '4', '4.0000', '0', '2013-11-02 00:00:59', '2013-11-18 00:00:36', '1');
drop table if exists coupons_redeem;
create table coupons_redeem (
  coupons_id int(11) not null ,
  customers_id int(11) default '0' not null ,
  coupons_amount decimal(15,4) default '0.0000' not null ,
  date_added datetime ,
  KEY idx_coupons_id (coupons_id),
  KEY idx_customers_id (customers_id)
);

drop table if exists currencies;
create table currencies (
  currencies_id int(11) not null auto_increment,
  title varchar(32) not null ,
  code char(3) not null ,
  symbol_left varchar(12) ,
  symbol_right varchar(12) ,
  decimal_point char(1) ,
  thousands_point char(1) ,
  decimal_places char(1) ,
  value float(13,8) ,
  last_updated datetime ,
  PRIMARY KEY (currencies_id)
);

insert into currencies (currencies_id, title, code, symbol_left, symbol_right, decimal_point, thousands_point, decimal_places, value, last_updated) values ('1', 'US Dollar', 'USD', '$', '', '.', ',', '2', '1.00000000', '2007-09-21 11:22:56');
insert into currencies (currencies_id, title, code, symbol_left, symbol_right, decimal_point, thousands_point, decimal_places, value, last_updated) values ('2', 'Euro', 'EUR', '', 'EUR', '.', ',', '2', '1.10360003', '2007-09-21 11:22:56');
insert into currencies (currencies_id, title, code, symbol_left, symbol_right, decimal_point, thousands_point, decimal_places, value, last_updated) values ('4', 'British Pound', 'GBP', '£', '', '.', ',', '2', '1.00000000', NULL);
drop table if exists customers;
create table customers (
  customers_id int(11) not null auto_increment,
  customers_gender char(1) not null ,
  customers_firstname varchar(32) not null ,
  customers_lastname varchar(32) not null ,
  customers_nickname varchar(64) default 'hidden' not null ,
  customers_dob datetime default '0000-00-00 00:00:00' not null ,
  customers_email_address varchar(96) not null ,
  customers_default_address_id int(11) ,
  customers_telephone varchar(32) not null ,
  customers_fax varchar(32) ,
  customers_password varchar(40) not null ,
  customers_newsletter char(1) ,
  customers_status tinyint(1) default '1' not null ,
  customers_bids int(11) default '0' not null ,
  sticky_poll tinyint(1) default '0' not null ,
  PRIMARY KEY (customers_id)
);

insert into customers (customers_id, customers_gender, customers_firstname, customers_lastname, customers_nickname, customers_dob, customers_email_address, customers_default_address_id, customers_telephone, customers_fax, customers_password, customers_newsletter, customers_status, customers_bids, sticky_poll) values ('7', '', 'AA', 'YY', 'Totally One', '0000-00-00 00:00:00', 'aleem786@hotmail.co.uk', '9', '45345345345345', '447466171826', '5c343377a2437ad1f782935a7efca75d:61', '1', '1', '7365', '0');
insert into customers (customers_id, customers_gender, customers_firstname, customers_lastname, customers_nickname, customers_dob, customers_email_address, customers_default_address_id, customers_telephone, customers_fax, customers_password, customers_newsletter, customers_status, customers_bids, sticky_poll) values ('8', '', 'harry', 'houdini', 'Harry Houdini', '0000-00-00 00:00:00', 'harryhoudini69@gmail.com', '10', '02085341111', '', '5ed200a29fdea33684621120169479fd:f2', '1', '1', '3130', '0');
insert into customers (customers_id, customers_gender, customers_firstname, customers_lastname, customers_nickname, customers_dob, customers_email_address, customers_default_address_id, customers_telephone, customers_fax, customers_password, customers_newsletter, customers_status, customers_bids, sticky_poll) values ('11', '', 'Mark', 'Tester', 'marktester1', '0000-00-00 00:00:00', 'test@asymmetrics.com', '13', '1234567890', '', '4b764ad7d057d8890cb3514e6c88895b:f5', '', '1', '925', '1');
drop table if exists customers_basket;
create table customers_basket (
  customers_basket_id int(11) not null auto_increment,
  customers_id int(11) not null ,
  products_id tinytext not null ,
  customers_basket_quantity int(2) not null ,
  final_price decimal(15,4) ,
  customers_basket_date_added char(8) ,
  auctions_history_id int(11) default '0' not null ,
  PRIMARY KEY (customers_basket_id)
);

drop table if exists customers_basket_attributes;
create table customers_basket_attributes (
  customers_basket_attributes_id int(11) not null auto_increment,
  customers_id int(11) not null ,
  products_id tinytext not null ,
  products_options_id int(11) not null ,
  products_options_value_id int(11) not null ,
  PRIMARY KEY (customers_basket_attributes_id)
);

drop table if exists customers_basket_group_fields;
create table customers_basket_group_fields (
  customers_id int(11) default '0' not null ,
  products_id varchar(255) not null ,
  group_values varchar(255) not null ,
  PRIMARY KEY (customers_id, products_id, group_values)
);

drop table if exists customers_info;
create table customers_info (
  customers_info_id int(11) not null ,
  customers_info_date_of_last_logon datetime ,
  customers_info_number_of_logons int(5) ,
  customers_info_date_account_created datetime ,
  customers_info_date_account_last_modified datetime ,
  global_product_notifications int(1) default '0' ,
  PRIMARY KEY (customers_info_id)
);

insert into customers_info (customers_info_id, customers_info_date_of_last_logon, customers_info_number_of_logons, customers_info_date_account_created, customers_info_date_account_last_modified, global_product_notifications) values ('7', '2013-10-06 22:57:46', '141', '2011-09-15 23:15:05', '2013-11-01 21:06:07', '0');
insert into customers_info (customers_info_id, customers_info_date_of_last_logon, customers_info_number_of_logons, customers_info_date_account_created, customers_info_date_account_last_modified, global_product_notifications) values ('8', '2013-07-16 13:22:03', '21', '2011-09-15 23:21:14', '2013-11-01 20:51:13', '0');
insert into customers_info (customers_info_id, customers_info_date_of_last_logon, customers_info_number_of_logons, customers_info_date_account_created, customers_info_date_account_last_modified, global_product_notifications) values ('11', '2013-06-09 17:09:50', '7', '2013-04-22 21:00:26', '2013-05-05 21:22:14', '0');
drop table if exists email_templates;
create table email_templates (
  id smallint(6) not null auto_increment,
  grp varchar(50) not null ,
  title varchar(255) not null ,
  subject varchar(255) not null ,
  contents text not null ,
  updated datetime default '0000-00-00 00:00:00' not null ,
  PRIMARY KEY (id)
);

insert into email_templates (id, grp, title, subject, contents, updated) values ('1', 'return_request', 'Returns', '[STORE_NAME] Product Returns #[CUSTOMER_RMA]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"http://www.maxabid.co.uk/images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] thanks you once&nbsp;again for your business. If you have any question or concerns about your order please do not hesitate to <a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>. The current details of your return request are shown here:</p>
<table style=\"border: 2px solid #eeeeee; background-color: #eeffff; height: 1px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"619\">
<tbody>
<tr>
<td><strong>[ORDER_URL]</strong></td>
</tr>
<tr>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_RMA]</strong></span></td>
</tr>
</tbody>
</table>
<p>Order updates and product notifications can be configured by your account pages. The following details are currently recorded with this order:</p>
<strong>ORDER #[ORDER_ID]&nbsp;DETAILS:</strong>&nbsp;    
<table style=\"border: 2px solid #eeeeee; background-color: #ffffee; height: 82px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\">
<tbody>
<tr>
<td><strong>Payment Gateway:</strong></td>
<td><strong>[ORDER_PAYMENT]</strong></td>
</tr>
<tr>
<td><strong>Products Returned:</strong></td>
<td><strong>[ORDER_PRODUCTS]</strong></td>
</tr>
<tr>
<td><strong>Refund:</strong></td>
<td><strong>[ORDER_TOTALS]</strong></td>
</tr>
</tbody>
</table>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:31:01');
insert into email_templates (id, grp, title, subject, contents, updated) values ('2', 'new_account', 'New Account', 'Your New Account with [STORE_NAME]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Penny Auctions\"><img src=\"http://www.maxabid.co.uk/images/Maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auction items\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong><span style=\"font-size: small;\">Welcome to [STORE_NAME]</span></strong><span><span style=\"font-size: small;\"><span><span style=\"font-size: small;\"><strong> </strong></span></span></span></span></p>
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,&nbsp;</strong></p>
<p>Your account with [STORE_NAME] is now&nbsp;active and can be accessed using the following details:</p>
<table style=\"border: 2px solid #eeeeee; background-color: #ffffee; height: 28px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"613\">
<tbody>
<tr>
<td><strong>Login:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_EMAIL]</strong></span></td>
</tr>
<tr>
<td><strong>Password:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_PASSWORD]</strong></span></td>
</tr>
</tbody>
</table>
<p><strong><a href=\"https://www.maxabid.co.uk/login.php\" title=\"Maxabid Login\">Click here to login now.</a></strong>&nbsp;</p>
<p>Thank you for creating an account with [STORE_NAME].</p>
<p>[STORE_NAME] strives to provide it&rsquo;s customer&rsquo;s with great value auctions. [STORE_NAME] was created in 2010 in United Kingdom.</p>
<p><strong>USEFUL LINKS:</strong></p>
<table style=\"border: 2px solid #eeeeee; height: 58px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\">
<tbody>
<tr>
<td><a href=\"http://www.maxabid.co.uk/auctions.html\" title=\"Auction Items\">Auction Items</a>, <a href=\"http://www.maxabid.co.uk/auctions.html\" title=\"Auction Items\">Auction Items</a> <br /></td>
</tr>
<tr>
<td><a href=\"http://www.maxabid.co.uk/return-policy.html\" title=\"Maxabid Return Policy\">Return Policy</a> <br /></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact Maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/new-Maxabid.html\" title=\"Online catalog of Maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"Maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:35:46');
insert into email_templates (id, grp, title, subject, contents, updated) values ('3', 'password_forgotten', 'Password Request', 'New Password Request in [STORE_NAME]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"http://www.maxabid.co.uk/images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong><span style=\"font-size: small;\">Welcome to [STORE_NAME]</span></strong><span><span style=\"font-size: small;\"><span><span style=\"font-size: small;\"><strong> </strong></span></span></span></span></p>
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,&nbsp;</strong></p>
<p>Your password with [STORE_NAME] is updated as requested. Here are the new details to login to your account:</p>
<table style=\"border: 2px solid #eeeeee; background-color: #ffffee; height: 28px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"613\">
<tbody>
<tr>
<td><strong>Login:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_EMAIL]</strong></span></td>
</tr>
<tr>
<td><strong>Password:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_PASSWORD]</strong></span></td>
</tr>
</tbody>
</table>
<p><strong><a href=\"https://www.maxabid.co.uk/login.php\" title=\"Maxabid Login\">Click here to login now.</a></strong>&nbsp;</p>
<p>Thank you for creating an account with [STORE_NAME].</p>
<p>[STORE_NAME] strives to provide it&rsquo;s customer&rsquo;s with great value auctions. [STORE_NAME] was created in 2010 in United Kingdom.</p>
<p><strong>USEFUL LINKS:</strong></p>
<table style=\"border: 2px solid #eeeeee; height: 58px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\">
<tbody>
<tr>
<td><a href=\"http://www.maxabid.co.uk/auctions.html\" title=\"Auction Items\">Auction Items</a>, <a href=\"http://www.maxabid.co.uk/auctions.html\" title=\"Auction Items\">Auction Items</a> <br /></td>
</tr>
<tr>
<td><a href=\"http://www.maxabid.co.uk/return-policy.html\" title=\"Maxabid Return Policy\">Return Policy</a> <br /></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact Maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /> </span></span></span></td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/auctions.html\" title=\"Online catalog of Auctions\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:36:14');
insert into email_templates (id, grp, title, subject, contents, updated) values ('4', 'new_order', 'Order Process', 'ORDER #[ORDER_ID] from [STORE_NAME]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"http://www.maxabid.co.uk/images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] thanks you once&nbsp;again for your business. If you have any question or concerns about your order please do not hesitate to <a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>. The current&nbsp;order&nbsp;can also be accessed from your account pages using the following tracking url:</p>
<table style=\"border: 2px solid #eeeeee; height: 1px; background-color: #eeffff;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"619\">
<tbody>
<tr>
<td>Click here to view [ORDER_URL]</td>
</tr>
</tbody>
</table>
<p>Order updates and product notifications can be configured by your account pages. The following details are currently recorded with this order:</p>
<strong>ORDER #[ORDER_ID]&nbsp;DETAILS:</strong>&nbsp;  
<table style=\"border: 2px solid #eeeeee; height: 82px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\">
<tbody>
<tr>
<td><strong>Payment Gateway:</strong></td>
<td><span style=\"color: #006600;\"><strong>[ORDER_PAYMENT]</strong></span></td>
</tr>
<tr>
<td><strong>Products Ordered:</strong></td>
<td><span style=\"color: #000066;\"><strong>[ORDER_PRODUCTS]</strong></span></td>
</tr>
<tr>
<td><strong>Payment Details:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[ORDER_TOTALS]</strong></span></td>
</tr>
</tbody>
</table>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:33:04');
insert into email_templates (id, grp, title, subject, contents, updated) values ('5', 'order_change', 'Order Update', '[STORE_NAME] Order # [ORDER_ID] status is now [ORDER_STATUS]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"http://www.maxabid.co.uk/images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear [CUSTOMER_NAME],&nbsp;</strong></p>
<p><strong>Order # [ORDER_ID] status is now <span style=\"color: #ff0000;\">[ORDER_STATUS]</span></strong></p>
<p><span style=\"color: #0000ff;\"><strong>[ORDER_COMMENTS]&nbsp;</strong></span></p>
<p>[STORE_NAME] thanks you once&nbsp;again for your business. If you have any question or concerns about your order please do not hesitate to <a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>. The current&nbsp;order&nbsp;can also be accessed from your account pages using the following tracking url:</p>
<table style=\"border: 2px solid #eeeeee; height: 1px; background-color: #eeffff;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"619\">
<tbody>
<tr>
<td>Click here to view [ORDER_URL]</td>
</tr>
</tbody>
</table>
<p>Order updates and product notifications can be configured by your account pages. The following details are currently recorded with this order:</p>
<strong>ORDER #[ORDER_ID]&nbsp;DETAILS:</strong>&nbsp;  
<table style=\"border: 2px solid #eeeeee; height: 82px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\">
<tbody>
<tr>
<td><strong>Payment Gateway:</strong></td>
<td><span style=\"color: #006600;\"><strong>[ORDER_PAYMENT]</strong></span></td>
</tr>
<tr>
<td><strong>Products Ordered:</strong></td>
<td><span style=\"color: #000066;\"><strong>[ORDER_PRODUCTS]</strong></span></td>
</tr>
<tr>
<td><strong>Payment Details:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[ORDER_TOTALS]</strong></span></td>
</tr>
</tbody>
</table>
<p><strong>DELIVERY/PAYMENT ADDRESS DETAILS:</strong>&nbsp;</p>
<table style=\"border: 2px solid #eeeeee; height: 39px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"618\">
<tbody>
<tr>
<td><strong>Ship To:</strong></td>
<td><strong>[ORDER_SHIPTO]</strong></td>
</tr>
<tr>
<td><strong>Bill To:</strong></td>
<td><strong>[ORDER_BILLTO]</strong></td>
</tr>
</tbody>
</table>
<span><strong><span style=\"font-size: x-small;\"><strong><span style=\"font-size: x-small;\"><span style=\"font-size: x-small;\"><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\">&nbsp;</span></span></span></span></strong></span></strong></span>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:35:03');
insert into email_templates (id, grp, title, subject, contents, updated) values ('6', 'create_account_admin', 'Admin Account', '[STORE_NAME] Account Details', '<table style=\"width: 716px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"918\" height=\"869\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px; width: 912px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"http://www.maxabid.co.uk/images/Maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auction items\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong><span style=\"font-size: small;\">Welcome to [STORE_NAME]</span></strong><span><span style=\"font-size: small;\"><span><span style=\"font-size: small;\"><strong> </strong></span></span></span></span></p>
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,&nbsp;</strong></p>
<p>Your account with [STORE_NAME] is now&nbsp;active and can be accessed using the following details:</p>
<table style=\"border: 2px solid #eeeeee; background-color: #ffffee; height: 28px; width: 613px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\">
<tbody>
<tr>
<td><strong>Login:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_EMAIL]</strong></span></td>
</tr>
<tr>
<td><strong>Password:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_PASSWORD]</strong></span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><strong>Account Details:</strong></p>
<p>&nbsp;</p>
<table style=\"border: 2px solid #eeeeee; background-color: #efffee; height: 32px; width: 619px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\">
<tbody>
<tr>
<td><strong>First Name:</strong></td>
<td><strong>[CUSTOMER_FIRSTNAME]</strong></td>
</tr>
<tr>
<td><strong>Last Name:</strong></td>
<td><strong>[CUSTOMER_LASTNAME]</strong></td>
</tr>
<tr>
<td><strong>Email:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[CUSTOMER_EMAIL] </strong></span><br /></td>
</tr>
<tr>
<td><strong>Address Line-1:</strong></td>
<td><strong>[CUSTOMER_ADDRESS_LINE1]&nbsp;</strong></td>
</tr>
<tr>
<td><strong>Address Line-2:&nbsp;</strong></td>
<td><strong>[CUSTOMER_ADDRESS_LINE2]&nbsp;</strong></td>
</tr>
<tr>
<td><strong>City:&nbsp;</strong></td>
<td><strong>[CUSTOMER_CITY]&nbsp;</strong></td>
</tr>
<tr>
<td><strong>State:</strong>&nbsp;</td>
<td><strong>[CUSTOMER_STATE]&nbsp;</strong></td>
</tr>
<tr>
<td><strong>Country:</strong>&nbsp;</td>
<td><strong>[CUSTOMER_COUNTRY] </strong><br /></td>
</tr>
<tr>
<td><strong>Zip Code:</strong>&nbsp;</td>
<td><strong>[CUSTOMER_ZIPCODE]&nbsp;</strong></td>
</tr>
<tr>
<td><strong>Telephone:</strong>&nbsp;</td>
<td><strong>[CUSTOMER_TELEPHONE]&nbsp;</strong></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><strong><a href=\"https://www.maxabid.co.uk/login.php\" title=\"Maxabid Login\">Click here to login now.</a></strong></p>
<p>Please review the account details. Corrections can be made from your account pages.</p>
<span><span style=\"font-size: small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">&nbsp;</span></span>
<p>[STORE_NAME] strives to provide it&rsquo;s customer&rsquo;s with penny auctions with very good pricing and excellent customer service, guide and support. [STORE_NAME] uses special services and methods, to ensure that the check out process is secure and your information remains confidential at all times..</p>
<p><span><strong>USEFUL LINKS:</strong> </span></p>
<table style=\"border: 2px solid #eeeeee; height: 58px; background-color: #ffffee; width: 611px;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\">
<tbody>
<tr>
<td><a href=\"http://www.maxabid.co.uk/return-policy.html\" title=\"Quality Products\">Auction Items</a>, <a href=\"http://www.maxabid.co.uk/auction-bids.html\" title=\"Auction Items\">Auction Items</a> <br /></td>
</tr>
<tr>
<td><a href=\"http://www.maxabid.co.uk/return-policy.html\" title=\"Maxabid Return Policy\">Return Policy</a> <br /></td>
</tr>
</tbody>
</table>
<p><span>Once again thank you for being a customer with [STORE_NAME]. </span></p>
<span><span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"http://www.maxabid.co.uk/contact-us.html\" title=\"Contact Maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><br /><span style=\"font-family: verdana,geneva; font-size: x-small;\">&nbsp;</span></span></span></span></span>
<p><span>&nbsp;</span></p>
</td>
<td style=\"width: 250px;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; width: 249px;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"http://www.maxabid.co.uk/new-Maxabid.html\" title=\"Online catalog of Maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"font-family: verdana,geneva; color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/terms-of-use.html\" title=\"Maxabid terms of use\"><span style=\"font-family: verdana,geneva; color: #030303; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"font-family: verdana,geneva; color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"http://www.maxabid.co.uk/contact-us.html\"><span style=\"font-family: verdana,geneva; color: #030303; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"http://www.maxabid.co.uk/faq.html\"><span style=\"font-family: verdana,geneva; color: #030303; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-07-26 18:35:16');
insert into email_templates (id, grp, title, subject, contents, updated) values ('7', 'auction_reminder', 'Auction Reminder', 'Auction Reminder', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"../\" title=\"Maxabid\"><img src=\"../images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] reminds you that you have unclaimed items in your cart from auctions you won. If you have any question or concerns about your order please do not hesitate to <a href=\"../contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>.</p>
<p>Auction details can be see from your shopping cart. Login to your account to claim your prizes at the following URL:</p>
<p>[ACCOUNT_URL]</p>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"../contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"../new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"../faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"../\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-11-16 14:03:51');
insert into email_templates (id, grp, title, subject, contents, updated) values ('8', 'auction_won', 'Auction Won', 'You won auction [AUCTION_NAME]', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] thanks you once&nbsp;again for your business. If you have any question or concerns about your order please do not hesitate to <a href=\"contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>.</p>
<p>Auction details can be see from your shopping cart. The following details are currently recorded with this auction:</p>
<strong>AUCTION [AUCTION_NAME] DETAILS:</strong>&nbsp;                
<table style=\"border: 2px solid #eeeeee; height: 82px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\" height=\"10\">
<tbody>
<tr>
<td><strong>Product Won:</strong></td>
<td><span style=\"color: #000066;\"><strong>[AUCTION_PRODUCTS]</strong></span></td>
</tr>
</tbody>
</table>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2012-04-14 20:45:30');
insert into email_templates (id, grp, title, subject, contents, updated) values ('9', 'auction_removed', 'Auction Removed', 'Auction Removed', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"../\" title=\"Maxabid\"><img src=\"../images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] has not heard from you since you won auctions. As a result we have removed the auction items from your cart If you have any question or concerns about your order please do not hesitate to <a href=\"../contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>.</p>
<p>Other auction details can be see from your shopping cart. Login to your account to claim your prizes at the following URL:</p>
<p>[ACCOUNT_URL]</p>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"../contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"../new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"../contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"../faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"../\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2011-11-16 14:47:11');
insert into email_templates (id, grp, title, subject, contents, updated) values ('10', 'coupon_redeem', 'Coupon Redeem', 'Coupon Redeem', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"716\" align=\"center\">
<tbody>
<tr>
<td bgcolor=\"#ffffff\">
<table style=\"height: 751px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\" width=\"912\">
<tbody>
<tr>
<td style=\"border-bottom: 1px solid; width: 600px;\"><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid\"><img src=\"images/maxabid-email.jpg\" border=\"0\" alt=\"maxabid.co.uk offers a variety of auctions\" title=\"maxabid.co.uk\" /></a></td>
<td style=\"border-bottom: 1px solid #c0c0c0;\" align=\"right\"><strong><span style=\"font-family: trebuchet ms,geneva; font-size: xx-small;\">Questions about [STORE_NAME]?<br />Call us today!<br />+44 20 1234 5678<br /></span></strong></td>
</tr>
<tr>
<td style=\"width: 600px;\" valign=\"top\">
<p><strong>Dear <span style=\"color: #ff0000;\">[CUSTOMER_NAME]</span>,</strong>&nbsp;</p>
<p>[STORE_NAME] in appreciation of your business is pleased to offer a free coupon. Just goto your account page and enter the following details in the coupon form to earn free bids with [STORE_NAME]. If you have any question or concerns about your order please do not hesitate to <a href=\"contact-us.html\" title=\"Contact maxabid.co.uk\">contact us</a>.</p>
<strong>COUPON [COUPON_CODE] DETAILS:</strong>&nbsp;                
<table style=\"border: 2px solid #eeeeee; height: 82px; background-color: #ffffee;\" border=\"2\" cellspacing=\"0\" cellpadding=\"4\" width=\"611\" height=\"10\">
<tbody>
<tr>
<td><strong>Coupon Code:</strong></td>
<td><span style=\"color: #000066;\"><strong>[COUPON_CODE]</strong></span></td>
</tr>
<tr>
<td><strong>Free Bids to Earn:</strong></td>
<td><span style=\"color: #ff0000;\"><strong>[COUPON_AMOUNT]</strong></span></td>
</tr>
</tbody>
</table>
<p>Once again thank you for being a customer with [STORE_NAME].</p>
<span><span style=\"font-size: x-small;\"><span style=\"font-family: verdana,geneva;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"><span style=\"font-family: verdana,geneva; font-size: x-small;\"> 
<hr size=\"1\" noshade=\"noshade\" />
<p><span style=\"font-family: verdana,geneva; font-size: x-small;\">Best Regards</span></p>
</span></span></span></span><span style=\"font-family: verdana,geneva; font-size: x-small;\"><a href=\"contact-us.html\" title=\"Contact maxabid online\">Maxabid Support Team</a> <br /></span><a href=\"mailto:support@maxabid.co.uk\"><span style=\"font-family: verdana,geneva; font-size: x-small;\">support@maxabid.co.uk</span></a><span style=\"font-family: verdana,geneva; font-size: x-small;\"><br /></span></span></span>
<p>&nbsp;</p>
</td>
<td style=\"width: 250px; height: 100%;\" align=\"center\" valign=\"top\">
<table class=\"norm\" style=\"border: 1px solid #c0c0c0; height: 85.87%;\" border=\"0\" cellpadding=\"10\" width=\"249\" bgcolor=\"#efefef\">
<tbody>
<tr>
<td style=\"height: 100%;\" valign=\"top\">
<p><span style=\"font-family: verdana,geneva; font-size: xx-small;\"><strong>[RIGHT_COL]</strong></span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><strong><a href=\"new-maxabid.html\" title=\"Online catalog of maxabid\">Products/Services</a></strong><span style=\"font-family: verdana,geneva;\"><span style=\"color: #030303; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"sitemap.html\" title=\"maxabid.co.uk sitemap\"><span style=\"color: #030303; font-size: xx-small;\"><strong>Site Map</strong></span></a></span><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"terms-of-use.html\" title=\"maxabid terms of use\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Conditions of Use</strong></span></a><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href=\"contact-us.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>Contact Us</strong></span></a><span style=\"color: #030303;\"><strong> <span style=\"font-family: verdana,geneva; font-size: xx-small;\">&nbsp;&nbsp;|&nbsp;&nbsp;</span></strong></span><a href=\"faq.html\"><span style=\"color: #030303; font-family: verdana,geneva; font-size: xx-small;\"><strong>FAQ</strong></span></a></p>
</td>
</tr>
<tr>
<td colspan=\"3\">
<div><span style=\"font-family: verdana,geneva;\"><span style=\"font-size: xx-small;\">&copy;2008-2009 </span><a href=\"http://www.maxabid.co.uk\" title=\"Maxabid Online\"><span style=\"color: #0000ff;\"><span style=\"font-size: xx-small;\"><span style=\"text-decoration: underline;\"><strong>[STORE_NAME]</strong></span></span></span></a> <span style=\"font-size: xx-small;\"> All Rights Reserved </span></span></div>
</td>
</tr>
</tbody>
</table>', '2012-01-22 13:45:38');
drop table if exists geo_zones;
create table geo_zones (
  geo_zone_id int(11) not null auto_increment,
  geo_zone_name varchar(32) not null ,
  geo_zone_description varchar(255) not null ,
  last_modified datetime ,
  date_added datetime not null ,
  PRIMARY KEY (geo_zone_id)
);

insert into geo_zones (geo_zone_id, geo_zone_name, geo_zone_description, last_modified, date_added) values ('2', 'United Kingdom', '', NULL, '2011-09-19 20:39:06');
drop table if exists group_fields;
create table group_fields (
  group_fields_id int(11) not null auto_increment,
  group_fields_name varchar(128) not null ,
  group_fields_description varchar(255) not null ,
  layout_id tinyint(2) default '1' not null ,
  limit_id tinyint(1) default '1' not null ,
  sort_id tinyint(3) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (group_fields_id),
  KEY idx_group_fields_order (sort_id),
  KEY idx_group_fields_status (status_id)
);

insert into group_fields (group_fields_id, group_fields_name, group_fields_description, layout_id, limit_id, sort_id, status_id) values ('1', 'Bids Entered', 'Number Selector', '1', '1', '1', '1');
insert into group_fields (group_fields_id, group_fields_name, group_fields_description, layout_id, limit_id, sort_id, status_id) values ('2', 'Auction Numbers', 'Number Selector', '1', '1', '1', '1');
drop table if exists group_options;
create table group_options (
  group_options_id int(11) not null auto_increment,
  group_fields_id int(11) not null ,
  group_types_id int(11) not null ,
  group_options_name varchar(255) not null ,
  price_status tinyint(1) default '0' not null ,
  stock_status tinyint(1) default '0' not null ,
  image_status tinyint(1) default '0' not null ,
  layout_id tinyint(2) default '1' not null ,
  limit_id tinyint(1) default '1' not null ,
  sort_id tinyint(3) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (group_options_id),
  KEY group_fields_id (group_fields_id),
  KEY idx_group_options_order (sort_id),
  KEY idx_group_options_status (status_id)
);

insert into group_options (group_options_id, group_fields_id, group_types_id, group_options_name, price_status, stock_status, image_status, layout_id, limit_id, sort_id, status_id) values ('1', '1', '5', 'Pick', '0', '0', '0', '1', '6', '1', '1');
insert into group_options (group_options_id, group_fields_id, group_types_id, group_options_name, price_status, stock_status, image_status, layout_id, limit_id, sort_id, status_id) values ('2', '2', '5', 'Pick', '0', '0', '0', '1', '6', '1', '1');
drop table if exists group_values;
create table group_values (
  group_values_id int(11) not null auto_increment,
  group_options_id int(11) not null ,
  group_fields_id int(11) not null ,
  group_values_name varchar(64) not null ,
  group_values_image varchar(128) ,
  group_values_price decimal(15,4) default '0.0000' not null ,
  group_values_stock int(3) not null ,
  sort_id tinyint(3) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (group_values_id),
  KEY group_fields_id (group_fields_id),
  KEY group_options_id (group_options_id),
  KEY idx_group_values_order (sort_id),
  KEY idx_group_values_status (status_id)
);

insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('1', '1', '1', 'N1', NULL, '0.0000', '0', '1', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('2', '1', '1', 'N2', NULL, '0.0000', '0', '2', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('3', '1', '1', 'N3', NULL, '0.0000', '0', '3', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('4', '1', '1', 'N4', NULL, '0.0000', '0', '4', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('5', '1', '1', 'N5', NULL, '0.0000', '0', '5', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('6', '1', '1', 'N6', NULL, '0.0000', '0', '6', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('7', '2', '2', 'N1', '', '0.0000', '0', '1', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('8', '2', '2', 'N2', '', '0.0000', '0', '2', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('9', '2', '2', 'N3', '', '0.0000', '0', '3', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('10', '2', '2', 'N4', '', '0.0000', '0', '4', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('11', '2', '2', 'N5', '', '0.0000', '0', '5', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('12', '2', '2', 'N6', '', '0.0000', '0', '6', '1');
insert into group_values (group_values_id, group_options_id, group_fields_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id) values ('13', '2', '2', 'N7', NULL, '0.0000', '0', '7', '1');
drop table if exists gtext;
create table gtext (
  gtext_id int(11) not null auto_increment,
  gtext_title varchar(64) not null ,
  gtext_description text ,
  status tinyint(1) default '1' not null ,
  PRIMARY KEY (gtext_id)
);

insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('1', 'FAQ Home', '<p>Add the FAQs here</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('2', 'About Us', '<p class=\"MsoNormal\"><span style=\"color: #6d6b6c;\">Maxabid is a trading name and a trademark of Maxiserve Limited. </span></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('4', 'Privacy Notice', '<p>Add the privacy details here</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('5', 'Terms & Conditions', '<p style=\"text-align: justify;\">LAST UPDATE: 27TH APRIL 2013</p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>1. Introduction</strong></span></p>
<p style=\"text-align: justify;\">This User Agreement is effective from 4th April 2010, for users that have registered on or prior to this date, and upon acceptance for new Users. For all intents and purposes under these Terms and Conditions:</p>
<ul style=\"text-align: justify;\">
<li>\"Maxabid\"      refers to the Maxabid.com website (and the company operated by Maxiserve      Limited)</li>
<li>\"User/Users\"      refers to any person who places a Bid on any item on Maxabid.com</li>
<li>\"Registered      User\" refers to any person that has registered as a user through the      Maxabid website</li>
<li>\"Unregistered      User\" refers to any person browsing the Maxabid website that has not      registered as a User through the Maxabid website</li>
<li>\"Bid/Bids/Bidding\"      refers to any Bid that a User chooses to place on an item</li>
<li>\"My      Maxabid\" refers to the personalised pages of the Maxabid website      where registered Users may edit their account details and view some recent      Maxabid activity including wins, orders, order status and.</li>
</ul>
<p style=\"text-align: justify;\">Welcome to Maxabid. Maxabid is a trading name of Maxiserve Limited, Unit 36, 88-90 Hatton Garden, London, England EC1N 8PN, United Kingdom.&nbsp;These Terms and Conditions apply to the services available from the domain: www.maxabid.com and from all other&nbsp;sites operated by&nbsp;Maxiserve Limited.&nbsp;If you reside in the U.K. or another country that is a member of the E.U. by using the services of the Maxabid.com and other sites where this User agreement appears you are agreeing to the following Terms&nbsp; and Conditions with Maxiserve Limited, Unit 36, 88-90 Hatton Garden, London, England EC1N 8PN, United Kingdom, and the general principles for the websites of our subsidiaries and affiliates worldwide.&nbsp;</p>
<p style=\"text-align: justify;\">Your use of our services is governed by the following Terms and Conditions, (also referred to as the \"User Agreement\"). Before you can become a registered User of Maxabid, you must read and accept all of the Terms and Conditions of this User Agreement and each of the \"Additional terms\" referred to in this User Agreement. We strongly recommend that you should read through all the terms carefully. The User Agreement constitutes a legally binding agreement between you and Maxabid.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>2. General and Scope</strong></span></p>
<p style=\"text-align: justify;\">The following General Terms and Conditions shall apply exclusively to the business relationship between Maxiserve Limited and its internet platform http://www.maxabid.com (hereinafter referred to as: \"Maxabid\") and our Users in the version applicable on the date of use. By participating in Maxabid, the User fully accepts the following Terms and Conditions, as well as the manner in which Maxabid operates as described on the website http://www.maxabid.com. Maxabid does not recognise any terms and conditions set by the User that differ from these unless Maxabid has agreed to their validity in writing. None of the content on the Maxabid website is intended to amount to advice on which reliance should be placed. Maxabid disclaims to the fullest extent permitted by law all liability and responsibility arising from any reliance placed on such materials by any visitors to the Maxabid website or by anyone who may be informed of any of its contents.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>3. Maxabid Registration and Participation</strong></span></p>
<p style=\"text-align: justify;\">You may not use Maxabid if you are under the age of 16 or if you are not able to form legally binding contracts, or if your Maxabid account/membership has been suspended. Maxabid Users must be UK residents in order to purchase items or place Bids.&nbsp;Employees and relatives of employees of Maxiserve Limited are not eligible to participate in Maxabid services. Users are not required to register with Maxabid to view Maxabid services and the website. Unregistered Users can browse the website, view items and auctions but cannot participate until registration is complete thus becoming a Registered User. Only Registered Users have access to their personalised \"My Maxabid Account\" page. Each User may register only once, using their residential postal address. Non-residential and post office box addresses may not be used for User registration purposes. During the registration process, the User must choose a user name. The user name chosen must not offend standards of common decency or infringe upon the rights of third parties. Additionally, the use of misleading user names or user names that imply marketing related names are strictly prohibited. Maxabid reserves the right to change or delete a user name if it finds a User violating the Maxabid Terms and Conditions. The associated password must be kept secret by the User. In general, the User shall be liable for all activities that are undertaken using the User account together with the associated password.</p>
<p style=\"text-align: justify;\">In order to obtain free sign up welcome bids the User must provide a legitimate United Kngdom mobile phone number during the registration (sign up) process. This is done in order for Maxabid to authenticate the User and prevent future misuse of the service. An authentication code is sent to the mobile phone which the User must use after the registration process to obtain free sign up bids. Should a User need to change the phone number linked to the Maxabid User account, the User must&nbsp;contact Maxabid and request this to be done.</p>
<p style=\"text-align: justify;\">User accounts created during registration and associated Bids are non-transferable. Only one person may use any one particular user account. Maxabid will not discuss a User account with any person other than the person registered with the User account. Maxabid&nbsp;reserves the right to restrict the number of User accounts that may be registered to a particular household. Maxabid has the right to verify the identity of each individual User. In the event of Maxabid being unable to verify or authenticate Users identity using reasonable endeavours, then Maxabid may ask Users to provide photo ID, proof of address and other documentation as Maxabid requires. Should such proof not be provided Maxabid has the right to suspend or permanently remove such unverified User accounts and withdraw any auctions won by such Users and any Bids may be suspended or cancelled until proof of identity is provided. Should a User refuse (or within 21 days of request fail) to provide the necessary documents either by upload, email&nbsp;or post/mail (as Maxabid requests from the User), the User forfeits the right to receive any won items and will be refunded only the amount the User has paid for any auctioned items won but not yet delivered. If a User account is permanantly deleted as a result of a breach to these Terms and Conditions then the User will not receive a refund on any outstanding bids in the Users Bids Balance and will be barred from re-registering as a new User.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>4. Points&nbsp;Program</strong></span></p>
<p style=\"text-align: justify;\">The Maxabid&nbsp;Points Program is active as of 4th April 2010. Maxabid customers who purchase Bids, e-Vouchers and full retail price items above are automatically enrolled into the Maxabid Points Program. Under this program, Maxabid Users will enjoy privileges that non-members will not have access to. Users will receive Points for every new sign up User they refer to Maxabid. A new sign up User will enter the email address of the User they were referred by and when the new User purchases their first Bids at retail price, the referer User will receive Points as a referral thank you. Users with Points in their balance will be able to redeem them at checkout making reduced or partial payments. The following terms and conditions apply: http://www.maxabid.com/my_points_help.html</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>5. Rules, Restrictions and Disqualifications</strong></span></p>
<p style=\"text-align: justify;\">Any abuse of a User account or violation of these Terms and Conditions by a User will inevitably result in the disqualification of the User and may result in criminal proceedings. Maxabid may, at any time, exercise its sole discretion to suspend or permanently exclude a User from participating in Maxabid and any of its services. Maxabid may exclude Users, if and insofar as, it is in the reasonable opinion of Maxabid that a User has:</p>
<ul style=\"text-align: justify;\">
<li>Breached      a provision of these Terms and Conditions</li>
<li>Violated      any laws or infringed upon the rights of a third party</li>
</ul>
<p style=\"text-align: justify;\">Maxabid may exclude Users for any other reasons justified in the interest of protection of its Users or to prevent manipulation of any kind, including, but not limited to:</p>
<ul style=\"text-align: justify;\">
<li>Collusion      and/or team Bidding</li>
<li>Use      of unauthorised software (including bidding software or any other      unauthorised software)</li>
<li>One      User or person using more than one account</li>
<li>Users      not paying for Bids, or making multiple User Registrations in order to      receive free sign up bids</li>
</ul>
<p style=\"text-align: justify;\">Users will also be excluded if there is reason to believe that the User has been reverse engineering, decompiling and/or disassembling any portion of the website, server or software provided by or run by Maxiserve Limited in order to commit fraud or to defraud Maxabid, or to manipulate the outcome of sales and/or auctions or any part of the services and facilities offered by Maxiserve Limited. Collusion includes co-operation between two or more Users that limits open competition and discourages other Users from participating in an auction. Collusion can be viewed as team Bidding or any other team use that detracts from the spirit and enjoyment of Maxabid auctions. Collusion includes any activities that give the impression that there is more competition than there actually is:</p>
<ul style=\"text-align: justify;\">
<li>Users      allowing one group member to bid on an item and then stop Bidding and      having another group member begin Bidding on the item</li>
<li>Members      of the same household/family/group of friends Bidding in tandem on an item</li>
<li>Pooling      of money between Users for any auctions</li>
<li>A      group of Users intentionally targeting a single customer</li>
</ul>
<p style=\"text-align: justify;\">In the event that Maxabid suspects that Users are working in collusion with each other, Maxabid reserves the right to investigate the matter further. In the event that Maxabid determines two or more Users are colluding, Maxabid reserves the right to disqualify the Users. Users whom have been found to be colluding by Maxabid will be disqualified from any auction in which they are determined to be colluding in and will not be refunded any bids placed in the auction. Maxabid may contact these Users and provide them with a list of Users that they may not participate in the same auctions with/against. Only one person is allowed to bid on one individual auction. The person may not be the member of a colluding team. Members of the same household, family members, partners, spouses and friends will not be allowed to participate in the same auction. If there is an exceptional circumstance, such as a credit card not working, a User may contact Maxabid ahead of time and ask for a special exception. It will be at the discretion of Maxabid whether or not to approve any outstanding circumstances.&nbsp;</p>
<p style=\"text-align: justify;\">Furthermore, Maxabid reserves the right in its absolute discretion to restrict the number of sales in which a User may participate or may win. Maxabid reserves the right to disqualify Users who have created more than one account. Maxabid further retains the right to disqualify User accounts where it can be proven that more than one person has been Bidding from one account. Maxabid has the right to disqualify a User and/or the Users winnings if Maxabid deems that a particular User or Users have used unfair means to win an auction. Maxabid shall neither be liable to return any Bids, purchased or non-purchased, nor is Maxabid liable to reimburse any monies that have been used to purchase Bids or products. This is to protect the right of its Users as well as Maxabid itself.</p>
<p style=\"text-align: justify;\">Maxabid.com is UK based&nbsp;with its operations bound by all applicable UK&nbsp;laws. If your IP address is&nbsp;based outside of our normal area of operation, we may suspend service to preserve the quality of service for our local customers and because we may not be properly licensed to operate in that country. Users may not hold Maxabid responsible for any loss you may incur as a result of Maxabid taking any of the actions outlined above. Any of the actions outlined above may lead to the suspension of service, including the suspension of Bids in a Users balance.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>6. Purchasing Bids and Payment</strong></span></p>
<p style=\"text-align: justify;\">Bidding rights, so-called \"Bids\", have to be purchased by Users prior to online Bidding. Only registered Users can submit the convenient online Bids, which can be purchased in packages (so-called &ldquo;Bid Packages&rdquo;) at the currently applicable prices, which can be viewed under \"Buy Bids\" on www.maxabid.com. All the Bids must be paid for in advance. Purchased Bid Packages are non-refundable and hence Users should buy only as many Bids as they are willing to spend on the Bidding process. Users can make payments using their PayPal account or if&nbsp; Users do not have&nbsp; a PayPal account payment can be made with a valid credit or debit card at the PayPal checkout page. Users can also cmake payments by using directebanking.com services to transfer funds from the User bank account directly to Maxabid. Payments may not be made over the phone. When the Bids are purchased and deposited into the User&rsquo;s Maxabid account, a contract for the purchase of the Bids is created.</p>
<p style=\"text-align: justify;\">Should a Bid payment not be honoured by a User&rsquo;s financial institution a returned payment fee will be charged to the user to cover the additional administrative costs incurred by Maxabid. Maxabid reserves the right to block access to the User account until payment in full of the invoice amount, including any fees due for returned payment, etc., and to withhold any outstanding deliveries of items purchased, bBds&nbsp; purchased and/or auctions won until payment in full is received. Maxabid also reserves the right to temporarily block access to any unverified User accounts that appear to be abusing the spirit of Maxabid by purchasing large volumes of Bids until identification has been verified.</p>
<p style=\"text-align: justify;\">All types of Bids (e.g. \"free Bids\", \"Bids won in auction\", \"special offers\" or \"Bids purchased in bundles\") have a expiration date. Upon expiration of the time limit stated, the Bids expire and can no longer be used. There will be no refund for Bids that have not been used before expiration. All purchased Bids expire after 180 days. All free Bids expire in 60 days from the date when the Bid has been credited on a User\'s Maxabid user account. All discount codes issued have a time limit. Discount codes must be used during the specified time in order to be valid. Discount codes will not be applied retroactively. Users must check the Terms and Conditions of each discount code for specific details. Any special promotions presented by Maxabid shall only be valid if they are announced on Maxabid&rsquo;s website, Maxabid&rsquo;s newsletter, Maxabid produced T.V., radio or print advertisements. Any promotions of a limited duration shall likewise be announced on the website or Maxabid produced newsletters, T.V., radio or prints advertisements. Upon expiration of the time limit stated, or after the sale(s) that are part of the particular promotion has/have ended, the promotion is deemed to have ended. Promotional codes are non-transferable.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>7. Bidding</strong></span></p>
<p style=\"text-align: justify;\">Bids can only be placed by registered Users and in order to place a Bid the User must be signed in to the User account. Once logged in, in order to place a bid the User&nbsp; will have to select an auction either from the mini-tabs in the main auction page or by using the mini-tabs to \'click\' a selected auction to open up a full page of that auction which gives a product description as well as in-depth bidding history, auction expiry date and other option such as Maxabid having the right to reserve any auction early, or when a reserve price/bid has been met. All Bids are to be submitted through the Bidding function and that can only only happen when a User presses the \'Place Your Bid\' button. Once a Bid has been submitted, it is deducted from the User&rsquo;s account cannot be retracted.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>8. The Bidding Process</strong></span></p>
<p style=\"text-align: justify;\">i) For non-timer auctions the auction ends on the expiry date with the single option for Users to click the \'Place Your Bid\' button. The last user to place a Bid before non-timers auction closes is deemed to be successful Bidder and will be extended an offer to purchase the item for the closing price +VAT (as well as&nbsp; the stated shipping and handling fees +VAT) by following all the steps necessary to complete the sale</p>
<p style=\"text-align: justify;\">&nbsp;ii) For timer auctions with the option for Users to click the \'Place Your Bid\' button, an auction will begin at a specified time or when it is added to the live auctions from the coming soon section or go live after a current auction ends. Each auctioned item will begin at &pound;0.00. The timer will begin to count down for a designated time and users may place Bids. With each Bid submitted, the price of the item on which the User is Bidding increases by the stated fixed increment (usually &pound;0.01 GBP). Each time a user places a bid, the timer will start over. The last user to place a Bid before the auction closes is deemed to be successful Bidder and will be extended an offer to purchase the item for the closing price +VAT (as well as&nbsp; the stated shipping and handling fees +VAT) by following all the steps necessary to complete the sale.</p>
<p style=\"text-align: justify;\">iii) For non-timer auctions with the option for Users to enter a Lowest Unique Bid and then click the \'Place Your Bid\' button the auction ends on the expiry date. The last user to place a Bid before non-timers auction closes is deemed to be successful Bidder and will be extended an offer to purchase the item for the closing price +VAT (as well as the stated shipping and handling fees +VAT) by following all the steps necessary to complete the sale.</p>
<p style=\"text-align: justify;\">iv) For timer auctions with the single option for Users to enter a Lowest Unique Bid and then click the \'Place Your Bid\' button, an auction will begin at a specified time or when it is added to the live auctions from the coming soon section or go live after a current auction ends. Each auctioned item will have a maximum bid amount and Bids cannot be submitted for beyond the stipulated amount. The timer will begin to count down for a designated time and users may place Bids. With each Bid submitted, the price of the item on which the User is Bidding will stay hidden but the user name&nbsp; will be visible to show who is winning the auction. Each time a user places a bid, the timer will start over. The last user to place a Bid before the auction closes is not automatically deemed to be successful winning Bidder. The lowest unique bid will determine the winner of the auction regarldess of who the last Bid was placed by. The winner shall be extended an offer to purchase the item for the closing price +VAT (as well as the stated shipping and handling fees +VAT) by following all the steps necessary to complete the sale.</p>
<p style=\"text-align: justify;\">In some cases an auction may display the number of bids remaining before the auction will come to an end and this will overrule the date of expiry. If the number of bids required before the auction is to end are not placed and Maxabid does not end the auction early, then Maxabid reserves the right to extend the auction by any period up to a maximum of 12 months and has the right to use the extending process a maximum of three times per auction if the reserve price is not reached and/or if the reserve bids are not placed, yet Maxabid still retains the right to end any auction early before the reserve price is reached or reserve bids are placed regardless of how many times an auction has been extended. In the event a reserve price and/or reserve bid has not been met and Maxabid ends the auction early (whether or not an auction has been extended), then Maxabid reserves the right to either allow the User to purchase the item/product at the final bid price or at its discretion may decide to invoke/apply (the Offsetting, Witholding Payment and Retention of Title clause) Section 14 of these Terms and Conditions and may withold the item/product being auctioned and only offer the winning User a 75% payment of the actual final price of the auction. The remainder 25% is to be retained by Maxabid and will be put towards (its Charities and Good Causes clause) Section 21.</p>
<p style=\"text-align: justify;\">In the event that an auction closes prematurely or irregularly by a technical fault, or otherwise Maxabid reserves the right to rescind the offer to purchase the item. (Details regarding these situations are outlined below)</p>
<p style=\"text-align: justify;\">Maxabid reserves the right to change the auction times at any time. Additionally, Maxabid can add, re-schedule or remove products from the Maxabid.com website at anytime without notice, including auctions which are already in progress or live. An auction is deemed to have closed when the timer on the auctioned item counts \"closed\" and no Users have placed subsequent Bids. Users should be aware that the timer available to the User is an approximation which may be affected by network delays. The final decision of when an auction closes will be based on the timer used by Maxabid servers. Maxabid will use its best endeavours to ensure that users timers are as accurate as possible. In the event that Maxabid cancels an auction, Maxabid may at its discretion give Bids back to affected Users but is under no obligation to do so.</p>
<p style=\"text-align: justify;\">Future auctions (referred to as Coming Soon auctions) are being queued in each tab to give Users an idea of up coming auctions in that tab but Maxabid does not necessarily intend for \'Coming Soon\' auctions to go live in the order they appear in the queue or the tab and can change the order of live auctions or soon to be live auctions without giving any notice to Users as from time to time it may suit the business needs of Maxabid at that particular time.</p>
<p style=\"text-align: justify;\">Some auctions have designated auction hours. These hours will be listed on the auction specific page. Auctions which pause during the night will resume at the designated time, the next day or early the same day if necessary. The last Bidder is determined by Maxabid&rsquo;s database. In the event of any dispute occurring, it is Maxabid&rsquo;s decision on which User was the last Bidder of any particular auction is/was and that decision shall be final.</p>
<p style=\"text-align: justify;\">All other Bids on the item expire and will not be refunded. In order to keep the auctions interesting for all Users Maxabid may decide to restrict Users from participating in Maxabid auctions in the following two cases: (i) if the User has won two products on the same day and (ii) if a User has won 10 products within a 30 day period. Special limits may apply to high value items and will be posted on the website.</p>
<p style=\"text-align: justify;\">We cannot guarantee continuous access to our services, and operations of our sites may be interfered with by numerous factors outside of our control. While we will use our reasonable endeavours to maintain an uninterrupted service, we cannot guarantee this and we do not give any promises or warranties (whether express or implied) about the availability of our services. In the event of any technical problem (third party or other) affecting the Maxabid auction platform, the countdown timers for all (or some) live auctions may be paused until the issue has been resolved. Auctions will continue once the system is brought back up. Should auctions close prematurely due to any technical problem, affected auctions may be restarted. The auctions&rsquo; countdown timers will be continued from the same point in time they were at prior to the technical problem. <br /> <br /> Alternatively, Maxiserve Limited. has the right to restart all auctions from the beginning, once the technical problem has been rectified or may apply/invoke Section 14 of these Terms and Conditions. Maxiserve Limited may attempt to return purchased Bids (not free Bids) placed on auctions that had to be paused and then restarted from the beginning due to third party technical problem. Should Maxiserve Limited suspect that an unlawful action(s) or action(s) against its Terms and Conditions brought the service down or illegal action manipulated the outcome of the auction; it has the right to investigate the matter and take legal action against perpetrators.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>9. Notification, Acceptance and Payment of Won Auctions</strong></span></p>
<p style=\"text-align: justify;\">When an auction closes, a notification is automatically sent by email to the final bidder. Since auctions may close prematurely due to technical error, this notification is not considered a binding contract. If an auction has closed due to a technical error, Maxabid reserves the right to void the notification, return any money paid for the item and resume or restart the auction, or invoke/apply Section 14 of these Terms and Conditions. This mechanism exists to maintain the fairness of the auction to all users who may have been prevented or discouraged from continuing to Bid in the auction. Once the item has shipped the formal invoice will be available via My Maxabid. After the auction has ended, if the last bidder (\"Winner\") is online, they have access to all of the important sale information such as the total price (sale price plus VAT, the shipping and handling costs plus VAT), and payment options on the \"My Maxabid\" page.</p>
<p style=\"text-align: justify;\">The winning User must actively confirm the win by following the steps indicated under My Maxabid. Maxabid reserves the right to withdraw an offer after 21 days unless the winner contacts Maxabid with a reason to delay payment and Maxabid agrees to hold the item and wait for payment via its online checkout facility at maxabid.com. In the event that the winner has not paid nor confirmed an interest to exercise the option to purchase the product within 21 days of the auction ending, the option to purchase the product expires. In the event of the expiration of the option to purchase the product, the winning User&rsquo;s right to ownership and delivery of the item will be permanently revoked. In this event Maxabid may at its discretion apply/invoke Section 14 of these Terms and Conditions&nbsp;. There is no entitlement to receive a refund on any Bids placed once the auction has ended, even if the winner does not exercise their option to purchase.</p>
<p style=\"text-align: justify;\">The winning bidder will also receive a notification of the win by e-mail or SMS with a request for payment. The User must pay the total price (sale price plus VAT, the shipping and handling costs plus VAT) no later than 30 days after the notification of the win is sent and/or the item&nbsp; has been purchased or the auction won. After this deadline has passed, Maxabid can set aside and rescind the purchase contract with the User, and the User&rsquo;s right to ownership and delivery of the item will be permanently revoked. The winner has no entitlement to a refund of Bids.</p>
<p style=\"text-align: justify;\">The item(s) will not be sent to a User until the total price of the item and any outstanding Bid payments due have been paid to Maxabid. In such a case, any incomplete payment received from the winning bidder will be regarded firstly as payments towards the Bids and then as payment towards the total sale price. A right of retention shall be exercised with regard to the item purchased at auction until all Bids and the total price has been paid for in full. Maxabid reserves the right to charge the winner reasonable storage charges for items won through Maxabid auction but not yet paid for.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>10. Cancellation and Rescission</strong></span></p>
<p style=\"text-align: justify;\">Maxabid grants to the User ordering for personal purposes the right to cancel in accordance with statutory provisions on mail order sales and e-commerce. The right to cancel does not cover purchase of audio and video recordings or of software, if and to the extent that the security seal on the delivered media has been broken by the consumer. This does not affect the consumer&rsquo;s statutory rights. Purchase contracts with parties who are not retail consumers are treated by Maxabid as international supply agreements and, therefore such parties do not have rights to cancel concluded contracts (unless agreed to in writing by Maxabid or required by law).</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>11. Cancellation Instructions</strong></span></p>
<p style=\"text-align: justify;\">User may cancel the purchase order within two weeks in written form (e.g. letter, fax, e-mail) or by returning the auction items, without stating your reasons. The cancellation period begins no earlier than when the item and these instructions are received. To meet the cancellation requirements, it is sufficient to send the cancellation notice or return the item in a timely manner. Cancellation is to be sent to the address given in the contact details page on this website: E-Mail: support@maxabid.com</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>12. Consequences of Cancellation</strong></span></p>
<p style=\"text-align: justify;\">In the case of a valid cancellation, both parties must return what has been received in performance of the purchase order and the benefits derived there from (e.g. interest) as applicable. If the User is unable to return to us the service or product received or is only able to return it in part or in a depreciated condition, the User will have to pay compensation to Maxabid, to cover the loss of value, unless the deterioration resulted from unavoidable use or as a result of a manufacturing fault. In other respects, the User may avoid this duty to pay compensation by not using the item(s) as the User would his own property, and by refraining from any action that would detract from its value. Items that have been delivered via Royal Mail or another carrier should be returned via the same method in the original packaging where possible. In these cases, return is free of charge for the User. Items that cannot be returned via this method will be collected by <strong>&nbsp;</strong>Maxabid\'s<strong>&nbsp;</strong> agents through co-ordination with the User. The User must pay any compensation as required by depreciation of auction items delivered to the User, within 30 days after sending notice of cancellation. If the User cancels by the deadline of two weeks, they are not obliged to take delivery of the item. Maxabid will refund the purchase price already paid. There is no entitlement to a refund or replacement of the Bid(s) used in the auction process.</p>
<p style=\"text-align: justify;\">If the customer decides to cancel after delivery and no misrepresentation has been made about the item by Maxabid or any of its partners on maxabid.com, the customer is responsible for the costs of the return shipment if total final auction price (auction closing price including VAT and shipping and handling) of the returned item does not exceed a sum of &pound;40.00, or, if the item price is greater than &pound;40 and the partial or total item price has not been paid by the winning bidder/User</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>13. Delivery</strong></span></p>
<p style=\"text-align: justify;\">Unless otherwise specified, delivery will be made from our partners warehouses or from our offices to the delivery address provided by the User. Deliveries are made solely within the UK including Northern Ireland (exclusively on the mainland, otherwise only upon inquiry). As Maxabid works with trusted delivery partners to ensure the best possible service and widest reach, all delivery estimates are for information only and do not constitute binding agreements. Obvious damage to the item or packaging during transit should be reported to the carrier by the winning user, if the item is damaged it should be refused, not signed for. If you wish to claim for concealed defects or damage after signing for an item, you must inform us about the damaged or faulty goods within 3 days of receipt. Maxabid cannot accept responsibility for damaged or faulty goods once 3 days have lapsed. In all cases you must inspect your item either before signing for receipt or immediately after delivery. Failure to raise a query with Maxabid concerning a damaged or incorrect item within 3 days of delivery constitutes acceptance of the item \'as is\'. Maxabid does not accept liability for damage or failure of products due to neglect or misuse by the recipient.</p>
<p style=\"text-align: justify;\">Maxabid will try to arrange delivery of all items purchased/won within 7 days but in some circumstances this may not be possible so Users may have to wait up to 14 days before items purchased/won can be delivered. In the event that Maxabid or its delivery partners are not able to deliver the item within a reasonable time, Maxabid shall be entitled to offer the winning User a substitute item with comparable or better features, or to set aside the contract through rescission and offer a refund of any payments received for the auction item. This does not affect the User&rsquo;s statutory rights.&nbsp;</p>
<p style=\"text-align: justify;\">In the event that a User fails to provide a shipping address recognized by Royal Mail we will ask the User for confirmation. If the User fails to respond within 30 days with a valid address, the item will be forfeited.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>14. Offsetting, Withholding Payment and Retention of Title</strong></span></p>
<p style=\"text-align: justify;\">i) The User shall only have the right to offset if his or her counter claims that have been upheld by a court of law or are not disputed by Maxabid. The User shall only be entitled to exercise a right to withhold payment to the extent that his or her counterclaim is based on the same contractual relationship. The item delivered shall remain in Maxabid\'s property until payment is made in full of the total price and the Bids used in the sales activity. Until such time as payment has been made in full the User undertakes to store the item safely and securely, including insuring the item against fire, theft and damage. Maxabid shall have full rights of access to the place where the item is located during normal business hours to take possession of it.</p>
<p style=\"text-align: justify;\">ii) Under these Terms and Conditions if Maxabid ends an auction early, Maxabid may withold any item/product being auctioned and only offer the winning User a 75% payment of the actual final price of the auction. The remainder 25% is to be retained by Maxabid and will be put towards (its Charities and Good Causes clause) Section 21.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>15. Damages, Reimbursement of Expenses and Limits on Liability</strong></span></p>
<p style=\"text-align: justify;\">Claims for damages by the User against Maxabid, its legal representatives, employees, vicarious agents or parties who assist in performance are prohibited. In particular, Maxabid shall not be liable for faulty operation of the computer hardware or software during Bid submission or website browsing and for incorrect or overly slow transmission of Bidding data by the Internet service provider. Should such an event occur, the bidder shall have no legal rights over the item and have no grounds for claim against Maxabid. This shall not apply in the case of damage that is due to the deliberate or grossly negligent conduct of Maxabid, its legal representatives, or parties assisting it in performance. This shall also not apply if Maxabid expressly provided the guarantee for an item&rsquo;s features in writing or a defect was fraudulently concealed.</p>
<p style=\"text-align: justify;\">Insofar as Maxabid is insured for its liability for any damage, Maxabid can also assign any entitlement to insurance settlement to the injured party. The User shall be obligated to accept this assignment. Liability for reimbursement of expenses shall likewise only be assumed in accordance with the stipulations above. Liability for damage arising from loss of life and limb, health, or well being, and that according to product liability law shall not be affected hereby.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>16. Guarantee and Return Policy</strong></span></p>
<p style=\"text-align: justify;\">The items are supplied on the basis that they are reasonably fit for any reasonable purpose to which the winner may put them and subject to what is authorized by the manufacturer in the instruction manual. All other warranties or conditions (whether express or implied) as to quality, condition or description (whether statutory or otherwise) are excluded to the fullest extent permitted by law.</p>
<p style=\"text-align: justify;\">The item descriptions on maxabid.com are provided according to the best of Maxabid&rsquo;s knowledge and belief. These descriptions do not constitute contractually stipulated features and characteristics, but rather serve only to provide information to the User. Images of products on maxabid.com may differ from those actually supplied.&nbsp; In the event that the User is sent a replacement product for an item already delivered, the User shall be obligated to return to Maxabid the item already delivered within 30 days at Maxabid&rsquo;s expense. Maxabid reserves the right to assert damages under the conditions set down by law.</p>
<p style=\"text-align: justify;\">The User has the right to return a faulty item within a reasonable period of time. The User must keep all the manufacturer&rsquo;s warranty information that accompanies the item as this may be needed should there be a fault. In the unlikely event that you have a faulty item, the User may find it quicker and easier to contact the manufacturer directly so that they can rectify the problem If the User chooses to return the item to us, please email to our Returns Support Centre (within 10 days of receipt) <a href=\"http://mce_host/%22mailto:returns@madBid.com%22\">returns@</a><a href=\"http://mce_host/%22mailto:returns@madBid.com%22\">maxabid</a><a href=\"http://mce_host/%22mailto:returns@madBid.com%22\">.com</a></p>
<p style=\"text-align: justify;\">If you find that Maxabid has sent you an incorrect item, please contact us at returns@maxabid.com. A Maxabid representative will then contact you with instructions on how to return the item. If the instructions provided by Maxabid are followed there will be no charge for the return. Maxabid will send the User the replacement as soon as the original item is received by Maxabid. The User must return the original item to Maxabid within 30 days of our email confirming the issue of the replacement item In case of delays Maxabid holds the right to cancel the issue of replacement.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>17. Technical Errors and Unforeseen Circumstances</strong></span></p>
<p style=\"text-align: justify;\">We cannot guarantee continuous access to our services, and operations of our sites may be interfered with by numerous factors outside of our control. While we will use all reasonable endeavours to maintain an uninterrupted service, we cannot guarantee this and we do not give any promises or warranties (whether express or implied) about the availability of our services. In the event of any technical problem (third party or other) affecting the maxabid.com auction platform, the countdown timers for all (or some) live auctions will be paused until the issue has been resolved. Auctions will continue once the system is brought back up. Should auctions close prematurely due to any technical problem, affected auctions may be restarted. The auctions&rsquo; countdown timers will be continued from the same point in time they were at prior to the technical problem where possible or from top of the associated timer. Maxabid will use its best endeavours to notify the user that the auction has closed irregularly as soon as possible. In the event that the user has already paid for the item, the user will be refunded the amount paid for the item and the auction will be resumed at a later time. Alternatively, Maxiserve Limited has the right to restart all auctions from the beginning, once the technical problem has been rectified. Maxiserve Limited may attempt to return purchased Bids placed on auctions that had to be paused and then restarted from the beginning due to third party technical problem. Free Bids will not be returned. In the event of a technical errors, Maxabid will use its best endeavours to ensure that Users are notified of any errors, however, immediate notification is not guaranteed. A delay in delivering notification of a technical error or unforseen circumstance is not grounds for grievance or compensation or complaint so Maxabid will not be held liable in the event that this scenario has occured.</p>
<p style=\"text-align: justify;\">Should Maxiserve Limited suspect any unlawful act or actions against its Terms and Conditions have manipulated the outcomes of sales and/or auctions then it has a right to investigate the matters(s) and if it deems appropriate or necessary to take legal action and to inform the police. Maxiserve Limited will suspend any User account as well as suspend the releasing of any item or items the User claims to have either purchased or won until it can be ascertained beyond any doubt that the User or Users involved in the investigation did not commit any crime and did not breach the Terms and Conditions that are contained within this document.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>18. Applicable Law and Severability Clause</strong></span></p>
<p style=\"text-align: justify;\">This User Agreement shall be governed by the sole Jurisdiction of England and Wales. This User Agreement Shall exclude applictions of the UN Convention on the International Sale of Goods.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>19. Forum Selection Clause</strong></span></p>
<p style=\"text-align: justify;\">In the event of a dispute between Users and Maxabid (or Maxiserve Limited), any legal action or suit may only be bought in the Court of England.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>20. The Gambling Commission (UK)</strong></span></p>
<p style=\"text-align: justify;\">All Users participating in using the Maxabid website auctions agree that they are not gambling (as defined by the Gambling Commission UK and the Gambling Act 2005). There is no randomness or element of chance in any of our auctions models or types. Users apply strategy and skill to succeed and do not wager on their success. Additionally, your costs cannot escalate as our Terms and Conditions do not allow more than one account per user. You may find it useful to read <a href=\"http://www.gamblingcommission.gov.uk/PDF/Reverse%20auctions%20-%20frequently%20asked%20questions%20-%20June%202008.pdf\" target=\"_blank\">Reverse Auctions, Frequently Asked Questions, June 2008</a> and <a href=\"http://www.gamblingcommission.gov.uk/PDF/prize%20competitions%20and%20free%20draws%20-%20the%20requirements%20of%20the%20gambling%20act%202005%20-%20december%202009.pdf\" title=\"Prize competitions and free draws - The requirements of the Gambling Act 2005 - December 2009\"> </a><a href=\"http://www.gamblingcommission.gov.uk/PDF/prize%20competitions%20and%20free%20draws%20-%20the%20requirements%20of%20the%20gambling%20act%202005%20-%20december%202009.pdf\" target=\"_blank\">Prize competitions and free draws - The requirements of the Gambling Act 2005 - December 2009</a>, which explains the UK Gambling Commission\'s view of lowest unique bid auctions and compliance to the Gambling Act 2005 as well as other information. The Gambling Commission (UK) do not vet websites before they go live in order to ascertain if they fall under&nbsp; Gambling Act 2005. Maxiserve Limited believes in good faith that the auctions and services provided on the Maxabid website are in fact competitions for leisure purposes and are definitely not gambling services. Users agree that Maxiserve Limited can alter the function, make-up and /or design of any auction if it receives any instructions or advice to make changes to its business or auction competition model from the Gambling Commission.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>21. Charities and Good Causes</strong></span></p>
<p style=\"text-align: justify;\">All final sale amounts (monies received from Users paying for items/products purchased or won) on auction in the \'Charity\' tab and those listed as \'Charity Auction\' in the \'Featured\' tab will be donated to charity. In most circumstances Maxabid may take the collective monthly, quarterly, bi-annual or annual figure as a gross representation and then deduct any fees it had to pay to receive the payment from its payment processing partners before calculating the net figure to be donated.</p>
<p style=\"text-align: justify;\">If any money is designated to be donated to charity as a result of Section 14, the amount will be added as a net figure. Donations may be made to charities numerous times over varying periods over a 12 month period but Maxabid will endeavor to donate all monies designated for charities within 12 months of receiving them, however, all monies designated to be donated to charities will be used solely for that purpose. In some circumstances where a large amount of money is involved on a charity auction, the User and Maxabid may agree for the User to make the donation directly to a charity nominated by Maxabid and&nbsp;sign the Gift Aid declaration as it will increase payment to the charity. Once it has been ascertained that the donation and Gift Aid declaration has been received by the charity, Maxabid will make arrangements for posting/delivery of the item to the winner/User subject to Section 13 (Delivery).</p>
<p style=\"text-align: justify;\">-- -- -- -- --</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>1. General</strong></span></p>
<p style=\"text-align: justify;\">Your  data is collected and processed by us exclusively according to statutory  regulations, in particular, the UK\'s Data Protection Act and Privacy  and Electronic Communications Regulations.</p>
<p style=\"text-align: justify;\">You  may use Maxabid at any time without registering. When you visit our  website, data is saved on our servers in an anonymous format, as is the  website that you visit us from, the websites that you proceed to from  our site, and your IP address. As this is being done, no personal data  is associated with you other than those mentioned.</p>
<p style=\"text-align: justify;\">However,  we do need some personal information from you so that you can actively  participate in Maxabid. We would therefore like to request that you  agree to the collection and storage of some of your data. You can revoke  your consent any time by cancelling your registration/membership. Once  you have registered on Maxabid you are asked to choose a Maxabid  \'username\' together with a password, which then allows you to take  advantage of all of the Maxabid services. Your username can only be  changed at your request?by contacting our Customer Services department.  Once we are informed, we will adjust incorrect data accordingly however  we have the right to amend and/or edit any username that we, any of our  business associates/partners, our Users and anyone in general browsing  the Maxabid website may (or could find) inappropriate or offensive. This  applies to (but not exclusively to) usernames incorporating swearing  (or swear words) and those deliberately designed to cause offence to  people based on their age, religion, gender and race.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>2. Data Collection</strong></span></p>
<p style=\"text-align: justify;\">Personal  data is the information needed to identify you, for example your name,  address, date of birth, e-mail address, mobile phone number, etc. With  your consent, personal data are recorded and processed by Maxabid, in  particular (but not exclusively):</p>
<ul style=\"text-align: justify;\">
<li>When      you register with Maxabid</li>
<li>When      you buy items or bids </li>
<li>When      you win an auction or set newsletter/SMS preferences</li>
</ul>
<p style=\"text-align: justify;\">We will<strong> NEVER </strong>collect  or store your debit/credit card data. The Maxabid website incoprporates  technology called \'API\' to connect to our payment processing partner  using industry standard SSL secure encyption. Your debit/credit card  number is passed securely to them without Maxabid storing it and in-turn  they process the order and notify Maxabid of the Users\' payment  transaction. Usage data are personal data that identify the user, as  well as information on the Maxabid services the user utilises, for  example your Maxabid username together with the associated password and  your IP address when you purchase items or bids (or win an auction). We  collect these data (but not exclusively) to make it possible for you to  use Maxabid more effectively and for us to target out marketing more  effectively. The data is stored only for as long as it is necessary to  process items or bids you have bought and auctions you have won.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>3. Use of the Data</strong></span></p>
<p style=\"text-align: justify;\">Personal  data such as name, address, etc. are used to process item shipment  after an auction is won. We ask for your e-mail address so that we can  stay in contact with you by e-mail. We also use your personal data to  communicate with you regarding orders, to process auctions, marketing  retentions and to process other transactions. Similarly, we use your  data in order to inform you of marketing promotions or to enable third  parties authorized by us to perform technical, logistical, or other  services. All data is stored securely on our hosting partners servers.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>4. E-mail and SMS Newsletters</strong></span></p>
<p style=\"text-align: justify;\">When  you register with Maxabid, you agree to receive e-mail and SMS  newsletters and/or notifications. We will use this method to inform you  of new functions or special promotions on our website. You can at any  time opt-out of these by changing your settings by logging in and going  to the \"My Account\" page.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>5. Forwarding Personal Data to Third Parties</strong></span></p>
<p style=\"text-align: justify;\">Your  personal information may be passed on to third parties only if this is  necessary to process a payment transaction or a business transaction  with our partners. Essentially these are respectable businesses that  strictly comply with applicable data protection regulations. Maxabid  will not release any personal information on you or forward this to  third parties insofar as you have not given your express consent to do  so, or unless we are required to do so by law. In other situations,  personal data will be passed on only to the extent that we are required  to do so by law. For example, personal data is passed on to the law  enforcement and regulatory authority.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>6. Providing Data within the Corporate Group</strong></span></p>
<p style=\"text-align: justify;\">Maxabid  is a member of Maxiserve Limited. You are consenting to allow your data  to be provided within this corporate group. We assure you that should  data be provided this way, security and confidentiality will be  guaranteed with no change, and that the data will continue to be subject  to the Data Protection Statement above.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>7. Viewing/Changing Personal Data</strong></span></p>
<p style=\"text-align: justify;\">You  can view the information in your Maxabid account at any time and update  it as necessary using your Maxabid user name and your Maxabid password.  Your mobile phone number for activating your account free bids can only  be changed by contacting our Customer Service. Once we are informed, we  will adjust incorrect data accordingly. We will notify you upon request  at any time as to whether and what personal data in respect to your  identity or your pseudonym has been stored.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>8. Revoking Consent/Deleting Your Account</strong></span></p>
<p style=\"text-align: justify;\">You  can revoke your consent to the storage and processing of your personal  data at any time (effective immediately), by contacting us. You can  delete or deactivate your Maxabid account at any time. When you delete  your membership, your personal data is removed from our database unless  we are required to save the data. As a rule it takes four to five  business days until your information is completely removed from all  records.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>9. The Use of Cookies at our Website</strong></span></p>
<p style=\"text-align: justify;\">Maxabid  uses \'cookies\', which are files that are stored on your computer that  can be retrieved to assist in customising your experience with the  online service. The information saved supports the functionality of the  site, for example by keeping track of your visual preferences or  controlling the frequency of \"pop-up\" windows. These cookies are deleted  after one month. You are free to prevent cookies from being saved on  your hard drive by adjusting the corresponding settings in your browser.  It is not necessary to accept cookies in order to use our service.  However, we would like to point out that turning off these settings may  result in limited functionality and slower page loading times as almost  all of the images and graphics on the Maxabid website are cached.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>10. Security Measures</strong></span></p>
<p style=\"text-align: justify;\">The  information in your Maxabid account and in your Maxabid profile is  password protected, so that only you have access to this personal  information. Please note that you are not permitted to provide your  password to anyone else. Maxabid will never ask for your password in  e-mails that you receive unexpectedly or that you did not request.  Please remember to log out of your account and to close your internet  browser window when you leave the Maxabid site, this is especially  important if you use a PC in public locations. At present, this is the  safest way to ensure that no one has access to your personal  information. We assume no liability for the abuse of login data and  passwords used.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>11. Protection from Web Crawlers or Spam</strong></span></p>
<p style=\"text-align: justify;\">Maxabid  assures you that we will use your e-mail address with your express  consent only for the purposes stated in this Privacy Policy and  associated Terms and Conditions. We will not sell your e-mail address to  third parties, and we will prevent your e-mail address from being  recorded by \"web crawlers\" or \"web spiders\" to the best of our ability.  If you believe that your e-mail address has been recorded in this way,  please let us know immediately.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>12. Data Protection Guidelines Changes</strong></span></p>
<p style=\"text-align: justify;\">When there are changes to the Data Protection Guidelines, we will indicate this on the Maxabid website.</p>
<p style=\"text-align: justify;\"><span style=\"color: #993300;\"><strong>13. Amendments and Updates To The Privacy Policy</strong></span></p>
<p style=\"text-align: justify;\">Maxabid  has the right to revise and amend this Privacy Policy from time to time  in its absolute discretion, but for reasons including but not limited  to changes in market conditions affecting the Maxabid business, changes  in technology, changes in payment methods, changes in relevant laws and  regulatory requirements and changes in the Maxabid system capabilities.  All Users will be subject to the policies and Terms and Conditions  coming in to effect/force at the time that they are updated and&nbsp;  published. Each update will have a corresponding date of policy coming  in to effect in the title page heading. Unless any such changes in  policy or Terms and Conditions is required to be made by law or  governmental authority or Maxabid notifies its Users of the changes to  such policies or Terms and Conditions it will be assumed that all Users  have accepted the updates.</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('6', 'Home Page', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://www.maxabid.co.uk/images/index_page_image_005.gif\" border=\"0\" alt=\"Home Image\" title=\"Home Image\" width=\"880\" height=\"352\" /></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('7', 'Affiliates', '<p>Add the Affiliate description here</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('8', 'Promotions', '<p>Add the promotions description here</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('11', 'Contact us', '<p>Contact us description goes here</p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('12', 'Quick Link 1', '<p><img src=\"images/featured1.png\" border=\"0\" alt=\"Featured1\" title=\"Featured1\" width=\"140\" height=\"140\" /></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('13', 'Quick Link 2', '<p><img src=\"images/featured2.png\" border=\"0\" alt=\"Featured2\" title=\"Featured2\" width=\"140\" height=\"140\" /></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('14', 'Quick Link 3', '<p><a href=\"http://www.maxabid.co.uk/auctions-featured-items.html\"><img src=\"images/featured3.png\" border=\"0\" alt=\"View Auctions\" title=\"View Live Auctions\" width=\"140\" height=\"140\" /></a></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('15', 'Quick Link 4', '<p><a href=\"http://www.maxabid.co.uk/purchase-bids.html\"><img src=\"images/featured4.png\" border=\"0\" alt=\"Purchase Bids\" title=\"Purchase Bid Tokens\" width=\"140\" height=\"140\" /></a></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('16', 'Quick Link 5', '<p><a href=\"http://www.maxabid.co.uk/charities.html\"><img src=\"images/featured5.png\" border=\"0\" alt=\"Charities\" title=\"How Maxabid Helps Charities\" width=\"140\" height=\"140\" /></a></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('17', 'Quick Link 6', '<p><a href=\"http://www.maxabid.co.uk/social_networks.html\" title=\"wwwwwwwww\"><img src=\"images/featured6.png\" border=\"0\" alt=\"Follow Maxabid On Social Networks\" title=\"Follow Maxabid On Social Networks\" width=\"140\" height=\"140\" /></a></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('18', 'Corporate', '<p>Corporate Information</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('20', 'Shipping & Returns', '<p>Shipping &amp; Returns</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('21', 'Tracking Returns', '<p>Tracking Returns</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('22', 'Auction Formats', '<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Maxabid offer a variety of auction style and formats to bring a new unique user experience to you.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Most Maxabid auctions are designed so users could win in more than one way. For example, if an auction has a required number of bids before ending then the auction will end after that number of bids has been reached. However, if the required numbers of bids are not reached the auction would still end if the expiry date/time is reached therefore the current winning bidder would end up winning the auction.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Other auctions will have no expiry date/time, instead a timer/countdown will trigger when the first bid has been submitted. When it reaches zero the auction will end </span><span style=\"color: #000000;\">therefore the current winning bidder would end up winning the auction.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Some auctions have a timer/countdown along with an expiry date/time. These auctions might not trigger the timer/countdown when the first bid is placed. It could be that there is a pre-determined set of bids needed to be submitted before the timer/countdown will trigger but if the expiry date/time is reached before the required number of bids to be submitted for the timer/countdown to be triggered, the expiry date/time will end the auction therefore </span><span style=\"color: #000000;\">the current winning bidder would end up winning the auction.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">The auctions have been set up in this manner because it adds to the user experience providing a new and enjoyable way to submit bids rather than the boring way nearly all other penny auction websites operate!</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Once your bid is submitted, the stated number of bids will be deducted from your balance and in the event your bid makes you the current winning bidder this will be displayed in the auction.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Auction information that is not displayed in the actual auction thumb can be seen by clicking the information icon in the top right of each individual auction thumb. All users are advised to always check the auction information by clicking the information icon before submitting bids because (as per the Terms and Conditions) the information could change.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Auctions that are in Coming Soon format will go in to live mode after a live auction has ended. Once a Coming Soon auction becomes live it usually (by default) will expire after 28 days but most auctions will expire after the timer reaches zero or in the case of Lowest Unique Bid auctions after the required number of bids have been placed.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">In order to make the&nbsp;auction bidding&nbsp;experience&nbsp;more competitive Maxabid may from time to time extend the expiry date/time of auctions that have no timer/coundown if the items are either of a high value or are being auctioned on a promotional basis or are being auctioned in conjuction with a tradng partner. </span></p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&nbsp;</span></p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p style=\"text-align: center;\">&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #cc99ff;\"><span style=\"color: #ffffff; font-size: small; background-color: #333399;\"><strong>&nbsp;** TIMER PENNY BID **&nbsp;</strong></span><span style=\"color: #ffffff; font-size: small;\"><strong> <br /></strong></span></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">These are the most common auction types from around the web. There are no input fields, just a \'submit\' or \'place bid\' button. When users place a bid they have the stated number of bids deducted from their bids/tokens account balance while during the same time the price of the auction item increases (usually) by a penny and the user is shown as having the current winning bid.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong>How Timer Penny Bid auctions end:</strong> The auction ends in one of two ways.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><span style=\"color: #3366ff;\"><strong><em>First:</em></strong></span><span style=\"color: #000000;\"> Most Timer Penny Bid will have a timer/countdown that will start counting down as soon as the first bid has been submitted by a user. Until the first bid has been submitted there will be a display in the auction stating \"Awaiting First Bid\". </span><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><span style=\"color: #3366ff;\"><strong><em>Second:</em></strong></span> <span style=\"color: #000000;\">Some Timer Penny Bid auctions will have a timer/countdown that will not be activated when the first bid has been submitted. This means that the auction will end when the expiry date/time is reached regardless of the number of bids submitted. However, if the timer/countdown has been activated then the expiry date/time in will become null/void and the auction will end when the timer/countdown reaches zero.</span></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #333399; color: #ffffff; font-size: small;\"><strong>&nbsp;** LOWEST UNIQUE BID **&nbsp; </strong></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\">LUB auctions have an input field allowing users to enter a bid of their choice. The users are aiming to have the lowest unique bid in order to win the auction. Users should click the information icon to check the limits that apply. All LUB auctions will have a minimum and maximum amount users can input, usually this be for an amount between &pound;0.01 and &pound;3.99). The user who wins the auction will have to pay the winning bid amount as well as any post/packaging fees associated. Here is an example for an ended LUB auction:</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 10 users bid:&nbsp; &pound;0.01</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">2 users bid:&nbsp;&nbsp;&nbsp; &pound;0.32<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.87 <span style=\"color: #3366ff;\"><em><strong>ACTUAL </strong></em><em><strong>WINNING BID</strong></em></span></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.88<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">2 users bid:&nbsp;&nbsp;&nbsp; &pound;0.92<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">62 users bid:&nbsp; &pound;1.99</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\">It could be that after a user has submitted a bid, their position when the auction ends will change because other users will be bidding too. Here is the above LUB auction example an hour before it ended:</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">2 users bid: &pound;0.01</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp; &pound;0.32</span><span style=\"background-color: #ffffff; color: #3366ff;\"><em><strong> CURRENT WINNING</strong></em></span><span style=\"color: #3366ff;\"><span style=\"background-color: #ffffff;\"><em><strong> </strong></em></span><span style=\"background-color: #ffffff;\"><em><strong>BID</strong></em></span></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp; &pound;0.87&nbsp;</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp; &pound;0.88<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">1 user bid:&nbsp;&nbsp; &pound;0.92<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"background-color: #ffffff;\">2 users bid:&nbsp; &pound;1.99</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\">As soon as another user submitted a bid of &pound;0.32 it was no longer the lowest \'unique\' bid. The user who submitted &pound;0.87 would have ended up winning the auction in this example because even though &pound;0.88 would also have been a unique bid regardless of whether it was submitted before or after the user submitting the &pound;0.87 bid, the lowest unique bids was it was &pound;0.87 therefore it won the auction.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong>How the LUB auction ends:</strong> The auction ends in one of three ways.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><span style=\"color: #3366ff;\"><strong><em>First:</em></strong></span> <span style=\"color: #000000;\">Most  LUB auctions display the number of bids remaining before the auction  ends. For example, if the auction ends after 99 bids are placed then when the 99th bid is submitted the auction will end. The winning bid will be calculated and displayed with the user name.</span></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">For LUB auction that do not display the number of bids remaining before</span><span style=\"background-color: #ffffff;\">&nbsp;</span> the auction expires, the auction will end when the expiry date/time will be reached.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #3366ff;\"><em><strong><span style=\"background-color: #ffffff;\">Second: </span></strong></em></span><span style=\"background-color: #ffffff;\"><span style=\"color: #000000;\">All  LUB auctions have an expiry date. By default this is usually set to 28 days from  when the auction goes live. The auction will end as soon as the expiry  date/time arrives regardless of how many bids are remaining. For example if the LUB auction is supposed to end when the 99th bid is placed or when the expiry date/time is reached, then the LUB auction will end on the expiry date/time even if just 1 bid had been submitted.</span><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><span style=\"color: #3366ff;\"><em><strong>Third:</strong></em></span> <span style=\"color: #000000;\">Some LUB auctions will have a timer/countdown as well as the expiry date/time. Once the timer is triggered and the countdown commences, the expiry date becomes null/void because the auction will only end when the timer reaches zero regardless of the date/time. If the timer/countdown is not triggered then the auction will expire when the expiry date/time is reached.</span></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">If there is no Lowest Unique when the auction has ended because there have been multiple bids, say for example the expiry date/time has been reached or the required number of bids have been submitted to end the auction, then the person who submitted the lowest bid first will win the auction. Here is an example of an ended Lowest Unique bid auction to show how this works:</span></p>
<p><span style=\"background-color: #ffffff;\"><em><strong><span style=\"color: #3366ff;\">Example Part One: </span></strong></em><span style=\"color: #000000;\">&nbsp;</span></span></p>
<p><span style=\"background-color: #ffffff;\"><span style=\"color: #000000;\">If there is no Lowest Unique bid then we have to find the<em> lowest</em> bid. The user submitting it will be the winner. In this case the lowest bid is &pound;0.01</span></span></p>
<p><span style=\"background-color: #ffffff;\"><span style=\"color: #000000;\"><br /></span></span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\"><strong>&nbsp;&nbsp; 10 users bid:&nbsp; &nbsp; &pound;0.01<em>&nbsp;</em></strong></span><span style=\"background-color: #ffffff; color: #3366ff;\"><em><strong>&nbsp;</strong></em></span><span style=\"color: #3366ff;\"><span style=\"background-color: #ffffff;\"><em>&nbsp;</em></span></span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\">20 users bid:&nbsp;&nbsp; &pound;0.02 <br /></span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\">30 users bid:&nbsp;&nbsp; &pound;0.03<br /></span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\">40 users bid:&nbsp;&nbsp; &pound;0.04<br /></span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\">50 users bid: &nbsp; &pound;0.05</span></p>
<p style=\"text-align: center;\"><span style=\"background-color: #ffffff;\"><br /></span></p>
<p><strong><em><span style=\"background-color: #ffffff; color: #3366ff;\">Example Part Two:</span></em></strong><span style=\"background-color: #ffffff;\"> Now we know that &pound;0.01 is the lowest bid, we need to determine the user who submitted that bid first:</span></p>
<p><span style=\"background-color: #ffffff;\"><br /></span></p>
<p style=\"text-align: center;\"><strong><span style=\"background-color: #ffffff;\">10 users bid:&nbsp;&nbsp; &pound;0.01 </span></strong></p>
<p style=\"text-align: center;\"><strong><span style=\"background-color: #ffffff;\"><br /></span></strong></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>JackSparrow:</strong></em>&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:22:01hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>LoveU: </strong></em>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:22:02hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Steven1000:</strong></em> &nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:42:21hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Wonderland:</strong></em>&nbsp;&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:58:10hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>Tarzan55: &nbsp;</em></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &pound;0.01 submitted on 1st January 2013 at 14:26:33hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>HappyDays8:</em></strong>&nbsp; &nbsp;&nbsp; &nbsp;&pound;0.01 submitted on 1st January 2013 at 14:54:04hrs</span><span style=\"background-color: #ffffff;\">&nbsp;</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>WishingStar:</strong></em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 15:09:43hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Diva99:</strong></em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 16:11:51hrs</span><span style=\"background-color: #ffffff;\"> </span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>TopBidz:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 16:47:23hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>Smile43:</em></strong> &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 18:30:00hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">In this example user <em><strong>JackSparrow</strong></em> wins the auction as he submitted the lowest bid first. The winning bid was submitted one second before user <em><strong>LoveU</strong></em> submitted her bid.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">In some instances there might be a winner when it seems more than one person submitted the lowest bid at the same time but when this is broken down in to micro-seconds, the user who submitted the lowest bid first will win. </span><br /><span style=\"background-color: #ffffff;\">&nbsp;</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">Using the same example above to determine the winner using a different display to show how this works:</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>JackSparrow:</strong></em>&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:22:01hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>LoveU: </strong></em>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:22:01hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Steven1000:</strong></em> &nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:42:21hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Wonderland:</strong></em>&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 13:58:10hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>Tarzan55: &nbsp;</em></strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 14:26:33hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>HappyDays8:</em></strong>&nbsp;&nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 14:54:04hrs</span><span style=\"background-color: #ffffff;\">&nbsp;</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>WishingStar:</strong></em>&nbsp;&nbsp;&nbsp;  &pound;0.01 submitted on 1st January 2013 at 15:09:43hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>Diva99:</strong></em> &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &pound;0.01 submitted on 1st January 2013 at 16:11:51hrs</span><span style=\"background-color: #ffffff;\"> </span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><em><strong>TopBidz: </strong></em>&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 16:47:23hrs</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\"><strong><em>Smile43:</em></strong> &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &pound;0.01 submitted on 1st January 2013 at 18:30:00hrs</span></p>
<p style=\"text-align: justify;\"><br /><span style=\"background-color: #ffffff;\">In this example user <em><strong>JackSparrow</strong></em> still wins the auction as he submitted the lowest bid first even though the display shows user<em> <strong>LoveU</strong></em> submitted her bid at the same time but the display only shows the time down to the second, not to the micro-second. If bids are submitted by users simultaneously then the database will calculate the user who submitted it first even if the auction displays the same time in the bidding process.</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">Most Lowest Unique bid auctions will allow users to make multiple bids so if a user is winning the auction there is nothing stopping the user to place another bid. Once a user has submitted a Lowest Unique the same user cannot submit the same amount a second time, an alert will pop-up to show the user this information if a repeat bid is attempted.<br /></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p style=\"text-align: left;\">&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #333399; color: #ffffff; font-size: small;\"><strong>&nbsp;** BUNDLES **&nbsp; </strong></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">These auctions are designed so that there can be more than one item to be won per auction. For example a user could win more than one item with one bid but more than one user cannot win this auction at the same time, so winner takes it all. <span style=\"background-color: #ffffff;\">Here is an example of a Bundles Lowest Unique auction with two items:</span></span></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">1st Item:</span></em></strong> &pound;500 Cash <br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">2nd Item:</span></em></strong> &pound;25 Argos Gift Voucher<br /></span></p>
<p style=\"text-align: justify;\"><br /><span style=\"background-color: #ffffff; color: #000000;\">Here is an example for a Bundles Timer Penny auction with three items. Again, winner takes it all as there can only be one winner per auction:</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">1st Item:</span></em></strong> Brand New Mini Cooper<br /></span></p>
<p><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">2nd Item:</span></em></strong> All Expenses Paid 14 Days Holiday For Two To Sydney, Australia (including flights, hotels and transport)<br /></span></p>
<p><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">3rd Item:</span></em></strong> 750 Free Bids</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Bundles auction can be in Lowest Unique Bid or Timer Penny Auction format.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #333399; color: #ffffff; font-size: small;\"><strong>&nbsp;** MULTIPLE WINNER **&nbsp; </strong></span></p>
<p style=\"text-align: justify;\">These auctions can be Lowest Unique Bid or Penny Bidding Auction format and can be in Bundles format too.</p>
<p style=\"text-align: justify;\">The  Multiple Winner auctions are designed so that there can be up to three  individual winners per auction for up to three individual items up for  auction. All current winning bids will shown along with their  respective current winning auction.</p>
<p style=\"text-align: justify;\"><br /><span style=\"background-color: #ffffff;\">Here is an example for a Multiple Winner ended Timer Penny auction with two items:</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">1st Item:</span></em></strong> </span><span style=\"background-color: #ffffff;\">&pound;500 Cash (Winning Bid: </span><span style=\"background-color: #ffffff;\"><em>Captain123</em></span><span style=\"background-color: #ffffff;\">)</span><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">2nd Item:</span></em></strong> </span><span style=\"background-color: #ffffff;\">&pound;25 Argos Gift Voucher (Winning Bid:</span><span style=\"background-color: #ffffff;\"><em> LadyLuck1</em>)<br /></span></p>
<p style=\"text-align: justify;\"><br /><span style=\"background-color: #ffffff;\">Here is an example of a Multiple Winner Lowest Unique Bid auction with three items:</span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">1st Item:</span></em></strong> </span><span style=\"background-color: #ffffff;\">Brand New Fiat 500L </span><span style=\"background-color: #ffffff;\">(Winning Bid:<em> JackSparrow </em>&pound;0.44)</span></p>
<p><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">2nd Item:</span></em></strong> </span><span style=\"background-color: #ffffff;\">1000 Bids </span><span style=\"background-color: #ffffff;\">(Winning Bid: <em>19MissYa </em>&pound;2.87)</span></p>
<p><span style=\"background-color: #ffffff; color: #000000;\"><strong><em><span style=\"color: #3366ff;\">3rd Item:</span></em></strong> </span><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">50 Cash </span><span style=\"background-color: #ffffff;\">(Winning Bid:<em> JackSparrow</em> &pound;2.91)</span></span></p>
<p><span style=\"background-color: #ffffff;\"><br /></span></p>
<p style=\"text-align: justify;\">In the above Lowest Unique bids example, user <strong><em>JackSparrow</em></strong> has placed two of three Lowest Unique bids therefore he has won two items. The Lowest Unique bid would win the first item. The second Lowest Unique bid would win the second place item and the third Lowest Unique bid would win the third place item.</p>
<p style=\"text-align: justify;\">In both Lowest Unique bid and Timer Penny auction formats consisting of Mulitple Winners, it is possible for one user to win more than one (or all) items listed for that auction.</p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></p>
<p style=\"text-align: center;\"><br /><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #333399; color: #ffffff; font-size: small;\"><strong>&nbsp;** TIMERS / COUNTDOWNS **&nbsp; </strong></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\">These can be in Lowest Unique Bid or Standard Penny Auction format and can be in Bundle or Multiple Winner format. When a timer/countdown has been triggered, the expiry date/time becomes null/void and the auction will continue to run until the timer reaches zero.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">In some cases auctions will be set up to offer a more competitive style of play. </span><span style=\"background-color: #ffffff;\">Here is an example of an auction that has varying timers which are displayed upon refreshing when a bid is submitted. The auction format is based on the total number of bids that have been submitted (not on the number of bids submitted by an individual user):</span></span></p>
<p style=\"text-align: justify;\">&nbsp;</p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"color: #3366ff;\"><em><strong><span style=\"background-color: #ffffff;\">Total Bids Placed: </span></strong></em></span><span style=\"background-color: #ffffff; color: #000000;\">1 to 14 = 59 second timer</span><em><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">&nbsp;</span></span></em></p>
<p><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"color: #3366ff;\"><em><strong><span style=\"background-color: #ffffff;\">Total Bids Placed: </span></strong></em></span><span style=\"background-color: #ffffff; color: #000000;\">15 to 94 = 30 second timer</span></p>
<p><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"color: #3366ff;\"><em><strong><span style=\"background-color: #ffffff;\">Total Bids Placed: </span></strong></em></span><span style=\"background-color: #ffffff; color: #000000;\">95 to 172 = 10 second timer</span></p>
<p><span style=\"background-color: #ffffff;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style=\"color: #3366ff;\"><em><strong><span style=\"background-color: #ffffff;\">Total Bids Placed: </span></strong></em></span><span style=\"background-color: #ffffff; color: #000000;\">173 until auction ends = 5 second timer</span></p>
<p><span style=\"background-color: #ffffff; color: #000000;\"><br /></span></p>
<p style=\"text-align: justify;\"><span style=\"background-color: #ffffff; color: #000000;\">In the above  example the timer/countdown has been set up to count down from 59  seconds from the first bid until the 14th bid is submitted. Once the 15th bid has been submitted the timer/countdown drops to 30 seconds per refresh, whereas when the 95th bid is placed the timer/countdown decreases to a refresh rate of 10  seconds. After the 173rd bid has been submitted the timer refreshes at a rate of 5 seconds per bid submitted and it will do so until the auction ends. </span><span style=\"color: #000000;\">In this case the number of bids submitted that determine the change in timer/countdown patterns will not be displayed.</span></p>
<p>&nbsp;<span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&nbsp;</span></p>
<p style=\"text-align: center;\"><span style=\"color: #c0c0c0;\">&lt;&lt;-----------&gt;&gt;</span></p>
<p>&nbsp;</p>
<p style=\"text-align: left;\"><span style=\"background-color: #333399; color: #ffffff; font-size: small;\"><strong>&nbsp;** PAUSED AUCTIONS **&nbsp; </strong></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">Some auctions with timers/countdowns will be paused for a short time. This will usually be between 11pm until 9am the following day (approximately 10 hours).</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\">For example, if  there was an auction with a fixed 30 second timer/countdown which refreshed every time a bid was submitted but there were just 8 seconds remaining on the timer/countdown at 11pm which forced the auction to pause, then the auction would resumed at 9am the following day  the current winning bid/bidder but the auction would display and the timer/countdown will be reset to 30 seconds again.</span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"color: #000000;\"><span style=\"background-color: #ffffff;\">Users are advised to click the </span>information icon in the auction to see the full auction details before submitting any bids as well as refer to the Terms and Conditions of using this site.</span></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"color: #000000;\">&nbsp;</span></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #000000;\"><span style=\"color: #000000;\">&nbsp;</span></span></p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('23', 'Common Questions', '<p>Common Questions</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('24', 'Points Program', '<ol>
<li><a>What is the Reward Point Program?</a><br />
<div id=\"answer_q1\" class=\"pointFaq\" style=\"display: block;\">To thank  you all for your support and to offer future incentives to you we wanted  to give something back, this is why we have launched this great Reward  Point Program. <br /><br />Our Reward Point Program is as simple as it sounds.  While  shopping at Maxabid you will earn Shopping Points for the money you  spend. <br />Once earned, you\'ll be able to use those points to pay for future purchases at  Maxabid. <br /><br />The Reward Point Program began on Tuesday 28 December, 2010 . All purchases made after that date will earn points.</div>
</li>
<li><a>How does the Program work?</a><br />
<div id=\"answer_q2\" class=\"pointFaq\" style=\"display: block;\">When an order is placed, the total amount<small><span style=\"color: #ff6633;\">*</span></small> of the order will be used to calculate the amount of points earned. These points are added to your Shopping Points account as pending points. <br />All pending points are listed in your <a href=\"../maxabid/my_points.html\"> <span style=\"text-decoration: underline;\">Shopping Points account </span></a> and will stay there until approved/confirmed by Maxabid. <br /><br />Once any pending points have been approved, they will be  released and your account will be credited with the value of those  points.  Ready for you to spend on whatever you want. <br /> <br />You must login to your account in order to view the status of your points. <br /><br />During the checkout procces you\'ll be able to pay for your order with your points balance.
<p><span style=\"color: #ff6633;\">*</span> in most cases shipping fees and taxes excluded. See refered FAQ for more details.</p>
</div>
</li>
<li><a>Points and Values</a><br /> Currently, for every &pound;1.00 spent at Maxabid you\'ll earn 1  <br />For example:<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Product Cost:</strong>&nbsp; &pound;100.00<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>Value of Points Earned:</strong>&nbsp; &pound;1.00<br /><br /> Please note, we reserve the right to make changes to the above rate at  any time without prior notice.  The rate shown here will always be  current.
<p align=\"right\"><small>Last updated: Saturday 03 April, 2010</small></p>
</li>
<li><a>Redeeming Shopping Points</a><br />
<div id=\"answer_q4\" class=\"pointFaq\" style=\"display: block;\">If you have a balance in your Shopping Points Account, you can use those points to pay for purchases made at Maxabid. <br />During the checkout proccess, on the same page that you select a  payment method, there will be a box to enter the amount of points you  wish to redeem.  Enter the amount of points you would like to spend or  tick the box to use all available points. Please note, you will still have to select another payment method if  there  is not enough in your Shopping Points Account to cover the cost of your  purchase.  <br />Continue the checkout procedure and at the confirmation page you\'ll  notice that the value of the points redeemed will have been credited  towards your order.  Once you confirm your order, your Shopping Points  account will be updated and the points used deducted from your balance. <br />Note that any purchase made by redeeming points will only be  rewarded with additional points for the amount spent other then points.</div>
</li>
<li><a>Minimum Points Required</a><br />
<div id=\"answer_q5\" class=\"pointFaq\" style=\"display: block;\">Currently, a minimum balance of <strong>50</strong> points <strong>(&pound;0.50)</strong> is required before you can redeem them. 	<br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Friday 01 January, 2010</small></p>
</div>
</li>
<li><a>Minimum Purchase Amount Required</a><br />
<div id=\"answer_q6\" class=\"pointFaq\" style=\"display: block;\">Currently, a minimum of <strong>&pound;25.00</strong> in total (per purchase) is required before any Points Redemptions can take place. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Tuesday 28 December, 2010</small></p>
</div>
</li>
<li><a>Maximum Points Redemptions allowed per order</a><br />
<div id=\"answer_q7\" class=\"pointFaq\" style=\"display: block;\">A maximum of <strong>1,000</strong> points <strong>(&pound;10.00)</strong> is allowed to redeem per order. <br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Tuesday 28 December, 2010</small></p>
</div>
</li>
<li><a>Will I earn points for shipping fees?</a><br />
<div id=\"answer_q8\" class=\"pointFaq\" style=\"display: block;\">No. When calculating the amount of points earned, the shipping fees are excluded.
<p align=\"right\"><small>Last updated Saturday 27 December, 2008</small></p>
</div>
</li>
<li><a>Will I earn points for tax fees?</a><br />
<div id=\"answer_q9\" class=\"pointFaq\" style=\"display: block;\">No. When calculating the amount of points earned, the taxes are excluded. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Thursday 22 April, 2010</small></p>
</div>
</li>
<li><a>Will I earn points for discounted products?</a><br />
<div id=\"answer_q10\" class=\"pointFaq\" style=\"display: block;\">No. When calculating the amount of points earned, all items which have been discounted are excluded. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Tuesday 28 December, 2010</small></p>
</div>
</li>
<li><a>Will I earn points when purchases paid with points?</a><br />
<div id=\"answer_q11\" class=\"pointFaq\" style=\"display: block;\">No. When calculating the amount of points earned. Any purchase made by redeeming points are excluded. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Saturday 27 December, 2008</small></p>
</div>
</li>
<li><a>Earning Referral Points</a><br />
<div id=\"answer_q12\" class=\"pointFaq\" style=\"display: block;\"><em>\"Word-of-mouth\" advertising is the most powerful form of advertising there is.</em> <br />Referral Points is based on the idea that we should both benefit from your referrals. 	<br />When referred friend place an order, during the checkout procces on  the same page that you select a payment method there will be a box to  enter a Referral code . 	Your Referral code is your registered email address with us. 	<br />When we receive your referred friends completed and approved order, we will reward your Points account with <strong>50</strong> points . 	<br />The more first time orders we receive from your referrals, the more reward points you will receive.
<p align=\"right\"><small>Last updated: Thursday 22 April, 2010</small></p>
</div>
</li>
<li><a>Earning Points While writing a Products Review</a><br />
<div id=\"answer_q13\" class=\"pointFaq\" style=\"display: block;\">Currently this feature is disabled.
<p align=\"right\"><small>Last updated: Thursday 22 April, 2010</small></p>
</div>
</li>
<li><a>Products Restrictions</a><br />
<div id=\"answer_q14\" class=\"pointFaq\" style=\"display: block;\">Currently, no restrictions apply to what items may be purchased using your points balance. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Sunday 27 December, 2009</small></p>
</div>
</li>
<li><a>Products on sale Restrictions</a><br />
<div id=\"answer_q15\" class=\"pointFaq\" style=\"display: block;\">Currently, no restrictions apply to the kind of items which may be purchased using your points balance. 	<br /><br />We strongly advise you to check this page often as we may make changes to this policy.
<p align=\"right\"><small>Last updated: Friday 01 January, 2010</small></p>
</div>
</li>
<li><a>Conditions of Use</a><br />
<div id=\"answer_q16\" class=\"pointFaq\" style=\"display: block;\">
<ul>
<li>Shopping Points are only available to registered Maxabid member\'s.</li>
<li>Shopping Points Reward can only be collected and used with online purchases. and are only validated at Maxabid.</li>
<li>Points are non-refundable and can\'t be transferred between member\'s.</li>
<li>Shopping Points are non-transferable or exchangeable for cash under any circumstances.</li>
<li>Shopping Points will not be refunded for any cancelled order.</li>
<li>When buying with Points,you will still have to select another  payment method if there is not enough in your Shopping Points Account to  cover the cost of your purchase.</li>
<li>When calculating the amount of points earned. shipping fees and  taxes are excluded(unless other.see refered FAQ for more details).</li>
</ul>
Please note, we reserve the right to make changes to this policy at any time without prior notice or liability.</div>
</li>
<li><a>When Problems Occur</a><br />
<div id=\"answer_q17\" class=\"pointFaq\" style=\"display: block;\">For any queries regarding our Reward Point Program, please <a href=\"../maxabid/contact_us.html\"> <span style=\"text-decoration: underline;\">contact us </span></a>.  Make sure you provide as much information as possible in the e-mail.</div>
</li>
</ol>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('25', 'Gift Vouchers', '<ol>
<li><a>Purchasing Gift Vouchers</a><br />
<div id=\"answer_q1\" style=\"background-color: #3399ef; display: block; position: relative; padding: 4px; margin: 2px; border-left: 5px solid #0c2582; text-align: justify;\">Gift Vouchers are purchased just like any other item in our store. You can    pay for them using the stores standard payment method(s).   Once purchased the value of the Gift Voucher will be added to your own personal    Gift Voucher Account. If you have funds in your Gift Voucher Account, you will    notice that the amount now shows in the Shopping Cart box, and also provides a    link to a page where you can send the Gift Voucher to someone via email.</div>
</li>
<li><a>How to send Gift vouchers</a><br />
<div id=\"answer_q2\" style=\"background-color: #3399ef; display: block; position: relative; padding: 4px; margin: 2px; border-left: 5px solid #0c2582; text-align: justify;\">To send a Gift Voucher you need to go to our Send Gift Voucher Page. You can    find the link to this page in the Shopping Cart Box in the right hand column of each page.   When you send a Gift Voucher, you need to specify the following:   The name of the person you are sending the Gift Voucher too.   The email address of the person you are sending the Gift Voucher too.   The amount you want to send. (Note that you don\'t have to send the full amount that    is in your Gift Voucher Account.)   A short message which will apear in the email.   Please ensure that you have entered all of the information correctly, although    you will be given the opportunity to change this as much as you want before    the email is actually sent.</div>
</li>
<li><a>Buying with Gift Vouchers</a><br />
<div id=\"answer_q3\" style=\"background-color: #3399ef; display: block; position: relative; padding: 4px; margin: 2px; border-left: 5px solid #0c2582; text-align: justify;\">If you have funds in your Gift Voucher Account, you can use those funds to    purchase other items in out store. At the checkout stage, an extra box will    appear. Ticking this box will apply those funds in your Gift Voucher Account.    Please note, you will still have to select another payment method if there    is not enough in your Gift Voucher Account to cover the cost of your purchase.    If you have more funds in your Gift Voucher Account than the total cost of    your purchase the balance will bel left in you Gift Voucher Account for the    future.</div>
</li>
<li><a>Redeeming Gift Vouchers</a><br />
<div id=\"answer_q4\" style=\"background-color: #3399ef; display: block; position: relative; padding: 4px; margin: 2px; border-left: 5px solid #0c2582; text-align: justify;\">If you receive a Gift Voucher by email it will contain details of who sent    you the Gift Voucher, along with possibly a short message from them. The Email    will also contain a link to redeem the voucher. You will need to Login or Create an Account before   you can redeem the Gift Voucher.  There are various ways you can redeem the voucher:<br /> 1. By clicking on the link contained within the email for this express purpose.    This will take you to the store\'s Redeem Voucher page. You will the be requested    to create an account or Login, before the Gift Voucher is validated and placed in your    Gift Voucher Account ready for you to spend it on whatever you want.<br /> 2. During the checkout procces, on the same page that you select a payment method  there will be a tick box and button to redeem your Gift Voucher balance against that purchase.</div>
</li>
<li><a>When problems occur</a><br />
<div id=\"answer_q5\" style=\"background-color: #3399ef; display: block; position: relative; padding: 4px; margin: 2px; border-left: 5px solid #0c2582; text-align: justify;\">For any queries regarding the Gift Voucher System, please contact the store    by email at test@asymmetrics.com. Please make sure you give    as much information as possible in the email.</div>
</li>
</ol>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('26', 'Bidding Questions', '<p>Bidding Questions</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('27', 'Winning Questions', '<p>Winning Questions</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('28', 'Payment Questions', '<p>Payment Questions</p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('29', 'About Maxabid', '<p style=\"text-align: justify;\">Hello, I am Ally and I would like to welcome you to Maxabid!</p>
<p style=\"text-align: justify;\">Maxabid is penny auction website that offers users the opportunity to win auctions at unbelievably low prices. Users have to purchase bids in advance and then use those bids from their account balance to submit bids in order to try and win the auctioned items.</p>
<p style=\"text-align: justify;\">Whereas most penny auction websites offer the opportunity to win  auctions in a single format that consists of customers pressing the  \'bid\' button which would increase the price of the item by a penny,  Maxabid has a wide array of auction formats to offer a more enjoyable style of the auction bidding processes.</p>
<p style=\"text-align: justify;\">For more details please see our <em><a href=\"http://www.maxabid.co.uk/auction_formats.html\">Auction Formats</a></em> page.</p>
<p style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>&nbsp;</strong></span></p>
<p style=\"text-align: justify;\"><span style=\"color: #800000;\"><strong>How Maxabid started: </strong></span>After watching the rise of the penny auctions website market in 2008, I decided to lay out the plans for what would become the Maxabid website and brand.</p>
<p style=\"text-align: justify;\">I was inspired to work on Maxabid after looking at the now defunct <em>Swoopo.com</em> website. As I researched the penny auction website market I noticed the growing number of websites that began launching. I knew I could do much better than all of these sites especially as far as different types of auction formats was concerned so I started to design the layout, colour scheme and other essentials. Most of the site was created using open source software but I spent numerous hours on designing and re-designing. Not being the best programmer in the world, I decided to outsource the bidding process part of the website.</p>
<p style=\"text-align: justify;\">As the series of designs and re-designs continued there were delays in the completion of the project because of programming and domestic related issues therefore I decided to put the project on hold. In 2010 I decided to give it another go and began to concentrate on the project again. I contacted a colleague of mine from the open source software forums that the bulk of the Maxabid website was based on and asked him to complete the auction bidding process part of the project for me. He said \"yes\" so I outsourced that part to him. After looking at the source code he pointed out it was getting old so I asked him to keep the look of the website but re-code the whole thing. (Thanks Mark!)</p>
<p style=\"text-align: justify;\">In 2012 after more delays because of programming and domestic related reasons the project was put on hold again. In March 2013 Mark and I began working on the project again and this time we completed enough of it to start testing for bugs with a view to taking it live as soon as possible.</p>
<p style=\"text-align: justify;\">As there was already a massive delay of at least 4 years since the  Maxabid website should have been launched, I took the decision to take  the site live using just the LUB format while always working behind the  scenes with Mark to get the other auction formats perfected before  launching them. The Maxabid website was finally ready to go live using the Lowest Unique bids format but these LUB auctions could be in Bundles and Multiple Winners format too. They could also be with or without countdown timers therefore providing a completely different auction experience for our users.</p>
<p style=\"text-align: justify;\">Maxabid will be launching dedicated websites in Germany, France, Italy, Spain&nbsp; and Ireland before the end of 2014. We will keep you posted as things develop :)</p>
<p style=\"text-align: justify;\">\"Maxabid\" is a trading name and trademark of Maxiserve Limited, <span>Office 8, 10 Buckhurst Road</span><span>, Bexhill-On-Sea</span><span>, East Sussex</span><span>, TN40 1QF</span><span>, United Kingdom</span></p>', '1');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('30', 'create_account.php', '<p style=\"text-align: center;\"><strong>Enter the create account information here </strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>
<p style=\"text-align: center;\"><strong>dfsdfsdfsdfsdddddddddddddddddddddddddddx</strong></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('31', 'logoff.php', '<p>Enter the logoff information or multimedia content here</p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('32', 'checkout_payment.php', '<p>Enter the content for the Checkout Payment Page here</p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('33', 'checkout_confirmation.php', '<p>Enter the Checkout Confirmation information or multimedia content here</p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('34', 'index.php', '<p>Enter the Categories Content or Information here Enter the the the the the the the the Categories Content or Information here</p>
<p><span style=\"font-size: small;\"><strong>Enter the Categories Content or Information here Enter the the the the the the the the Categories Content or Information here</strong></span></p>
<p><span style=\"font-size: x-large;\"><strong><span style=\"color: #00ff00;\">Enter the Categories Content or Information here Enter the the the the the the the the Categories Content or Information here</span></strong></span></p>', '0');
insert into gtext (gtext_id, gtext_title, gtext_description, status) values ('35', 'Footer Area', '<div class=\"totalsize balancer\">
<div class=\"bounder vpad\">
<div class=\"floater hpadder\">
<table style=\"background-color: #cccccc; width: 915px; height: 2px; margin-left: auto; margin-right: auto;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
</table>
</div>
</div>
</div>', '0');
drop table if exists gtext_to_display;
create table gtext_to_display (
  abstract_zone_id int(11) not null ,
  gtext_id int(11) not null ,
  sequence_order int(3) ,
  PRIMARY KEY (abstract_zone_id, gtext_id)
);

insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('7', '1', '2');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('7', '7', '3');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('7', '8', '4');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('8', '4', '2');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('8', '5', '3');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('8', '7', '4');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('8', '18', '1');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('9', '14', '3');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('9', '15', '4');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('9', '16', '5');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('9', '17', '6');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '2', '1');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '4', '3');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '5', '4');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '20', '2');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '21', '5');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '22', '6');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('10', '29', NULL);
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '1', NULL);
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '23', '1');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '24', '2');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '25', '3');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '26', '4');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '27', '5');
insert into gtext_to_display (abstract_zone_id, gtext_id, sequence_order) values ('11', '28', '6');
drop table if exists gtext_to_products;
create table gtext_to_products (
  abstract_zone_id int(11) not null ,
  gtext_id int(11) not null ,
  products_id int(11) default '0' not null ,
  sort_order int(3) ,
  PRIMARY KEY (abstract_zone_id, gtext_id, products_id)
);

insert into gtext_to_products (abstract_zone_id, gtext_id, products_id, sort_order) values ('9', '12', '96', NULL);
insert into gtext_to_products (abstract_zone_id, gtext_id, products_id, sort_order) values ('9', '17', '93', NULL);
insert into gtext_to_products (abstract_zone_id, gtext_id, products_id, sort_order) values ('9', '17', '94', NULL);
drop table if exists helpdesk_attachments;
create table helpdesk_attachments (
  auto_id int(11) not null auto_increment,
  helpdesk_entries_id int(11) not null ,
  ticket varchar(7) not null ,
  attachment varchar(255) not null ,
  PRIMARY KEY (auto_id),
  KEY idx_helpdesk_entries_id (helpdesk_entries_id),
  KEY idx_ticket (ticket)
);

drop table if exists helpdesk_departments;
create table helpdesk_departments (
  department_id int(11) not null auto_increment,
  title varchar(32) not null ,
  email_address varchar(96) not null ,
  name varchar(32) not null ,
  password varchar(32) not null ,
  front tinyint(1) default '1' not null ,
  PRIMARY KEY (department_id)
);

insert into helpdesk_departments (department_id, title, email_address, name, password, front) values ('1', 'Support', 'support@maxabid.co.uk', 'support@maxabid.co.uk', 'h5Wyy1Pe4FG45', '1');
insert into helpdesk_departments (department_id, title, email_address, name, password, front) values ('2', 'Sales', 'sales@maxabid.co.uk', 'sales@maxabid.co.uk', 'h5Wyy1Pe4FG45', '1');
insert into helpdesk_departments (department_id, title, email_address, name, password, front) values ('3', 'Aleem', 'ally@maxabid.co.uk', 'ally@maxabid.co.uk', 'h5Wyy1Pe4FG45', '0');
drop table if exists helpdesk_entries;
create table helpdesk_entries (
  helpdesk_entries_id int(11) not null auto_increment,
  ticket varchar(7) not null ,
  parent_id int(11) default '0' not null ,
  message_id varchar(100) not null ,
  ip_address varchar(15) not null ,
  host varchar(50) not null ,
  datestamp_local datetime default '0000-00-00 00:00:00' not null ,
  datestamp datetime default '0000-00-00 00:00:00' not null ,
  receiver varchar(50) not null ,
  receiver_email_address varchar(96) not null ,
  sender varchar(50) not null ,
  email_address varchar(96) not null ,
  subject varchar(255) not null ,
  body text not null ,
  entry_read char(1) default '0' ,
  attachment varchar(255) ,
  PRIMARY KEY (helpdesk_entries_id),
  KEY idx_helpdesk_entries_ticket (ticket)
);

insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('1', 'D7BRULU', '0', '<78B26AF7216B491AA6A0FF25B65459A0@obsolete>', '208.85.4.6', 'server1.asymmetrics.com', '2012-01-18 10:58:08', '2012-01-18 11:57:59', 'support@maxabid.co.uk', 'support@maxabid.co.uk', 'Asymmetrics Test', 'test@asymmetrics.com', 'test message', 'test message', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('2', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:09:49', '2012-01-18 11:09:49', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test reply message</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('3', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:14:28', '2012-01-18 11:14:28', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>another test message</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('4', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:14:56', '2012-01-18 11:14:56', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test3 message</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('5', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:17:30', '2012-01-18 11:17:30', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('6', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:18:08', '2012-01-18 11:18:08', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('7', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:19:42', '2012-01-18 11:19:42', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('8', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:20:07', '2012-01-18 11:20:07', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('9', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:20:34', '2012-01-18 11:20:34', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('10', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:21:01', '2012-01-18 11:21:01', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('11', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:21:39', '2012-01-18 11:21:39', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('12', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:26:16', '2012-01-18 11:26:16', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('13', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:26:25', '2012-01-18 11:26:25', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('14', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:26:36', '2012-01-18 11:26:36', 'Asymmetrics Test', 'test@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>twq qw qwerewqr qwer qw</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('15', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 11:48:34', '2012-01-18 11:48:34', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test123 dsafd afd asfds ds</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('16', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-18 15:29:57', '2012-01-18 15:29:57', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>tests test</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('17', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 10:47:16', '2012-01-19 10:47:16', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test99</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('18', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 10:57:04', '2012-01-19 10:57:04', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>dsaafds ads</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('19', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 11:04:33', '2012-01-19 11:04:33', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>teste555</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('20', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 11:09:38', '2012-01-19 11:09:38', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test888</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('21', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 11:14:26', '2012-01-19 11:14:26', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test909</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('22', 'D7BRULU', '1', '', '85.17.133.25', 'www.maxabid.co.uk', '2012-01-19 11:17:48', '2012-01-19 11:17:48', 'Asymmetrics Test', 'webmaster@asymmetrics.com', 'Support', 'support@maxabid.co.uk', 'RE: [maxabidD7BRULU] test message', '<p>test654553</p>', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('23', '7FXD6WR', '0', '<BLU151-W40A94AF3C6406E3AB35E09CB3F0@phx.gbl>', '65.55.116.100', 'blu0-omc3-s25.blu0.hotmail.com', '2012-04-17 19:45:37', '2012-04-17 21:43:18', 'support@maxabid.co.uk', 'support@maxabid.co.uk', 'a y', 'aleem786@hotmail.co.uk', 'Heelloo', 'Just testing, This site', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('24', 'ZRE5M27', '0', '<20120417081200.13845.qmail@maxabid.co.uk>', '', '', '2012-04-17 19:45:37', '2012-04-17 10:12:00', 'sales@maxabid.co.uk', 'sales@maxabid.co.uk', 'sdfdsfsdd', 'aleem786@hotmail.co.uk', 'Bids order 123 Enquiry from Maxabid', 'Hi this is a test,

Just checking

Regards

Aleem', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('25', 'NR7Y4BD', '0', '<20120417174503.24968.qmail@maxabid.co.uk>', '', '', '2012-04-17 19:45:37', '2012-04-17 19:45:03', 'sales@maxabid.co.uk', 'sales@maxabid.co.uk', 'asasas', 'aleem786@hotmail.co.uk', 'ppppppppppp Enquiry from Maxabid', 'rtyrt
rtyty

rtyrtyrtyrtyr', '1', NULL);
insert into helpdesk_entries (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read, attachment) values ('26', 'ZRE5M27', '24', '', '85.17.133.63', 'www.maxabid.co.uk', '2012-04-17 19:47:31', '2012-04-17 19:47:31', 'sdfdsfsdd', 'aleem786@hotmail.co.uk', 'Sales', 'sales@maxabid.co.uk', 'RE: [maxabidZRE5M27] Bids order 123 Enquiry from Maxabid', '<p>fsdfs dfsdfsdf</p>
<p>&nbsp;</p>
<p><span style=\"color: #ff9900;\"><strong>rtyrtr</strong></span></p>', '1', NULL);
drop table if exists helpdesk_log;
create table helpdesk_log (
  helpdesk_log_id int(11) not null auto_increment,
  helpdesk_log_name varchar(64) not null ,
  msg_count int(11) not null ,
  access_date datetime not null ,
  PRIMARY KEY (helpdesk_log_id)
);

insert into helpdesk_log (helpdesk_log_id, helpdesk_log_name, msg_count, access_date) values ('19', 'AdminFirstname AdminLastname', '0', '2012-04-04 10:11:34');
insert into helpdesk_log (helpdesk_log_id, helpdesk_log_name, msg_count, access_date) values ('20', 'AdminFirstname AdminLastname', '3', '2012-04-17 19:45:37');
insert into helpdesk_log (helpdesk_log_id, helpdesk_log_name, msg_count, access_date) values ('21', 'AdminFirstname AdminLastname', '0', '2012-04-17 19:48:32');
drop table if exists helpdesk_priorities;
create table helpdesk_priorities (
  priority_id int(11) not null auto_increment,
  languages_id int(11) default '0' not null ,
  title varchar(32) not null ,
  PRIMARY KEY (priority_id, languages_id)
);

insert into helpdesk_priorities (priority_id, languages_id, title) values ('1', '1', 'Medium');
insert into helpdesk_priorities (priority_id, languages_id, title) values ('2', '1', 'High');
insert into helpdesk_priorities (priority_id, languages_id, title) values ('3', '1', 'Low');
drop table if exists helpdesk_statuses;
create table helpdesk_statuses (
  status_id int(11) not null auto_increment,
  languages_id int(11) default '0' not null ,
  title varchar(32) not null ,
  PRIMARY KEY (status_id, languages_id)
);

insert into helpdesk_statuses (status_id, languages_id, title) values ('1', '1', 'Pending');
insert into helpdesk_statuses (status_id, languages_id, title) values ('2', '1', 'Open');
insert into helpdesk_statuses (status_id, languages_id, title) values ('3', '1', 'Closed');
drop table if exists helpdesk_templates;
create table helpdesk_templates (
  template_id int(11) not null auto_increment,
  title varchar(32) not null ,
  template text not null ,
  PRIMARY KEY (template_id)
);

insert into helpdesk_templates (template_id, title, template) values ('1', 'Are you legit store?', 'We have many happy customers and respond to all E-Mails within 24 hours. In short we are here to stay and plan in providing our customers with the best support regarding our orders on cheese and spices');
drop table if exists helpdesk_tickets;
create table helpdesk_tickets (
  ticket varchar(7) not null ,
  department_id int(11) default '0' not null ,
  priority_id int(11) ,
  status_id int(11) ,
  datestamp_last_entry datetime ,
  comment text ,
  datestamp_comment datetime ,
  KEY idx_helpdesk_tickets_ticket (ticket)
);

insert into helpdesk_tickets (ticket, department_id, priority_id, status_id, datestamp_last_entry, comment, datestamp_comment) values ('D7BRULU', '1', '1', '1', '2012-01-19 11:17:48', NULL, NULL);
insert into helpdesk_tickets (ticket, department_id, priority_id, status_id, datestamp_last_entry, comment, datestamp_comment) values ('7FXD6WR', '1', '1', '1', '2012-04-17 19:45:37', NULL, NULL);
insert into helpdesk_tickets (ticket, department_id, priority_id, status_id, datestamp_last_entry, comment, datestamp_comment) values ('ZRE5M27', '2', '1', '1', '2012-04-17 19:47:31', 'wassup', '2012-04-17 19:46:22');
insert into helpdesk_tickets (ticket, department_id, priority_id, status_id, datestamp_last_entry, comment, datestamp_comment) values ('NR7Y4BD', '2', '1', '1', '2012-04-17 19:45:37', NULL, NULL);
drop table if exists languages;
create table languages (
  languages_id int(11) not null auto_increment,
  name varchar(32) not null ,
  code char(2) not null ,
  image varchar(64) ,
  directory varchar(32) ,
  sort_order int(3) ,
  PRIMARY KEY (languages_id),
  KEY IDX_LANGUAGES_NAME (name)
);

insert into languages (languages_id, name, code, image, directory, sort_order) values ('1', 'English', 'en', 'icon.gif', 'english', '1');
drop table if exists manufacturers;
create table manufacturers (
  manufacturers_id int(11) not null auto_increment,
  manufacturers_name varchar(32) not null ,
  manufacturers_image varchar(64) ,
  date_added datetime ,
  last_modified datetime ,
  PRIMARY KEY (manufacturers_id),
  KEY IDX_MANUFACTURERS_NAME (manufacturers_name)
);

drop table if exists manufacturers_info;
create table manufacturers_info (
  manufacturers_id int(11) not null ,
  languages_id int(11) not null ,
  manufacturers_url varchar(255) not null ,
  url_clicked int(5) default '0' not null ,
  date_last_click datetime ,
  PRIMARY KEY (manufacturers_id, languages_id)
);

drop table if exists meta_abstract;
create table meta_abstract (
  abstract_zone_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (abstract_zone_id, language_id)
);

insert into meta_abstract (abstract_zone_id, language_id, meta_title, meta_keywords, meta_text) values ('11', '1', 'frequently asked questions', 'common questions,points program,gift vouchers,bidding questions,winning questions,payment questions', 'Frequently Asked Questions');
drop table if exists meta_auctions;
create table meta_auctions (
  auctions_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (auctions_id, language_id)
);

insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('1', '1', '10 bids 1', '', '');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('2', '1', '100 bids 2', '', '');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('3', '1', 'bundle 1 3', 'unique,offer,for,the,bundle1,product', 'A unique offer for the bundle1 
product');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('4', '1', '500 bids 4', 'nbsp,bids', '&nbsp;500 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('22', '1', 'bundle 1 22', 'bundle1', 'bundle1');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('23', '1', '10 bids 23', 'nbsp,bids', '&nbsp;10 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('24', '1', '1000 bids 24', 'nbsp,bids', '&nbsp;1000 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('25', '1', '500 bids 25', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('26', '1', 'bundle2 26', 'bundle2', 'Bundle2');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('27', '1', '10 bids 27', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('28', '1', '50 bids 28', 'nbsp,bids', '&nbsp;50 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('29', '1', '500 bids 29', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('30', '1', 'bundle2 30', 'bundle2', 'Bundle2');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('31', '1', '1000 bids 31', 'nbsp,bids', '&nbsp;1000 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('32', '1', '10 bids 32', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('33', '1', 'bundle2 33', 'bundle2', 'Bundle2');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('35', '1', '100 bids 35', 'nbsp,bids', '&nbsp;100 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('36', '1', '1000 bids 36', 'nbsp,bids', '&nbsp;1000 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('38', '1', '100 bids 38', 'nbsp,bids', '&nbsp;100 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('39', '1', '1000 bids 39', 'nbsp,bids', '&nbsp;1000 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('40', '1', '50 bids 40', 'nbsp,bids', '&nbsp;50 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('41', '1', '500 bids 41', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('42', '1', '100 bids 42', 'nbsp,bids', '&nbsp;100 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('43', '1', '1000 bids 43', 'nbsp,bids', '&nbsp;1000 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('44', '1', '10 bids 44', 'nbsp,bids', '&nbsp;10 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('45', '1', '10 bids 45', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('46', '1', '500 bids 46', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('47', '1', '1000 bids 47', 'nbsp,bids', '&nbsp;1000 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('48', '1', 'bundle2 48', 'bundle2', 'Bundle2');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('49', '1', '50 bids 49', 'nbsp,bi', '&nbsp;50 Bi d 
s&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('50', '1', '500 bids 50', 'nbsp,bids', '&nbsp;500 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('51', '1', '50 bids 51', 'nbsp,bids', '&nbsp;50 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('53', '1', '100 bids 53', 'nbsp,bids', '&nbsp;100 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('54', '1', 'bundle 1 54', 'bundle1', 'bundle1');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('55', '1', '10 bids 55', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('56', '1', '100 bids 56', 'nbsp,bids', '&nbsp;100 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('57', '1', '50 bids 57', 'nbsp,bids', '&nbsp;50 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('58', '1', 'bundle 1 58', 'bundle1', 'bundle1');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('63', '1', '50 bids 64', 'nbsp,bids', '&nbsp;50 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('64', '1', '10 bids 64', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('69', '1', '10 bids 69', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('70', '1', '100 bids 70', 'nbsp,bids,biggin,up,the,for,all,maxabidders', '&nbsp;100 Bids&nbsp;
&nbsp;
biggin up the bids for all the Maxabidders :)');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('71', '1', '500 bids 71', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('73', '1', '10 bids 73', 'nbsp,bids', '&nbsp;10 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('77', '1', '500 bids 77', 'nbsp,bids', '&nbsp;500 Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('80', '1', 'bundle 1 80', 'bundle1', 'bundle1');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('81', '1', '100 bids 81', 'nbsp,ids', '&nbsp;100 
B ids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('86', '1', '10 bids 86', 'nbsp,bids', '&nbsp;10 
Bids&nbsp;');
insert into meta_auctions (auctions_id, language_id, meta_title, meta_keywords, meta_text) values ('89', '1', '750 bids 89', '', '');
drop table if exists meta_auctions_group;
create table meta_auctions_group (
  auctions_group_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (auctions_group_id, language_id)
);

insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('1', '1', 'featured', 'featured,auctions,description,optional', 'Featured Auctions Description
Optional');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('2', '1', 'lowest unique bid', '', '');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('3', '1', 'countdown timer', '', '');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('4', '1', 'bundle auctions', '', '');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('5', '1', 'multiple winner', '', '');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('6', '1', 'charity', '', '');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('7', '1', 'bidszzzzzzzzzzzzz', 'bids,auctions', 'Bids Auctions');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('8', '1', 'vouchers', 'gift,certificate,and,vouchers,auctions', 'Gift Certificate and Vouchers 
Auctions');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('9', '1', 'for him', 'for,him,auctions', '\"For Him\" Auctions');
insert into meta_auctions_group (auctions_group_id, language_id, meta_title, meta_keywords, meta_text) values ('10', '1', 'for her', 'for,her,auctions', '\"For Her\" Auctions');
drop table if exists meta_categories;
create table meta_categories (
  categories_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (categories_id, language_id)
);

insert into meta_categories (categories_id, language_id, meta_title, meta_keywords, meta_text) values ('8', '1', 'Auction Items', 'Auction Items', 'Auction Items');
insert into meta_categories (categories_id, language_id, meta_title, meta_keywords, meta_text) values ('9', '1', 'Purchase Your Bids Here', 'Purchase Your Bids Here', 'Purchase Your Bids Here');
insert into meta_categories (categories_id, language_id, meta_title, meta_keywords, meta_text) values ('10', '1', 'Bundles', 'Bundles', 'Bundles');
drop table if exists meta_exclude;
create table meta_exclude (
  meta_exclude_key varchar(32) not null ,
  meta_exclude_text varchar(96) not null ,
  meta_exclude_status tinyint(1) default '1' not null ,
  PRIMARY KEY (meta_exclude_key)
);

drop table if exists meta_gtext;
create table meta_gtext (
  gtext_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (gtext_id, language_id)
);

insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('1', '1', 'faq', 'add,the,faqs,here', 'Add the FAQs here');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('2', '1', 'about us', 'about,this,store', 'about this store');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('4', '1', 'privacy notice', 'add,the,privacy,details,here', 'Add the privacy details here');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('5', '1', 'terms conditions', 'terms,of,use', 'terms of use');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('7', '1', 'Affiliates', 'Affiliates', 'Affiliates');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('8', '1', 'Promotions', 'Promotions', 'Promotions');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('14', '1', 'Quick Link 3', 'Quick Link 3', 'Quick Link 3');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('15', '1', 'Quick Link 4', 'Quick Link 4', 'Quick Link 4');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('16', '1', 'quick link 5', 'Quick Link 5', 'Quick Link 5');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('17', '1', 'Quick Link 6', 'Quick Link 6', 'Quick Link 6');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('18', '1', 'corporate', 'corporate,information', 'Corporate Information');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('20', '1', 'Shipping & Returns', 'Shipping & Returns', 'Shipping & Returns');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('21', '1', 'Tracking Returns', 'Tracking Returns', 'Tracking Returns');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('22', '1', 'Auction Formats', 'Auction Formats', 'Auction Formats');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('23', '1', 'Common Questions', 'Common Questions', 'Common Questions');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('24', '1', 'Points Program', 'Points Program', 'Points Program');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('25', '1', 'Gift Vouchers', 'Gift Vouchers', 'Gift Vouchers');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('26', '1', 'Bidding Questions', 'Bidding Questions', 'Bidding Questions');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('27', '1', 'Winning Questions', 'Winning Questions', 'Winning Questions');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('28', '1', 'Payment Questions', 'Payment Questions', 'Payment Questions');
insert into meta_gtext (gtext_id, language_id, meta_title, meta_keywords, meta_text) values ('29', '1', 'heeeelo!! :)', 'fgddfgddddddddddd,dgd', 'aa, bb, cc');
drop table if exists meta_lexico;
create table meta_lexico (
  meta_lexico_key varchar(32) not null ,
  meta_lexico_text varchar(255) not null ,
  sort_id int(11) not null ,
  meta_lexico_status tinyint(1) default '1' not null ,
  PRIMARY KEY (meta_lexico_key)
);

drop table if exists meta_manufacturers;
create table meta_manufacturers (
  manufacturers_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (manufacturers_id, language_id)
);

drop table if exists meta_products;
create table meta_products (
  products_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (products_id, language_id)
);

insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('93', '1', 'Bundle2', 'Bundle2', 'Bundle2');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('94', '1', 'bundle1', 'bundle1', 'bundle1');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('95', '1', '10 Bids', '10 Bids,win lots of prizes,', '10 Bids on Maxabid.co.uk :)');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('96', '1', '100 Bids', '100 Bids', '100 Bids');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('97', '1', '1000 Bids', '1000 Bids', '1000 Bids');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('98', '1', '50 Bids', '50 Bids', '50 Bids');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('99', '1', '500 Bids', '500 Bids', '500 Bids');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('100', '1', '250 Bids', '250 Bids', '');
insert into meta_products (products_id, language_id, meta_title, meta_keywords, meta_text) values ('101', '1', '750 Bids', '750 Bids', '');
drop table if exists meta_ranges;
create table meta_ranges (
  numeric_ranges_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (numeric_ranges_id, language_id)
);

drop table if exists meta_scripts;
create table meta_scripts (
  meta_scripts_key varchar(32) not null ,
  language_id int(11) default '1' not null ,
  meta_scripts_file varchar(64) not null ,
  meta_title varchar(255) not null ,
  meta_keywords text not null ,
  meta_text text not null ,
  PRIMARY KEY (meta_scripts_key, language_id)
);

insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('0354a1fa71096ec8d93521a17d9aaed8', '1', 'address_book_process.php', 'address book process', 'address,book,process', 'address book process');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('07d2009d614db091bea595130306359a', '1', 'account.php', 'account', 'account', 'account');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('093a284ec5bdfa6399c756c45c12d4da', '1', 'specials.php', 'specials', 'specials', 'specials');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('09c3e91db8f38ddc5e3a097db2d15230', '1', 'shopping_cart.php', 'shopping cart', 'shopping,cart', 'shopping cart');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('103293e8e147116b4b70129251c8b238', '1', 'root.php', 'root', 'root', 'root');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('13dae7d4d9c5a71489a5d3523e16b1b5', '1', 'advanced_search_result.php', 'advanced search result', 'advanced,search,result', 'advanced search result');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('19ea9a346c0da02aefa62f4648622880', '1', 'download.php', 'download', 'download', 'download');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('217e3aa957ad2ba590dc3fc681d7152b', '1', 'info_shopping_cart.php', 'info shopping cart', 'info,shopping,cart', 'info shopping cart');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('22d04ed11f06a095c75a4bddebc0ccc3', '1', 'categories.php', 'Maxabid Auction Types', 'Maxabid Auction Types', 'Maxabid Auction Types');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('2d98efc0dc31e7eaf11f5232e792126a', '1', 'popup_search_help.php', 'popup search help', 'popup,search,help', 'popup search help');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('2ecb5406494a423b123a0cef0a1af863', '1', 'logoff.php', 'logoff', 'logoff', 'logoff');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('336d3861f98bb3f159b5dedaac02752a', '1', 'checkout_shipping_address.php', 'checkout shipping address', 'checkout,shipping,address', 'checkout shipping address');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('3af26970edb0baf84534601f700d8fce', '1', 'checkout_shipping.php', 'checkout shipping', 'checkout,shipping', 'checkout shipping');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('3caf70d85e698d3c5ea45e768e607fc5', '1', 'account_notifications.php', 'account notifications', 'account,notifications', 'account notifications');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('43a84ecf1cb41160c6b7606118c8b9d1', '1', 'account_edit.php', 'account edit', 'account,edit', 'account edit');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('440d5ed9d6090d6b9e0cfbd48fbe82e8', '1', 'product_reviews_write.php', 'product reviews write', 'product,reviews,write', 'product reviews write');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('46e39f67e702bb434edb47007b9f09e4', '1', 'cookie_usage.php', 'cookie usage', 'cookie,usage', 'cookie usage');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('583c427a60d5deadc94e0d5763c952b8', '1', 'product_reviews_info.php', 'product reviews info', 'product,reviews,info', 'product reviews info');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('64fa2df269458e57a3ab3ba9ffa68123', '1', 'create_account.php', 'create account', 'create,account', 'create account');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('72f3f5d50b3609fb45f42e9ec5c3f897', '1', 'address_book.php', 'address book', 'address,book', 'address book');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('73dce75d92181ca956e737b3cb66db98', '1', 'login.php', 'login', 'login', 'login');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('78fb4811059e9254f73450ccc7d42973', '1', 'auction_info.php', 'Auction Information', 'auction,info', 'auction info');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('7dd0084da8e5e251d48bf7610d1f39fe', '1', 'checkout_success.php', 'checkout success', 'checkout,success', 'checkout success');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('7f1b6294a6a8258375b3477859d7472f', '1', 'shop_by_price.php', 'shop by price', 'shop,by,price', 'shop by price');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('828e0013b8f3bc1bb22b4f57172b019d', '1', 'index.php', 'Maxabid Auctions', 'Maxabid Auctions', 'Maxabid Auctions');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('89950d830af95a898d9b424aa5834f80', '1', 'auction_groups.php', 'AA UU CC TT II OO NN SS', 'auction,groups', 'auction groups');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('8a130abac7552d4eacedcfc42c842510', '1', 'account_history.php', 'account history', 'account,history', 'account history');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('8ab0d68227e00cf804a51a07132cc83d', '1', 'create_account_success.php', 'create account success', 'create,account,success', 'create account success');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('8b27695473c9fdb061fe2a0d1f3d59d6', '1', 'checkout_process.php', 'checkout process', 'checkout,process', 'checkout process');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('8c031f8523c49134f8cbf357f077f8e4', '1', 'reviews.php', 'reviews', 'reviews', 'reviews');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('8fac42d0b85c28cd267a8feca1176b9a', '1', 'shipping.php', 'shipping', 'shipping', 'shipping');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('976ac8e8ec1237e6570db90c8a281f0f', '1', 'password_forgotten.php', 'password forgotten', 'password,forgotten', 'password forgotten');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('98ae1a97d3ff0626ef3ef5d671ec798f', '1', 'checkout_payment.php', 'checkout payment', 'checkout,payment', 'checkout payment');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('9d13bf694e990bee4aca4de1d06206a2', '1', 'popup_image.php', 'popup image', 'popup,image', 'popup image');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('a0792d505743e14db359414dff768dec', '1', 'products_new.php', 'products new', 'products,new', 'products new');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('b0d6961abe0b6944a7f0f419df812fc0', '1', 'advanced_search.php', 'advanced search', 'advanced,search', 'advanced search');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('c054dc7050952a64cfe78892c64043b8', '1', 'redirect.php', 'redirect', 'redirect', 'redirect');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('d4253a0dc07fc73d332aa67b9b3b42cc', '1', 'account_password.php', 'account password', 'account,password', 'account password');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('d4885abbf8d5f0fb80daeaf728e0a0d8', '1', 'product_info.php', 'Penny Auction Products', 'Penny Auction Products', 'Penny Auction Products');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('da8bb3a63ccd1c386a28a34896f376dc', '1', 'generic_pages.php', 'Customer Information', 'Customer Information, Help Pages', 'Help and Information for Maxabid Auctions');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('dab63f8d8a7c9f1b2217b9681a379cc4', '1', 'account_history_info.php', 'account history info', 'account,history,info', 'account history info');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('dcfe702546310dad8220cbb16d0ee983', '1', 'checkout_payment_address.php', 'checkout payment address', 'checkout,payment,address', 'checkout payment address');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('dd323aa110c0d21fe5acdafe3da021bc', '1', 'checkout_confirmation.php', 'checkout confirmation', 'checkout,confirmation', 'checkout confirmation');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('e0483ffd5b88b78136d5494dee45325a', '1', 'ssl_check.php', 'ssl check', 'ssl,check', 'ssl check');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('e0f745105a7f3d071b78a91e7e2ed1ea', '1', 'account_newsletters.php', 'account newsletters', 'account,newsletters', 'account newsletters');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('ef5152128b30d2d33b30b1ca34aced7f', '1', 'shop_by_filter.php', 'Maxabid Auctions by filter', 'Maxabid Auctions by filter', 'Maxabid Auctions by filter');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('f7bddc759d4d0aaa68a91416de3ba32b', '1', 'product_reviews.php', 'product reviews', 'product,reviews', 'product reviews');
insert into meta_scripts (meta_scripts_key, language_id, meta_scripts_file, meta_title, meta_keywords, meta_text) values ('fc8d4bb3bf7f56ca700507fee0558c16', '1', 'contact_us.php', 'Contact Maxabid Auctions', 'contact information', 'Contact Maxabid Auctions');
drop table if exists meta_types;
create table meta_types (
  meta_types_id int(11) not null auto_increment,
  meta_types_name varchar(32) not null ,
  meta_types_class varchar(64) not null ,
  meta_types_linkage int(3) default '1' not null ,
  sort_order int(3) default '1' not null ,
  meta_types_status tinyint(1) default '1' not null ,
  PRIMARY KEY (meta_types_id)
);

insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('1', 'Products', 'meta_products', '5', '16', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('2', 'Categories', 'meta_categories', '5', '17', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('3', 'Manufacturers', 'meta_manufacturers', '5', '18', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('4', 'Scripts', 'meta_scripts', '-1', '49', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('5', 'Abstract Zones', 'meta_abstract', '5', '22', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('6', 'Text Pages', 'meta_gtext', '5', '19', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('7', 'Auctions', 'meta_auctions', '5', '13', '1');
insert into meta_types (meta_types_id, meta_types_name, meta_types_class, meta_types_linkage, sort_order, meta_types_status) values ('8', 'Auction Groups', 'meta_auctions_group', '5', '14', '1');
drop table if exists newsletters;
create table newsletters (
  newsletters_id int(11) not null auto_increment,
  title varchar(255) not null ,
  content text not null ,
  module varchar(255) not null ,
  date_added datetime not null ,
  date_sent datetime ,
  status int(1) ,
  locked int(1) default '0' ,
  PRIMARY KEY (newsletters_id)
);

insert into newsletters (newsletters_id, title, content, module, date_added, date_sent, status, locked) values ('1', 'Test Newsletter', 'Test Newsletter', 'newsletter', '2012-04-27 20:48:36', NULL, '0', '1');
drop table if exists numeric_ranges;
create table numeric_ranges (
  numeric_ranges_id int(11) not null auto_increment,
  numeric_ranges_min int(3) not null ,
  numeric_ranges_max int(3) not null ,
  numeric_ranges_desc varchar(64) not null ,
  sort_id tinyint(3) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (numeric_ranges_id)
);

insert into numeric_ranges (numeric_ranges_id, numeric_ranges_min, numeric_ranges_max, numeric_ranges_desc, sort_id, status_id) values ('1', '0', '100', 'Up to $100', '1', '1');
insert into numeric_ranges (numeric_ranges_id, numeric_ranges_min, numeric_ranges_max, numeric_ranges_desc, sort_id, status_id) values ('2', '100', '199', '$100-$200', '2', '1');
insert into numeric_ranges (numeric_ranges_id, numeric_ranges_min, numeric_ranges_max, numeric_ranges_desc, sort_id, status_id) values ('3', '200', '299', '$200-$299', '3', '1');
insert into numeric_ranges (numeric_ranges_id, numeric_ranges_min, numeric_ranges_max, numeric_ranges_desc, sort_id, status_id) values ('4', '300', '399', '$300+', '4', '1');
drop table if exists orders;
create table orders (
  orders_id int(11) not null auto_increment,
  customers_id int(11) not null ,
  customers_name varchar(64) not null ,
  customers_company varchar(32) ,
  customers_street_address varchar(64) not null ,
  customers_suburb varchar(32) ,
  customers_city varchar(32) not null ,
  customers_postcode varchar(10) not null ,
  customers_state varchar(32) ,
  customers_country varchar(32) not null ,
  customers_telephone varchar(32) not null ,
  customers_email_address varchar(96) not null ,
  customers_address_format_id int(5) not null ,
  delivery_name varchar(64) not null ,
  delivery_company varchar(32) ,
  delivery_street_address varchar(64) not null ,
  delivery_suburb varchar(32) ,
  delivery_city varchar(32) not null ,
  delivery_postcode varchar(10) not null ,
  delivery_state varchar(32) ,
  delivery_country varchar(32) not null ,
  delivery_address_format_id int(5) not null ,
  billing_name varchar(64) not null ,
  billing_company varchar(32) ,
  billing_street_address varchar(64) not null ,
  billing_suburb varchar(32) ,
  billing_city varchar(32) not null ,
  billing_postcode varchar(10) not null ,
  billing_state varchar(32) ,
  billing_country varchar(32) not null ,
  billing_address_format_id int(5) not null ,
  payment_method varchar(255) not null ,
  cc_type varchar(20) ,
  cc_owner varchar(64) ,
  cc_number varchar(32) ,
  cc_expires varchar(4) ,
  last_modified datetime ,
  date_purchased datetime ,
  orders_status int(5) not null ,
  orders_date_finished datetime ,
  currency char(3) ,
  currency_value decimal(14,6) ,
  PRIMARY KEY (orders_id)
);

insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('58', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 10:51:34', '6', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('59', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', '2013-10-05 17:10:55', '2013-10-05 10:53:06', '8', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('60', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 10:58:40', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('61', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', '2013-10-05 17:09:52', '2013-10-05 16:55:51', '3', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('62', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 18:12:51', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('63', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 18:17:23', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('64', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 18:17:47', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('65', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 19:48:09', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('66', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', '2013-10-05 20:09:36', '2013-10-05 19:56:30', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('67', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal (all major credit cards accepted)', '', '', '', '', NULL, '2013-10-05 19:59:41', '3', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('68', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Cash on Delivery', '', '', '', '', NULL, '2013-10-05 20:05:24', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('69', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Credit/Debit Card or eCheck (via PayPal)', '', '', '', '', NULL, '2013-10-05 20:55:40', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('70', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Cash on Delivery', '', '', '', '', NULL, '2013-10-05 20:39:17', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('71', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Credit/Debit Card or eCheck (via PayPal)', '', '', '', '', NULL, '2013-10-05 21:18:18', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('72', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal Website Payments Pro (Direct Payments)', '', '', '', '', NULL, '2013-10-05 22:07:42', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('73', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal Website Payments Pro (Direct Payments)', '', '', '', '', NULL, '2013-10-05 22:10:10', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('74', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal Website Payments Pro (Direct Payments)', '', '', '', '', NULL, '2013-10-05 22:57:14', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('75', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'PayPal Website Payments Pro (Direct Payments)', '', '', '', '', NULL, '2013-10-05 23:01:17', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('76', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Credit/Debit Card or eCheck (via PayPal)', '', '', '', '', NULL, '2013-10-06 22:23:54', '5', NULL, 'GBP', '1.000000');
insert into orders (orders_id, customers_id, customers_name, customers_company, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, last_modified, date_purchased, orders_status, orders_date_finished, currency, currency_value) values ('77', '7', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '45345345345345', 'aleem786@hotmail.co.uk', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'AA YY', '', 'None Street', '', 'London', 'e6123', 'Greater London', 'United Kingdom', '1', 'Credit/Debit Card or eCheck (via PayPal)', '', '', '', '', NULL, '2013-10-06 23:29:27', '6', NULL, 'GBP', '1.000000');
drop table if exists orders_comments;
create table orders_comments (
  orders_comments_id int(11) not null auto_increment,
  orders_comments_title varchar(255) not null ,
  orders_comments_text text not null ,
  orders_status_id int(11) not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (orders_comments_id)
);

insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('2', 'Royal Mail', 'Your order has been shipped via Royal Mail. Thank you for shopping with us. You can track the status of your order online at www.royalmail.com using the tracking number below:
Tracking Number: [%sTracking No]', '2', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('3', 'OoS Refund', 'Unfortunately the shoes you ordered are out of stock. Your transaction has been refunded, thank you for shopping with us.', '3', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('4', 'Updated', 'Your shipping address has been updated to reflect your Paypal.com confirmed address. Contact us within 48 hours if you prefer we cancel this order.', '2', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('5', 'No Match Void', 'The shipping address you provided does not match the address the issuing bank of your credit card has on file. Your payment has been voided and your order will not be processed.', '1', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('6', 'No Match Refund', 'The shipping address you provided does not match the address the issuing bank of your credit card has on file. Your payment has been refunded and your order will not be processed.', '1', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('7', 'Package Signing', 'All packages must be signed for. If you are not home at the time of delivery, a notice will be left and you will be required to pick up the package at your nearest post office.
Additionally, once your package is shipped you will also be given a tracking number.
Please use this tracking number [%s Tracking No] to anticipate approximate delivery date.', '3', '1');
insert into orders_comments (orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('8', 'Separately', '[%s Initial Comment] Your order has been shipped via Royal Mail. Thank you for shopping with us  You can track the status of your order online at www.royalmail.com using the tracking number below:
Tracking Number:
[%sTracking No]', '3', '1');
drop table if exists orders_products;
create table orders_products (
  orders_products_id int(11) not null auto_increment,
  orders_id int(11) not null ,
  products_id int(11) not null ,
  products_model varchar(12) ,
  products_name varchar(64) not null ,
  products_price decimal(15,4) not null ,
  final_price decimal(15,4) not null ,
  products_tax decimal(7,4) not null ,
  products_quantity int(2) not null ,
  products_returned tinyint(2) unsigned default '0' ,
  products_exchanged tinyint(2) default '0' not null ,
  products_exchanged_id int(11) default '0' not null ,
  tracking_number varchar(128) not null ,
  PRIMARY KEY (orders_products_id)
);

insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('83', '59', '0', '', '500 Bids [500 Bids]', '1.2800', '1.2800', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('84', '59', '0', '', 'Bundle-1 [Bundle-1]', '8.7800', '8.7800', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('85', '59', '0', '', 'Bundle2 [Bundle2]', '1.5600', '1.5600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('86', '59', '0', '', '100 Bids [100 Bids]', '0.0900', '0.0900', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('87', '59', '0', '', 'Bundle-1 [Bundle-1]', '7.1100', '7.1100', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('88', '59', '0', '', 'Bundle2 [Bundle2]', '13.0500', '13.0500', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('89', '59', '0', '', '750 Bids [750 Bids]', '12.1000', '12.1000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('90', '59', '0', '', '10 Bids [10 Bids]', '12.5700', '12.5700', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('91', '59', '0', '', '10 Bids [10 Bids]', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('92', '59', '0', '', '500 Bids [500 Bids]', '0.1100', '0.1100', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('93', '59', '0', '', '1000 Bids [1000 Bids]', '10.1200', '10.1200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('94', '59', '0', '', '10 Bids [10 Bids]', '5.0700', '5.0700', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('95', '59', '0', '', '250 Bids [250 Bids]', '78.3200', '78.3200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('96', '59', '0', '', '750 Bids [750 Bids]', '12.3300', '12.3300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('97', '59', '0', '', '750 Bids [750 Bids]', '14.9800', '14.9800', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('98', '59', '97', '', '1000 Bids', '140.8333', '140.8333', '0.0000', '2', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('99', '59', '99', '', '500 Bids', '99.0000', '99.0000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('100', '59', '101', '', '750 Bids', '139.0000', '139.0000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('101', '60', '97', '', '1000 Bids', '140.8333', '140.8333', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('102', '61', '99', '', '500 Bids', '99.0000', '99.0000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('107', '66', '97', '', '1000 Bids', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('108', '66', '100', '', '250 Bids', '0.0300', '0.0300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('109', '67', '96', '', '100 Bids', '0.0200', '0.0200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('110', '67', '100', '', '250 Bids', '0.0300', '0.0300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('111', '68', '95', '', '10 Bids', '0.0100', '0.0100', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('113', '70', '101', '', '750 Bids', '139.0000', '139.0000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('118', '69', '97', '', '1000 Bids', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('119', '69', '100', '', '250 Bids', '0.0300', '0.0300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('120', '69', '96', '', '100 Bids', '0.0200', '0.0200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('121', '69', '95', '', '10 Bids', '0.0100', '0.0100', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('122', '71', '97', '', '1000 Bids', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('123', '71', '100', '', '250 Bids', '0.0300', '0.0300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('124', '71', '96', '', '100 Bids', '0.0200', '0.0200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('125', '72', '97', '', '1000 Bids', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('126', '72', '100', '', '250 Bids', '0.0300', '0.0300', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('127', '72', '96', '', '100 Bids', '0.0200', '0.0200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('128', '73', '98', '', '50 Bids', '12.5000', '12.5000', '20.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('129', '74', '101', '', '750 Bids', '139.0000', '139.0000', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('130', '75', '98', '', '50 Bids', '12.5000', '12.5000', '20.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('131', '76', '96', '', '100 Bids', '0.0200', '0.0200', '0.0000', '1', '0', '0', '0', '');
insert into orders_products (orders_products_id, orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity, products_returned, products_exchanged, products_exchanged_id, tracking_number) values ('141', '77', '97', '', '1000 Bids', '0.0600', '0.0600', '0.0000', '1', '0', '0', '0', '');
drop table if exists orders_products_attributes;
create table orders_products_attributes (
  orders_products_attributes_id int(11) not null auto_increment,
  orders_id int(11) not null ,
  orders_products_id int(11) not null ,
  products_options varchar(32) not null ,
  products_options_values varchar(32) not null ,
  options_values_price decimal(15,4) not null ,
  price_prefix char(1) not null ,
  PRIMARY KEY (orders_products_attributes_id)
);

drop table if exists orders_products_download;
create table orders_products_download (
  orders_products_download_id int(11) not null auto_increment,
  orders_id int(11) default '0' not null ,
  orders_products_id int(11) default '0' not null ,
  orders_products_filename varchar(255) not null ,
  download_maxdays int(2) default '0' not null ,
  download_count int(2) default '0' not null ,
  PRIMARY KEY (orders_products_download_id)
);

drop table if exists orders_products_group_fields;
create table orders_products_group_fields (
  orders_id int(11) default '0' not null ,
  orders_products_id int(11) default '0' not null ,
  group_values_id int(11) default '0' not null ,
  group_values_name varchar(64) not null ,
  group_values_price decimal(15,4) default '0.0000' not null ,
  PRIMARY KEY (orders_id, orders_products_id, group_values_id)
);

insert into orders_products_group_fields (orders_id, orders_products_id, group_values_id, group_values_name, group_values_price) values ('2', '2', '6', '6', '0.0000');
drop table if exists orders_status;
create table orders_status (
  orders_status_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  orders_status_name varchar(32) not null ,
  sort_order int(2) default '1' not null ,
  PRIMARY KEY (orders_status_id, language_id),
  KEY idx_orders_status_name (orders_status_name)
);

insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('1', '1', 'Pending', '1');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('2', '1', 'Processing', '1');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('3', '1', 'Delivered', '1');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('4', '1', 'Shipped', '4');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('5', '1', 'Completed', '5');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('6', '1', 'Preparing [PayPal IPN]', '1');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('7', '1', 'Processing [PayPal IPN]', '1');
insert into orders_status (orders_status_id, language_id, orders_status_name, sort_order) values ('8', '1', 'Received [PayPal IPN]', '1');
drop table if exists orders_status_history;
create table orders_status_history (
  orders_status_history_id int(11) not null auto_increment,
  orders_id int(11) not null ,
  orders_status_id int(5) not null ,
  date_added datetime not null ,
  customer_notified int(1) default '0' ,
  comments text ,
  PRIMARY KEY (orders_status_history_id)
);

insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('2', '2', '1', '2010-03-29 13:11:26', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('75', '59', '6', '2013-10-05 10:53:06', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('76', '59', '5', '2013-10-05 10:57:46', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('77', '60', '5', '2013-10-05 10:58:40', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('78', '61', '5', '2013-10-05 16:55:51', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('79', '61', '3', '2013-10-05 17:09:52', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('80', '59', '8', '2013-10-05 17:10:55', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('85', '66', '5', '2013-10-05 19:56:30', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('86', '66', '3', '2013-10-05 19:57:58', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('87', '67', '3', '2013-10-05 19:59:41', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('88', '68', '5', '2013-10-05 20:05:24', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('89', '66', '5', '2013-10-05 20:09:36', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('91', '70', '5', '2013-10-05 20:39:17', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('93', '69', '5', '2013-10-05 20:55:40', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('94', '71', '5', '2013-10-05 21:18:18', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('95', '72', '5', '2013-10-05 22:07:42', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('96', '73', '5', '2013-10-05 22:10:10', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('97', '74', '5', '2013-10-05 22:57:14', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('98', '75', '5', '2013-10-05 23:01:17', '1', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('99', '76', '5', '2013-10-06 22:23:54', '0', '');
insert into orders_status_history (orders_status_history_id, orders_id, orders_status_id, date_added, customer_notified, comments) values ('109', '77', '6', '2013-10-06 23:29:27', '0', '');
drop table if exists orders_total;
create table orders_total (
  orders_total_id int(10) unsigned not null auto_increment,
  orders_id int(11) not null ,
  title varchar(255) not null ,
  text varchar(255) not null ,
  value decimal(15,4) not null ,
  class varchar(64) not null ,
  sort_order int(11) not null ,
  PRIMARY KEY (orders_total_id),
  KEY idx_orders_total_orders_id (orders_id)
);

insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('4', '2', 'Sub-Total:', '$100.00CAD', '100.0000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('5', '2', 'Flat Rate (Best Way):', '$5.00CAD', '5.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('6', '2', 'Total:', '<b>$105.00CAD</b>', '105.0000', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('156', '59', 'Sub-Total:', '£697.19', '697.1900', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('157', '59', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('158', '59', 'Total:', '<b>£697.19</b>', '697.1900', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('159', '60', 'Sub-Total:', '£140.83', '140.8300', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('160', '60', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('161', '60', 'Total:', '<b>£140.83</b>', '140.8300', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('162', '61', 'Sub-Total:', '£99.00', '99.0000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('163', '61', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('164', '61', 'Total:', '<b>£99.00</b>', '99.0000', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('177', '66', 'Sub-Total:', '£0.09', '0.0900', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('178', '66', 'Per Item (Best Way):', '£5.00', '5.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('179', '66', 'Total:', '<b>£5.09</b>', '5.0900', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('180', '67', 'Sub-Total:', '£0.05', '0.0500', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('181', '67', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('182', '67', 'Total:', '<b>£0.05</b>', '0.0500', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('183', '68', 'Sub-Total:', '£0.01', '0.0100', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('184', '68', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('185', '68', 'Total:', '<b>£0.01</b>', '0.0100', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('189', '70', 'Sub-Total:', '£139.00', '139.0000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('190', '70', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('191', '70', 'Total:', '<b>£139.00</b>', '139.0000', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('195', '69', 'Sub-Total:', '£0.12', '0.1200', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('196', '69', 'Per Item (Best Way):', '£0.04', '0.0400', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('197', '69', 'Total:', '<b>£0.16</b>', '0.1600', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('198', '71', 'Sub-Total:', '£0.11', '0.1100', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('199', '71', 'Per Item (Best Way):', '£0.03', '0.0300', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('200', '71', 'Total:', '<b>£0.14</b>', '0.1400', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('201', '72', 'Sub-Total:', '£0.11', '0.1100', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('202', '72', 'Per Item (Best Way):', '£0.03', '0.0300', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('203', '72', 'Total:', '<b>£0.14</b>', '0.1400', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('204', '73', 'Sub-Total:', '£12.50', '12.5000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('205', '73', 'Per Item (Best Way):', '£0.01', '0.0100', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('206', '73', 'UK VAT 20%:', '£2.50', '2.5000', 'ot_tax', '3');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('207', '73', 'Total:', '<b>£15.01</b>', '15.0100', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('208', '74', 'Sub-Total:', '£139.00', '139.0000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('209', '74', 'Per Item (Best Way):', '£0.01', '0.0100', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('210', '74', 'Total:', '<b>£139.01</b>', '139.0100', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('211', '75', 'Sub-Total:', '£12.50', '12.5000', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('212', '75', 'Delivery (P+P):', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('213', '75', 'UK VAT 20%:', '£2.50', '2.5000', 'ot_tax', '3');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('214', '75', 'Total:', '<b>£15.00</b>', '15.0000', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('215', '76', 'Sub-Total:', '£0.02', '0.0200', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('216', '76', 'Per Item (Best Way):', '£0.01', '0.0100', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('217', '76', 'Total:', '<b>£0.03</b>', '0.0300', 'ot_total', '4');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('245', '77', 'Sub-Total:', '£0.06', '0.0600', 'ot_subtotal', '1');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('246', '77', '0:', '£0.00', '0.0000', 'ot_shipping', '2');
insert into orders_total (orders_total_id, orders_id, title, text, value, class, sort_order) values ('247', '77', 'Total:', '<b>£0.06</b>', '0.0600', 'ot_total', '4');
drop table if exists products;
create table products (
  products_id int(11) not null auto_increment,
  products_quantity int(4) not null ,
  products_model varchar(32) ,
  products_image varchar(64) ,
  products_price decimal(15,4) not null ,
  products_cost decimal(15,4) not null ,
  products_date_added datetime not null ,
  products_last_modified datetime ,
  products_date_available datetime ,
  products_weight decimal(5,2) not null ,
  products_status tinyint(1) default '1' not null ,
  products_display tinyint(1) default '1' not null ,
  products_tax_class_id int(11) not null ,
  manufacturers_id int(11) ,
  products_ordered int(11) default '0' not null ,
  products_bids int(11) default '0' not null ,
  products_type_id int(2) default '0' ,
  products_taste_id int(2) default '0' ,
  buyout int(1) default '0' ,
  play_times int(1) default '0' ,
  PRIMARY KEY (products_id),
  KEY idx_products_date_added (products_date_added)
);

insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('93', '3', '', 'lotto-649.jpg', '2.0000', '0.0000', '2010-03-09 18:19:38', '2011-07-25 17:25:42', NULL, '0.00', '1', '1', '0', '0', '24', '0', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('94', '1', '', 'game1.jpg', '3.0000', '0.0000', '2010-03-19 16:54:30', '2011-07-26 15:52:32', NULL, '0.00', '1', '1', '0', '0', '4', '0', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('95', '0', '', 'bids10-image.jpg', '0.0100', '0.0000', '2011-07-25 17:27:08', '2013-10-05 19:52:43', NULL, '0.00', '1', '1', '0', '0', '6', '10', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('96', '0', '', 'bids100-image.jpg', '0.0200', '0.0000', '2011-07-25 17:30:05', '2013-10-05 19:53:30', NULL, '0.00', '1', '1', '0', '0', '3', '100', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('97', '0', '', 'bids1000-image.jpg', '0.0600', '0.0000', '2011-07-25 17:34:14', '2013-10-05 19:55:04', NULL, '0.00', '1', '1', '0', '0', '13', '1000', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('98', '0', '', 'bids50-image.jpg', '12.5000', '0.0000', '2011-07-25 17:35:33', '2013-05-01 10:23:07', NULL, '0.00', '1', '1', '2', '0', '4', '50', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('99', '0', '', 'bids500-image.jpg', '99.0000', '0.0000', '2011-07-25 17:36:23', '2013-05-06 18:03:09', NULL, '0.00', '1', '1', '0', '0', '4', '500', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('100', '0', '', NULL, '0.0300', '0.0000', '2013-05-01 10:27:01', '2013-10-05 19:53:52', NULL, '0.00', '1', '1', '0', '0', '1', '250', '0', '0', '0', '0');
insert into products (products_id, products_quantity, products_model, products_image, products_price, products_cost, products_date_added, products_last_modified, products_date_available, products_weight, products_status, products_display, products_tax_class_id, manufacturers_id, products_ordered, products_bids, products_type_id, products_taste_id, buyout, play_times) values ('101', '0', '', NULL, '139.0000', '0.0000', '2013-05-01 10:29:37', '2013-05-06 18:01:40', NULL, '0.00', '1', '1', '0', '0', '2', '750', '0', '0', '0', '0');
drop table if exists products_attributes;
create table products_attributes (
  products_attributes_id int(11) not null auto_increment,
  products_id int(11) not null ,
  options_id int(11) not null ,
  options_values_id int(11) not null ,
  options_values_price decimal(15,4) not null ,
  price_prefix char(1) not null ,
  PRIMARY KEY (products_attributes_id)
);

drop table if exists products_attributes_download;
create table products_attributes_download (
  products_attributes_id int(11) not null ,
  products_attributes_filename varchar(255) not null ,
  products_attributes_maxdays int(2) default '0' ,
  products_attributes_maxcount int(2) default '0' ,
  PRIMARY KEY (products_attributes_id)
);

insert into products_attributes_download (products_attributes_id, products_attributes_filename, products_attributes_maxdays, products_attributes_maxcount) values ('26', 'unreal.zip', '7', '3');
drop table if exists products_description;
create table products_description (
  products_id int(11) not null ,
  language_id int(11) default '1' not null ,
  products_name varchar(64) not null ,
  products_description text ,
  products_url varchar(255) ,
  products_viewed int(5) default '0' ,
  last_accessed datetime not null ,
  PRIMARY KEY (products_id, language_id),
  KEY products_name (products_name)
);

insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('93', '1', 'Bundle2', '<p>Bundle2</p>', '', '166', '2012-10-16 06:52:01');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('94', '1', 'Bundle-1', '<p>bundle1</p>', '', '28', '2013-10-05 11:04:04');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('95', '1', '10 Bids', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>10 Bids&nbsp;</strong></span></p>', '', '46', '2012-11-10 00:45:26');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('96', '1', '100 Bids', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>100 Bids&nbsp;</strong></span></p>', '', '24', '2013-04-08 16:42:18');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('97', '1', '1000 Bids', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>1000 Bids&nbsp;</strong></span></p>', '', '79', '2013-05-01 12:23:10');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('98', '1', '50 Bids', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>50 Bids&nbsp;</strong></span></p>', '', '4', '2012-12-16 12:51:25');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('99', '1', '500 Bids', '<p><span style=\"background-color: #6666cc; color: #cc9999; font-size: xx-large;\">&nbsp;<strong>500 Bids&nbsp;</strong></span></p>', '', '22', '2013-05-14 22:42:02');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('100', '1', '250 Bids', '', '', '1', '2013-05-01 10:48:05');
insert into products_description (products_id, language_id, products_name, products_description, products_url, products_viewed, last_accessed) values ('101', '1', '750 Bids', '', '', '2', '2013-05-01 12:23:29');
drop table if exists products_extra_images;
create table products_extra_images (
  products_extra_images_id int(11) not null auto_increment,
  products_id int(11) ,
  products_extra_image varchar(64) ,
  KEY products_extra_images_id (products_extra_images_id)
);

drop table if exists products_filters;
create table products_filters (
  products_filter_id int(11) not null auto_increment,
  products_filter_name varchar(64) not null ,
  products_filter_db_name varchar(32) not null ,
  products_filter_parameter varchar(32) not null ,
  width_id tinyint(1) default '1' not null ,
  layout_id int(2) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  sort_id tinyint(3) default '1' not null ,
  PRIMARY KEY (products_filter_id)
);

insert into products_filters (products_filter_id, products_filter_name, products_filter_db_name, products_filter_parameter, width_id, layout_id, status_id, sort_id) values ('1', 'Products Buyout', 'buyout', 'buyout', '1', '0', '1', '1');
insert into products_filters (products_filter_id, products_filter_name, products_filter_db_name, products_filter_parameter, width_id, layout_id, status_id, sort_id) values ('2', 'Products Play Times', 'play_times', 'play_times', '1', '0', '1', '2');
drop table if exists products_notifications;
create table products_notifications (
  products_id int(11) not null ,
  customers_id int(11) not null ,
  date_added datetime not null ,
  PRIMARY KEY (products_id, customers_id)
);

drop table if exists products_options;
create table products_options (
  products_options_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  products_options_name varchar(32) not null ,
  PRIMARY KEY (products_options_id, language_id)
);

drop table if exists products_options_values;
create table products_options_values (
  products_options_values_id int(11) default '0' not null ,
  language_id int(11) default '1' not null ,
  products_options_values_name varchar(64) not null ,
  PRIMARY KEY (products_options_values_id, language_id)
);

drop table if exists products_options_values_to_products_options;
create table products_options_values_to_products_options (
  products_options_values_to_products_options_id int(11) not null auto_increment,
  products_options_id int(11) not null ,
  products_options_values_id int(11) not null ,
  PRIMARY KEY (products_options_values_to_products_options_id)
);

drop table if exists products_select;
create table products_select (
  products_select_id int(11) not null ,
  products_filter_id int(11) not null ,
  products_select_name varchar(64) not null ,
  status_id tinyint(1) default '1' not null ,
  sort_id tinyint(3) default '1' not null ,
  PRIMARY KEY (products_select_id, products_filter_id),
  KEY idx_select_status_id (status_id),
  KEY idx_select_sort_id (sort_id)
);

insert into products_select (products_select_id, products_filter_id, products_select_name, status_id, sort_id) values ('0', '1', 'N/A', '0', '0');
insert into products_select (products_select_id, products_filter_id, products_select_name, status_id, sort_id) values ('0', '2', 'N/A', '0', '0');
insert into products_select (products_select_id, products_filter_id, products_select_name, status_id, sort_id) values ('1', '1', '3 Tickets', '1', '1');
insert into products_select (products_select_id, products_filter_id, products_select_name, status_id, sort_id) values ('1', '2', 'Wed-Sat', '1', '1');
insert into products_select (products_select_id, products_filter_id, products_select_name, status_id, sort_id) values ('2', '2', 'Fri', '1', '2');
drop table if exists products_to_categories;
create table products_to_categories (
  products_id int(11) not null ,
  categories_id int(11) not null ,
  KEY idx_products_id (products_id),
  KEY idx_categories_id (categories_id)
);

insert into products_to_categories (products_id, categories_id) values ('93', '10');
insert into products_to_categories (products_id, categories_id) values ('94', '10');
insert into products_to_categories (products_id, categories_id) values ('95', '9');
insert into products_to_categories (products_id, categories_id) values ('96', '9');
insert into products_to_categories (products_id, categories_id) values ('97', '9');
insert into products_to_categories (products_id, categories_id) values ('98', '9');
insert into products_to_categories (products_id, categories_id) values ('99', '9');
insert into products_to_categories (products_id, categories_id) values ('100', '9');
insert into products_to_categories (products_id, categories_id) values ('101', '9');
drop table if exists products_to_group_fields;
create table products_to_group_fields (
  products_id int(11) default '0' not null ,
  group_fields_id int(11) default '0' not null ,
  PRIMARY KEY (products_id, group_fields_id)
);

drop table if exists products_zones_to_categories;
create table products_zones_to_categories (
  abstract_zone_id int(11) not null ,
  categories_id int(11) default '0' not null ,
  products_id int(11) default '0' not null ,
  sort_id tinyint(3) default '0' not null ,
  status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (abstract_zone_id, categories_id, products_id)
);

insert into products_zones_to_categories (abstract_zone_id, categories_id, products_id, sort_id, status_id) values ('6', '8', '93', '1', '1');
drop table if exists reviews;
create table reviews (
  reviews_id int(11) not null auto_increment,
  products_id int(11) not null ,
  customers_id int(11) ,
  customers_name varchar(64) not null ,
  reviews_rating int(1) ,
  date_added datetime ,
  last_modified datetime ,
  reviews_read int(5) default '0' not null ,
  approved tinyint(1) default '0' not null ,
  PRIMARY KEY (reviews_id)
);

drop table if exists reviews_description;
create table reviews_description (
  reviews_id int(11) not null ,
  languages_id int(11) not null ,
  reviews_text text not null ,
  PRIMARY KEY (reviews_id, languages_id)
);

drop table if exists seo_cache;
create table seo_cache (
  auto_id int(11) not null auto_increment,
  osc_url_key varchar(32) not null ,
  seo_cache_keys text not null ,
  seo_cache_urls text not null ,
  seo_cache_separators text not null ,
  date_added datetime not null ,
  PRIMARY KEY (auto_id),
  KEY idx_osc_url_key (osc_url_key),
  KEY idx_date_added (date_added)
);

insert into seo_cache (auto_id, osc_url_key, seo_cache_keys, seo_cache_urls, seo_cache_separators, date_added) values ('435', '461ed2d50788578736cde74c4c258b76', 'FZLJDUQxCEMb+gd2QjmEpf8SJqOcEonY5vkMazFdja06nT7rLhawcWscvuZMCUzHVYfZXbZCeMf1ivInYJKwxweQjFlV+hw4Gu2Wi184BOjx08jid5YybDxwsY/T/VK7lUTTbwpK9kCE7xoNkLR8UArASmvH5Y2pO4P6sWSukPgYMz2i8fAG6+UBO+1aBUBB9V0Kq43qCci8xoLT93gLBj33X9EmDhMDRCIpwhGSDVJllJhvDPbGELCPCBpTQI9QsSY36Se23iFz6ogKsu4iMreuwBzb75qaJiqmuHv8n83Ty2aesT3fOR4F4RUsoUe76Hk0Sn0yXf6BKTY+3dN0TOwu+BPF1uzHgr5A7oztqCypl5KfZIm9L6ut5guRY9hybsKwXe9Z2LckEsda+zQexmi7fK3vA4PrSQ9T4Quy+u1riaeFu3KJeBwHfPHIXiEY90t/eDLX5s54iuWDaBAU7woWPw==', 'lZLLcsMwCEV/qJL3+ZkOxXowqUBBKG7+vnbTbQZnraM75wLVrF+WZdu22OAHvmiNKHFel4/66oU4izYwEo7V2vdrEiYe1Ag5gU1NayBLbTi/+lSsMFLYZTx2VOmduAQENSc3w80hUFoTDreZxp+3g3chthG6SlFoDlwoW7jLxJrUa7UXX49SZ0U2Yn6H7/Boie10fi65OPVGpeciNO2bZq9hV7oDPgKLESYn25K2EVB4pTNrMQW8HuM4p/J/pOF51J74bmGA9jlH7LX/Ag==', '?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', '2013-11-03 16:46:46');
insert into seo_cache (auto_id, osc_url_key, seo_cache_keys, seo_cache_urls, seo_cache_separators, date_added) values ('436', 'fc95aed78650dcaed27ae7ce2a14b464', 'FZLJDUQxCEMb+gd2QjmEpf8SJqOcsKJgP+cMazFdja06nT7rLhawcWscvn2qp4W7col4HAc8tmT4FNxPwCRhjw8gGbOq9DlwNNotF79wCNDjp5HF7yxl2HjgYh+n+zVnSmA6rjrM7rIVwjuuV5S/1G4l0fSbgpI9EOG7RgMkLd85HgXhFSyhR7uo7zFKFaou/8AUGwP6NB0Tuwu+3tia/VbQF8idsR2VJWUsrKYl9p6stpovRI5hy7kJw3a9Z2EBiMSx1j6NByDaLl/r+xzjetILWKgrq1+6umeuzZ3xFMtn3iAo3ggWH5QCsNLacXl03nUG9WPJXCHxMWZ6ROPhDdbLA/ba0arnI6i+S2G1z/EEZN4XA+eB8BYMeiV9RZs4TAwQiaQIR0g2SF+XEvONwd4YAvYRQWN60OZRZE1u0k/scQuZU0dUkHUXkblfRpj3L75rj1yiYoq7x182Ty+becb2/AA=', 'lZJBbgMhDEUvVGb2uUzlegxYKYYYk2luX0bZRYo8WfN5et92NmuXdd33fSnwBz+8LViXcV2/8rsXGGhcJcSqBawv2cqvm+4hEthQ2gIbFe9XG4oZOoUp5GVZnibTyXHpubbGkgKCmpONKSYn0jM/cUqzmXieTfkO+AhSjZEctpGWHrDKxsewPbYp4PVodk5lcg3QvkdfWm7vlxfh5nhiLWXewm1QP+PZKov10LQmheKwE0cL9zowk3oDmHeyHf3Piuws8km+waOQ2Av/Hw==', '?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?', '2013-11-03 16:46:50');
insert into seo_cache (auto_id, osc_url_key, seo_cache_keys, seo_cache_urls, seo_cache_separators, date_added) values ('437', '258c6a72209854d28ff84e6563d49ae3', 'DZJLEgAxBEQvNAvxdxwJ7n+EUTZSJbo9vEke4ZWY97zSesxYAybua4PveD1WvY+EpaZuD6fyG3d5RfgxKCeMW8NBJRLhcgeXKNOc84VBgLh5HWLb/5ihbXHmlBveryiT46SdEYOeGdJ3YMPkstCXUiXIknaTD2c1RNiMYgNy8YdDokpx775Yb2/luweJLg4Gf9ab5lHLS40kqWtFMewQwI67HqbCh0DeI3KozrRhuNU75MWv7a7j12dANqLf4KV8JpNXwL4QH8k+6KgT+Dgk4G0L7yhn/UoAOqnkSNHDtoFEOgFHEzngM1frXl8ifc1OHsBQrHMvxgL+7iFzW4TyolG7sHvZybhyk8mXY9GxA2fD3VxuKFCf3QTULvA7ONqrzYtwzfDlZxQxZyUVrD/Y9nsDvSzJ0Z+yL7qpghX1sm9lw3HYDSBPm1HtQawPS7lL9Qc=', 'jZLNbgMhDIRfqN5tDqtWlaI+yoqAE6zwV7BD8/bd0KpSDiu4wjf2MIxlTh/zXGudvPpWJzKTjpNc5xe7d6NEM8WwXnKUVKZk0+fT0UrmeOjKC5xRsWQ0QIy+TJa921clydqqgrA57LEUzjF79TDZmVpsTInCBbTK3GH/3lggq0QGmDzmUcnDNCSlr6MCL44pOYRKIYzvcbFiYZBAX9KiGt13i6It5l6y/xmcJBiHw/j2H7CNH3XTcPKj+FaMTHwfw59q24p8zKgxsLuvGAya/Q4ur619sLx1Vv2mAwdY3hv5Aw==', '?,&,?,?,?,?,?,?,?,?,?,?,?,?,?,&,?,?', '2013-11-03 16:48:07');
insert into seo_cache (auto_id, osc_url_key, seo_cache_keys, seo_cache_urls, seo_cache_separators, date_added) values ('438', '7f0f11013d77cb4b776da3972d81a7b2', 'Dc1JEcAwCABAQ3lAuOVAAP8S2lkB60Py6JbEvuedNmvGGrBRbwwOg3LCug3gVSIRbndwiTbNxRMGAeLmjcRWszdDxwIX2+3WacrkwDRcMZjdJX0IP5NioZPSLZclrZKRswcibFfvwOXmM9nQhpJCkMVAK0F/doejr+rZskV1BErejiKLCmSQlw+q4gM=', 'hY7BDgIhDER/SIoevHjZTzGVZbdEgaYtwc9XE5M9Ec7zZuaRGd+8771Dxjc+0gqhQnv6E40SbMFSLeq2iNYkri5ZzApk+TVucZNAqNF9L2ZsKluVjL+XyapSZU5ldwHFJuzf+75LbazAxMtl7Hs9H6Yf', '?,?,?,?,?,&,?', '2013-11-03 16:53:00');
insert into seo_cache (auto_id, osc_url_key, seo_cache_keys, seo_cache_urls, seo_cache_separators, date_added) values ('439', '50e5bbef6fd94810a670f30765279ba8', 'HZFZCkQhDAQv9D6yL8eJJrn/EUaGgCiIXV3GsF6mo7n3RpfPuoslbJ47Dl91UrkRWBlKaB3Grimrsjy1n4BJwYYPIBmzqnQEhGa71eKXDgkaHo0sfmap0sYTFzucztdcJYnluOowu8t2Ed64HlH+SruVRMtPCUr1QKbvGg2QtHy0rGac57yT2Jl38x4k5kNLKS9iO2MZ9F7mgJ4qX4HT0689fePnAd3BBX2Tc5cO13XdOgr+pcZqDVKQbdKV1IT7nojJDrGvFWCKW1GbL40vFDEmoBVJwudhPiNuqnPcsRAojRrPoXz+voPs4c+Q3hyyaZp5anTDZNj1q/WcfH1q4Ly9njTgwScamlU+pLV52fIMPRg5cp0zF1+kgc+3LxBbAiMf3rSBlvEc+Ndr/EBN0KiCnriJsDqtS3HjXjsP0lUEH5LyW+Kp8hSr4Z73CRz+Aw==', 'jZJdbgMhDIQvVLObqFKiSlGPsiLgBKv8FUxpbt8NaSvlYQWv8I3tsccwx7dpqrUKJ7/lmbRQQZSP6cVs/ciimIJfrimUmEU08f3paSF9OnTlGS4ouSTUQIwuC8PObqtiScrIjLBO2GPJX0Jy8j5kp2o2IUbyV1AycYf99ZghyUgamBymUYkrlilahErej8tsqJgZiqfP0pyP9vsKRRlMvUX9WzoXry0O4+t6YS0/Ok3DyY3i650T8W0Mf0phy+UpoULP9rag16i3I7WbW5jgcOy0emxnD8fXDrib57+a+4b+AA==', '?,&,?,?,?,?,?,?,?,?,?,?,?,?,&,?,?,?', '2013-11-03 16:54:21');
drop table if exists seo_exclude;
create table seo_exclude (
  seo_exclude_key varchar(32) not null ,
  seo_exclude_script varchar(64) not null ,
  seo_exclude_params varchar(255) ,
  PRIMARY KEY (seo_exclude_key)
);

insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('0354a1fa71096ec8d93521a17d9aaed8', 'address_book_process.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('07d2009d614db091bea595130306359a', 'account.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('13dae7d4d9c5a71489a5d3523e16b1b5', 'advanced_search_result.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('336d3861f98bb3f159b5dedaac02752a', 'checkout_shipping_address.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('3af26970edb0baf84534601f700d8fce', 'checkout_shipping.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('3caf70d85e698d3c5ea45e768e607fc5', 'account_notifications.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('43a84ecf1cb41160c6b7606118c8b9d1', 'account_edit.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('47a0938398cbb0753da7c6818b25d71e', 'checkout.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('72f3f5d50b3609fb45f42e9ec5c3f897', 'address_book.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('7dd0084da8e5e251d48bf7610d1f39fe', 'checkout_success.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('8a130abac7552d4eacedcfc42c842510', 'account_history.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('8b27695473c9fdb061fe2a0d1f3d59d6', 'checkout_process.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('8fc56af6321dae6786052974a43c041d', 'ajax_control.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('98ae1a97d3ff0626ef3ef5d671ec798f', 'checkout_payment.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('9d1c0a93eba9d0eb3d09511f49e06d0e', 'auction_callback.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('b908a63ec43fb25dbce18b3df064d4c6', 'fly_thumb.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('d4253a0dc07fc73d332aa67b9b3b42cc', 'account_password.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('dab63f8d8a7c9f1b2217b9681a379cc4', 'account_history_info.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('dcfe702546310dad8220cbb16d0ee983', 'checkout_payment_address.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('dd323aa110c0d21fe5acdafe3da021bc', 'checkout_confirmation.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('e0f745105a7f3d071b78a91e7e2ed1ea', 'account_newsletters.php', NULL);
insert into seo_exclude (seo_exclude_key, seo_exclude_script, seo_exclude_params) values ('fc8d4bb3bf7f56ca700507fee0558c16', 'contact_us.php', NULL);
drop table if exists seo_frequency;
create table seo_frequency (
  seo_frequency_id int(11) not null auto_increment,
  seo_frequency_name varchar(32) not null ,
  PRIMARY KEY (seo_frequency_id)
);

insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('1', 'always');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('2', 'hourly');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('3', 'daily');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('4', 'weekly');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('5', 'monthly');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('6', 'yearly');
insert into seo_frequency (seo_frequency_id, seo_frequency_name) values ('7', 'never');
drop table if exists seo_languages;
create table seo_languages (
  language_id int(11) default '1' not null ,
  language_text text not null ,
  PRIMARY KEY (language_id)
);

drop table if exists seo_redirect;
create table seo_redirect (
  seo_url_key varchar(32) not null ,
  seo_url_get text not null ,
  seo_url_org text not null ,
  seo_redirect int(3) default '301' not null ,
  seo_url_hits int(11) default '0' not null ,
  last_modified date not null ,
  PRIMARY KEY (seo_url_key)
);

drop table if exists seo_to_abstract;
create table seo_to_abstract (
  abstract_zone_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (abstract_zone_id)
);

insert into seo_to_abstract (abstract_zone_id, seo_name) values ('7', 'menu-zones');
insert into seo_to_abstract (abstract_zone_id, seo_name) values ('11', 'information');
drop table if exists seo_to_auctions;
create table seo_to_auctions (
  auctions_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (auctions_id)
);

insert into seo_to_auctions (auctions_id, seo_name) values ('1', '10-bids-1');
insert into seo_to_auctions (auctions_id, seo_name) values ('2', '100-bids-2');
insert into seo_to_auctions (auctions_id, seo_name) values ('3', 'bundle-1-3');
insert into seo_to_auctions (auctions_id, seo_name) values ('4', '500-bids-4');
insert into seo_to_auctions (auctions_id, seo_name) values ('22', 'bundle-1-22');
insert into seo_to_auctions (auctions_id, seo_name) values ('23', '10-bids-23');
insert into seo_to_auctions (auctions_id, seo_name) values ('24', '1000-bids-24');
insert into seo_to_auctions (auctions_id, seo_name) values ('25', '500-bids-25');
insert into seo_to_auctions (auctions_id, seo_name) values ('26', 'bundle2-26');
insert into seo_to_auctions (auctions_id, seo_name) values ('27', '10-bids-27');
insert into seo_to_auctions (auctions_id, seo_name) values ('28', '50-bids-28');
insert into seo_to_auctions (auctions_id, seo_name) values ('29', '500-bids-29');
insert into seo_to_auctions (auctions_id, seo_name) values ('30', 'bundle2-30');
insert into seo_to_auctions (auctions_id, seo_name) values ('31', '1000-bids-31');
insert into seo_to_auctions (auctions_id, seo_name) values ('32', '10-bids-32');
insert into seo_to_auctions (auctions_id, seo_name) values ('33', 'bundle2-33');
insert into seo_to_auctions (auctions_id, seo_name) values ('34', '10-bids-34');
insert into seo_to_auctions (auctions_id, seo_name) values ('35', '100-bids-35');
insert into seo_to_auctions (auctions_id, seo_name) values ('36', '1000-bids-36');
insert into seo_to_auctions (auctions_id, seo_name) values ('37', '50-bids-37');
insert into seo_to_auctions (auctions_id, seo_name) values ('38', '100-bids-38');
insert into seo_to_auctions (auctions_id, seo_name) values ('39', '1000-bids-39');
insert into seo_to_auctions (auctions_id, seo_name) values ('40', '50-bids-40');
insert into seo_to_auctions (auctions_id, seo_name) values ('41', '500-bids-41');
insert into seo_to_auctions (auctions_id, seo_name) values ('42', '100-bids-42');
insert into seo_to_auctions (auctions_id, seo_name) values ('43', '1000-bids-43');
insert into seo_to_auctions (auctions_id, seo_name) values ('44', '10-bids-44');
insert into seo_to_auctions (auctions_id, seo_name) values ('45', '10-bids-45');
insert into seo_to_auctions (auctions_id, seo_name) values ('46', '500-bids-46');
insert into seo_to_auctions (auctions_id, seo_name) values ('47', '1000-bids-47');
insert into seo_to_auctions (auctions_id, seo_name) values ('48', 'bundle2-48');
insert into seo_to_auctions (auctions_id, seo_name) values ('49', '50-bids-49');
insert into seo_to_auctions (auctions_id, seo_name) values ('50', '500-bids-50');
insert into seo_to_auctions (auctions_id, seo_name) values ('51', '50-bids-51');
insert into seo_to_auctions (auctions_id, seo_name) values ('53', '100-bids-53');
insert into seo_to_auctions (auctions_id, seo_name) values ('54', 'bundle-1-54');
insert into seo_to_auctions (auctions_id, seo_name) values ('55', '10-bids-55');
insert into seo_to_auctions (auctions_id, seo_name) values ('56', '100-bids-56');
insert into seo_to_auctions (auctions_id, seo_name) values ('57', '50-bids-57');
insert into seo_to_auctions (auctions_id, seo_name) values ('58', 'bundle-1-58');
insert into seo_to_auctions (auctions_id, seo_name) values ('59', '1000-bids-59');
insert into seo_to_auctions (auctions_id, seo_name) values ('60', '50-bids-60');
insert into seo_to_auctions (auctions_id, seo_name) values ('61', '100-bids-61');
insert into seo_to_auctions (auctions_id, seo_name) values ('62', '10-bids-62');
insert into seo_to_auctions (auctions_id, seo_name) values ('63', '50-bids-63');
insert into seo_to_auctions (auctions_id, seo_name) values ('64', '10-bids-64');
insert into seo_to_auctions (auctions_id, seo_name) values ('65', '10-bids-65');
insert into seo_to_auctions (auctions_id, seo_name) values ('66', '100-bids-66');
insert into seo_to_auctions (auctions_id, seo_name) values ('67', '500-bids-67');
insert into seo_to_auctions (auctions_id, seo_name) values ('68', '500-bids-68');
insert into seo_to_auctions (auctions_id, seo_name) values ('69', '10-bids-69');
insert into seo_to_auctions (auctions_id, seo_name) values ('70', '100-bids-70');
insert into seo_to_auctions (auctions_id, seo_name) values ('71', '500-bids-71');
insert into seo_to_auctions (auctions_id, seo_name) values ('72', '1000-bids-72');
insert into seo_to_auctions (auctions_id, seo_name) values ('73', '10-bids-73');
insert into seo_to_auctions (auctions_id, seo_name) values ('74', '500-bids-74');
insert into seo_to_auctions (auctions_id, seo_name) values ('75', '1000-bids-75');
insert into seo_to_auctions (auctions_id, seo_name) values ('76', '50-bids-76');
insert into seo_to_auctions (auctions_id, seo_name) values ('77', '500-bids-77');
insert into seo_to_auctions (auctions_id, seo_name) values ('78', '10-bids-78');
insert into seo_to_auctions (auctions_id, seo_name) values ('79', '500-bids-79');
insert into seo_to_auctions (auctions_id, seo_name) values ('80', 'bundle-1-80');
insert into seo_to_auctions (auctions_id, seo_name) values ('81', '100-bids-81');
insert into seo_to_auctions (auctions_id, seo_name) values ('82', '250-bids-82');
insert into seo_to_auctions (auctions_id, seo_name) values ('83', '750-bids-83');
insert into seo_to_auctions (auctions_id, seo_name) values ('84', 'bundle2-84');
insert into seo_to_auctions (auctions_id, seo_name) values ('86', '10-bids-86');
insert into seo_to_auctions (auctions_id, seo_name) values ('89', '750-bids-89');
insert into seo_to_auctions (auctions_id, seo_name) values ('90', '10-bids-90');
insert into seo_to_auctions (auctions_id, seo_name) values ('91', '250-bids-91');
insert into seo_to_auctions (auctions_id, seo_name) values ('92', '750-bids-92');
insert into seo_to_auctions (auctions_id, seo_name) values ('93', '750-bids-93');
drop table if exists seo_to_auctions_group;
create table seo_to_auctions_group (
  auctions_group_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (auctions_group_id)
);

insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('1', 'auctions-featured-items');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('2', 'auctions-lowest-unique-bid');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('3', 'auctions-rapid-timer');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('4', 'auctions-bundles');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('5', 'auctions-multiple-winner');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('6', 'auctions-charity');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('7', 'auctions-bids-pack');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('8', 'auctions-vouchers');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('9', 'auctions-for-him');
insert into seo_to_auctions_group (auctions_group_id, seo_name) values ('10', 'auctions-for-her');
drop table if exists seo_to_categories;
create table seo_to_categories (
  categories_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (categories_id)
);

insert into seo_to_categories (categories_id, seo_name) values ('8', 'auction-items');
insert into seo_to_categories (categories_id, seo_name) values ('9', 'purchase-bids');
insert into seo_to_categories (categories_id, seo_name) values ('10', 'bundles');
drop table if exists seo_to_filters;
create table seo_to_filters (
  products_filter_id int(11) default '0' not null ,
  products_select_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (products_filter_id, products_select_id)
);

insert into seo_to_filters (products_filter_id, products_select_id, seo_name) values ('1', '1', '3-tickets');
insert into seo_to_filters (products_filter_id, products_select_id, seo_name) values ('2', '1', 'wed-sat');
insert into seo_to_filters (products_filter_id, products_select_id, seo_name) values ('2', '2', 'fri');
drop table if exists seo_to_gtext;
create table seo_to_gtext (
  gtext_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (gtext_id)
);

insert into seo_to_gtext (gtext_id, seo_name) values ('1', 'faq');
insert into seo_to_gtext (gtext_id, seo_name) values ('2', 'about-us');
insert into seo_to_gtext (gtext_id, seo_name) values ('4', 'privacy-notice');
insert into seo_to_gtext (gtext_id, seo_name) values ('5', 'terms-conditions');
insert into seo_to_gtext (gtext_id, seo_name) values ('7', 'affiliates');
insert into seo_to_gtext (gtext_id, seo_name) values ('8', 'promotions');
insert into seo_to_gtext (gtext_id, seo_name) values ('18', 'corporate');
insert into seo_to_gtext (gtext_id, seo_name) values ('20', 'shipping-returns');
insert into seo_to_gtext (gtext_id, seo_name) values ('21', 'tracking-returns');
insert into seo_to_gtext (gtext_id, seo_name) values ('22', 'auction-formats');
insert into seo_to_gtext (gtext_id, seo_name) values ('23', 'common-questions');
insert into seo_to_gtext (gtext_id, seo_name) values ('24', 'points-program');
insert into seo_to_gtext (gtext_id, seo_name) values ('25', 'gift-vouchers');
insert into seo_to_gtext (gtext_id, seo_name) values ('26', 'bidding-questions');
insert into seo_to_gtext (gtext_id, seo_name) values ('27', 'winning-questions');
insert into seo_to_gtext (gtext_id, seo_name) values ('28', 'payment-questions');
insert into seo_to_gtext (gtext_id, seo_name) values ('29', 'fgfg');
drop table if exists seo_to_manufacturers;
create table seo_to_manufacturers (
  manufacturers_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (manufacturers_id)
);

drop table if exists seo_to_products;
create table seo_to_products (
  products_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (products_id)
);

insert into seo_to_products (products_id, seo_name) values ('93', 'bundle2');
insert into seo_to_products (products_id, seo_name) values ('94', 'bundle1');
insert into seo_to_products (products_id, seo_name) values ('95', '10-bids');
insert into seo_to_products (products_id, seo_name) values ('96', '100-bids');
insert into seo_to_products (products_id, seo_name) values ('97', '1000-bids');
insert into seo_to_products (products_id, seo_name) values ('98', '50-bids');
insert into seo_to_products (products_id, seo_name) values ('99', '500-bids');
insert into seo_to_products (products_id, seo_name) values ('100', '250-bids');
insert into seo_to_products (products_id, seo_name) values ('101', '750-bids');
drop table if exists seo_to_ranges;
create table seo_to_ranges (
  numeric_ranges_id int(11) default '0' not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (numeric_ranges_id)
);

insert into seo_to_ranges (numeric_ranges_id, seo_name) values ('1', 'up-to-100');
insert into seo_to_ranges (numeric_ranges_id, seo_name) values ('2', '100-200');
insert into seo_to_ranges (numeric_ranges_id, seo_name) values ('3', '200-299');
insert into seo_to_ranges (numeric_ranges_id, seo_name) values ('4', '300');
drop table if exists seo_to_scripts;
create table seo_to_scripts (
  script varchar(255) not null ,
  seo_name varchar(255) not null ,
  PRIMARY KEY (script)
);

drop table if exists seo_types;
create table seo_types (
  seo_types_id int(11) not null auto_increment,
  seo_types_name varchar(32) not null ,
  seo_types_handler varchar(255) ,
  seo_types_subfix varchar(255) ,
  seo_types_class varchar(64) not null ,
  seo_types_prefix varchar(64) not null ,
  seo_types_linkage int(3) default '1' not null ,
  sort_order int(3) default '1' not null ,
  seo_types_status tinyint(1) default '1' not null ,
  PRIMARY KEY (seo_types_id)
);

insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('1', 'Products', '', '', 'seo_products', '', '1', '5', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('2', 'Categories', '', '', 'seo_categories', '', '2', '6', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('3', 'Manufacturers', '', '', 'seo_manufacturers', '', '3', '7', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('9', 'Generic Text', '', '', 'seo_gtext', '', '20', '19', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('10', 'Abstract Zones', '', '', 'seo_abstract', '', '21', '20', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('11', 'Numeric Ranges', '', '', 'seo_ranges', 'range-', '25', '35', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('12', 'Scripts', '', '', 'seo_scripts', '', '999', '999', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('13', 'Product Filters', '', '', 'seo_filters', '', '4', '8', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('14', 'Auctions', '', '', 'seo_auctions', '', '5', '11', '1');
insert into seo_types (seo_types_id, seo_types_name, seo_types_handler, seo_types_subfix, seo_types_class, seo_types_prefix, seo_types_linkage, sort_order, seo_types_status) values ('15', 'Auction Groups', '', '', 'seo_auctions_group', '', '6', '12', '1');
drop table if exists seo_url;
create table seo_url (
  auto_id int(11) not null auto_increment,
  seo_url_key varchar(32) not null ,
  seo_url_get text not null ,
  osc_url_key varchar(32) not null ,
  seo_url_org text not null ,
  seo_url_hits int(11) default '0' not null ,
  seo_url_priority decimal(2,1) default '0.5' not null ,
  seo_frequency_id int(11) default '4' not null ,
  date_added date not null ,
  last_modified datetime not null ,
  PRIMARY KEY (auto_id),
  KEY idx_osc_url_key (osc_url_key),
  KEY idx_seo_url_key (seo_url_key),
  KEY idx_last_modified (last_modified),
  KEY idx_date_added (date_added)
);

insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('4', 'dc87ee9a38456aa04bda51dac1bda224', 'http://www.maxabid.co.uk/shopping-cart.html', 'a5dd5245a7ba414ade0997ff62e024d4', 'http://www.maxabid.co.uk/shopping_cart.php', '0', '0.5', '4', '2012-01-14', '2013-11-01 16:24:25');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('5', '401596e51c7b9c22384b1f10162e658e', 'http://www.maxabid.co.uk/corporate.html', '166db0405fe7f33fd68e4345b8842db1', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=18', '0', '0.5', '4', '2012-01-14', '2012-05-04 10:02:54');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('6', '8c90f4c67965b360f5b07dff302632e5', 'http://www.maxabid.co.uk/privacy-notice.html', '913da9fd9cac4c6343565c467c9cd6ce', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=4', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('7', 'd2cd0ee57dc12d290b2da07694522ca4', 'http://www.maxabid.co.uk/terms-conditions.html', '944861d48ba0e36b7def0f0022471cf6', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=5', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('8', 'c011d5e1458a9c9b6e62d1c434bb7499', 'http://www.maxabid.co.uk/affiliates.html', '6dca5d270f2e0d49b5e20cdcd505f5af', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=7', '0', '0.5', '4', '2012-01-14', '2012-05-04 10:02:54');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('9', '8f1c327f51cc14c5435b8a6d721875d5', 'http://www.maxabid.co.uk/about-us.html', 'f2d9959aa10b34d5d1daf961f83777c7', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=2', '0', '0.5', '4', '2012-01-14', '2013-04-30 10:21:15');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('10', 'bb57373a36f54d122a92d3e9da86c761', 'http://www.maxabid.co.uk/shipping-returns.html', '0651d190d8d28646bf07f7d1d5adb452', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=20', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('11', 'ea44427604160eb32e5cd59bca379f75', 'http://www.maxabid.co.uk/tracking-returns.html', '59e019d6b3b6db5dd1f7a2872c15f4f5', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=21', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('12', 'fc95aed78650dcaed27ae7ce2a14b464', 'http://www.maxabid.co.uk/auction-formats.html', 'fc8d7a697753c4479870186f2617531f', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=22', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('13', 'cc62c06196de53d311e3e86a26147e69', 'http://www.maxabid.co.uk/common-questions.html', '31aa799d183f935b3e068d75cc00292c', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=23', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('14', 'deafc305dd90a1822b182d4363c3b0b0', 'http://www.maxabid.co.uk/points-program.html', 'b296cf9cde90aab6341edb87d4192a0f', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=24', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('16', 'a68601338c14004fc39c2454dbd6ced2', 'http://www.maxabid.co.uk/bidding-questions.html', 'e60fb9e2037e44163290de42c35a3d25', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=26', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('17', '6c137046bf47caf1837689bc2a6048fc', 'http://www.maxabid.co.uk/winning-questions.html', '46f7d94e8c8454135ff1133d5f40e86f', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=27', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:50');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('18', '29e4782bcd32bf87443c53d53103d0f4', 'http://www.maxabid.co.uk/payment-questions.html', 'b6565a151a47779f40e67a7c6ee9cdf8', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=28', '0', '0.5', '4', '2012-01-14', '2013-11-03 16:46:50');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('42', '769014845615c1182ab2e86e525548dc', 'http://www.maxabid.co.uk/bundle2-33.html', '200a626d5cc38fd9f0b81e46e2ab119e', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=33', '0', '0.5', '4', '2012-01-14', '2012-04-17 10:23:57');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('43', '393e8f87250c28146c1b485552cd5611', 'http://www.maxabid.co.uk/bundle2.html', '068f46abd91a14e960c07a9592968291', 'http://www.maxabid.co.uk/product_info.php?products_id=93', '0', '0.5', '4', '2012-01-14', '2013-06-02 13:32:26');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('50', '2c54adb71b1b81898bddcb1eba285e5d', 'http://www.maxabid.co.uk/bundle2-30.html', 'f18083a76c0401e7136ed9ebde182b60', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=30', '0', '0.5', '4', '2012-01-14', '2013-05-25 20:43:36');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('102', 'e58c3f9e3f26cd25aa644716feaa7c26', 'http://www.maxabid.co.uk/bundle2-26.html', '95a688ef15b073884dc30aeae2a1949d', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=26', '0', '0.5', '4', '2012-01-18', '2012-05-02 11:32:36');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('104', '845aeee68deca6e4ac45aa7ae744196c', 'http://www.maxabid.co.uk/bundle-1-3.html', '8275c7e48d3de1cc8fe835e442b141a9', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=3', '0', '0.5', '4', '2012-01-18', '2012-05-02 11:32:36');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('106', '5660224827743ee6e17adf394e3bae9e', 'http://www.maxabid.co.uk/bundle1.html', 'f2ededa4bd4403de5ae1e2068b33dc5f', 'http://www.maxabid.co.uk/product_info.php?products_id=94', '0', '0.5', '4', '2012-01-18', '2013-10-05 10:37:19');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('137', '636c4b2765232f10f582b0b1c3e96f4f', 'http://www.maxabid.co.uk/account-coupons.html', 'bab08a0ef47a74b2a5f60e13fecb04f6', 'http://www.maxabid.co.uk/account_coupons.php', '0', '0.5', '4', '2012-04-12', '2012-04-12 11:55:58');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('192', 'cc71ccde92a892fc255683724f143772', 'http://www.maxabid.co.uk/purchase-bids.html', '970905878d1347bef2a96e791f1d872b', 'http://www.maxabid.co.uk/index.php?cPath=9', '0', '0.5', '4', '2012-04-13', '2013-11-01 16:24:25');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('193', '9815630a50875783813462a0e19ee1cb', 'http://www.maxabid.co.uk/1000-bids.html', '0208d2530062fe1fcc8ae9a5d6bdf813', 'http://www.maxabid.co.uk/product_info.php?products_id=97', '0', '0.5', '4', '2012-04-13', '2013-10-05 10:41:27');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('194', '97b9bb5c030926db8ee7a6be3b2cdf1b', 'http://www.maxabid.co.uk/500-bids.html', '06f31c01cab532178d9aa2da43da51cf', 'http://www.maxabid.co.uk/product_info.php?products_id=99', '0', '0.5', '4', '2012-04-13', '2013-10-05 10:38:54');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('195', '9cfa0945b29b58e5990ba450ae949369', 'http://www.maxabid.co.uk/100-bids.html', '1bbd1de02c287dbef62c4b2f9318a6a6', 'http://www.maxabid.co.uk/product_info.php?products_id=96', '0', '0.5', '4', '2012-04-13', '2013-10-05 10:53:38');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('196', 'cadb992747562a78e27341d92d83c9c5', 'http://www.maxabid.co.uk/50-bids.html', 'fb7f168103a4fd9b379b91405cac0bb9', 'http://www.maxabid.co.uk/product_info.php?products_id=98', '0', '0.5', '4', '2012-04-13', '2013-11-03 16:53:00');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('197', 'eff1c7f53fca05443f492fde26f90952', 'http://www.maxabid.co.uk/10-bids.html', '77316570a7711e8329ca53c6e3cedf6c', 'http://www.maxabid.co.uk/product_info.php?products_id=95', '0', '0.5', '4', '2012-04-13', '2013-10-05 10:53:39');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('199', 'b679d53e505204c1a43b87c7b821c9a6', 'http://www.maxabid.co.uk/100-bids-38.html', '8840603e187aeb64faa57c171aa733d7', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=38', '0', '0.5', '4', '2012-04-13', '2013-04-23 22:02:32');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('200', 'c09ff8914700414f4afb4567f66b5a85', 'http://www.maxabid.co.uk/1000-bids-39.html', 'addb06f64c236fc1363ce2b155176215', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=39', '0', '0.5', '4', '2012-04-13', '2013-10-05 11:13:15');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('201', 'c97aa8fc5f31c351ce58e1a34da56aa6', 'http://www.maxabid.co.uk/50-bids-40.html', 'cc91132e4110ee75d1ab698b7e52827a', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=40', '0', '0.5', '4', '2012-04-13', '2013-10-05 11:13:15');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('202', 'a8d8fe5747f000346eb542b0789407db', 'http://www.maxabid.co.uk/500-bids-41.html', '93152300267115d2b1df457c7c915702', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=41', '0', '0.5', '4', '2012-04-13', '2013-10-05 11:13:15');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('203', '954247805e1f8056444078bb6c721510', 'http://www.maxabid.co.uk/gift-vouchers.html', 'c2fa1e323009a125108424f92553149e', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=25', '0', '0.5', '4', '2012-04-13', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('204', '66cc13b84a763b4ddc75dc888f9b0d67', 'http://www.maxabid.co.uk/100-bids-2.html', '93a946296d82d866ddba1596e50f7ab0', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=2', '0', '0.5', '4', '2012-04-13', '2012-09-23 23:24:24');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('205', 'f2bb91f87f170e38d385dbd97993df2b', 'http://www.maxabid.co.uk/50-bids-37.html', '0d830ccf06308bf680fce22069578b3d', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=37', '0', '0.5', '4', '2012-04-13', '2012-04-17 10:23:57');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('206', '3382069eb37089c57e0a9c76b6e295f7', 'http://www.maxabid.co.uk/1000-bids-43.html', '62379fb869566a33fe6cc9bdb9e4714c', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=43', '0', '0.5', '4', '2012-04-13', '2012-04-13 11:41:02');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('207', '0631d1a56a11cb01e1810c21dd7644ee', 'http://www.maxabid.co.uk/auction-groups.html', '5a6471a2e35d8bb5c1758e0c92fdaad4', 'http://www.maxabid.co.uk/auction_groups.php', '0', '0.5', '4', '2012-04-13', '2013-10-05 10:50:12');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('208', '461ed2d50788578736cde74c4c258b76', 'http://www.maxabid.co.uk/information.html', 'd3aa491a71f570efff36c1010175b453', 'http://www.maxabid.co.uk/generic_pages.php?abz_id=11', '0', '0.5', '4', '2012-04-13', '2013-11-01 16:24:25');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('209', 'b884bb818f6452ab4732c63402648416', 'http://www.maxabid.co.uk/500-bids-25.html', 'd846595d0bdcdf885fa1f2bb3c365830', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=25', '0', '0.5', '4', '2012-04-14', '2013-04-02 09:46:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('210', '1c10aca8456168eee1f99c2cbb7b55ef', 'http://www.maxabid.co.uk/10-bids-45.html', '4d6a721e1f4e7c86134a33d13352ebdb', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=45', '0', '0.5', '4', '2012-04-14', '2013-10-05 11:06:44');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('211', 'def75a030b1650292bf2ec1bd73864a2', 'http://www.maxabid.co.uk/100-bids-42.html', '9af9b74bdef95945aeeff26b6a3c3891', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=42', '0', '0.5', '4', '2012-04-14', '2013-06-09 14:24:45');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('212', '380a82bcf04857a69a9b7a8ca662fb98', 'http://www.maxabid.co.uk/10-bids-44.html', '9b85829158a6005ad9897112b3033583', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=44', '0', '0.5', '4', '2012-04-14', '2013-10-05 10:44:38');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('213', '262042c41b71f9265c192487073c0ad9', 'http://www.maxabid.co.uk/10-bids-1.html', 'c7b8eb4ca6bf20e30387326c23fdaed4', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=1', '0', '0.5', '4', '2012-04-14', '2012-09-23 23:24:24');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('214', '04e31a444de2b0f9fa2c752a161bf1a1', 'http://www.maxabid.co.uk/auction-info.html', '9b2385e47a77c81c50480c6d4add20fd', 'http://www.maxabid.co.uk/auction_info.php', '0', '0.5', '4', '2012-04-14', '2013-06-09 15:04:06');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('215', 'efb505b77c49294a9c2bb3c0df027947', 'http://www.maxabid.co.uk/10-bids-23.html', 'ae04e327eef2113f944d1f5c300e98e1', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=23', '0', '0.5', '4', '2012-04-14', '2013-04-02 09:46:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('216', 'bc4e7db0502ad4dc228e4a9ebf2c5eb8', 'http://www.maxabid.co.uk/1000-bids-24.html', '119bb8ddcad545a72d681d47757bc672', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=24', '0', '0.5', '4', '2012-04-14', '2012-09-23 23:24:24');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('217', '5c5e8f8fb65de167c12647fdb4476302', 'http://www.maxabid.co.uk/10-bids-32.html', 'e76b5e00dcf6913eb767b257aa965c7c', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=32', '0', '0.5', '4', '2012-04-14', '2013-06-09 20:43:46');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('218', '6c17528b23255a17c98033114baf6434', 'http://www.maxabid.co.uk/50-bids-28.html', 'ed0820d804d04e628bbaab719d8e64bc', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=28', '0', '0.5', '4', '2012-04-16', '2013-07-16 13:10:58');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('219', '4cb8e7319296420f5cba7295f8bf1fda', 'http://www.maxabid.co.uk/500-bids-29.html', '5a751ca45d875b820e7771eec04b8b8f', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=29', '0', '0.5', '4', '2012-04-16', '2013-07-16 13:10:58');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('220', 'eff8c0553dd3fa51540618aa323921ac', 'http://www.maxabid.co.uk/1000-bids-31.html', 'a4b0b350ad60785b51d6dc288a10a171', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=31', '0', '0.5', '4', '2012-04-16', '2013-06-09 14:44:52');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('221', '4ed2d6118e78ed4be3b1108a36cc8e18', 'http://www.maxabid.co.uk/fgfg.html', '8879c097c9349585dc2db862a542cdc7', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=29', '0', '0.5', '4', '2012-04-17', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('222', '1132510645b7351091a17f0b1616f7dc', 'http://www.maxabid.co.uk/faq.html', '0c500352f68741f1577305786a33c949', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=1', '0', '0.5', '4', '2012-04-17', '2013-11-03 16:46:49');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('224', '8bd8f0c48be9e339e791b8e875aceff1', 'http://www.maxabid.co.uk/cookie-usage.html', '23a858e67a8a67fb71ff4a9133f8e173', 'http://www.maxabid.co.uk/cookie_usage.php', '0', '0.5', '4', '2012-04-17', '2013-10-05 14:59:25');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('225', 'e41a3d5166815992b386cc08e48a6f2d', 'http://www.maxabid.co.uk/sitemap.html', '006a10fdb0a52098b247678a87333cc0', 'http://www.maxabid.co.uk/sitemap.php', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('226', 'a33cbe6d827357122353893144d91c81', 'http://www.maxabid.co.uk/reviews.html', '024eab8d07a6ea7523c25a3d072bc52b', 'http://www.maxabid.co.uk/reviews.php', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('231', '96f026f089e0fee809b706980384f2ce', 'http://www.maxabid.co.uk/documentation.html', 'e1ac071c027a0845321a6ecae3a8c2a7', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=10', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('232', '2dd49b772bc637e4ac4a65a96fb33b99', 'http://www.maxabid.co.uk/home-page.html', '414fc888ce758d89ceb53753293ef00a', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=6', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('235', 'a2a35b7c53a4fdfec09cec1f351e4424', 'http://www.maxabid.co.uk/lottery-results.html', 'fbfb0fc7d713e45b12bf1387a2c22864', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=9', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('236', 'ecf13531ebcf87332f8fb64272f5a2df', 'http://www.maxabid.co.uk/promotions.html', '5f60b1b367b42617f458b9eb468408c8', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=8', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('243', 'bf2eddacae2f73787bc096f0d4f024fe', 'http://www.maxabid.co.uk/auction-items.html', '39a519914193b0d8cf24f04d78a3e9c9', 'http://www.maxabid.co.uk/index.php?cPath=8', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('244', 'd8f038dace11b6b31f9fa4562f9645a1', 'http://www.maxabid.co.uk/bundles.html', 'b4e06582877cb19ec552277fa398b0cc', 'http://www.maxabid.co.uk/index.php?cPath=8_10', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('245', '59ad97dd3bc35760d98fca385c2d9c17', 'http://www.maxabid.co.uk/3-tickets.html', 'b0483a5f4d5a65e002c6e14894b3fb3b', 'http://www.maxabid.co.uk/shop_by_filter.php?buyout=1', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('247', 'a0fe05f7b0d09da8a66cd55badb0051b', 'http://www.maxabid.co.uk/fri.html', '6f4493c2291a43f4e90e5fd71d7ccdd9', 'http://www.maxabid.co.uk/shop_by_filter.php?play_times=2', '0', '0.5', '4', '2012-04-19', '2012-04-19 12:57:05');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('254', 'a679f1f54a7f6cfdcdc76a8445c6c042', 'http://www.maxabid.co.uk/500-bids-46.html', '7cb785a4918175f57605caf8eb7e6bf4', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=46', '0', '0.5', '4', '2012-04-19', '2013-07-16 13:02:14');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('255', '0eff393a3efb1dcde51e3f013fb23cdf', 'http://www.maxabid.co.uk/10-bids-34.html', 'e7fee4bde188a958bf0b0263d240fd06', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=34', '0', '0.5', '4', '2012-04-19', '2012-05-02 09:55:52');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('256', '75e04b6e54872bd2652616fb07f7cf68', 'http://www.maxabid.co.uk/100-bids-35.html', '85130c65a567deb5f9c0191825c5dda0', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=35', '0', '0.5', '4', '2012-04-19', '2013-07-16 13:02:14');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('257', '309d5f8d112966fe616e0d67e706c2d3', 'http://www.maxabid.co.uk/1000-bids-36.html', '2cc14daf537cfe6883cd989cdf68a7bd', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=36', '0', '0.5', '4', '2012-04-19', '2013-06-09 14:44:48');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('258', '0e1d7234b7b6289939c91aba8f434283', 'http://www.maxabid.co.uk/1000-bids-47.html', 'ed283ecbf30e0ace6f60afc7e0a891dd', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=47', '0', '0.5', '4', '2012-04-19', '2013-07-16 13:02:14');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('260', '258c6a72209854d28ff84e6563d49ae3', 'http://www.maxabid.co.uk/auctions-featured-items.html', '4064a0f87e012633554d880859d76af1', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=1', '0', '0.5', '4', '2012-04-19', '2013-11-01 16:24:25');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('261', '18f75b80cb0d68f4a8653bc8073ebd88', 'http://www.maxabid.co.uk/auctions-rapid-timer.html', '2f356639bb4d446bea7bcb1233b2f294', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=3', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('262', '50e5bbef6fd94810a670f30765279ba8', 'http://www.maxabid.co.uk/auctions-bids-pack.html', '7e33ba167ab3e235a634762971300469', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=7', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('263', '96cd4adca687e69afb85dfeaed396a31', 'http://www.maxabid.co.uk/auctions-multiple-winner.html', 'd3fd98f305cc3380deaa7f40bded32b2', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=5', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('264', 'e62063f38e49b9bebed63a4e178ca4be', 'http://www.maxabid.co.uk/auctions-lowest-unique-bid.html', 'e7b91fce1f050509ecf2b3ac75fab507', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=2', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('265', '90f0fcc4a473da6b14bcd600f760afbf', 'http://www.maxabid.co.uk/auctions-vouchers.html', '958f5ae12826f92c49590cf408e9d846', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=8', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('266', '85f535c383fef1b83d985160850f73a7', 'http://www.maxabid.co.uk/auctions-bundles.html', 'd500ea3d515d3c2e7f0a2319016a2490', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=4', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('267', '475b34e6fe939a323e3157186e5922de', 'http://www.maxabid.co.uk/auctions-for-her.html', '7867ee47655eb771a102962d1bb29f87', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=10', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('268', '8be5c72812201bc8f0816196146ca10f', 'http://www.maxabid.co.uk/auctions-for-him.html', 'b137877be5c9e26ed2ee3aa5f864e375', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=9', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('269', '15da6f720061a774f505f3b292d8b67b', 'http://www.maxabid.co.uk/auctions-charity.html', 'af79e9294ae0bf795b9603e1d760d354', 'http://www.maxabid.co.uk/auction_groups.php?auctions_group_id=6', '0', '0.5', '4', '2012-04-19', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('270', '3ae4d922a4c117377cc8db36caa8c61d', 'http://www.maxabid.co.uk/generic-pages.html', 'fa5a67042d013a1a3c8c7f2ef3b63591', 'http://www.maxabid.co.uk/generic_pages.php?gtext_id=', '0', '0.5', '4', '2012-05-01', '2012-05-01 11:44:57');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('271', '23df61e8b594787c92db14cd865d9cb8', 'http://www.maxabid.co.uk/bundle-1-22.html', 'fadc961de63b34471e3a91d215b2ed6e', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=22', '0', '0.5', '4', '2012-05-02', '2013-04-02 09:46:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('272', '98d99479d12ab3e082cb96be175b8c40', 'http://www.maxabid.co.uk/testimonials.html', '2b632d594b12d1ec34e1218a32264031', 'http://www.maxabid.co.uk/testimonials.php', '0', '0.5', '4', '2012-05-03', '2013-04-08 13:34:46');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('273', 'f113d2ddb6b0fa99cd9429b64edf41c4', 'http://www.maxabid.co.uk/bundle2-48.html', 'e0ef5e5891136b440a0a3fa2f9b583d6', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=48', '0', '0.5', '4', '2012-05-09', '2013-06-09 14:24:45');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('274', '17377a5cf795ee8cef623957fe53de0a', 'http://www.maxabid.co.uk/50-bids-49.html', '051c5a71bf939be2f2f0714400864512', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=49', '0', '0.5', '4', '2012-05-09', '2013-04-10 12:00:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('275', 'c7418fb4766d7e9dcefac6e44fc5518a', 'http://www.maxabid.co.uk/500-bids-50.html', 'fe7c55a50f93572823898830d6374235', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=50', '0', '0.5', '4', '2012-05-09', '2013-10-05 10:38:48');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('276', '43a1383113bc6aad63ff153acc0463c5', 'http://www.maxabid.co.uk/50-bids-51.html', '4ba15a4eefb93797f1f3adbc17d8449a', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=51', '0', '0.5', '4', '2012-05-09', '2013-04-02 09:46:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('277', '6eda88d2c5e40e559aff84c124b892c7', 'http://www.maxabid.co.uk/100-bids-53.html', '7340d226851e2fa86cf30623b60bafdb', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=53', '0', '0.5', '4', '2012-09-26', '2013-05-02 19:50:56');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('278', '03d6cddeb2f9f1e5b4af964c7484d219', 'http://www.maxabid.co.uk/bundle-1-54.html', '83ccea697c33541758d4d6f7e4851397', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=54', '0', '0.5', '4', '2012-09-26', '2013-05-25 20:42:23');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('279', 'b764fc0b15cdacf2ddd612004c5f92d7', 'http://www.maxabid.co.uk/10-bids-55.html', 'dd7228436306331ea413efec07435824', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=55', '0', '0.5', '4', '2012-09-26', '2013-04-10 12:00:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('280', '5e049a78e772ef134096811ddd6ee487', 'http://www.maxabid.co.uk/100-bids-56.html', '9849748be63c540faec404e3d43c5925', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=56', '0', '0.5', '4', '2012-09-26', '2013-05-25 20:42:23');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('281', '7f0f11013d77cb4b776da3972d81a7b2', 'http://www.maxabid.co.uk/50-bids-57.html', '077b7efe39b3828c648e33fdd0e5c8d7', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=57', '0', '0.5', '4', '2012-09-26', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('282', 'f48ef9cb2be0a2297946c8f05dcb8375', 'http://www.maxabid.co.uk/1000-bids-59.html', '0b17fb05907e594ed083271eb4293558', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=59', '0', '0.5', '4', '2013-04-03', '2013-04-03 18:01:47');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('283', 'fb435800b8796e8ae894e44c4dddc08b', 'http://www.maxabid.co.uk/50-bids-60.html', '41ae29a76a84207a8a00f1d4ef239e70', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=60', '0', '0.5', '4', '2013-04-03', '2013-04-03 18:06:18');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('284', 'a839830c944ce808f81020a2a772ff90', 'http://www.maxabid.co.uk/100-bids-61.html', '7bc5720c2203b7d871b05082de79d528', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=61', '0', '0.5', '4', '2013-04-03', '2013-04-03 18:09:41');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('285', 'fa3bb3180730827da99d444c29ddd9c4', 'http://www.maxabid.co.uk/10-bids-62.html', 'cbdfe2546a88f622cc4208f39b1d7ddf', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=62', '0', '0.5', '4', '2013-04-03', '2013-04-03 18:12:50');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('286', 'fe8e4bf52de8e90199b293ccb87b2357', 'http://www.maxabid.co.uk/50-bids-63.html', '059508ac8f5f2ce883f713112fdb81f9', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=63', '0', '0.5', '4', '2013-04-03', '2013-05-13 09:34:48');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('287', '8c81380785985e0500fc695b6378eb41', 'http://www.maxabid.co.uk/10-bids-64.html', 'b07256150fa3d5487139d87488001fab', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=64', '0', '0.5', '4', '2013-04-03', '2013-04-18 13:24:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('288', 'b77bf5f0405b0d0d187d50561e00edef', 'http://www.maxabid.co.uk/10-bids-65.html', '53ea875336950a9994e032cee2ddef02', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=65', '0', '0.5', '4', '2013-04-03', '2013-04-03 18:25:04');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('289', '18e6d172c210269680386eb04adad139', 'http://www.maxabid.co.uk/100-bids-66.html', 'c1b16811fa5f710068e63d72a7f1e152', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=66', '0', '0.5', '4', '2013-04-03', '2013-04-18 13:24:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('290', '2171ea529d7950745d82047a5f7916ae', 'http://www.maxabid.co.uk/500-bids-67.html', 'b42c4d58284dc6fedd65673ce2eb10f5', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=67', '0', '0.5', '4', '2013-04-03', '2013-04-18 13:24:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('291', 'e8fd11633202c1ef93757095e2af03a6', 'http://www.maxabid.co.uk/500-bids-68.html', '912f70af8af659fdf9c8083510bb6971', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=68', '0', '0.5', '4', '2013-04-03', '2013-04-18 13:24:03');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('292', '81481b9772ab5c429454aca5babe6b34', 'http://www.maxabid.co.uk/10-bids-69.html', '3ce43286e109821266f584d30ef6cd4b', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=69', '0', '0.5', '4', '2013-04-08', '2013-05-02 19:50:56');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('293', '22de7d055d925f75fbc63f5092866b76', 'http://www.maxabid.co.uk/100-bids-70.html', '7b43f81ef5d6d4495aa0482a38d01dec', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=70', '0', '0.5', '4', '2013-04-08', '2013-04-08 10:37:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('294', '0acb03b34ab3bf22071d3b0c12cf5b44', 'http://www.maxabid.co.uk/500-bids-71.html', '778f930f786ea155026eb08dd432f949', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=71', '0', '0.5', '4', '2013-04-08', '2013-06-09 20:51:14');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('295', '2191d75c149e4c6483e487a7d766b9a4', 'http://www.maxabid.co.uk/1000-bids-72.html', '754415f8535f880507946ae3de2f3387', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=72', '0', '0.5', '4', '2013-04-08', '2013-11-03 16:54:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('296', '0740a2023e2b9e0e6f2cf4490caf6d77', 'http://www.maxabid.co.uk/10-bids-73.html', 'ad6d602e8e77295824325f24fa577d82', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=73', '0', '0.5', '4', '2013-04-12', '2013-06-09 20:47:38');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('297', 'b3fab31a04c01020962f058b8ea59f34', 'http://www.maxabid.co.uk/500-bids-74.html', 'c439feee0feca923e6b7ce003053c1c5', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=74', '0', '0.5', '4', '2013-04-21', '2013-07-16 13:02:02');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('298', '5c9d0da7590e6e087d63e2f85a6320e2', 'http://www.maxabid.co.uk/bundle-1-58.html', '26e982f48700a1e773df4aee37a5b634', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=58', '0', '0.5', '4', '2013-04-22', '2013-11-03 16:48:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('299', 'b44dea045ec8bd2f0176d5a86a271b85', 'http://www.maxabid.co.uk/1000-bids-75.html', '993d2c94252fc14921e9861ac12c2ca1', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=75', '0', '0.5', '4', '2013-04-23', '2013-07-16 13:11:01');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('300', 'c949ed7d5a2a351e27a9a08b32236ead', 'http://www.maxabid.co.uk/50-bids-76.html', '13753db951f05c1d6510ed8a4ad92573', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=76', '0', '0.5', '4', '2013-04-23', '2013-05-02 22:33:57');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('301', '2fb9538244ea87e34eaa92ecb40cd37d', 'http://www.maxabid.co.uk/500-bids-77.html', '2678b99b3da3b1303ef1ac049658ea53', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=77', '0', '0.5', '4', '2013-04-26', '2013-07-16 13:11:01');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('302', 'cf4e229025e0028b62c265a5d7228666', 'http://www.maxabid.co.uk/10-bids-78.html', 'f9621d48189f0aed605a63eb0958f5d1', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=78', '0', '0.5', '4', '2013-04-26', '2013-11-03 16:54:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('303', '2156ea1eda55aeb28f1b5bc4368751d0', 'http://www.maxabid.co.uk/500-bids-79.html', '3c7175a5ae758f3dae1aff74a95aa3bf', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=79', '0', '0.5', '4', '2013-04-28', '2013-07-16 13:02:17');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('304', '57b35ac9e2cce51b78b911f12f0e749e', 'http://www.maxabid.co.uk/250-bids.html', 'ce09b10ca02c0d897240d3c01017fc03', 'http://www.maxabid.co.uk/product_info.php?products_id=100', '0', '0.5', '4', '2013-05-01', '2013-10-05 10:53:38');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('305', '87f29426f09d4a8290d21deb142bf3f4', 'http://www.maxabid.co.uk/750-bids.html', 'd22ff96d41c17e8d356e65fb836c3ae8', 'http://www.maxabid.co.uk/product_info.php?products_id=101', '0', '0.5', '4', '2013-05-01', '2013-10-05 10:50:12');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('306', '2d0806d339d1b5791279e82626427c9c', 'http://www.maxabid.co.uk/bundle-1-80.html', '3080db3b99a55127f0cc0c29d186e446', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=80', '0', '0.5', '4', '2013-05-02', '2013-05-13 09:34:48');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('307', 'ee04a952a84b0619617f2ceb67227ea4', 'http://www.maxabid.co.uk/100-bids-81.html', 'cb2f6ed65ad07e09e8cc6f2ab5c54ba1', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=81', '0', '0.5', '4', '2013-05-02', '2013-05-25 20:43:37');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('308', 'fe28ebd8be17e2889fbf6086c78ec58e', 'http://www.maxabid.co.uk/250-bids-82.html', '731f5440384a287c02dbc82c315ee4a7', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=82', '0', '0.5', '4', '2013-05-02', '2013-07-16 13:02:16');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('309', '0d5c07a7a739361c2035542b13817def', 'http://www.maxabid.co.uk/750-bids-83.html', '33310dad3a2972d38ff970c48bbd4265', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=83', '0', '0.5', '4', '2013-05-02', '2013-07-16 13:02:16');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('311', 'f45f32db9e462fd3c4d8f1345fb1b26c', 'http://www.maxabid.co.uk/bundle2-84.html', '0564162a822f2e886abd5f28c8cc6b87', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=84', '0', '0.5', '4', '2013-05-08', '2013-11-03 16:54:21');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('312', 'cb69d4ab6e2f2cda3ec5b7e613616ae5', 'http://www.maxabid.co.uk/10-bids-86.html', '38e3d221fa47fbf752ed7e9bdc4d62c5', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=86', '0', '0.5', '4', '2013-05-25', '2013-06-09 14:24:45');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('314', 'cbad47045d6946f475b1cc3833232889', 'http://www.maxabid.co.uk/750-bids-89.html', 'e6235c16baae33f1e171f1f4ba370941', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=89', '0', '0.5', '4', '2013-05-27', '2013-06-09 14:24:45');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('315', '04cb85260d09bd560db6a098e4388a6c', 'http://www.maxabid.co.uk/10-bids-90.html', '4a15b346729ba465410579ad6a972017', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=90', '0', '0.5', '4', '2013-06-09', '2013-06-09 20:58:07');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('316', 'c7f5164726decf7b924163ac8e785efb', 'http://www.maxabid.co.uk/250-bids-91.html', '20a5fb49674db4b4986b4829ff6ff123', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=91', '0', '0.5', '4', '2013-06-09', '2013-10-05 11:06:44');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('317', '44880a0609195015c252ed96f4172cca', 'http://www.maxabid.co.uk/750-bids-92.html', 'f2893fe0a39928b87629ee16a96ac4e4', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=92', '0', '0.5', '4', '2013-06-09', '2013-10-05 11:06:44');
insert into seo_url (auto_id, seo_url_key, seo_url_get, osc_url_key, seo_url_org, seo_url_hits, seo_url_priority, seo_frequency_id, date_added, last_modified) values ('318', 'd687c4cc2ab9b583c33cca8eb4630f69', 'http://www.maxabid.co.uk/750-bids-93.html', '72c4d6e8db82b11e64cb912e462ed539', 'http://www.maxabid.co.uk/auction_info.php?auctions_id=93', '0', '0.5', '4', '2013-06-09', '2013-10-05 11:06:44');
drop table if exists sessions;
create table sessions (
  sesskey varchar(32) not null ,
  expiry int(11) unsigned not null ,
  value text not null ,
  PRIMARY KEY (sesskey)
);

drop table if exists sessions_admin;
create table sessions_admin (
  sesskey varchar(32) not null ,
  expiry int(11) unsigned not null ,
  value text not null ,
  PRIMARY KEY (sesskey)
);

insert into sessions_admin (sesskey, expiry, value) values ('05da1cca4ddc0b6b4e66a09f5013308d', '1353979237', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('068236f35e33aeed74ab85eaf7766893', '1320389293', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('06d47d5c37f510534717d1a3907ea12a', '1347240882', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('06o4uf0p93tk70k3tetr76pv05', '1335358504', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('0aa36588fece584911c459c22a8998db', '1348587316', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('0e7e33f5ee17e961d42e6b11a992c702', '1348972510', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('112eb4ef67aa67faf0850876fefea00b', '1353456308', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:15:\"abstract_config\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1136d4024945c517f0aaa821659d72b3', '1348801196', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('117370823d39febf859828a4e862aa8f', '1348697370', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1219f4462b3f105068344ce49bade75a', '1312253421', 'language|s:7:\"english\";languages_id|s:1:\"1\";messageToStack|a:0:{}selected_box|s:5:\"tools\";login_id|s:1:\"1\";login_groups_id|s:1:\"1\";login_first_name|s:0:\"\";');
insert into sessions_admin (sesskey, expiry, value) values ('1406sk9rralhqli19r0a8l4vv1', '1334963669', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('162c83bdc0289814bcba1e37b730fa19', '1370192972', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('187aa27f2e4ff372832877267916b198', '1364760915', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1aaf0711c10a62df4d9d0ee835a6cf79', '1348410175', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1aca8b59f8245c361e18b45d425877a9', '1313440426', 'language|s:7:\"english\";languages_id|s:1:\"1\";messageToStack|a:0:{}selected_box|s:5:\"tools\";login_id|s:1:\"1\";login_groups_id|s:1:\"1\";login_first_name|s:0:\"\";');
insert into sessions_admin (sesskey, expiry, value) values ('1cgodd4limaocuvcdd58kcevj4', '1335355171', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1d60647a9b632465c6a8ebfd450a4063', '1347240782', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1e418c829d4ac3286b78975013829a3c', '1355724215', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('1e6e0d45f04214cf5502e7ad2cd58d8d', '1322453196', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('220ac726ee4de4771971dc189284561c', '1365521687', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('23a9aabd2d3308f99317ef92b0ec313b', '1355724403', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('24d8c631be71251c26b928836b2a4988', '1319569620', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('2bafc87d49b8ddbe43d2fd97c1850df0', '1347240840', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('2l5lgc0t47jnkgaju5k0ppgdm5', '1330474882', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('330v50s60cevusqr4e4i35d7e1', '1336328056', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('36ed70856c8c6399b690257129aee279', '1365606060', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('3cedad7a48985a88c96fd8be0ea9eef6', '1321028060', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('3d8cb522eee8a766ca132dd476e456bb', '1369678073', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('3k6256jluiudeurcu9gks0smk0', '1335362724', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('3llv11e9qromnuags5l09h7bq6', '1334522059', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('421eca21d796c314e6ba26f66b59b85d', '1366765782', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('438d0ba8e9de1a27551f9eea43f72f74', '1355725733', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4403bec9ce15741e24956e4e9682b7d4', '1355725584', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('44eb17d55bdfb99ad9fafcab9c0d3663', '1366302081', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('45m8pgg3u4gvi0epvntd3k2gs2', '1336482719', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4bcc48011ff658d6eab915fcd8cc8543', '1365031614', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4cd45ea2427032663e717e49eddae3d5', '1355724337', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4eb8bf3af495bae356357fd7f4421437', '1317485393', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4ee08cba857d5d4dd21a97b94fe4c214', '1317344226', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('4fb55277d3d440cda9b841d0edeb6694', '1348801582', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('52444a30dd9daa3319952e21f9634bf3', '1365436666', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"catalog\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('5341ee5fb2ba21ee6f0643cb7b99c37b', '1348715862', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('53a014e6a05ec916762858fe0cf51736', '1348586304', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('5646d2a8015a228929f5066b6c2310cf', '1347240884', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('57e4910ef5c9bcccf6113ef90613891a', '1313370423', 'language|s:7:\"english\";languages_id|s:1:\"1\";messageToStack|a:0:{}selected_box|s:8:\"helpdesk\";login_id|s:1:\"1\";login_groups_id|s:1:\"1\";login_first_name|s:0:\"\";');
insert into sessions_admin (sesskey, expiry, value) values ('59d21303673d69e869a1051e0dc77d07', '1367414014', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('5ae5f57bc6d6d68a9eedce534b311bc8', '1347240861', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('5c79b5a6fdcfbdc99ad3b0d02e79b20c', '1348801570', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('662d81d4a01c9694387c40d9ba7f5f45', '1365890407', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('67396930fd1c5de0acec4b0df14097c1', '1367414183', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('6864fd6680b3d57d761f5ade43522e13', '1355725743', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('69ce32e319572971293411f93449f487', '1316476914', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:11:\"meta_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('6d587466e44a22d9743b49992e123c58', '1316381242', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('7513d2c291b3b12eb517d8dad6e427ef', '1352694096', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('7at1snnjnck605ni0cl1oms843', '1330351843', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('7bf378138fb09c077ca24b41e7e1d413', '1367872806', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('7juqjt347956tdl5cpphhqj0f3', '1335565607', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('7t1r9vd3477a6s4v2l5h5h50l4', '1336586267', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('81hunjearq0nte1oj54svgrqd2', '1335580229', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:15:\"abstract_config\";}');
insert into sessions_admin (sesskey, expiry, value) values ('857b619bd380e34ca0c7592279935708', '1366759099', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('85p71c3rati1ams1itu13kk8g1', '1334847540', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('8a01b5fa460a24155a86312caa2a3555', '1321044422', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('9114a37287d68e1b6e4e77b7ea8bbc0b', '1355725654', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('95o77tupjrak68faoeotjbcj13', '1332111037', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('960259f0c31eaf13e9952e417f40ad23', '1366595747', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('9722a4e9ff7315dcbd19e73130ce7929', '1366590818', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('99cotnv6nog0cptv7d0l0o87l1', '1334955259', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('9bba14af609878c2dbf86fd9d42ca6da', '1348591135', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('9c6dc9232d4de1c96ddce157f1050583', '1348696531', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('a02hu0fn1hu2sraodgt0gia5e5', '1326402402', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('a2e5c52e144fc88aab7999cac973d4c9', '1365812752', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:15:\"abstract_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('a62bd666f0cf3ead35a620690beacb4c', '1381346639', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"modules\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('a7f98f5b156e9fbd23d74bd5e22e2aa7', '1316139985', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('a848d181a96e10638e074fc8abaeae0f', '1347240836', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ad064b372671682625838c143a7bb3e2', '1365079305', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('b53b7e593bdcd96ff6c6b2f4bb97796d', '1367874189', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"catalog\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('b71b8ba9c42d3c9500a42a44d7dee7f0', '1321466730', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('b9812a05a87b7d238f44c45007dda03a', '1366734917', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";}');
insert into sessions_admin (sesskey, expiry, value) values ('bb07a9d3dde78052f9b4bdafb8c92e4f', '1381108778', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"modules\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('bc537d18c3e0eddca26543058c5959b8', '1370204930', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('bopr4uvnrk3str7ku6p3uqk535', '1334701795', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('btea63p0ji3umtod252ftptdb0', '1333303105', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cb01effc855aa634e298dbaa1b980d6c', '1367669618', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cbb28ab8a01028158928255d6d1e69e4', '1367169470', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ccfe9bdb4ab0ee03891e46521c9afa2b', '1365111119', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:11:\"meta_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cd6c90acfd0ae0d5635df156162862b1', '1383509680', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cefec587979cf151b6fbc86431b2213b', '1364828287', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cja8rgm91ultrpiqbiicb4smp5', '1336166586', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:15:\"abstract_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('cknabt4unthj6m454ndm5fkkd2', '1330614183', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:10:\"seo_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d24ldupqlntc1benqc5sdqmo86', '1334525526', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d4p7admknekeno02r69dtfmgg6', '1330353859', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d51b407f23173d793a7c9e08203d5199', '1367115372', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d551af3c55b32dafb5560903fb5f34a3', '1348587447', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d75be954f18a57435018f5516ab79379', '1367464756', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"catalog\";}');
insert into sessions_admin (sesskey, expiry, value) values ('d8bbla053nat73cffcmea0gkk0', '1333541502', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"helpdesk\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('dc8626f5be4e5ff97e00ab9b6dd39243', '1367076417', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('dc9233d62c4e0428feeba6c3eeb00b9b', '1348802708', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('df31e7eb483104e27f02cc1f84cfd8d2', '1370808678', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('dfb9856c7596b5954e3cafbe259fb90a', '1368569481', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('e199f045e5e63aa3c645c1d76d753825', '1370831532', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('e2b2f7035606256b090ab20313168f5d', '1365802685', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:15:\"abstract_config\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('e75f3733f6a4a407240e9ad88ad26379', '1348587223', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('e99bba7938737d23e2937a53be8906bd', '1315956730', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ec5de58c118eface7bcdef99706c1dde', '1321469232', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ece2670270a3f78fb35fc301aaa0d26f', '1347241252', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ecljo0pnscd79f976n725p3c94', '1336133006', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ehqerr30in2e1tfa0jv5v2mfd1', '1330351844', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f18077e9575db65e3a51fcd92e4562fa', '1312202464', 'language|s:7:\"english\";languages_id|s:1:\"1\";messageToStack|a:0:{}selected_box|s:10:\"seo_config\";login_id|s:1:\"1\";login_groups_id|s:1:\"1\";login_first_name|s:0:\"\";last_id|i:0;');
insert into sessions_admin (sesskey, expiry, value) values ('f251f497f29c6dc5c22005e7e8a95e9b', '1355724626', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"3\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f40cb513203d13ac17ea7bbf8fa868e3', '1383518374', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:1:{i:0;a:2:{s:4:\"text\";s:22:\"SUCCESS_DATABASE_SAVED\";s:4:\"type\";s:7:\"success\";}}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f4bde43e649bb7fa5c53e569fd4a8bd2', '1365689877', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f71d38dc233544097e024bb08b0cd721', '1347240893', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f79326e120e4ad7e49750c66edcc8f3f', '1364845908', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";}');
insert into sessions_admin (sesskey, expiry, value) values ('f7b47cb83b0a717a39b99bc09fee502e', '1322191985', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('fc890d297f18f2731386820468dfd857', '1312145314', 'language|s:7:\"english\";languages_id|s:1:\"1\";messageToStack|a:0:{}selected_box|s:7:\"modules\";login_first_name|s:0:\"\";login_id|s:1:\"1\";login_groups_id|s:1:\"1\";');
insert into sessions_admin (sesskey, expiry, value) values ('fdc98d451994838912730b387d1cf457', '1320729585', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('gn9d151s2kj3s3g0dp5quk2mf6', '1335565397', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('hlnt51ba20hsnh0405rq875c44', '1335809211', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";}');
insert into sessions_admin (sesskey, expiry, value) values ('i3e07d05k8e5c7ksfnh67a82a1', '1330351865', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('k1qsp9iu6ha3kbbdhrpggl2n07', '1332603542', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('kcemsispl9vgjpqt2pm1ug7hr3', '1334800189', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('kde561fek4o59sbr94msq8q261', '1335977883', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('kk1nvd80fdcsmhqhvmjbo08t97', '1326938283', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('l9hhtlckbmqt4pe986kqf8d2n4', '1334847749', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('mbi98m81fbd83268akccjhmga5', '1335821694', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"modules\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('mf1sjtfqm0jcqck3df1bgakgs7', '1327029055', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('mftojq1rsq6htk7oua34bhu355', '1327250818', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('mmvofas8kvqq2etb1jginfbqv3', '1335395198', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('n9i6lp5umj6rgghvdm7m9ecp97', '1335565608', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ne6hhfa3bqials5ajiqhmh0ud2', '1334849198', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('o15u18a1m10sp2n934utf23rp2', '1330613902', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('o1jhck276ttuj5a6d2d6a2a365', '1335886441', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('og8eb830ckj9siflnf5v9ejaa2', '1326854919', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:8:\"auctions\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('oro473flt25m60nf903rj00r37', '1335573065', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('p8f121ht0f50g5f2lor10bm0a1', '1335317309', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:7:\"modules\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('qda18chpo83cs6hmino9q3np47', '1335565607', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('qk5ghv4th1km6qnn2334nfm8i4', '1334443547', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:5:\"tools\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('qv57sucobs2gpkl4n4iciu5mn7', '1335146003', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('sqjmr2d21brqkg6jt84fbntmr6', '1336647440', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('srpd1c2e0hhkhmkdvune5u0jm6', '1330351864', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('to71ms9ib0iibnibk9hhpmnma3', '1335565394', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
insert into sessions_admin (sesskey, expiry, value) values ('unifs4pcu0k8no0h6fin661ol0', '1326661756', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"administrator\";}');
insert into sessions_admin (sesskey, expiry, value) values ('ur7ha9b6m2qgd5d4gu88vigqa5', '1335980070', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('v1ei8j5vjic16co999ri7h2c22', '1336648021', 'a:7:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:9:\"customers\";s:8:\"login_id\";s:1:\"1\";s:15:\"login_groups_id\";s:1:\"1\";s:16:\"login_first_name\";s:0:\"\";}');
insert into sessions_admin (sesskey, expiry, value) values ('v3milp1kuqsupb1hrtf022u8s5', '1330613902', 'a:4:{s:8:\"language\";s:7:\"english\";s:12:\"languages_id\";s:1:\"1\";s:14:\"messageToStack\";a:0:{}s:12:\"selected_box\";s:13:\"configuration\";}');
drop table if exists specials;
create table specials (
  specials_id int(11) not null auto_increment,
  products_id int(11) not null ,
  specials_new_products_price decimal(15,4) not null ,
  specials_date_added datetime ,
  specials_last_modified datetime ,
  expires_date datetime ,
  date_status_change datetime ,
  status int(1) default '1' not null ,
  PRIMARY KEY (specials_id),
  KEY idx_products_id (products_id)
);

insert into specials (specials_id, products_id, specials_new_products_price, specials_date_added, specials_last_modified, expires_date, date_status_change, status) values ('1', '99', '249.9900', '2013-04-08 12:57:17', NULL, '2013-04-30 00:00:00', '2013-04-30 10:09:53', '0');
insert into specials (specials_id, products_id, specials_new_products_price, specials_date_added, specials_last_modified, expires_date, date_status_change, status) values ('2', '97', '500.0000', '2013-04-08 12:57:45', NULL, '2013-04-29 00:00:00', '2013-04-29 06:12:56', '0');
drop table if exists tax_class;
create table tax_class (
  tax_class_id int(11) not null auto_increment,
  tax_class_title varchar(32) not null ,
  tax_class_description varchar(255) not null ,
  last_modified datetime ,
  date_added datetime not null ,
  PRIMARY KEY (tax_class_id)
);

insert into tax_class (tax_class_id, tax_class_title, tax_class_description, last_modified, date_added) values ('2', 'UK VAT 20%', 'UK 20% VAT', '2013-05-02 19:36:22', '2011-09-19 20:47:06');
drop table if exists tax_rates;
create table tax_rates (
  tax_rates_id int(11) not null auto_increment,
  tax_zone_id int(11) not null ,
  tax_class_id int(11) not null ,
  tax_priority int(5) default '1' ,
  tax_rate decimal(7,4) not null ,
  tax_description varchar(255) not null ,
  last_modified datetime ,
  date_added datetime not null ,
  PRIMARY KEY (tax_rates_id)
);

insert into tax_rates (tax_rates_id, tax_zone_id, tax_class_id, tax_priority, tax_rate, tax_description, last_modified, date_added) values ('2', '2', '2', '1', '20.0000', 'UK VAT 20%', '2013-05-02 19:36:39', '2011-09-19 20:40:34');
drop table if exists testimonials;
create table testimonials (
  testimonials_title varchar(64) not null ,
  testimonials_id int(5) not null auto_increment,
  testimonials_html_text longtext not null ,
  testimonials_name varchar(50) not null ,
  testimonials_url varchar(150) not null ,
  testimonials_url_title varchar(150) not null ,
  date_added varchar(50) not null ,
  status tinyint(1) default '1' not null ,
  PRIMARY KEY (testimonials_id)
);

insert into testimonials (testimonials_title, testimonials_id, testimonials_html_text, testimonials_name, testimonials_url, testimonials_url_title, date_added, status) values ('My test testimonal', '1', 'Test Testimonial', 'Joe Doe', '', '', '2007-11-16 14:11:22', '1');
drop table if exists whos_online;
create table whos_online (
  customer_id int(11) default '0' not null ,
  full_name varchar(255) not null ,
  session_id varchar(32) not null ,
  ip_address varchar(15) not null ,
  time_entry varchar(14) not null ,
  time_last_click varchar(14) not null ,
  last_page_url varchar(255) not null ,
  cookie_sent tinyint(1) default '0' not null 
);

drop table if exists zones;
create table zones (
  zone_id int(11) not null auto_increment,
  zone_country_id int(11) not null ,
  zone_code varchar(32) not null ,
  zone_name varchar(32) not null ,
  status_id tinyint(1) default '1' not null ,
  pay_status_id tinyint(1) default '1' not null ,
  ship_status_id tinyint(1) default '1' not null ,
  PRIMARY KEY (zone_id)
);

insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('1', '223', 'AL', 'Alabama', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('2', '223', 'AK', 'Alaska', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('3', '223', 'AS', 'American Samoa', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('4', '223', 'AZ', 'Arizona', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('5', '223', 'AR', 'Arkansas', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('6', '223', 'AF', 'Armed Forces Africa', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('7', '223', 'AA', 'Armed Forces Americas', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('8', '223', 'AC', 'Armed Forces Canada', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('9', '223', 'AE', 'Armed Forces Europe', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('10', '223', 'AM', 'Armed Forces Middle East', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('11', '223', 'AP', 'Armed Forces Pacific', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('12', '223', 'CA', 'California', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('13', '223', 'CO', 'Colorado', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('14', '223', 'CT', 'Connecticut', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('15', '223', 'DE', 'Delaware', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('16', '223', 'DC', 'District of Columbia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('17', '223', 'FM', 'Federated States Of Micronesia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('18', '223', 'FL', 'Florida', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('19', '223', 'GA', 'Georgia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('20', '223', 'GU', 'Guam', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('21', '223', 'HI', 'Hawaii', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('22', '223', 'ID', 'Idaho', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('23', '223', 'IL', 'Illinois', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('24', '223', 'IN', 'Indiana', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('25', '223', 'IA', 'Iowa', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('26', '223', 'KS', 'Kansas', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('27', '223', 'KY', 'Kentucky', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('28', '223', 'LA', 'Louisiana', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('29', '223', 'ME', 'Maine', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('30', '223', 'MH', 'Marshall Islands', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('31', '223', 'MD', 'Maryland', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('32', '223', 'MA', 'Massachusetts', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('33', '223', 'MI', 'Michigan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('34', '223', 'MN', 'Minnesota', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('35', '223', 'MS', 'Mississippi', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('36', '223', 'MO', 'Missouri', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('37', '223', 'MT', 'Montana', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('38', '223', 'NE', 'Nebraska', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('39', '223', 'NV', 'Nevada', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('40', '223', 'NH', 'New Hampshire', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('41', '223', 'NJ', 'New Jersey', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('42', '223', 'NM', 'New Mexico', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('43', '223', 'NY', 'New York', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('44', '223', 'NC', 'North Carolina', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('45', '223', 'ND', 'North Dakota', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('46', '223', 'MP', 'Northern Mariana Islands', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('47', '223', 'OH', 'Ohio', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('48', '223', 'OK', 'Oklahoma', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('49', '223', 'OR', 'Oregon', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('50', '223', 'PW', 'Palau', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('51', '223', 'PA', 'Pennsylvania', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('52', '223', 'PR', 'Puerto Rico', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('53', '223', 'RI', 'Rhode Island', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('54', '223', 'SC', 'South Carolina', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('55', '223', 'SD', 'South Dakota', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('56', '223', 'TN', 'Tennessee', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('57', '223', 'TX', 'Texas', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('58', '223', 'UT', 'Utah', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('59', '223', 'VT', 'Vermont', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('60', '223', 'VI', 'Virgin Islands', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('61', '223', 'VA', 'Virginia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('62', '223', 'WA', 'Washington', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('63', '223', 'WV', 'West Virginia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('64', '223', 'WI', 'Wisconsin', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('65', '223', 'WY', 'Wyoming', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('66', '38', 'AB', 'Alberta', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('67', '38', 'BC', 'British Columbia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('68', '38', 'MB', 'Manitoba', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('69', '38', 'NB', 'New Brunswick', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('70', '38', 'NL', 'Newfoundland and Labrador', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('71', '38', 'NT', 'Northwest Territories', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('72', '38', 'NS', 'Nova Scotia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('73', '38', 'NU', 'Nunavut', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('74', '38', 'ON', 'Ontario', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('75', '38', 'PE', 'Prince Edward Island', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('76', '38', 'QC', 'Quebec', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('77', '38', 'SK', 'Saskatchewan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('78', '38', 'YT', 'Yukon Territory', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('79', '81', 'BAW', 'Baden-Wuumlrttemberg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('80', '81', 'BAY', 'Bayern', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('81', '81', 'BER', 'Berlin', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('82', '81', 'BRG', 'Brandenburg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('83', '81', 'BRE', 'Bremen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('84', '81', 'HAM', 'Hamburg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('85', '81', 'HES', 'Hessen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('86', '81', 'MEC', 'Mecklenburg-Vorpommern', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('87', '81', 'NDS', 'Niedersachsen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('88', '81', 'NRW', 'Nordrhein-Westfalen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('89', '81', 'RHE', 'Rheinland-Pfalz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('90', '81', 'SAR', 'Saarland', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('91', '81', 'SAS', 'Sachsen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('92', '81', 'SAC', 'Sachsen-Anhalt', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('93', '81', 'SCN', 'Schleswig-Holstein', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('94', '81', 'THE', 'Thuumlringen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('95', '14', 'BUR', 'Burgenland', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('96', '14', 'KAR', 'Kaumlrnten', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('97', '14', 'NOS', 'Niederoumlesterreich', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('98', '14', 'OOS', 'Oberoumlesterreich', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('99', '14', 'SAL', 'Salzburg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('100', '14', 'STE', 'Steiermark', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('101', '14', 'TIR', 'Tirol', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('102', '14', 'VOR', 'Vorarlberg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('103', '14', 'WIE', 'Wien', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('104', '204', 'AG', 'Aargau', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('105', '204', 'AR', 'Appenzell Ausserrhoden', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('106', '204', 'AI', 'Appenzell Innerrhoden', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('107', '204', 'BS', 'Basel-Stadt', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('108', '204', 'BL', 'Basel-Landschaft', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('109', '204', 'BE', 'Bern', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('110', '204', 'FR', 'Fribourg', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('111', '204', 'GE', 'Genegraveve', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('112', '204', 'GL', 'Glarus', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('113', '204', 'GR', 'Graub?nden', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('114', '204', 'JU', 'Jura', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('115', '204', 'LU', 'Luzern', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('116', '204', 'NE', 'Neuch?tel', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('117', '204', 'NW', 'Nidwald', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('118', '204', 'OW', 'Obwald', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('119', '204', 'SG', 'St. Gallen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('120', '204', 'SH', 'Schaffhausen', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('121', '204', 'SZ', 'Schwyz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('122', '204', 'SO', 'Solothurn', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('123', '204', 'TG', 'Thurgau', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('124', '204', 'TI', 'Ticino', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('125', '204', 'UR', 'Uri', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('126', '204', 'VS', 'Valais', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('127', '204', 'VD', 'Vaud', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('128', '204', 'ZG', 'Zug', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('129', '204', 'ZH', 'Zuumlrich', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('130', '195', 'CA', 'A Coruntildea', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('131', '195', 'AL', 'Aacutelava', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('132', '195', 'AB', 'Albacete', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('133', '195', 'AC', 'Alicante', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('134', '195', 'AM', 'Almeria', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('135', '195', 'AS', 'Asturias', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('136', '195', 'AV', 'Aacutevila', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('137', '195', 'BJ', 'Badajoz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('138', '195', 'IB', 'Baleares', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('139', '195', 'BA', 'Barcelona', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('140', '195', 'BU', 'Burgos', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('141', '195', 'CC', 'Caacuteceres', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('142', '195', 'CZ', 'Caacutediz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('143', '195', 'CT', 'Cantabria', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('144', '195', 'CL', 'Castelloacuten', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('145', '195', 'CE', 'Ceuta', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('146', '195', 'CR', 'Ciudad Real', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('147', '195', 'CD', 'Coacuterdoba', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('148', '195', 'CU', 'Cuenca', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('149', '195', 'GI', 'Girona', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('150', '195', 'GD', 'Granada', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('151', '195', 'GJ', 'Guadalajara', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('152', '195', 'GP', 'Guipuacutezcoa', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('153', '195', 'HL', 'Huelva', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('154', '195', 'HS', 'Huesca', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('155', '195', 'JN', 'Jaeacuten', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('156', '195', 'RJ', 'La Rioja', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('157', '195', 'PM', 'Las Palmas', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('158', '195', 'LE', 'Leon', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('159', '195', 'LL', 'Lleida', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('160', '195', 'LG', 'Lugo', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('161', '195', 'MD', 'Madrid', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('162', '195', 'MA', 'Malaga', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('163', '195', 'ML', 'Melilla', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('164', '195', 'MU', 'Murcia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('165', '195', 'NV', 'Navarra', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('166', '195', 'OU', 'Ourense', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('167', '195', 'PL', 'Palencia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('168', '195', 'PO', 'Pontevedra', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('169', '195', 'SL', 'Salamanca', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('170', '195', 'SC', 'Santa Cruz de Tenerife', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('171', '195', 'SG', 'Segovia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('172', '195', 'SV', 'Sevilla', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('173', '195', 'SO', 'Soria', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('174', '195', 'TA', 'Tarragona', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('175', '195', 'TE', 'Teruel', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('176', '195', 'TO', 'Toledo', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('177', '195', 'VC', 'Valencia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('178', '195', 'VD', 'Valladolid', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('179', '195', 'VZ', 'Vizcaya', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('180', '195', 'ZM', 'Zamora', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('181', '195', 'ZR', 'Zaragoza', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('182', '1', 'BDS', 'Badakhshan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('183', '1', 'BDG', 'Badghis', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('184', '1', 'BGL', 'Baghlan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('185', '1', 'BAL', 'Balkh', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('186', '1', 'BAM', 'Bamian', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('187', '1', 'FRA', 'Farah', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('188', '1', 'FYB', 'Faryab', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('189', '1', 'GHA', 'Ghazni', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('190', '1', 'GHO', 'Ghowr', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('191', '1', 'HEL', 'Helmand', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('192', '1', 'HER', 'Herat', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('193', '1', 'JOW', 'Jowzjan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('194', '1', 'KAB', 'Kabul', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('195', '1', 'KAN', 'Kandahar', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('196', '1', 'KAP', 'Kapisa', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('197', '1', 'KHO', 'Khost', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('198', '1', 'KNR', 'Konar', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('199', '1', 'KDZ', 'Kondoz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('200', '1', 'LAG', 'Laghman', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('201', '1', 'LOW', 'Lowgar', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('202', '1', 'NAN', 'Nangrahar', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('203', '1', 'NIM', 'Nimruz', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('204', '1', 'NUR', 'Nurestan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('205', '1', 'ORU', 'Oruzgan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('206', '1', 'PIA', 'Paktia', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('207', '1', 'PKA', 'Paktika', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('208', '1', 'PAR', 'Parwan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('209', '1', 'SAM', 'Samangan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('210', '1', 'SAR', 'Sar-e Pol', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('211', '1', 'TAK', 'Takhar', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('212', '1', 'WAR', 'Wardak', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('213', '1', 'ZAB', 'Zabol', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('214', '2', 'BR', 'Berat', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('215', '2', 'BU', 'Bulqize', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('216', '2', 'DL', 'Delvine', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('217', '2', 'DV', 'Devoll', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('218', '2', 'DI', 'Diber', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('219', '2', 'DR', 'Durres', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('220', '2', 'EL', 'Elbasan', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('221', '2', 'ER', 'Kolonje', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('222', '2', 'FR', 'Fier', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('223', '2', 'GJ', 'Gjirokaster', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('224', '2', 'GR', 'Gramsh', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('225', '2', 'HA', 'Has', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('226', '2', 'KA', 'Kavaje', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('227', '2', 'KB', 'Kurbin', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('228', '2', 'KC', 'Kucove', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('229', '2', 'KO', 'Korce', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('230', '2', 'KR', 'Kruje', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('231', '2', 'KU', 'Kukes', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('232', '2', 'LB', 'Librazhd', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('233', '2', 'LE', 'Lezhe', '1', '1', '1');
insert into zones (zone_id, zone_country_id, zone_code, zone_name, status_id, pay_status_id, ship_status_id) values ('234', '2', 'LU', 'Lushnje', '1', '1', '1');
