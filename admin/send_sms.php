<?php
/*
  $Id: mail.php,v 1.31 2003/06/20 00:37:51 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Mail page
// I-Metrics Engine - Version: Renegade I
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if( isset($_POST['back_x']) || isset($_POST['back_y']) ) {
    $action = 'back';
  }

  switch($action) {
    case 'send_email_to_user':
      if( !isset($_POST['customers_email_address']) ) break;
      $query_flag = true;
      switch ($_POST['customers_email_address']) {
        case '***':
          $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS);
          $mail_sent_to = TEXT_ALL_CUSTOMERS;
          break;
        case '**D':
          $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_newsletter = '1'");
          $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
          break;
        default:
          $where_string = 'where customers_email_address';
          if( isset($_POST['customers_list']) && is_array($_POST['customers_list']) && count($_POST['customers_list']) ) {
            $query_flag = false;
            $mail_array = array();
            $mail_sent_to = '';
            foreach( $_POST['customers_list'] as $key => $value ) {
              $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input(tep_db_prepare_input($value)) . "'");
              if( $mail_info = tep_db_fetch_array($mail_query) ) {
                $mail_array[] = $mail_info;
              } else {
                $mail_array[] = array(
                                      'customers_firstname' => '', 
                                      'customers_lastname' => '',
                                      'customers_email_address' => tep_db_prepare_input($value),
                                     );
              }
              $mail_sent_to .= tep_db_prepare_input($value) . '<br />';
            }
            $mail_sent_to = substr($mail_sent_to, 0, -6);
          } else {
            $customers_email_address = tep_db_prepare_input($_POST['customers_email_address']);
            $where_string .= "= '" . tep_db_input($customers_email_address) . "'";
            $mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($customers_email_address) . "'");
            $mail_sent_to = tep_db_prepare_input($_POST['customers_email_address']);
          }
          //$mail_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($customers_email_address) . "'");
          //$mail_sent_to = $_POST['customers_email_address'];
          break;
      }

      if( isset($_POST['attach_file']) && is_array($_POST['attach_file']) ) {
        $attach_array = array();
        foreach($_POST['attach_file'] as $key => $file ) {
          $name = $file;
          $file = DIR_FS_BACKUP . $file;
          $fp = fopen($file, "r");
          if( $fp ) {
            $attachment = fread($fp, filesize($file));
            $attach_array[] = array(
                                  'attachment' => $attachment,
                                  'name' => $name,
                                  'type' => 'application/octet-stream',
                                 );
            fclose($fp);
            @unlink($file);
          }
        }
      }

      $from = tep_db_prepare_input($_POST['from']);
      $subject = tep_db_prepare_input($_POST['subject']);
      $message = tep_db_prepare_input($_POST['message']);

      //Let's build a message object using the email class
      $mimemessage = new email(array('X-Mailer: Asymmetrics Mailer'));
      // add the message to the object
      //$mimemessage->add_text($message);

      if (EMAIL_USE_HTML == 'true') {
        $text = strip_tags($message);
        $mimemessage->add_html($message, $text);
      } else {
        $mimemessage->add_text($message);
      }
      if( isset($attach_array) && is_array($attach_array) ) {
        foreach($attach_array as $value) {
          $mimemessage->add_attachment($value['attachment'], $value['name'], $value['type']);
        }
      }

      $mimemessage->build_message();

      if( $query_flag ) {
        while ($mail = tep_db_fetch_array($mail_query)) {
          $mimemessage->send($mail['customers_firstname'] . ' ' . $mail['customers_lastname'], $mail['customers_email_address'], '', $from, $subject);
        }
      } else {
        foreach($mail_array as $mail) {
          if( tep_not_null($mail['customers_firstname']) && tep_not_null($mail['customers_lastname']) ) {
            $name = $mail['customers_firstname'] . ' ' . $mail['customers_lastname'];
          } else {
            $name = $mail['customers_email_address'];
          }
          $mimemessage->send($name, $mail['customers_email_address'], '', $from, $subject);
        }
      }
      $messageStack->add_session(sprintf(NOTICE_EMAIL_SENT_TO, $mail_sent_to), 'success');
      tep_redirect(tep_href_link( basename($PHP_SELF)));
      break;
    case 'preview':
     if ( !isset($_POST['customers_email_address']) ) {
        $messageStack->add(ERROR_NO_CUSTOMER_SELECTED, 'error');
        break;
      }

     if ( !isset($_POST['subject']) || !tep_not_null($_POST['subject']) ) {
        $messageStack->add(ERROR_NO_SUBJECT, 'error');
        break;
      }

      if( isset($_FILES['attach_file']) && is_array($_FILES['attach_file']) && isset($_FILES['attach_file']['name']) && is_array($_FILES['attach_file']['name']) ) {
        $attach_array = array();
        foreach( $_FILES['attach_file']['name'] as $key => $value ) {
          if( !tep_not_null($value) ) continue;
          $value = basename($value);
          $check = $_FILES["attach_file"]["error"][$key];
          if ($check != UPLOAD_ERR_OK) {
            $messageStack->add(sprintf(ERROR_FILE_UPLOAD, $value), 'error');
            break;
          }
          if(file_exists(DIR_FS_BACKUP . $value) ) {
            unlink (DIR_FS_BACKUP . $value);
          }
          $tmp_name = $_FILES["attach_file"]["tmp_name"][$key];
          move_uploaded_file($tmp_name, DIR_FS_BACKUP . $value);
          $attach_array[] = $value;
        }
      }
      break;
    case 'back':
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<script language="javascript" src="includes/general.js" type="text/javascript"></script>
<?php
  if (!tep_not_null($action) ) {
    $mce_str = 'message'; // Comma separated list of textarea names
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    require(DIR_WS_INCLUDES . 'javascript/tiny_mce/general2.php');
  }
?>
<script language="javascript" type="text/javascript"><!--
function setCustomers(source, target) {

  text = source;
  value = source;
  target = document.getElementById(target);

  var entry = document.createElement('option');
  entry.text = text;
  entry.value = value;
  var entry_old;
  entry_old = null;

  for( i=0, j=target.options.length; i<j; i++) {
    if(target.options[i].value == source) {
      alert(text+' already in the list');
      return;
    }
  }

  try {
    target.add(entry, entry_old); // non-IE support
  }
  catch(ex) {
    select = target.options.length;
    target.add(entry, select); // IE support
  }
  // Select entire list
  for( i=0, j=target.options.length; i<j; i++) {
    target.options[i].selected = true;
  }

}

function setList(source, target) {
  found = false;
  if( source.type == 'text' && source.value.length > 6 ) {
    text = source.value;
    value = source.value;
    found = 2;
  } else if(source.type == 'select-one') {
    for( i=0, j=source.options.length; i<j; i++) {
      if( source.options[i].selected ) {
        text = source.options[i].text;
        value = source.options[i].value;
        found = true;
        break;
      }
    }
  }
  if( !found ) {
    return;
  }

  if( text == 'Select Customer' || text == 'All Customers' || text == 'To All Newsletter Subscribers') {
    return;
  }
  for( i=0, j=target.options.length; i<j; i++) {
    if(target.options[i].text == text && target.options[i].value == value) {
      found = false;
      break;
    }
  }

  if( !found ) {
    return;
  }

  var entry = document.createElement('option');
  entry.text = text;
  entry.value = value;
  var entry_old;
  if( found == 2 ) {
    entry_old = null;
  } else {
    entry_old = target.options[source.selectedIndex];
  }

  try {
    target.add(entry, entry_old); // non-IE support
  }
  catch(ex) {
    select = target.options.length;
    target.add(entry, select); // IE support
  }
  // Select entire list
  for( i=0, j=target.options.length; i<j; i++) {
    target.options[i].selected = true;
  }

}

function removeList(object) {
  var i;
  for (i = object.length - 1; i>=0; i = i-1) {
    if (object.options[i].selected) {
      object.remove(i);
    }
  }
}

g_rows_index = 1;
function addFileRows(name) {
  html = '<input type="file" size="44" name="attach_file['+g_rows_index+']" /><br />';
  newElem = document.getElementById("extrarows");
  newElem.innerHTML += html;

  g_rows_index++;
/*
  var entry = document.createElement('input');
  entry.type = 'file';
  entry.size = '44';
  entry.name = name;
  newelement = '<br />';
  target = document.getElementById(name);
  alert(entry);
  target.add(entry, null); // non-IE support
*/

}
//--></script>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ( ($action == 'preview') && isset($_POST['customers_email_address']) ) {
    switch ($_POST['customers_email_address']) {
      case '***':
        $mail_sent_to = TEXT_ALL_CUSTOMERS;
        break;
      case '**D':
        $mail_sent_to = TEXT_NEWSLETTER_CUSTOMERS;
        break;
      default:
        if( isset($_POST['customers_list']) && is_array($_POST['customers_list']) ) {
          $mail_sent_to = '';
          foreach($_POST['customers_list'] as $key => $value) {
            $mail_sent_to .= $value . '<br />';
          }
        } else {
          $mail_sent_to = $_POST['customers_email_address'];
        }
        break;
    }
?>
          <tr>
            <td><?php echo tep_draw_form('mail', basename($PHP_SELF), 'action=send_email_to_user'); ?><table border="0" width="100%" cellpadding="0" cellspacing="2">
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_CUSTOMER; ?></b>
<?php
    if( isset($_POST['customers_list']) && is_array($_POST['customers_list']) ) {
      foreach($_POST['customers_list'] as $key2 => $value2) {
        $value2 = htmlspecialchars(stripslashes($value2));
        echo '<br />' . $value2 . tep_draw_hidden_field('customers_list[' . $key2 . ']', $value2);
      }
    } else {
      echo '<br />' . htmlspecialchars(stripslashes($mail_sent_to));
    }
?>
                </td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_FROM; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['from'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_SUBJECT; ?></b><br><?php echo htmlspecialchars(stripslashes($_POST['subject'])); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo TEXT_MESSAGE; ?></b><br>
<?php 
    if (EMAIL_USE_HTML == 'true') {
      echo tep_db_prepare_input($_POST['message']);
    } else {
      echo nl2br(htmlspecialchars(stripslashes($_POST['message']))); 
    }
?>
                </td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
<?php
    if( isset($attach_array) && is_array($attach_array) && count($attach_array) ) {
?>
              <tr>
                <td class="smallText"><b><?php echo TEXT_ATTACHMENTS; ?></b>
<?php
      foreach($attach_array as $key2 => $value2) {
        $value2 = htmlspecialchars(stripslashes($value2));
        echo '<br />' . $value2 . tep_draw_hidden_field('attach_file[' . $key2 . ']', $value2);
      }
?>

              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
<?php
    }
?>
              <tr>
                <td><table border="0" width="100%" cellpadding="0" cellspacing="2">
                  <tr>
                    <td>

<?php
/* Re-Post all POST'ed variables */
    reset($_POST);
    while (list($key, $value) = each($_POST)) {
      if( $key == 'message' ) {
        echo tep_draw_hidden_field($key, tep_db_prepare_input($value) );
      } elseif (!is_array($_POST[$key])) {
        echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
      }
    }
/*
    if( isset($_POST['customers_list']) && is_array($_POST['customers_list']) ) {
      foreach($_POST['customers_list'] as $key2 => $value2) {
        echo tep_draw_hidden_field('customers_list[' . $key2 . ']', htmlspecialchars(stripslashes($value2)));
      }
    }
    if( isset($attach_array) && is_array($attach_array) && count($attach_array) ) {
      foreach($attach_array as $key2 => $value2) {
        echo tep_draw_hidden_field('attach_file[' . $key2 . ']', htmlspecialchars(stripslashes($value2)));
      }
    }
*/
    echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="back"'); 
?>
                    </td>
                    <td align="right"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF)) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_send_mail.gif', IMAGE_SEND_EMAIL); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></form></td>
          </tr>
<?php
  } else {
?>
          <tr>
            <td><?php echo tep_draw_form('mail_form', basename($PHP_SELF), 'action=preview', 'post', 'enctype="multipart/form-data"'); ?><table border="0" cellpadding="0" cellspacing="2">
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
<?php
    $customers = $customers_list2 = array();
    $customers_list[] = array('id'=>'', 'text' => '');
    $customers[] = array('id' => '', 'text' => TEXT_SELECT_CUSTOMER);
    //$customers[] = array('id' => '***', 'text' => TEXT_ALL_CUSTOMERS);
    //$customers[] = array('id' => '**D', 'text' => TEXT_NEWSLETTER_CUSTOMERS);
    $mail_query = tep_db_query("select customers_email_address, customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " order by customers_lastname");
    while($customers_values = tep_db_fetch_array($mail_query)) {
      $customers[] = array('id' => $customers_values['customers_email_address'],
                           'text' => $customers_values['customers_lastname'] . ', ' . $customers_values['customers_firstname'] . ' (' . $customers_values['customers_email_address'] . ')');
    }
?>
              <tr>
                <td width="60" class="main"><?php echo TEXT_FROM; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_input_field('from', EMAIL_FROM, 'size="48"'); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_SUBJECT; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_input_field('subject', '', 'size="48"'); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_CUSTOMER; ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_pull_down_menu('customers_email_address', $customers, (isset($_GET['customer']) ? $_GET['customer'] : ''), 'id="customers_email_address"'); ?></td>
                <td><?php echo '<a href="javascript:void(0)" onclick="setList(document.mail_form.customers_email_address, document.mail_form.customers_list);">' . tep_image(DIR_WS_ICONS . 'arrow_down.gif', 'Assign Customer to the receipient list') . '</a>'; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_CUSTOMER_BLANK; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_input_field('customers_blank', (isset($_GET['customers_blank']) ? $_GET['customers_blank'] : ''), 'size="48" id="customers_blank"'); ?></td>
                <td><?php echo '<a href="javascript:void(0)" onclick="setList(document.mail_form.customers_blank, document.mail_form.customers_list);">' . tep_image(DIR_WS_ICONS . 'arrow_down.gif', 'Specify your own email address to the receipient list') . '</a>'; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_CUSTOMER_LIST; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_pull_down_menu('customers_list[]', $customers_list2, '', 'multiple="multiple" size="6" style="width: 306px" id="customers_list"'); ?></td>
                <td><?php echo '<a href="javascript:void(0)" onclick="removeList(document.mail_form.customers_list);">' . tep_image(DIR_WS_ICONS . 'cross.gif', 'Remove selected entries from the receipient list') . '</a>'; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_ATTACH_FILE; ?></td>
              </tr>
              <tr>
                <td class="main"><div id="extrarows" style="position:relative; padding:0; margin:0"><?php echo tep_draw_file_field('attach_file[0]', 'size="44"'); ?></div></td>
                <td><?php echo '<a href="javascript:void(0)" onclick="addFileRows(document.mail_form.extrarows);">' . tep_image(DIR_WS_ICONS . 'arrow_down.gif', 'Add more rows for file attachments') . '</a>'; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td valign="top" class="main"><?php echo TEXT_MESSAGE; ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_textarea_field('message', 'soft', '60', '15'); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_image_submit('button_send_mail.gif', IMAGE_SEND_EMAIL); ?></td>
              </tr>
            </table></form></td>
          </tr>
<?php
  }
?>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
