<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Repository Text Storage script for orders comments
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch ($action) {
    case 'setflag':
      if ($_GET['flag'] == '1') {
        tep_db_query("update " . TABLE_ORDERS_COMMENTS . " set status_id = '1' where orders_comments_id = '" . (int)$_GET['gtID'] . "'");
      } elseif ($_GET['flag'] == '0') {
        tep_db_query("update " . TABLE_ORDERS_COMMENTS . " set status_id = '0' where orders_comments_id = '" . (int)$_GET['gtID'] . "'");
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action','flag')) ));
      break;
    case 'delete_confirm':
      if( isset($_POST['orders_comments_id']) && tep_not_null($_POST['orders_comments_id']) ) {
        $orders_comments_id = $_POST['orders_comments_id'];
        tep_db_query("delete from " . TABLE_ORDERS_COMMENTS . " where orders_comments_id = '" . (int)$orders_comments_id . "'");
      }

      tep_redirect(tep_href_link(basename($PHP_SELF)));
      break;
    case 'insert':
    case 'update':
      if (isset($_POST['edit_x']) || isset($_POST['edit_y'])) {
        $action = 'new_generic_text';
      } else {
        if (isset($_GET['gtID'])) 
          $orders_comments_id = tep_db_prepare_input($_GET['gtID']);

          $sql_data_array = array('orders_comments_title' => tep_db_prepare_input($_POST['orders_comments_title']),
                                  'orders_comments_text' => tep_db_prepare_input($_POST['orders_comments_text']),
                                  'orders_status_id' => (int)$_POST['orders_status_id'],
                                  'status_id' => (int)$_POST['status']
                                 );

        if($action == 'insert') {
          tep_db_perform(TABLE_ORDERS_COMMENTS, $sql_data_array);
          $orders_comments_id = tep_db_insert_id();
        } elseif ($action == 'update') {
          tep_db_perform(TABLE_ORDERS_COMMENTS, $sql_data_array, 'update', "orders_comments_id = '" . (int)$orders_comments_id . "'");
        }
        tep_redirect(tep_href_link(basename($PHP_SELF), 'gtID=' . $orders_comments_id));
      }
      break;
    case 'copy_to_confirm':
      if( isset($_POST['orders_comments_id']) && tep_not_null($_POST['orders_comments_id']) ) {
        $orders_comments_id = tep_db_prepare_input($_POST['orders_comments_id']);
        if ($_POST['copy_as'] == 'duplicate') {
          $generic_text_query = tep_db_query("select orders_comments_title, orders_comments_text from " . TABLE_ORDERS_COMMENTS . " where orders_comments_id = '" . (int)$orders_comments_id . "'");
          $generic_text = tep_db_fetch_array($generic_text_query);

          tep_db_query("insert into " . TABLE_ORDERS_COMMENTS . " (orders_comments_title, orders_comments_text, orders_status_id, status_id) values ('" . tep_db_input($generic_text['orders_comments_title']) . "', '" . tep_db_input($generic_text['orders_comments_text']) . "', '" . (int)$generic_text['orders_status_id'] . "', '0')");
          $orders_comments_id = tep_db_insert_id();
        }
      }

      tep_redirect(tep_href_link(basename($PHP_SELF), 'gtID=' . $orders_comments_id));
      break;

    case 'preview':
      break;
    default:
      break;
  }

  $orders_status_array = array();
  $orders_query = tep_db_query("select orders_status_id as id, orders_status_name as text from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "'");
  while($orders_status_array[] = tep_db_fetch_array($orders_query) );
  array_pop($orders_status_array);
  $display_status_array = array();
  for($i = 0, $j=count($orders_status_array); $i<$j; $i++) {
    $display_status_array[$orders_status_array[$i]['id']] = $orders_status_array[$i]['text'];
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if ($action == 'new_generic_text') {
?>

      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    $parameters = array(
                        'orders_comments_id' => '',
                        'orders_comments_title' => '',
                        'orders_comments_text' => '',
                        'orders_status_id' => '',
                        'status_id' => '',
                       );

    $gtInfo = new objectInfo($parameters);

    if (isset($_GET['gtID']) && empty($_POST)) {
      $generic_text_query = tep_db_query("select orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id from " . TABLE_ORDERS_COMMENTS . " where orders_comments_id = '" . (int)$_GET['gtID'] . "'");
      $generic_text = tep_db_fetch_array($generic_text_query);
      $gtInfo->objectInfo($generic_text);
    } elseif (tep_not_null($_POST)) {
      $gtInfo->objectInfo($_POST);
      $orders_comments_title = $_POST['orders_comments_title'];
      $orders_comments_text = $_POST['orders_comments_text'];
    }

    if (!isset($gtInfo->status_id)) $gtInfo->status_id = '1';
    switch ($gtInfo->status_id) {
      case '0': 
        $in_status = false; 
        $out_status = true; 
        break;
      case '1':
      default: 
        $in_status = true; 
        $out_status = false;
        break;
    }
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo TEXT_NEW_COMMENTS; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><hr /></td>
      </tr>

      <tr>
        <td><?php echo tep_draw_form('new_generic_text', basename($PHP_SELF), (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '') . '&action=preview', 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main"><b><?php echo TEXT_COMMENTS_STATUS; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_radio_field('status', '0', $out_status) . '&nbsp;' . TEXT_COMMENTS_NOT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('status', '1', $in_status) . '&nbsp;' . TEXT_COMMENTS_AVAILABLE; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo TEXT_COMMENTS_ORDER_STATUS; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_pull_down_menu('orders_status_id', $orders_status_array, $gtInfo->orders_status_id); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo TEXT_COMMENTS_NAME; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_input_field('orders_comments_title', (isset($orders_comments_title)?$orders_comments_title:$gtInfo->orders_comments_title), 'size="70"'); ?></td>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo TEXT_COMMENTS_DESCRIPTION; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_textarea_field('orders_comments_text', 'soft', '70', '15', (isset($orders_comments_text)?$orders_comments_text:$gtInfo->orders_comments_text) ); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo tep_image_submit('button_preview.gif', IMAGE_PREVIEW) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } elseif ($action == 'preview') {

    if (tep_not_null($_POST)) {
      $gtInfo = new objectInfo($_POST);
      $orders_comments_title = $_POST['orders_comments_title'];
      $orders_comments_text = $_POST['orders_comments_text'];
      $orders_status_id = $_POST['orders_status_id'];
    } else {
      $generic_text_query = tep_db_query("select orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id from " . TABLE_ORDERS_COMMENTS . " where orders_comments_id = '" . (int)$_GET['gtID'] . "'");
      $generic_text = tep_db_fetch_array($generic_text_query);
      $gtInfo = new objectInfo($generic_text);
    }

    $form_action = (isset($_GET['gtID'])) ? 'update' : 'insert';
?>
      <tr>
        <td><?php echo tep_draw_form($form_action, basename($PHP_SELF), (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="pageHeading"><?php echo $gtInfo->orders_comments_title; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    if ($gtInfo->orders_comments_text) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo $gtInfo->orders_comments_text; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    }
?>

<?php
    if (isset($_GET['read']) && ($_GET['read'] == 'only')) {
      if (isset($_GET['origin'])) {
        $pos_params = strpos($_GET['origin'], '?', 0);
        if ($pos_params != false) {
          $back_url = substr($_GET['origin'], 0, $pos_params);
          $back_url_params = substr($_GET['origin'], $pos_params + 1);
        } else {
          $back_url = $_GET['origin'];
          $back_url_params = '';
        }
      } else {
        $back_url = basename($PHP_SELF);
        $back_url_params = 'gtID=' . $gtInfo->orders_comments_id;
      }
?>
          <tr>
            <td align="right"><?php echo '<a href="' . tep_href_link($back_url, $back_url_params, 'NONSSL') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
<?php
    } else {
?>
          <tr>
            <td align="right" class="smallText">
<?php
// Re-Post all POSTed variables
      reset($_POST);
      while (list($key, $value) = each($_POST)) {
        if (!is_array($_POST[$key])) {
          echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
        }
      }
//      echo tep_draw_hidden_field('orders_status_id', $orders_status_id);
      echo tep_draw_hidden_field('orders_comments_title', htmlspecialchars(stripslashes($orders_comments_title)));
      echo tep_draw_hidden_field('orders_comments_text', htmlspecialchars(stripslashes($orders_comments_text)));
      echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="edit"') . '&nbsp;&nbsp;';

      if (isset($_GET['gtID'])) {
        echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
      } else {
        echo tep_image_submit('button_insert.gif', IMAGE_INSERT);
      }
      echo '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?>
            </td>
          </tr>
        </table></form></td>
      </tr>
<?php
    }
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
<?php
    echo tep_draw_form('search', basename($PHP_SELF), '', 'get');
    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
    echo tep_draw_hidden_field(tep_session_name(),tep_session_id());
    echo '</form>';
?>
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TITLE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $generic_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $generic_text_query = tep_db_query("select orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id from " . TABLE_ORDERS_COMMENTS . " where (orders_comments_title like '%" . tep_db_input($search) . "%' or orders_comments_text like '%" . tep_db_input($search) . "%') order by orders_comments_title");
    } else {
      $generic_text_query = tep_db_query("select orders_comments_id, orders_comments_title, orders_comments_text, orders_status_id, status_id from " . TABLE_ORDERS_COMMENTS . " order by orders_comments_title");
    }

    while ($generic_text = tep_db_fetch_array($generic_text_query)) {
      if( (!isset($_GET['gtID']) && !isset($gtInfo) ) || (isset($_GET['gtID']) && ($_GET['gtID'] == $generic_text['orders_comments_id'])) && (substr($action, 0, 3) != 'new')) {
        $gtInfo = new objectInfo($generic_text);
      }
      $generic_count++;
      $rows++;

      if (isset($gtInfo) && is_object($gtInfo) && ($generic_text['orders_comments_id'] == $gtInfo->orders_comments_id) ) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(basename($PHP_SELF), 'gtID=' . $generic_text['orders_comments_id'] . '&action=preview&read=only') . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(basename($PHP_SELF), 'gtID=' . $generic_text['orders_comments_id']) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $generic_text['orders_comments_id'] . '&action=preview&read=only') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $generic_text['orders_comments_id']; ?></td>
                <td class="dataTableContent"><?php echo $generic_text['orders_comments_title']; ?></td>
                <td class="dataTableContent"><?php echo $display_status_array[$generic_text['orders_status_id']]; ?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($generic_text['status_id'] == '1') {
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'action=setflag&flag=0&gtID=' . $generic_text['orders_comments_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
      } else {
        echo '<a href="' . tep_href_link(basename($PHP_SELF), 'action=setflag&flag=1&gtID=' . $generic_text['orders_comments_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
      }
?>
                </td>
                <td class="dataTableContent" align="right"><?php if (isset($gtInfo) && is_object($gtInfo) && ($generic_text['orders_comments_id'] == $gtInfo->orders_comments_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $generic_text['orders_comments_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo TEXT_COMMENTS . '&nbsp;' . $generic_count; ?></td>
                    <td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo '<a href="' . tep_href_link(basename($PHP_SELF), 'action=new_generic_text') . '">' . tep_image_button('button_new.gif', IMAGE_NEW_COMMENTS_TEXT) . '</a>'; ?>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();
    switch ($action) {
      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_COMMENTS . '</b>');

        $contents = array('form' => tep_draw_form('generic_text', basename($PHP_SELF), 'action=delete_confirm') . tep_draw_hidden_field('orders_comments_id', $gtInfo->orders_comments_id));
        $contents[] = array('text' => TEXT_DELETE_COMMENTS_INTRO);
        $contents[] = array('text' => '<br><b>' . $gtInfo->orders_comments_title . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $gtInfo->orders_comments_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      case 'copy_to':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_COPY_TO . '</b>');
        $contents = array('form' => tep_draw_form('copy_to', basename($PHP_SELF), 'action=copy_to_confirm') . tep_draw_hidden_field('orders_comments_id', $gtInfo->orders_comments_id));
        $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
        $contents[] = array('text' => tep_draw_hidden_field('copy_as', 'duplicate'));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_copy.gif', IMAGE_COPY) . ' <a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $gtInfo->orders_comments_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      default:
        if ($rows > 0) {
          if (isset($gtInfo) && is_object($gtInfo)) { // generic_text info box contents
            $heading[] = array('text' => '<b>' . $gtInfo->orders_comments_title . '</b>');
            $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $gtInfo->orders_comments_id . '&action=new_generic_text') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $gtInfo->orders_comments_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a> <a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $gtInfo->orders_comments_id . '&action=copy_to') . '">' . tep_image_button('button_copy_to.gif', IMAGE_COPY_TO) . '</a>');
            $contents[] = array('text' => '<br>' . $gtInfo->orders_comments_title);
          }
        } else { // create generic_text dummy info
          $heading[] = array('text' => '<b>' . EMPTY_COMMENTS . '</b>');
          $contents[] = array('text' => TEXT_NO_COMMENTS);
        }
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }

?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
