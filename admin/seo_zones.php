<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// SEO-G component for osCommerce Admin
// Generates url names from parameters
// Featuring:
// - Products, Categories, Articles
// - Multi-Products instant selection
// - Multi-Categories instant selection
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
  require(DIR_WS_CLASSES . FILENAME_SEO_ZONES);

// initialize the abstract zone class for different type support
  if( isset($_GET['zID']) && tep_not_null($_GET['zID']) ) {
    $seozone_query = tep_db_query("select seo_types_class, seo_types_name from " . TABLE_SEO_TYPES . " where seo_types_id = '" . (int)$_GET['zID'] . "'");
    if( $seozone = tep_db_fetch_array($seozone_query) ) {
      $seozone_script = $seozone['seo_types_class'];
    }
  }

  if( isset($seozone_script) && tep_not_null($seozone_script) ) {
    require(DIR_WS_CLASSES . $seozone_script . '.php');
  } else {
    $seozone_script = 'seo_zones';
  }

  $cSEO = new $seozone_script();
  $cSEO->process_saction();
  $cSEO->process_action();

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td colspan="2" width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">
<?php 
  echo HEADING_TITLE; 
  if (isset($_GET['zID']) && tep_not_null($_GET['zID']) ) {
    echo '&nbsp;&raquo;&nbsp;' . $seozone['seo_types_name'];
  }
?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" width="75%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
  echo $cSEO->display_html();
?>
            </table></td>
          </tr>


        </table></td>
<?php
  echo $cSEO->display_right_box();
?>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
