<?php
/*
  $Id: admin_email_templates.php,v 1.0 2005/01/08 10:50:10 idabagusmade Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Email Templates for osC Admin
// - Moved language strings into a separate file
// - Removed template functions
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/


  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $out = '<div align=right><a href="' . tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, 'action=update&id=0', 'NONSSL') . '">' . TITLE_ADD_EMAIL_TEMPLATE . '</a> | <a href="' . tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, '', 'NONSSL') . '">' . TITLE_LIST_EMAIL_TEMPLATE . '</a></div>';

  if( $action == 'update') {
    if( isset($_POST['Submit_Action']) && tep_not_null($_POST['Submit_Action']) ) {
      $id = tep_db_prepare_input($_POST['id']);
      $delete = (isset($_POST['delete']) ? $_POST['delete'] : '');
      $title = tep_db_prepare_input($_POST['title']);
      $subject = tep_db_prepare_input($_POST['subject']);
      $contents = tep_db_prepare_input($_POST['contents']);
      $group_template = (tep_not_null($_POST['grp_new'])) ? tep_db_prepare_input($_POST['grp_new']) : tep_db_prepare_input($_POST['grp']);
        
        
      if(!tep_not_null($group_template)){
        $messageStack->add_session('Email template group is empty!', 'error');
        tep_redirect(tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, 'action=update' . '&id=' . $id));
      }
      if(!tep_not_null($_POST['title'])){
        $messageStack->add_session('Email template title is empty!', 'error');
        tep_redirect(tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, 'action=update' . '&id=' . $id));
      }
  
      if((int)$id > 0) {
        if(tep_not_null($delete)) {
          $sql = "delete from " . TABLE_EMAIL_TEMPLATES . " where id = '" . (int)$id . "'";
        } else {
          $sql = "update " . TABLE_EMAIL_TEMPLATES . " set grp = '" . tep_db_input($group_template) . "',  title = '" . tep_db_input($title) . "',  subject = '" . tep_db_input($subject) . "',  contents = '" . tep_db_input($contents) . "',  updated = '" . date("Y-m-d H:i:s") . "' where id = '" . (int)$id . "'";
          $messageStack->add_session('Email template "' . $title . '" updated.', 'success');
        }
      } else {
        $sql = "insert into " . TABLE_EMAIL_TEMPLATES . " (grp, title, subject, contents, updated) values ('" . tep_db_input($group_template) . "', '" . tep_db_input($title) . "', '" . tep_db_input($subject) . "', '" . tep_db_input($contents) . "', '" . date("Y-m-d H:i:s")."') ";
        $messageStack->add_session('Email template "' . $title . '" added.', 'success');
      }
      $tmp = tep_db_query($sql); 
      tep_redirect(tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, ''));
    }
      
    $hidden_field = tep_draw_hidden_field('id', '0') . tep_draw_hidden_field('Submit_Action', '1');
    $submit_field = tep_image_submit('button_save.gif', IMAGE_SAVE); 
    $form_title   = TABLE_HEADING_ADD;

    $P = array(
               'grp' => '',
               'title' => '',
               'subject' => '',
               'contents' => '',
              );
    if( isset($_POST['id']) && tep_not_null($_POST['id']) || tep_not_null($_GET['id'])) {
      $id  = (isset($_POST['id']) && tep_not_null($_POST['id'])) ? $_POST['id'] : $_GET['id'];
      $result = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where id = '" . (int)$id . "'");
      if(tep_db_num_rows($result)) {
        $hidden_field = tep_draw_hidden_field('id', $id) . tep_draw_hidden_field('Submit_Action', '1');
        $submit_field = tep_draw_checkbox_field('delete', 1, false, '') . '<font color="red">Delete?&nbsp;&nbsp;&nbsp;&nbsp;' . $submit_field;
        $form_title   = TABLE_HEADING_UPDATE;
        $P = tep_db_fetch_array($result);
      }
    }
        
    $grp_text = tep_get_group($P['grp']);
      
    $out .= tep_draw_form('email_template', FILENAME_ADMIN_EMAIL_TEMPLATES, 'action=' . $_GET['action'], 'post') . '
                      <table width=100% border=0 cellspacing=1 cellpadding=2>
                          <tr class="dataTableHeadingRow"><td class="dataTableHeadingContent" colspan="2" height="25"><b>' . $form_title . '</b></td></tr>
                          <tr class="dataTableRow"><td class="dataTableContent" width="30%"><b>' . TABLE_HEADING_GROUP . '</b>' . TEXT_FIELD_REQUIRED . '</td><td class="dataTableContent">' . $grp_text . '</td></tr>
                          <tr class="dataTableRow"><td class="dataTableContent"><b>' . TABLE_HEADING_TITLE . '</b>' . TEXT_FIELD_REQUIRED . '</td><td>' . tep_draw_input_field('title', stripslashes($P['title']), 'style="width:95%"') . '</td></tr>
                          <tr class="dataTableRow"><td class="dataTableContent"><b>' . TABLE_HEADING_EMAIL_SUBJECT . '</b></td><td>' . tep_draw_input_field('subject', stripslashes($P['subject']), 'style="width:95%"') . '</td></tr>
                          <tr class="dataTableRow"><td class="dataTableContent" colspan="2" valign="top"><b>' . TABLE_HEADING_EMAIL_CONTENTS . '</b><br />' . tep_draw_textarea_field('contents', 'soft', '60', '15', stripslashes($P['contents']), 'style="width:100%"', 'false') . '</td></tr>
                          <tr class="dataTableRow"><td class="dataTableContent" colspan="2" align="center"><br><p>' . $hidden_field . $submit_field . '</p>&nbsp;</td></tr>
                      </table></form>';
  } elseif($action=="" || $action=="search") {
    $result  = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " order by grp asc");
    
    $out .= '<table width=100% border=0 cellspacing=1 cellpadding=2>
                        <tr class="dataTableHeadingRow" height="25">
                            <td class="dataTableHeadingContent"><b>' . TABLE_HEADING_TITLE . '</b></td>
                            <td class="dataTableHeadingContent"><b>' . TABLE_HEADING_GROUP . '</b></td>
                            <td class="dataTableHeadingContent"><b>' . TABLE_HEADING_EMAIL_SUBJECT . '</b></td>
                            <td  class="dataTableHeadingContent" align=right><b>' . TABLE_HEADING_LAST_UPDATE . '</b></td>
                        </tr>';
    while ($line = tep_db_fetch_array($result)) {
        $out .= '<tr class="dataTableRow">
                            <td class="dataTableContent" valign="top">' . '<a href="' . tep_href_link(FILENAME_ADMIN_EMAIL_TEMPLATES, 'action=update' . '&id=' . $line['id']) . '">' . stripslashes($line['title']) . '</a>' . '</td>
                            <td class="dataTableContent" valign="top">' . stripslashes($line['grp']) . '</td>
                            <td class="dataTableContent" valign="top">' . stripslashes($line['subject']) . '</td>
                            <td class="dataTableContent" valign="top" align="right">' . tep_date_long($line['updated']) . '</td>
                         </tr>';
    }
    $out .= '</table>';
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<?php
  if($action == 'update') {
    $mce_str = 'contents'; // YOURCODEHERE // Comma separated list of textarea names
  // You can add more textareas to convert in the $mce_str, be careful that they are all separated by a comma.
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    include "includes/javascript/tiny_mce/general2.php";
  }
?>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top">
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td>
<?php 
###############################################################################################
    echo $out;
###############################################################################################
?>
            </td>
          </tr>
        </table>
    </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
