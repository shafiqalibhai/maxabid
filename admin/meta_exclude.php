<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Dictionary for the Admin end
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require(DIR_WS_CLASSES . FILENAME_META_ZONES);
  $cMeta = new meta_zones();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch($action) {
    case 'setflag':
      $sql_data_array = array('meta_exclude_status' => (int)$_GET['flag']);
      tep_db_perform(TABLE_META_EXCLUDE, $sql_data_array, 'update', "meta_exclude_key='" . tep_db_prepare_input($_GET['tag_id']) . "'");
      tep_redirect(tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action','flag','tag_id')) ));
      break;

    case 'insert':
      $phrase_key = md5($_POST['phrase']);
      $check_query = tep_db_query("select count(*) as total from " . TABLE_META_EXCLUDE . " where meta_exclude_key = '" . tep_db_input(tep_db_prepare_input($phrase_key)) . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $sql_data_array = array(
                                'meta_exclude_key' => tep_db_prepare_input($phrase_key),
                                'meta_exclude_text' => tep_db_prepare_input($_POST['phrase'])
                               );
        tep_db_perform(TABLE_META_EXCLUDE, $sql_data_array);
      }
      tep_redirect(tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) ));
      break;

    case 'update_multi':
      foreach ($_POST['tag_id'] as $key=>$value) {

        tep_db_query("delete from " . TABLE_META_EXCLUDE . " where meta_exclude_key = '" . tep_db_input(tep_db_prepare_input($key)) . "'");

        $sql_data_array = array(
                                'meta_exclude_key' => tep_db_prepare_input(md5($_POST['phrase'][$key])),
                                'meta_exclude_text' => tep_db_prepare_input($_POST['phrase'][$key])
                               );

        tep_db_perform(TABLE_META_EXCLUDE, $sql_data_array);
        //tep_db_perform(TABLE_META_EXCLUDE, $sql_data_array, 'update', "meta_exclude_key = '" . tep_db_input(tep_db_prepare_input($key)) . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) ));
      break;

    case 'delete_confirm_multi':
      foreach ($_POST['tag_id'] as $key=>$value) {
        tep_db_query("delete from " . TABLE_META_EXCLUDE . " where meta_exclude_key = '" . tep_db_input(tep_db_prepare_input($value)) . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) ));
      break;

    default:
      break;
  }

  $lexico_query = tep_db_query("select meta_types_id as id, meta_types_name as text from " . TABLE_META_TYPES . " where meta_types_status='1' order by sort_order");
  while($lexico_array[] = tep_db_fetch_array($lexico_query) );
  array_pop($lexico_array);

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if( $action == 'delete_multi' ) {
?>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_DELETE; ?></td>
      </tr>

      <tr>
        <td valign="top"><?php echo tep_draw_form('rl_confirm', FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=delete_confirm_multi', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PHRASE; ?></td>
          </tr>
<?php
    $rows = 0;
    foreach ($_POST['tag_id'] as $key=>$value) {
      $delete_query = tep_db_query("select meta_exclude_text from " . TABLE_META_EXCLUDE . " where meta_exclude_key = '" . tep_db_input(tep_db_prepare_input($key)) . "'");
      if( $delete_array = tep_db_fetch_array($delete_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                  <tr class="' . $row_class . '">';    
?>
            <td class="dataTableContent"><?php echo tep_draw_hidden_field('tag_id[]', $key) . $delete_array['meta_exclude_text']; ?></td>
          </tr>
<?php
      }
    }
?>

          <tr>
            <td>
<?php 
    if( count($_POST['tag_id']) ) {
      echo '<a href="' . tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) ) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
    }
?>
            </td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_MAIN; ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
// Catalog File List Stored in the database
    $rows = 0;
    $meta_query_raw = "select * from " . TABLE_META_EXCLUDE . " order by meta_exclude_text";
    $meta_split = new splitPageResults($_GET['page'], SEO_PAGE_SPLIT, $meta_query_raw, $meta_query_numrows, 'meta_exclude_key');
    if( $meta_query_numrows > 0 ) {
?>
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl', FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=delete_multi', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'tag_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PHRASE; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                  </tr>
<?php
      $meta_query = tep_db_query($meta_query_raw);
      $bCheck = false;
      while ($meta = tep_db_fetch_array($meta_query)) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('tag_id[' . $meta['meta_exclude_key'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                    <td class="dataTableContent"><?php echo tep_draw_input_field('phrase[' . $meta['meta_exclude_key'] . ']', $meta['meta_exclude_text'], '', false, 'text', true); ?></td>
                    <td class="dataTableContent" align="center">
<?php
    if ($meta['meta_exclude_status'] == '1') {
      echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_META_EXCLUDE, 'action=setflag&flag=0&tag_id=' . $meta['meta_exclude_key'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
    } else {
      echo '<a href="' . tep_href_link(FILENAME_META_EXCLUDE, 'action=setflag&flag=1&tag_id=' . $meta['meta_exclude_key'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
    }
?>
                    </td>
                  </tr>
<?php
      }
?>
                  <tr>
                    <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td>
<?php 
        echo tep_image_submit('button_update.gif', TEXT_UPDATE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=update_multi') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=delete_multi') . '\'' . '"');
        //echo tep_image_submit('button_delete.gif', TEXT_DELETE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=delete_multi') . '\'' . '"');
?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table></form></td>
              </tr>

              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $meta_split->display_count($meta_query_numrows, SEO_PAGE_SPLIT, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_SCRIPTS); ?></td>
                    <td class="smallText" align="right"><?php echo $meta_split->display_links($meta_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('action', 'page')), 'page'); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php 
    }
?>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td><hr /></td>
              </tr>
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE2; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_INFO_MAIN2; ?></td>
              </tr>
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="formArea"><?php echo tep_draw_form('mz', FILENAME_META_EXCLUDE, tep_get_all_get_params(array('action')) . 'action=insert', 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="4">
<?php
      echo '<td nowrap class="smallText"><b>Exclude Word:</b></td>' . "\n";
      echo '<td>' . tep_draw_input_field('phrase', '', '', false, 'text', true) . '</td>' . "\n";
      echo '<td nowrap>' . tep_image_submit('button_insert.gif', 'Enter the word to exclude') . '</td>' . "\n";
      echo '<td nowrap width="90%">&nbsp;</td>' . "\n";
?>
                    </table></form></td>
                  </tr>
                </table></td>
              </tr>

            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
