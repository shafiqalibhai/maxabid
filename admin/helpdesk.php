<?php
/*
  $Id: helpdesk.php,v 1.6 2005/08/16 21:14:04 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $subaction = (isset($_GET['subaction']) ? $_GET['subaction'] : '');
  $page = (isset($_GET['page']) ? $_GET['page'] : 1);
  $ticket = (isset($_GET['ticket']) ? tep_db_prepare_input($_GET['ticket']) : '');
  $template_id = (isset($_GET['template_id']) ? (int)$_GET['template_id'] : '');
  $department_filter = (isset($_GET['department_filter']) ? (int)$_GET['department_filter'] : '');
  $status_filter = (isset($_GET['status_filter']) ? (int)$_GET['status_filter'] : '');
  $priority_filter = (isset($_GET['priority_filter']) ? (int)$_GET['priority_filter'] : '');
  $entry_filter = (isset($_GET['entry_filter']) ? (int)$_GET['entry_filter'] : '');

  switch ($action) {
    case 'reply_confirm':
      $id = tep_db_prepare_input($_GET['id']);
      $status_id = tep_db_prepare_input($_POST['status']);
      $from_name = tep_db_prepare_input($_POST['from_name']);
      $from_email_address = tep_db_prepare_input($_POST['from_email_address']);
      $to_name = tep_db_prepare_input($_POST['to_name']);
      $to_email_address = tep_db_prepare_input($_POST['to_email_address']);
      $subject = tep_db_prepare_input($_POST['subject']);
      $body = tep_db_prepare_input($_POST['body']);

      $sql_data_array = array('ticket' => $ticket,
                              'parent_id' => $id,
                              'message_id' => '',
                              'ip_address' => getenv('SERVER_ADDR'),
                              'host' => getenv('SERVER_NAME'),
                              'datestamp_local' => 'now()',
                              'datestamp' => 'now()',
                              'receiver' => $to_name,
                              'receiver_email_address' => $to_email_address,
                              'sender' => $from_name,
                              'email_address' => $from_email_address,
                              'subject' => $subject,
                              'body' => $body,
                              'entry_read' => '1');

      tep_db_perform(TABLE_HELPDESK_ENTRIES, $sql_data_array);
      $sql_data_array = array('status_id' => $status_id,
                              'datestamp_last_entry' => 'now()');
      tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array, 'update', "ticket = '" . tep_db_input($ticket) . "'");
      tep_mail($to_name, $to_email_address, $subject, $body, $from_name, $from_email_address);

      $messageStack->add_session(SUCCESS_REPLY_PROCESSED, 'success');
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view'));
      break;
    case 'save_entry':
      $department_id = tep_db_prepare_input($_POST['department']);
      $status_id = tep_db_prepare_input($_POST['status']);
      $priority_id = tep_db_prepare_input($_POST['priority']);

      $id = tep_db_prepare_input($_GET['id']);
      $from_name = tep_db_prepare_input($_POST['from_name']);
      $from_email_address = tep_db_prepare_input($_POST['from_email_address']);
      $to_name = tep_db_prepare_input($_POST['to_name']);
      $to_email_address = tep_db_prepare_input($_POST['to_email_address']);
      $subject = tep_db_prepare_input($_POST['subject']);
      $body = tep_db_prepare_input($_POST['body']);

      $entry_query = tep_db_query("select ticket, datestamp_local from " . TABLE_HELPDESK_ENTRIES . " where helpdesk_entries_id = '" . tep_db_input($id) . "'");
      $entry = tep_db_fetch_array($entry_query);

      $sql_data_array = array('department_id' => $department_id,
                              'status_id' => $status_id,
                              'priority_id' => $priority_id);

      if ($entry['ticket'] == $ticket) {
        $new_ticket = false;

        tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array, 'update', "ticket = '" . tep_db_input($ticket) . "'");
      } else {
        $new_ticket = true;

        $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        $check = tep_db_fetch_array($check_query);

        if ($check['count'] > 0) {
          $ticket_date_query = tep_db_query("select datestamp_last_entry from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
          $ticket_date = tep_db_fetch_array($ticket_date_query);

          if ($entry['datestamp_local'] > $ticket_date['datestamp_last_entry']) {
            $sql_data_array['datestamp_last_entry'] = $entry['datestamp_local'];
          }

          tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array, 'update', "ticket = '" . tep_db_input($ticket) . "'");
        } else {
          $sql_data_array['ticket'] = $ticket;
          $sql_data_array['datestamp_last_entry'] = $entry['datestamp_local'];

          tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array);
        }
      }

      $sql_data_array = array('ticket' => $ticket,
                              'receiver' => $to_name,
                              'receiver_email_address' => $to_email_address,
                              'sender' => $from_name,
                              'email_address' => $from_email_address,
                              'subject' => $subject,
                              'body' => $body);

      if ($new_ticket == true) $sql_data_array['parent_id'] = '0';

      tep_db_perform(TABLE_HELPDESK_ENTRIES, $sql_data_array, 'update', "helpdesk_entries_id = '" . tep_db_input($id) . "'");

      $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . $entry['ticket'] . "'");
      $check = tep_db_fetch_array($check_query);

      $ticket_exists = true;
      if ($check['count'] < 1) {
         $ticket_exists = false;
        tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . $entry['ticket'] . "'");
      }

      $messageStack->add_session(SUCCESS_ENTRY_UPDATED, 'success');
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $ticket . '&action=view&id=' . $_GET['id']));
      break;
    case 'delete_confirm':

      foreach( $_POST['ticket'] as $key => $value ) {
        $ticket = tep_db_prepare_input($key);
        tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");

        $check_query = tep_db_query("select attachment from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($ticket) . "'");
        while($check_array = tep_db_fetch_array($check_query) ) {
          if( file_exists($check_array['attachment']) ) {
            unlink($check_array['attachment']);
          }
        }
        tep_db_query("delete from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($ticket) . "'");
      }
      $messageStack->add_session(SUCCESS_ENTRY_REMOVED, 'success');
/*
      $ticket = tep_db_prepare_input($_GET['ticket']);
      $id = tep_db_prepare_input($_GET['id']);
      $whole = tep_db_prepare_input($_POST['whole']);

      if ($whole == 'true') {
        tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");

        $messageStack->add_session(SUCCESS_WHOLE_THREAD_REMOVED, 'success');
      } else {
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where helpdesk_entries_id = '" . tep_db_input($id) . "'");

        $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");
        $check = tep_db_fetch_array($check_query);

        if ($check['count'] > 0) {
          tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view'));
        } else {
          tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        }
        $messageStack->add_session(SUCCESS_ENTRY_REMOVED, 'success');
      }
*/
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page));
      break;

    case 'delete_confirm_single':
      $id = tep_db_prepare_input($_GET['id']);
      $whole = tep_db_prepare_input($_POST['whole']);

      if ($whole == 'true') {
        tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");
        $check_query = tep_db_query("select attachment from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($ticket) . "'");
        while($check_array = tep_db_fetch_array($check_query) ) {
          if( file_exists($check_array['attachment']) ) {
            unlink($check_array['attachment']);
          }
        }
        tep_db_query("delete from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($ticket) . "'");

        $messageStack->add_session(SUCCESS_WHOLE_THREAD_REMOVED, 'success');
      } else {
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "' and helpdesk_entries_id = '" . tep_db_input($id) . "'");

        $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "' and parent_id=0");
        $check = tep_db_fetch_array($check_query);

        if ($check['count'] > 0) {
          tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view'));
        } else {
          tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");
          tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        }

        $check_query = tep_db_query("select attachment from " . TABLE_HELPDESK_ATTACHMENTS . " where helpdesk_entries_id= '" . (int)$id . "'");
        while($check_array = tep_db_fetch_array($check_query) ) {
          if( file_exists($check_array['attachment']) ) {
            unlink($check_array['attachment']);
          }
        }
        tep_db_query("delete from " . TABLE_HELPDESK_ATTACHMENTS . " where helpdesk_entries_id = '" . (int)$id . "'");

        $messageStack->add_session(SUCCESS_ENTRY_REMOVED, 'success');
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page));
      break;
    case 'updatestatus':
      $department_id = tep_db_prepare_input($_POST['department']);
      $status_id = tep_db_prepare_input($_POST['status']);
      $priority_id = tep_db_prepare_input($_POST['priority']);

      tep_db_query("update " . TABLE_HELPDESK_TICKETS . " set department_id = '" . tep_db_input($department_id) . "', status_id = '" . tep_db_input($status_id) . "', priority_id = '" . tep_db_input($priority_id) . "' where ticket = '" . tep_db_input($ticket) . "'");

      $messageStack->add_session(SUCCESS_TICKET_UPDATED, 'success');
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $ticket . '&action=view'));
      break;
    case 'view':
      $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");
      $check = tep_db_fetch_array($check_query);

      if ($check['count'] < 1) {
        $messageStack->add_session(ERROR_TICKET_DOES_NOT_EXIST, 'error');
        tep_redirect(tep_href_link(basename($PHP_SELF), 'ticket=' . $ticket));
      }
      break;
    case 'updatecomment':
      $comment = tep_db_prepare_input($_POST['comment']);

      $sql_data_array = array('comment' => $comment,
                              'datestamp_comment' => 'now()');

      tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array, 'update', "ticket = '" . tep_db_input($ticket) . "'");

      $messageStack->add_session(SUCCESS_COMMENT_UPDATED, 'success');
      tep_redirect(tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view'));
      break;
    case 'delete':
      if( !isset($_POST['ticket']) || !is_array($_POST['ticket']) || !count($_POST['ticket']) ) {
        $messageStack->add_session(ERROR_TICKET_DOES_NOT_EXIST, 'error');
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action','subaction','ticket')) . '&action=view'));
      }
      break;
    default:
      break;
  }

  $statuses_array = array();
  $priorities_array = array();
  $departments_array = array();
  $entries_array = array(array('id' => '0', 'text' => TEXT_ALL_ENTRIES),
                         array('id' => '1', 'text' => TEXT_ONLY_NEW_ENTRIES));

  if(empty($action) ) {
    $statuses_array[] = array('id' => '0', 'text' => TEXT_ALL_STATUSES);
    $priorities_array[] = array('id' => '0', 'text' => TEXT_ALL_PRIORITIES);
    $departments_array[] = array('id' => '0', 'text' => TEXT_ALL_DEPARTMENTS);
  }

  $statuses_query = tep_db_query("select status_id, title from " . TABLE_HELPDESK_STATUSES . " order by title");
  while ($statuses = tep_db_fetch_array($statuses_query)) {
    $statuses_array[] = array('id' => $statuses['status_id'], 'text' => $statuses['title']);
  }

  $priorities_query = tep_db_query("select priority_id, title from " . TABLE_HELPDESK_PRIORITIES . " order by title");
  while ($priorities = tep_db_fetch_array($priorities_query)) {
    $priorities_array[] = array('id' => $priorities['priority_id'], 'text' => $priorities['title']);
  }

  $departments_query = tep_db_query("select department_id, title from " . TABLE_HELPDESK_DEPARTMENTS . " order by title");
  while ($departments = tep_db_fetch_array($departments_query)) {
    $departments_array[] = array('id' => $departments['department_id'], 'text' => $departments['title']);
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<?php
  if( $action == 'view' && ($subaction == 'edit' || $subaction == 'reply') ) {
    $mce_str = 'body'; // YOURCODEHERE // Comma separated list of textarea names
  // You can add more textareas to convert in the $mce_str, be careful that they are all separated by a comma.
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    include "includes/javascript/tiny_mce/general.php";
  }
?>
<script language="javascript" type="text/javascript"><!--
  var g_checkbox = 0;
  function check_boxes(form) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        form.elements[i].checked = g_checkbox?"":"on";
      }
    }
    g_checkbox ^= 1;
  }
//--></script>
</head>
<body onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', '1', HEADING_IMAGE_HEIGHT); ?></td>
<?php
  if( empty($action) ) {
?>
            <td class="formArea"><table border="0" cellspacing="1" cellpadding="2">
              <tr>
                <td><table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo 'Keyword'; ?></td>
                    <td class="smallText"><?php echo tep_draw_form('keyword', basename($PHP_SELF), '', 'get') . tep_draw_input_field('keyword', '', 'size="10" maxlength="17"') . '</form>'; ?></td>
                  </tr>
                  <tr>
                    <td class="smallText"><?php echo TEXT_TICKET_NUMBER; ?></td>
                    <td class="smallText"><?php echo tep_draw_form('ticket', basename($PHP_SELF), '', 'get') . tep_draw_hidden_field('page', $page) . tep_draw_input_field('ticket', '', 'size="10" maxlength="7"') . tep_draw_hidden_field('action', 'view') . '</form>'; ?></td>
                  </tr>
                </table></td>
                <td align="right"><?php echo tep_draw_form('filter', basename($PHP_SELF), '', 'get'); ?><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td align="right"><table border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText"><?php echo TEXT_ENTRIES; ?></td>
                        <td class="smallText" align="right"><?php echo tep_draw_pull_down_menu('entry_filter', $entries_array, $entry_filter, 'onchange="this.form.submit();"' . (!empty($entry_filter) ? ' style="background-color: #fedecb;"' : '')); ?></td>
                      </tr>
                      <tr>
                        <td class="smallText"><?php echo TEXT_DEPARTMENT; ?></td>
                        <td class="smallText" align="right"><?php echo tep_draw_pull_down_menu('department_filter', $departments_array, $department_filter, 'onChange="this.form.submit();"' . (!empty($department_filter) ? ' style="background-color: #fedecb;"' : '')); ?></td>
                      </tr>
                    </table></td>
                    <td><table border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText"><?php echo TEXT_STATUS; ?></td>
                        <td class="smallText" align="right"><?php echo tep_draw_pull_down_menu('status_filter', $statuses_array, $status_filter, 'onchange="this.form.submit();"' . (!empty($status_filter) ? ' style="background-color: #fedecb;"' : '')); ?></td>
                      </tr>
                      <tr>
                        <td class="smallText"><?php echo TEXT_PRIORITY; ?></td>
                        <td class="smallText" align="right"><?php echo tep_draw_pull_down_menu('priority_filter', $priorities_array, $priority_filter, 'onchange="this.form.submit();"' . (!empty($priority_filter) ? ' style="background-color: #fedecb;"' : '')); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></form></td>
              </tr>
            </table></td>
<?php
  } elseif( $subaction == 'reply') {
    $templates_array = array(array('id' => '', 'text' => TEXT_PLEASE_SELECT));
    $templates_query = tep_db_query("select template_id, title from " . TABLE_HELPDESK_TEMPLATES . " order by title");
    while ($templates = tep_db_fetch_array($templates_query)) {
      $templates_array[] = array('id' => $templates['template_id'], 'text' => $templates['title']);
    }
?>
            <td class="smallText" align="right"><?php echo tep_draw_form('ticket', basename($PHP_SELF), '', 'get') . tep_draw_hidden_field('page', $page) . tep_draw_hidden_field('ticket', $_GET['ticket']) . tep_draw_hidden_field('action', 'view') . tep_draw_hidden_field('id', $_GET['id']) . tep_draw_hidden_field('subaction', 'reply') . TEXT_TEMPLATES . ' ' . tep_draw_pull_down_menu('template_id', $templates_array, $template_id, 'onChange="this.form.submit();"') . '</form>'; ?></td>
<?php
  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  if($action == 'view') {
    $id = (isset($_GET['id']) ? (int)$_GET['id'] : '');

    if( !empty($id) ) {
      $entry_query = tep_db_query("select he.helpdesk_entries_id, he.ticket, ifnull(he.host, he.ip_address) as host, he.ip_address, he.datestamp_local, he.datestamp, he.receiver, he.receiver_email_address, he.sender, he.email_address, he.subject, he.body, he.entry_read, ht.department_id, ht.status_id, ht.priority_id from " . TABLE_HELPDESK_ENTRIES . " he left join " . TABLE_HELPDESK_TICKETS . " ht on (he.ticket = ht.ticket) where he.ticket = '" . tep_db_input($ticket) . "' and he.helpdesk_entries_id = '" . tep_db_input($id) . "'");
    } else {
      $entry_query = tep_db_query("select he.helpdesk_entries_id, he.ticket, ifnull(he.host, he.ip_address) as host, he.ip_address, he.datestamp_local, he.datestamp, he.receiver, he.receiver_email_address, he.sender, he.email_address, he.subject, he.body, he.entry_read, ht.department_id, ht.status_id, ht.priority_id from " . TABLE_HELPDESK_ENTRIES . " he left join " . TABLE_HELPDESK_TICKETS . " ht on (he.ticket = ht.ticket) where he.ticket = '" . tep_db_input($ticket) . "' and he.parent_id = '0'");
    }
    $entry = tep_db_fetch_array($entry_query);

// mark entry as read
    if ($entry['entry_read'] != '1') {
      tep_db_query("update " . TABLE_HELPDESK_ENTRIES . " set entry_read = '1' where ticket = '" . $entry['ticket'] . "' and helpdesk_entries_id = '" . $entry['helpdesk_entries_id'] . "'");
    }

    $department_query = tep_db_query("select hd.title, hd.email_address, hd.name from " . TABLE_HELPDESK_DEPARTMENTS . " hd, " . TABLE_HELPDESK_TICKETS . " ht where ht.ticket = '" . tep_db_input($ticket) . "' and ht.department_id = hd.department_id");
    $department = tep_db_fetch_array($department_query);

    if( $subaction == 'reply') {
      $template = '';

      if( !empty($template_id) ) {
        $template_query = tep_db_query("select template from " . TABLE_HELPDESK_TEMPLATES . " where template_id = '" . tep_db_input($template_id) . "'");
        $template = tep_db_fetch_array($template_query);
        $template = $template['template'];
      }

      $subject = $entry['subject'];
      if (!strstr($subject, '['. DEFAULT_HELPDESK_TICKET_PREFIX . $entry['ticket'] . ']')) {
        $subject = '[' .DEFAULT_HELPDESK_TICKET_PREFIX. $entry['ticket'] . '] ' . $subject;
      }
      $subject = $subject;
?>
      <tr>
        <td><?php echo tep_draw_form('reply', basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&id=' . $_GET['id'] . '&action=reply_confirm'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><?php echo TEXT_SEND_INTRO; ?></td>
                <td align="right"><?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><?php echo TEXT_STATUS; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_pull_down_menu('status', $statuses_array, $entry['status_id']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_FROM_NAME; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('from_name', $department['title']); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_FROM_EMAIL_ADDRESS; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('from_email_address', $department['email_address']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_TO_NAME; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('to_name', $entry['sender']); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_TO_EMAIL_ADDRESS; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('to_email_address', $entry['email_address']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_SUBJECT; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('subject', $subject); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText" valign="top"><?php echo TEXT_BODY; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_textarea_field('body', 'virtual', '60', '10', $template, 'style="width: 100%"'); ?></td>
              </tr>
            </table></td>
          </tr>          
        </table></form></td>
      </tr>
<?php
    } elseif( $subaction == 'edit') {
?>
      <tr>
        <td><?php echo tep_draw_form('edit', basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&id=' . $_GET['id'] . '&action=save_entry'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2" class="columnLeft">
              <tr>
                <td class="smallText"><?php echo TEXT_UPDATE_INTRO; ?></td>
                <td align="right"><?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><?php echo TEXT_DEPARTMENT; ?></td>
                <td class="smallText"><?php echo tep_draw_pull_down_menu('department', $departments_array, $entry['department_id']); ?></td>
                <td class="smallText"><?php echo TEXT_STATUS; ?></td>
                <td class="smallText"><?php echo tep_draw_pull_down_menu('status', $statuses_array, $entry['status_id']); ?></td>
                <td class="smallText"><?php echo TEXT_PRIORITY; ?></td>
                <td class="smallText"><?php echo tep_draw_pull_down_menu('priority', $priorities_array, $entry['priority_id']); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><?php echo TEXT_TICKET_NUMBER; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('ticket', $entry['ticket'], 'maxlength="7"'); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_FROM_NAME; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('from_name', $entry['sender']); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_FROM_EMAIL_ADDRESS; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('from_email_address', $entry['email_address']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_TO_NAME; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('to_name', $entry['receiver']); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_TO_EMAIL_ADDRESS; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('to_email_address', $entry['receiver_email_address']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_SUBJECT; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_input_field('subject', $entry['subject']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText" valign="top"><?php echo TEXT_BODY; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo tep_draw_textarea_field('body', 'virtual', '60', '10', $entry['body'], 'style="width: 100%"'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></form></td>
      </tr>
<?php
    } elseif ($subaction == 'delete') {
?>
      <tr>
        <td><?php echo tep_draw_form('delete_single', basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&id=' . $_GET['id'] . '&action=delete_confirm_single'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2" class="columnLeft">
          <tr>
            <td class="smallText"><?php echo TEXT_DELETE_INTRO; ?></td>
            <td class="smallText"><?php echo tep_draw_checkbox_field('whole', 'true') . '&nbsp;' . TEXT_DELETE_WHOLE_THREAD; ?></td>
            <td align="right"><?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
    } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=reply') . '">' . tep_image_button('button_reply.gif', IMAGE_REPLY) . '</a>&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a>&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>'; ?></td>
            <td align="right"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('ticket', basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=updatestatus'); ?><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo TEXT_DEPARTMENT; ?></td>
            <td class="smallText"><?php echo tep_draw_pull_down_menu('department', $departments_array, $entry['department_id']); ?></td>
            <td class="smallText"><?php echo TEXT_STATUS; ?></td>
            <td class="smallText"><?php echo tep_draw_pull_down_menu('status', $statuses_array, $entry['status_id']); ?></td>
            <td class="smallText"><?php echo TEXT_PRIORITY; ?></td>
            <td class="smallText"><?php echo tep_draw_pull_down_menu('priority', $priorities_array, $entry['priority_id']); ?></td>
            <td class="smallText"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
    }
    if( $subaction != 'delete' ) {
?>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><b><?php echo $entry['subject']; ?></b></td>
                <td class="smallText" align="right"><b><?php echo '[' . $entry['ticket'] . ']'; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '2'); ?></td>
          </tr>
          <tr>
            <td class="formarea"><table border="0" cellspacing="1" cellpadding="3">
              <tr>
                <td class="smallText"><?php echo TEXT_TO; ?></td>
                <td class="smallText"><?php echo $entry['receiver'] . ' (' . $entry['receiver_email_address'] . ')'; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_FROM; ?></td>
                <td class="smallText"><?php echo $entry['sender'] . ' (' . $entry['email_address'] . ') (' . $entry['host'] . ')'; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_IP; ?></td>
                <td class="smallText"><?php echo $entry['ip_address']; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_DATE; ?></td>
                <td class="smallText"><?php echo $entry['datestamp_local'] . ' (' . TEXT_REMOTE . ' ' . $entry['datestamp'] . ')'; ?></td>
              </tr>
<?php
      $attachments_query = tep_db_query("select attachment from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($ticket) . "'");
      if( tep_db_num_rows($attachments_query) ) {
?>
              <tr>
                <td class="smallText"><?php echo TEXT_ATTACHMENTS; ?></td>
                <td class="smallText">
<?php
        while($attachments_array = tep_db_fetch_array($attachments_query) ) {
          echo '<a href="' . HTTP_SERVER . DIR_WS_ADMIN . HELPDESK_ATTACHMENTS_FOLDER . basename($attachments_array['attachment']) . '"><b>' . basename($attachments_array['attachment']) . '</b></a><br/ >';
        }
?>
              </tr>
<?php
      }
?>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="smallText"><b><?php echo TEXT_MESSAGE; ?></b></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '2'); ?></td>
          </tr>
          <tr>
            <td class="formarea"><table border="0" cellspacing="1" cellpadding="3">
              <tr>
                <td class="smallText"><?php echo nl2br($entry['body']); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=reply') . '">' . tep_image_button('button_reply.gif', IMAGE_REPLY) . '</a>&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a>&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=view&id=' . $entry['helpdesk_entries_id'] . '&subaction=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>'; ?></td>
            <td align="right"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
<?php
      $threads_query = tep_db_query("select helpdesk_entries_id, ticket, subject, sender, datestamp_local, datestamp, entry_read from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "' order by datestamp_local");
      if (tep_db_num_rows($threads_query) > 1) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SUBJECT; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SENDER; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE; ?></td>
          </tr>
<?php
        while ($threads = tep_db_fetch_array($threads_query)) {
          if ($entry['helpdesk_entries_id'] == $threads['helpdesk_entries_id']) {
            echo '                  <tr class="dataTableRowSelected">' . "\n";
          } else {
            echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $threads['ticket'] . '&id=' . $threads['helpdesk_entries_id'] . '&action=view') . '\'">' . "\n";
          }

          $entry_icon = (($threads['entry_read'] != '1') ? tep_image(DIR_WS_ICONS . 'unread.gif', ICON_UNREAD) : tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW));
?>
            <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $threads['ticket'] . '&id=' . $threads['helpdesk_entries_id'] . '&action=view') . '">' . $entry_icon . '</a>&nbsp;' . $threads['subject']; ?></td>
            <td class="dataTableContent"><?php echo $threads['sender']; ?></td>
            <td class="dataTableContent" align="center"><?php echo $threads['datestamp_local']; ?></td>
          </tr>
<?php
        }
?>
        </table></td>
      </tr>
<?php
      }

      if( empty($subaction) ) {
        $internal_query = tep_db_query("select comment, datestamp_comment from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        $internal = tep_db_fetch_array($internal_query);
?>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <?php echo tep_draw_form('internal', basename($PHP_SELF), 'page=' . $page . '&ticket=' . $_GET['ticket'] . '&action=updatecomment'); ?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2" class="columnLeft">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText"><b><?php echo TEXT_INTERNAL_COMMENTS; ?></b></td>
<?php
        if (tep_not_null($internal['datestamp_comment'])) {
          echo '              <td class="smallText" align="right"><b>' . TEXT_LAST_UPDATE . '</b> ' . $internal['datestamp_comment'] . '</td>' . "\n";
        }
?>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo tep_draw_textarea_field('comment', 'virtual', '60', '10', $internal['comment'], 'style="width: 100%"'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td align="right"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
      </tr>
      </form>
<?php
      }
    }
  } elseif($action == 'delete') {
?>
      <tr>
        <td><?php echo tep_draw_form('delete', basename($PHP_SELF), tep_get_all_get_params(array('action','subaction','ticket')) . 'action=delete_confirm'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TICKET; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SENDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_POST; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRIORITY; ?>&nbsp;</td>
              </tr>

<?php
      foreach($_POST['ticket'] as $key => $value) {
?>
              <tr class="dataTableRow">
                <td class="dataTableContent"><?php echo $key . tep_draw_hidden_field('ticket[' . $key . ']', $key); ?></td>
                <td class="dataTableContent"><?php echo $_POST['sender'][$key]; ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_date_short($_POST['datestamp_last_entry'][$key]); ?></td>
                <td class="dataTableContent" align="right"><?php echo $_POST['status'][$key]; ?></td>
                <td class="dataTableContent" align="right"><?php echo $_POST['priority'][$key]; ?></td>
              </tr>
<?php
      }
?>
            </table></td>
          </tr>
          <tr>
            <td align="right"><?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action','subaction','ticket')) ) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><?php echo tep_draw_form('cross', basename($PHP_SELF), tep_get_all_get_params(array('action','subaction')) . 'action=delete', 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr>
                <td colspan="9"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td><?php echo tep_image_submit('button_delete.gif', IMAGE_DELETE) . '</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="check_boxes(document.cross)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TICKET; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SUBJECT; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SENDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_POST; ?></td>
                <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_PRIORITY; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_IP; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ATTACHMENTS; ?>&nbsp;</td>
              </tr>
<?php
    if( !empty($entry_filter) ) {
      $unread_array = array();
      $unread_entries_query = tep_db_query("select distinct he.ticket from " . TABLE_HELPDESK_ENTRIES . " he, " . TABLE_HELPDESK_TICKETS . " ht where he.entry_read = '0' and he.ticket = ht.ticket order by ht.datestamp_last_entry desc limit " . MAX_DISPLAY_ADMIN_HELP_DESK);
      while ($unread_entries = tep_db_fetch_array($unread_entries_query)) {
        $unread_array[] = $unread_entries['ticket'];
      }
    }

    $entries_query_raw = "select ht.ticket, hd.email_address, ht.datestamp_last_entry, hs.title as status, hp.title as priority from " . TABLE_HELPDESK_TICKETS . " ht, " . TABLE_HELPDESK_STATUSES . " hs, " . TABLE_HELPDESK_PRIORITIES . " hp, " . TABLE_HELPDESK_DEPARTMENTS . " hd where ht.status_id = hs.status_id and ht.priority_id = hp.priority_id and ht.department_id = hd.department_id";
    if( !empty($department_filter) ) {
      $entries_query_raw .= " and ht.department_id = '" . tep_db_input($department_filter) . "'";
    }
    if( !empty($status_filter) ) {
      $entries_query_raw .= " and ht.status_id = '" . tep_db_input($status_filter) . "'";
    }
    if( !empty($priority_filter) ) {
      $entries_query_raw .= " and ht.priority_id = '" . tep_db_input($priority_filter) . "'";
    }
    if( !empty($entry_filter) ) {
      $entries_query_raw .= " and ht.ticket in ('" . implode("', '", $unread_array) . "')";
    }
    $entries_query_raw .= " order by ht.datestamp_last_entry desc";
    $entries_split = new splitPageResults($page, MAX_DISPLAY_ADMIN_HELP_DESK, $entries_query_raw, $entries_query_numrows);
    $entries_query = tep_db_query($entries_query_raw);
    while ($entries = tep_db_fetch_array($entries_query)) {
      $ticket_query = tep_db_query("select helpdesk_entries_id, sender, subject, ip_address from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . $entries['ticket'] . "' and parent_id = '0'");
      $ticket_array = tep_db_fetch_array($ticket_query);
        if( isset($_GET['keyword']) ) {
          if (!preg_match("/{$_GET['keyword']}/i", $ticket_array['sender'] . $ticket_array['subject'])) {
            continue;
          }
        }

      $postings_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . $entries['ticket'] . "' and helpdesk_entries_id != '" . $ticket_array['helpdesk_entries_id'] . "'");
      $postings = tep_db_fetch_array($postings_query);

      $unread_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . $entries['ticket'] . "' and entry_read = '0'");
      $unread = tep_db_fetch_array($unread_query);

      $last_post_query = tep_db_query("select email_address from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . $entries['ticket'] . "' order by datestamp_local desc limit 1");
      $last_post = tep_db_fetch_array($last_post_query);
      if( (empty($ticket) || $ticket == $entries['ticket']) && !isset($tInfo) && substr($action, 0, 3) != 'new') {
        $tInfo = new objectInfo(array_merge($entries, $ticket_array));
      }

/*
      if ( (is_object($tInfo)) && ($entries['ticket'] == $tInfo->ticket) ) {
        echo '                  <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $tInfo->ticket . '&action=view') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $entries['ticket']) . '\'">' . "\n";
      }
*/
      if( isset($tInfo) && is_object($tInfo) && ($entries['ticket'] == $tInfo->ticket) ) {
        echo '                  <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'">' . "\n";
      }

      $entry_icon = (($unread['count'] > 0) ? tep_image(DIR_WS_ICONS . 'unread.gif', ICON_UNREAD) : tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW));

      if ($entries['email_address'] == $last_post['email_address']) {
        $entry_icon .= tep_image(DIR_WS_ICONS . 'outgoing.gif', ICON_OUTGOING);
      } else {
        $entry_icon .= tep_image(DIR_WS_ICONS . 'incoming.gif', ICON_INCOMING);
      }
?>
                <td class="dataTableContent" width="56">
<?php 
      echo tep_draw_checkbox_field('ticket[' . $entries['ticket'] . ']', '', false) . '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $entries['ticket'] . '&action=view') . '">' . $entry_icon . '</a>&nbsp;' . tep_draw_hidden_field('sender[' . $entries['ticket'] . ']', $ticket_array['sender']) . tep_draw_hidden_field('datestamp_last_entry[' . $entries['ticket'] . ']', $entries['datestamp_last_entry']) . tep_draw_hidden_field('status[' . $entries['ticket'] . ']', $entries['status']) . tep_draw_hidden_field('priority[' . $entries['ticket'] . ']', $entries['priority']);
/*
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), 'page=' . $page . '&ticket=' . $entries['ticket'] . '&action=view') . '">' . $entry_icon . '</a>&nbsp;' . $entries['ticket']; ?></td>
*/
?>
                </td>
                <td class="dataTableContent"><?php echo $entries['ticket']; ?></td>
                <td class="dataTableContent"><?php echo $ticket_array['subject'] . ' (' . $postings['count'] . ')'; ?></td>
                <td class="dataTableContent"><?php echo $ticket_array['sender']; ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_date_short($entries['datestamp_last_entry']); ?></td>
                <td class="dataTableContent"><?php echo $entries['status']; ?></td>
                <td class="dataTableContent"><?php echo $entries['priority']; ?></td>
                <td class="dataTableContent" align="center"><?php echo $ticket_array['ip_address']; ?></td>
                <td class="dataTableContent" align="center">
<?php 
      $attachments_query = tep_db_query("select count(*) as total from " . TABLE_HELPDESK_ATTACHMENTS . " where ticket = '" . tep_db_input($entries['ticket']) . "'");
      $attachments_array = tep_db_fetch_array($attachments_query);
      echo $attachments_array['total'];
?>
                </td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="9"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td><?php echo tep_image_submit('button_delete.gif', IMAGE_DELETE) . '</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" valign="top"><?php echo $entries_split->display_count($entries_query_numrows, MAX_DISPLAY_ADMIN_HELP_DESK, $page, TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText" align="right"><?php echo $entries_split->display_links($entries_query_numrows, MAX_DISPLAY_ADMIN_HELP_DESK, MAX_DISPLAY_PAGE_LINKS, $page); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
