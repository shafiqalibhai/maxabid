<?php
/*
  $Id: checkout_payment.php,v 1.113 2003/06/29 23:03:27 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Checkout Payment page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
                                 
// if the customer is not logged on, redirect them to the login page
  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }  

// if no shipping method has been selected, redirect the customer to the shipping method selection page
  if (!tep_session_is_registered('shipping')) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  }

// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
    if ($cart->cartID != $cartID) {
      tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
    }
  }

// Stock Check
  if ( (STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true') ) {
    $products = $cart->get_products();
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
      if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
        tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
        break;
      }
    }
  }             
         
  // if no billing destination address was selected, use the customers own address as default
  if (!tep_session_is_registered('billto')) {
    tep_session_register('billto');
    $billto = $customer_default_address_id;
  } else {
    // verify the selected billing address
    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$billto . "'");
    $check_address = tep_db_fetch_array($check_address_query);

    if ($check_address['total'] != '1') {
      $billto = $customer_default_address_id;
      if (tep_session_is_registered('payment')) tep_session_unregister('payment');
    }
  }
        
  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

  if (!tep_session_is_registered('comments')) tep_session_register('comments');

  $total_weight = $cart->show_weight();
  $total_count = $cart->count_contents();

// load all enabled payment modules
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment;
                   
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PAYMENT);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_payment.payment[0]) {
    document.checkout_payment.payment[buttonSelect].checked=true;
  } else {
    document.checkout_payment.payment.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}
//--></script>
<?php echo $payment_modules->javascript_validation(); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  //$body_form = tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL'), 'post', 'onsubmit="return check_form();"');
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
<?php
  if (isset($_GET['payment_error']) && is_object(${$_GET['payment_error']}) && ($error = ${$_GET['payment_error']}->get_error())) {
    if( !isset($error['title']) ) {
      $error['title'] = '';
    }
?>
      <div class="bounder script_error">
        <div class="heavy pad"><?php echo tep_output_string_protected($error['title']); ?></div>
        <div class="hpad bpad"><?php echo tep_output_string_protected($error['error']); ?></div>
      </div>
<?php
  }
?>
      <div><h1><?php echo HEADING_TITLE; ?></h1></div> 
      <div class="heavy"><?php echo TABLE_HEADING_BILLING_ADDRESS; ?></div>
      <div class="bounder">  
        <div class="floater hpad">
          <div class="bounder">
            <div class="vpad"><?php echo TEXT_SELECTED_BILLING_DESTINATION; ?></div>
            <div><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL') . '" class="tbutton2">' . IMAGE_BUTTON_CHANGE_ADDRESS . '</a>'; ?></div>
          </div>
        </div>
        <div class="floatend hpad">
          <div class="bounder">
            <div class="heavy floater calign"><?php echo TITLE_BILLING_ADDRESS . '<br />' . tep_image(DIR_WS_IMAGES . 'arrow_south_east.gif'); ?></div>
            <div class="floater hpad"><?php echo tep_address_label($customer_id, $billto, true, ' ', '<br>'); ?></div>
          </div>
        </div>
      </div>
      <div class="bounder tspacer"><?php echo tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_CONFIRMATION,  'action=process', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="bounder"> 
<?php
  $selection = $payment_modules->selection();
  if (sizeof($selection) > 1) {
?>
          <div class="floater heavy quarter3"><?php echo TEXT_SELECT_PAYMENT_METHOD; ?></div>
          <div class="floatend quarter calign"><?php echo '<b>' . TITLE_PLEASE_SELECT . '</b><br />' . tep_image(DIR_WS_IMAGES . 'arrow_east_south.gif'); ?></div>
<?php
  } else {
    echo TEXT_ENTER_PAYMENT_INFORMATION;
  }
  $radio_buttons = 0;
  for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
?>

<?php
    if ( ($selection[$i]['id'] == $payment) || ($n == 1) ) {
      $selected_class = 'moduleRowSelected';
    } else {
      $selected_class = 'moduleRow';
    }
?>
          <div class="bounder <?php echo $selected_class; ?>">
<?php
    if (sizeof($selection) > 1) {
      $selected_radio = tep_draw_radio_field('payment', $selection[$i]['id']);
    } else {
      $selected_radio = tep_draw_hidden_field('payment', $selection[$i]['id']);
    }
    if (isset($selection[$i]['error'])) {
?>
            <div class="floater heavy quarter3"><div class="hpad"><?php echo $selection[$i]['module']; ?></div></div>
            <div class="floatend quarter calign">
              <div class="bounder"><?php echo $selection[$i]['error']; ?></div>
            </div>
<?php
    } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
?>
            <div class="floater heavy quarter3"><div class="hpad"><?php echo $selection[$i]['module']; ?></div></div>
            <div class="floatend quarter calign"><?php echo $selected_radio; ?></div>
            <div class="bounder"><div class="quarter3" style="padding-left: 32px;">
<?php
      for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
?>
                <div class="floater halfer"><?php echo $selection[$i]['fields'][$j]['title']; ?></div>
                <div class="floater halfer"><?php echo $selection[$i]['fields'][$j]['field']; ?></div>
<?php
      }
?>
            </div></div>
<?php
    } else {
?>
            <div class="floater heavy quarter3"><div class="hpad"><?php echo $selection[$i]['module']; ?></div></div>
            <div class="floatend quarter calign"><?php echo $selected_radio; ?></div>

<?php
    }
?>
          </div>
<?php
    $radio_buttons++;
  }
?>               
        </div>
        <div class="heavy vspacer"><?php echo TABLE_HEADING_COMMENTS; ?></div>
        <div><?php echo tep_draw_textarea_field('comments', 'soft', '60', '5'); ?></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><div class="bounder">
            <div class="heavy"><?php echo TITLE_CONTINUE_CHECKOUT_PROCEDURE; ?></div>
            <div><?php echo TEXT_CONTINUE_CHECKOUT_PROCEDURE; ?></div>
          </div></div>
          <div class="floatend rspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>

      </fieldset></form></div>
      <div class="bounder vspacer"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%" align="right"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            </tr>
          </table></td>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              <td><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            </tr>
          </table></td>
          <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="center" width="25%" class="checkoutBarFrom"><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '" class="checkoutBarFrom">' . CHECKOUT_BAR_DELIVERY . '</a>'; ?></td>
          <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
        </tr>
      </table></div>
<?php require('includes/objects/html_end.php'); ?>   
                    