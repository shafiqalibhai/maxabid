<?php
/*
  $Id: password_forgotten.php,v 1.50 2003/06/05 23:28:24 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Password Forgotten page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PASSWORD_FORGOTTEN);

  if (isset($_GET['action']) && ($_GET['action'] == 'process')) {
    $email_address = tep_db_prepare_input($_POST['email_address']);

    $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
    if (tep_db_num_rows($check_customer_query)) {
      $check_customer = tep_db_fetch_array($check_customer_query);

      $new_password = tep_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
      $crypted_password = tep_encrypt_password($new_password);
      tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_db_input($crypted_password) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");

//-MS- Email Templates added
      $name = $check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'];
      $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='password_forgotten'");
      if( $email_array = tep_db_fetch_array($mail_query) ) {
        $right_col = tep_email_column(DEFAULT_EMAIL_ABSTRACT_ZONE_ID);
        $temp_array = array(
                            'ACCOUNT_STATUS' => 'Customer',
                            'CUSTOMER_NAME' => $name,
                            'CUSTOMER_EMAIL' => $email_address,
                            'CUSTOMER_PASSWORD' => $new_password,
                            'STORE_NAME' => STORE_NAME,
                            'STORE_EMAIL_ADDRESS' => STORE_OWNER_EMAIL_ADDRESS,
                            'RIGHT_COL' => $right_col
                           );
        $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
        $email_array['subject'] = tep_email_templates_replace_entities($email_array['subject'], $temp_array);
        tep_mail($name, $email_address, $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
      } else {
//-MS- Email Templates added EOM
        tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, EMAIL_PASSWORD_REMINDER_SUBJECT, sprintf(EMAIL_PASSWORD_REMINDER_BODY, $new_password), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
      }

      $messageStack->add_session('login', SUCCESS_PASSWORD_SENT, 'success');

      tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
    } else {
      $messageStack->add('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
    }
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $body_form = tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL'));
  require('includes/objects/html_body_header.php');
?>
      <div style="width:60%; float:left;"><?php echo TEXT_MAIN; ?></div>
      <div class="bounder tspacer" style="width:60%; float:left;"><?php echo tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL')); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="halfer highter hider" style="width:100%">
          <div class="heavy floater halfer"><?php echo ENTRY_EMAIL_ADDRESS; ?></div>
          <div class="floater halfer"><?php echo tep_draw_input_field('email_address'); ?></div>
        </div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_BACK . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
