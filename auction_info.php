<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Information Page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . $g_script);

  $auctions_id = isset($_GET['auctions_id'])?(int)$_GET['auctions_id']:0;
  $check_query = tep_db_query("select * from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
  if( !tep_db_num_rows($check_query) ) {
    $messageStack->add_session(tep_get_script_name(), ERROR_AUCTION_INVALID);
    tep_redirect(tep_href_link(FILENAME_AUCTION_GROUPS, '', 'NONSSL'));
  }
  $auction_info = tep_db_fetch_array($check_query);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch($action) {
    default:
      break;
  }

?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div class="bounder">
        <div class="floater" style="width:513px"><h1><?php echo $auction_info['auctions_name']; ?></h1></div>
        
<?php
  $bids_string = '';
  if( tep_session_is_registered('customer_id') ) {
    $customer_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customer_array = tep_db_fetch_array($customer_query);
    $bids_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '"><span class="pad notice">' . sprintf(TEXT_INFO_AUCTION_BIDS_LEFT, '<span id="bids_notice">' . $customer_array['customers_bids'] . '</span>') . '</span></a>';
?>
        <div class="floatend" style="padding-top: 14px; padding-right: 8px; height:25px; margin-bottom: 5px;"><?php echo $bids_string; ?></div>
                
<?php
  }
?>
<div class="floatend" style="padding-top: 14px; padding-right: 8px; height:25px; margin-bottom: 5px;" id="server_time"></div>
      </div>
<?php
  $new_price = '<span class="textPrice">' . TEXT_INFO_PRICE . '</span>&nbsp;<span class="productSpecialPrice">' . $currencies->display_price($auction_info['start_price'], 0) . '</span>';

  $auctions_name = strtoupper($auction_info['auctions_name']);
  $action_params = '&action=add_auction';

  $price_symbol = $currencies->currencies[$currency]['symbol_left'];
?>
        <div class="bounder" id="auction-info">
<?php
  $display_image = '';

  if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
    $display_image = IMAGE_NOT_AVAILABLE;
  } else {
    $display_image = $auction_info['auctions_image'];
  }

  $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
  $tiers_array = tep_db_fetch_array($tiers_query);
  $row_info_class = 'rowInfo1';
  if( $tiers_array['total'] == 1 ) {
    $row_info_class = 'rowInfo2';
  } elseif($tiers_array['total'] > 1) {
    $row_info_class = 'rowInfo3';
  }
  $group_link = tep_href_link(FILENAME_AUCTION_GROUPS, $auction_info['auctions_group_id'], 'NONSSL');
?>
          <div class="floater auction_item hider" style="width: 220px;" id="auction_item_<?php echo $auction_info['auctions_id']; ?>" data-link="<?php echo $group_link; ?>">
            <div class="hpad hider" id="auction_info_block" data-group="$auction_info['auctions_group_id']">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div id="auction_block_<?php echo $auction_info['auctions_id']; ?>" style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; min-height: 270px;" class="auction-block">
<?php
  if( $auction_info['status_id'] != 2 ) {
    $splash_class = tep_auction_get_splash($auction_info);
?>
                <div class="abs <?php echo $splash_class; ?> heavy lalign" style="z-index: 2; left: 3px;" title="<?php echo sprintf(TEXT_INFO_AUCTION_BIDS_STEP_REQUIRED, $auction_info['bid_step'], $auction_info['auctions_name']); ?>"><div class="splash_text"><?php echo $auction_info['bid_step'] ?></div></div>
<?php
  }
?>
                <div class="ralign rpad bspacer <?php echo $row_info_class; ?>" title="<?php echo sprintf(TEXT_INFO_AUCTION_PRIZES_COUNT, $tiers_array['total']+1); ?>"><?php echo '<a href="#" attr="' . $auction_info['auctions_id'] . '" class="auction_details" title="' . sprintf(TEXT_INFO_AUCTION_DETAILS, $auction_info['auctions_name']) . '">' . tep_image(DIR_WS_ICONS . 'icon_info.png', sprintf(TEXT_INFO_AUCTION_DETAILS, $auction_info['auctions_name'])) . '</a>'; ?></div>
                <div style="height: <?php echo SMALL_IMAGE_HEIGHT; ?>px">
<?php
  if( $auction_info['status_id'] != 2 ) {
    $splash_class = tep_auction_get_splash($auction_info);
?>
                  <div id="fancyx"><?php echo '<a href="' . DIR_WS_IMAGES . $display_image . '" class="pg_link" target="_blank">' . tep_image(DIR_WS_IMAGES . $display_image, $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
<?php
  } else {
?>
                  <div class="calign" id="fancyx"><?php echo '<a href="' . DIR_WS_IMAGES . $display_image . '" class="pg_link" target="_blank">' . tep_image(DIR_WS_IMAGES . $display_image, $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
<?php
  }
?>
                </div>
                <div style="height: 39px;">
<?php
    $string_array = array();
    if($tiers_array['total']) {
      $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE, $currencies->format(tep_get_products_price($auction_info['products_id'])) );
      $tiers_array = array();
      $tiers_query_raw = "select products_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "' order by sort_id";
      tep_query_to_array($tiers_query_raw, $tiers_array);
      for($i=0, $j=count($tiers_array); $i<$j; $i++) {
        $index_string = $i+2;
        if( $i+2 == 2 ) {
          $index_string .= 'nd';
        } elseif( $i+2 == 3 ) {
          $index_string .= 'rd';
        } else {
          $index_string .= 'th';
        }
        $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE_TIERS, $index_string, $currencies->format(tep_get_products_price($tiers_array[$i]['products_id'])) );
      }
    } else {
      $string_array[] = sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction_info['products_id'])) );
    }
    echo implode('<br />', $string_array);
?>
                </div>
<?php
    $current_price_array = array();
    $count_query = tep_db_query("select count(*) as total  from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
    $count_array = tep_db_fetch_array($count_query);

      if( $auction_info['status_id'] == 2 ) {
        $history_array = array();
        $history_query_raw = "select auto_id, customers_id, auctions_id, auctions_name, auctions_price, shipping_cost, products_name, started, completed, bid_count, winner_bids, customers_nickname, customer_count from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'";
        tep_query_to_array($history_query_raw, $history_array);
        $auction = $history_array[0];

        if( $auction['customers_id'] ) {
          for( $i=0, $j=count($history_array); $i<$j; $i++) {
            $current_price_array[] = array(
              'customers_id' => $history_array[$i]['customers_id'],
              'auctions_price' => $history_array[$i]['auctions_price'],
              'shipping_cost' => $history_array[$i]['shipping_cost'],
            );
          }
        }
?>
                <div class="vspacer"><?php echo sprintf(TEXT_INFO_AUCTION_ENDED, tep_date_time_short($auction['completed']) ); ?></div>
                <div class="heavy">
<?php 
//        echo sprintf(TEXT_INFO_BIDS_ENTERED, $auction['bid_count']);
?>
                </div>
                <div class="heavy hider">
<?php
        if( tep_session_is_registered('customer_id') ) {
          $winner_flag = false;
          $output = '';
          $output .= '<table class="tabledata" style="margin-bottom: -1px;">';

          $j = count($history_array);
          for($i=0; $i<$j; $i++) {
            $self_bids = '';
            if( $auction['customers_id'] == $customer_id) {
              $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $auction['winner_bids']) . '</span>';
            }

            $history_array[$i]['customers_nickname'] = tep_truncate_string($history_array[$i]['customers_nickname']);

            if( !empty($auction['customers_nickname']) ) {
              $rclass = ($i%2)?'altrow':'oddrow';
              $output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+1) . '.</td><td class="lalign mark_green">' . $history_array[$i]['customers_nickname'] . '</td><td class="lalign"><!--' . $history_array[$i]['winner_bids'] . '--></td></tr>';
              if( $history_array[$i]['customers_id'] == $customer_id) {
                $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $history_array[$i]['winner_bids']) . '</span>';
              }
            }
          }

          $output .= '</table>';

          if( !empty($self_bids) ) {
            echo '<div>' . $self_bids . '</div>';
          }

          if( !empty($auction['customers_nickname']) ) {
            echo '<div class="bspacer">' . sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS, $j) . '</div>';
            echo $output;
          } else {
            echo '<div>' . TEXT_INFO_AUCTION_NO_WINNER . '</div>';
          }

        } else {
          echo sprintf(TEXT_INFO_AUCTION_LOGIN_DETAILS);
        }
?>
                </div>
<?php
      } elseif( $auction_info['status_id'] == 1 ) {
        if( !tep_session_is_registered('customer_id') ) {
          $note = TEXT_INFO_AUCTION_LOGIN_FIRST_2;
        } else {
          //$check_query = tep_db_query("select customers_id, customers_nickname from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
          $check_query = tep_db_query("select customers_id, customers_nickname from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
          $lowest_query = tep_db_query("select customers_id, count(bid_price) as winning from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "' group by bid_price order by winning, bid_price, date_added limit 1");
    $lowest_array = tep_db_fetch_array($lowest_query);

    $current_query2 = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS .  " where customers_id = '" . (int)$lowest_array['customers_id'] . "'");
    
          
          $check_query1 = tep_db_query("select customers_id, customers_nickname from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
          $check_array1 = tep_db_fetch_array($check_query);
          $orders_info_query = tep_db_query("select count(*) as orders from " . TABLE_ORDERS . " where customers_id = '". (int)$lowest_array['customers_id'] . "'");
          $orders_info = tep_db_fetch_array($orders_info_query);
          if( !tep_db_num_rows($check_query) ) {
            $note = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_WAIT_FIRST_BID . '</span>';
          } else {
            $check_array = tep_db_fetch_array($check_query);
            $note = sprintf(tep_auction_get_bidder_string($auction_info), tep_truncate_string($check_array['customers_nickname']));
            $check_array = tep_db_fetch_array($check_query1);
            $current_array2 = tep_db_fetch_array($current_query2);
            $note = sprintf(tep_auction_get_bidder_string($auction), tep_truncate_string($current_array2['customers_nickname']).' ('.$orders_info['orders'].')');
          
          }
        }

        $bids_left = '';
        if( $auction_info['max_bids'] && $auction_info['bids_left'] ) {
          $bids_left = max(0, $auction_info['max_bids'] - $count_array['total']);
          $bids_left = '<span class="heavy mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_REMAIN, $bids_left) . '</span>';
        }
?>
                <div id="bg_flash_<?php echo $auction_info['auctions_id']; ?>">
                
                <?php
                $tiers_query1 = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
          $tiers_array1 = tep_db_fetch_array($tiers_query1);
          
          
          $check_query = tep_db_query("select customers_id, customers_nickname from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
          $check_array1 = tep_db_fetch_array($check_query);
          
          for($i=0; $i<=($tiers_array1['total']); $i++)
          {                       
             $current_query[$i] = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS .  " where customers_id = '" . (int)$check_array1['customers_id'] . "'");
             //$orders_info_query[$i] = tep_db_query("select count(*) as orders from " . TABLE_ORDERS . " where customers_id = '". (int)$check_array1['customers_id'] . "'");
             //$orders_info[$i] = tep_db_fetch_array($orders_info_query[$i]);
             $current_array[$i] = tep_db_fetch_array($current_query[$i]);
          }
          
          for($i=0; $i<=($tiers_array1['total']); $i++) {
                $note[$i] = sprintf(tep_auction_get_bidder_string($auction), tep_truncate_string($current_array[$i]['customers_nickname']).' ('.$orders_info[$i]['orders'].')');
            }
            
                for($i=0; $i<=($tiers_array1['total']); $i++)
                {
                
                if(count($note)>1){
                   //echo '<div id="bid_note" style="height: 14px;">'.$note[$i].'</div>';
                }
                else {
                   //echo '<div id="bid_note" style="height: 14px;">'.$note[0].'</div>';
                }
                   
                }
                
                ?>
                  <!-- <div id="bid_note<?php echo $auction_info['auctions_id']; ?>" style="height: 14px"><?php echo $note[0]; ?></div>-->
                  <!-- <div id="bid_left<?php echo $auction_info['auctions_id']; ?>" style="height: 14px"><?php echo $bids_left; ?></div>  -->
<?php
        $bids_query = tep_db_query("select customers_id, customers_nickname, bid_price, last_modified, signature from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
        if( !tep_db_num_rows($bids_query) ) {

          $auction_price = tep_round($auction_info['start_price'], 2);

          $bids_array = array(
            'customers_id' => 0,
            'customers_nickname' => '',
            'bid_price' => $auction_price,
            'last_modified' => date('Y-m-d H:i:s'),
          );

          if( $auction_info['auctions_type'] == 1 ) {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s'));
          } else {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s') . $auction_price);
          }

        } else {
          $bids_array = tep_db_fetch_array($bids_query);
        }
      }

      if( $auction_info['status_id'] < 2 ) {
?>
                  <div class="vpad heavy" id="bid_note2_<?php echo $auction_info['auctions_id']; ?>" style="height: 32px; line-height: 18px;">
<?php
        $start_time = tep_auction_check_start($auction_info);
        $end_time = tep_auction_check_end($auction_info);
        $index_array = tep_get_countdown_bids_index($auction_info, $count_array['total']);

        if( $index_array['countdown_bids'] ) {
          $timer_string = $index_array['countdown_timer'];
          $x_bid_timer = 'First Bid ';
          if($index_array['countdown_bids'] > 1) {
            $x_bid_timer = 'X Bid ';
          }
          echo $x_bid_timer . sprintf(TEXT_INFO_AUCTION_TIMER_PRESENT, '<br /><span class="font_large nobg" style="color: #000;">' . $timer_string . '</span>');
        } elseif( !empty($start_time) ) {
          $tmp_array = explode(' ', tep_date_time_short($start_time));
          $starting_date = sprintf(TEXT_INFO_AUCTION_STARTS, ' ' . $tmp_array[0] . '<br /><span class="font_large" style="color: #000">' . $tmp_array[1] . '</span>');
          echo $starting_date;
        } elseif( !empty($end_time) ) {
          $tmp_array = explode(' ', tep_date_time_short($end_time));
          $ending_date = sprintf(TEXT_INFO_AUCTION_ENDS, ' ' . $tmp_array[0] . '<br /><span class="font_large" style="color: #000">' . $tmp_array[1] . '</span>');
          echo $ending_date;
        } else {
          echo '<img src="' . DIR_WS_IMAGES . 'no_countdown_timer.png' . '" border="0" class="nobg" />';
        }
?>
                  </div>
                </div>
<?php
      }
      if( $auction_info['status_id'] < 2 ) {
        $symbol = $currencies->currencies[DEFAULT_CURRENCY]['symbol_left'];
        
        
        
        
        if( $auction_info['auctions_type'] == 1 ) {
          $current_price_array[] = array(
            'customers_id' => 0,
            'auctions_price' => $auction_info['start_price'],
            'shipping_cost' => $auction_info['shipping_cost'],
          );
          $input_line = TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;' . $symbol . tep_draw_input_field('auction_bid[' . $auction_info['auctions_id'] . ']', '0.00', 'size="4" class="bid_inputs" id="auction_input_' . $auction_info['auctions_id'] . '"');
        } elseif($auction_info['auctions_type'] == 2) {
          $current_price_array[] = array(
            'customers_id' => 0,
            'auctions_price' => $bids_array['bid_price'],
            'shipping_cost' => $auction_info['shipping_cost'],
          );
          $input_line = TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;' . $symbol . tep_draw_input_field('auction_bid[' . $auction_info['auctions_id'] . ']', tep_round($bids_array['bid_price']+(1/100),2), 'size="4" class="bid_inputs" id="auction_input_' . $auction_info['auctions_id'] . '"');
        } else {
          $current_price_array[] = array(
            'customers_id' => 0,
            'auctions_price' => $bids_array['bid_price'],
            'shipping_cost' => $auction_info['shipping_cost'],
          );
          
          
          
          $input_line = TEXT_INFO_AUCTION_CURRENT_VALUE . '&nbsp;<span id="auction_display_' . $auction_info['auctions_id'] . '">' . tep_round($bids_array['bid_price']+0.01,2) . '</span>';
          //$input_line = tep_draw_input_field('auction_bid[' . $auction_info['auctions_id'] . ']', tep_round($bids_array['bid_price']+(1/100),2), 'size="4" class="bid_inputs" id="auction_input_' . $auction_info['auctions_id'] . '"');
        }

        if( $auction_info['status_id'] == 1 ) {
?>
                  <div class="heavy" id="auction_entry_bid_<?php echo $auction_info['auctions_id']; ?>" style="height: 20px; line-height: 20px;"><?php echo $input_line; ?></div>
                  <div class="heavy" style="padding: 4px 0px;">
<?php
          echo tep_draw_hidden_field('hidden_bid[' . $auction_info['auctions_id'] . ']', $bids_array['signature'], 'id="backup_bid_' . $auction_info['auctions_id'] . '"');
          if( !tep_session_is_registered('customer_id') ) {
            echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" attr="' . $auction_info['auctions_id'] . '" class="sticky_bid tbutton" title="' . TEXT_INFO_AUCTION_LOGIN_FIRST . '">' . TEXT_INFO_AUCTION_LOGIN_FIRST . '</a>';
          } else {
            echo '<a href="#" attr="' . $auction_info['auctions_id'] . '" class="sticky_bid tbutton"  style="padding-left: 20px; padding-right: 20px;" title="' . sprintf(TEXT_INFO_AUCTION_PLACE_BID, $auction_info['auctions_name']) . '">' . 'Submit Bid Now' . '</a>'; 
          }
?>
                  </div>
<?php
        }
      }
?>
                </div>

                <?php
                 if( $auction_info['status_id'] == 0) {
                ?>
              <div class="b2" style="margin-left: 16px; margin-right: 16px;"></div>

              <div class="b2" style="margin-left: 17px; margin-right: 17px;"></div>

              <div class="b1" style="margin-left: 19px; margin-right: 19px;"></div>

              <div class="b1" style="margin-left: 20px; margin-right: 20px;"></div>

              <?php
                } else {
                ?>
                              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>

              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>

              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>

              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>


                
              <?php
                
                }
              ?>
              
              <div class="bounder lalign" id='latest_update'>
              <div class="cleaner bspacer"></div>
              
              
              <?php               
              $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
              $tiers_array = tep_db_fetch_array($tiers_query);
              if($tiers_array['total'] > 0)  {
                 $tiers_array1 = array();
                 $tiers_query_raw1 = "select products_id, shipping_cost from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "' order by sort_id";
                 tep_query_to_array($tiers_query_raw1, $tiers_array1);
                 //$auction_info['products_id'] first item
                 //var_dump($tiers_array1);  //second and third item
                 
              }
              
              for($i=0; $i<=$tiers_array['total']; $i++)
              {              
              ?>
              <table class="tabledata">
                <tr>
                  <th><?php echo '(Item #'.($i+1).')'; ?></th>
                  <th class="ralign"><?php echo TEXT_INFO_COST; ?></th>
                </tr>
                
                <tr>
                  <td><?php echo ($auction_info['status_id'] == 2?TEXT_INFO_LAST_BID_ENDED:TEXT_INFO_LAST_BID_ONLY); ?></td>
                  <td class="ralign"><?php 
                     echo $currencies->format($current_price_array[0]['auctions_price']-($i/100));                   
                   ?>
                  </td>
                </tr>
                <tr>
                  <td>					<?php echo TEXT_INFO_SHIPPING_COST; ?>				  </td>
                  <td class="ralign bold red">				  <?php echo TEXT_INFO_SHIPPING_TEXT; ?>				  </td>
                </tr>                <tr>
                  <td>					<?php echo TEXT_INFO_ADMIN_COST; ?>				  </td>
                  <td class="ralign"><?php 
                  if($i == 0) {
                  echo $currencies->format($current_price_array[0]['shipping_cost']);
                  }else {
                      echo $currencies->format($tiers_array1[$i-1]['shipping_cost']);
                  }
                   ?></td>
                </tr>
                <tr>
                  <td class="heavy"><?php echo TEXT_INFO_TOTAL_COST; ?></td>
                  <td class="heavy ralign"><?php 
                  if($i == 0) {
                  echo $currencies->format(($current_price_array[0]['auctions_price']-($i/100))+$current_price_array[0]['shipping_cost']); 
                  }
                  else {
                  echo $currencies->format(($current_price_array[0]['auctions_price']-($i/100))+$tiers_array1[$i-1]['shipping_cost']);
                  }
                  ?></td>
                </tr> 
                 
              </table>
              

            
             <br>
<?php 
}
?>              
              <?php echo TEXT_INFO_TOTAL_COST_STAR; ?>
                             <?php         
              if( $auction_info['status_id'] != 0 ) {
                echo '</div>';
              }
            ?>
              </div>
            </div>
           
          <div class="extend"></div>

          <div class="floater" style="width: 670px; padding-left: 16px;">
            <div class="bounder">
<?php
      if( $auction_info['status_id'] < 2 ) {
        $products_name = tep_get_products_name($auction_info['products_id']);
        $row_class = 'oddrow';
        $tiers_array1 = array();
        $tiers_query1 = "select products_image from " . TABLE_PRODUCTS . " where products_id = '" . $auction_info['products_id'] . "'";
        tep_query_to_array($tiers_query1, $tiers_array1);
?>
              <div class="heavy pad dtrans" style="font-size: 20px"><?php echo TEXT_INFO_AUCTION_PRIZES; ?></div>
              <div style="">
                <div class="heavy <?php echo $row_class; ?> vpad lpad auction_item" style="text-align:left;"><?php echo '1)&nbsp;&nbsp;&nbsp;' . '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction_info['products_id']) . '">' . $products_name . '</a>'; ?></div>
                <div class="bounder vpad <?php echo $row_class; ?>">    
                  <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction_info['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $tiers_array1[0]['products_image'], $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; 
                  echo '<br>'.sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction_info['products_id'])) );
                  ?></div>
                  <div><?php echo $auction_info['auctions_description'];
                   ?></div>
                </div>
              </div>
<?php
        $tiers_array = array();
        $tiers_query = "select products_id, auctions_name, auctions_image, auctions_description from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . $auction_info['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query, $tiers_array);
        for($i=0, $j=count($tiers_array); $i<$j; $i++) {
          $row_class = ($i%2)?'oddrow':'altrow';
          $products_name = tep_get_products_name($tiers_array[$i]['products_id']);
          if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
            $tier_image = IMAGE_NOT_AVAILABLE;
          } else {
            $tier_image = $tiers_array[$i]['auctions_image'];
          }         

?>
              <div style="">
                <div class="heavy <?php echo $row_class; ?> vpad lpad auction_item" style="text-align:left;"><?php echo ($i+2) . ')&nbsp;&nbsp;&nbsp;' . '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tiers_array[$i]['products_id']) . '">' . $products_name . '</a>'; ?></div>
                <div class="bounder vpad <?php echo $row_class; ?>">
                  <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tiers_array[$i]['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $tier_image, $tiers_array[$i]['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>';
                  echo '<br>'.sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction_info['products_id'])) );
                   ?></div>
                  <div><?php echo $tiers_array[$i]['auctions_description']; ?></div>
                </div>
              </div>
<?php
        }
      } elseif( $auction_info['status_id'] == 2 ) {
        $history_array = array();
        $history_query = "select customers_id, customers_nickname, products_id, products_name, auctions_name, winner_bids, bid_count, started, completed from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'";
        tep_query_to_array($history_query, $history_array);
?>
              <div class="heavy pad dtrans" style="font-size: 20px"><?php echo TEXT_INFO_AUCTION_PRIZES_WON; ?></div>
<?php
        $tmp_array = array(
          'auctions_image' => $auction_info['auctions_image'],
          'auctions_description' => $auction_info['auctions_description'],
        );
        $tiers_array = array();
        $tiers_query_raw = "select auctions_image, auctions_description from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query_raw, $tiers_array);
        array_unshift($tiers_array, $tmp_array);

        for($i=0, $j=count($history_array); $i<$j; $i++) {
          $description = $auction_info['auctions_description'];

          $row_class = ($i%2)?'oddrow':'altrow';
          if( $i ) {
            if( !tep_not_null($tiers_array[$i]['auctions_image']) || !file_exists(DIR_WS_IMAGES . $tiers_array[$i]['auctions_image']) || filesize(DIR_WS_IMAGES . $tiers_array[$i]['auctions_image']) <= 0 ) {
              $auction_image = IMAGE_NOT_AVAILABLE;
            } else {
              $auction_image = $tiers_array[$i]['auctions_image'];
            }
            $description = $tiers_array[$i]['auctions_description'];
          } else {
            if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
              $auction_image = IMAGE_NOT_AVAILABLE;
            } else {
              $auction_image = $auction_info['auctions_image'];
            }
          }
?>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="lpad heighter">
                  <div class="heavy floater halfer auction_item"><?php echo '#'.($i+1) . ': ' . $history_array[$i]['customers_nickname']; ?></div>
                  <div class="heavy floater halfer auction_item"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $history_array[$i]['products_id']) . '">' . $history_array[$i]['products_name'] . '</a>'; ?></div>
                </div>
              </div>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $history_array[$i]['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction_image, $history_array[$i]['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>';
                echo '<br>'.sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction_info['products_id'])) );
                
                 ?></div>
                <div><?php echo $description; ?></div>
              </div>
<?php
        }
      }
?>
            </div>
          </div>
        </div>
<?php require('includes/objects/html_end.php'); ?>
