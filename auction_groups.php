<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Groups Page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . $g_script);

  //tep_db_query("delete from " . TABLE_AUCTIONS_CUSTOMER . " where status_id != '1' and (unix_timestamp(now()) - unix_timestamp(date_added)) > " . 86400);

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';

  $auctions_group_id = isset($_GET['auctions_group_id'])?(int)$_GET['auctions_group_id']:0;

  $auctions_group_array = array(
    'auctions_group_id' => 0,
    'auctions_group_name' => TEXT_INFO_GROUP_DEFAULT_NAME,
    'auctions_group_desc' => TEXT_INFO_GROUP_DEFAULT_DESC,
  );

  if( $action == 'recently_ended' ) {

    $auctions_group_array = array(
      'auctions_group_id' => 0,
      'auctions_group_name' => TEXT_INFO_GROUP_ENDED_NAME,
      'auctions_group_desc' => TEXT_INFO_GROUP_ENDED_DESC,
    );

    $breadcrumb->add(NAVBAR_TITLE, tep_href_link($g_script, 'action=recently_ended'));
  } elseif( $action == 'favorites' ) {
    if( !tep_session_is_registered('customer_id') ) {
      tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
    }

    $auctions_group_array = array(
      'auctions_group_id' => 0,
      'auctions_group_name' => TEXT_INFO_GROUP_FAVORITES_NAME,
      'auctions_group_desc' => TEXT_INFO_GROUP_FAVORITES_DESC,
    );
    $breadcrumb->add(NAVBAR_TITLE, tep_href_link($g_script, 'action=favorites'));

  } else {
    $check_query = tep_db_query("select auctions_group_id, auctions_group_name, auctions_group_desc from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$auctions_group_id . "'");
    if( !tep_db_num_rows($check_query) ) {
      $check_query = tep_db_query("select auctions_group_id, auctions_group_name, auctions_group_desc from " . TABLE_AUCTIONS_GROUP . " order by sort_order limit 1");
      if( tep_db_num_rows($check_query) ) {
        $auctions_group_array = tep_db_fetch_array($check_query);
      } else {
        tep_redirect(tep_href_link('', '', 'NONSSL', false));
      }
    } else {
      $auctions_group_array = tep_db_fetch_array($check_query);
    }
    $auctions_group_id = $auctions_group_array['auctions_group_id'];
    $breadcrumb->add(NAVBAR_TITLE, tep_href_link($g_script, 'auction_group_id=' . $auctions_group_id));
  }
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php 
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
  ?>   
    
  <?php
  //$winner_query = tep_db_query("select customers_id from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' group by bid_price having count(*) = 1 order by bid_price desc limit 1");

  $bids_string = '';
  if( tep_session_is_registered('customer_id') ) {
    $customer_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customer_array = tep_db_fetch_array($customer_query);
    $bids_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '" class="deco_none"><span class="pad notice">' . sprintf(TEXT_INFO_AUCTION_BIDS_LEFT, '<span id="bids_notice">' . $customer_array['customers_bids'] . '</span>') . '</span></a>';
  }
    
  $groups_array = array();
  $groups_query_raw = "select auctions_group_id, auctions_group_name from " . TABLE_AUCTIONS_GROUP . " order by sort_order";
  tep_query_to_array($groups_query_raw, $groups_array, 'auctions_group_id');

  if( $action == 'coming_soon') {
?>    

      <div class="bounder">
        <div class="floater"><h1><?php echo HEADING_COMING_SOON; ?></h1></div>
        <div class="floatend" style="padding-top: 14px;"><?php echo $bids_string; ?></div>
       
      </div>
      <div class="bounder auction_list" style="border: 1px solid #777; background: #FFF;">
        <div class="pad">
          <div class="atabaltrow highter pad" style="padding-top: 20px;">
<?php
    foreach($groups_array as $key => $group) {
      echo '<a href="' . tep_href_link($g_script, 'auctions_group_id=' . $key) . '" class="atabs pad atabrow">' . $group['auctions_group_name'] . '</a>';
    }
    echo '<span class="atabs_selected heavy" style="padding: 12px 8px 14px 8px;">' . TEXT_INFO_GROUP_COMING_SOON_NAME . '</span>'; 
    echo '<a href="' . tep_href_link($g_script, 'action=recently_ended') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_ENDED_NAME . '</a>';
    if( tep_session_is_registered('customer_id') ) {
      //echo '<a href="' . tep_href_link($g_script, 'action=favorites') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_FAVORITES_NAME . '</a>';
    }

?>
          </div>
<?php
/*
    $description = TEXT_INFO_GROUP_COMING_SOON_DESC;
    if( !empty($description) ) {
?>
          <div class="cleaner"></div>
          <div class="vmargin"><?php echo $description; ?></div>
<?php
    } else {
*/
?>
          <div class="cleaner vmargin"></div>
<?php
    //}
    $auctions_array = array();
    $auctions_query_raw = "select auctions_id, auctions_name, auctions_image, products_id, start_price, bid_step, date_start from " . TABLE_AUCTIONS . " where status_id = '0' order by sort_id, date_start";

    $split_params = '';
    $auctions_split = new splitPageResults($auctions_query_raw, MAX_DISPLAY_SEARCH_RESULTS, 'auctions_id');
    if ( ($auctions_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>
          <div class="bounder">
            <div class="floater"><?php echo $auctions_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></div>
            <div class="floateend"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $split_params); ?></div>
          </div>
<?php
    }
    tep_query_to_array($auctions_split->sql_query, $auctions_array, 'auctions_id');
    foreach($auctions_array as $key => $auction) {
      $splash_class = tep_auction_get_splash($auction);

      $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$key . "'");
      $tiers_array = tep_db_fetch_array($tiers_query);
      $row_info_class = 'rowInfo1';
      if( $tiers_array['total'] == 1 ) {
        $row_info_class = 'rowInfo2';
      } elseif($tiers_array['total'] > 1) {
        $row_info_class = 'rowInfo3';
      }
?>
          <div class="quarter floater auction_item" style="height: 320px;" id="auction_item_<?php echo $key; ?>">
            <div class="hpad hider">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div id="auction_block_<?php echo $key; ?>" style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; min-height: 270px; padding-top: 0px;">
                <div class="ralign rpad <?php echo $row_info_class; ?>" title="<?php echo sprintf(TEXT_INFO_AUCTION_PRIZES_COUNT, $tiers_array['total']+1); ?>"><?php echo '<a href="#" attr="' . $auction['auctions_id'] . '" class="auction_details" title="' . sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name']) . '">' . tep_image(DIR_WS_ICONS . 'icon_info.png', sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name'])) . '</a>'; ?></div>
                <div><table class="talign"><tr><td class="calign" style="height: 42px;"><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . $auction['auctions_name'] . '</a>'; ?></td></tr></table></div>
                <div style="height: <?php echo SMALL_IMAGE_HEIGHT; ?>px;">
                  <div class="abs">
                    <div class="<?php echo $splash_class; ?> heavy lalign" style="margin-left: 2px;" title="<?php echo sprintf(TEXT_INFO_AUCTION_BIDS_STEP_REQUIRED, $auction['bid_step'], $auction['auctions_name']); ?>"><div class="splash_text"><?php echo $auction['bid_step'] ?></div></div>
                  </div>
                  <div><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction['auctions_image'], $auction['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="fader_info"') . '</a>'; ?></div>
                </div>
                <div style="height: 39px;">
<?php
      $string_array = array();
      if($tiers_array['total']) {
        $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE, $currencies->format(tep_get_products_price($auction['products_id'])) );
        $tiers_array = array();
        $tiers_query_raw = "select products_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query_raw, $tiers_array);
        for($i=0, $j=count($tiers_array); $i<$j; $i++) {
          $index_string = $i+2;
          if( $i+2 == 2 ) {
            $index_string .= 'nd';
          } elseif( $i+2 == 3 ) {
            $index_string .= 'rd';
          } else {
            $index_string .= 'th';
          }
          $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE_TIERS, $index_string, $currencies->format(tep_get_products_price($tiers_array[$i]['products_id'])) );
        }
      } else {
        $string_array[] = sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction['products_id'])) );
      }
      echo implode('<br />', $string_array);
?>
                </div>
                <div class="heavy bspacer calign" style="height: 40px;">
<?php
    $starting_date = '';
    if( $auction['date_start'] && $auction['date_start'] != '0000-00-00 00:00:00' ) {
      $start_time = tep_auction_check_start($auction);
      if( strlen($start_time) ) {
        $tmp_array = explode(' ', tep_date_time_short($start_time));
        $starting_date = sprintf(TEXT_INFO_AUCTION_STARTS, '<br />' . $tmp_array[0] . '<br /><span class="font_large">' . $tmp_array[1] . '</span>');
      }
    } else {
      //$starting_date = sprintf(TEXT_INFO_AUCTION_STARTS, '<br />' . TEXT_NA);
    }
    echo $starting_date;
?>
                </div>
                <div class="vpad heavy tiny_text calign mark_red" id="bid_note2_<?php echo $key; ?>"></div>
              </div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
            </div>
          </div>
<?php
    }
?>
<?php
    if ( ($auctions_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>
          <div class="bounder">
            <div class="floater"><?php echo $auctions_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></div>
            <div class="floateend"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $split_params); ?></div>
            
          </div>
<?php
    }
?>
        </div>
      </div>
<?php
  } elseif( $action == 'recently_ended') {
?>
      <div class="bounder">
        <div class="floater"><h1><?php echo HEADING_RECENTLY_ENDED; ?></h1></div>
        <div class="floatend" style="padding-top: 14px;"><?php echo $bids_string; ?></div>
        <div class="floatend" style="padding-top: 14px; padding-right: 8px; height:25px; margin-bottom: 5px;" id="server_time"></div>
      </div>
      <div class="bounder auction_list" style="border: 1px solid #777; background: #FFF;">
        <div class="pad">
          <div class="atabaltrow highter pad" style="padding-top: 20px;">
<?php
    foreach($groups_array as $key => $group) {
      echo '<a href="' . tep_href_link($g_script, 'auctions_group_id=' . $key) . '" class="atabs pad atabrow">' . $group['auctions_group_name'] . '</a>';
    }
    //echo '<a href="' . tep_href_link($g_script, 'action=coming_soon') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_COMING_SOON_NAME . '</a>';
    echo '<span class="atabs_selected heavy" style="padding: 12px 8px 14px 8px;">' . TEXT_INFO_GROUP_ENDED_NAME . '</span>'; 
    if( tep_session_is_registered('customer_id') ) {
      //echo '<a href="' . tep_href_link($g_script, 'action=favorites') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_FAVORITES_NAME . '</a>';
    }
?>
          </div>
          <div class="cleaner vmargin"></div>
<?php
    $auctions_array = array();
    //$auctions_query_raw = "select auctions_id, auctions_name, auctions_image, completed from " . TABLE_AUCTIONS . " where status_id = '2' order by completed desc limit 4";
    $auctions_query_raw = "select auto_id, customers_id, auctions_id, auctions_name, auctions_price, products_name, started, completed, bid_count, winner_bids, customers_nickname, customer_count from " . TABLE_AUCTIONS_HISTORY . " group by auctions_id order by auto_id desc limit 4";

    tep_query_to_array($auctions_query_raw, $auctions_array, 'auctions_id');
    if( empty($auctions_array) ) {
?>
          <div class="vmargin"><?php echo TEXT_INFO_GROUP_ENDED_EMPTY; ?></div>
<?php
    } else {
/*
?>
          <div class="vmargin"><?php echo TEXT_INFO_GROUP_ENDED_DESC; ?></div>
          <div id="auction_group"></div>
<?php
*/
      foreach($auctions_array as $key => $auction) {
        //$splash_class = tep_auction_get_splash($auction);

        $data_query = tep_db_query("select auctions_image from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$key . "'");
        if( !tep_db_num_rows($data_query) ) { 
          $auction['auctions_image'] = IMAGE_NOT_AVAILABLE;
        } else {
          $data_array = tep_db_fetch_array($data_query);
          $auction['auctions_image'] = $data_array['auctions_image'];
        }

        $tier_array = array();
        $tier_query_raw = "select customers_id, customers_nickname, winner_bids from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction['auctions_id'] . "' and auto_id != '" . $auction['auto_id'] . "'";
        tep_query_to_array($tier_query_raw, $tier_array);
?>
          <div class="quarter floater auction_item" style="height: 365px;">
            <div class="hpad hider">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; height: 347px; padding-top: 0px;">
                <div class="ralign rpad moduleRowSelected"><?php echo '<a href="#" attr="' . $auction['auctions_id'] . '" class="auction_details" title="' . sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name']) . '">' . tep_image(DIR_WS_ICONS . 'icon_info.png', sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name'])) . '</a>'; ?></div>
                <div><table class="talign"><tr><td style="height: 42px;"><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . $auction['auctions_name'] . '</a>'; ?></td></tr></table></div>
                <div><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction['auctions_image'], $auction['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="fader_info"') . '</a>'; ?></div>
                <div><?php echo sprintf(TEXT_INFO_AUCTION_FINAL_PRICE, $currencies->format($auction['auctions_price'])); ?></div>
                <div class="vspacer"><?php echo sprintf(TEXT_INFO_AUCTION_ENDED, tep_date_long($auction['completed'])); ?></div>
<?php
/*
                <div class="heavy bspacer">
<?php 
        echo sprintf(TEXT_INFO_BIDS_ENTERED, $auction['bid_count']);
?>
                </div>
*/
?>
                <div class="heavy hider">
<?php
      if( tep_session_is_registered('customer_id') ) {
        $output_array = array();
        $self_bids = '';
        if( $auction['customers_id'] == $customer_id) {
          $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $auction['winner_bids']) . '</span>';
        }

        $j = count($tier_array);

        $auction['customers_nickname'] = tep_truncate_string($auction['customers_nickname']);

        $output = '';
        if( !empty($auction['customers_nickname']) ) {
          $output .= '<table class="tabledata">';
          //$output .= '<tr style="background: #FFFFCC"><td class="ralign">1.</td><td class="lalign mark_green">' . $auction['customers_nickname'] . '</td><td class="lalign">' . $auction['winner_bids'] . '</td></tr>';
          $output .= '<tr style="background: #FFFFCC"><td class="ralign">1.</td><td class="lalign mark_green">' . $auction['customers_nickname'] . '</td><td class="lalign"></td></tr>';

          //$output_array[] = sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS_DATA, 1, $auction['customers_nickname'], $auction['winner_bids']);

          for($i=0; $i<$j; $i++) {
            $rclass = ($i%2)?'altrow':'oddrow';
            $tier_array[$i]['customers_nickname'] = tep_truncate_string($tier_array[$i]['customers_nickname']);

            //$output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+2) . '.</td><td class="lalign mark_green">' . $tier_array[$i]['customers_nickname'] . '</td><td class="lalign">' . $tier_array[$i]['winner_bids'] . '</td></tr>';
            $output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+2) . '.</td><td class="lalign mark_green">' . $tier_array[$i]['customers_nickname'] . '</td><td class="lalign"></td></tr>';
            //$output_array[] = sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS_DATA, $i+2, $tier_array[$i]['customers_nickname'], $tier_array[$i]['winner_bids']);
            if( $tier_array[$i]['customers_id'] == $customer_id) {
              $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $tier_array[$i]['winner_bids']) . '</span>';
            }
          }
          $output .= '</table>';
        }

        if( !empty($self_bids) ) {
          echo '<div>' . $self_bids . '</div>';
        }

        if( !empty($auction['customers_nickname']) ) {
          echo '<div class="bspacer">' . sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS, $j+1) . '</div>';
        } else {
          echo '<div>' . TEXT_INFO_AUCTION_NO_WINNER . '</div>';
        }

        echo $output;


      } else {
        echo sprintf(TEXT_INFO_AUCTION_LOGIN_DETAILS);
      }
?>
                </div>
              </div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
            </div>
          </div>
<?php
      }
    }
?>
        </div>
      </div>
<?php
  } elseif( $action == 'favorites' ) {
    if( !tep_session_is_registered('favorite_auctions') ) {
      tep_session_register('favorite_auctions');
      $favorite_auctions = array();
    }
?>
      <div class="bounder">
        <div class="floater"><h1><?php echo HEADING_FAVORITES; ?></h1></div>
        <div class="floatend" style="padding-top: 14px;"><?php echo $bids_string; ?></div>
        
      </div>
      <div class="bounder auction_list" style="border: 1px solid #777; background: #FFF;">
        <div class="pad">
          <div class="atabaltrow highter pad" style="padding-top: 20px;">
<?php
    foreach($groups_array as $key => $group) {
      echo '<a href="' . tep_href_link($g_script, 'auctions_group_id=' . $key) . '" class="atabs pad atabrow">' . $group['auctions_group_name'] . '</a>';
    }
    //echo '<a href="' . tep_href_link($g_script, 'action=coming_soon') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_COMING_SOON_NAME . '</a>';
    echo '<a href="' . tep_href_link($g_script, 'action=recently_ended') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_ENDED_NAME . '</a>';
    echo '<span class="atabs_selected heavy" style="padding: 12px 8px 14px 8px;">' . TEXT_INFO_GROUP_FAVORITES_NAME . '</span>';
?>
          </div>
          <div class="cleaner"></div>
<?php
    if( !empty($favorite_auctions) ) {
      $auctions_array = array();
      $auctions_query_raw = "select auctions_id, auctions_name, auctions_image, completed from " . TABLE_AUCTIONS . " where auctions_id in (" . implode(',', $favorite_auctions) . ")";
      tep_query_to_array($auctions_query_raw, $auctions_array, 'auctions_id');
      if( empty($auctions_array) ) {
?>
          <div class="vmargin"><?php echo TEXT_INFO_GROUP_FAVORITES_EMPTY; ?></div>
<?php
      } else {
?>
          <div class="vmargin"><?php echo TEXT_INFO_GROUP_FAVORITES_DESC; ?></div>
<?php
        foreach($auctions_array as $key => $auction) {
?>
          <div class="quarter floater auction_item" style="height: 300px;" id="auction_item_<?php echo $key; ?>">
            <div class="hpad hider">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; min-height: 250px; padding-top: 16px;">
                <div class="calign"><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . $auction['auctions_name'] . '</a>'; ?></div>
                <div class="calign"><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction['auctions_image'], $auction['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>

                <div class="calign vspacer"><?php echo sprintf(TEXT_INFO_AUCTION_ENDED, $auction['completed']); ?></div>
                <div class="heavy tiny_text bspacer calign">
<?php 
          $count_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . $auction['auctions_id'] . "'");
          $count_array = tep_db_fetch_array($count_query);
          echo sprintf(TEXT_INFO_BIDS_ENTERED,$count_array['total']);
?>
                </div>
                <div class="heavy tiny_text mark_green bspacer calign">
<?php 
          $count_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . $auction['auctions_id'] . "'");
          $count_array = tep_db_fetch_array($count_query);
          echo sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED,$count_array['total']);
?>
                </div>
                <div class="heavy tiny_text mark_red bspacer calign">
<?php

          $winner_id = tep_auction_get_winner($auction['auctions_id']);
          switch( $winner_id ) {
            case 0:
              echo ERROR_AUCTION_NO_WINNER;
              break;
            case -1:
              echo ERROR_AUCTION_RUNNING;
              break;
            case -2:
              echo ERROR_AUCTION_NOT_STARTED;
              break;
            default:
              $winner_query = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$winner_id . "'");
              if( !tep_db_num_rows($winner_query) ) {
                echo ERROR_AUCTION_NO_WINNER;
              }
              $winner_array = tep_db_fetch_array($winner_query);
              echo sprintf(TEXT_INFO_AUCTION_WON, $winner_array['customers_nickname']);
              break;
          }
?>
                </div>
              </div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
            </div>
          </div>
<?php
        }
      }
    } else {
?>
          <div class="vmargin"><?php echo TEXT_INFO_GROUP_FAVORITES_EMPTY; ?></div>
<?php
    }
?>
        </div>
      </div>
<?php
  } else {
?>
      <div class="bounder">
        <div class="floater"><h1><?php echo sprintf(HEADING_TITLE, $auctions_group_array['auctions_group_name']); ?></h1></div>
        <div class="floatend" style="padding-top: 14px;"><?php echo $bids_string; ?></div>
        <div class="floatend" style="padding-top: 14px; padding-right: 8px; height:25px; margin-bottom: 5px;" id="server_time"></div>
      </div>

<?php
    $groups_array = array();
    $groups_query_raw = "select auctions_group_id, auctions_group_name from " . TABLE_AUCTIONS_GROUP . " order by sort_order";
    tep_query_to_array($groups_query_raw, $groups_array, 'auctions_group_id');
?>
      <div class="bounder auction_list" style="border: 1px solid #777; background: #FFF;">
        <div class="pad">
          <div class="atabaltrow highter pad" style="padding-top: 20px;">
<?php
    foreach($groups_array as $key => $group) {
?>
<?php
      if( $auctions_group_id == $group['auctions_group_id'] ) {
        echo '<span class="atabs_selected heavy" style="padding: 12px 8px 14px 8px;">' . $group['auctions_group_name'] . '</span>'; 
      } else {
        echo '<a href="' . tep_href_link($g_script, 'auctions_group_id=' . $key) . '" class="atabs pad atabrow">' . $group['auctions_group_name'] . '</a>';
      }
    }
    //echo '<a href="' . tep_href_link($g_script, 'action=coming_soon') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_COMING_SOON_NAME . '</a>';
    echo ' <a href="' . tep_href_link($g_script, 'action=recently_ended') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_ENDED_NAME . '</a>';
    if( tep_session_is_registered('customer_id') ) {
      //echo '<a href="' . tep_href_link($g_script, 'action=favorites') . '" class="atabs pad atabrow">' . TEXT_INFO_GROUP_FAVORITES_NAME . '</a>';
    }
?>
          </div>
          <div class="cleaner vmargin"></div>
<?php
    if( !empty($auctions_group_array['auctions_group_desc']) ) {
/*
?>
          <div class="vmargin"><?php echo $auctions_group_array['auctions_group_desc']; ?></div>
<?php
*/
    }
?>
          <div id="auction_group_block" attr="<?php echo (int)$auctions_group_id; ?>"></div>
<?php
    $auctions_array = $coming_array = array();
    $auctions_query_raw = "select * from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$auctions_group_id . "' and status_id = 1 order by sort_id";
    tep_query_to_array($auctions_query_raw, $auctions_array, 'auctions_id');

    $coming_query_raw = "select * from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$auctions_group_id . "' and status_id = 0 order by sort_id limit 4";
    tep_query_to_array($coming_query_raw, $coming_array, 'auctions_id');
    $auctions_array += $coming_array;

    foreach($auctions_array as $key => $auction) {
      $splash_class = tep_auction_get_splash($auction);

      $tiers_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$key . "'");
      $tiers_array = tep_db_fetch_array($tiers_query);
      $row_info_class = 'rowInfo1';
      if( $tiers_array['total'] == 1 ) {
        $row_info_class = 'rowInfo2';
      } elseif($tiers_array['total'] > 1) {
        $row_info_class = 'rowInfo3';
      }
      $height = ($auction['status_id']==1)?410:260;
      $height2 = ($auction['status_id']==1)?370:230;
?>
          <div class="quarter floater auction_item" style="height: <?php echo $height ?>px;" id="auction_item_<?php echo $key; ?>">
            <div class="hpad hider">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div id="auction_block_<?php echo $key; ?>" style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; padding-top: 0px; height:<?php echo $height2 ?>px">
                <div class="abs <?php echo $splash_class; ?> heavy lalign" style="z-index: 2; left: 3px;" title="<?php echo sprintf(TEXT_INFO_AUCTION_BIDS_STEP_REQUIRED, $auction['bid_step'], $auction['auctions_name']); ?>"><div class="splash_text"><?php echo $auction['bid_step'] ?></div></div>
                <div class="ralign rpad <?php echo $row_info_class; ?>" title="<?php echo sprintf(TEXT_INFO_AUCTION_PRIZES_COUNT, $tiers_array['total']+1); ?>">
<?php 
      echo '<a href="#" attr="' . $auction['auctions_id'] . '" class="auction_details" title="' . sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name']) . '">' . tep_image(DIR_WS_ICONS . 'icon_info.png', sprintf(TEXT_INFO_AUCTION_DETAILS, $auction['auctions_name'])) . '</a>'; 
?>

                </div>
                <div><table class="talign"><tr><td style="height: 42px;"><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . $auction['auctions_name'] . '</a>'; ?></td></tr></table></div>
                <div style="height: <?php echo SMALL_IMAGE_HEIGHT; ?>px">
                  <div><?php echo '<a href="' . tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction['auctions_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction['auctions_image'], $auction['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'class="fader_info"') . '</a>'; ?></div>
                </div>
                <div style="height: 39px;">
<?php
      $string_array = array();
      if($tiers_array['total']) {
        $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE, $currencies->format(tep_get_products_price($auction['products_id'])) );
        $tiers_array = array();
        $tiers_query_raw = "select products_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query_raw, $tiers_array);
        for($i=0, $j=count($tiers_array); $i<$j; $i++) {
          
          if( $i+2 == 2 ) {
            $index_string = '#';
          } elseif( $i+2 == 3 ) {
            $index_string = '#';
          } else {
            $index_string = '#';
          }
          
          $index_string .= $i+2;
          
          $string_array[] = sprintf(TEXT_INFO_AUCTION_PRICE_TIERS, $index_string, $currencies->format(tep_get_products_price($tiers_array[$i]['products_id'])) );
        }
      } else {
        $string_array[] = sprintf(TEXT_INFO_AUCTION_RRP, $currencies->format(tep_get_products_price($auction['products_id'])) );
      }
      echo implode('<br />', $string_array);
?>
                </div>
<?php
      if( $auction['status_id'] ) {
?>

                <div id="bg_flash_<?php echo $auction['auctions_id']; ?>" style="">
<?php
        //if( !tep_session_is_registered('customer_id') ) {
          //$note = TEXT_INFO_AUCTION_LOGIN_FIRST;
        //} else {
          //$check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . $auction['auctions_id'] . "'");
          //$check_array = tep_db_fetch_array($check_query);      

          $check_query = tep_db_query("select customers_id, customers_nickname from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
          
          //for LUBs
          $lowest_query = "select customers_id, count(bid_price) as winning from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' group by bid_price order by winning, bid_price, date_added";
          tep_query_to_array($lowest_query, $lowest_array);
          
          
          $tiers_query1 = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$key . "'");
          $tiers_array1 = tep_db_fetch_array($tiers_query1);
      
          for($i=0; $i<=($tiers_array1['total']); $i++)
          {                       
             $current_query[$i] = tep_db_query("SELECT count(distinct(auctions_id)) as orders FROM auctions_history WHERE customers_id = '" . (int)$lowest_array[$i]['customers_id'] . "'");
             $orders_info_query[$i] = tep_db_query("select count(*) as orders from " . TABLE_ORDERS . " where customers_id = '". (int)$lowest_array[$i]['customers_id'] . "'");
             $orders_info[$i] = tep_db_fetch_array($orders_info_query[$i]);
             $current_array[$i] = tep_db_fetch_array($current_query[$i]);
          }
           

          $check_array1 = tep_db_fetch_array($check_query);
          if( !tep_db_num_rows($check_query) ) {
            $auction_query = tep_db_query("select auctions_id, auctions_name, auctions_type, auctions_overbid, cap_price, start_price, bid_step, max_bids, bids_left, countdown_bids, countdown_timer, date_start, date_expire, start_pause, end_pause, status_id from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");      
            $auction = tep_db_fetch_array($auction_query);      
            $remaining = -1;
            $restart = false;
            $pause = tep_auction_check_pause($auction, $restart, $remaining);
            if( $pause ) {
              $note[0] = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_PAUSED . '</span>';
              $winning_label = '<div style="height: 14px; font-weight:bold; padding-top: 3px;"></div>';
            }
            else {
              $note[0] = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_WAIT_FIRST_BID . '</span>';
              $winning_label = '<div style="height: 14px; font-weight:bold; padding-top: 3px;"></div>';
              $awaiting_first_bid = true;
            } 
          } else {    
          //echo $auction['auctions_type'];
          $winning_label = '<div style="height: 14px; font-weight:bold; padding-top: 3px;">Winning</div>';
          if($auction['auctions_type'] == 1) {
          $tier_arrayx = array();
        $tier_query_rawx = "select customers_id, customers_nickname, winner_bids from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction['auctions_id'] . "'";
        tep_query_to_array($tier_query_rawx, $tier_arrayx);

        //var_dump($auction['auctions_id']);
          
          }
                    
          //if($auction['auctions_type'] == '0') {
            for($i=0; $i<=($tiers_array1['total']); $i++)
          {                       
             //$current_query[$i] = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS .  " where customers_id = '" . (int)$check_array1['customers_id'] . "'");
             //$current_query[$i]['customers_nickname'] = $check_array1['customers_nickname'];
             //$orders_info_query[$i] = tep_db_query("select count(*) as orders from " . TABLE_ORDERS . " where customers_id = '". (int)$check_array1['customers_id'] . "'");
             $orders_info_query[$i] = tep_db_query("SELECT count(distinct(auctions_id)) as orders FROM auctions_history WHERE customers_id = '". (int)$check_array1['customers_id'] . "'");
             $orders_info[$i] = tep_db_fetch_array($orders_info_query[$i]);
             $current_array[$i]['customers_nickname'] = $check_array1['customers_nickname'];
          }
          //}
          
            $check_array = tep_db_fetch_array($check_query);  
            for($i=0; $i<=($tiers_array1['total']); $i++) {
                //$note[$i] = sprintf(tep_auction_get_bidder_string($auction), tep_truncate_string($current_array[$i]['customers_nickname']).' ('.$orders_info[$i]['orders'].')');
                $note[$i] = tep_truncate_string($current_array[$i]['customers_nickname']).' ('.$orders_info[$i]['orders'].')';
            } 
          }
        //}
        $bids_left = '';

        $count_query = tep_db_query("select count(*) as total  from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
        $count_array = tep_db_fetch_array($count_query);
        
        if( $auction['max_bids'] && $auction['bids_left'] ) {
          $bids_left = '<span class="heavy mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_REMAIN, $auction['max_bids'] - $count_array['total']) . '</span>';
        }
        
         echo $winning_label;
        for($i=0; $i<=($tiers_array1['total']); $i++)
        {
        
        if(count($note)>1 && $awaiting_first_bid){
           echo '<div id="bid_note'.$auction['auctions_id'].'" class="bid_note'.$auction['auctions_id'].'" style="height: 14px;">' . $note[$i].'</div>';
        }
        elseif (count($note)>1) {
           echo '<div id="bid_note'.$auction['auctions_id'].'" class="bid_note'.$auction['auctions_id'].'" style="height: 14px;"> #'. ($i+1) . ' ' . $note[$i].'</div>';
        }
        else {
           echo '<div id="bid_note'.$auction['auctions_id'].'" class="bid_note'.$auction['auctions_id'].'" style="height: 14px;">' .$note[0].'</div>';
        }
           
        }
        
?>        
        
        
        
        
        
        
        
        
  
  
  
  <?php
    $auctions_arrayx = array();
    //$auctions_query_rawx = "select auctions_id, auctions_name, auctions_image, completed from " . TABLE_AUCTIONS . " where status_id = '2' order by completed desc limit 4";
    $auctions_query_rawx = "select auto_id, customers_id, auctions_id, auctions_name, auctions_price, products_name, started, completed, bid_count, winner_bids, customers_nickname, customer_count from " . TABLE_AUCTIONS_HISTORY . " group by auctions_id order by auto_id desc limit 4";

    tep_query_to_array($auctions_query_rawx, $auctions_arrayx, 'auctions_id');

      foreach($auctions_arrayx as $keyx => $auctionx) {
        //$splash_class = tep_auction_get_splash($auction);

        $data_queryx = tep_db_query("select auctions_image from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$keyx . "'");
        if( !tep_db_num_rows($data_queryx) ) { 
          $auctionx['auctions_image'] = IMAGE_NOT_AVAILABLE;
        } else {
          $data_arrayx = tep_db_fetch_array($data_queryx);
          $auctionx['auctions_image'] = $data_arrayx['auctions_image'];
        }

        $tier_arrayx = array();
        $tier_query_rawx = "select customers_id, customers_nickname, winner_bids from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auctionx['auctions_id'] . "' and auto_id != '" . $auctionx['auto_id'] . "'";
        tep_query_to_array($tier_query_rawx, $tier_arrayx);

      if( tep_session_is_registered('customer_id') ) {
        $output_arrayx = array();
        $self_bidsx = '';
        if( $auctionx['customers_id'] == $customer_id) {
          $self_bidsx = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $auctionx['winner_bids']) . '</span>';
        }

        $jx = count($tier_arrayx);
        $auctionx['customers_nickname'] = tep_truncate_string($auctionx['customers_nickname']);

        $outputx = '';
        if( !empty($auctionx['customers_nickname']) ) {
          //$output .= '<table class="tabledata">';
          //$output .= '<tr style="background: #FFFFCC"><td class="ralign">1.</td><td class="lalign mark_green">' . $auction['customers_nickname'] . '</td><td class="lalign">' . $auction['winner_bids'] . '</td></tr>';
          //$output .= '<tr style="background: #FFFFCC"><td class="ralign">1.</td><td class="lalign mark_green">' . $auction['customers_nickname'] . '</td><td class="lalign"></td></tr>';
          //$output .= '<div id="bid_note'.$auction['auctions_id'].'" class="bid_note'.$auction['auctions_id'].'" style="height: 14px;padding-left: 20px;">' . $auction['customers_nickname'] . '</div>';
          //$output_array[] = sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS_DATA, 1, $auction['customers_nickname'], $auction['winner_bids']);

          for($i=0; $i<$jx; $i++) {
            $rclassx = ($i%2)?'altrow':'oddrow';
            $tier_arrayx[$i]['customers_nickname'] = tep_truncate_string($tier_arrayx[$i]['customers_nickname']);

            //$output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+2) . '.</td><td class="lalign mark_green">' . $tier_array[$i]['customers_nickname'] . '</td><td class="lalign">' . $tier_array[$i]['winner_bids'] . '</td></tr>';
            //$output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+2) . '.</td><td class="lalign mark_green">' . $tier_array[$i]['customers_nickname'] . '</td><td class="lalign"></td></tr>';
            $outputx .= '<div id="bid_note'.$auction['auctions_id'].'" class="bid_note'.$auction['auctions_id'].'" style="height: 14px;padding-left: 20px;">#' . ($i+2) . ' ' . $tier_arrayx[$i]['customers_nickname'] . '</div>';            
            //$output_array[] = sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS_DATA, $i+2, $tier_array[$i]['customers_nickname'], $tier_array[$i]['winner_bids']);
            //if( $tier_array[$i]['customers_id'] == $customer_id) {
              //$self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $tier_array[$i]['winner_bids']) . '</span>';
            //}
          }
          //$output .= '</table>';
        }
        //echo $outputx;


      }
      }    
?>

  
  
  
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
<?php

//echo $tiers_array1['total'];
if($tiers_array1['total'] == 0)
{
  //$height = '20px';
}
else {
  //$height = '14px';
}
       
if($tiers_array1['total']<= 1 ) {
?>

                  <div id="bid_left<?php echo $auction['auctions_id']; ?>" style="height: 28px;"><?php echo $bids_left; ?></div>

<?php
          }
          elseif($auction['auctions_type'] != 0) {
           ?>
           <div id="bid_left<?php echo $auction['auctions_id']; ?>" style="height: 14px;padding: 5px 0px 5px 20px;"><?php echo $bids_left; ?></div>
           <?php
          }
          else {
 ?>
 <div id="bid_left<?php echo $auction['auctions_id']; ?>" style="height: 0px;padding: 0px;"><?php echo $bids_left; ?></div>

 <?php         
          }
        $bids_query = tep_db_query("select customers_id, customers_nickname, bid_price, last_modified, signature from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . $auction['auctions_id'] . "'");
        if( !tep_db_num_rows($bids_query) ) {
          $auction_price = tep_round($auction['start_price'], 2);
          $bids_array = array(
            'customers_id' => 0,
            'customers_nickname' => '',
            'bid_price' => $auction_price,
            'last_modified' => date('Y-m-d H:i:s'),
          );

          if( $auction['auctions_type'] == 1 ) {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s'));
          } else {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s') . $auction_price);
          }
        } else {
          $bids_array = tep_db_fetch_array($bids_query);
        }
?>
                  
<?php
        $start_time = tep_auction_check_start($auction);
        $end_time = tep_auction_check_end($auction);
        $index_array = tep_get_countdown_bids_index($auction, $count_array['total']);
        if( $index_array['countdown_bids'] ) {
        ?>
        <div class="vpad heavy" id="bid_note2_<?php echo $key; ?>" style="height: 32px; line-height: 18px;text-align:center;">
        <?php
          $timer_string = $index_array['countdown_timer'];
          $x_bid_timer = '1st Bid ';
          if($index_array['countdown_bids'] > 1) {
            $x_bid_timer = 'X Bid ';
          }
          echo $x_bid_timer . sprintf(TEXT_INFO_AUCTION_TIMER_PRESENT, '<br /><span class="font_large nobg" style="color: #000;">' . $timer_string . '</span>');
        } elseif( !empty($start_time) ) {
        ?>
        <div class="vpad heavy" id="bid_note2_<?php echo $key; ?>" style="height: 32px; line-height: 18px;text-align:center;">
        <?php
          $tmp_array = explode(' ', tep_date_time_short($start_time));
          $starting_date = sprintf(TEXT_INFO_AUCTION_STARTS, ' ' . $tmp_array[0] . '<br /><span class="font_large" style="color: #000">' . $tmp_array[1] . '</span>');
          echo $starting_date;
        } elseif( !empty($end_time) ) { 
        ?>
        <div class="vpad heavy" id="bid_note2_<?php echo $key; ?>" style="height: 19px; line-height: 18px;text-align:center;">
        <?php
          $tmp_array = explode(' ', tep_date_time_short($end_time));
          $ending_date = sprintf(TEXT_INFO_AUCTION_ENDS, ' ' . $tmp_array[0] . '<br /><span class="font_large" style="color: #000">' . $tmp_array[1] . '</span>');
          echo $ending_date;
        } else {
        ?>
        <div class="vpad heavy" id="bid_note2_<?php echo $key; ?>" style="height: 32px; line-height: 18px;text-align:center;">
        <?php
          //echo '<img src="' . DIR_WS_IMAGES . 'no_countdown_timer.png' . '" border="0" class="nobg" />';
        }
?>
                  </div>
                </div>
                <div class="heavy" id="auction_entry_bid_<?php echo $key; ?>" style="height: 20px;
line-height: 20px;
position: absolute;
margin-left: auto;
margin-right: auto;
left: 0;
right: 0;
bottom: 41px;">
<?php 
        $symbol = $currencies->currencies[DEFAULT_CURRENCY]['symbol_left'];
        if( $auction['auctions_type'] == 1 ) {
          echo TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;' . $symbol . tep_draw_input_field('auction_bid[' . $key . ']', '0.00', 'size="4" class="bid_inputs" id="auction_input_' . $key . '"');
        } elseif($auction['auctions_type'] == 2) {
          echo TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;' . $symbol . tep_draw_input_field('auction_bid[' . $key . ']', tep_round($bids_array['bid_price']+(1/100),2), 'size="4" class="bid_inputs" id="auction_input_' . $key . '"');
        } else {
          echo TEXT_INFO_AUCTION_CURRENT_VALUE . '&nbsp;<span id="auction_display_' . $key . '">' . tep_round($bids_array['bid_price']+0.01,2) . '</span>';
        }
?>
                </div>
                <?php
                if($tiers_array1['total']<=1) {
                ?>
          
                <div class="heavy" style="padding: 9px 0px;
bottom: 0px;
position: absolute;
padding-left: 20px;
">
                 <?php
                 
                  }
                  elseif($auction['auctions_type'] == 1) {
                  ?>
                  <div class="heavy" style="padding: 11px 0px;">
                  <?php
                  }
                  else {
                  ?>
                   <div class="heavy" style="padding: 9px 0px;
bottom: 0px;
position: absolute;
padding-left: 20px;">
                  <?php
                  }
                 ?>
                
<?php
        echo tep_draw_hidden_field('hidden_bid[' . $key . ']', $bids_array['signature'], 'id="backup_bid_' . $key . '"');
        if( !tep_session_is_registered('customer_id') ) {
          echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" attr="' . $key . '" class="sticky_bid tbutton" title="' . TEXT_INFO_AUCTION_LOGIN_FIRST . '">' . TEXT_INFO_AUCTION_LOGIN_FIRST . '</a>';
        } else {
          echo '<a href="#" attr="' . $key . '" class="sticky_bid tbutton" style="padding-left: 20px; padding-right: 20px;" title="' . sprintf(TEXT_INFO_AUCTION_PLACE_BID, $auction['auctions_name']) . '">' . 'Place Bid Now' . '</a>';
        }
?>
                </div>
<?php
      } else {
?>
                <div id="bg_flash_<?php echo $auction['auctions_id']; ?>"><?php echo TEXT_INFO_AUCTION_COMING_SOON; ?></div>
<?php
      }
?>
              </div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
            </div>
          </div>
<?php
    }
?>
        </div>
      </div>
<?php
  }
?>
      <div class="bspacer"></div>
<?php require('includes/objects/html_end.php'); ?>
