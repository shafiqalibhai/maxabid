<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: For auction callbacks - minimum application initialization
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $expire_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key='MAX_CATALOG_SESSION_TIME'");
  $expire_array = tep_db_fetch_array($expire_query);
  $expiry = time() + $expire_array['configuration_value'];
  tep_db_query("update " . TABLE_SESSIONS . " set expiry = '" . tep_db_input($expiry) . "' where sesskey = '" . tep_db_input($_COOKIE[$session_name]) . "'");

  exit();
?>
