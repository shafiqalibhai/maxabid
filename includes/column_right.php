<?php
/*
  $Id: column_right.php,v 1.17 2003/06/09 22:06:41 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  $box_array = array();
  switch($g_script) {
    case FILENAME_DEFAULT:
      $box_array = array('shopping_cart.php');
      break;
    case FILENAME_PRODUCT_INFO:
      $box_array = array('tell_a_friend.php', 'shopping_cart.php');
      break;
    default:
      break;
  }
  if( !empty($box_array) ) {
?>

          <td id="right_pane"><table border="0" width="100%" cellspacing="0" cellpadding="0">
<!-- right_navigation //-->
<?php
    for($i=0, $j=count($box_array); $i<$j; $i++) {
      include(DIR_WS_BOXES . $box_array[$i]);
    }
?>
<!-- right_navigation_eof //-->
          </table></td>
<?php
  }
?>