<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Support Functions
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  function tep_auction_get_winners($auctions_id, &$winners_array) {
    $winners_array = array();
    $result = 0;

    $auction_query = tep_db_query("select auctions_type, status_id from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
    if( !tep_db_num_rows($auction_query) ) {
      return $result;
    }

    $auction_array = tep_db_fetch_array($auction_query);
    if( $auction_array['status_id'] == 0 ) {
      return -2;
    }
    if( $auction_array['status_id'] == 1 ) {
      return -1;
    }

    if( $auction_array['status_id'] != 2 ) {
      return $result;
    }

    switch($auction_array['auctions_type']) {
      case 0:
        $result = tep_auction_penny_winner($auctions_id, $winners_array);
        break;
      case 1:
        $result = tep_auction_lowest_unique_winner($auctions_id, $winners_array);
        break;
      case 2:
        $result = tep_auction_normal_winner($auctions_id, $winners_array);
        break;
      default:
        break;
    }
    return $result;
  }

  function tep_auction_penny_winner($auctions_id, &$winners_array) {
    $result = 0;

    $tier_limit = 1;

    $auctions_query = tep_db_query("select products_id, auctions_image, start_price as auctions_price, shipping_cost from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id");
    $auctions_array = tep_db_fetch_array($auctions_query);

    $tier_array = array();
    $tier_query_raw = "select products_id, auctions_price, auctions_image, shipping_cost from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id";
    tep_query_to_array($tier_query_raw, $tier_array);
    array_unshift($tier_array, $auctions_array);

    $tier_limit = count($tier_array);

    $winners_query_raw = "select customers_id, bid_price from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' order by bid_price desc limit " . (int)$tier_limit;
    tep_query_to_array($winners_query_raw, $winners_array);

    $result = $j = count($winners_array);

    for($i=0; $i<$j; $i++ ) {
      $winners_array[$i]['bid_price'] = $winners_array[$i]['bid_price'];
      $winners_array[$i]['customers_nickname'] = tep_auction_customer_nickname($winners_array[$i]['customers_id']);
      $winners_array[$i]['products_id'] = $tier_array[$i]['products_id'];
      $winners_array[$i]['auctions_price'] = $tier_array[$i]['auctions_price'];
      $winners_array[$i]['shipping_cost'] = $tier_array[$i]['shipping_cost'];
      $winners_array[$i]['auctions_image'] = $tier_array[$i]['auctions_image'];
    }

    return $result;
  }


  function tep_auction_lowest_unique_winner($auctions_id, &$winners_array) {
    $result = 0;
    $tier_limit = 1;

    $auctions_query = tep_db_query("select products_id, auctions_image, start_price as auctions_price, shipping_cost from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id");
    $auctions_array = tep_db_fetch_array($auctions_query);

    $tier_array = array();
    $tier_query_raw = "select products_id, auctions_price, auctions_image, shipping_cost from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id";
    tep_query_to_array($tier_query_raw, $tier_array);
    array_unshift($tier_array, $auctions_array);

    $tier_limit = count($tier_array);

    //$winners_query_raw = "select customers_id, bid_price from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' group by bid_price having count(*) = 1 order by bid_price limit " . (int)$tier_limit;
    $winners_query_raw = "select customers_id, bid_price, count(bid_price) as winning from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' group by bid_price order by winning, bid_price, date_added limit " . (int)$tier_limit;
    tep_query_to_array($winners_query_raw, $winners_array);

/*
    if( empty($winners_array) ) {
      $winners_query_raw = "select customers_id from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' order by bid_price, date_added limit " . (int)$tier_limit;
      tep_query_to_array($winners_query_raw, $winners_array);
    }
*/

    $result = $j = count($winners_array);

    for($i=0; $i<$j; $i++ ) {
      //$winners_array[$i]['bid_price'] = 0;
      $winners_array[$i]['bid_price'] = $winners_array[$i]['bid_price'];

      $winners_array[$i]['customers_nickname'] = tep_auction_customer_nickname($winners_array[$i]['customers_id']);
      $winners_array[$i]['products_id'] = $tier_array[$i]['products_id'];
      $winners_array[$i]['auctions_price'] = $tier_array[$i]['auctions_price'];
      $winners_array[$i]['shipping_cost'] = $tier_array[$i]['shipping_cost'];
      $winners_array[$i]['auctions_image'] = $tier_array[$i]['auctions_image'];
    }
    return $result;
  }

  function tep_auction_normal_winner($auctions_id, &$winners_array) {
    $result = 0;

    $tier_limit = 1;

    $auctions_query = tep_db_query("select products_id, auctions_image, start_price as auctions_price, shipping_cost from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id");
    $auctions_array = tep_db_fetch_array($auctions_query);

    $tier_array = array();
    $tier_query_raw = "select products_id, auctions_price, auctions_image, shipping_cost from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id";
    tep_query_to_array($tier_query_raw, $tier_array);
    array_unshift($tier_array, $auctions_array);

    $tier_limit = count($tier_array);

    $winners_query_raw = "select customers_id, bid_price from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "' order by bid_price desc limit " . (int)$tier_limit;
    tep_query_to_array($winners_query_raw, $winners_array);

    $result = $j = count($winners_array);

    for($i=0; $i<$j; $i++ ) {
      $winners_array[$i]['bid_price'] = $winners_array[$i]['bid_price'];
      $winners_array[$i]['customers_nickname'] = tep_auction_customer_nickname($winners_array[$i]['customers_id']);
      $winners_array[$i]['products_id'] = $tier_array[$i]['products_id'];
      $winners_array[$i]['auctions_price'] = $tier_array[$i]['auctions_price'];
      $winners_array[$i]['shipping_cost'] = $tier_array[$i]['shipping_cost'];
      $winners_array[$i]['auctions_image'] = $tier_array[$i]['auctions_image'];
    }

    return $result;
  }

  function tep_auction_customer_nickname($cid) {
    $result = 'N/A';
    $customers_query = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$cid . "'");
    if( !tep_db_num_rows($customers_query) ) {
      return $result;
    }
    $customers_array = tep_db_fetch_array($customers_query);
    $result = $customers_array['customers_nickname'];
    return $result;
  }

  function tep_auction_expire() {
    $check_array = array();
    $check_query_raw = "select auctions_id from " . TABLE_AUCTIONS . " where status_id='1' and now() >= date_expire and date_expire > 0";
    tep_query_to_array($check_query_raw, $check_array, 'auctions_id');

    foreach( $check_array as $key => $auction ) {
      tep_auction_expire_single($key);
    }
  }

  function tep_auction_expire_single($auctions_id) {

    $check_query = tep_db_query("select * from " . TABLE_AUCTIONS . " where status_id='1' and auctions_id = '" . (int)$auctions_id . "'");
    if( !tep_db_num_rows($check_query) ) {
      return;
    }
    $auction = tep_db_fetch_array($check_query);

    tep_db_query("update " . TABLE_AUCTIONS . " set status_id='2' where auctions_id = '" . (int)$auction['auctions_id'] . "'");


    $result = false;
    $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
    $check_array = tep_db_fetch_array($check_query);
    if( !$check_array['total'] ) {
      return;
    }

    $result = tep_db_query("delete from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
    if( !$result ) {
      return;
    }

    $sql_data_array = array(
      'auctions_id' => $auction['auctions_id'],
      'products_id' => $auction['products_id'],
      'auctions_name' => $auction['auctions_name'],
      'auctions_image' => $auction['auctions_image'],
      'auctions_price' => $auction['start_price'],
      'shipping_cost' => $auction['shipping_cost'],
      'started' => $auction['date_start'],
      'completed' => date('Y-m-d H:i:s'),
    );

    $winners_array = array();

    $result = tep_auction_get_winners($auction['auctions_id'], $winners_array);

    if( !empty($winners_array) ) {
      // set the application parameters
      $configuration_query = tep_db_query('select configuration_key as cfgKey, configuration_value as cfgValue from ' . TABLE_CONFIGURATION);
      while ($configuration = tep_db_fetch_array($configuration_query)) {
        if( !defined($configuration['cfgKey']) ) {
          define($configuration['cfgKey'], $configuration['cfgValue']);
        }
      }

      // include the mail classes
      require_once(DIR_WS_CLASSES . 'mime.php');
      require_once(DIR_WS_CLASSES . 'email.php');

      foreach($winners_array as $key => $value ) {
        $tmp_data_array = $sql_data_array;

        $tmp_data_array['products_id'] = $value['products_id'];
        $tmp_data_array['customers_id'] = $value['customers_id'];
        $tmp_data_array['customers_nickname'] = $value['customers_nickname'];
        $tmp_data_array['auctions_image'] = $value['auctions_image'];
        $tmp_data_array['last_notified'] = date('Y-m-d H:i:s');

        $stats_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$value['products_id'] . "' and language_id='1'");
        if( tep_db_num_rows($stats_query) ) {
          $stats_array = tep_db_fetch_array($stats_query);
          $products_name = $stats_array['products_name'];
        } else {
          $products_name = 'N/A';
        }
        $tmp_data_array['products_name'] = $products_name;

        $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
        $stats_array = tep_db_fetch_array($stats_query);
        $tmp_data_array['bid_count'] = $stats_array['total'];

        $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' and customers_id = '" . (int)$value['customers_id'] . "'");
        $stats_array = tep_db_fetch_array($stats_query);
        $tmp_data_array['winner_bids'] = $stats_array['total'];

        $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' group by customers_id");
        $tmp_data_array['customer_count'] = tep_db_num_rows($stats_query);

        if( $auction['auctions_type'] == 1) {
          //$tmp_data_array['auctions_price'] = $value['auctions_price'];
          //$tmp_data_array['auctions_price'] = $value['auctions_price']+$value['bid_price'];
          $tmp_data_array['auctions_price'] = $value['bid_price'];
        } else {
          $tmp_data_array['auctions_price'] = $value['bid_price'];
        }
        $tmp_data_array['shipping_cost'] = $value['shipping_cost'];
        
        tep_db_perform(TABLE_AUCTIONS_HISTORY, $tmp_data_array);
        $history_id = tep_db_insert_id();

        $basket_array = array(
          'auctions_history_id' => (int)$history_id,
          'customers_id' => (int)$value['customers_id'],
          'products_id' => tep_get_auction_product_id(),
          'customers_basket_quantity' => 1,
          //@todo remove start price
          'final_price' => tep_round($tmp_data_array['shipping_cost']+$tmp_data_array['auctions_price'],2),
          'customers_basket_date_added' => date('Ymd'),
        );
        tep_db_perform(TABLE_CUSTOMERS_BASKET, $basket_array);

        $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='auction_won'");

        if( tep_db_num_rows($mail_query) ) {
          $email_array = tep_db_fetch_array($mail_query);
          $customers_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$value['customers_id'] . "'");
          $customers_array = tep_db_fetch_array($customers_query);

//ob_start();
          $customers_name = $customers_array['customers_firstname'] . ' ' . $customers_array['customers_lastname'];
          $temp_array = array(
            'CUSTOMER_NAME' => $customers_name,
            'CUSTOMER_EMAIL' => $customers_array['customers_email_address'],
            'AUCTION_NAME' => $auction['auctions_name'],
            'AUCTION_PRODUCTS' => $products_name,
            'AUCTION_TOTALS' => tep_round($value['auctions_price']+$value['shipping_cost'],2),
            'STORE_NAME' => STORE_OWNER,
            'STORE_EMAIL_ADDRESS' => STORE_OWNER_EMAIL_ADDRESS,
          );
          $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
          $email_array['subject'] = tep_email_templates_replace_entities($email_array['subject'], $temp_array);
          tep_mail($customers_name, $customers_array['customers_email_address'], $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
//$errors = ob_get_contents();
//ob_end_clean();
//$output_result_array[$auctions_id]['growl'][] = $errors;
//tep_auction_format_callback($output_result_array);

        }
      }

    } else {
      $stats_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$auction['products_id'] . "' and language_id='1'");
      if( tep_db_num_rows($stats_query) ) {
        $stats_array = tep_db_fetch_array($stats_query);
        $products_name = $stats_array['products_name'];
      } else {
        $products_name = 'N/A';
      }

      $sql_data_array['products_name'] = $products_name;

      $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");
      $stats_array = tep_db_fetch_array($stats_query);
      $sql_data_array['bid_count'] = $stats_array['total'];

      $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' and customers_id = '" . (int)$value['customers_id'] . "'");
      $stats_array = tep_db_fetch_array($stats_query);
      $sql_data_array['winner_bids'] = $stats_array['total'];

      $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "' group by customers_id");
      $stats_array = tep_db_fetch_array($stats_query);
      $sql_data_array['customer_count'] = $stats_array['total'];

      tep_db_perform(TABLE_AUCTIONS_HISTORY, $sql_data_array);
    }
    tep_db_query("delete from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction['auctions_id'] . "'");

    $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where auctions_group_id = '" . $auction['auctions_group_id'] . "' and status_id = '1'");
    $check_array = tep_db_fetch_array($check_query);

    if( $check_array['total'] < 4 ) {
      $new_array = array();
      $new_query_raw = "select auctions_id, auto_timer from " . TABLE_AUCTIONS . " where auctions_group_id = '" . $auction['auctions_group_id'] . "' and status_id = '0' order by sort_id limit " . (4-$check_array['total']);
      tep_query_to_array($new_query_raw, $new_array, 'auctions_id');

      if( count($new_array) ) {
        tep_db_query("update " . TABLE_AUCTIONS . " set status_id = '1' where auctions_id in (" . implode(',', array_keys($new_array)) . ")");
        foreach($new_array as $key => $timer_array) {
          if( $timer_array['auto_timer'] ) {
            $off_time = date('Y-m-d H:i:s', strtotime('+ ' . $timer_array['auto_timer'] . 'days' ) );
            tep_db_query("update " . TABLE_AUCTIONS . " set live_off = '" . tep_db_input($off_time) . "' where auctions_id = '" . (int)$key . "'");
          }
        }
      }
    }
/*
    // Refill from coming soon
    $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where auctions_group_id = '" . $auction['auctions_group_id'] . "'");

    $new_array = array();
    $new_query_raw = "select auctions_id from " . TABLE_AUCTIONS . " where status_id = '0' order by sort_id limit 4";
    tep_query_to_array($new_query_raw, $new_array, 'auctions_id');
    if( count($new_array) ) {
      tep_db_query("update " . TABLE_AUCTIONS . " set status_id = '1' where auctions_id in " . implode(',', array_keys($new_array)) );
    }
*/

    //tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value=0 where configuration_key = 'AUCTION_SEMAPHORE'");
  }

  function tep_auction_check_pause($auction, &$restart, &$remaining, $last_modified=false) {
    $remaining = -1;
    $pause = false;
    $restart = $date = false;

    if( $auction['start_pause'] && $auction['start_pause'] != '00:00:00' && $auction['end_pause'] && $auction['end_pause'] != '00:00:00') {
      $time = strtotime(strftime("%H:%M:%S"), 0);
      $start = strtotime($auction['start_pause'], 0);
      $end = strtotime($auction['end_pause'], 0);
      if( $start > $end ) {
        if( $time < $end || $time > $start ) {
          $date = date('Y-m-d');
          $gm_end = tep_mysql_to_time_stamp($auction['end_pause']);
          $date = strtotime($date)+86400+$gm_end;
          $pause = true;
          $remaining = $time-$start;
        }
      } elseif( $time > $start && $time < $end ) {
        $date = date('Y-m-d');
        $gm_end = tep_mysql_to_time_stamp($auction['end_pause']);
        $date = strtotime($date)+$gm_end;
        $pause = true;
        $remaining = $end-$time;
      }
    }

    if( $pause && !empty($last_modified) && !empty($date) ) {
      $lm = strtotime($last_modified);
      if( $date > $lm ) {
        tep_db_query("update " . TABLE_AUCTIONS_BID . " set last_modified = FROM_UNIXTIME('" . tep_db_input($date) . "') where auctions_id = '" . $auction['auctions_id'] . "'");
      }
    }
    $restart = strftime("%Y-%m-%d %H:%M:%S", $date);
    return $pause;
  }

  function tep_auction_check_start($auction) {
    $pause = '';

    if( strlen($auction['date_start']) > 10 && $auction['date_start'] != '0000-00-00 00:00:00') {
      $time = time();
      $start = strtotime($auction['date_start'], 0);

      if( $start > $time ) {
        $pause = $auction['date_start'];
        if( $start-$time < 86400 ) {
          $tmp_array = explode(' ', $auction['date_start']);
          $pause = $tmp_array[1];
        }
      }
    }
    return $pause;
  }

  function tep_auction_check_end($auction) {
    $pause = '';

    if( strlen($auction['date_expire']) > 10 && $auction['date_expire'] != '0000-00-00 00:00:00') {
      $time = time();
      $start = strtotime($auction['date_expire'], 0);

      if( $start > $time ) {
        $pause = $auction['date_start'];
        if( $start-$time < 86400 ) {
          $tmp_array = explode(' ', $auction['date_expire']);
          $pause = $tmp_array[1];
        }
      }
    }
    return $pause;
  }


  function tep_auction_get_group_differences($group_id, $request_array) {

    $result_array = array();

    if( !empty($request_array) && is_array($request_array) ) {
      $check_query_raw = "select auctions_id from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$group_id . "' and status_id = '1' and auctions_id not in (" . implode(',', array_keys($request_array)) . ")";
    } else {
      $request_array = array();
      $check_query_raw = "select auctions_id from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$group_id . "' and status_id = '1'";
    }

    $check_array = array();
    tep_query_to_array($check_query_raw, $check_array, 'auctions_id');
    foreach($check_array as $key => $value ) {

      if( !isset($request_array[$key]) ) {
        $result_array[$key] = md5(date('Y-m-d H:i:s'));
      }
    }
    return $result_array;
  }

  function tep_get_countdown_bids_index($auction, $bid_count) {

    $result_array = array(
      'countdown_bids' => 0,
      'countdown_timer' => 0,
    );

    $bids_array = explode(',', $auction['countdown_bids']);
    $timer_array = explode(',', $auction['countdown_timer']);

    $j = count($bids_array);
    if( !$j ) {
      return $result_array;
    }
    if( $j == 1 ) {
      $result_array = array(
        'countdown_bids' => $auction['countdown_bids'],
        'countdown_timer' => $auction['countdown_timer'],
      );
      return $result_array;
    }

    for( $i=0; $i<$j; $i++ ) {
      if( $i && $bid_count < $bids_array[$i] ) {
        break;
      }
    }

    $result_array = array(
      'countdown_bids' => trim($bids_array[$i-1]),
      'countdown_timer' => trim($timer_array[$i-1]),
    );
    return $result_array;

  }

  function tep_auction_get_splash($auction) {
    return 'splash48_blue';

    switch($auction['bid_step']) {
      case 2:
        $result = 'splash48_green';
        break;
      case 3:
        $result = 'splash48_orange';
        break;
      case 4:
        $result = 'splash48';
        break;
      case 5:
        $result = 'splash48_purple';
        break;
      default:
        $result = 'splash48_blue';
        break;
    }
    return $result;
  }

  function tep_auction_get_bidder_string($auction) {

    if( $auction['auctions_type'] == 1 ) {
      $result = TEXT_INFO_LAST_BID;
    } else {
      $result = TEXT_INFO_LAST_LEADER;
    }
    return $result;
  }

  function tep_auction_display_growl_end($auctions_id) {
    global $output_result_array;

    $history_query = tep_db_query("select auctions_name, customers_nickname from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auctions_id . "'");

    if( !tep_db_num_rows($history_query) ) {
      $auctions_query = tep_db_query("select auctions_name from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
      $output_result_array[$auctions_id]['growl'][] = sprintf(TEXT_INFO_AUCTION_END_NULL, $auction['auctions_name']);
    } else {
      $counter = 1;
      while($history_array = tep_db_fetch_array($history_query) ) {
        if( empty($history_array['customers_nickname']) ) {
          $output_result_array[$auctions_id]['growl'][] = '<span class="heavy mark_red">' . sprintf(TEXT_INFO_AUCTION_END_NULL, $history_array['auctions_name']) . '</span>';
        } else {
          $output_result_array[$auctions_id]['growl'][] = '<span class="heavy" style="color: #33FFFF;">' . sprintf(TEXT_INFO_AUCTION_END_WINNER, $history_array['auctions_name'],  $history_array['customers_nickname'], $counter) . '</span>';
          $counter++;
        }
      }
    }
    //tep_auction_format_callback($output_result_array);
  }

  function tep_auction_format_callback($input_array, $output = true, $exit = true) {
    $result = '';

    if( !empty($input_array) ) {
      $result = base64_encode(serialize($input_array));
      if( $output ) {
        echo $result;
      }
    }

    if( $exit ) {
      exit();
    }
    return $result;
  }

?>