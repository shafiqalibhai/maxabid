<?php
/*
  $Id: html_output.php,v 1.56 2003/07/09 01:15:48 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

////
// The HTML href link wrapper function
  function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true, $url_encode=true) {
//-MS- SEO-G Added
    global $request_type, $session_started, $SID, $g_seo_url;
//-MS- SEO-G Added EOM

    if( empty($parameters) && $page == FILENAME_DEFAULT ) $page = '';

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == true) {
        $link = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine connection method on a link!<br><br>Known methods: NONSSL SSL</b><br><br>');
    }

    if (tep_not_null($parameters)) {
      $link .= $page . '?' . tep_output_string($parameters);
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

// Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
    if ( ($add_session_id == true) && ($session_started == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
      if (tep_not_null($SID)) {
        $_sid = $SID;
      } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
        if (HTTP_COOKIE_DOMAIN != HTTPS_COOKIE_DOMAIN) {
          $_sid = tep_session_name() . '=' . tep_session_id();
        }
      }
    }

//-MS- SEO-G Added
    if( isset($g_seo_url) && is_object($g_seo_url) && $search_engine_safe ) {
      if( $connection == 'NONSSL' || SEO_PROCESS_SSL == 'true' )
        $link = $g_seo_url->get_seo_url($link, $separator);
    }
//-MS- SEO-G Added EOM

    if (isset($_sid)) {
      $link .= $separator . tep_output_string($_sid);
    }

    if( $url_encode ) {
      $link = str_replace('&', '&amp;', $link);
    }

    return $link;
  }

// -MS- OTF Added
// "On the Fly" Auto Thumbnailer using GD Library originally Nate Welch (v1.5) Updated by various contributors (v1.7.1)
// Scales product images dynamically, resulting in smaller file sizes, and keeps
// proper image ratio.  Used in conjunction with product_thumb.php t/n generator.
  function tep_image($src, $alt = '', $width = '', $height = '', $params = '', $bForce = false, $full_path=false) { 
    global $g_relpath;
    $image_filesize = @filesize($src);
    if($image_filesize < 1024 ) {
      $bForce = true;
    }
    // Set default image variable and code
    if( !$full_path ) {
      $image = '<img src="' . $src . '"';
    } else {
      $image = '<img src="' . $g_relpath . $src . '"';
    }
    
    // Don't calculate if the image is set to a "%" width
    //if( $bForce == true || substr(strtolower($src), -4, 4) == '.gif' ) {
    if( $bForce == true ) {
      $dont_calculate = 1;
    } elseif (strstr($width,'%') == false && strstr($height,'%') == false) { 
      $dont_calculate = 0; 
    } else {
      $dont_calculate = 1;
    }

    // Do we calculate the image size?
    if (CONFIG_CALCULATE_IMAGE_SIZE && !$dont_calculate) { 
      
      // Get the image's information
      if ($image_size = @getimagesize($src)) { 
        
        $ratio = $image_size[1] / $image_size[0];
        
        // Set the width and height to the proper ratio
        if( $image_size[0] < $width && $image_size[1] < $height) { 
          $dont_calculate = 1;
        } elseif (!$width && $height) { 
          $ratio = $height / $image_size[1]; 
          $width = $image_size[0] * $ratio;
        } elseif ($width && !$height) { 
          $ratio = $width / $image_size[0]; 
          $height = $image_size[1] * $ratio;
        } elseif (!$width && !$height) { 
          $width = $image_size[0]; 
          $height = $image_size[1]; 
        } 
        
        // Scale the image if not the original size
        if( !$dont_calculate && ($image_size[0] != $width || $image_size[1] != $height) ) { 
          $rx = $image_size[0] / $width; 
          $ry = $image_size[1] / $height; 
    
          if ($rx < $ry) { 
            $width = $height / $ratio;
          } else { 
            $height = $width * $ratio;
          }
          $width = intval($width); 
          $height = intval($height);

          if( !$full_path ) {
            $premade = false;
            $tmp_array = explode('/',$src);
            if( is_array($tmp_array) && count($tmp_array) ) {
              $new_name = $tmp_array[count($tmp_array)-1];
              $new_name = substr($new_name, 0, -4);
              $base_image = FLY_THUMB_FOLDER . $new_name . FLY_THUMB_POSTFIX . $width . 'x' . $height;
              $check_image = $base_image . '.jpg';
              if ($image_size = @getimagesize($check_image)) {
                $image = '<img src="' . $check_image .'"';
                $premade = true;
              }
            }
            if( !$premade ) {
              $image = '<img src="fly_thumb.php?img='.$src.'&amp;w='.tep_output_string($width).'&amp;h='.tep_output_string($height).'"';
            }
          } else {
            $image = '<img src="' .  $g_relpath . 'fly_thumb.php?img='.$src.'&amp;w='.tep_output_string($width).'&amp;h='.tep_output_string($height).'"';
          }
          $width=$height=0;
        }
      } elseif (IMAGE_REQUIRED == 'false') { 
        return ''; 
      } 
    } 
    
    // Add remaining image parameters if they exist
    if ($width) { 
      $image .= ' width="' . tep_output_string($width) . '"'; 
    } 
    
    if ($height) { 
      $image .= ' height="' . tep_output_string($height) . '"'; 
    }       
    
    if (tep_not_null($params)) $image .= ' ' . $params;
    
    if( !empty($alt) ) {
      $image .= ' title="' . tep_output_string($alt) . '"';
    }

    $image .= ' alt="' . tep_output_string($alt) . '"';

    $image .= ' />';

    return $image; 
  }
// -MS- OTF Added EOM

  function tep_calculate_image($src, $width, $height) {
    $dont_calculate = 0;
    // Get the image's information
    if ($image_size = @getimagesize($src)) { 
      $ratio = $image_size[1] / $image_size[0];
      
      // Set the width and height to the proper ratio
      if( $image_size[0] < $width && $image_size[1] < $height) { 
        $dont_calculate = 1;
      } elseif (!$width && $height) { 
        $ratio = $height / $image_size[1]; 
        $width = $image_size[0] * $ratio;
      } elseif ($width && !$height) { 
        $ratio = $width / $image_size[0]; 
        $height = $image_size[1] * $ratio;
      } elseif (!$width && !$height) { 
        $width = $image_size[0]; 
        $height = $image_size[1]; 
      } 
      // Scale the image if not the original size
      if( !$dont_calculate && ($image_size[0] != $width || $image_size[1] != $height) ) { 
        $rx = $image_size[0] / $width; 
        $ry = $image_size[1] / $height; 
  
        if ($rx < $ry) { 
          $width = $height / $ratio;
        } else { 
          $height = $width * $ratio;
        }
        $width = intval($width); 
        $height = intval($height); 
        $image = 'fly_thumb.php?img='.$src.'&amp;w='.tep_output_string($width).'&amp;h='.tep_output_string($height);
        return $image;
      }
    } 
    return ''; 
  }

////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
  function tep_image_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="image" src="' . tep_output_string(DIR_WS_LANGUAGES . $language . '/images/buttons/' . $image) . '" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) $image_submit .= ' title="' . tep_output_string($alt) . '"';

    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= ' />';

    return $image_submit;
  }

////
// The HTML form submit image wrapper function
// Outputs a button in the selected language
  function tep_main_image_submit($image, $alt = '', $width = '', $height = '', $parameters = '') {
    global $language;
    $string = tep_calculate_image($image,$width,$height);
    if( !tep_not_null($string) ) {
      $string = $image;
    }
    $image_submit = '<input type="image" src="' . $string . '" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) $image_submit .= ' title="' . tep_output_string($alt) . '"';
    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= ' />';

    return $image_submit;
  }


////
// The HTML form submit text wrapper function
// Outputs a text for a form
  function tep_text_submit($name, $value, $parameters = 'class="inputResults"') {
    global $language;

    $text_submit = '<input type="submit" name="' . $name . '"' . ' value="' . $value . '"';

    if (tep_not_null($parameters)) $text_submit .= ' ' . $parameters;

    $text_submit .= ' />';

    return $text_submit;
  }

////
// Output a function button in the selected language
  function tep_image_button($image, $alt = '', $parameters = '') {
    global $language;

    return tep_image(DIR_WS_LANGUAGES . $language . '/images/buttons/' . $image, $alt, '', '', $parameters);
  }

////
// Output a separator either through whitespace, or with an image
  function tep_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1', $full_path=false) {
    return tep_image(DIR_WS_IMAGES . $image, '', $width, $height, '', false, $full_path);
  }

////
// Output a form
  function tep_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . tep_output_string($name) . '" action="' . tep_output_string($action) . '" method="' . tep_output_string($method) . '"';

    if (tep_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    return $form;
  }

////
// Output a form input field
  function tep_draw_input_field($name, $value = '', $parameters = 'class="txtInput"', $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Output a form password field
  function tep_draw_password_field($name, $value = '', $parameters = 'maxlength="40" class="txtInput"') {
    return tep_draw_input_field($name, $value, $parameters, 'password', false);
  }

////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) $selection .= ' value="' . tep_output_string($value) . '"';

    if ( ($checked == true) || ( isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ( ($GLOBALS[$name] == 'on') || (isset($value) && (stripslashes($GLOBALS[$name]) == $value)) ) ) ) {
      $selection .= '';
    }

    if (tep_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= ' />';

    return $selection;
  }

////
// Output a form checkbox field
  function tep_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

////
// Output a form radio field
  function tep_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }

////
// Output a form textarea field
  function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    $field = '<textarea name="' . tep_output_string($name) . '" cols="' . tep_output_string($width) . '" rows="' . tep_output_string($height) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= tep_output_string_protected(stripslashes($GLOBALS[$name]));
    } elseif (tep_not_null($text)) {
      $field .= tep_output_string_protected($text);
    }

    $field .= '</textarea>';

    return $field;
  }

////
// Output a form file field
  function tep_draw_file_field($name, $parameters='', $required = false) {
    $field = tep_draw_input_field($name, '', $parameters, 'file', $required);

    return $field;
  }

////
// Output a form hidden field
  function tep_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Hide form elements
  function tep_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && tep_not_null($SID)) {
      return tep_draw_hidden_field(tep_session_name(), tep_session_id());
    }
  }

////
// Output a form pull down menu
  function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
    $field = '<select name="' . tep_output_string($name) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {

      if( isset($values[$i]['group_start']) ) {
        $field .= '<optgroup label="' . tep_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '">';
        continue;
      }
      if( isset($values[$i]['group_end']) ) {
        $field .= '</optgroup>';
        continue;
      }

      $field .= '<option value="' . tep_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' selected="selected"';
      }

      $field .= '>' . tep_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Creates a pull-down list of countries
  function tep_get_country_list($name, $selected = '', $parameters = '') {
    //$countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = tep_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
    }

    if( count($countries_array) > 1 ) {
      $result = tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
    } else {
      $result = $countries_array[0]['text'];
    }
    return $result;
  }

  function tep_break_array($name, $value='', $result='') {
    if (is_string($name)) {
      $result = tep_draw_hidden_field($name, $value);
      return $result;
    } elseif (is_array($name)) {
      foreach($name as $key => $value) {
        $result .= tep_break_array($value, $key);
      }
      return $result;
    } else {
      return $result;
    }
  }

?>
