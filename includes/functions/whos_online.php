<?php
/*
  $Id: whos_online.php,v 1.11 2003/06/20 00:12:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Whos Online functions
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// - Modified Script to use the IP address instead of the session
// - Added cookie sent column
// - Corrected visitor time entry
// - Added bot support
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  function tep_update_whos_online() {
    global $customer_id, $spider_name, $g_cookie;

    if (tep_session_is_registered('customer_id')) {
      $wo_customer_id = $customer_id;

      $customer_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
      $customer = tep_db_fetch_array($customer_query);

      $wo_full_name = $customer['customers_firstname'] . ' ' . $customer['customers_lastname'];
    } else {
      $wo_customer_id = '';
      if( isset($spider_name) && tep_not_null($spider_name) ) {
        $wo_full_name = 'Bot: ' . $spider_name;
      } else {
        $wo_full_name = 'Guest';
      }
    }

    $wo_session_id = tep_session_id();
    $wo_ip_address = getenv('REMOTE_ADDR');
    $wo_last_page_url = getenv('REQUEST_URI');

    $current_time = time();
    $xx_mins_ago = ($current_time - MAX_WHOS_ONLINE_TIME);

    // remove entries that have expired
    tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where time_last_click < '" . $xx_mins_ago . "'");

    $stored_customer_query = tep_db_query("select count(*) as count from " . TABLE_WHOS_ONLINE . " where ip_address = '" . tep_db_input(tep_db_prepare_input($wo_ip_address)) . "'");
    $stored_customer = tep_db_fetch_array($stored_customer_query);
    if ($stored_customer['count'] > 0) {
/*
      $sql_data_array = array(
                              'customer_id' => (int)$wo_customer_id,
                              'full_name' => tep_db_prepare_input($wo_full_name),
                              'session_id' => tep_db_prepare_input($wo_session_id),
                              'time_last_click' => tep_db_prepare_input($current_time),
                              'last_page_url' => tep_db_prepare_input($wo_last_page_url),
                              'cookie_sent' => ( isset($g_cookie))?1:0
                             );
      tep_db_perform(TABLE_WHOS_ONLINE, $sql_data_array, 'update', "ip_address = '" . tep_db_input(tep_db_prepare_input($wo_ip_address)) . "'");
*/
    } else {
      $sql_data_array = array(
                              'customer_id' => (int)$wo_customer_id,
                              'full_name' => tep_db_prepare_input($wo_full_name),
                              'ip_address' => tep_db_prepare_input($wo_ip_address),
                              'time_entry' => tep_db_prepare_input($current_time),
                              'time_last_click' => tep_db_prepare_input($current_time),
                              'last_page_url' => tep_db_prepare_input($wo_last_page_url),
                              'session_id' => tep_db_prepare_input($wo_session_id),
                              'cookie_sent' => ( isset($g_cookie))?1:0
                             );
      tep_db_perform(TABLE_WHOS_ONLINE, $sql_data_array, 'insert');
    }
  }
?>
