<?php
/*
  $Id: general.php,v 1.231 2003/07/09 01:15:48 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Added Catalog Support Functions
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 01/11/2008: HTML Cache functions, email templates, rma added
// - 02/17/2008: Removed unused functions, added active countriess
// - 02/27/2008: Support functions for products, categories added
// - 03/05/2008: Parameter validators added
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

// Strips special characters
  function tep_special_chars_to_html($special_string) {

    $chars = array(
           151 => '&#8212;',
           153 => '&#8482;',
           139 => '&#8249;',
           155 => '&#8250;',
           174 => '&#174;',
           146 => '&#8217;',
           147 => '&#8220;',
           148 => '&#8221;',
           150 => '&#8211;',
           39 => '&#8216;',
           169 => '&#169;');

    $special_string = str_replace(array_map('chr', array_keys($chars)), $chars, $special_string);
    return $special_string;
  }

// Strips special characters
  function tep_html_to_special_chars($special_string) {

    $chars = array(
           8482 => '',
           153 => '',
           174 => '',
           169 => '');

    $special_string = str_replace(array_map('chr', array_keys($chars)), $chars, $special_string);
    return $special_string;
  }

  function tep_query_to_array( $string_query, &$result_array, $index=false) {
    if( !is_array($result_array) ) {
      $result_array = array();
    }

    $query = tep_db_query($string_query);
    if( tep_db_num_rows($query) ) {
      if( !$index ) {
        while( $result_array[] = tep_db_fetch_array($query) );
        array_pop($result_array);
      } else {
        while( $tmp_array = tep_db_fetch_array($query) ) {
          $result_array[$tmp_array[$index]] = $tmp_array;
        }
      }
    }
    tep_db_free_result($query);
  }

  function tep_result_to_array( $query, &$result_array, $free=true) {
    if( !is_array($result_array) )
      $result_array = array();
    while( $result_array[] = tep_db_fetch_array($query) );
    array_pop($result_array);
    if($free) {
      tep_db_free_result($query);
    }
  }

  function tep_php_string($string) {
    $converted = stripslashes($string);
    $converted = tep_special_chars_to_html($converted);
    return $converted;
  }
  
  function tep_html_string($string) {
    $converted = htmlspecialchars($string);
    $converted = tep_special_chars_to_html($converted);
    return $converted;
  }

// -MS- words filter
  function profanity_check($text, $list) {
    if( !is_array($list) )
      return $text;

	foreach ($list['badwords'] as $k => $v) {
      $text = preg_replace("/$k/i",$v,$text);
    }
    return $text;
  } // END filter_bad_words


  function restricted_check($text, $list) {
    if( !is_array($list) )
      return $text;

	foreach ($list['restricted'] as $k) {
      if(preg_match("/$k/i",$text) )
        return true;
    }
    return false;
  }
// -MS- words filter EOM

////-MS- products collection added
  function tep_get_collection($category_id=0, $filter_array=array(), $where_flag=true, $select_flag=true, $products_array=array(), $order_by='', $count_flag=false) {
    global $languages_id;

    $in_string = '1';
    if( $category_id > 0 ) {
      $categories_array = array();
      tep_get_subcategories($categories_array, $category_id);
      $categories_array[] = (int)$category_id;
      $in_string = 'p2c.categories_id in (' . implode(',',$categories_array) . ')';
    }
    $select_string = '';

    if( is_array($filter_array) ) {
      foreach($filter_array as $db_col => $index) {
        if($where_flag) {
          $in_string .= " and p." . tep_db_input(tep_db_prepare_input($db_col)) . "='" . (int)$index  . "'";
        }
        if($select_flag) {
          $select_string .= "p." . tep_db_input(tep_db_prepare_input($db_col)) . ", ";
        }
      }
    }

    $products_string = '';
    if( is_array($products_array) && count($products_array) ) {
      $products_string = ' and p2c.products_id in (' . tep_db_input(tep_db_prepare_input(implode(',',$products_array))) . ')';
    }

    if( !tep_not_null($order_by) ) {
      $order_by = 'order by p.products_status desc, p.products_ordered desc, p.products_id desc';
    } else {
      $order_by = 'order by ' . $order_by;
    }

    $group_by = 'group by p.products_id';
    $final_select_string = 'p.products_id,' . $select_string . 'p.products_image, p.products_price, p.products_model, p.products_status, p.products_tax_class_id';
    if( $count_flag ) {
      $final_select_string = 'count(*) as total';
      $group_by = $order_by = '';
    }
    $collection_query_raw = "select " . $final_select_string . " from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (p2c.products_id=p.products_id) where " . $in_string . $products_string . " and p.products_display='1' " . $group_by . " " . $order_by;
    return $collection_query_raw;
  }
////-MS- products collection added EOM

////
// Stop from parsing any further PHP code
  function tep_exit() {
    if( function_exists('tep_session_close') ) {
      tep_session_close();
    }
    exit();
  }

////
// Redirect to another page or site
  function tep_redirect($url, $type_redirect='') {
    global $session_started;
    if ( (strstr($url, "\n") != false) || (strstr($url, "\r") != false) ) { 
      tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
    }

    if( !isset($session_started) || $session_started == false ) {
      $type_redirect='301';
    }
    $url = str_replace('&amp;', '&', $url);
    if( tep_not_null($type_redirect)) {
      header("HTTP/1.1 " . $type_redirect);
    }
    header('P3P: CP="NOI ADM DEV PSAi COM NAV STP IND"');
    header('Location: ' . $url);
    tep_exit();
  }

////
// Parse the data used in the html tags to ensure the tags will not break
  function tep_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function tep_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return tep_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return tep_parse_input_field_data($string, $translate);
      }
    }
  }

  function tep_output_string_protected($string) {
    return tep_output_string($string, false, true);
  }

  function tep_sanitize_string($string) {
    $patterns = array ('/ +/','/[<>]/');
    $replace = array (' ', '_');
    return preg_replace($patterns, $replace, trim($string));
  }

////
// Return a random row from a database query
  function tep_random_select($query) {
    $random_product = '';
    $random_query = tep_db_query($query);
    $num_rows = tep_db_num_rows($random_query);
    if ($num_rows > 0) {
      $random_row = tep_rand(0, ($num_rows - 1));
      tep_db_data_seek($random_query, $random_row);
      $random_product = tep_db_fetch_array($random_query);
    }

    return $random_product;
  }

////
// Return a product's name
// TABLES: products
  function tep_get_products_name($product_id, $language = '') {
    global $languages_id;

    if (empty($language)) $language = $languages_id;

    $product_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_name'];
  }

  function tep_get_category_name($category_id, $language= '') {
    global $languages_id;

    if (empty($language)) $language = $languages_id;

    $category_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_name'];
  }

  function tep_get_products_price($product_id) {
    $product_query = tep_db_query("select products_price from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    $product = tep_db_fetch_array($product_query);
    return $product['products_price'];
  }

////
// Return a product's special price (returns nothing if there is no offer)
// TABLES: products
  function tep_get_products_special_price($product_id) {
    $product_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$product_id . "' and status");
    $product = tep_db_fetch_array($product_query);

    return $product['specials_new_products_price'];
  }

////
// Return a product's stock
// TABLES: products
  function tep_get_products_stock($products_id) {
    $products_id = tep_get_prid($products_id);
    $stock_query = tep_db_query("select products_quantity from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
    $stock_values = tep_db_fetch_array($stock_query);

    return $stock_values['products_quantity'];
  }

////
// Check if the required stock is available
// If insufficent stock is available return an out of stock message
  function tep_check_stock($products_id, $products_quantity) {
    $stock_left = tep_get_products_stock($products_id) - $products_quantity;
    $out_of_stock = '';

    if ($stock_left < 0) {
      $out_of_stock = '<span class="markProductOutOfStock">' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '</span>';
    }

    return $out_of_stock;
  }

////
// Break a word in a string if it is longer than a specified length ($len)
  function tep_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

////
// Returns an array with countries
// TABLES: countries
  function tep_get_countries($countries_id = '', $with_iso_codes = false, $type='') {
    switch($type) {
      case 1:
        $filter_query = " and status_id = '1' and pay_status_id = '1'";
        break;
      case 2:
        $filter_query = " and status_id = '1' and ship_status_id = '1'";
        break;
      default:
        $filter_query = " and status_id = '1'";
        break;
    }

    $iso_filter = '';
    if( $with_iso_codes == true ) {
      $iso_filter = ',countries_iso_code_2, countries_iso_code_3';
    }

    $countries_array = array();
    if (tep_not_null($countries_id)) {
      $countries = tep_db_query("select countries_name "  . $iso_filter . " from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$countries_id . "'" . $filter_query . "");
      $countries_array = tep_db_fetch_array($countries);
    } else {
      $countries = tep_db_query("select countries_id, countries_name " . $iso_filter . " from " . TABLE_COUNTRIES . " where true " . $filter_query . " order by countries_name");
      while ($countries_values = tep_db_fetch_array($countries)) {
        $countries_array[] = $countries_values;
      }
    }

    return $countries_array;
  }

////
// Alias function to tep_get_countries, which also returns the countries iso codes
  function tep_get_countries_with_iso_codes($countries_id) {
    return tep_get_countries($countries_id, true);
  }

////
// Returns the clients browser
  function tep_browser_detect($component) {
    return stristr($_SERVER['HTTP_USER_AGENT'], $component);
  }

////
// Alias function to tep_get_countries()
  function tep_get_country_name($country_id) {
    $country_array = tep_get_countries($country_id);

    return $country_array['countries_name'];
  }

////
// Returns the zone (State/Province) name
// TABLES: zones
  function tep_get_zone_name($country_id, $zone_id, $default_zone) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    } else {
      return $default_zone;
    }
  }

////
// Returns the zone (State/Province) code
// TABLES: zones
  function tep_get_zone_code($country_id, $zone_id, $default_zone) {
    $zone_query = tep_db_query("select zone_code from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_code'];
    } else {
      return $default_zone;
    }
  }

////
// Wrapper function for round()
  function tep_round($n, $d = 0) {
    $result ='';
    $n = $n - 0;
    $org_n = $n;
    $n = abs($n);

    if ($d === NULL) $d = 2;

    $f = pow(10, $d);
    $n += pow(10, - ($d + 1));
    $n = round($n * $f) / $f;
    $n += pow(10, - ($d + 1));
    $n += '';

    if( $d == 0 ) {
      $result = substr($n, 0, strpos($n, '.'));
    } else {
      $result = substr($n, 0, strpos($n, '.') + $d + 1);
    }
    if( $org_n < 0 ) {
      $result = '-' . $result;
    }
    return $result;
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function tep_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {
    global $customer_zone_id, $customer_country_id;

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      if (!tep_session_is_registered('customer_id')) {
        $country_id = STORE_COUNTRY;
        $zone_id = STORE_ZONE;
      } else {
        $country_id = $customer_country_id;
        $zone_id = $customer_zone_id;
      }
    }

    $tax_query = tep_db_query("select sum(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " tr left join " . TABLE_ZONES_TO_GEO_ZONES . " za on (tr.tax_zone_id = za.geo_zone_id) left join " . TABLE_GEO_ZONES . " tz on (tz.geo_zone_id = tr.tax_zone_id) where (za.zone_country_id is null or za.zone_country_id = '0' or za.zone_country_id = '" . (int)$country_id . "') and (za.zone_id is null or za.zone_id = '0' or za.zone_id = '" . (int)$zone_id . "') and tr.tax_class_id = '" . (int)$class_id . "' group by tr.tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 1.0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier *= 1.0 + ($tax['tax_rate'] / 100);
      }
      return ($tax_multiplier - 1.0) * 100;
    } else {
      return 0;
    }
  }

////
// Return the tax description for a zone / class
// TABLES: tax_rates;
  function tep_get_tax_description($class_id, $country_id, $zone_id) {
    $tax_query = tep_db_query("select tax_description from " . TABLE_TAX_RATES . " tr left join " . TABLE_ZONES_TO_GEO_ZONES . " za on (tr.tax_zone_id = za.geo_zone_id) left join " . TABLE_GEO_ZONES . " tz on (tz.geo_zone_id = tr.tax_zone_id) where (za.zone_country_id is null or za.zone_country_id = '0' or za.zone_country_id = '" . (int)$country_id . "') and (za.zone_id is null or za.zone_id = '0' or za.zone_id = '" . (int)$zone_id . "') and tr.tax_class_id = '" . (int)$class_id . "' order by tr.tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_description = '';
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_description .= $tax['tax_description'] . ' + ';
      }
      $tax_description = substr($tax_description, 0, -3);

      return $tax_description;
    } else {
      return TEXT_UNKNOWN_TAX_RATE;
    }
  }

////
// Add tax to a products price
  function tep_add_tax($price, $tax = 0) {
    global $currencies;

    if ( (DISPLAY_PRICE_WITH_TAX == 'true') && ($tax > 0) ) {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + tep_calculate_tax($price, $tax);
    } else {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
    }
  }

// Calculates Tax rounding the result
  function tep_calculate_tax($price, $tax) {
    global $currencies;

    return tep_round($price * $tax / 100, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
  }

////
// Return the number of products in a category
// TABLES: products, products_to_categories, categories
  function tep_count_products_in_category($category_id, $include_inactive = false) {
    $products_count = 0;
    if ($include_inactive == true) {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p2c.categories_id = '" . (int)$category_id . "'");
    } else {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p.products_status = '1' and p2c.categories_id = '" . (int)$category_id . "'");
    }
    $products = tep_db_fetch_array($products_query);
    $products_count += $products['total'];

    $child_categories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$category_id . "'");
    if (tep_db_num_rows($child_categories_query)) {
      while ($child_categories = tep_db_fetch_array($child_categories_query)) {
        $products_count += tep_count_products_in_category($child_categories['categories_id'], $include_inactive);
      }
    }

    return $products_count;
  }

////
// Return true if the category has subcategories
// TABLES: categories
  function tep_has_category_subcategories($category_id) {
    $child_category_query = tep_db_query("select count(*) as count from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$category_id . "'");
    $child_category = tep_db_fetch_array($child_category_query);

    if ($child_category['count'] > 0) {
      return true;
    } else {
      return false;
    }
  }

////
// Returns the address_format_id for the given country
// TABLES: countries;
  function tep_get_address_format_id($country_id) {
    $address_format_query = tep_db_query("select address_format_id as format_id from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$country_id . "'");
    if (tep_db_num_rows($address_format_query)) {
      $address_format = tep_db_fetch_array($address_format_query);
      return $address_format['format_id'];
    } else {
      return '1';
    }
  }

////
// Return a formatted address
// TABLES: address_format
  function tep_address_format($address_format_id, $address, $html, $boln, $eoln) {
    $address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $company = tep_output_string_protected($address['company']);
    if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
      $firstname = tep_output_string_protected($address['firstname']);
      $lastname = tep_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && tep_not_null($address['name'])) {
      $firstname = tep_output_string_protected($address['name']);
      $lastname = '';
      $address['firstname'] = tep_output_string_protected($address['name']);
      $address['lastname'] = '';
      unset($address['name']);
    } else {
      $address['firstname'] = tep_output_string_protected($address['name']);
      $address['lastname'] = '';
      unset($address['name']);
      $firstname = '';
      $lastname = '';
    }
    $street = tep_output_string_protected($address['street_address']);
    $suburb = tep_output_string_protected($address['suburb']);
    $city = tep_output_string_protected($address['city']);
    $state = tep_output_string_protected($address['state']);
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        //$state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
        $state = tep_get_zone_name($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      //$country = tep_output_string_protected($address['country']['title']);
      $country = tep_output_string_protected($address['country']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($address['postcode']);
    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr>';
      $hr = '<hr>';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br>';
        $cr = '<br>';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($state != '') $statecomma = $state . ', ';

    $fmt = trim($address_format['format']);
    eval("\$address = \"$fmt\";");
    if ( (ACCOUNT_COMPANY == 'true') && (tep_not_null($company)) ) {
    $address = $company . $cr . $address;}
    return $address;
  }

////
// Return a formatted address
// TABLES: customers, address_book
  function tep_address_label($customers_id, $address_id = 1, $html = false, $boln = '', $eoln = "\n") {
    $address_query = tep_db_query("select entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customers_id . "' and address_book_id = '" . (int)$address_id . "'");
    $address = tep_db_fetch_array($address_query);

    $format_id = tep_get_address_format_id($address['country_id']);

    return tep_address_format($format_id, $address, $html, $boln, $eoln);
  }

  function tep_row_number_format($number) {
    if ( ($number < 10) && (substr($number, 0, 1) != '0') ) $number = '0' . $number;

    return $number;
  }

  function tep_get_categories($categories_array = '', $parent_id = '0', $indent = '') {
    global $languages_id;

    if (!is_array($categories_array)) $categories_array = array();

    $categories_query = tep_db_query("select c.categories_id, cd.categories_name from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where parent_id = '" . (int)$parent_id . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' order by sort_order, cd.categories_name");
    while ($categories = tep_db_fetch_array($categories_query)) {
      $categories_array[] = array('id' => $categories['categories_id'],
                                  'text' => $indent . $categories['categories_name']);

      if ($categories['categories_id'] != $parent_id) {
        $categories_array = tep_get_categories($categories_array, $categories['categories_id'], $indent . '&nbsp;&nbsp;');
      }
    }

    return $categories_array;
  }

  function tep_get_manufacturers($manufacturers_array = '') {
    if (!is_array($manufacturers_array)) $manufacturers_array = array();

    $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
    while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
      $manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'], 'text' => $manufacturers['manufacturers_name']);
    }

    return $manufacturers_array;
  }

////
// Return all subcategory IDs
// TABLES: categories
  function tep_get_subcategories(&$subcategories_array, $parent_id = 0) {
    $subcategories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$parent_id . "'");
    while ($subcategories = tep_db_fetch_array($subcategories_query)) {
      $subcategories_array[sizeof($subcategories_array)] = $subcategories['categories_id'];
      if ($subcategories['categories_id'] != $parent_id) {
        tep_get_subcategories($subcategories_array, $subcategories['categories_id']);
      }
    }
  }

  function tep_time_seconds($raw_date) {

  }

  function tep_mysql_to_time_stamp($mysqlDate, $gm=true) {

    if(!strlen($mysqlDate)) return 0;

    $mysqlDate = str_replace('/', '-', $mysqlDate);

    if (strlen($mysqlDate) > 10) {
      list($year, $month, $day_time) = explode('-', $mysqlDate);
      list($day, $time) = explode(" ", $day_time);
      list($hour, $minute, $second) = explode(":", $time);
      if( $gm ) {
        $ts = gmmktime($hour, $minute, $second, $month, $day, $year);
      } else {
        $ts = mktime($hour, $minute, $second, $month, $day, $year);
      }
    } elseif( strpos($mysqlDate, ':') ) {
      //list($hour, $minute, $second) = explode(":", $mysqlDate);
      //$ts = mktime($hour, $minute, $second);
      $tmp_array = explode(":", $mysqlDate);
      if( $gm ) {
        $ts = gmmktime($tmp_array[0], $tmp_array[1], $tmp_array[2], 1, 1, 1970);
      } else {
        $ts = mktime($tmp_array[0], $tmp_array[1], $tmp_array[2], 1, 1, 1970);
      }
      $year = 1970;
      $month = 1;
      $day = 1;

    } else {
      list($year, $month, $day) = explode('-', $mysqlDate);
      if( $gm ) {
        $ts = gmmktime(0, 0, 0, $month, $day, $year);
      } else {
        $ts = mktime(0, 0, 0, $month, $day, $year);
      }
    }
    if( !(int)$month || !(int)$day || !(int)$year ) $ts = 0;
    return $ts;
  }

// Output a raw date string in the selected locale date format
// $raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
  function tep_date_long($raw_date) {
  //var_dump($raw_date);
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = (int)substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    //return strftime(DATE_FORMAT_LONG, mktime($hour,$minute,$second,$month,$day,$year));
    $time= mktime($hour,$minute,$second,$month,$day,$year);
    return date("l j F, Y H:m:s", $time);
  }

////
// Output a raw date string in the selected locale date format
// $raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
// NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
  function tep_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || empty($raw_date) ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return preg_replace('/2037$/', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
  }

  function tep_date_time_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || empty($raw_date) ) {
      return false;
    }

    $format = '%d/%m/%Y %H:%M:%S';

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return strftime($format, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return preg_replace('/2037$/', $year, strftime($format, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
  }
////
// Parse search string into indivual objects
  function tep_parse_search_string($search_str = '', &$objects) {
    $search_str = trim(strtolower($search_str));

// Break up $search_str on whitespace; quoted string will be reconstructed later
    $pieces = preg_split('/[[:space:]]+/', $search_str);
    $objects = array();
    $tmpstring = '';
    $flag = '';

    for ($k=0; $k<count($pieces); $k++) {
      while (substr($pieces[$k], 0, 1) == '(') {
        $objects[] = '(';
        if (strlen($pieces[$k]) > 1) {
          $pieces[$k] = substr($pieces[$k], 1);
        } else {
          $pieces[$k] = '';
        }
      }

      $post_objects = array();

      while (substr($pieces[$k], -1) == ')')  {
        $post_objects[] = ')';
        if (strlen($pieces[$k]) > 1) {
          $pieces[$k] = substr($pieces[$k], 0, -1);
        } else {
          $pieces[$k] = '';
        }
      }

// Check individual words

      if ( (substr($pieces[$k], -1) != '"') && (substr($pieces[$k], 0, 1) != '"') ) {
        $objects[] = trim($pieces[$k]);

        for ($j=0; $j<count($post_objects); $j++) {
          $objects[] = $post_objects[$j];
        }
      } else {
/* This means that the $piece is either the beginning or the end of a string.
   So, we'll slurp up the $pieces and stick them together until we get to the
   end of the string or run out of pieces.
*/

// Add this word to the $tmpstring, starting the $tmpstring
        $tmpstring = trim(preg_replace('/"/', ' ', $pieces[$k]));

// Check for one possible exception to the rule. That there is a single quoted word.
        if (substr($pieces[$k], -1 ) == '"') {
// Turn the flag off for future iterations
          $flag = 'off';

          $objects[] = trim($pieces[$k]);

          for ($j=0; $j<count($post_objects); $j++) {
            $objects[] = $post_objects[$j];
          }

          unset($tmpstring);

// Stop looking for the end of the string and move onto the next word.
          continue;
        }

// Otherwise, turn on the flag to indicate no quotes have been found attached to this word in the string.
        $flag = 'on';

// Move on to the next word
        $k++;

// Keep reading until the end of the string as long as the $flag is on

        while ( ($flag == 'on') && ($k < count($pieces)) ) {
          while (substr($pieces[$k], -1) == ')') {
            $post_objects[] = ')';
            if (strlen($pieces[$k]) > 1) {
              $pieces[$k] = substr($pieces[$k], 0, -1);
            } else {
              $pieces[$k] = '';
            }
          }

// If the word doesn't end in double quotes, append it to the $tmpstring.
          if (substr($pieces[$k], -1) != '"') {
// Tack this word onto the current string entity
            $tmpstring .= ' ' . $pieces[$k];

// Move on to the next word
            $k++;
            continue;
          } else {
/* If the $piece ends in double quotes, strip the double quotes, tack the
   $piece onto the tail of the string, push the $tmpstring onto the $haves,
   kill the $tmpstring, turn the $flag "off", and return.
*/
            $tmpstring .= ' ' . trim(preg_replace('/"/', ' ', $pieces[$k]));

// Push the $tmpstring onto the array of stuff to search for
            $objects[] = trim($tmpstring);

            for ($j=0; $j<count($post_objects); $j++) {
              $objects[] = $post_objects[$j];
            }

            unset($tmpstring);

// Turn off the flag to exit the loop
            $flag = 'off';
          }
        }
      }
    }

// add default logical operators if needed
    $temp = array();
    for($i=0; $i<(count($objects)-1); $i++) {
      $temp[] = $objects[$i];
      if ( ($objects[$i] != 'and') &&
           ($objects[$i] != 'or') &&
           ($objects[$i] != '(') &&
           ($objects[$i+1] != 'and') &&
           ($objects[$i+1] != 'or') &&
           ($objects[$i+1] != ')') ) {
        $temp[] = ADVANCED_SEARCH_DEFAULT_OPERATOR;
      }
    }
    $temp[] = $objects[$i];
    $objects = $temp;

    $keyword_count = 0;
    $operator_count = 0;
    $balance = 0;
    for($i=0; $i<count($objects); $i++) {
      if ($objects[$i] == '(') $balance --;
      if ($objects[$i] == ')') $balance ++;
      if ( ($objects[$i] == 'and') || ($objects[$i] == 'or') ) {
        $operator_count ++;
      } elseif ( ($objects[$i]) && ($objects[$i] != '(') && ($objects[$i] != ')') ) {
        $keyword_count ++;
      }
    }

    if ( ($operator_count < $keyword_count) && ($balance == 0) ) {
      return true;
    } else {
      return false;
    }
  }

////
// Check date
  function tep_checkdate($date_to_check, $format_string, &$date_array) {
    $separator_idx = -1;

    $separators = array('-', ' ', '/', '.');
    $month_abbr = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
    $no_of_days = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    $format_string = strtolower($format_string);

    if (strlen($date_to_check) != strlen($format_string)) {
      return false;
    }

    $size = sizeof($separators);
    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($date_to_check, $separators[$i]);
      if ($pos_separator != false) {
        $date_separator_idx = $i;
        break;
      }
    }

    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($format_string, $separators[$i]);
      if ($pos_separator != false) {
        $format_separator_idx = $i;
        break;
      }
    }

    if ($date_separator_idx != $format_separator_idx) {
      return false;
    }

    if ($date_separator_idx != -1) {
      $format_string_array = explode( $separators[$date_separator_idx], $format_string );
      if (sizeof($format_string_array) != 3) {
        return false;
      }

      $date_to_check_array = explode( $separators[$date_separator_idx], $date_to_check );
      if (sizeof($date_to_check_array) != 3) {
        return false;
      }

      $size = sizeof($format_string_array);
      for ($i=0; $i<$size; $i++) {
        if ($format_string_array[$i] == 'mm' || $format_string_array[$i] == 'mmm') $month = $date_to_check_array[$i];
        if ($format_string_array[$i] == 'dd') $day = $date_to_check_array[$i];
        if ( ($format_string_array[$i] == 'yyyy') || ($format_string_array[$i] == 'aaaa') ) $year = $date_to_check_array[$i];
      }
    } else {
      if (strlen($format_string) == 8 || strlen($format_string) == 9) {
        $pos_month = strpos($format_string, 'mmm');
        if ($pos_month != false) {
          $month = substr( $date_to_check, $pos_month, 3 );
          $size = sizeof($month_abbr);
          for ($i=0; $i<$size; $i++) {
            if ($month == $month_abbr[$i]) {
              $month = $i;
              break;
            }
          }
        } else {
          $month = substr($date_to_check, strpos($format_string, 'mm'), 2);
        }
      } else {
        return false;
      }

      $day = substr($date_to_check, strpos($format_string, 'dd'), 2);
      $year = substr($date_to_check, strpos($format_string, 'yyyy'), 4);
    }

    if (strlen($year) != 4) {
      return false;
    }

    if (!settype($year, 'integer') || !settype($month, 'integer') || !settype($day, 'integer')) {
      return false;
    }

    if ($month > 12 || $month < 1) {
      return false;
    }

    if ($day < 1) {
      return false;
    }

    if (tep_is_leap_year($year)) {
      $no_of_days[1] = 29;
    }

    if ($day > $no_of_days[$month - 1]) {
      return false;
    }

    $date_array = array($year, $month, $day);

    return true;
  }

////
// Check if year is a leap year
  function tep_is_leap_year($year) {
    if ($year % 100 == 0) {
      if ($year % 400 == 0) return true;
    } else {
      if (($year % 4) == 0) return true;
    }

    return false;
  }

////
// Recursively go through the categories and retreive all parent categories IDs
// TABLES: categories
  function tep_get_parent_categories(&$categories, $categories_id) {
    $parent_categories_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
    while ($parent_categories = tep_db_fetch_array($parent_categories_query)) {
      if ($parent_categories['parent_id'] == 0) return true;
      $categories[sizeof($categories)] = $parent_categories['parent_id'];
      if ($parent_categories['parent_id'] != $categories_id) {
        tep_get_parent_categories($categories, $parent_categories['parent_id']);
      }
    }
  }

////
// Construct a category path to the product
// TABLES: products_to_categories
  function tep_get_product_path($products_id) {
    $cPath = '';

    $category_query = tep_db_query("select p2c.categories_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = '" . (int)$products_id . "' and p.products_status = '1' and p.products_id = p2c.products_id limit 1");
    if (tep_db_num_rows($category_query)) {
      $category = tep_db_fetch_array($category_query);

      $categories = array();
      tep_get_parent_categories($categories, $category['categories_id']);

      $categories = array_reverse($categories);

      $cPath = implode('_', $categories);

      if (tep_not_null($cPath)) $cPath .= '_';
      $cPath .= $category['categories_id'];
    }

    return $cPath;
  }

////
// Return a product ID with attributes
  function tep_get_uprid($prid, $params) {
    if (is_numeric($prid)) {
      $uprid = $prid;

      if (is_array($params) && (sizeof($params) > 0)) {
        $attributes_check = true;
        $attributes_ids = '';

        reset($params);
        while (list($option, $value) = each($params)) {
          if (is_numeric($option) && is_numeric($value)) {
            $attributes_ids .= '{' . (int)$option . '}' . (int)$value;
          } else {
            $attributes_check = false;
            break;
          }
        }

        if ($attributes_check == true) {
          $uprid .= $attributes_ids;
        }
      }
    } else {
      $uprid = tep_get_prid($prid);

      if (is_numeric($uprid)) {
        if (strpos($prid, '{') !== false) {
          $attributes_check = true;
          $attributes_ids = '';

// strpos()+1 to remove up to and including the first { which would create an empty array element in explode()
          $attributes = explode('{', substr($prid, strpos($prid, '{')+1));

          for ($i=0, $n=sizeof($attributes); $i<$n; $i++) {
            $pair = explode('}', $attributes[$i]);

            if (is_numeric($pair[0]) && is_numeric($pair[1])) {
              $attributes_ids .= '{' . (int)$pair[0] . '}' . (int)$pair[1];
            } else {
              $attributes_check = false;
              break;
            }
          }

          if ($attributes_check == true) {
            $uprid .= $attributes_ids;
          }
        }
      } else {
        return false;
      }
    }

    return $uprid;
  }

////
// Return a product ID from a product ID with attributes
  function tep_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    if (is_numeric($pieces[0])) {
      return $pieces[0];
    } else {
      return false;
    }
  }

////
// Return a customer greeting
  function tep_customer_greeting() {
    global $customer_id, $customer_first_name;

    if (tep_session_is_registered('customer_first_name') && tep_session_is_registered('customer_id')) {
      $greeting_string = sprintf(TEXT_GREETING_PERSONAL, tep_output_string_protected($customer_first_name), tep_href_link(FILENAME_PRODUCTS_NEW));
    } else {
      $greeting_string = sprintf(TEXT_GREETING_GUEST, tep_href_link(FILENAME_LOGIN, '', 'SSL'), tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'));
    }

    return $greeting_string;
  }

////
//! Send email (text/html) using MIME
// This is the central mail function. The SMTP Server should be configured
// correct in php.ini
// Parameters:
// $to_name           The name of the recipient, e.g. "Jan Wildeboer"
// $to_email_address  The eMail address of the recipient,
//                    e.g. jan.wildeboer@gmx.de
// $email_subject     The subject of the eMail
// $email_text        The text of the eMail, may contain HTML entities
// $from_email_name   The name of the sender, e.g. Shop Administration
// $from_email_adress The eMail address of the sender,
//                    e.g. info@mytepshop.com

  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;

    // Instantiate a new mail object
    $message = new email(array('X-Mailer: osCommerce Mailer'));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

////
// Check if product has attributes
  function tep_has_product_attributes($products_id) {
    $attributes_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$products_id . "'");
    $attributes = tep_db_fetch_array($attributes_query);

    if ($attributes['count'] > 0) {
      return true;
    } else {
      return false;
    }
  }

////
// Get the number of times a word/character is present in a string
  function tep_word_count($string, $needle) {
    $temp_array = preg_split('/' . $needle . '/', $string);

    return sizeof($temp_array);
  }

  function tep_count_modules($modules = '') {
    $count = 0;

    if (empty($modules)) return $count;

    $modules_array = explode(';', $modules);

    for ($i=0, $n=sizeof($modules_array); $i<$n; $i++) {
      $class = substr($modules_array[$i], 0, strrpos($modules_array[$i], '.'));

      if(isset($GLOBALS[$class]) && is_object($GLOBALS[$class])) {
        if ($GLOBALS[$class]->enabled) {
          $count++;
        }
      }
    }

    return $count;
  }

  function tep_count_payment_modules() {
    return tep_count_modules(MODULE_PAYMENT_INSTALLED);
  }

  function tep_count_shipping_modules() {
    return tep_count_modules(MODULE_SHIPPING_INSTALLED);
  }

  function tep_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) return false;

    $rand_value = '';
    while (strlen($rand_value) < $length) {
      if ($type == 'digits') {
        $char = tep_rand(0,9);
      } else {
        $char = chr(tep_rand(0,255));
      }
      if ($type == 'mixed') {
        if (preg_match('/^[a-z0-9]$/i', $char)) $rand_value .= $char;
      } elseif ($type == 'chars') {
        if (preg_match('/^[a-z]$/i', $char)) $rand_value .= $char;
      } elseif ($type == 'digits') {
        if (preg_match('/^[0-9]$/i', $char)) $rand_value .= $char;
      }
    }

    return $rand_value;
  }

  function tep_params_to_string($array, $sep='=', $glue='&') {
    $result = '';
    if( !is_array($array) || !count($array) ) return $result;

    $result_array = array();
    foreach($array as $key => $value) {
      if( empty($key) ) continue;
      $result_array[] = $key . $sep . $value;
    }
    $result = implode($glue, $result_array);
    return $result;
  }

  function tep_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {
    if (!is_array($exclude)) $exclude = array();

    $get_string = '';
    if (sizeof($array) > 0) {
      while (list($key, $value) = each($array)) {
        if ( (!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y') ) {
          $get_string .= $key . $equals . $value . $separator;
        }
      }
      $remove_chars = strlen($separator);
      $get_string = substr($get_string, 0, -$remove_chars);
    }

    return $get_string;
  }

  function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function tep_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

////
// Checks to see if the currency code exists as a currency
// TABLES: currencies
  function tep_currency_exists($code) {
    $code = tep_db_prepare_input($code);

    $currency_code = tep_db_query("select currencies_id from " . TABLE_CURRENCIES . " where code = '" . tep_db_input($code) . "'");
    if (tep_db_num_rows($currency_code)) {
      return $code;
    } else {
      return false;
    }
  }

  function tep_string_to_int($string) {
    return (int)$string;
  }

////
// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!isset($seeded)) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

  function tep_setcookie($name, $value = '', $expire = 0, $path = '/', $domain = '', $secure = 0) {
    setcookie($name, $value, $expire, $path, (tep_not_null($domain) ? $domain : ''), $secure);
  }

  function tep_get_ip_address() {
    return $_SERVER['REMOTE_ADDR'];
  }

  function tep_count_customer_orders($id = '', $check_session = true) {
    global $customer_id;

    if (is_numeric($id) == false) {
      if (tep_session_is_registered('customer_id')) {
        $id = $customer_id;
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( (tep_session_is_registered('customer_id') == false) || ($id != $customer_id) ) {
        return 0;
      }
    }

    $orders_check_query = tep_db_query("select count(*) as total from " . TABLE_ORDERS . " where customers_id = '" . (int)$id . "'");
    $orders_check = tep_db_fetch_array($orders_check_query);

    return $orders_check['total'];
  }

  function tep_count_customer_address_book_entries($id = '', $check_session = true) {
    global $customer_id;

    if (is_numeric($id) == false) {
      if (tep_session_is_registered('customer_id')) {
        $id = $customer_id;
      } else {
        return 0;
      }
    }

    if ($check_session == true) {
      if ( (tep_session_is_registered('customer_id') == false) || ($id != $customer_id) ) {
        return 0;
      }
    }

    $addresses_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$id . "'");
    $addresses = tep_db_fetch_array($addresses_query);

    return $addresses['total'];
  }

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function tep_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return preg_replace('/(' . implode('|', $from) . ')/', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }

  function tep_get_script_name($page='') {
    global $PHP_SELF;
    if( !tep_not_null($page) ) {
      $page = basename($PHP_SELF);
    }
    if( strlen($page) > 4 ) {
      $page = substr(basename($page), 0, -4);
    }
    return $page;
  }

//-MS- Added Cache HTML
// These HTML Cache functions used only for spiders
  function tep_check_modified_header() {
    global $PHP_SELF;
    if( SPIDERS_HTML_CACHE_ENABLE == 'false' )
      return;

    if( SPIDERS_HTML_CACHE_GLOBAL == 'true' ) {
      tep_send_304_header(SPIDERS_HTML_CACHE_TIMEOUT);
      return;
    }

    $script = basename($PHP_SELF);
    $md5_script = md5($script);
    $check_query = tep_db_query("select cache_html_duration from " . TABLE_CACHE_HTML . " where cache_html_type='1' and cache_html_key = '" . tep_db_input(tep_db_prepare_input($md5_script)) . "'");
    if( !tep_db_num_rows($check_query) )
      return;

    $check_array = tep_db_fetch_array($check_query);
    tep_send_304_header($check_array['cache_html_duration']);
  }

  function tep_send_304_header($timeout) {
    global $PHP_SELF;

    $oldtime = time() - $timeout;
    if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
      $if_modified_since = preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']);

      $expiry = strtotime($if_modified_since);
      if($expiry > $oldtime) {
        tep_set_cache_record(true);
        $expiry = tep_get_time_offset($expiry+$timout, false);
        header('Pragma: public');
        header('Expires: ' . $expiry);
        header('Cache-Control: must-revalidate, max-age=' . $timeout . ', s-maxage=' . $timeout . ', public');
        header('HTTP/1.1 304 Not Modified');
        tep_exit();
      }
    }
    tep_set_cache_record();
    $script_signature = md5(basename($PHP_SELF) . implode('', array_keys($_GET)) . implode('', $_GET));
    $now = tep_get_time_offset(0);
    $expiry = tep_get_time_offset($timeout);
    header('Pragma: public');
    header('Last-Modified: ' . $now);
    header('Expires: ' . $expiry);
    header('ETag: "' . $script_signature . '"');
    header('Cache-Control: must-revalidate, max-age=' . $timeout . ', s-maxage=' . $timeout . ', public');
  }

  function tep_get_time_offset($offset) {
    $newtime = time() + $offset;
    $gmt_time = gmdate('D, d M Y H:i:s', $newtime).' GMT';
    return $gmt_time;
  }

  function tep_set_cache_record($hit = false) {
    global $PHP_SELF;
    if( SPIDERS_HTML_CACHE_HITS == 'false' )
      return;
    $script = basename($PHP_SELF);
    $md5_script = md5($script);
    $check_query = tep_db_query("select cache_html_key from " . TABLE_CACHE_HTML_REPORTS . " where cache_html_key = '" . tep_db_input(tep_db_prepare_input($md5_script)) . "'");
    if( tep_db_num_rows($check_query) ) {
      if( $hit == false ) {
        tep_db_query("update " . TABLE_CACHE_HTML_REPORTS . " set cache_spider_misses = cache_spider_misses+1 where cache_html_key = '" . tep_db_input(tep_db_prepare_input($md5_script)) . "'");
      } else {
        tep_db_query("update " . TABLE_CACHE_HTML_REPORTS . " set cache_spider_hits  = cache_spider_hits+1 where cache_html_key = '" . tep_db_input(tep_db_prepare_input($md5_script)) . "'");
      }
    } else {
      $sql_data_array = array(
                              'cache_html_key' => tep_db_prepare_input($md5_script),
                              'cache_html_script' => tep_db_prepare_input($script)
                             );
      if( $hit == false ) {
        $sql_insert_array = array(
                                  'cache_spider_misses' => '1'
                                 );
      } else {
        $sql_insert_array = array(
                                  'cache_spider_hits' => '1'
                                 );
      }
      $sql_data_array = array_merge($sql_data_array, $sql_insert_array);
      tep_db_perform(TABLE_CACHE_HTML_REPORTS, $sql_data_array);
    }
  }
//-MS- Added Cache HTML EOM

//-MS- Added CSS Flyout Menu
  function tep_get_category_css_tree($parent_id = '0', $level_string='', $level=0) {
    global $languages_id;

    $categories_query = tep_db_query("select c.categories_id, c.parent_id, cd.categories_name from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");

    $level++;
    $sub_string = '';
    $sub_entries = tep_db_num_rows($categories_query);
    if( $sub_entries ) {
      if( $parent_id ) {
        $sub_string = '<ul style="width: 670px;">' . "\n";
      } else {
        $sub_string = '<ul>' . "\n";
      }
      $branch_string =
        '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
        '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n";

      while ($categories = tep_db_fetch_array($categories_query)) {
        if( !$categories['parent_id'] ) {
          $sub_class = '<li class="top">';
        } else {
          $sub_class = '<li class="sub">';
        }

        if( strlen($categories['categories_name']) > 50  ) {
          $categories['categories_name'] = substr($categories['categories_name'], 0, 50) . '...';
        }

        if( $categories['parent_id'] != '0' ) {
          $categories['categories_name'] = ucwords($categories['categories_name']);
        } else {
          $categories['categories_name'] = strtoupper($categories['categories_name']);
        }
        $categories['categories_name'] = htmlspecialchars(stripslashes($categories['categories_name']));
        $categories['categories_name'] = str_replace(' ', '&nbsp;', $categories['categories_name'] );

        if( $categories['parent_id'] != '0' ) {
          $tmp_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($categories['categories_id']) ) . '">' . $level_string . $categories['categories_name'];
        } else {
          $awidth = (strlen($categories['categories_name'])*9)+16;
          $tmp_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($categories['categories_id']) ) . '" class="top" style="width:' . $awidth . 'px">' . $level_string . $categories['categories_name'];
        }
        $result_string = tep_get_category_css_tree($categories['categories_id'], '&nbsp;&nbsp;&nbsp;&nbsp;', $level);
        if( tep_not_null($result_string) ) {
          $sub_string .= $sub_class . $tmp_string . $branch_string . $result_string;
          $sub_string .=
          '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
          '</li>' . "\n";
        } else {
          $sub_string .= '<li>' . $tmp_string . '</a></li>' . "\n";
        }
        $sub_entries--;
        //if( !$categories['parent_id'] && $sub_entries > 0 ) {
        if( !$categories['parent_id'] ) {
          $sub_string .= '<li class="sep">' . tep_image(DIR_WS_IMAGES . 'design/words_space.gif') . '</li>' . "\n";
        }
      }
      $sub_string .= '</ul>' . "\n";
    }
    return $sub_string;
  }

  function tep_get_product_css_tree($parent_id = '0', $level_string='', $level=0) {
    global $languages_id;

    $categories_query = tep_db_query("select c.categories_id, c.parent_id, c.logo_image, cd.categories_name from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");
    $awidth=0;
    $level++;
    $sub_string = '';
    $sub_entries = tep_db_num_rows($categories_query);
    if( $sub_entries ) {
      $sub_string = '<ul>' . "\n";
      $nve_width = 5;
      while ($categories = tep_db_fetch_array($categories_query)) {

        $awidth = (strlen($categories['categories_name'])*9)+16;
        $sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($categories['categories_id']) ) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($categories['categories_name']);
        $products_sql = tep_get_collection($categories['categories_id'], false, false, true);
        $products_sql .= ' limit 49';
        $total_items = array();
        tep_query_to_array($products_sql, $total_items);

        $tmp_array = array();
        for($i=0, $j=count($total_items); $i<$j; $i++ ) {
          $total_items[$i]['products_name'] = tep_get_products_name($total_items[$i]['products_id']);
          $total_items[$i]['products_name'] = tep_create_safe_string($total_items[$i]['products_name']);
          $tmp_array[$total_items[$i]['products_name']] = $total_items[$i];
        }
        ksort($tmp_array);
        $total_items = array_values($tmp_array);

        $j = count($total_items);

        $sub_string .=
          '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
          '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n";

        if( $j > 20 ) {
          $sub_string .=
            '<ul style="' . (($nve_width>0)?'margin-left:-' . $nve_width . 'px':'left:0') . '; width: 680px; ">' . "\n";
        } else {
          $sub_string .=
            '<ul style="left:0;">' . "\n";
        }

        $nve_width += $awidth;

        for( $i=0; $i<$j; $i++ ) {
          //$name = tep_get_products_name($total_items[$i]['products_id']);
          $name = $total_items[$i]['products_name'];
          $total_items[$i]['products_name'] = ucwords(tep_get_products_name($total_items[$i]['products_id']));
          if( strlen($total_items[$i]['products_name']) > 50  ) {
            $total_items[$i]['products_name'] = substr($total_items[$i]['products_name'], 0, 50) . '...';
          }
          $total_items[$i]['products_name'] = htmlspecialchars(stripslashes($total_items[$i]['products_name']));
          $tmp_string =
          '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '">&nbsp;&nbsp;&nbsp;&nbsp;' . $total_items[$i]['products_name'];
          $sub_string .= '<li>' . $tmp_string . '</a></li>' . "\n";
        }
        if( $j == 49 ) {
          $tmp_string =
          '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($categories['categories_id'])) . '">&nbsp;&nbsp;&nbsp;&nbsp;See All';
          $sub_string .= '<li>' . $tmp_string . '</a></li>' . "\n";
        }
        $sub_string .=
          '</ul>' . "\n" . 
          '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
          '</li>' . "\n";

        if( !$categories['parent_id'] ) {
          $sub_string .= '<li class="sep">' . tep_image(DIR_WS_IMAGES . 'design/words_space.gif') . '</li>' . "\n";
        }

      }
      $sub_string .= '</ul>' . "\n";
    }
    return $sub_string;
  }


  function tep_get_price_css_tree( $wrapper_open='<ul>', $wrapper_close = '</ul>') {
    $sub_string = '';
    $numeric_query = tep_db_query("select numeric_ranges_id, numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " order by sort_id, numeric_ranges_id");
    $sub_entries = tep_db_num_rows($numeric_query);
    if( $sub_entries ) {

      $sub_string = $wrapper_open;
      $sub_string =
        '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
        '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
        '<ul>' . "\n";

      while ($numeric = tep_db_fetch_array($numeric_query)) {
        if( strlen($numeric['numeric_ranges_desc']) > 42  ) {
          $numeric['numeric_ranges_desc'] = substr($numeric['numeric_ranges_desc'], 0, 50) . '...';
        }
        $numeric['numeric_ranges_desc'] = ucwords($numeric['numeric_ranges_desc']);
        $numeric['numeric_ranges_desc'] = htmlspecialchars(stripslashes($numeric['numeric_ranges_desc']));
        $numeric['numeric_ranges_desc'] = str_replace(' ', '&nbsp;', $numeric['numeric_ranges_desc'] );
        $tmp_string =
        '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $numeric['numeric_ranges_id']) . '" class="top">&nbsp;&nbsp;&nbsp;&nbsp;' . $numeric['numeric_ranges_desc'];
        $sub_string .= '<li>' . $tmp_string . '</a></li>' . "\n";
      }
      $sub_string .=
        '</ul>' . "\n" . 
        '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
        '</li>' . "\n";

      $sub_string .= $wrapper_close;
    }
    return $sub_string;
  }

  function tep_get_brand_css_tree( $wrapper_open='<ul>', $wrapper_close = '</ul>') {
    $brand_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . "");

    $sub_string = '';
    $sub_entries = tep_db_num_rows($brand_query);
    if( $sub_entries ) {

      $sub_string = $wrapper_open;
      $sub_string =
        '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
        '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
        '<ul>' . "\n";

      while ($brand = tep_db_fetch_array($brand_query)) {
        if( strlen($brand['manufacturers_name']) > 42  ) {
          $brand['manufacturers_name'] = substr($brand['manufacturers_name'], 0, 50) . '...';
        }
        $brand['manufacturers_name'] = ucwords($brand['manufacturers_name']);
        $brand['manufacturers_name'] = str_replace(' ', '&nbsp;', $brand['manufacturers_name'] );
        $tmp_string =
        '<a href="' . tep_href_link(FILENAME_MANUFACTURERS, 'manufacturers_id=' . $brand['manufacturers_id']) . '">&nbsp;&nbsp;' . $brand['manufacturers_name'];
        $sub_string .= '<li class="top">' . $tmp_string . '</a></li>' . "\n";
      }
      $sub_string .=
        '</ul>' . "\n" . 
        '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
        '</li>' . "\n";

      $sub_string .= $wrapper_close;
    }
    return $sub_string;
  }

  function tep_get_filter_linear_css_tree( $wrapper_open='<ul>', $wrapper_close = '</ul>') {
    global $g_filters, $languages_id;

    $filter_array = $g_filters->get_layout(array(1,2));
    $sub_string = "\n";
    foreach($filter_array as $key => $value) {
      $sub_entries = count($value);
      if( $sub_entries ) {
        $param_key = $g_filters->get_filters_parameter($key);
/*
        $name = $g_filters->get_filters_name($key);
        $awidth = (strlen($name)*9)+16;

        //$sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $param_key . '=' . $key) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($name);
        $sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_PRODUCTS_NEW) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($name);
        //$sub_string = $wrapper_open;
        $sub_string .=
          '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
          '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
          '<ul>' . "\n";
*/
        foreach($value as $key2 => $value2) {
          if( strlen($value2) > 42  ) {
            $value2 = substr($value2, 0, 50) . '...';
          }
          $value2 = strtoupper($value2);
          $awidth = (strlen($value2)*9)+16;
          $value2 = str_replace(' ', '&nbsp;', $value2 );
          $sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $param_key . '=' . $key2) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($value2);

          $categories_tree = $g_filters->get_categories($key, $key2);
          if( count($categories_tree) ) {
            $sub_string .=
              '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
              '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
              '<ul>' . "\n";

            //$sub_string .= '<li class="sub">' . $tmp_string . $branch_string;
            foreach($categories_tree as $key3 => $value3) {
              $subs_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$value3['categories_id'] . "' and language_id = '" . (int)$languages_id . "'");
              $subs_array = tep_db_fetch_array($subs_query);
              if( strlen($subs_array['categories_name']) > 50  ) {
                $subs_array['categories_name'] = substr($subs_array['categories_name'], 0, 50) . '...';
              }
              $subs_array['categories_name'] = ucwords($subs_array['categories_name']);
              $subs_array['categories_name'] = htmlspecialchars(stripslashes($subs_array['categories_name']));
              $subs_array['categories_name'] = str_replace(' ', '&nbsp;', $subs_array['categories_name'] );

              $sub_string .= '<li><a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, 'cPath=' . tep_get_category_path($value3['categories_id']) . '&' . $param_key . '=' . $key2) . '">&nbsp;&nbsp;&nbsp;&nbsp;' . $subs_array['categories_name'] . '</a></li>' . "\n";
            }

            $sub_string .=
            '</ul>' . "\n" . 
            '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
            '</li>' . "\n";

          } else {
            $sub_string .= '</a></li>' . "\n";
          }
          //$sub_string .= '<li class="sep">' . tep_image(DIR_WS_IMAGES . 'design/words_space.gif') . '</li>' . "\n";
          $sub_string .= $wrapper_close;
        }
/*
        $sub_string .=
          '</ul>' . "\n" . 
          '</td></tr></table><!--[if lte IE 6]></a><![endif]-->' . "\n" . 
          '</li>' . "\n";
*/
        //$sub_string .= '<li class="sep">' . tep_image(DIR_WS_IMAGES . 'design/words_space.gif') . '</li>' . "\n";
        //$sub_string .= $wrapper_close;
      }
    }
    return $sub_string;
  }


  function tep_get_filter_css_tree( $wrapper_open='<ul>', $wrapper_close = '</ul>') {
    global $g_filters, $languages_id;

    $last_sep = '';
    $filter_array = $g_filters->get_layout(array(1,2));
    $sub_string = '';
    foreach($filter_array as $key => $value) {
      $sub_entries = count($value);
      if( $sub_entries ) {
        $param_key = $g_filters->get_filters_parameter($key);
        $name = $g_filters->get_filters_name($key);
        $awidth = (strlen($name)*9)+16;

        //$sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $param_key . '=' . $key) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($name);
        $sub_string .= '<li class="top"><a href="' . tep_href_link(FILENAME_PRODUCTS_NEW) . '" class="ext" style="width:' . $awidth . 'px">' . strtoupper($name);
        //$sub_string = $wrapper_open;
        $sub_string .=
          '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
          '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
          '<ul>' . "\n";

        foreach($value as $key2 => $value2) {
          if( strlen($value2) > 42  ) {
            $value2 = substr($value2, 0, 50) . '...';
          }
          $value2 = ucwords($value2);
          $value2 = str_replace(' ', '&nbsp;', $value2 );
          $tmp_string =
          '<a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $param_key . '=' . $key2) . '" class="top">&nbsp;&nbsp;&nbsp;&nbsp;' . $value2;
          $categories_tree = $g_filters->get_categories($key, $key2);

          if( count($categories_tree) ) {
            $branch_string =
              '<!--[if gte IE 7]><!--></a><!--<![endif]-->' . "\n" . 
              '<!--[if lte IE 6]><table><tr><td><![endif]-->' . "\n" . 
              '<ul>' . "\n";

            $sub_string .= '<li class="sub">' . $tmp_string . $branch_string;
            foreach($categories_tree as $key3 => $value3) {
              $subs_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$value3['categories_id'] . "' and language_id = '" . (int)$languages_id . "'");
              $subs_array = tep_db_fetch_array($subs_query);
              if( strlen($subs_array['categories_name']) > 50  ) {
                $subs_array['categories_name'] = substr($subs_array['categories_name'], 0, 50) . '...';
              }
              $subs_array['categories_name'] = ucwords($subs_array['categories_name']);
              $subs_array['categories_name'] = htmlspecialchars(stripslashes($subs_array['categories_name']));
              $subs_array['categories_name'] = str_replace(' ', '&nbsp;', $subs_array['categories_name'] );

              $sub_string .= '<li><a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, 'cPath=' . tep_get_category_path($value3['categories_id']) . '&' . $param_key . '=' . $key2) . '">&nbsp;&nbsp;&nbsp;&nbsp;' . $subs_array['categories_name'] . '</a></li>';
            }
            $sub_string .=
            '</ul>' . "\n" . 
            '</td></tr></table><!--[if lte IE 6]></a><![endif]-->' . "\n" . 
            '</li>' . "\n";
          } else {
            $sub_string .= '<li>' . $tmp_string . '</a></li>' . "\n";
          }
        }
        $sub_string .=
          '</ul>' . "\n" . 
          '<!--[if lte IE 6]></td></tr></table></a><![endif]-->' . "\n" . 
          '</li>' . "\n";

        $last_sep = '<li class="sep">' . tep_image(DIR_WS_IMAGES . 'design/words_space.gif') . '</li>' . "\n";
        $sub_string .= $last_sep;
        //$sub_string .= $wrapper_close;
      }
    }
    if( strlen($last_sep) ) {
      $sub_string = substr($sub_string, 0, -strlen($last_sep));
    }
    return $sub_string;
  }


//-MS- Added CSS Flyout Menu EOM

  function tep_get_category_path($categories_id) {
    $categories = array();
    $categories[] = $categories_id;
    tep_get_parent_categories($categories, $categories_id);
    $categories = array_reverse($categories);
    $path = implode('_', $categories);
    if (substr($path, 0, 1) == '_') {
      $path = substr($path, 1);
    }
    return $path;
  }

//-MS- safe string added
  function tep_create_safe_string($string, $separator=' ') {
    $string = preg_replace('/\s\s+/', ' ', trim($string));
    $string = preg_replace("/[^0-9a-z]+/i", $separator, $string);
    $string = trim($string, $separator);
    $string = str_replace($separator . $separator . $separator, $separator, $string);
    $string = str_replace($separator . $separator, $separator, $string);
    return $string;
  }
//-MS- safe string added EOM

  function tep_truncate_string($string, $length=16, $trailer='...') {
    if( strlen($string) > $length ) {
      $string = substr($string, 0, $length) . $trailer;
    }
    return $string;
  }


//-MS- Email templates added
  function tep_email_templates_replace_entities($email_contents, $contents_array) {
    if( !is_array($contents_array) || !tep_not_null($email_contents) )
      return false;

    $translation = $email_contents;
    foreach($contents_array as $key => $value) {
      $translation = str_replace('[' . $key . ']', $value, $translation);
    }
    return $translation;
  }

  function tep_email_column($zone) {
    global $currencies;
    $result = '';
    $cBox = new product_front();
    $data = $cBox->get_zone_data($zone);
    if( is_array($data) && count($data) ) {
      $products_array = $cBox->get_random_products($zone);
      if( count($products_array) ) {
        $total_items = array();
        $listing_sql = tep_get_collection(0, '', false, false, $products_array);
        tep_query_to_array($listing_sql, $total_items);
        $result = 
          '<table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" .
          '  <tr>' . "\n" . 
          '    <td valign="top"><font size="3" face="verdana,geneva" color="000077"><strong>' . strtoupper($data['abstract_zone_name']) . '</strong></font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td>' . tep_draw_separator('pixel_trans.gif', '100%', '8', true) . '</td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td><font size="2" face="verdana,geneva">' . $data['abstract_zone_desc'] . '</font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td>' . tep_draw_separator('pixel_trans.gif', '100%', '8', true) . '</td>' . "\n" . 
          '  </tr>' . "\n"; 

        for($i=0, $j=count($total_items); $i<$j; $i++) {
          $total_items[$i]['products_name'] = tep_get_products_name($total_items[$i]['products_id']);
          $total_items[$i]['products_name'] = tep_create_safe_string($total_items[$i]['products_name']);

          if( !isset($total_items[$i]['specials_new_products_price']) ) {
            $total_items[$i]['specials_new_products_price'] = tep_get_products_special_price($total_items[$i]['products_id']);
          }
          if( tep_not_null($total_items[$i]['specials_new_products_price'])) {
            $products_price = '<s>' . $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id'])) . '</s><br />';
            $products_price .= $currencies->display_price($total_items[$i]['specials_new_products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id']));
          } else {
            $products_price = $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id']));
          }
          $tmp_image = DIR_WS_IMAGES . $total_items[$i]['products_image'];
          if( !tep_not_null($total_items[$i]['products_image']) || !file_exists($tmp_image) || filesize($tmp_image) <= 0 ) {
            $total_items[$i]['products_image'] = IMAGE_NOT_AVAILABLE;
          }

          $result .= 
          '  <tr>' . "\n" . 
          '    <td align="center"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $total_items[$i]['products_image'], $total_items[$i]['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, '', false, true) . '</a></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td align="center"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '" title="' . $total_items[$i]['products_name'] . '"><font size="2" face="verdana,geneva" color="000077"><strong>' . $total_items[$i]['products_name'] . '</strong></font></a></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td align="center"><font size="2" face="verdana,geneva" color="CC0000"><strong>' . $products_price . '</strong></font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td>' . tep_draw_separator('pixel_trans.gif', '100%', '8', true) . '</td>' . "\n" . 
          '  </tr>' . "\n"; 
        }
        $result .= 
          '</table>' . "\n";
      }
    }
    return $result;
  }
//-MS- Email templates added EOM

//-MS- Extra Fields Support Added
  // Extracts fields enclosed in brackets, can be used for attributes
  // Ex: products_id = 151{5}4{6}7 will return an array
  // array[0] = 'key' => 5, 'value' => 4
  // array[1] = 'key' => 6, 'value' => 7
  function tep_get_bracket_fields($products_id, $append=false) {
     $attributes_array = array();
     $tmp_array1 = explode('{', $products_id);
     $tmp_array2 = explode('}', $products_id);

     if( !is_array($tmp_array1) || !is_array($tmp_array2) || count($tmp_array1) < 2 || count($tmp_array1) != count($tmp_array2) )
       return $attributes_array;

     $tmp_string = str_replace('{', '_', $products_id); 
     $tmp_string = str_replace('}', '_', $tmp_string);
     $tmp_array1 = array();
     $tmp_array1 = explode('_', $tmp_string);

     unset($tmp_array1[0]);
     $tmp_array1 = array_values($tmp_array1);

     for($i=0, $j=count($tmp_array1); $i<$j; $i++ ) {
       if($i%2) {
         if( !is_numeric($tmp_array1[$i-1]) || !is_numeric($tmp_array1[$i]) )
           continue;
         if( !$append) {
           $attributes_array[$tmp_array1[$i-1] . '_' . $tmp_array1[$i]] = $tmp_array1[$i-1] . '_' . $tmp_array1[$i];
         } else {
           $attributes_array[$tmp_array1[$i-1]] = $tmp_array1[$i];
         }
       }
       //if($i%2)
       //  $attributes_array[$tmp_array1[$i-1]] = $tmp_array1[$i];
     }
     return $attributes_array;
  }
// Check if product has attributes
  function tep_has_product_fields($products_id) {
    return false;
  }

//-MS- Extra Fields Support Added EOM


//-MS- Active Countries Added
  function tep_get_country_zones($country=STORE_COUNTRY, $type=0) {
    switch($type) {
      case 1:
        $filter_query = " and pay_status_id = '1'";
        break;
      case 2:
        $filter_query = " and ship_status_id = '1'";
        break;
      default:
        $filter_query = '';
        break;
    }

    $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where status_id='1'" . $filter_query . " and zone_country_id = '" . (int)$country . "' order by zone_name");
    $zones_array = array();
    while ($zones_values = tep_db_fetch_array($zones_query)) {
      $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
    }
    return $zones_array;
  }

////
// Validate a country
  function tep_validate_active_countries($address_book_id, $type=0) {
    global $customer_id;
    switch($type) {
      case 1:
        $filter_query = " and c.pay_status_id = '1' and z.pay_status_id = '1'";
        break;
      case 2:
        $filter_query = " and c.ship_status_id = '1' and z.ship_status_id = '1'";
        break;
      default:
        $filter_query = '';
        break;
    }
    $check_query = tep_db_query("select ab.entry_country_id from " . TABLE_COUNTRIES . " c left join " . TABLE_ZONES . " z on (c.countries_id=z.zone_country_id) left join " . TABLE_ADDRESS_BOOK . " ab on (c.countries_id=ab.entry_country_id and z.zone_id=ab.entry_zone_id) where ab.address_book_id = '" . (int)$address_book_id ."' and z.status_id ='1' and c.status_id = '1'" . $filter_query . " and ab.customers_id = '" . (int)$customer_id . "'");
    return tep_db_num_rows($check_query)?true:false;
  }

  function tep_default_country_zone($cz_flag=0, $type=0, $country='') {
    if( !$cz_flag ) {
      $result = STORE_COUNTRY;
    } else {
      $result = STORE_ZONE;
    }
    switch($type) {
      case 1:
        $filter_query = " and pay_status_id = '1'";
        break;
      case 2:
        $filter_query = " and ship_status_id = '1'";
        break;
      default:
        $filter_query = '';
        break;
    }

    if( !$cz_flag ) {
      $check_query = tep_db_query("select countries_id as id from " . TABLE_COUNTRIES . " where status_id='1'" . $filter_query . " order by sort_id limit 1");
    } else {
      $check_query = tep_db_query("select zone_id as id from " . TABLE_ZONES . " where status_id='1'" . $filter_query . " and zone_country_id = '" . (int)$country . "' order by zone_name limit 1");
    }

    if( $check_array = tep_db_fetch_array($check_query) ) {
      $result = $check_array['id'];
    }
    return $result;
  }

//-MS- Active Countries Added EOM

//-MS- Array Invert Added
// Function array_invert came from:
// http://www.php.net/manual/en/function.array-flip.php
// By: benles at bldigital dot com
  function tep_array_invert($arr) {
    $res = array();
    foreach(array_keys($arr) as $key) {
      if (!array_key_exists($arr[$key], $res)) {
        $res[$arr[$key]] = array();
      }
      array_push($res[$arr[$key]], $key);
    }
    return $res;
  } 
//-MS- Array Invert Added EOM

  function tep_array_invert_element($input_array, $element, $filter=false) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) ) {
        $index = $value[$element];
        if( !isset($result_array[$index]) ) {
          $result_array[$index] = array();
        }
        if( $filter && isset($value[$filter] ) ) {
          $result_array[$index][] = $value[$filter];
        } else {
          $result_array[$index][] = $value;
        }
      }
    }
    return $result_array;
  } 

  function tep_array_invert_flat($input_array, $element, $filter) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) && isset($value[$filter]) ) {
        $index = $value[$element];
        $result_array[$index] = $value[$filter];
      }
    }
    return $result_array;
  } 

  function tep_array_flat($input_array, $element) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) ) {
        $result_array[] = $value[$element];
      }
    }
    return $result_array;
  } 

  function tep_array_invert_from_element($input_array, $element, $filter=false) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) ) {
        $index = $value[$element];
        if( $filter && isset($value[$filter]) ) {
          $result_array[$index] = $value[$filter];
        } else {
          $result_array[$index] = $value;
        }
      }
    }
    return $result_array;
  }

  function tep_array_merge_keys(){
    $args = func_get_args();
    $result = array();
    foreach($args as $array){
      if( is_array($array) ) {
        foreach($array as $key => $value){
          $result[$key] = $value;
        }
      }
    }
    return $result;
  }

  function tep_is_auction_product($products_id) {
    global $languages_id;

    if( !$products_id ) return true;

    $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_DESCRIPTION . " where products_name = 'auction_product_prize' and products_id = '" . (int)$products_id . "' and language_id = '" . (int)$languages_id . "' limit 1");
    $check_array = tep_db_fetch_array($check_query);
    return ($check_array['total'] > 0);
  }

  function tep_get_auction_product_id() {
    global $languages_id;
    $check_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_DESCRIPTION . " where products_name = 'auction_product_prize' and language_id = '" . (int)$languages_id . "' limit 1");
    if( tep_db_num_rows($check_query) ) {
      $check_array = tep_db_fetch_array($check_query);
    } else {
      $check_array = array('products_id' => 0);
    }
    return $check_array['products_id'];
  }


  function tep_get_dump() {
    $result = '';
    $args = func_get_args();
    if( empty($args) ) return $result;

    ob_start();
    foreach($args as $entry) {
      var_dump($entry);
    }
    $result = ob_get_contents();
    ob_end_clean();
    $result = '<pre>' . $result . '</pre>';
    return $result;
  }
?>
