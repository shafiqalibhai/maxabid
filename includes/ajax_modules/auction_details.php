<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Auction Detalis AJAX module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce   
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $auction_status_array = array(
    array('id' => 0, 'text' => 'Coming Soon'),
    array('id' => 1, 'text' => 'Live'),
    array('id' => 2, 'text' => 'Ended'),
  );

  $auction_types_array = array(
    array('id' => 0, 'text' => 'Penny Auction Format'),
    array('id' => 1, 'text' => 'Lowest Unique Bid Format'),
    array('id' => 2, 'text' => 'Normal Auction'),
  );
  
  $action = isset($_POST['action'])?tep_db_prepare_input($_POST['action']):'';
  $auction_id = isset($_POST['auction_id'])?(int)$_POST['auction_id']:0;

  if( $action == 'details' && $auction_id) {
    $auction_query = tep_db_query("select * from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auction_id . "'");
    if( !tep_db_num_rows($auction_query) ) {
      tep_exit();
    }
    $auction = tep_db_fetch_array($auction_query);
    //var_dump($auction);
    $auction_tab_query = tep_db_query("select auctions_group_name from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$auction['auctions_group_id'] . "'");
    $auction_tab = tep_db_fetch_array($auction_tab_query);
?>
    <table class="tabledata" style="background: #FFF">
      <tr>
        <th colspan="2"><?php echo sprintf('<b>LATEST INFORMATION FOR AUCTION</b>:<p></p>'); ?><?php echo sprintf(' <font color="red">%s (%s)</font>', $auction['auctions_name'], tep_date_time_short(date('Y-m-d H:i:s'))); ?></th>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Current Status:'; ?></td>
        <td class="">
        <?php       
            $remaining = -1;
            $restart = false;
            $pause = tep_auction_check_pause($auction, $restart, $remaining);
            if( $pause ) {
              echo $auction_status_array[$auction['status_id']]['text'].' (Currently Paused)';
            }else {
              echo $auction_status_array[$auction['status_id']]['text'];
            }
         
        ?>
        </td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Format/Type:'; ?></td>
        <td class=""><?php echo $auction_types_array[$auction['auctions_type']]['text']; ?></td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Tab Location:'; ?></td>
        <td class=""><?php echo $auction_tab['auctions_group_name']; ?></td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Bids Deducted Per Submission:'; ?></td>
        <td class=""><?php echo $auction['bid_step']; ?></td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Are Overbids Permitted?:'; ?></td>
        <td class=""><?php echo $auction['auctions_overbid']?'Yes':'No'; ?></td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Minimum Bid Allowed To Be Submitted:'; ?></td>
        <td class="">
<?php 
    if( $auction['start_price'] > 0 ) {
      echo $currencies->format($auction['start_price']);
    }  else {
      echo $currencies->format($auction['bid_step']/100);
    }
?>
        </td>
      </tr>
      <?php 
      if( $auction['cap_price'] > 0 ) {
      ?>
      <tr>
        <td class="iconinfopopup"><?php echo 'Maximum Bid Allowed To Be Submitted:'; ?></td>
        <td class="">
<?php 
    
      echo $currencies->format($auction['cap_price']);
?>
        </td>
      </tr>#
      <?php } ?>
      <tr>
        <td class="iconinfopopup"><?php echo 'Administration & Post/Packaging Fees:'; ?></td>
        <td class="">
<?php 
    if( $auction['shipping_cost'] > 0 ) {
      echo $currencies->format($auction['shipping_cost']);
    }  else {
      echo '<font color="red"><b>FREE</b></font>';
    }
?>
        </td>
      </tr>
      <?php
       $start = tep_auction_check_start($auction);
    if( strlen($start) ) {
      ?>
      <tr>
        <td class="iconinfopopup"><?php echo 'Start Date/Time:'; ?></td>
        <td class="">
<?php
    
      echo $start;
?>
        </td>
      </tr>
      
<?php   }
if( $auction['start_pause'] && $auction['start_pause'] != '00:00:00' && $auction['end_pause'] && $auction['end_pause'] != '00:00:00') {
   ?>
      <tr>
        <td class="iconinfopopup"><?php echo 'Pause Duration:'; ?></td>
        <td class="">
<?php   
          echo  'Bewteen ' . $auction['start_pause'] . 'hrs and ' . $auction['end_pause']. 'hrs' ;
?>
        </td>
      </tr>
      <?php
        }
       ?>
      <tr>
        <td class="iconinfopopup"><?php echo 'How The Auction Ends:'; ?></td>
        <td class="">
<?php
    $countdown_bids = false;
    if( $auction['countdown_bids']) {
      $check_query = tep_db_query("select bid_count, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_id . "'");
      if( tep_db_num_rows($check_query) ) {
        $countdown_bids = true;
      }
    }
    $check_query = tep_db_query("select bid_count, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_id . "'");
    if( tep_db_num_rows($check_query) ) {
       $bid_count = tep_db_fetch_array($check_query);
    } 
    
    if( $countdown_bids ) {
      echo 'Ends when timer reaches zero <font color="red"><b>OR </font></b>';
      $configuration_query = tep_db_query("select configuration_id, configuration_title, configuration_value, use_function from " . TABLE_CONFIGURATION . " where configuration_group_id = '26' and configuration_id = '374' order by sort_order");
      while ($configuration = tep_db_fetch_array($configuration_query)) {
        $cfgValue = $configuration['configuration_value'];
      }
      echo tep_date_long($auction['live_off']) . 'hrs, which ever occurs first';
    } else if( $auction['date_expire'] && $auction['date_expire'] != '0000-00-00 00:00:00' && $auction['auctions_type'] != '0') {
        echo 'Ends when ' . ($auction['max_bids'] - $bid_count['bid_count'] . ' bids have been submitted <font color="red"><b>OR </font></b> on ');
        echo tep_date_long($auction['date_expire']) . 'hrs, whichever occurs first';
    } elseif ($auction['auctions_type'] == '0') {    
      echo 'Ends when the timer reaches Zero';
      if($auction['countdown_bids']  && $auction['live_off']) {
        echo ' <font color="red"><b>OR </font></b> on ' . tep_date_long($auction['date_expire']) . 'hrs, whichever occurs first';
      }
      elseif ($auction['bid_step'] > 1) {
         echo ' <font color="red"><b>OR </font></b> on ' . tep_date_long($auction['date_expire']) . 'hrs, whichever occurs first';
      }
    } elseif( $auction['status_id'] == 1 && $auction['auto_timer'] && $auction['live_off'] != '0000-00-00 00:00:00') {
      //echo tep_date_time_short($auction['live_off']);
      echo 'Ends when ' . ($auction['max_bids'] - $bid_count['bid_count'] . ' bids have been submitted <font color="red"><b>OR </font></b>');
      echo tep_date_long($auction['live_off']) . 'hrs, whichever occurs first'  ;
    }
    else {
      echo 'Ends when ' . ($auction['max_bids'] - $bid_count['bid_count'] . ' bids have been submitted');
    }
?>
        </td>
      </tr>
      <?php
      $ac_bids = explode(',', $auction['countdown_bids']);
              if(($auction['countdown_bids'] >= 1 || $ac_bids[0] >1) && $auction['status_id'] != 2) {
      ?>
       <tr>
        <td class="iconinfopopup"><?php echo "Type of Timer:"; ?></td>
        <td class=""><?php 			if($auction['countdown_bids'] == 1) {				echo '1st Bid Timer';			}			elseif($auction['countdown_bids'] > 1) {				echo "'X' Bid Timer";			}		?></td>
      </tr>	  <?php 	  if($auction['countdown_bids'] == 1) {	  ?>       <tr>
        <td class="iconinfopopup"><?php echo "Triggered By:"; ?></td>
        <td class=""><?php			if($auction['countdown_bids'] == 1) {			}			elseif($auction['countdown_bids'] > 1) {				$stats_query = tep_db_query("select customers_id from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' limit ". (int)$auction['countdown_bids'] ."");				$stats_array = tep_db_fetch_array($stats_query);								$stats_query = tep_db_query("select customers_nickname from " . TABLE_CUSTOMERS . " where customers_id like " . $stats_array['customers_id']);				$stats_array = tep_db_fetch_array($stats_query);				echo $stats_array['customers_nickname'];			}		?></td>
      </tr>	  <?php	  }		$stats_query = tep_db_query("select bid_price, count(*) as count from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' limit ". (int)$auction['countdown_bids'] ."");		$stats_array = tep_db_fetch_array($stats_query);		if($stats_array['count'] >= $auction['countdown_bids'])		{	  ?>       <tr>
        <td class="iconinfopopup"><?php echo "Triggered At Amount:"; ?></td>
        <td class=""><?php 			if($auction['countdown_bids'] == 1) {			}			elseif($auction['countdown_bids'] > 1) {						echo '&pound;' . $stats_array['bid_price'];			}		?></td>
      </tr>
      
      <?php		}
       }
      ?>
      <?php if(!empty($auction['end_price']) && $auction['end_price'] > 0.1)   {
      ?>
       <tr>
        <td class="iconinfopopup"><?php echo 'Timer Will Activatate Before Bids Reach:'; ?></td>
        <td class=""><?php 
        //@TODO configure auto currency
        echo '&pound;'.$auction['end_price']; ?></td>
      </tr>
      <?php } ?>
      <!--
      <tr>
        <td class="iconinfopopup"><?php echo 'X Bid Timer Trigger Prize(s):'; ?></td>
        <td class=""><?php echo $auction['auctions_overbid']?'Yes':'No'; ?></td>
      </tr>
      <tr>
        <td class="iconinfopopup"><?php echo 'Timer Triggred By:'; ?></td>
        <td class=""><?php echo $auction['auctions_overbid']?'Yes':'No'; ?></td>
      </tr>
      -->
      <tr>
        <th colspan="2"><?php echo '</br><b>ITEM(S) ON AUCTION:</b>'; ?></th>
      </tr>
      <tr class="moduleRowSelected">
        <td colspan="2" class="heavy"><?php echo '1. <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction['products_id']) . '">' . tep_get_products_name($auction['products_id']) . '</a>'; ?></td>
      </tr>
<?php
    $tier_array = array();
    $tier_query_raw = "select products_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_id . "' order by sort_id";
    tep_query_to_array($tier_query_raw, $tier_array);
    for( $i=0, $j=count($tier_array); $i<$j; $i++) {
?>
      <tr class="moduleRowSelected">
        <td colspan="2" class="heavy"><?php echo ($i+2) . '. <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tier_array[$i]['products_id']) . '">' . tep_get_products_name($tier_array[$i]['products_id']) . '</a>'; ?></td>
      </tr>
<?php
    }
    if( tep_session_is_registered('customer_id') && $auction['status_id'] == 1 ) {
?>
      <tr>
        <th colspan="2"><?php echo sprintf('Your participation in %s', $auction['auctions_name']); ?></th>
      </tr>
      <tr>
        <td><?php echo 'Your Current Bids'; ?></td>
<?php
      $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' and customers_id = '" . (int)$customer_id . "'");
      $stats_array = tep_db_fetch_array($stats_query);
?>
        <td><?php echo $stats_array['total']; ?></td>
      </tr>
<?php
      if( $stats_array['total']) {
?>
      <tr>
        <th colspan="2"><?php echo sprintf('Your Bid Details (displaying up to %s Bids)', 10); ?></th>
      </tr>
<?php
        $bids_array = array();
        $bids_query = "select bid_price, date_added from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' and customers_id = '" . (int)$customer_id . "' order by date_added desc limit 10";
        tep_query_to_array($bids_query, $bids_array);
        for( $i=0, $j=count($bids_array); $i<$j; $i++ ) {
?>
      <tr>
        <td><?php echo tep_date_time_short($bids_array[$i]['date_added']); ?></td>
        <td class="heavy"><?php echo $bids_array[$i]['bid_price']; ?></td>
      </tr>
<?php
        }
      }
      $bids_array = array();
      $bids_query = "select customers_id, bid_price, date_added from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' order by date_added desc limit 10";
      tep_query_to_array($bids_query, $bids_array);
      $j = count($bids_array);
      if( $j ) {
?>
      <tr>
        <th colspan="2"><?php echo sprintf('Last Bids Entered by Players (displaying up to %s Bids)', 10); ?></th>
      </tr>
<?php
        $customers_array = array();
        for( $i=0; $i<$j; $i++ ) {
?>
      <tr>
        <td><?php echo tep_date_time_short($bids_array[$i]['date_added']); ?></td>
        <td class="heavy">
<?php
          if( $auction['auctions_type'] == 0 || $customer_id == $bids_array[$i]['customers_id'] ) {
            echo $bids_array[$i]['bid_price']; 
          } else {
            if( !isset($customers_array[$bids_array[$i]['customers_id']]) ) {
              $customers_array[$bids_array[$i]['customers_id']] = tep_auction_customer_nickname($bids_array[$i]['customers_id']);
            }
            echo '<span class="mark_red">' . $customers_array[$bids_array[$i]['customers_id']] . '</span>';
          }
?>
        </td>
      </tr>
<?php
        }
      }
    } elseif ( tep_session_is_registered('customer_id') && $auction['status_id'] == 2 ) {
?>
      <tr>
        <th colspan="2"><?php echo sprintf('Details of ended auction %s', $auction['auctions_name']); ?></th>
      </tr>
<?php
      $history_array = array();
      $history_query = "select customers_id, customers_nickname, auctions_price, products_name, winner_bids, bid_count, started, completed from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_id . "'";
      tep_query_to_array($history_query, $history_array);
      $self_bid = false;

      for($i=0, $j=count($history_array); $i<$j; $i++) {
        $history = $history_array[$i];
        if( empty($history['customers_nickname']) ) {
          $history['customers_nickname'] = 'No Winner';
        }
        if( empty($history['products_name']) ) {
          $history['products_name'] = TEXT_NA;
        }
        if($history['customers_id'] == $customer_id && !$self_bid) {
?>
      <tr class="moduleRowSelected">
        <td class="heavy mark_red"><?php echo 'Your Bids as a Winner'; ?></td>
        <td class="heavy"><?php echo $history['winner_bids']; ?></td>
      </tr>
<?php
          $self_bid = true;
        }
?>
<?php
        if( !$i ) {
?>
      <tr>
        <td><?php echo 'Final Price'; ?></td>
        <td><?php echo $currencies->format($history['auctions_price']); ?></td>
      </tr>
      <tr>
        <td><?php echo 'Total Bids Placed'; ?></td>
        <td><?php echo $history['bid_count']; ?></td>
      </tr>
      <tr>
        <td><?php echo 'Auction Completed'; ?></td>
        <td><?php echo tep_date_time_short($history['completed']); ?></td>
      </tr>
      <tr>
        <th colspan="2"><?php echo 'Winners & Prizes'; ?></td>
      </tr>
<?php
        }
?>
<?php
        if($history['customers_id'] == $customer_id) {
?>
      <tr class="moduleRowSelected">
        <td class="heavy"><?php echo ($i+1) . '. You won'; ?></td>
        <td class="heavy mark_red"><?php echo $history['products_name']; ?></td>
      </tr>
<?php
        } else {
?>
      <tr>
        <td class="heavy"><?php echo ($i+1) . '. <span class="mark_blue">' . $history['customers_nickname'] . '</span>'; ?></td>
        <td class="heavy"><?php echo $history['products_name']; ?></td>
      </tr>
<?php
        }
      }
    }
?>
    </table>
<?php
  }
  tep_exit();
?>

