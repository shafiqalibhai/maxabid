/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: Stickers for auction messages
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var sticky_auctions = {

  mainURL:         false,
  baseURL:         false,
  detailsURL:      false,
  timer:           false,
  busy_timer:      0,
  period:          1000,
//  period:          4000,
  busy:            false,
  busy_poll:       false,
  busy_poll_timer: 0,
  inputs_array:    {},
  backup_array:    {},
  post_inquiry:    false,
  failures:        0,
  keep_ended:      0,
  //isActive:        true,
  critical_end:    false,

  set_bid: function(index, bid_value) {
    if( sticky_auctions.critical_end == true ) {
      return;
    }

    if( sticky_auctions.busy == true ) {
      $.jGrowl('<b class="mark_red">Queue is busy, please retry</b>');

      sticky_auctions.reset_timers();
      sticky_auctions.busy_timer = setTimeout(sticky_auctions.wait_busy, 2*sticky_auctions.period );
      return;
    }

    sticky_auctions.busy = true;
    var sep = (sticky_auctions.baseURL.indexOf('?') == -1 ? '?' : '&');

    var href = sticky_auctions.baseURL+sep+'action=set_auction';
    var parameters = 'auction_id='+index+'&bid_value='+bid_value;
    //alert(href);
    $.ajax({
      type: 'POST',
      url: href,
      data: parameters,
      dataType: 'html',
      complete: function(msg){
      },
      success: function(msg) {
        if( !msg.length ) {
          sticky_auctions.busy = false;
          return;
        }

        msg = $.base64Decode(msg);
        var data_array = general.unserialize(msg);

        anim1 = parseInt((sticky_auctions.period-50)/2);
        anim2 = parseInt((sticky_auctions.period-50)/2);

        for (var tag in data_array) {
          switch(tag) {
            case 'bids_notice':
              if( $('#bids_notice').length ) {
                $('#bids_notice').html(data_array[tag]);
                $('#bids_notice').parent().animate({'background-color': '#0000FF'}, 1200).animate({'background-color': '#FF0000'}, 800);
              }
              break;
            default:
              break;
          }
          for( var field in data_array[tag] ) {
            //alert(field);
            switch(field) {
              case 'growl':
                var tmp_array = data_array[tag][field];
                for( var i in tmp_array ) {
                  $.jGrowl(tmp_array[i]);
                }
                break;
              case 'note':
                if( $('#bid_note'+tag).length ) {
                  $('#bid_note'+tag).html(data_array[tag][field]);
                  //$('.bid_note'+tag).html(data_array[tag][field]);
                  //alert(data_array[tag][field]);
                  //$('#bid_note' + tag).animate({'background-color': '#00FF00'}, 1200).animate({'background-color': '#FFFFFF'}, 800);
                }
                break;
              case 'bg_flash':
                if( $('#bg_flash_' + tag).length ) {
                  $('#bg_flash_' + tag).animate({'background-color': '#00FF00'}, anim1).animate({'background-color': '#FFFFFF'}, anim2);
                }
                break;
              case 'input':
                if( $('#auction_input_'+tag).length ) {
                  $('#auction_input_'+tag).val(data_array[tag][field]);
                }
                break;
              case 'bid_display':              
                if( $('#auction_display_'+tag).length ) {
                  //$('#bid_note' + tag).append(' ('+data_array[tag][field]+')');
                  var count_bid = Math.round(((1*data_array[tag][field])+(1*0.01))*100)/100;
                  $('#auction_display_' + tag).html(count_bid);
                }
                break;
              case 'backup':
                if( $('#backup_bid_'+tag).length ) {
                  $('#backup_bid_'+tag).val(data_array[tag][field]);
                }
                break;
              case 'bids_added':
                if( $('#bid_added'+tag).length ) {
                  $('#bid_added'+tag).html(data_array[tag][field]);
                }
                break;
              case 'bids_left':
              //alert(data_array[tag][field]);
                if( $('#bid_left'+tag).length ) {
                  $('#bid_left'+tag).html(data_array[tag][field]);
                }
                break;
              case 'latest_update':
              //alert(data_array[tag][field]);
                //if( $('#bid_left'+tag).length ) {
                  $('#latest_update').html(data_array[tag][field]);
                //}
                break;
              case 'background':
                if( $('#auction_block_'+tag).length ) {
                  $('#auction_block_' + tag).animate({'background-color': data_array[tag][field]}, 1800).animate({'background-color': '#FFFFFF'}, 400);
                }
                break;
              default:
                break;
            }
          }
        }
        sticky_auctions.release();
      }
    });
  },

  wait_busy: function() {
    clearTimeout(sticky_auctions.busy_timer);
    sticky_auctions.busy_timer = 0;
  },

  clear_auction: function(tag) {
    if( $('#auction_item_'+tag).length ) {
      $('#auction_item_'+tag).empty().remove();
      sticky_auctions.get_auctions();
    }
  },

  inquiry: function() {

    var sep = (sticky_auctions.baseURL.indexOf('?') == -1 ? '?' : '&');
    sticky_auctions.get_parameters = sep+'action=browse_auctions';

    sticky_auctions.timer = function() {
      var href = sticky_auctions.baseURL+sticky_auctions.get_parameters;

      if( sticky_auctions.critical_end == true ) {
        return;
      }

      if( !sticky_auctions.busy_poll ) {
        sticky_auctions.busy_poll = true;

        sticky_auctions.post_inquiry = '';
        for( var tag in sticky_auctions.backup_array ) {
          sticky_auctions.post_inquiry += 'auction_bid[' + tag + ']=' + sticky_auctions.backup_array[tag] + '&';
        }
        sticky_auctions.post_inquiry = sticky_auctions.post_inquiry.substring(0,sticky_auctions.post_inquiry.lastIndexOf('&'));
        var parameters = sticky_auctions.post_inquiry;
        sticky_auctions.send_request(href, parameters);
      } else {
        sticky_auctions.busy_poll_timer = setTimeout(sticky_auctions.wait_poll_busy, 2*sticky_auctions.period );
      }
      return setTimeout(sticky_auctions.timer, sticky_auctions.period );
    }
    setTimeout(sticky_auctions.timer, sticky_auctions.period );
  },

  send_request: function(href, parameters) {

    $.ajax({
      type: 'POST',
      url: href,
      data: parameters,
      dataType: 'html',
      complete: function(msg){
      },
      success: function(msg) {

        if( !msg.length ) {
          sticky_auctions.busy_poll = false;
          return;
        }

        msg = $.base64Decode(msg);
        var data_array = general.unserialize(msg);
        var new_auctions = 0;
        var anim1, anim2;
        anim1 = parseInt((sticky_auctions.period-50)/2);
        anim2 = parseInt((sticky_auctions.period-50)/2);

        for (var tag in data_array) {

          if( tag == 'critical') {
            sticky_auctions.critical_end = true;

            var title = 'Notice';
            var msg = '<div style="background: #FFF">';
            var tmp_array = data_array[tag];
            for( var i in tmp_array ) {
              msg += tmp_array[i] + '<br />';
            }
            msg += '</div>'

            $.fancybox(msg, {
              'type'          : 'html',
              'autoScale'     : true,
              'transitionIn'  : 'elastic',
              'transitionOut' : 'elastic',
              'title'         : title,
              'onClosed'      : function() {
	            $(location).attr('href', sticky_auctions.mainURL);
	          }
            });
            return;
          }

          for( var field in data_array[tag] ) {
            switch(field) {
              case 'growl':
                var tmp_array = data_array[tag][field];
                for( var i in tmp_array ) {
                  $.jGrowl(tmp_array[i]);
                }
                break;
              case 'note':
                if( $('#bid_note' + tag).length ) {
                  $('#bid_note' + tag).html(data_array[tag][field]);
                  //$('#bid_note' + tag).animate({'background-color': '#00FF00'}, anim1).animate({'background-color': '#FFFFFF'}, anim2);
                }
                break;
              case 'note2':
                if( $('#bid_note2_' + tag).length ) {
                  $('#bid_note2_' + tag).html(data_array[tag][field]);
                  var check_bg = $(data_array[tag][field]).hasClass('nobg');
                  if( !check_bg && data_array[tag][field].length ) {
                    //$('#bid_note2_' + tag).animate({'background-color': '#99FF99'}, anim1).animate({'background-color': '#FFFFFF'}, anim2);
                  }
                }
                break;
              case 'bg_flash':
                if( $('#bg_flash_' + tag).length ) {
                  $('#bg_flash_' + tag).animate({'background-color': '#00FF00'}, anim1).animate({'background-color': '#FFFFFF'}, anim2);
                }
                break;
              case 'input':
                if( $('#auction_input_'+tag).length ) {
                  $('#auction_input_'+tag).val(data_array[tag][field]);
                }
                break;
              case 'bid_display':
                if( $('#auction_display_'+tag).length ) {
                  $('#auction_display_'+tag).html(data_array[tag][field]);
                }
                break;
              case 'bids_added':
                if( $('#bid_added'+tag).length ) {
                  $('#bid_added'+tag).html(data_array[tag][field]);
                }
                break;
              case 'bids_left':
                if( $('#bid_left'+tag).length ) {
                  $('#bid_left'+tag).html(data_array[tag][field]);
                }
                break;
              case 'backup':
                if( $('#backup_bid_'+tag).length ) {
                  $('#backup_bid_'+tag).val(data_array[tag][field]);
                }
                break;
              case 'background':
                if( $('#auction_block_' + tag).length ) {
                  $('#auction_block_' + tag).animate({'background-color': data_array[tag][field]}, anim1).animate({'background-color': '#FFFFFF'}, anim2);
                }
                break;
              case 'ended':
                if( !sticky_auctions.keep_ended && $('#auction_item_'+tag).length ) {
                  var end_tag = tag;
                  $('#auction_item_'+end_tag).fadeOut(7*sticky_auctions.period);
                  setTimeout( function() {
                    sticky_auctions.clear_auction(end_tag);
                  }, 8*sticky_auctions.period);
                } else if( sticky_auctions.keep_ended==2 && $('#auction_item_'+tag).length ) {
                  sticky_auctions.keep_ended==1;
                  var end_tag = tag;

                  setTimeout( function() {
                    $(location).attr('href', $('#auction_item_'+tag).attr('data-link') );
                  }, 8*sticky_auctions.period);

                  return;
                }
                break;
              case 'new':
                new_auctions++;
                break;
              default:
                break;
            }
          }
        }
        if( new_auctions ) {
          $(location).attr('href', sticky_auctions.mainURL);
        }
        sticky_auctions.release_poll();
      }
    });
  },

  wait_poll_busy: function() {
    clearTimeout(sticky_auctions.busy_poll_timer);
    sticky_auctions.busy_poll = false;
    sticky_auctions.busy_poll_timer = 0;
  },

  get_auctions: function() {
    sticky_auctions.backup_array = {};

    $('a.sticky_bid').each(function(count) {
      var index = $(this).attr('attr');
      sticky_auctions.backup_array[index] = $('#backup_bid_'+index).val();
    });
  },

  reset_timers: function() {
    sticky_auctions.failures++;

    if( sticky_auctions.failures > 5 ) {
      clearTimeout(sticky_auctions.timer);
      sticky_auctions.timer = false;
      sticky_auctions.release();
      sticky_auctions.inquiry();
    }
  },

  release: function() {
    sticky_auctions.get_auctions();
    sticky_auctions.busy = false;
    sticky_auctions.failures = 0;
  },

  release_poll: function() {
    sticky_auctions.get_auctions();
    sticky_auctions.busy_poll = false;
  },

  initialize: function() {

    $(".fader_info").hover(
      function() {
        $(this).stop().animate({"opacity": "0.4"}, "slow");
      },
      function() {
        $(this).stop().animate({"opacity": "1"}, "slow");
      }
    );

    if( $('#auction_group_block').length ) {
      var sep = (sticky_auctions.baseURL.indexOf('?') == -1 ? '?' : '&');
      var key = $('#auction_group_block').attr('attr');
      sticky_auctions.baseURL += sep+'auctions_group_id='+key;
      sticky_auctions.mainURL += sep+'auctions_group_id='+key;
    } else if( $('#auction_info_block').length ) {
      var sep = (sticky_auctions.baseURL.indexOf('?') == -1 ? '?' : '&');
      var key = $('#auction_info_block').attr('data-group');
      sticky_auctions.baseURL += sep+'auctions_group_id='+key;
      sticky_auctions.mainURL += sep+'auctions_group_id='+key;
    }

    var sep = (sticky_auctions.baseURL.indexOf('?') == -1 ? '?' : '&');
    var href = sticky_auctions.baseURL+sep+'action=initialize';

    $.ajax({
      type: 'GET',
      url: href,
      data: '',
      async: false,
      dataType: 'html',
      complete: function(msg){},
      success: function(msg) {
        if( !msg.length ) {
          return;
        }

        msg = $.base64Decode(msg);
        var data_array = general.unserialize(msg);

        for (var tag in data_array) {
          sticky_auctions[tag] = data_array[tag];
        }
      }
    });
  },

  get_details: function($sel) {

    var index = $sel.attr('attr');
    var href = sticky_auctions.detailsURL;
    var parameters = 'auction_id='+index+'&action=details&module=auction_details';
    var title = $sel.attr('title');
    $.fancybox.showActivity();

    $.ajax({
      type: 'POST',
      url: href,
      data: parameters,
      dataType: 'html',
      complete: function(msg){},
      success: function(msg) {
        $.fancybox(msg, {
         'orig'          : $sel,
         'autoScale'     : true,
         'transitionIn'  : 'elastic',
         'transitionOut' : 'elastic',
         'title'         : title
        });
      }
    });
/*
    $.fancybox({
      'type'          : 'ajax',
      'href'          : href,
      'data'          : parameters,
      'autoScale'     : false,
      'transitionIn'  : 'elastic',
      'transitionOut' : 'elastic',
      'title'         : title
    });
*/
  },

  launch: function() {
    sticky_auctions.initialize();

    $.jGrowl.defaults.closer = false;
    $.jGrowl.defaults.position = 'bottom-left';

    //$('a.sticky_bid').each(function(count) {
    //  var index = $(this).attr('attr');
    //  sticky_auctions.inputs_array[index] = $('#auction_input_'+index).val();
    //});

    $("input:text['bid_inputs']").keypress( function(e) {

      if (e.keyCode == 13) {
        if( sticky_auctions.busy_timer ) return;

        var str = $(this).attr('id');

        var index = str.substring(str.lastIndexOf('_')+1);
        var bid = $('#auction_input_'+index).val();
        sticky_auctions.set_bid(index, bid);
      }
    });

    $("a.sticky_bid").click(function(e) {
      if( sticky_auctions.busy_timer ) return false;

      var href = $(this).attr('href');
      if( href != '#' ) {
        return true;
      }

      var index = $(this).attr('attr');

      var bid = $('#auction_input_'+index).val();
      sticky_auctions.set_bid(index, bid);
      return false;

    });

    $("a.auction_details").click(function(e) {
      e.preventDefault();
      sticky_auctions.get_details($(this));
    });

    //$(window).focus(function() { sticky_auctions.isActive = true; });
    //$(window).blur(function() { sticky_auctions.isActive = false; });

    sticky_auctions.get_auctions();
    sticky_auctions.inquiry();
  }
}

$("a.tbutton2").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false
	});