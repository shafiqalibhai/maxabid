/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: General Support Function for the I-Metrics CMS
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var general = {

  serial_array:        {},

  unserialize: function(data) {

    // Takes a string representation of variable and recreates it  
    // 
    // version: 1107.2516
    // discuss at: http://phpjs.org/functions/unserialize    // +     original by: Arpad Ray (mailto:arpad@php.net)
    // +     improved by: Pedro Tainha (http://www.pedrotainha.com)
    // +     bugfixed by: dptr1988
    // +      revised by: d3x
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    // +        input by: Brett Zamir (http://brett-zamir.me)
    // +     improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: Chris
    // +     improved by: James
    // +        input by: Martin (http://www.erlenwiese.de/)    // +     bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +     improved by: Le Torbi
    // +     input by: kilops
    // +     bugfixed by: Brett Zamir (http://brett-zamir.me)
    // -      depends on: utf8_decode    // %            note: We feel the main purpose of this function should be to ease the transport of data between php & js
    // %            note: Aiming for PHP-compatibility, we have to translate objects to arrays
    // *       example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
    // *       returns 1: ['Kevin', 'van', 'Zonneveld']
    // *       example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');    // *       returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}
    var that = this;
    var utf8Overhead = function (chr) {
      // http://phpjs.org/functions/unserialize:571#comment_95906
      var code = chr.charCodeAt(0);        
      if (code < 0x0080) {
        return 0;
      }
      if (code < 0x0800) {
        return 1;        
      }
      return 2;
    };

    var error = function (type, msg, filename, line) {
        throw new that.window[type](msg, filename, line);
    };

    var read_until = function (data, offset, stopchr) {
      var buf = [];        
      var chr = data.slice(offset, offset + 1);
      var i = 2;
      while (chr != stopchr) {
        if ((i + offset) > data.length) {
          error('Error', 'Invalid');
        }
        buf.push(chr);
        chr = data.slice(offset + (i - 1), offset + i);
        i += 1;
      }
      return [buf.length, buf.join('')];
    };

    var read_chrs = function (data, offset, length) {
      var buf;
      buf = [];
      for (var i = 0; i < length; i++) {
        var chr = data.slice(offset + (i - 1), offset + i);
        buf.push(chr);
        length -= utf8Overhead(chr);
      }
      return [buf.length, buf.join('')];
    };
    var _unserialize = function (data, offset) {
      var readdata;
      var readData;
      var chrs = 0;
      var ccount;
      var stringlength;
      var keyandchrs;        
      var keys;

      if (!offset) {
        offset = 0;
      }        
      var dtype = (data.slice(offset, offset + 1)).toLowerCase();

      var dataoffset = offset + 2;
      var typeconvert = function (x) {
        return x;        
      };

      switch (dtype) {
        case 'i':
          typeconvert = function (x) {
            return parseInt(x, 10);
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];            
          dataoffset += chrs + 1;
          break;
        case 'b':
          typeconvert = function (x) {
            return parseInt(x, 10) !== 0;            
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 1;            
          break;
        case 'd':
          typeconvert = function (x) {
            return parseFloat(x);
          };
          readData = read_until(data, dataoffset, ';');
          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 1;
          break;        case 'n':
          readdata = null;
          break;
        case 's':

          ccount = read_until(data, dataoffset, ':');
          chrs = ccount[0];
          stringlength = ccount[1];
          dataoffset += chrs + 2;

          readData = read_chrs(data, dataoffset + 1, parseInt(stringlength, 10));            

          chrs = readData[0];
          readdata = readData[1];
          dataoffset += chrs + 2;
          if (chrs != parseInt(stringlength, 10) && chrs != readdata.length) {
            error('SyntaxError', 'String length mismatch');            
          }
//alert($.dump(readdata));
          // Length was calculated on an utf-8 encoded string
          // so wait with decoding
          //readdata = that.utf8_decode(readdata);

          break;
        case 'a':
          readdata = {};

          keyandchrs = read_until(data, dataoffset, ':');            
          chrs = keyandchrs[0];
          keys = keyandchrs[1];
          dataoffset += chrs + 2;

          for (var i = 0; i < parseInt(keys, 10); i++) {                
            var kprops = _unserialize(data, dataoffset);
            var kchrs = kprops[1];
            var key = kprops[2];
            dataoffset += kchrs;
            var vprops = _unserialize(data, dataoffset);
            var vchrs = vprops[1];
            var value = vprops[2];
            dataoffset += vchrs;
            readdata[key] = value;
          }

          dataoffset += 1;
          break;        
        default:
          error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
          break;
      }
      return [dtype, dataoffset - offset, typeconvert(readdata)];    
    };

    return _unserialize((data + ''), 0)[2];
  },

  unserialize2: function(data) {
    general.serial_array = {};
    general.parse_data(data);
  },

  parse_data: function(data) {
    var type;
    type = data.indexOf(':');
//alert(type);
    switch(type) {
      case 'a':
        break;
      default:
        break;
    }
  },

  launch: function() {
    $('a.bsubmit').live("click", function(e) {
      e.preventDefault();
      var name = $(this).attr('name');
      var $parent = $(this).closest("form");

      if( $parent.length ) {
        if( typeof name != 'undefined' && name.length ) {
          var action = $parent.attr('action');
          var sep = '';
          if( action.length ) {
            sep = (action.indexOf('?') == -1 ? '?' : '&');
          }
          action += sep + 'action=' + name;
          $parent.attr('action', action);
        }
        $parent.submit();
      }
    });

    $('.row_link').click(function(e) {
      if(e.target.nodeName != 'TD'){
        return true;
      }
      $(location).attr('href',$(this).attr('href'));
    });
  }
}
