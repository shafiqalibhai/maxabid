<?php
/*
  $Id: database_tables.php,v 1.1 2003/03/14 02:10:58 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// define the database table names used in the project
  define('TABLE_ADDRESS_BOOK', 'address_book');
  define('TABLE_ADDRESS_FORMAT', 'address_format');
  define('TABLE_BANNERS', 'banners');
  define('TABLE_BANNERS_HISTORY', 'banners_history');
  define('TABLE_CATEGORIES', 'categories');
  define('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
  define('TABLE_CONFIGURATION', 'configuration');
  define('TABLE_CONFIGURATION_GROUP', 'configuration_group');
  define('TABLE_COUNTER', 'counter');
  define('TABLE_COUNTER_HISTORY', 'counter_history');
  define('TABLE_COUNTRIES', 'countries');
  define('TABLE_CURRENCIES', 'currencies');
  define('TABLE_CUSTOMERS', 'customers');
  define('TABLE_CUSTOMERS_BASKET', 'customers_basket');
  define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES', 'customers_basket_attributes');
  define('TABLE_CUSTOMERS_INFO', 'customers_info');
  define('TABLE_LANGUAGES', 'languages');
  define('TABLE_MANUFACTURERS', 'manufacturers');
  define('TABLE_MANUFACTURERS_INFO', 'manufacturers_info');
  define('TABLE_ORDERS', 'orders');
  define('TABLE_ORDERS_PRODUCTS', 'orders_products');
  define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');
  define('TABLE_ORDERS_PRODUCTS_DOWNLOAD', 'orders_products_download');
  define('TABLE_ORDERS_STATUS', 'orders_status');
  define('TABLE_ORDERS_STATUS_HISTORY', 'orders_status_history');
  define('TABLE_ORDERS_TOTAL', 'orders_total');
  define('TABLE_PRODUCTS', 'products');
  define('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
  define('TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD', 'products_attributes_download');
  define('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
  define('TABLE_PRODUCTS_NOTIFICATIONS', 'products_notifications');
  define('TABLE_PRODUCTS_OPTIONS', 'products_options');
  define('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
  define('TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS', 'products_options_values_to_products_options');
  define('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
  define('TABLE_REVIEWS', 'reviews');
  define('TABLE_REVIEWS_DESCRIPTION', 'reviews_description');
  define('TABLE_SESSIONS', 'sessions');
  define('TABLE_SPECIALS', 'specials');
  define('TABLE_TAX_CLASS', 'tax_class');
  define('TABLE_TAX_RATES', 'tax_rates');
  define('TABLE_GEO_ZONES', 'geo_zones');
  define('TABLE_ZONES_TO_GEO_ZONES', 'zones_to_geo_zones');
  define('TABLE_WHOS_ONLINE', 'whos_online');
  define('TABLE_ZONES', 'zones');

//-MS- Abstract Zones Support Added
  define('TABLE_ABSTRACT_ZONES', 'abstract_zones');
  define('TABLE_ABSTRACT_TYPES', 'abstract_types');
  define('TABLE_PRODUCTS_ZONES_TO_CATEGORIES','products_zones_to_categories');
  define('TABLE_ACCESSORIES_ZONES_TO_CATEGORIES','accessories_zones_to_categories');
  define('TABLE_CONTROL_ZONES_TO_CATEGORIES','control_zones_to_categories');

  define('TABLE_GTEXT', 'gtext');
  define('TABLE_GTEXT_TO_PRODUCTS', 'gtext_to_products');
  define('TABLE_GTEXT_TO_DISPLAY', 'gtext_to_display');

  define('TABLE_ATTRIBUTES_ZONES_TO_OPTIONS','attributes_zones_to_options');
  define('TABLE_ATTRIBUTES_ZONES_TO_CATEGORIES','attributes_zones_to_categories');

  define('TABLE_WEAK_ZONES_TO_OPTIONS','weak_zones_to_options');
  define('TABLE_WEAK_ZONES_TO_CATEGORIES','weak_zones_to_categories');
//-MS- Abstract Zones Support Added EOM

//-MS- Email Templates added
  define('TABLE_EMAIL_TEMPLATES', 'email_templates');
//-MS- Email Templates added EOM

//-MS- Numeric Ranges
define('TABLE_NUMERIC_RANGES', 'numeric_ranges');
//-MS- Numeric Ranges EOM

//-MS- SEO-G Added
  define('TABLE_SEO_URL', 'seo_url');
  define('TABLE_SEO_CACHE', 'seo_cache');
  define('TABLE_SEO_REDIRECT', 'seo_redirect');
  define('TABLE_SEO_EXCLUDE', 'seo_exclude');
  define('TABLE_SEO_FREQUENCY', 'seo_frequency');
  define('TABLE_SEO_TYPES', 'seo_types');
  define('TABLE_SEO_TO_CATEGORIES','seo_to_categories');
  define('TABLE_SEO_TO_PRODUCTS','seo_to_products');
  define('TABLE_SEO_TO_MANUFACTURERS','seo_to_manufacturers');
  define('TABLE_SEO_TO_GTEXT','seo_to_gtext');
  define('TABLE_SEO_TO_ABSTRACT','seo_to_abstract');
  define('TABLE_SEO_TO_RANGES','seo_to_ranges');
  define('TABLE_SEO_TO_FILTERS','seo_to_filters');
  define('TABLE_SEO_TO_AUCTIONS','seo_to_auctions');
  define('TABLE_SEO_TO_AUCTIONS_GROUP','seo_to_auctions_group');
  define('TABLE_SEO_TO_SCRIPTS','seo_to_scripts');
//-MS- SEO-G Added EOM

//-MS- META-G Added
  define('TABLE_META_SCRIPTS', 'meta_scripts');
  define('TABLE_META_LEXICO', 'meta_lexico');
  define('TABLE_META_EXCLUDE', 'meta_exclude');
  define('TABLE_META_TYPES', 'meta_types');
  define('TABLE_META_PRODUCTS', 'meta_products');
  define('TABLE_META_CATEGORIES', 'meta_categories');
  define('TABLE_META_MANUFACTURERS', 'meta_manufacturers');
  define('TABLE_META_GTEXT','meta_gtext');
  define('TABLE_META_ABSTRACT','meta_abstract');
  define('TABLE_META_RANGES', 'meta_ranges');
  define('TABLE_META_AUCTIONS', 'meta_auctions');
  define('TABLE_META_AUCTIONS_GROUP', 'meta_auctions_group');
//-MS- META-G Added EOM

//-MS- Cache Support added
  define('TABLE_CACHE_HTML', 'cache_html');
  define('TABLE_CACHE_HTML_REPORTS', 'cache_html_reports');
//-MS- Cache Support added

//-MS- Extra Images Added
  define('TABLE_PRODUCTS_EXTRA_IMAGES', 'products_extra_images');
//-MS- Extra Images Added EOM

//-MS- Added testimonials
  define('TABLE_TESTIMONIALS', 'testimonials');
//-MS- Added testimonials EOM

//-MS- Add Help Desk system
  define('TABLE_HELPDESK_DEPARTMENTS', 'helpdesk_departments');
//-MS- Add Help Desk system EOM

//-MS- Added Products Filters
  define('TABLE_PRODUCTS_FILTERS', 'products_filters');
  define('TABLE_PRODUCTS_SELECT', 'products_select');
//-MS- Added Products Filters EOM

//-MS- Added Group Fields
  define('TABLE_PRODUCTS_TO_GROUP_FIELDS', 'products_to_group_fields');
  define('TABLE_GROUP_FIELDS', 'group_fields');
  define('TABLE_GROUP_OPTIONS', 'group_options');
  define('TABLE_GROUP_VALUES', 'group_values');
  define('TABLE_CUSTOMERS_BASKET_GROUP_FIELDS', 'customers_basket_group_fields');
  define('TABLE_ORDERS_PRODUCTS_GROUP_FIELDS', 'orders_products_group_fields');
//-MS- Added Group Fields EOM

//-MS- Added Auctions
  define('TABLE_AUCTIONS_GROUP', 'auctions_group');
  define('TABLE_AUCTIONS', 'auctions');
  define('TABLE_AUCTIONS_BID', 'auctions_bid');
  define('TABLE_AUCTIONS_CUSTOMER', 'auctions_customer');
  define('TABLE_AUCTIONS_TIER', 'auctions_tier');
  define('TABLE_AUCTIONS_HISTORY', 'auctions_history');
//-MS- Added Auctions EOM

//-MS- Added Coupons
  define('TABLE_COUPONS', 'coupons');
  define('TABLE_COUPONS_REDEEM', 'coupons_redeem');
//-MS- Added Coupons EOM
?>
