<?php
/*
  $Id: footer.php,v 1.26 2003/02/10 22:30:54 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Footer customizations
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  //require(DIR_WS_INCLUDES . 'counter.php');
?>
      <div class="dtrans_footer">
<?php
  $footer_query = tep_db_query("select gtext_description from " . TABLE_GTEXT . " where gtext_id = '" . GTEXT_FOOTER_INFO_ID . "'");
  $footer_array = tep_db_fetch_array($footer_query);
  //echo $footer_array['gtext_description'];

?>
        <div class="totalsize balancer">
          <div class="bounder vpad">
            <div class="floater hpadder"><a href="#" title="Affiliates"><?php echo tep_image(DIR_WS_BANNERS . 'affiliatebanner.jpg', 'Become an affiliate'); ?></a></div>
            <div class="floatend quarter calign tpad">
              <span class="charsep"><a href="http://www.facebook.com/maxabid" title="coming soon"><?php echo tep_image(DIR_WS_IMAGES . 'design/facebook.png', STORE_NAME . ' on Facebook'); ?></a></span>
              <span class="charsep"><a href="http://www.twitter.com/maxabid" title="coming soon"><?php echo tep_image(DIR_WS_IMAGES . 'design/twitter.png', STORE_NAME . ' on Twitter'); ?></a></span>
              <span class="charsep"><a href="http://www.youtube.com/maxabid" title="coming soon"><?php echo tep_image(DIR_WS_IMAGES . 'design/youtube.png', STORE_NAME . ' on YouTube'); ?></a></span>
            </div>
          </div>
        </div>
        <div class="bounder ralign">
<?php
  $footer_contents = array();
  $cText = new gtext_front();
  $tmp_array = $cText->get_gtext_entries(DEFAULT_FOOTER_TEXT_ZONE_ID);
  $footer_array = array();
  foreach( $tmp_array as $key => $value ) {
    $footer_array[] = '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $key) . '" title="' . $value['gtext_title'] . '">' . $value['gtext_title'] . '</a>';
  }
  if( !empty($footer_array) ) {
    $footer_string = implode(' | ', $footer_array);
    echo $footer_string;
  }
?>
        </div>
<?php

?>
      </div>
