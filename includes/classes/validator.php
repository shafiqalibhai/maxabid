<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Validator for /GET /POST parameters class
// Validates parameters passed
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class validator {
    var $get_array, $get_keys, $post_array, $post_keys;
// class constructor
    function validator() {
      global $session_started;
      $this->get_array = array();
      $this->post_array = array();

      $this->get_keys = array(
                              'products_id' => 'products_id', 
                              'cPath' => 'cPath', 
                              'manufacturers_id' => 'manufacturers_id', 
                              'abz_id' => 'abz_id', 
                              'gtext_id' => 'gtext_id', 
                              'range_id' => 'range_id', 
                              'order_id' => 'order_id', 
                              'testimonial_id' => 'testimonial_id', 
                              'page' => 'page', 
                              'action' => '',
                             );
      $this->post_keys = array(
                              );

      $this->forbid_keys = array(
                                 'GLOBALS' => 'GLOBALS',
                                 '_SESSION' => '_SESSION',
                                 '_COOKIE' => '_COOKIE',
                                 '_GET' => '_GET',
                                 '_POST' => '_POST',
                                 '_REQUEST' => '_REQUEST',
                                 '_SERVER' => '_SERVER',
                                 '_ENV' => '_ENV',
                                 '_FILES' => '_FILES',
                                 'g_external' => 'g_external',
                                );

      foreach($this->forbid_keys as $key => $value) {
        if( isset($_REQUEST[$key]) ) {
          $this->process_error();
        }
      }

      foreach($this->get_keys as $key => $value) {
        if( !isset($_GET[$key]) ) continue;
        $method = 'get_' . $key;
        if( method_exists($this, 'get_' . $key) ) {
          $result = $this->$method($_GET[$key]);
          if($result !== false ) {
            if( tep_not_null($value) ) {
              $this->get_array[$key] = $result;
            }
          } else {
            $this->process_error();
          }
        }
      }

      if( $session_started == true ) {
        foreach($this->post_keys as $key => $value) {
          if( !isset($_POST[$key]) ) continue;
          $method = 'post_' . $key;
          if( method_exists($this, 'post_' . $key) ) {
            $result = $this->$method($value);
            if($result !== false ) {
              if( tep_not_null($value) ) {
                $this->post_array[$key] = $result;
              }
            } else {
              $this->process_error();
            }
          }
        }
      }
    }

    function remove_get_entry($key) {
      unset($this->get_array[$key]);
    }

    function remove_post_entry($key) {
      unset($this->post_array[$key]);
    }

    function process_error() {
      tep_session_destroy();
      tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
    }

    function common_strip($value) {
      $result = '';
      if( !is_array($value) && !empty($value) ) {
        $search = array('/<\?((?!\?>).)*\?>/s');
        $result = strip_tags(preg_replace($search, '', $value));
      }
      return $result;
    }

    function common_validate($input) {
      $result = '';
      $tmp_input = $this->common_strip($input);
      if( $input == $tmp_input ) {
        $result = $tmp_input;
      }
      return $result;
    }

    function post_validate($input_array) {
      if( !is_array($input_array) || !count($input_array) ) {
        return;
      }

      foreach($input_array as $key => $value) {
        if( isset($_POST[$value]) ) {
          $_POST[$value] = $this->common_validate($_POST[$value]);
          // In case a parameter conflicts with a session one skip
          if( !tep_session_is_registered($_POST[$value]) ) {
            $GLOBALS[$value] = $_POST[$value];
          }
        }
      }
    }

    function convert_to_get($exclude_array=array()) {
      $tmp_array = array();
      foreach( $this->get_array as $key => $value ) {
        if( !isset($exclude_array[$key]) ) {
          $tmp_array[] = $key . '=' . $value;
        }
      }
      $result = implode('&', $tmp_array);
      return $result;
    }

    

    function get_products_id($id, $set_global = true) {
      global $current_product_id;

      if( !defined('TABLE_PRODUCTS_TO_CATEGORIES') || !defined('TABLE_PRODUCTS') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_id = '" . (int)$id . "' and products_display='1'");

      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['products_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } elseif($set_global) {
        $current_product_id = $id;
      }
      return $id;
    }

    function get_manufacturers_id($id) {
      global $current_manufacturer_id;

      if( !defined('TABLE_MANUFACTURERS') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select manufacturers_id from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$id . "'");
      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['manufacturers_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } else {
        $current_manufacturer_id = $id;
      }
      return $id;    
    }

    function get_abz_id($id) {
      global $current_abstract_id;

      if( !defined('TABLE_ABSTRACT_ZONES') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select abstract_zone_id from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$id . "'");
      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['abstract_zone_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } else {
        $current_abstract_id = $id;
      }
      return $id;    
    }

    function get_gtext_id($id) {
      global $current_gtext_id;

      if( !defined('TABLE_GTEXT') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select gtext_id from " . TABLE_GTEXT . " where gtext_id = '" . (int)$id . "'");
      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['gtext_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } else {
        $current_gtext_id = $id;
      }
      return $id;    
    }

    function get_testimonial_id($id) {
      global $current_testimonial_id;

      if( !defined('TABLE_TESTIMONIALS') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select testimonials_id from " . TABLE_TESTIMONIALS . " where testimonials_id = '" . (int)$id . "'");
      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['testimonials_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } else {
        $current_testimonial_id = $id;
      }
      return $id;    
    }

    function get_order_id($id) {
      global $customer_id;

      if( !defined('TABLE_ORDERS') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$id . "'");
      $id = false;
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['orders_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      }
      return $id;    
    }

    function get_range_id($id) {
      global $current_range_id;

      if( !defined('TABLE_NUMERIC_RANGES') ) {
        return false;
      }

      $org_id = $id;
      $check_query = tep_db_query("select numeric_ranges_id from " . TABLE_NUMERIC_RANGES . " where numeric_ranges_id = '" . (int)$id . "'");
      $id = false;

      if( $check_array = tep_db_fetch_array($check_query) ) {
        $id = $check_array['numeric_ranges_id'];
      }

      if( $id != $org_id ) {
        $id = false;
      } else {
        $current_range_id = $id;
      }
      return $id;    
    }

    function get_cPath($cpath_string) {
      global $current_category_id, $cPath_array, $cPath;

      $cPath = '';
      if( !defined('TABLE_CATEGORIES') ) {
        $this->process_error();
        return $cPath;
      }

      $categories_id = false;
      $tmp_array = explode('_', $cpath_string);
      if( is_array($tmp_array) && count($tmp_array) ) {
        $categories_id = $tmp_array[count($tmp_array)-1];

        $check_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
        $categories_id = false;
        if( $check_array = tep_db_fetch_array($check_query) ) {
          $categories_id = $check_array['categories_id'];
        }
        if( $categories_id !== false ) {
          $cPath = tep_get_category_path($categories_id);
          if( $cPath != $cpath_string ) {
            $this->process_error();
          }
          $cPath_array = explode('_', $cPath);
          $current_category_id = $categories_id;
        } else {
          $this->process_error();
        }
      } else {
        $this->process_error();
      }
      return $cPath;
    }

    function get_page($id) {
      global $current_page_id;

      $org_id = (int)$id;
      if( $org_id <= 0 ) {
        return false;
      }
      $org_id .= "";
      if( $org_id == $id ) {
        $current_page_id = $id;
        return $id;
      }
      return false;
    }

    function get_action($action) {
      $result = false;
      $tmp_action = $this->common_strip($action);
      if( $action == $tmp_action ) {
        $result = $tmp_action;
      }
      return $result;
    }

    function update_get($param, $value) {
      global $navigation;
      $old_array = $this->get_array;
      $this->get_array[$param] = $value;
      $navigation->update_get_entry($old_array, $this->get_array);
    }

    function update_post($param, $value) {
      global $navigation;
      $old_array = $this->post_array;
      $this->post_array[$param] = $value;
      $navigation->update_post_entry($old_array, $this->post_array);
    }
  }
?>
