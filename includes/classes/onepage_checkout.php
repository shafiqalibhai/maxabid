<?php
  class osC_onePageCheckout {
      
      function osC_onePageCheckout(){
       if( isset($_GET['action']) && $_GET['action'] == 'regularCheckout') {
         $this->regularCheckout();
         tep_exit();
        }
        $this->buildSession();
      }
      
      function reset(){
        $this->buildSession(true);
      }

      function buildSession($forceReset = false){
        global $customer_id, $sendto, $billto, $cart, $languages_id, $currency, $currencies, $shipping, $payment, $onepage;

        if (!tep_session_is_registered('onepage') || $forceReset === true ) {
          tep_session_register('onepage');
          $onepage = array(
                'info'           => array(
                    'payment_method' => '', 'shipping_method' => '', 'comments' => ''
                ),
                'customer'       => array(
                    'firstname' => '',  'lastname' => '', 'company' => '',  'street_address' => '',
                    'suburb' => '',     'city' => '',     'postcode' => '', 'state' => '',
                    'zone_id' => '',    'country' => array('id' => '', 'title' => '', 'iso_code_2' => '', 'iso_code_3' => ''),
                    'format_id' => '',  'telephone' => '', 'email_address' => '', 'password' => '', 'newsletter' => ''
                ),
                'delivery'       => array(
                    'firstname' => '',  'lastname' => '', 'company' => '',  'street_address' => '', 
                    'suburb' => '',     'city' => '',     'postcode' => '', 'state' => '', 
                    'zone_id' => '',    'country' => array('id' => '', 'title' => '', 'iso_code_2' => '', 'iso_code_3' => ''), 
                    'country_id' => '', 'format_id' => ''
                ),
                'billing'        => array(
                    'firstname' => '',  'lastname' => '', 'company' => '',  'street_address' => '', 
                    'suburb' => '',     'city' => '',     'postcode' => '', 'state' => '', 
                    'zone_id' => '',    'country' => array('id' => '', 'title' => '', 'iso_code_2' => '', 'iso_code_3' => ''), 
                    'country_id' => '', 'format_id' => ''
                ),
                'logged_in'      => false,
                'create_account' => false
            );
        }

        if (!tep_session_is_registered('shipping')) {
          tep_session_register('shipping');
          $shipping = false;
        }
        if (!tep_session_is_registered('payment')) {
          tep_session_register('payment');
          $payment = false;
        }

        if( tep_session_is_registered('customer_id')) {
          $onepage['logged_in'] = true;
          $onepage['create_account'] = false;
        }
      }

      function loadSessionVars($type = 'checkout'){
        global $order, $customer_id, $sendto, $billto, $cart, $languages_id, $currency, $currencies, $shipping, $payment, $onepage, $comments;

        if( tep_not_null($onepage['info']['payment_method']) ) {
          $payment = $onepage['info']['payment_method'];

          if( isset($GLOBALS[$payment]) ){
            $pModule = $GLOBALS[$payment];
            if (isset($pModule->public_title)) {
              $order->info['payment_method'] = $pModule->public_title;
            } else {
              $order->info['payment_method'] = $pModule->title;
            }
            
            if (isset($pModule->order_status) && is_numeric($pModule->order_status) && ($pModule->order_status > 0)){
              $order->info['order_status'] = $pModule->order_status;
            }
          }
        }

        if( tep_not_null($onepage['info']['shipping_method']) ) {
          $shipping = $onepage['info']['shipping_method'];
          $order->info['shipping_method'] = $shipping['title'];
          $order->info['shipping_cost'] = $shipping['cost'];
        }

        if( tep_not_null($onepage['info']['comments']) ) {
          $comments = $onepage['info']['comments'];
        }

        if( !tep_session_is_registered('customer_id') ) {
          $order->billing = $onepage['billing'];
          $order->delivery = $onepage['delivery'];
          $order->customer = $onepage['customer'];
        } else {
          if (tep_not_null($onepage['billing']['firstname'])) {
            $order->billing = $onepage['billing'];
          }
          if (tep_not_null($onepage['delivery']['firstname'])) {
            $order->delivery = $onepage['delivery'];
          }
        }

        if( tep_not_null($onepage['customer']['firstname']) ) {
          $order->customer = $onepage['customer'];
        } else {
          if (tep_not_null($onepage['billing']['firstname'])){
            $order->customer = $onepage['billing'];
          }
          if (tep_not_null($onepage['customer']['email_address'])) {
            $order->customer['email_address'] = $onepage['customer']['email_address'];
            $order->customer['telephone'] = $onepage['customer']['telephone'];
            //$order->customer['dob'] = $onepage['customer']['dob'];
          }
        }
/*
        if (tep_not_null($onepage['billing']['firstname'])) {
          $order->billing = $onepage['billing'];
        }
        if (tep_not_null($onepage['delivery']['firstname'])) {
          $order->delivery = $onepage['delivery'];
        }
*/
      }
      
      function init(){
        $this->verifyContents();
        $this->reset();
        
        if (STOCK_CHECK == 'true' && STOCK_ALLOW_CHECKOUT != 'true') {
          $this->checkStock();
        }
        
        $this->setDefaultSendTo();
        $this->setDefaultBillTo();
        
        $this->removeCCGV();
      }
      
      
      function checkEmailAddress($emailAddress){
        $success = 'true';
        $errMsg = '';
        
        $Qcheck = tep_db_query('select customers_id from ' . TABLE_CUSTOMERS . ' where customers_email_address = "' . tep_db_prepare_input(tep_db_input($emailAddress)) . '"');
        if (tep_db_num_rows($Qcheck)){
          $success = 'false';
          $errMsg = 'Your email address already exists, please log into your account or use a different email address.';
        } else {
          require_once('includes/functions/validations.php');
          if (tep_validate_email($emailAddress) === false){
            $success = 'false';
            $errMsg = 'The email address provided is invalid.';
          }
        }

        return '{
            success: ' . $success . ',
            errMsg: "' . $errMsg . '"
        }';
      }
      
      function setTelephoneNumber($phoneNumber){
        global $order, $onepage;
        $order->customer['telephone'] = $phoneNumber;
        $onepage['customer']['telephone'] = $phoneNumber;

        return '{
            success: true
        }';
      }
      
      function setNewsletter($status){
        global $order, $onepage;
        $order->customer['newsletter'] = ($status == 'false' ? '0' : '1');
        $onepage['customer']['newsletter'] = $order->customer['newsletter'];

        return '{
            success: true
        }';
      }
/*
      function setDOB($dob){
        global $order, $onepage;
        $order->customer['dob'] = $dob;
        $onepage['customer']['dob'] = $order->customer['dob'];

        return '{
            success: true
        }';
      }
*/
      function setSuburb($suburb){
        global $order, $onepage;
        $order->billing['suburb'] = $suburb;
        $onepage['billing']['suburb'] = $order->billing['suburb'];

        return '{
            success: true
        }';
      }
      
      function getAjaxStateField(){
        global $onepage;

        $country = (int)$_POST['cID'];
        $name = $_POST['fieldName'];
        if ($name == 'billing_state'){
          $key = 'billing';
        }else{
          $key = 'delivery';
        }
        $html = '';
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
        $check = tep_db_fetch_array($check_query);
        if ($check['total'] > 0) {
          $zones_array = array(
            array('id' => '', 'text' => TEXT_PLEASE_SELECT)
          );
          $zones_query = tep_db_query("select zone_id, zone_code, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' order by zone_name");
          $selected = '';
          while ($zones_values = tep_db_fetch_array($zones_query)) {
            if (isset($onepage[$key]['zone_id']) && $onepage[$key]['zone_id'] == $zones_values['zone_id']){
              $selected = $zones_values['zone_code'];
            }
            $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
          }
          $html .= '<div class="floater">' . tep_draw_pull_down_menu($name, $zones_array, $selected, 'class="floater required" id="state" style="width: 82%;"') . '</div>';
        } else {
          $html .= tep_draw_input_field($name, (isset($onepage[$key]['state']) ? $onepage[$key]['state']: ''), 'class="floater required" id="state"');
        }
        return $html;
      }

      function updateCartProducts($qtys, $ids, $xfids){
        global $cart;
        foreach($qtys as $pID => $qty){
          $id = $xfid = '';
          if( is_array($ids) && isset($ids[$pID]) ) $id = $ids[$pID];
          if( is_array($xfids) && isset($xfids[$pID]) ) $xfid = $xfids[$pID];

          $cart->update_quantity($pID, $qty, $id, $xfid);
        }
        
        $json = '';
        if (isset($_GET['rType']) && $_GET['rType'] == 'ajax'){
          $json .= '{
              success: true
          }';
        }else{
          tep_redirect(tep_href_link(FILENAME_CHECKOUT));
        }
        return $json;
      }
      
      function removeProductFromCart($productID){
        global $cart;
          $cart->remove($productID);
          
          $json = '';
          if (isset($_GET['rType']) && $_GET['rType'] == 'ajax'){
              $json .= '{
                  success: true,
                  products: "' . $cart->count_contents() . '"
              }';
          } else {
            tep_redirect(tep_href_link(FILENAME_CHECKOUT));
          }
        return $json;
      }
      
      function processAjaxLogin($emailAddress, $password){
        //global $wishList;
        global $cart, $customer_id, $customer_first_name, $customer_default_address_id, $customer_country_id, $customer_zone_id, $logged_id, $billto, $sendto;
        $error = false;
            
        $check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_password, customers_email_address, customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($emailAddress) . "'");
        if (!tep_db_num_rows($check_customer_query)) {
          $error = true;
        } else {
          $check_customer = tep_db_fetch_array($check_customer_query);
          // Check that password is good
          if (!tep_validate_password($password, $check_customer['customers_password'])) {
            $error = true;
          } else {
            if (SESSION_RECREATE == 'True') {
              tep_session_recreate();
            }
            
            $check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$check_customer['customers_id'] . "' and address_book_id = '" . (int)$check_customer['customers_default_address_id'] . "'");
            $check_country = tep_db_fetch_array($check_country_query);

            $customer_id = $check_customer['customers_id'];
            $customer_default_address_id = $check_customer['customers_default_address_id'];
            $customer_country_id = $check_country['entry_country_id'];
            $customer_zone_id = $check_country['entry_zone_id'];
            $onepage['logged_in'] = true;
            $onepage['createAccount'] = false;

            $sendto = $billto = $check_customer['customers_default_address_id'];

            tep_session_register('customer_id');
            tep_session_register('customer_first_name');
            tep_session_register('customer_default_address_id');
            tep_session_register('customer_country_id');
            tep_session_register('customer_zone_id');

            if (!tep_session_is_registered('billto')) tep_session_register('billto');
            if (!tep_session_is_registered('sendto')) tep_session_register('sendto');

            tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");
                
            // restore cart contents
            $cart->restore_contents();
            //$wishList->restore_wishlist();
          }
        }
        $json = '';
        if ($error === false){
          $json .= '{
            success: true,
            msg: "Loading your account info"
          }';
        } else {
          $json .= '{
            success: false,
            msg: "Authorization Failed"
          }';
        }
        return $json;
      }
      
      function confirmation(){
        global $payment, $order;

        $success = true;
        $errMsg = '';

        if (!tep_session_is_registered('payment') || $payment == false) {
          $success = false;
          $errMsg = 'Payment not set';
        } else {
          $pModule = $GLOBALS[$payment];
            
          if (isset($pModule->order_status) && is_numeric($pModule->order_status) && ($pModule->order_status > 0)){
            $order->info['order_status'] = $pModule->order_status;
            $confirmation = $GLOBALS[$payment]->confirmation();
          } else {
            //$success = false;
            //$errMsg = 'Order Status not set';
          }
        }

        return '{
            success: ' . $success . ',
            errMsg: "' . $errMsg . '"
        }';

      }

      function setPaymentMethod($method){
        global $payment_modules, $language, $order, $cart, $payment, $onepage;

          if (!tep_session_is_registered('payment')) {
            tep_session_register('payment');
            $payment = false;
          }

          /* Comment IF statement below for oscommerce versions before MS2.2 RC2a */
          if( tep_not_null($payment) && $payment != $method) {
            $GLOBALS[$payment]->selection();
          }
          
          $payment = $method;
          $onepage['info']['payment_method'] = $method;
         
          $order->info['payment_method'] = $GLOBALS[$payment]->title;
          
          /* Comment line below for oscommerce versions before MS2.2 RC2a */
          //$confirmation = $GLOBALS[$payment]->confirmation();
                   
          $inputFields = '';
/*
          if ($confirmation !== false){
            for ($i=0, $n=sizeof($confirmation['fields']); $i<$n; $i++) {
              $inputFields .= '<tr>' . 
               '<td width="10">' . tep_draw_separator('pixel_trans.gif', '10', '1') . '</td>' . 
               '<td class="main">' . $confirmation['fields'][$i]['title'] . '</td>' . 
               '<td>' . tep_draw_separator('pixel_trans.gif', '10', '1') . '</td>' . 
               '<td class="main">' . $confirmation['fields'][$i]['field'] . '</td>' . 
               '<td width="10">' . tep_draw_separator('pixel_trans.gif', '10', '1') . '</td>' . 
              '</tr>';
            }

            if ($inputFields != ''){
              $inputFields = '<tr class="paymentFields">' . 
               '<td width="10">' . tep_draw_separator('pixel_trans.gif', '10', '1') . '</td>' . 
               '<td colspan="2"><table border="0" cellspacing="0" cellpadding="2">' . 
               $inputFields . 
               '</table></td>' . 
               '<td width="10">' . tep_draw_separator('pixel_trans.gif', '10', '1') . '</td>' . 
              '</tr>';
            }
          }
*/
              
        return '{
            success: true,
            inputFields: "' . addslashes($inputFields) . '"
        }';
      }
      
      function setShippingMethod($method = ''){
        global $shipping_modules, $language, $order, $cart, $shipping, $onepage, $info, $free_shipping;

        if (defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') {
          $pass = false;
              
          switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
              case 'national':
                if ($order->delivery['country_id'] == STORE_COUNTRY) {
                  $pass = true;
                }
                break;
              case 'international':
                if ($order->delivery['country_id'] != STORE_COUNTRY) {
                  $pass = true;
                }
                break;
              case 'both':
                $pass = true;
                break;
          }
              
          $free_shipping = false;
          if ($pass == true && $order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) {
            $free_shipping = true;
            include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
          }
        } else {
          $free_shipping = false;
        }
        
        if (!tep_session_is_registered('shipping')) {
          tep_session_register('shipping');
        }
        $shipping = false;
        $onepage['info']['shipping_method'] = false;
            
        if (tep_count_shipping_modules() > 0 || $free_shipping == true) {
          if (strpos($method, '_')) {
            $shipping = $method;
            
            list($module, $method) = explode('_', $shipping);
            global $$module;
            if (is_object($$module) || $shipping == 'free_free') {
              if($shipping == 'free_free') {
                $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
                $quote[0]['methods'][0]['cost'] = '0';
              } else {
                $quote = $shipping_modules->quote($method, $module);
              }
                  
              if (isset($quote['error'])) {
                tep_session_unregister('shipping');
              } else {
                if (isset($quote[0]['methods'][0]['title']) && isset($quote[0]['methods'][0]['cost'])) {
                  $shipping = array(
                      'id' => $shipping,
                      'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                      'cost' => $quote[0]['methods'][0]['cost']
                  );
                  $onepage['info']['shipping_method'] = $shipping;
                }
              }
            } else {
              tep_session_unregister('shipping');
            }
          }
        }

        return '{
            success: true
        }';
      }
      
      function setCheckoutAddress($action){
        global $order, $onepage, $sendto, $billto, $customer_id;

        $success = true;
        $errMsg = '';

        if( tep_session_is_registered('customer_id') ) {
          return '{
              success: ' . $success . ',
              errMsg: "' . $errMsg . '"
          }';
        }

        if( $action == 'setBillTo' && tep_not_null($onepage['info']['payment_method']) ) {
          $payment = $onepage['info']['payment_method'];

          if( isset($GLOBALS[$payment]) ){
            $pModule = $GLOBALS[$payment];
            if (isset($pModule->public_title)) {
              $order->info['payment_method'] = $pModule->public_title;
            } else {
              $order->info['payment_method'] = $pModule->title;
            }
            
            if (isset($pModule->order_status) && is_numeric($pModule->order_status) && ($pModule->order_status > 0)){
              $order->info['order_status'] = $pModule->order_status;
            }
          }
        }

        $prefix = ($action == 'setSendTo' ? 'shipping_' : 'billing_');
        if( isset($_POST['copyover']) && $_POST['copyover'] == 1 ) {
          $prefix = 'billing_';
        }

        $gender = (isset($_POST[$prefix . 'gender'])) ? tep_db_prepare_input($_POST[$prefix . 'gender']): '';
        $company = (isset($_POST[$prefix . 'company'])) ? tep_db_prepare_input($_POST[$prefix . 'company']):'';
        $suburb = (isset($_POST[$prefix . 'suburb'])) ? tep_db_prepare_input($_POST[$prefix . 'suburb']):'';
        $country = tep_db_prepare_input($_POST[$prefix . 'country']);
        if (ACCOUNT_STATE == 'true') {
          if (isset($_POST[$prefix . 'zone_id'])) {
            $zone_id = tep_db_prepare_input($_POST[$prefix . 'zone_id']);
          } else {
            $zone_id = false;
          }
          $state = tep_db_prepare_input($_POST[$prefix . 'state']);
          $zone_name = '';
              
          $zone_id = 0;
          $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
          $check = tep_db_fetch_array($check_query);
          $entry_state_has_zones = ($check['total'] > 0);
          if ($entry_state_has_zones == true) {
            $zone_query = tep_db_query("select distinct zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
            if (tep_db_num_rows($zone_query) == 1) {
              $zone = tep_db_fetch_array($zone_query);
              $zone_id = $zone['zone_id'];
              $zone_name = $zone['zone_name'];
            } else {
              //$success = false;
              //$errMsg = 'Invalid State specified';
            }
          }
        }
            
        $QcInfo = tep_db_query('select * from ' . TABLE_COUNTRIES . ' where countries_id = "' . (int)$country . '"');
        $cInfo = tep_db_fetch_array($QcInfo);
        
        if ($action == 'setBillTo'){
          $varName = 'billing';
        }else{
          $varName = 'delivery';
        }
        
        if ($action == 'setBillTo'){
          if (tep_not_null($_POST['billing_email_address'])){
            $order->customer['email_address'] = tep_db_prepare_input($_POST['billing_email_address']);
            $onepage['customer']['email_address'] = $order->customer['email_address'];
          }
          if (tep_not_null($_POST['billing_telephone'])){
            $order->customer['telephone'] = tep_db_prepare_input($_POST['billing_telephone']);
            $onepage['customer']['telephone'] = $order->customer['telephone'];
          }
          if (tep_not_null($_POST['password'])){
            $onepage['customer']['password'] = tep_encrypt_password($_POST['password']);
          }

        }
                
        $order->{$varName}['gender'] = $gender;
        $order->{$varName}['firstname'] = tep_db_prepare_input($_POST[$prefix . 'firstname']);
        $order->{$varName}['lastname'] = tep_db_prepare_input($_POST[$prefix . 'lastname']);
        $order->{$varName}['company'] = $company;
        $order->{$varName}['street_address'] = tep_db_prepare_input($_POST[$prefix . 'street_address']);
        $order->{$varName}['suburb'] = $suburb;
        $order->{$varName}['city'] = tep_db_prepare_input($_POST[$prefix . 'city']);
        $order->{$varName}['postcode'] = tep_db_prepare_input($_POST[$prefix . 'postcode']);
        $order->{$varName}['state'] = $state;
        $order->{$varName}['zone_id'] = $zone_id;
        $order->{$varName}['country'] = array(
            'id'         => $cInfo['countries_id'], 
            'title'      => $cInfo['countries_name'], 
            'iso_code_2' => $cInfo['countries_iso_code_2'], 
            'iso_code_3' => $cInfo['countries_iso_code_3']
        );
        $order->{$varName}['country_id'] = $cInfo['countries_id'];
        $order->{$varName}['format_id'] = $cInfo['address_format_id'];
        
        if ($action == 'setBillTo'){
          $onepage['billing'] = $order->billing;
        } else {
          $onepage['delivery'] = $order->delivery;
        }

        if( isset($_POST['copyover']) && $_POST['copyover'] == 1 ) {
          //$order->customer = $onepage['customer'] = $order->delivery = $onepage['delivery'] = $order->billing;
        }

        return '{
            success: ' . $success . ',
            errMsg: "' . $errMsg . '"
        }';
      }
      
      function setAddress($addressType, $addressID){
        global $billto, $sendto;
        switch($addressType){
          case 'billing':
            $billto = $addressID;
          break;
          case 'shipping':
            $sendto = $addressID;
          break;
        }

        return '{
          success: true
        }';
      }
      
      function saveAddress($action){
        global $customer_id;

        $error = false;
        $error_array = array();

        if (ACCOUNT_GENDER == 'true') $gender = tep_db_prepare_input($_POST['gender']);

        if (ACCOUNT_COMPANY == 'true') $company = tep_db_prepare_input($_POST['company']);

        $firstname = tep_db_prepare_input($_POST['firstname']);
        if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
          return $this->displayError(false, ENTRY_FIRST_NAME_ERROR);
        }

        $lastname = tep_db_prepare_input($_POST['lastname']);
        if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
          return $this->displayError(false, ENTRY_LAST_NAME_ERROR);
        }

        $street_address = tep_db_prepare_input($_POST['street_address']);
        if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
          return $this->displayError(false, ENTRY_STREET_ADDRESS_ERROR);
        }

        if (ACCOUNT_SUBURB == 'true') $suburb = tep_db_prepare_input($_POST['suburb']);
        $postcode = tep_db_prepare_input($_POST['postcode']);
        $city = tep_db_prepare_input($_POST['city']);
        $country = tep_db_prepare_input($_POST['country']);
        if (ACCOUNT_STATE == 'true') {
          if (isset($_POST['zone_id'])) {
            $zone_id = tep_db_prepare_input($_POST['zone_id']);
          } else {
            $zone_id = false;
          }
          $state = (int)$_POST['state'];
              
          $zone_id = 0;
          $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
          $check = tep_db_fetch_array($check_query);
          $entry_state_has_zones = ($check['total'] > 0);
          if ($entry_state_has_zones == true) {
            $zone_query = tep_db_query("select zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_id = '" . (int)$state . "'");
            if (tep_db_num_rows($zone_query) == 1) {
              $zone = tep_db_fetch_array($zone_query);
              $zone_id = $zone['zone_id'];
            } else {
              return $this->displayError(false, 'Zone not in the database ' . $state);
            }
          } else {
            return $this->displayError(false, 'Invalid Country specified');
          }
        }
            
        $sql_data_array = array(
            'customers_id'         => $customer_id,
            'entry_firstname'      => $firstname,
            'entry_lastname'       => $lastname,
            'entry_street_address' => $street_address,
            'entry_postcode'       => $postcode,
            'entry_city'           => $city,
            'entry_country_id'     => $country
        );
            
        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
        if (ACCOUNT_STATE == 'true') {
          if ($zone_id > 0) {
            $sql_data_array['entry_zone_id'] = $zone_id;
            $sql_data_array['entry_state'] = '';
          } else {
            return $this->displayError(false, 'Invalid Country Zone specified');
            //$sql_data_array['entry_zone_id'] = '0';
            //$sql_data_array['entry_state'] = $state;
          }
        }
        
        if( !$error ) {
          if ($action == 'saveAddress'){
            $Qcheck = tep_db_query('select address_book_id from ' . TABLE_ADDRESS_BOOK . ' where address_book_id = "' . (int)$_POST['address_id'] . '" and customers_id = "' . (int)$customer_id . '"');
            if (tep_db_num_rows($Qcheck)){
              tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', 'address_book_id = "' . (int)$_POST['address_id'] . '"');
            }
          } else {
            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
          }
          $success = true;
          $errMsg = '';
        } else {
          $success = false;
          $errMsg = implode('<br />', $error_array);
        }

        return '{
            success: ' . $success . ',
            errMsg: "' . $errMsg . '"
        }';

      }
      
      function regularCheckout(){
        global $noajax;
        tep_session_register('noajax');
        $noajax = 1;
        tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL', true, false));
      }

      function processCheckout(){
        global $customer_id, $comments, $order, $currencies, $request_type, $languages_id, $currency;
        global $cart_PayPal_IPN_ID, $shipping, $cartID, $order_total_modules, $payment, $shipping, $billto, $sendto;
        global $onepage, $messageStack, $coversAll, $payment_data;

          $result = '';
          if( !isset($payment) || !isset($GLOBALS[$payment]) || !is_object($GLOBALS[$payment]) ) {
            $messageStack->add_session('checkout', 'Invalid Payment Method Selected', 'error');
            tep_redirect(tep_href_link(FILENAME_CHECKOUT, '', 'SSL', true, false));

//            return $this->displayError(false, 'Invalid Payment Method');
          }
/*
          if( !isset($shipping) || !is_array($shipping) ) {
            $messageStack->add_session('checkout', 'Invalid Shipping Method Selected', 'error');
            tep_redirect(tep_href_link(FILENAME_CHECKOUT, '', 'SSL', true, false));
            //return $this->displayError(false, 'Invalid Shipping Method');
          }
*/
          if( tep_session_is_registered('payment_data') ) {
            tep_session_unregister('payment_data');
          }

          if( !tep_session_is_registered('payment') ) {
            tep_session_register('payment');
          }

          if( !tep_session_is_registered('shipping') ) {
            tep_session_register('shipping');
          }
          if( !tep_session_is_registered('billto') ) {
            tep_session_register('billto');
          }
          if( !tep_session_is_registered('sendto') ) {
            tep_session_register('sendto');
          }


          if( !tep_session_is_registered('comments') ) {
            tep_session_register('comments');
            $comments = '';
          }
          $comments = (isset($_POST['comments']))?tep_db_prepare_input($_POST['comments']):'';
        
          // Start - CREDIT CLASS Gift Voucher Contribution
          if( tep_session_is_registered('credit_covers') ) {
            $payment = 'credit_covers';
          }
          // End - CREDIT CLASS Gift Voucher Contribution
          
          $html = '';
          $infoMsg = 'Please press the continue button to confirm your order.';
          $formUrl = tep_href_link(FILENAME_CHECKOUT_PROCESS, '', $request_type);
          if( isset($GLOBALS[$payment]->form_action_url) && tep_not_null($GLOBALS[$payment]->form_action_url) ){
            $formUrl = $GLOBALS[$payment]->form_action_url;
            $infoMsg = 'Please press the continue button to proceed to the payment processor page.';
          }

          $GLOBALS[$payment]->pre_confirmation_check();
          $GLOBALS[$payment]->confirmation();

          if( $GLOBALS[$payment]->code == 'paypal_ipn' ) {
            ob_start();
            $GLOBALS[$payment]->process_button();
            $hiddenFields = ob_get_contents();
            ob_end_clean();
          } else {
            $hiddenFields = $GLOBALS[$payment]->process_button();
            if( !tep_not_null($hiddenFields) ){
              //$hiddenFields = tep_break_array($_POST);
              foreach($_POST as $varName => $val){
                if (is_array($_POST[$varName])){
                  continue;
                  //foreach($_POST[$varName] as $varName2 => $val2){
                  //  $hiddenFields .= tep_draw_hidden_field($varName2, $val2);
                  //}
                } else {
                  $hiddenFields .= tep_draw_hidden_field($varName, $val);
                }
              }
            }
          }

          ob_start();
          include(DIR_WS_INCLUDES . 'checkout/checkout_splash.php');
          $html = ob_get_contents();
          ob_end_clean();
          return $html;
      }

      function createCustomerAccount(){
        //global $wishList;
        global $currencies, $onepage, $billto, $sendto, $shipping, $payment, $cart;
        global $customer_id, $customer_first_name, $customer_default_address_id, $customer_country_id, $customer_zone_id;

        if( !tep_session_is_registered('onepage') ) {
          tep_session_register('onepage');
          $onepage = false;
          return;
        }

        if( $onepage['logged_in'] == true ) {
          return;
        }

        if (SESSION_RECREATE == 'True') {
          tep_session_recreate();
        }

        $success = 'true';
        $errMsg = '';

        $password = tep_db_prepare_input($_POST['password']);

        //if( tep_not_null($password) || ONEPAGE_ACCOUNT_CREATE == 'true') {

          if( !tep_not_null($password) ) {
            $password = $new_password = tep_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
            $password_enc = tep_encrypt_password($new_password);
          } else {
            $password_enc = tep_encrypt_password($password);
          }

          $sql_data_array = array(
              'customers_firstname'     => $onepage['billing']['firstname'],
              'customers_lastname'      => $onepage['billing']['lastname'],
              'customers_email_address' => $onepage['customer']['email_address'],
              'customers_telephone'     => $onepage['customer']['telephone'],
              'customers_newsletter'    => $onepage['customer']['newsletter'],
              'customers_password'      => $password_enc
          );
          
          if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $onepage['billing']['gender'];
          if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($onepage['customer']['dob']);

          tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
          
          $customer_id = tep_db_insert_id();
          
          $sql_data_array = array(
              'customers_id'         => (int)$customer_id,
              'entry_firstname'      => $onepage['billing']['firstname'],
              'entry_lastname'       => $onepage['billing']['lastname'],
              'entry_street_address' => $onepage['billing']['street_address'],
              'entry_postcode'       => $onepage['billing']['postcode'],
              'entry_city'           => $onepage['billing']['city'],
              'entry_country_id'     => $onepage['billing']['country_id']
          );
          
          if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $onepage['billing']['gender'];
          if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $onepage['billing']['company'];
          if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $onepage['billing']['suburb'];
          if (ACCOUNT_STATE == 'true') {
            $state = $onepage['billing']['state'];
            $zone_name = '';
            
            $zone_id = 0;
            $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$onepage['billing']['country_id'] . "'");
            $check = tep_db_fetch_array($check_query);
            $entry_state_has_zones = ($check['total'] > 0);
            if ($entry_state_has_zones == true) {
              $zone_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$onepage['billing']['country_id'] . "' and zone_id = '" . (int)$onepage['billing']['zone_id'] . "'");
              if (tep_db_num_rows($zone_query) == 1) {
                $zone = tep_db_fetch_array($zone_query);
                $zone_id = $zone['zone_id'];
                $zone_name = $zone['zone_name'];
              }
            } else {
              return 'country has no zones <br><pre>' . var_dump($onepage);
            }
            
            if( $zone_id > 0) {
              $sql_data_array['entry_zone_id'] = $zone_id;
              $sql_data_array['entry_state'] = '';
            } else {
              return 'zone_id is 0 <br><pre>' . var_dump($onepage);
              //$sql_data_array['entry_zone_id'] = '0';
              //$sql_data_array['entry_state'] = $state;
            }
          }
          
          tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
          
          $address_id = tep_db_insert_id();
          
          $customer_default_address_id = $address_id;
          $customer_first_name = $onepage['billing']['firstname'];
          $customer_country_id = $onepage['billing']['country_id'];
          $customer_zone_id = $zone_id;
          
          if (isset($_POST['diffShipping'])){
            $sql_data_array = array(
                'customers_id'         => (int)$customer_id,
                'entry_firstname'      => $onepage['delivery']['firstname'],
                'entry_lastname'       => $onepage['delivery']['lastname'],
                'entry_street_address' => $onepage['delivery']['street_address'],
                'entry_postcode'       => $onepage['delivery']['postcode'],
                'entry_city'           => $onepage['delivery']['city'],
                'entry_country_id'     => $onepage['delivery']['country_id']
            );
            
            if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $onepage['delivery']['gender'];
            if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $onepage['delivery']['company'];
            if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $onepage['delivery']['suburb'];
            if (ACCOUNT_STATE == 'true') {
              $state = $onepage['delivery']['state'];
              $zone_name = '';
          
              $zone_id = 0;
              $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$onepage['delivery']['country_id'] . "'");
              $check = tep_db_fetch_array($check_query);
              $entry_state_has_zones = ($check['total'] > 0);
              if ($entry_state_has_zones == true) {
                $zone_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$onepage['delivery']['country_id'] . "' and zone_id = '" . (int)$state . "'");
                if (tep_db_num_rows($zone_query) == 1) {
                  $zone = tep_db_fetch_array($zone_query);
                  $zone_id = $zone['zone_id'];
                  $zone_name = $zone['zone_name'];
                }
              } else {
                return;
              }
              
              if ($zone_id > 0) {
                $sql_data_array['entry_zone_id'] = $zone_id;
                $sql_data_array['entry_state'] = '';
              } else {
                return;
                //$sql_data_array['entry_zone_id'] = '0';
                //$sql_data_array['entry_state'] = $state;
              }
            }
            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
            $address_id2 = tep_db_insert_id();
          }
          
          tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$address_id . "' where customers_id = '" . (int)$customer_id . "'");
          tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int)$customer_id . "', '0', now())");
          
          tep_session_register('customer_id');
          tep_session_register('customer_first_name');
          tep_session_register('customer_default_address_id');
          tep_session_register('customer_country_id');
          tep_session_register('customer_zone_id');

          if (!tep_session_is_registered('billto')) tep_session_register('billto');
          if (!tep_session_is_registered('sendto')) tep_session_register('sendto');
          $sendto = $billto = $address_id;
          if( isset($address_id2) ) {
            $sendto = $address_id2;
          }

          if (!tep_session_is_registered('payment')) {
            tep_session_register('payment');
            $payment = false;
          }

          if (!tep_session_is_registered('shipping')) {
            tep_session_register('shipping');
            $shipping = false;
          }

          $onepage['logged_in'] = true;
          $onepage['create_account'] = false;

          tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");
          // restore cart contents
          $cart->restore_contents();
          //$wishList->restore_wishlist();

          $Qcustomer = tep_db_query('select customers_firstname, customers_lastname, customers_email_address from ' . TABLE_CUSTOMERS . ' where customers_id = "' . (int)$customer_id . '"');
          $customer = tep_db_fetch_array($Qcustomer);

          // build the message content
          $customer_name = $customer['customers_firstname'] . ' ' . $customer['customers_lastname'];
                  
          if (ACCOUNT_GENDER == 'true') {
            if ($sql_data_array['entry_gender'] == ''){
              $email_text = sprintf(EMAIL_GREET_NONE, $customer['customers_firstname'] . ' ' . $customer['customers_lastname']);
            } elseif ($sql_data_array['entry_gender'] == 'm') {
              $email_text = sprintf(EMAIL_GREET_MR, $customer['customers_lastname']);
            } else {
              $email_text = sprintf(EMAIL_GREET_MS, $customer['customers_lastname']);
            }
          } else {
            $email_text = sprintf(EMAIL_GREET_NONE, $customer['customers_firstname']);
          }

          //-MS- Email Templates added
          $email_address = $onepage['customer']['email_address'];

          $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='new_account'");
          if( $email_array = tep_db_fetch_array($mail_query) ) {
            $right_col = tep_email_column(DEFAULT_EMAIL_ABSTRACT_ZONE_ID);
            $temp_array = array(
                                'CUSTOMER_NAME' => $customer_name,
                                'CUSTOMER_EMAIL' => $email_address,
                                'CUSTOMER_PASSWORD' => $password,
                                'STORE_NAME' => STORE_NAME,
                                'STORE_EMAIL_ADDRESS' => STORE_OWNER_EMAIL_ADDRESS,
                                'RIGHT_COL' => $right_col
                               );
            $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
            $email_array['subject'] = tep_email_templates_replace_entities($email_array['subject'], $temp_array);
            tep_mail($customer_name, $email_address, $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
          } else {
          //-MS- Email Templates added EOM

            $email_text .= EMAIL_WELCOME;
            
            if (isset($new_password)){
              $email_text .= 'You can log into your account using the following' . "\n" . 
                             'Username: ' . $onepage['customer']['email_address'] . "\n" . 
                             'Password: ' . $new_password . "\n\n";
            }
                    
            $email_text .= EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
            
            // Start - CREDIT CLASS Gift Voucher Contribution
            if( defined('NEW_SIGNUP_GIFT_VOUCHER_AMOUNT') && NEW_SIGNUP_GIFT_VOUCHER_AMOUNT > 0) {
                $coupon_code = create_coupon_code();
                tep_db_query("insert into " . TABLE_COUPONS . " (coupon_code, coupon_type, coupon_amount, date_created) values ('" . $coupon_code . "', 'G', '" . NEW_SIGNUP_GIFT_VOUCHER_AMOUNT . "', now())");
                $insert_id = tep_db_insert_id();
                tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . $insert_id ."', '0', 'Admin', '" . $customer['customers_email_address'] . "', now() )");
                
                $email_text .= sprintf(EMAIL_GV_INCENTIVE_HEADER, $currencies->format(NEW_SIGNUP_GIFT_VOUCHER_AMOUNT)) . "\n\n" .
                               sprintf(EMAIL_GV_REDEEM, $coupon_code) . "\n\n" .
                               EMAIL_GV_LINK . tep_href_link(FILENAME_GV_REDEEM, 'gv_no=' . $coupon_code,'NONSSL', false) . "\n\n";
            }
            
            if( defined('NEW_SIGNUP_DISCOUNT_COUPON') && NEW_SIGNUP_DISCOUNT_COUPON != '') {
                $coupon_code = NEW_SIGNUP_DISCOUNT_COUPON;
                $coupon_query = tep_db_query("select * from " . TABLE_COUPONS . " where coupon_code = '" . $coupon_code . "'");
                $coupon = tep_db_fetch_array($coupon_query);
                $coupon_id = $coupon['coupon_id'];        
                $coupon_desc_query = tep_db_query("select * from " . TABLE_COUPONS_DESCRIPTION . " where coupon_id = '" . $coupon_id . "' and language_id = '" . (int)$languages_id . "'");
                $coupon_desc = tep_db_fetch_array($coupon_desc_query);
                $insert_query = tep_db_query("insert into " . TABLE_COUPON_EMAIL_TRACK . " (coupon_id, customer_id_sent, sent_firstname, emailed_to, date_sent) values ('" . $coupon_id ."', '0', 'Admin', '" . $customer['customers_email_address'] . "', now() )");
                $email_text .= EMAIL_COUPON_INCENTIVE_HEADER .  "\n" .
                               sprintf("%s", $coupon_desc['coupon_description']) ."\n\n" .
                               sprintf(EMAIL_COUPON_REDEEM, $coupon['coupon_code']) . "\n\n" . "\n\n";
            }
            // End - CREDIT CLASS Gift Voucher Contribution
            
            tep_mail($customer_name, $customer['customers_email_address'], EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            
            if (isset($onepage['info']['order_id'])){
              tep_db_query('update ' . TABLE_ORDERS . ' set customers_id = "' . (int)$customer_id . '" where orders_id = "' . (int)$onepage['info']['order_id'] . '"');
              unset($onepage['info']['order_id']);
            }
          }

//        }
/*
        return '{
            success: true
        }';
*/

        return '{
            success: ' . $success . ',
            errMsg: "' . $errMsg . '"
        }';
      }
      
      function redeemCoupon($code){
        global $customer_id, $cc_id, $order_total_modules;

        if( !defined('TABLE_COUPONS') ) {
          return $this->displayError(false, 'Invalid Parameter');
        }

        $error = false;
        if ($code) {
          // get some info from the coupon table
          $coupon_query = tep_db_query("select coupon_id, coupon_amount, coupon_type, coupon_minimum_order,uses_per_coupon, uses_per_user, restrict_to_products,restrict_to_categories from " . TABLE_COUPONS . " where coupon_code='".$code."' and coupon_active='Y'");
          $coupon_result = tep_db_fetch_array($coupon_query);
          
          if ($coupon_result['coupon_type'] != 'G') {
            if (tep_db_num_rows($coupon_query) == 0) {
              $error = true;
              $errMsg = ERROR_NO_INVALID_REDEEM_COUPON;
            }
            
            $date_query = tep_db_query("select coupon_start_date from " . TABLE_COUPONS . " where coupon_start_date <= now() and coupon_code='".$code."'");
            if (tep_db_num_rows($date_query) == 0) {
              $error = true;
              $errMsg = ERROR_INVALID_STARTDATE_COUPON;
            }
            
            $date_query = tep_db_query("select coupon_expire_date from " . TABLE_COUPONS . " where coupon_expire_date >= now() and coupon_code='".$code."'");
            if (tep_db_num_rows($date_query) == 0) {
              $error = true;
              $errMsg = ERROR_INVALID_FINISDATE_COUPON;
            }
            
            $coupon_count = tep_db_query("select coupon_id from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $coupon_result['coupon_id']."'");
            $coupon_count_customer = tep_db_query("select coupon_id from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $coupon_result['coupon_id']."' and customer_id = '" . (int)$customer_id . "'");
            if (tep_db_num_rows($coupon_count) >= $coupon_result['uses_per_coupon'] && $coupon_result['uses_per_coupon'] > 0) {
              $error = true;
              $errMsg = ERROR_INVALID_USES_COUPON . $coupon_result['uses_per_coupon'] . TIMES;
            }
            
            if (tep_db_num_rows($coupon_count_customer) >= $coupon_result['uses_per_user'] && $coupon_result['uses_per_user'] > 0) {
              $error = true;
              $errMsg = ERROR_INVALID_USES_USER_COUPON . $coupon_result['uses_per_user'] . TIMES;
            }
            
            if ($error === false){
              if( !tep_session_is_registered('cc_id') ) tep_session_register('cc_id');
              $cc_id = $coupon_result['coupon_id'];
              $order_total_modules->pre_confirmation_check();
              
              return '{
                success: true
              }';
            }
          }
        }
        return '{
            success: false
        }';
      }

      function getAddressFormatted($type){
        global $order;
          switch($type){
              case 'sendto':
                  $address = $order->delivery;
              break;
              case 'billto':
                  $address = $order->billing;
              break;
          }
        return tep_address_format($address['format_id'], $address, false, '', "\n");
      }
      
      function verifyContents(){
        global $cart;
          // if there is nothing in the customers cart, redirect them to the shopping cart page
          if ($cart->count_contents() < 1) {
              tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
          }
      }
      
      function checkStock(){
        global $cart;
          $products = $cart->get_products();
          for ($i=0, $n=sizeof($products); $i<$n; $i++) {
              if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
                  tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
                  break;
              }
          }
      }
      
      function setDefaultSendTo(){
        global $sendto, $customer_default_address_id, $customer_id, $shipping;

        // if no shipping destination address was selected, use the customers own address as default
        if (!tep_session_is_registered('sendto')) {
          tep_session_register('sendto');
          $sendto = $customer_default_address_id;
        } else {
          // verify the selected shipping address
          $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
          $check_address = tep_db_fetch_array($check_address_query);
            
          if ($check_address['total'] != '1') {
            $sendto = $customer_default_address_id;
            if(tep_session_is_registered('shipping')) tep_session_unregister('shipping');
          }
        }
      }
      
      function setDefaultBillTo(){
        global $billto, $customer_default_address_id, $customer_id;
        // if no billing destination address was selected, use the customers own address as default
        if( !tep_session_is_registered('billto') ) {
          tep_session_register('billto');
          $billto = $customer_default_address_id;
        } else {
          $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$billto . "'");
          $check_address = tep_db_fetch_array($check_address_query);
                
          if ($check_address['total'] != '1') {
            $billto = $customer_default_address_id;
            if( tep_session_is_registered('payment') ) {
              tep_session_unregister('payment');
            }
          }
        }
      }
      
      function removeCCGV(){
        global $credit_covers, $cot_gv;
        // Start - CREDIT CLASS Gift Voucher Contribution
        if( tep_session_is_registered('credit_covers') ) tep_session_unregister('credit_covers');
        if( tep_session_is_registered('cot_gv') ) tep_session_unregister('cot_gv');
        // End - CREDIT CLASS Gift Voucher Contribution
      }

      function displayError($success=true, $errMsg='') {
        return '{
          success: ' . $success . ',
          errMsg: "' . $errMsg . '"
        }';
      }
  }
?>