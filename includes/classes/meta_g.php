<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Meta-Tags class
// Processes Tag tables, generates keywords, descriptions, titles
// Featuring:
// - Products, Categories, Manufacturers tags processing
// - Articles, Topics, Authors tags processing
// - Link Categories tags processing
// - Information Pages Unlimitied tags processing
// - Abstract Zones tags processing
// - GText Module tags processing
// - Dual Dictionary processing to include/exclude keywords
// - Multi-layer support by the keywords generator/mixer
// - Priority support for passed arguments and executed scripts
// - Auto builder for keywords/phrases
// - SEO-G assist with keywords density/emphasis
// - Added Google verify header (10/10/2007)
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class metaG {
    var $path, $query, $params_array, $error_level, $handler_flag, $script;

    function metaG() {
      global $PHP_SELF;
      $this->script = basename($PHP_SELF);
      $this->path = $this->query = '';
      $this->params_array = array();
      $this->query_array = array();
      $this->error_level = 0;
    }

    function create_safe_string($string, $separator=META_DEFAULT_WORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }

      //$string = preg_replace('/\s\s+/', ' ', trim($string));
      $string = trim($string);
	  $string = preg_replace("/[^0-9a-z]+/i", $separator, strtolower($string));
      $string = trim($string, $separator);
      $string = str_replace($separator . $separator . $separator, $separator, $string);
      $string = str_replace($separator . $separator, $separator, $string);
      if(META_DEFAULT_WORD_LENGTH > 1) {
        $words_array = explode($separator, $string);
        if( is_array($words_array) ) {
          for($i=0, $j=count($words_array); $i<$j; $i++) {
            if(strlen($words_array[$i]) < META_DEFAULT_WORD_LENGTH) {
              unset($words_array[$i]);
            }
          }
          if(count($words_array))
            $string = implode($separator, $words_array);
        }
      }
      return $string;
    }

    function create_keywords_array($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }
      $string = str_replace(META_DEFAULT_WORDS_SEPARATOR, $separator, $string);
      $string = $this->create_safe_string($string, $separator);
      $keywords_array = explode(META_DEFAULT_KEYWORDS_SEPARATOR, $string, META_MAX_KEYWORDS);
      return $keywords_array;
    }

    function create_keywords_string($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }
      $string = $this->strip_all_tags($string);
      $keywords_array = $this->create_keywords_array($string, $separator);
      $string = implode(',',$keywords_array);
      return $string;
    }

    function create_keywords_lexico($string, $separator=META_DEFAULT_KEYWORDS_SEPARATOR) {
      if( !tep_not_null($separator) ) {
        $separator = ' ';
      }

      $string = $this->strip_all_tags($string);
      $string = $this->create_safe_string($string);
      $phrases_array = explode($separator, $string);
      $keywords_array = array();
      $index = 0;
      foreach($phrases_array as $key => $value) {
        if( $index > META_MAX_KEYWORDS) break;
        if( is_numeric($value) ) continue;
        if( strlen($value) <= META_DEFAULT_WORD_LENGTH) continue;
        $check_query = tep_db_query("select meta_exclude_key from " . TABLE_META_EXCLUDE . " where meta_exclude_text like '%" . tep_db_input($value) . "%' and meta_exclude_status='1'");
        if( tep_db_num_rows($check_query) ) continue;

        $process = false;
        if( META_USE_LEXICO == 'true' ) {
          $check_query = tep_db_query("select meta_lexico_key as id, meta_lexico_text as text from " . TABLE_META_LEXICO . " where meta_lexico_text like '%" . tep_db_input(tep_db_prepare_input($value)) . "%' and meta_lexico_status='1' order by sort_id");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $tmp_string = $this->create_safe_string($check_array['text']);
            $md5_key = md5($tmp_string);
            $keywords_array[$check_array['id']] = $tmp_string;
            $process = true;
            $index++;
          }
        }
        if( !$process && META_USE_GENERATOR == 'true') {
          $tmp_string = $this->create_safe_string($value);
          $md5_key = md5($tmp_string);
          $keywords_array[$md5_key] = $tmp_string;
          $index++;
        }
      }
      $string = implode(',',$keywords_array);
      return $string;
    }

    function create_safe_description($string, $max_length = META_MAX_DESCRIPTION, $extension = '') {
      $string = $this->strip_all_tags($string);
      $string = $this->truncate_string($string, $max_length, $extension);
      return $string;
    }

// Following function taken from php.net and integrated for the META-G module
// $string        = "Truncates a string at a certain position without &raquo;cutting&laquo; words into senseless pieces.";
// $maxlength    = 75;
// $extension    = " ...";
// This Will output:
// "Truncates a string at a certain position without ?cutting? ..."
// Usage: echo truncate_string ($string, $maxlength, $extension);
    function truncate_string($string, $maxlength=META_MAX_DESCRIPTION, $extension='') {
      // Set the replacement for the "string break" in the wordwrap function
      $cutmarker = "**cut_here**";

      // Checking if the given string is longer than $maxlength
      if (strlen($string) > $maxlength) {
        // Using wordwrap() to set the cutmarker
        // NOTE: wordwrap (PHP 4 >= 4.0.2, PHP 5)
        $string = wordwrap($string, $maxlength, $cutmarker);

        // Exploding the string at the cutmarker, set by wordwrap()
        $string = explode($cutmarker, $string);

        // Adding $extension to the first value of the array $string, returned by explode()
        $string = $string[0] . $extension;
      }
      // returning $string
      return $string;
    }

// Following function taken from php.net and integrated for the META-G module
    function strip_all_tags($string) {
      $length=strlen($string);
      $newstring='';

      for ($tag=0, $i=0; $i < $length; $i++) {
        if ($string{$i} == '<'){
          $tag++;
          continue;
        }
        if ($string{$i} == '>') {
          if ($tag > 0 ) {
            $tag--;
          }
          continue;
        }
        if ($tag == 0){
          $newstring .= $string{$i};
        }
      }
      return $newstring;
    } 


    function get_script_tags(&$results_array) {
      global $languages_id;

      $result = false;
      $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_scripts' and meta_types_status='1'");
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
      } else {
        return $result;
      }

      $md5_key = md5($this->script);

      if( !is_array($results_array) ) {
        $results_array = array();
      }

      $scripts_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_SCRIPTS . " where meta_scripts_key = '" . tep_db_input(tep_db_prepare_input($md5_key)) . "' and language_id = '" . (int)$languages_id . "'");
      if( $scripts_array = tep_db_fetch_array($scripts_query) ) {
        $results_array[$index_key] = array(
          'type' => 'meta_scripts',
          'title' => $scripts_array['meta_title'],
          'keywords' => $scripts_array['meta_keywords'],
          'text' => $scripts_array['meta_text']
        );
        $result = true;
      } else {
        //$this->auto_builder($inner[0], $inner[1]);
      }
      return $result;
    }

    function get_meta_tags($params_array) {
      global $category_depth;

      $tmp_array = array();
      if( tep_not_null(META_GOOGLE_VERIFY) && $this->script == FILENAME_DEFAULT && $category_depth == 'top' ) {
        $tmp_array['google_verify'] = '<meta name="verify-v1" content="' . META_GOOGLE_VERIFY . '" />';
      }

      $tags_array = $this->get_tags_info($params_array);
      if( !is_array($tags_array) || !count($tags_array) ) {
        $string = substr($this->script, 0, -4);
        $title = $this->create_safe_string($string);
        $tmp_array['title'] = '<title>' . STORE_NAME . ' ' . $title . '</title>';
        $tmp_array['author'] = '<meta name="author" content="' . STORE_NAME . '" />';
        $meta_string = implode("\n", $tmp_array) . "\n";
        return $meta_string;
      }

      $tmp_array['keywords'] = '';
      $tmp_array['text'] = '';
      $tmp_array['title'] = '<title>';
      foreach($tags_array as $key => $value) {
//        if( !isset($tmp_array['title']) && tep_not_null($value['title']) ) {
//          $tmp_array['title'] = '<title>' . ucwords($value['title']) . '</title>';
//        }
        $tmp_array['title'] .= ucwords($value['title']) . '-';
        $tmp_array['keywords'] .= $value['keywords'] . ',';
        $tmp_array['text'] .= $value['text'] . '.';
      }
      $tmp_array['title'] = substr($tmp_array['title'], 0, -1);
      $tmp_array['title'] .= '</title>';

      if( strlen($tmp_array['keywords']) ) {
        $tmp_array['keywords'] = substr($tmp_array['keywords'], 0, -1);
      }
      if( strlen($tmp_array['text']) ) {
        $tmp_array['text'] = substr($tmp_array['text'], 0, -1);
      }

      $tmp_array['keywords'] = '<meta name="keywords" content="' . $tmp_array['keywords'] . '" />';
      $tmp_array['text'] = '<meta name="description" content="' . $tmp_array['text'] . '" />';
      $tmp_array['author'] = '<meta name="author" content="' . STORE_NAME . '" />';
      $meta_string = implode("\n", $tmp_array) . "\n";
      return $meta_string;
    }

    // Get tags for parameters/scripts
    function get_tags_info($params_array) {
      global $languages_id;

      $results_array = array();
      $flags_array = array('other' => false);
      $result = $this->get_script_tags($results_array);
      if( !$result ) {
        return $results_array;
      }

      if( !is_array($params_array) ) {
        return $results_array;
      }
      foreach ($params_array as $key => $value) {
        switch($key) {
          //case 'pID':
          case 'products_id':
            if( isset($flags_array['products_id']) || !tep_not_null($value) || $value == 0) break;

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_products' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_PRODUCTS . " where products_id = '" . (int)$value . "' and language_id = '" . (int)$languages_id . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_products',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;
          case 'cPath':
            if( isset($flags_array['cpath']) || !tep_not_null($value) || $value == '0') break;
            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_categories' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $path_flag = false;
            $path_link = explode('_', $value);

            $categories_id = $path_link[count($path_link)-1];
            if( !is_numeric($categories_id) ) {
              break;
            }

            $this->auto_builder($key, $categories_id);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_CATEGORIES . " where categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages_id . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_categories',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;
          case 'manufacturers_id':
            if( isset($flags_array['manufacturers_id']) || !tep_not_null($value) || $value == 0 ) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_manufacturers' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_MANUFACTURERS . " where manufacturers_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_manufacturers',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;

          case 'abz_id':
            if( isset($flags_array['abz_id']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_abstract' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_ABSTRACT . " where abstract_zone_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_abstract',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;
          case 'gtext_id':
            if( isset($flags_array['gtext_id']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_gtext' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_gtext',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;

          case 'auctions_id':
            if( isset($flags_array['auctions_id']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_auctions' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                'type' => 'meta_auctions',
                'title' => $tags['meta_title'],
                'keywords' => $tags['meta_keywords'],
                'text' => $tags['meta_text']
              );
            }
            $flags_array[$key] = $value;
            break;

          case 'auctions_group_id':
            if( isset($flags_array['auctions_group_id']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_auctions_group' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                'type' => 'meta_auctions_group',
                'title' => $tags['meta_title'],
                'keywords' => $tags['meta_keywords'],
                'text' => $tags['meta_text']
              );
            }
            $flags_array[$key] = $value;
            break;

          case 'range_id':
            if( isset($flags_array['range_id']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }

            $check_query = tep_db_query("select sort_order, meta_types_linkage from " . TABLE_META_TYPES . " where meta_types_class='meta_numeric_ranges' and meta_types_status='1'");
            if( $check_array = tep_db_fetch_array($check_query) ) {
              $index_key = $check_array['sort_order'] . '_' . $check_array['meta_types_linkage'];
            } else {
              break;
            }

            $this->auto_builder($key, $value);
            $tags_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_PRICE_RANGES . " where numeric_ranges_id = '" . (int)$value . "'");
            if( $tags = tep_db_fetch_array($tags_query) ) {
              $results_array[$index_key] = array(
                                       'type' => 'meta_gtext',
                                       'title' => $tags['meta_title'],
                                       'keywords' => $tags['meta_keywords'],
                                       'text' => $tags['meta_text']
                                      );
            }
            $flags_array[$key] = $value;
            break;

          case 'page': 
            if( isset($flags_array['page']) || !tep_not_null($value) || $value == 0) break;
            if( !is_numeric($value) ) {
              break;
            }
            $index_key = '99' . '_' . '-1';

            $results_array[$index_key] = array(
                                       'type' => 'page',
                                       'title' => 'Page-' . $value,
                                       'keywords' => '',
                                       'text' => ''
                                      );

            $flags_array[$key] = $value;
           break;
          default:
            //$this->assign_default($params_array, $value);
            $flags_array['other'] = true;
            break;
        }
      }
      if( count($results_array) ) {
        $this->resolve_linkage($results_array);

        //asort($results_array, SORT_NUMERIC);
        //$results_array = array_keys($results_array);
        //$params_array = array_merge($results_array, $params_array);
        //$result = 1;
      }
      $other = $flags_array['other'];
      return $results_array;

    }

    function resolve_linkage(&$results_array) {
      $keys_array = $link_array = array();
      foreach($results_array as $key => $value) {
        list($sort, $link) = preg_split("/_/", $key, 2);
        $keys_array[$sort] = $value;
        $link_array[$sort] = $link;
      }

      ksort($keys_array, SORT_NUMERIC);
      ksort($link_array, SORT_NUMERIC);
      foreach($link_array as $key => $value) {
        if( $value < 0 )
          continue;

        if( !isset($reduce) && $value > 0 ) {
          $reduce = $value;
          continue;
        }
        if($reduce != $value && $value > 0) {
          unset($keys_array[$key]);
        }
      }
      $results_array = $keys_array;
    }


    function auto_builder($entity, $id) {
      global $languages_id;

      if( META_BUILDER == 'false' )
        return;

      switch($entity) {
        case 'products_id':
          $check_query = tep_db_query("select products_id from " . TABLE_META_PRODUCTS . " where products_id = '" . (int)$id . "' and language_id = '" . (int)$languages_id . "'");
          if( tep_db_num_rows($check_query) ) return;

          $tags_query = tep_db_query("select products_name, products_description from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$id . "' and language_id = '" . (int)$languages_id . "'");
          if( tep_db_num_rows($tags_query) ) {
            $tags_array = tep_db_fetch_array($tags_query);

            $meta_name = $this->create_safe_string($tags_array['products_name']);
            $meta_keywords = $this->create_keywords_lexico($tags_array['products_description']);
            $meta_text = $this->create_safe_description($tags_array['products_description']);

            $sql_data_array = array(
              'products_id' => (int)$id,
              'language_id' => (int)$languages_id,
              'meta_title' => tep_db_prepare_input($meta_name),
              'meta_keywords' => tep_db_prepare_input($meta_keywords),
              'meta_text' => tep_db_prepare_input($meta_text)
            );
            tep_db_perform_insert(TABLE_META_PRODUCTS, $sql_data_array);
          }
          break;
        case 'cPath':
          $check_query = tep_db_query("select categories_id from " . TABLE_META_CATEGORIES . " where categories_id = '" . (int)$id . "' and language_id = '" . (int)$languages_id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $categories_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where language_id = '" . (int)$languages_id . "' and categories_id = '" . (int)$id . "'");
          if( !tep_db_num_rows($categories_query) ) return;
          $categories_array = tep_db_fetch_array($categories_query);

          $meta_name = $this->create_safe_string($categories_array['categories_name']);
          $keywords_array = array();
          $meta_keywords = '';
          $meta_text = '';
          $tags_query = tep_db_query("select pd.products_name from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p2c.products_id) where p2c.categories_id = '" . (int)$id . "' and pd.language_id = '" . (int)$languages_id . "'");
          while( $tags_array = tep_db_fetch_array($tags_query) ) {
            if( count($keywords_array) < META_MAX_KEYWORDS ) {
              $tmp_string = $this->create_safe_string($tags_array['products_name']);
              $keywords_array[$tmp_string] = $tmp_string;
            }
            if( strlen($meta_text) < META_MAX_DESCRIPTION ) {
              $meta_text .= $this->create_safe_description($tags_array['products_name']) . ' ';
            }
          }

          if( count($keywords_array) ) {
            $meta_keywords = implode(',',$keywords_array);
          } else {
            $meta_keywords = $meta_name;
          }
          if( strlen($meta_text) ) {
            $meta_text = substr($meta_text, 0, -1);
          } else {
            $meta_text = $meta_name;
          }

          $sql_data_array = array(
            'categories_id' => (int)$id,
            'language_id' => (int)$languages_id,
            'meta_title' => tep_db_prepare_input($meta_name),
            'meta_keywords' => tep_db_prepare_input($meta_keywords),
            'meta_text' => tep_db_prepare_input($meta_text)
          );
          tep_db_perform_insert(TABLE_META_CATEGORIES, $sql_data_array);
          break;
        case 'manufacturers_id':
          $check_query = tep_db_query("select manufacturers_id from " . TABLE_META_MANUFACTURERS . " where manufacturers_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $manufacturers_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$id . "'");
          if( !tep_db_num_rows($manufacturers_query) ) return;

          $manufacturers_array = tep_db_fetch_array($manufacturers_query);
          $meta_name = $this->create_safe_string($manufacturers_array['manufacturers_name']);
          $keywords_array = array();
          $meta_keywords = '';
          $meta_text = '';
          $tags_query = tep_db_query("select pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) where p.manufacturers_id = '" . (int)$id . "' and pd.language_id = '" . (int)$languages_id . "'");
          while( $tags_array = tep_db_fetch_array($tags_query) ) {
            if( count($keywords_array) < META_MAX_KEYWORDS ) {
              $tmp_string = $this->create_safe_string($tags_array['products_name']);
              $keywords_array[$tmp_string] = $tmp_string;
            }

            if( strlen($meta_text) < META_MAX_DESCRIPTION ) {
              $meta_text .= $this->create_safe_description($tags_array['products_name']) . ' ';
            }
          }

          if( count($keywords_array) ) {
            $meta_keywords = implode(',',$keywords_array);
          } else {
            $meta_keywords = $meta_name;
          }
          if( strlen($meta_text) ) {
            $meta_text = substr($meta_text, 0, -1);
          } else {
            $meta_text = $meta_name;
          }

          $sql_data_array = array(
            'manufacturers_id' => (int)$id,
            'meta_title' => tep_db_prepare_input($meta_name),
            'meta_keywords' => tep_db_prepare_input($meta_keywords),
            'meta_text' => tep_db_prepare_input($meta_text)
          );
          tep_db_perform_insert(TABLE_META_MANUFACTURERS, $sql_data_array);
          break;

        case 'abz_id':
          $check_query = tep_db_query("select abstract_zone_id from " . TABLE_META_ABSTRACT . " where abstract_zone_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;

          $abstract_query = tep_db_query("select azt.abstract_types_class, azt.abstract_types_table, abstract_zone_name, abstract_zone_desc from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " azt on (az.abstract_types_id=azt.abstract_types_id) where azt.abstract_types_status='1' and az.abstract_zone_id = '" . (int)$id . "'");
          if( !tep_db_num_rows($abstract_query) ) return;

          $abstract_array = tep_db_fetch_array($abstract_query);
          $meta_name = $this->create_safe_string($abstract_array['abstract_zone_name']);
          $meta_text = $this->create_safe_description($abstract_array['abstract_zone_desc']);
          $keywords_array = array();
          $meta_keywords = '';

          switch($abstract_array['abstract_types_class']) {
            case 'products_zones':
            case 'products_control_zones':
            case 'accessory_zones':
              $keywords_array = $this->get_group_products($id, $abstract_array['abstract_types_table']);
              break;
              break;
            case 'attributes_zones':
              break;
            case 'generic_zones':
              $keywords_array = $this->get_group_text($id, $abstract_array['abstract_types_table']);
              break;
            default:
              break;

          }

          if( count($keywords_array) ) {
            $meta_keywords = implode(',',$keywords_array);
          } else {
            $meta_keywords = $meta_name;
          }

          if( !strlen($meta_text) ) {
            $meta_text = $meta_name;
          }

          $sql_data_array = array(
            'abstract_zone_id' => (int)$id,
            'meta_title' => tep_db_prepare_input($meta_name),
            'meta_keywords' => tep_db_prepare_input($meta_keywords),
            'meta_text' => tep_db_prepare_input($meta_text)
          );
          tep_db_perform_insert(TABLE_META_ABSTRACT, $sql_data_array);
          break;
        case 'gtext_id':
          $check_query = tep_db_query("select gtext_id from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) return;

          $tags_query = tep_db_query("select gtext_title, gtext_description from " . TABLE_GTEXT . " where gtext_id = '" . (int)$id . "'");
          if( tep_db_num_rows($tags_query) ) {
            $tags_array = tep_db_fetch_array($tags_query);

            $meta_name = $this->create_safe_string($tags_array['gtext_title']);
            $meta_keywords = $this->create_keywords_lexico($tags_array['gtext_description']);
            $meta_text = $this->create_safe_description($tags_array['gtext_description']);

            $sql_data_array = array(
              'gtext_id' => (int)$id,
              'language_id' => (int)$languages_id,
              'meta_title' => tep_db_prepare_input($meta_name),
              'meta_keywords' => tep_db_prepare_input($meta_keywords),
              'meta_text' => tep_db_prepare_input($meta_text)
            );
            tep_db_perform_insert(TABLE_META_GTEXT, $sql_data_array);
          }
          break;
        case 'auctions_id':
          $check_query = tep_db_query("select auctions_id from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) return;

          $tags_query = tep_db_query("select auctions_name, auctions_description from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$id . "'");
          if( tep_db_num_rows($tags_query) ) {
            $tags_array = tep_db_fetch_array($tags_query);
            $meta_name = $this->create_safe_string($tags_array['auctions_name']);
            $meta_keywords = $this->create_keywords_lexico($tags_array['auctions_description']);
            $meta_text = $this->create_safe_description($tags_array['auctions_description']);

            $sql_data_array = array(
              'auctions_id' => (int)$id,
              'language_id' => (int)$languages_id,
              'meta_title' => tep_db_prepare_input($meta_name),
              'meta_keywords' => tep_db_prepare_input($meta_keywords),
              'meta_text' => tep_db_prepare_input($meta_text)
            );
            tep_db_perform_insert(TABLE_META_AUCTIONS, $sql_data_array);
          }
          break;

        case 'auctions_group_id':
          $check_query = tep_db_query("select auctions_group_id from " . TABLE_META_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) return;

          $tags_query = tep_db_query("select auctions_group_name, auctions_group_desc from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$id . "'");
          if( tep_db_num_rows($tags_query) ) {
            $tags_array = tep_db_fetch_array($tags_query);
            $meta_name = $this->create_safe_string($tags_array['auctions_group_name']);
            $meta_keywords = $this->create_keywords_lexico($tags_array['auctions_group_desc']);
            $meta_text = $this->create_safe_description($tags_array['auctions_group_desc']);

            $sql_data_array = array(
              'auctions_group_id' => (int)$id,
              'language_id' => (int)$languages_id,
              'meta_title' => tep_db_prepare_input($meta_name),
              'meta_keywords' => tep_db_prepare_input($meta_keywords),
              'meta_text' => tep_db_prepare_input($meta_text)
            );
            tep_db_perform_insert(TABLE_META_AUCTIONS_GROUP, $sql_data_array);
          }
          break;

        case 'range_id':
          $check_query = tep_db_query("select numeric_ranges_id from " . TABLE_META_PRICE_RANGES . " where numeric_ranges_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) return;

          $tags_query = tep_db_query("select numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " where numeric_ranges_id = '" . (int)$id . "'");
          if( tep_db_num_rows($tags_query) ) {
            $tags_array = tep_db_fetch_array($tags_query);

            $meta_name = $this->create_safe_string($tags_array['gtext_title']);
            $meta_keywords = $this->create_keywords_lexico($tags_array['numeric_ranges_desc']);
            $meta_text = $this->create_safe_description($tags_array['numeric_ranges_desc']);

            $sql_data_array = array(
              'numeric_ranges_id' => (int)$id,
              'language_id' => (int)$languages_id,
              'meta_title' => tep_db_prepare_input($meta_name),
              'meta_keywords' => tep_db_prepare_input($meta_keywords),
              'meta_text' => tep_db_prepare_input($meta_text)
            );
            tep_db_perform_insert(TABLE_META_PRICE_RANGES, $sql_data_array);
          }
          break;

        default:
          break;
      }
    }

    function get_group_text($zone_id, $table) {
      global $languages_id;

      $products_array = array();
      $text_array = array();

      $tables_array = explode(',', $table);
      if( !is_array($tables_array) ) {
        return $text_array;
      }

      if( isset($tables_array[1]) ) {
        $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) left join " . tep_db_input(tep_db_prepare_input($tables_array[1])) . " gtp on (gtp.products_id=p.products_id) where gtp.abstract_zone_id = '" . (int)$zone_id . "' and p.products_status = '1' and pd.language_id = '" . (int)$languages_id . "'");
        while( $product = tep_db_fetch_array($products_query) ) {
          $text_array[$product['products_id']] = $this->create_safe_string($product['products_name']);
        }
      }

      $text_query = tep_db_query("select gtd.gtext_id, gt.gtext_title from " . TABLE_GTEXT . " gt left join " . tep_db_prepare_input(tep_db_input($tables_array[0])) . " gtd on (gtd.gtext_id=gt.gtext_id) where gtd.abstract_zone_id = '" . (int)$zone_id . "' and gt.status='1' order by gtd.sequence_order");
      while($text = tep_db_fetch_array($text_query) ) { 
        $text_array['txt_' . $text['gtext_id']] = $this->create_safe_string($text['gtext_title']);
        if( count($text_array) > META_MAX_KEYWORDS ) {
          break;
        }
      }
      return $text_array;
    }

    function get_group_products($zone_id, $table) {
      global $languages_id;

      $products_array = array();
      $zone_query = tep_db_query("select products_id, categories_id from " . tep_db_prepare_input(tep_db_input($table)) . " where abstract_zone_id = '" . (int)$zone_id . "' and status_id='1' order by sort_id");

      while( $zone = tep_db_fetch_array($zone_query) ) {
        if( !$zone['categories_id'] && !$zone['products_id'] ) {
          return $products_array;
        }
        if( !$zone['products_id'] ) {
          $key_array = array();
          $this->set_key_products($key_array, $zone['categories_id']);
          if( count($key_array) ) {
            $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) where p.products_status = '1' and p.products_id in (" . implode(',', $key_array) . ") and pd.language_id = '" . (int)$languages_id . "'");
            while( $product = tep_db_fetch_array($products_query) ) {
              $products_array[$product['products_id']] = $this->create_safe_string($product['products_name']);
            }
          }
        } else {
          $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) where p.products_id = '" . (int)$zone['products_id'] . "' and p.products_status = '1' and pd.language_id = '" . (int)$languages_id . "'");
          if( $product = tep_db_fetch_array($products_query) ) {
            $products_array[$zone['products_id']] = $this->create_safe_string($product['products_name']);
          }
        }
        if( count($products_array) > META_MAX_KEYWORDS ) {
          break;
        }
      }
      return $products_array;
    }

    function set_key_products(&$key_array, $categories_id) {
      $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
      while($products = tep_db_fetch_array($products_query) ) {
        $key_array[$products['products_id']] = $products['products_id'];
      }
    }

  }
?>
