<?php
/*
  $Id: shopping_cart.php,v 1.35 2003/06/25 21:14:33 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Modifications by Asymmetrics
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// - Added Group Fields Support.
// - Added Attributes and Extra fields verification.
// - Added limits for quantity, total.
// - Fix for product attributes highlight.
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------

*/

  class shoppingCart {
    var $contents, $total, $weight, $cartID, $content_type;

    function shoppingCart() {
      $this->reset();
    }

    function restore_contents() {
      global $customer_id;

      if (!tep_session_is_registered('customer_id')) return false;

// insert current cart contents in database
      if (is_array($this->contents)) {
        reset($this->contents);
        while (list($products_id, ) = each($this->contents)) {
          $qty = $this->contents[$products_id]['qty'];
          $product_query = tep_db_query("select products_id from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input(tep_db_prepare_input($products_id)) . "'");
          if( !tep_db_num_rows($product_query) && !tep_is_auction_product($products_id) ) {
            tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, customers_basket_quantity, customers_basket_date_added) values ('" . (int)$customer_id . "', '" . tep_db_input(tep_db_prepare_input($products_id)) . "', '" . (int)$qty . "', '" . date('Ymd') . "')");
            if (isset($this->contents[$products_id]['attributes'])) {
              reset($this->contents[$products_id]['attributes']);
              while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
                tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, products_options_id, products_options_value_id) values ('" . (int)$customer_id . "', '" . tep_db_input(tep_db_prepare_input($products_id)) . "', '" . (int)$option . "', '" . (int)$value . "')");
              }
            }
//-MS- Group Fields Added
            if (isset($this->contents[$products_id]['groups']) && is_array($this->contents[$products_id]['groups'])) {
              $groups_string = serialize($this->contents[$products_id]['groups']);
              $sql_data_array = array(
                                      'customers_id' => (int)$customer_id,
                                      'products_id' => tep_db_prepare_input($products_id),
                                      'group_values' => tep_db_prepare_input($groups_string),
                                     );
              tep_db_perform(TABLE_CUSTOMERS_BASKET_GROUP_FIELDS, $sql_data_array);
            }
//-MS- Group Fields Added EOM
          } elseif( !tep_is_auction_product($products_id) ) {
            tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set customers_basket_quantity = '" . $qty . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
          }
        }
      }

// reset per-session cart contents, but not the database contents
      $this->reset(false);

      $products_query = tep_db_query("select products_id, customers_basket_quantity, auctions_history_id, final_price from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
      while ($products = tep_db_fetch_array($products_query)) {

        if( tep_is_auction_product($products['products_id']) ) {
          continue;
        }

        $this->contents[$products['products_id']] = array('qty' => $products['customers_basket_quantity']);
// attributes
        $attributes_query = tep_db_query("select products_options_id, products_options_value_id from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products['products_id']) . "'");
        while ($attributes = tep_db_fetch_array($attributes_query)) {
          $this->contents[$products['products_id']]['attributes'][$attributes['products_options_id']] = $attributes['products_options_value_id'];
        }

//-MS- Group Fields Added
        $group_fields_query = tep_db_query("select group_values from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input(tep_db_prepare_input($products['products_id'])) . "'");
        while ($group_fields = tep_db_fetch_array($group_fields_query)) {
          $tmp_array = unserialize($group_fields['group_values']);
          $products_id_string = tep_get_uprid($products['products_id'], $tmp_array);
          $this->contents[$products_id_string]['groups'] = $tmp_array;
          //$tmp_array = explode(',',$group_fields['group_values']);
/*
          $groups = array();
          foreach($tmp_array as $key => $value) {
            $values_query = tep_db_query("select group_values_id, group_values_name from " . TABLE_GROUP_VALUES . " where group_values_id = '" . (int)$value . "' and status_id = '1' order by sort_id");
            if($values_array = tep_db_fetch_array($values_query)) {
              $groups[$values_array['group_values_id']] = $value;
            }
          }

          if( count($groups) ) {
            $this->contents[$products['products_id']]['groups'] = $groups;
          }
*/
        }
//-MS- Group Fields Added EOM
      }
      $this->cleanup();
      $this->cartID = $this->generate_cart_id();
    }

    function load_auction_products() {
      global $customer_id;

      $this->auction_products = array();
      if (!tep_session_is_registered('customer_id')) {
        return $this->auction_products;
      }

      $basket_array = array();
      $basket_query_raw = "select auctions_history_id from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and auctions_history_id > 0";
      tep_query_to_array($basket_query_raw, $basket_array, 'auctions_history_id');

      if( empty($basket_array) ) {
        return $this->auction_products;
      }

      $auctions_array = array();
      $auctions_query_raw = "select auto_id, auctions_id, products_id, auctions_name, auctions_image, auctions_price, shipping_cost, products_name from " . TABLE_AUCTIONS_HISTORY . " where customers_id = '" . (int)$customer_id . "' and auto_id in (" . implode(',', array_keys($basket_array)) . ")";
      tep_query_to_array($auctions_query_raw, $auctions_array, 'auto_id');

      foreach($basket_array as $key => $value) {
        if( !isset($auctions_array[$key]) ) {
          tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where  customers_id = '" . (int)$customer_id . "' and auctions_history_id = '" . (int)$key . "'");
        }
      }

      foreach( $auctions_array as $key => $value ) {
        $final_price = tep_round($value['auctions_price']+$value['shipping_cost'],2);
        $this->auction_products[] = array(
          'id' => tep_get_auction_product_id(),
          'pid' => $value['products_id'],
          'name' => $value['products_name'],
          'model' => '',
          'image' => $value['auctions_image'],
          'price' => $final_price,
          'quantity' => 1,
          'weight' => 0,
          'groups' => '',
          'extra' => '',
          'final_price' => $final_price,
          'tax_class_id' => 2,
          'attributes' => '',
          'auctions_history_id' => $value['auto_id'],
          'auctions_id' => $value['auctions_id']
        );
      }
      return $this->auction_products;
    }

    function reset($reset_database = false) {
      global $customer_id;

      $this->auction_products = array();
      $this->contents = array();
      $this->total = 0;
      $this->weight = 0;
      $this->content_type = false;

      if (tep_session_is_registered('customer_id') && ($reset_database == true)) {
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");
//-MS- Group Fields Added
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "'");
//-MS- Group Fields Added EOM
      }

      unset($this->cartID);
      if (tep_session_is_registered('cartID')) tep_session_unregister('cartID');
    }

    function add_cart($products_id, $qty = '1', $attributes = '', $notify = true, $groups='', $replace_groups=false) {
      global $new_products_id_in_cart, $customer_id, $messageStack, $g_extra_fields;
//-MS- Check cart limits
      if( $qty > 99 ) {
        $this->reset(true);
        return;
      }

      if( $this->count_contents() > 19 ) {
        $this->calculate();
      }

      if( tep_is_auction_product($products_id) ) {
        return;
      }

//-MS- Check cart limits EOM

      $products_id_string = tep_get_uprid($products_id, $attributes);
      if( !empty($groups) ) {
        $products_id_string = tep_get_uprid($products_id, $groups);
      }

//-MS- added check for attributes.
      if( tep_has_product_attributes($products_id) ) {
         $error = false;
         if( !is_array($attributes) ) {
           $error = true;
         }
         if( !$error ) {
           foreach($attributes as $key => $value ) {
             if( !is_numeric($key) )
               continue;
             if(!tep_not_null($value) || $value == '0') {
               $error = true;
               break;
             } 
           }
         }
         if($error ) {
           $messageStack->add_session('product_info', ERROR_SELECT_PRODUCT_OPTION );
           tep_redirect(tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_id));
           return;
         }
      }
//-MS- added check for attributes. EOM

      $products_id = tep_get_prid($products_id_string);
      $attributes_pass_check = true;
      if (is_array($attributes)) {
        reset($attributes);
        while (list($option, $value) = each($attributes)) {
          if (!is_numeric($option) || !is_numeric($value)) {
            $attributes_pass_check = false;
            break;
          }
        }
      }

      if (is_numeric($products_id) && is_numeric($qty) && ($attributes_pass_check == true)) {
        $check_product_query = tep_db_query("select products_status from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
        $check_product = tep_db_fetch_array($check_product_query);

        if (($check_product !== false) && ($check_product['products_status'] == '1')) {
          if ($notify == true) {
//-MS- Mods to highlite items with attributes in cart
            $new_products_id_in_cart = $products_id_string;
//-MS- Mods to highlite items with attributes in cart EOM
            tep_session_register('new_products_id_in_cart');
          }

          if ($this->in_cart($products_id_string)) {
            $this->update_quantity($products_id, $qty, $attributes, $groups, $replace_groups);
          } else {
            $this->contents[$products_id_string] = array('qty' => $qty);
// insert into database
            if (tep_session_is_registered('customer_id')) tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, customers_basket_quantity, customers_basket_date_added) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$qty . "', '" . date('Ymd') . "')");

            if (is_array($attributes)) {
              reset($attributes);
              while (list($option, $value) = each($attributes)) {
                $this->contents[$products_id_string]['attributes'][$option] = $value;
// insert into database
                if (tep_session_is_registered('customer_id')) tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, products_options_id, products_options_value_id) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$option . "', '" . (int)$value . "')");
              }
            }

//-MS- Group Fields Added
            if( tep_session_is_registered('customer_id') ) {
              tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_prepare_input(tep_db_input($products_id_string)) . "'");
            }

            if( is_array($groups) ) {
              $this->contents[$products_id_string]['groups'] = $groups;
              if( tep_session_is_registered('customer_id') ) {
                //$keys_array = array_keys($groups);
                //$groups_string = implode(',', $keys_array);
                $groups_string = serialize($groups);
                $sql_data_array = array(
                                        'customers_id' => (int)$customer_id,
                                        'products_id' => tep_db_prepare_input($products_id_string),
                                        'group_values' => tep_db_prepare_input($groups_string),
                                       );
                tep_db_perform(TABLE_CUSTOMERS_BASKET_GROUP_FIELDS, $sql_data_array);
              }
            }
//-MS- Group Fields Added EOM
          }

          $this->cleanup();

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
          $this->cartID = $this->generate_cart_id();
        }
      }
    }
//-MS- Extra Fields Added
    function update_quantity($products_id, $quantity = '', $attributes = '', $groups='', $replace_groups=false) {
//-MS- Extra Fields Added EOM
      global $customer_id;
      $quantity = (int)$quantity;
      if( $quantity <= 0) return; // nothing needs to be updated if theres no quantity, so we return true..

      $products_id_string = tep_get_uprid($products_id, $attributes);
      $products_id = tep_get_prid($products_id_string);

      $attributes_pass_check = true;

      if (is_array($attributes)) {
        reset($attributes);
        while (list($option, $value) = each($attributes)) {
          if (!is_numeric($option) || !is_numeric($value)) {
            $attributes_pass_check = false;
            break;
          }
        }
      }
//-MS- Group Fields Added
      if( !is_array($groups) && !$replace_groups && isset($this->contents[$products_id_string]['groups']) && is_array($this->contents[$products_id_string]['groups']) ) {
        $groups = $this->contents[$products_id]['groups'];
      }
//-MS- Group Fields Added EOM
      if (is_numeric($products_id) && isset($this->contents[$products_id_string]) && is_numeric($quantity) && ($attributes_pass_check == true)) {
        $this->contents[$products_id_string] = array('qty' => $quantity);
// update database
        if( tep_session_is_registered('customer_id') && !tep_is_auction_product($products_id) ) {
          tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set customers_basket_quantity = '" . (int)$quantity . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "'");
        }

        if (is_array($attributes)) {
          reset($attributes);
          while (list($option, $value) = each($attributes)) {
            $this->contents[$products_id_string]['attributes'][$option] = $value;
// update database
            if (tep_session_is_registered('customer_id')) tep_db_query("update " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " set products_options_value_id = '" . (int)$value . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "' and products_options_id = '" . (int)$option . "'");
          }
        }
      }

//-MS- Group Fields Added
      if (is_array($groups)) {
        $this->contents[$products_id_string]['groups'] = $groups;
      } else {
        unset($this->contents[$products_id_string]['groups']);
        if( tep_session_is_registered('customer_id') ) {
          tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_prepare_input(tep_db_input($products_id_string)) . "'");
        }
      }
//-MS- Group Fields Added EOM
    }

    function cleanup() {
      global $customer_id;

      reset($this->contents);
      while (list($key,) = each($this->contents)) {
        if( !isset($this->contents[$key]['qty']) || $this->contents[$key]['qty'] < 1) {
          unset($this->contents[$key]);
// remove from database
          if (tep_session_is_registered('customer_id')) {
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
//-MS- Group Fields Added
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input(tep_db_prepare_input($key)) . "'");
//-MS- Group Fields Added EOM

          }
        }
      }
    }

    function count_contents() {  // get total number of items in cart 
      $total_items = count($this->load_auction_products());
      if (is_array($this->contents)) {
        reset($this->contents);
        while (list($products_id, ) = each($this->contents)) {
          $total_items += $this->get_quantity($products_id);
        }
      }
      return $total_items;
    }

    function get_quantity($products_id) {
      if (isset($this->contents[$products_id])) {
        return $this->contents[$products_id]['qty'];
      } else {
        return 0;
      }
    }

    function in_cart($products_id) {
      if (isset($this->contents[$products_id])) {
        return true;
      } else {
        return false;
      }
    }

    function remove($products_id) {
      global $customer_id;
      unset($this->contents[$products_id]);
// remove from database
      if (tep_session_is_registered('customer_id')) {
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
//-MS- Group Fields Added
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input(tep_db_prepare_input($products_id)) . "'");
//-MS- Group Fields Added EOM
      }

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
      $this->cartID = $this->generate_cart_id();
    }

    function remove_all() {
      $this->reset();
    }

    function get_product_id_list() {
      $product_id_list = '';
      if (is_array($this->contents)) {
        reset($this->contents);
        while (list($products_id, ) = each($this->contents)) {
          $product_id_list .= ', ' . $products_id;
        }
      }

      return substr($product_id_list, 2);
    }

    function calculate() {
//-MS- Extra Fields Added
      global $g_extra_fields, $g_group_fields;
//-MS- Extra Fields Added EOM
      $this->total = 0;
      $this->weight = 0;

      if (!is_array($this->contents) && empty($this->auction_products) ) return 0;

      for( $i=0, $j=count($this->auction_products); $i<$j; $i++) {
        $this->total += $this->auction_products[$i]['final_price'];
      }

      reset($this->contents);
      while (list($products_id, ) = each($this->contents)) {
        $qty = $this->contents[$products_id]['qty'];

        // products price
        $product_query = tep_db_query("select products_id, products_price, products_tax_class_id, products_weight from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
        if ($product = tep_db_fetch_array($product_query)) {
          $prid = $product['products_id'];
          $products_tax = tep_get_tax_rate($product['products_tax_class_id']);
          $products_price = $product['products_price'];
          $products_weight = $product['products_weight'];

          $specials_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$prid . "' and status = '1'");
          if (tep_db_num_rows ($specials_query)) {
            $specials = tep_db_fetch_array($specials_query);
            $products_price = $specials['specials_new_products_price'];
          }

          $this->total += tep_add_tax($products_price, $products_tax) * $qty;
//-MS- Total Check Added
          if( $this->total > 99999 ) {
            $this->reset(true);
            return;
          }
//-MS- Total Check Added EOM
          $this->weight += ($qty * $products_weight);
        }

// attributes price
        if (isset($this->contents[$products_id]['attributes'])) {
          reset($this->contents[$products_id]['attributes']);
          while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
            $attribute_price_query = tep_db_query("select options_values_price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$prid . "' and options_id = '" . (int)$option . "' and options_values_id = '" . (int)$value . "'");
            $attribute_price = tep_db_fetch_array($attribute_price_query);
            if ($attribute_price['price_prefix'] == '+') {
              $this->total += $qty * tep_add_tax($attribute_price['options_values_price'], $products_tax);
            } else {
              $this->total -= $qty * tep_add_tax($attribute_price['options_values_price'], $products_tax);
            }
          }
//-MS- Extra Fields Added
        } elseif(isset($this->contents[$products_id]['extra']) ) {
          $price = $g_extra_fields->get_price_from_brackets($products_id);
          $this->total += $qty * tep_add_tax($price, $products_tax);
        }
//-MS- Extra Fields Added EOM

//-MS- Group Fields Added
        if(isset($this->contents[$products_id]['groups']) ) {
          $price = $g_group_fields->get_price_from_group($this->contents[$products_id]['groups']);
          $this->total += $qty * tep_add_tax($price, $products_tax);
        }
//-MS- Group Fields Added EOM
      }
    }

    function attributes_price($products_id) {
      $attributes_price = 0;

      if (isset($this->contents[$products_id]['attributes'])) {
        reset($this->contents[$products_id]['attributes']);
        while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
          $attribute_price_query = tep_db_query("select options_values_price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$products_id . "' and options_id = '" . (int)$option . "' and options_values_id = '" . (int)$value . "'");
          $attribute_price = tep_db_fetch_array($attribute_price_query);
          if ($attribute_price['price_prefix'] == '+') {
            $attributes_price += $attribute_price['options_values_price'];
          } else {
            $attributes_price -= $attribute_price['options_values_price'];
          }
        }
      }

      return $attributes_price;
    }

    function get_products() {
      global $languages_id, $g_group_fields;

      $products_array = $this->load_auction_products();

      if(!is_array($this->contents) && empty($products_array) ) return false;

      reset($this->contents);
      while (list($products_id, ) = each($this->contents)) {
        $products_query = tep_db_query("select p.products_id, pd.products_name, p.products_model, p.products_image, p.products_price, p.products_weight, p.products_tax_class_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$products_id . "' and pd.products_id = p.products_id and pd.language_id = '" . (int)$languages_id . "'");
        if ($products = tep_db_fetch_array($products_query)) {
          $prid = $products['products_id'];
          $products_price = $products['products_price'];

          $specials_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$prid . "' and status = '1'");
          if (tep_db_num_rows($specials_query)) {
            $specials = tep_db_fetch_array($specials_query);
            $products_price = $specials['specials_new_products_price'];
          }

          $products_array[] = array('id' => $products_id,
                                    'name' => $products['products_name'],
                                    'model' => $products['products_model'],
                                    'image' => $products['products_image'],
                                    'price' => $products_price,
                                    'quantity' => $this->contents[$products_id]['qty'],
                                    'weight' => $products['products_weight'],
//-MS- Group Fields Added
                                    'groups' => (isset($this->contents[$products_id]['groups']) ? $this->contents[$products_id]['groups']:''),
                                    'extra' => (isset($this->contents[$products_id]['extra']) ? $this->contents[$products_id]['extra']:''),
                                    'final_price' => $products_price + $this->attributes_price($products_id)+(isset($this->contents[$products_id]['groups'])?$g_group_fields->get_price_from_group($this->contents[$products_id]['groups']):0),
//-MS- Group Fields Added EOM
                                    'tax_class_id' => $products['products_tax_class_id'],
                                    'attributes' => (isset($this->contents[$products_id]['attributes']) ? $this->contents[$products_id]['attributes'] : ''));
        }
      }

      return $products_array;
    }

    function show_total() {
      $this->calculate();

      return $this->total;
    }

    function show_weight() {
      $this->calculate();

      return $this->weight;
    }

    function generate_cart_id($length = 5) {
      return tep_create_random_value($length, 'digits');
    }

    function get_content_type() {
      $this->content_type = false;

      if ( (DOWNLOAD_ENABLED == 'true') && ($this->count_contents() > 0) ) {
        reset($this->contents);
        while (list($products_id, ) = each($this->contents)) {
          if (isset($this->contents[$products_id]['attributes'])) {
            reset($this->contents[$products_id]['attributes']);
            while (list(, $value) = each($this->contents[$products_id]['attributes'])) {
              $virtual_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . " pad where pa.products_id = '" . (int)$products_id . "' and pa.options_values_id = '" . (int)$value . "' and pa.products_attributes_id = pad.products_attributes_id");
              $virtual_check = tep_db_fetch_array($virtual_check_query);

              if ($virtual_check['total'] > 0) {
                switch ($this->content_type) {
                  case 'physical':
                    $this->content_type = 'mixed';

                    return $this->content_type;
                    break;
                  default:
                    $this->content_type = 'virtual';
                    break;
                }
              } else {
                switch ($this->content_type) {
                  case 'virtual':
                    $this->content_type = 'mixed';

                    return $this->content_type;
                    break;
                  default:
                    $this->content_type = 'physical';
                    break;
                }
              }
            }
          } else {
            switch ($this->content_type) {
              case 'virtual':
                $this->content_type = 'mixed';

                return $this->content_type;
                break;
              default:
                $this->content_type = 'physical';
                break;
            }
          }
        }
      } else {
        $this->content_type = 'physical';
      }

      return $this->content_type;
    }

    function unserialize($broken) {
      for(reset($broken);$kv=each($broken);) {
        $key=$kv['key'];
        if (gettype($this->$key)!="user function")
        $this->$key=$kv['value'];
      }
    }

  }
?>
