<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2012 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $text_query = tep_db_query("select gtext_id, gtext_description from " . TABLE_GTEXT . " where gtext_title = '" . tep_db_input($g_script) . "'");
  if( tep_db_num_rows($text_query) ) {
    $text_array = tep_db_fetch_array($text_query);
?>
          <div id="shome_right" class="extend">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array(
     'text' => $text_array['gtext_description'],
     //'class' => 'contentBoxContents',
     //'class' => 'bounder rpad',
    );
    new contentBox($info_box_contents);
?>
          </div>
<?php
  }
?>
