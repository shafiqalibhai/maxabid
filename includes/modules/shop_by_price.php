<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Shop by price module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  $ranges_query = tep_db_query("select numeric_ranges_id, numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " where status_id = '1' order by sort_id");
  if( tep_db_num_rows($ranges_query) ) {
?>

<!-- shop_by_price //-->
          <tr>
            <td valign="top">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array(
                                 'text' => BOX_HEADING_SHOP_BY_PRICE
                                );

    new contentBoxHeading($info_box_contents);

    $info_box_contents = array();

    while( $ranges_array = tep_db_fetch_array($ranges_query) ) {
      if( isset($_GET['range_id']) && $ranges_array['numeric_ranges_id'] == $_GET['range_id'] ) {
        $info_box_contents[] = array(
                                     'text' => '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $ranges_array['numeric_ranges_id']) . '" class="contentBoxBullet"><b>' . $ranges_array['numeric_ranges_desc'] . '</b></a>',
                                    );
      } else {
        $info_box_contents[] = array(
                                     'text' => '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $ranges_array['numeric_ranges_id']) . '" class="contentBoxBullet">' . $ranges_array['numeric_ranges_desc'] . '</a>',
                                    );
      }
    }
    new contentBox($info_box_contents, 'contentBoxBullet');
?>
            </td>
          </tr>
<!-- shop_by_price_eof //-->
<?php
  }
?>