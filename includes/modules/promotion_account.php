<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <div id="account_left" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'PERSONAL ACCOUNT',
   'class' => 'heavy contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();

  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . 'Account Edit' . '</a>'
  );
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . 'Account Orders' . '</a>'
  );
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK) . '">' . 'Address Book' . '</a>'
  );
  new contentBox($info_box_contents, 'contentBoxContents');

  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'INFORMATION',
   'class' => 'heavy contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $tmp_array = $cText->get_gtext_entries('Information Entries');
  foreach($tmp_array as $key => $value) {
    $info_box_contents[] = array(
      'class' => 'account_bullet',
      'text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $key) . '" title="' . $value['gtext_title'] . '">' . $value['gtext_title'] . '</a>'
    );
  }
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '">' . 'Contact ' . STORE_NAME . '</a>'
  );
  new contentBox($info_box_contents, 'contentBoxContents');

  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'FAQ',
   'class' => 'heavy contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $tmp_array = $cText->get_gtext_entries('Frequently Asked Questions');
  foreach($tmp_array as $key => $value) {
    $info_box_contents[] = array(
      'class' => 'account_bullet',
      'text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $key) . '" title="' . $value['gtext_title'] . '">' . $value['gtext_title'] . '</a>'
    );
  }
  new contentBox($info_box_contents, 'contentBoxContents');

?>
          </div>
