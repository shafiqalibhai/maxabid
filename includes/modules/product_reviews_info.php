<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Reviews Write module
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
?>
        <tr>
          <td>
<?php
define('MAX_REVIEWS', 10); 
define('NO_REVIEWS_TEXT', '<b><center><font size=2>' . sprintf('Be the first one to  comment %s', $product_info['products_name']) );
define('BOX_REVIEWS_HEADER_TEXT', 'COMMENTS');


  $reviews_query = tep_db_query("select r.reviews_id, r.customers_name, r.date_added, rd.reviews_text, r.reviews_rating FROM reviews r, reviews_description rd WHERE r.approved='1' and r.reviews_id = rd.reviews_id AND r.products_id = '" . (int)$_GET['products_id'] . "' AND rd.languages_id = '" . (int)$languages_id . "' ORDER BY r.date_added DESC LIMIT " . MAX_REVIEWS);

  $info_box_header = array();
  $info_box_header[] = array('text' => BOX_REVIEWS_HEADER_TEXT);
  new contentBoxHeading($info_box_header);

  $info_box_contents = array();

  while ($reviews = tep_db_fetch_array($reviews_query)) {
    $review_info = 
'                 <table border="0" cellspacing="0" cellpadding="2" class="main">' ."\n" . 
'                   <tr>' . "\n" . 
'                     <td><a href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS_INFO, 'products_id=' . (int)$_GET['products_id'] . '&reviews_id=' . $reviews['reviews_id']) . '"><b>' . $reviews['customers_name'] . '</b></a></td>' . "\n" . 
'                     <td class="smallText"><b>&nbsp;' . tep_date_short($reviews['date_added']) . '&nbsp;</b></td>' . "\n" . 
'                     <td>' . tep_image(DIR_WS_IMAGES . 'stars_' . $reviews['reviews_rating'] . '.gif' , sprintf(BOX_REVIEWS_TEXT_OF_5_STARS, $reviews['reviews_rating'])) . '</td>' . "\n" . 
'                   </tr>' . "\n" . 
'                 </table>' . "\n";

    $info_box_contents[] = array(
                                 'params' => 'class="main"',
                                 'text' => $review_info
                                );

    $info_box_contents[] = array(
                                 'params' => 'class="smallText"',
                                 'text' => $reviews['reviews_text']
                                );

/*
                                    'text' => '<a href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS_INFO, 'products_id=' . (int)$_GET['products_id'] . '&reviews_id=' . $reviews['reviews_id']) . '"><b>' . $reviews['customers_name'] . '</b>&nbsp;-&nbsp;' . tep_date_short($reviews['date_added']) . '&nbsp;' . tep_image(DIR_WS_IMAGES . 'stars_' . $reviews['reviews_rating'] . '.gif' , sprintf(BOX_REVIEWS_TEXT_OF_5_STARS, $reviews['reviews_rating'])) . '</a><br> ' . $reviews['reviews_text']);
*/
  }
  $index = count($info_box_contents);
  if(mysql_num_rows($reviews_query) > 0) {
    $info_box_contents[$index] = array('align' => 'left',
                                    'params' => 'class="smallText" valign="top"',
                                    'text' => '<a href="' . tep_href_link(FILENAME_PRODUCT_REVIEWS, 'products_id=' . (int)$_GET['products_id']) . '">Click to view All Reviews</a>'
                                   );
  } else {
    $info_box_contents[$index] = array('align' => 'left',
                                    'params' => 'class="smallText" valign="top"',
                                    'text' => NO_REVIEWS_TEXT
                                   );
  }
  new contentBox($info_box_contents);
?>
          </td>
        </tr>
