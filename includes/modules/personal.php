<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <div id="shome_right" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'MEMBERS LOGIN',
   'class' => 'contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array('form' => tep_draw_form('login', tep_href_link(FILENAME_CREATE_ACCOUNT, 'action=login', 'SSL')));

  $info_box_contents[] = array('text' => '<label for="email_address">E-Mail:</label>');
  $info_box_contents[] = array(
    'text' => tep_draw_input_field('email_address', '', 'class="wider" id="email_address"'),
    'style' => 'margin-right: 8px; margin-bottom: 8px;',
  );
  $info_box_contents[] = array('text' => '<label for="password">Password:</label>');
  $info_box_contents[] = array(
    'text' => tep_draw_password_field('password', '', 'class="wider" id="password"'),
    'style' => 'margin-right: 8px; margin-bottom: 16px;',
  );

  $info_box_contents[] = array(
    'text' => '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_LOGIN . '</a>',
    'style' => 'text-align: right; margin-right: 8px; margin-bottom: 8px;',
  );

/*
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="footerBoxContents">' . 'Shipping Info' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '" class="footerBoxContents">' . 'FAQ' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="contentBoxContents">' . 'My Account' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '" class="footerBoxContents">' . 'Contact Us' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_SHOPPING_CART) . '" class="footerBoxContents">' . 'Shopping Cart' . '</a>');
*/
  new contentBox($info_box_contents, 'contentBoxContents');

  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'FREE TRIAL',
   'class' => 'contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array(
    'text' => BOX_TEXT_SPECIAL
  );
  new contentBox($info_box_contents, 'contentBoxContents');

?>
          </div>