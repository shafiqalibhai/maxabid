<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Normal Bid processing
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  if( $action == 'end_auction') {
    tep_auction_expire_single($auctions_id);
    tep_auction_display_growl_end($auctions_id);
  } elseif( $action == 'set_auction' ) {

    $org_bid_step = $auction['bid_step'];

    $auction['bid_step'] /= 100;
    $auction['bid_step'] = tep_round($auction['bid_step'],2);
    $insert_bid = false;

/*
    $bid_value = tep_round((float)$_POST['bid_value'],2);
    if( $bid_value <= 0 ) {
      $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_BID_VALUE_ENTRY, $auction['auctions_name']);
      tep_auction_format_callback($output_result_array, true, true);
    }

    $last_bid_value = $bid_value;

    if( $bid_value <= $auction['start_price'] || ($auction['cap_price'] > 0 && $bid_value > $auction['cap_price']) ) {
      $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_BID_VALUE_BETWEEN, $auction['auctions_name'], tep_round($auction['start_price'],2), $auction['cap_price']);
      tep_auction_format_callback($output_result_array, true, true);
    } elseif ($bid_value <= $auction['start_price'] ) {
      $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_BID_VALUE_AT_LEAST, $auction['auctions_name'], tep_round($auction['start_price']+$auction['bid_step'],2));
      tep_auction_format_callback($output_result_array, true, true);
    }
*/

    $check_query = tep_db_query("select customers_id, bid_price from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auctions_id . "'");
    if( !tep_db_num_rows($check_query) ) {
      $bid_value = tep_round($auction['start_price']+0.01,2);
      $bid_display = tep_round($auction['start_price'],2);
      $insert_bid = true;
      //$last_bid_value = $auction['start_price'];
    } else {
      $check_array = tep_db_fetch_array($check_query);

      if( !$auction['auctions_overbid'] && $customer_id == $check_array['customers_id'] ) {
        $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_AUCTION_OVERBID, $auction['auctions_name']);
        tep_auction_format_callback($output_result_array, true, true);
      }
      $bid_value = tep_round($check_array['bid_price']+0.01,2);
      $bid_display = $bid_value;
/*
      $last_bid_value = $check_array['bid_price'];
      if( $bid_value <= $last_bid_value ) {
        $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_BID_VALUE_AT_LEAST, $auction['auctions_name'], tep_round($check_array['bid_price']+0.01,2));
        $output_result_array[$auctions_id]['input'] = tep_round($last_bid_value+0.01,2);
        tep_auction_format_callback($output_result_array, true, true);
      }
*/
    }

    if( $cbids_array['customers_bids'] < $org_bid_step ) {
      $output_result_array[$auctions_id]['growl'][] = sprintf(ERROR_INSUFFICIENT_BIDS, $org_bid_step, $auction['auctions_name']);
      tep_auction_format_callback($output_result_array, true, true);
    }

    $customers_query = tep_db_query("select customers_nickname, customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customers_array = tep_db_fetch_array($customers_query);

    $now_time = date('Y-m-d H:i:s');
    $signature = md5($now_time . $customers_array['customers_nickname'] . $bid_value);

    if( $insert_bid ) {
      $sql_data_array = array(
        'customers_id' => $customer_id,
        'customers_nickname' => $customers_array['customers_nickname'],
        'signature' => $signature,
        'bid_price' => $bid_value,
        'auctions_id' => $auctions_id,
        'bid_count' => 1,
        'last_modified' => $now_time,
      );
      tep_db_perform(TABLE_AUCTIONS_BID, $sql_data_array);

    } else {
      $sql_data_array = array(
        'customers_id' => $customer_id,
        'customers_nickname' => $customers_array['customers_nickname'],
        'signature' => $signature,
        'bid_price' => $bid_value,
        'last_modified' => $now_time,
      );
      tep_db_perform(TABLE_AUCTIONS_BID, $sql_data_array, 'update', "auctions_id = '" . (int)$auctions_id . "'");
      tep_db_query("update " . TABLE_AUCTIONS_BID . " set bid_count=bid_count+1 where auctions_id = '" . (int)$auctions_id . "'");
    }

    $sql_data_array = array(
      'customers_id' => $customer_id,
      'bid_price' => $bid_value,
      'auctions_id' => $auctions_id,
      'date_added' => $now_time,
    );
    tep_db_perform(TABLE_AUCTIONS_CUSTOMER, $sql_data_array);

    tep_db_query("update " . TABLE_CUSTOMERS . " set customers_bids=customers_bids-'" . (int)$org_bid_step . "' where customers_id = '" . (int)$customer_id . "'");

    if( $auction['max_bids'] && $auction['bids_left'] ) {
      $count_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auctions_id . "'");
      $count_array = tep_db_fetch_array($count_query);

      $bids_left = max(0, $auction['max_bids'] - $count_array['total']);
      $output_result_array['bids_left'] = $bids_left = '<span class="heavy">' . sprintf(TEXT_INFO_AUCTION_BIDS_REMAIN, $bids_left) . '</span>';
    }

    $output_result_array['bids_notice'] = $customers_array['customers_bids'] - (int)$org_bid_step;

    if( $sticky_poll ) {
      $output_result_array[$auctions_id]['growl'][] = '<span class="heavy" style="color: #00FFFF">' . sprintf(TEXT_INFO_BID_NEW_SELF, $bid_value, $auction['auctions_name']) . '</span>';
    }

    $output_result_array[$auctions_id]['note'] = sprintf(TEXT_INFO_LAST_LEADER, tep_truncate_string($customers_array['customers_nickname']));
    $output_result_array[$auctions_id]['backup'] = $signature;
/*
    if( $auction['auctions_overbid'] ) {
      $output_result_array[$auctions_id]['input'] = tep_round($bid_value+0.01,2);
    } else {
      $output_result_array[$auctions_id]['input'] = $bid_value;
    }
*/
    $output_result_array[$auctions_id]['bid_display'] = $bid_value;

    $output_result_array[$auctions_id]['bg_flash'] = 1;
     
   
   
    
              
                 
    $output_result_array[$auctions_id]['latest_update'] = latest_update_block($auctions_id);
    //$output_result_array[$auctions_id]['background'] = '#FF9999';
    //tep_auction_format_callback($output_result_array, true);
  }  
  
  function latest_update_block($auctions_id) {
  //die('asdasd');
      $check_query = tep_db_query("select * from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
      
  $auction_info = tep_db_fetch_array($check_query);
   $bids_query = tep_db_query("select customers_id, customers_nickname, bid_price, last_modified, signature from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auctions_id . "'");
        if( !tep_db_num_rows($bids_query) ) {

          $auction_price = tep_round($auction_info['start_price'], 2);

          $bids_array = array(
            'customers_id' => 0,
            'customers_nickname' => '',
            'bid_price' => $auction_price,
            'last_modified' => date('Y-m-d H:i:s'),
          );

          if( $auction_info['auctions_type'] == 1 ) {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s'));
          } else {
            $bids_array['signature'] = md5(date('Y-m-d H:i:s') . $auction_price);
          }

        } else {
          $bids_array = tep_db_fetch_array($bids_query);
        }             
    $current_price_array[] = array(
            'customers_id' => 0,
            'auctions_price' => $bids_array['bid_price'],
            'shipping_cost' => $auction_info['shipping_cost'],
          );          
          
          //$input_line = TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;' . $symbol . tep_draw_input_field('auction_bid[' . $auction_info['auctions_id'] . ']', tep_round($bids_array['bid_price']+(1/100),2), 'size="4" class="bid_inputs" id="auction_input_' . $auction_info['auctions_id'] . '"');
                      
     $tiers_query = tep_db_query("select count(*) as total from auctions_tier where auctions_id = '" . (int)$auctions_id . "'");  
  $tiers_array = tep_db_fetch_array($tiers_query);
              //if($tiers_array['total'] > 0)  {
                 $tiers_array1 = array();
                 $tiers_query_raw1 = "select products_id, shipping_cost from auctions_tier where auctions_id = '" . (int)$auctions_id . "' order by sort_id";
                 tep_query_to_array($tiers_query_raw1, $tiers_array1);
                 //}
               $latest_update_block = '';       
              for($i=0; $i<=$tiers_array['total']; $i++)
              {   
                     
               
                $latest_update_block .= '<table class="tabledata">
                <tr>
                  <th>(Item #'.($i+1).')</th>
                  <th class="ralign">Amount</th>
                </tr>
                
                <tr>
                  <td>Current Price:</td>
                  <td class="ralign">&pound'.round(($current_price_array[0]['auctions_price']-($i/100)),2).'</td>
                </tr>
                <tr>
                  <td>Post/Admin Fees:</td>
                  <td class="ralign">'; 
                  if($i == 0) {
                  $latest_update_block .= '&pound'.round($current_price_array[0]['shipping_cost'],2);
                  }else {
                      $latest_update_block .= '&pound'.round($tiers_array1[$i-1]['shipping_cost'],2);
                  }
                  $latest_update_block .= '</td>
                </tr>
                <tr>
                  <td class="heavy">* Winner To Pay:</td>
                  <td class="heavy ralign">';
                  if($i == 0) {
                  $latest_update_block .= '&pound'.round(($current_price_array[0]['auctions_price']-($i/100)+$current_price_array[0]['shipping_cost']),2); 
                  }
                  else {
                  $latest_update_block .= '&pound'.round((($current_price_array[0]['auctions_price']-($i/100))+$tiers_array1[$i-1]['shipping_cost']),2);
                  }      
                  $latest_update_block .= '</td>
                </tr> 
                 
              </table><br>';  
              }   
              //$latest_update_block = 'asdasdas'; 
              return $latest_update_block;
  }               
?>