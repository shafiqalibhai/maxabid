<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <div id="scart_left" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'INVOICE SOLUTIONS',
   'class' => 'contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array(
    'text' => tep_image(DIR_WS_IMAGES . 'design/invoice_cart.jpg', 'Invoice Cart', 250, 250),
    'class' => 'calign'
  );

  $info_box_contents[] = array(
    'text' => TEXT_INFO_INTRO,
    'style' => 'padding: 8px 0px; color: #FFF',
  );

/*
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="footerBoxContents">' . 'Shipping Info' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '" class="footerBoxContents">' . 'FAQ' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="contentBoxContents">' . 'My Account' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '" class="footerBoxContents">' . 'Contact Us' . '</a>');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_SHOPPING_CART) . '" class="footerBoxContents">' . 'Shopping Cart' . '</a>');
*/
  new contentBox($info_box_contents, 'contentBoxHeading');

?>
          </div>