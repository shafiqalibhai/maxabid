<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Shop by brand module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name limit 5");
  if ($number_of_rows = tep_db_num_rows($manufacturers_query)) {
?>
<!-- shop_by_brand //-->
          <tr>
            <td valign="top">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array('text' => BOX_HEADING_SHOP_BY_BRAND);

    new contentBoxHeading($info_box_contents);
// Display a list
    $info_box_contents = array();
    while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
      $manufacturers_list = '';
      $manufacturers_name = ((strlen($manufacturers['manufacturers_name']) > MAX_DISPLAY_MANUFACTURER_NAME_LEN) ? substr($manufacturers['manufacturers_name'], 0, MAX_DISPLAY_MANUFACTURER_NAME_LEN) . '..' : $manufacturers['manufacturers_name']);
      if (isset($_GET['manufacturers_id']) && ($_GET['manufacturers_id'] == $manufacturers['manufacturers_id'])) $manufacturers_name = '<b>' . $manufacturers_name .'</b>';
      $manufacturers_list .= '<a href="' . tep_href_link(FILENAME_DEFAULT, 'manufacturers_id=' . $manufacturers['manufacturers_id']) . '" class="contentBoxBullet">' . $manufacturers_name . '</a>';

      $info_box_contents[] = array('text' => $manufacturers_list);
    }
    $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_ALL_MANUFACTURERS) . '" class="contentBoxBullet">' . 'More Brands' . '</a>');
    new contentBox($info_box_contents, 'contentBoxBullet');
?>
            </td>
          </tr>
<!-- shop_by_brand_eof //-->
<?php
  }
?>
