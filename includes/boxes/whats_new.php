<?php
/*
  $Id: whats_new.php,v 1.31 2003/02/10 22:31:09 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  if ($random_product = tep_random_select("select products_id, products_image, products_tax_class_id, products_price from " . TABLE_PRODUCTS . " where products_status = '1' order by rand() desc limit " . MAX_RANDOM_SELECT_NEW)) {
?>
<!-- whats_new //-->
          <tr>
            <td>
<?php
    $random_product['products_name'] = tep_get_products_name($random_product['products_id']);
    $random_product['specials_new_products_price'] = tep_get_products_special_price($random_product['products_id']);

    $info_box_contents = array();
    $info_box_contents[] = array('text' => BOX_HEADING_WHATS_NEW);

    new infoBoxHeading($info_box_contents, false, false, tep_href_link(FILENAME_PRODUCTS_NEW));

    if (tep_not_null($random_product['specials_new_products_price'])) {
      $whats_new_price = '<span class="productPrice">' . TEXT_RETAIL . '&nbsp;' . $currencies->display_price($random_product['products_price'], tep_get_tax_rate($random_product['products_tax_class_id'])) . '</span><br />';
      $whats_new_price .= '<span class="productSpecialPrice">' . TEXT_OUR_PRICE . '&nbsp;' . $currencies->display_price($random_product['specials_new_products_price'], tep_get_tax_rate($random_product['products_tax_class_id'])) . '</span>';
    } else {
      $whats_new_price = '<span class="productSpecialPrice">' . TEXT_OUR_PRICE . '&nbsp;' . $currencies->display_price($random_product['products_price'], tep_get_tax_rate($random_product['products_tax_class_id'])) . '</span>';
    }

    $button1 = '<td>' . tep_draw_form('buy_now' . $random_product['products_id'], tep_href_link(basename($PHP_SELF), 'action=buy_now'), 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
      '           <tr>' . "\n" . 
      '             <td>' . tep_draw_hidden_field('products_id', $random_product['products_id']) . tep_image_submit('button_buy_now.gif', IMAGE_BUTTON_BUY_NOW) . '</td>' . "\n" .
      '           </tr>' . "\n" . 
      '         </table></form></td>' . "\n";

    $final_string = 
             '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="smallText">' . "\n" . 
             '  <tr>' . "\n" . 
             '    <td valign="top" class="small_frame"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $random_product['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $random_product['products_image'], $random_product['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>' . "\n" .
             '    <td>' . tep_draw_separator('pixel_trans.gif', '8', '1') . '</td>' . "\n" .
             '    <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">' . "\n" . 
             '      <tr>' . "\n" . 
             '        <td class="smallText"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $random_product['products_id']) . '">' . $random_product['products_name'] . '</a></td>' . "\n" .
             '      </tr>' . "\n" .
             '      <tr>' . "\n" . 
             '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '6') . '</td>' . "\n" .
             '      </tr>' . "\n" . 
             '      <tr>' . "\n" . 
             '        <td class="smallText">' . $whats_new_price . '</td>' . "\n" .
             '      </tr>' . "\n" . 
             '      <tr>' . "\n" . 
             '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" .
             '      </tr>' . "\n" .
             '      <tr>' . "\n" . $button1 . '</tr>' . "\n" . 
             '      </tr>' . "\n" . 
             '      <tr>' . "\n" . 
             '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '6') . '</td>' . "\n" .
             '      </tr>' . "\n" .
             '      <tr>' . "\n" . 
             '        <td><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $random_product['products_id']) . '">' . tep_image_button('button_details.gif', IMAGE_BUTTON_DETAILS) . '</a></td>' . "\n" .
             '      </tr>' . "\n" . 
             '    </table></td>' . "\n" . 
             '  </tr>' . "\n" . 
             '</table>' . "\n";
    $info_box_contents = array();
    $info_box_contents[] = array('align' => 'left',
                                 'text' => $final_string);

    new infoBox($info_box_contents, 'boxText2');
?>
            </td>
          </tr>
<!-- whats_new_eof //-->
<?php
  }
?>
