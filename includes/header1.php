<?php
/*
  $Id: header.php,v 1.42 2003/06/10 18:20:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Custom Main Header section
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
  <div class="cleaner wider ralign">
    <div class="totalsize balancer" style="padding-top: 2px;">
      <div class="floater"><?php echo '<a href="' . tep_href_link() . '">' . tep_image(DIR_WS_IMAGES . 'design/maxabid_logo.png', STORE_NAME) . '</a>'; ?></div>
      <div class="floatend">
        <div id="header_right">
          <div id="header_inner">
<?php
  $tmp_query = tep_db_query("select auctions_group_id, auctions_group_name from " . TABLE_AUCTIONS_GROUP . " order by sort_order limit 1");
  $tmp_array = tep_db_fetch_array($tmp_query);
  $tmp_id = $tmp_array['auctions_group_id'];
  $tmp_name = $tmp_array['auctions_group_name'];

  $tmp_array = array();
  // $tmp_array[] = '<a href="' . tep_href_link(FILENAME_AUCTION_GROUPS, 'auctions_group_id=' . $tmp_id, 'NONSSL') . '" title="' . $tmp_name . '">' . strtoupper(HEADER_TITLE_VIEW_AUCTIONS) . '</a>';
  // $tmp_array[] = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '">' . strtoupper(HEADER_TITLE_PRODUCTS) . '</a>';
  $tmp_array[] = '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'abz_id=' . DEFAULT_MENU_TEXT_ZONE_ID, 'NONSSL') . '">' . strtoupper(HEADER_TITLE_INFORMATION) . '</a>';

  $tmp_array[] = '<a href="' . tep_href_link(FILENAME_SHOPPING_CART, '', 'NONSSL') . '">' . strtoupper(HEADER_TITLE_SHOPPING_CART) . '</a>';

  if ($cart->count_contents() ) {
    //$tmp_array[] = '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'NONSSL') . '">' . strtoupper(HEADER_TITLE_CHECKOUT) . '</a>';
  }

  if (tep_session_is_registered('customer_id')) {
    $tmp_array[] = '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . strtoupper(HEADER_TITLE_MY_ACCOUNT) . '</a>';
    $tmp_array[] = '<a href="' . tep_href_link(FILENAME_LOGOFF, '', 'SSL') . '">' . strtoupper(HEADER_TITLE_LOGOFF) . '</a>';
  } else {
    $tmp_array[] = '<a href="' . tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL') . '">' . strtoupper(HEADER_TITLE_LOGIN) . '</a>';
  }
  $tmp_string = implode('', $tmp_array);
  echo $tmp_string;
?>
          </div>
        </div>
      </div>
    </div>
    <div class="bounder lalign vpad">
      <div class="floater" style="width:316px; margin-right: 12px;"><?php echo '<a href="' . tep_href_link() . '">' . tep_image(DIR_WS_IMAGES . 'pixel_black.gif', STORE_NAME, '100%','140') . '</a>'; ?></div>
      <div class="floater" style="width:632px;">

<?php
  $quick_contents = array();
  $cText = new gtext_front();
  $quick_array = $cText->get_gtext_entries('Header Quicks');
  $index = 0;
  foreach( $quick_array as $key => $value ) {
    if(!$index) {
      $align = 'lalign';
    } elseif($index == count($quick_array)-1 || $index==3) {
      $align = 'ralign';
    } else {
      $align = 'calign';
    }
    echo '<div class="floater ' . $align . '" style="width: 158px">' . strip_tags($value['gtext_description'], '<img><br><a>') . '</div>';
    $index++;
    if( $index >= 4 ) break;
  }
?>
      </div>
    </div>
    
  </div>

  <div class="cleaner" style="margin-bottom: 5px;"></div>
<?php
/*
  <div class="cleaner wider" id="header">
    <div class="totalsize balancer">
      <div class="innersize">
        <div class="bounder">
          <div class="floatend ralign" style="padding: 36px 16px 0px 16px;"><?php echo tep_draw_form('quick_search', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL'), 'post', 'id="quick_search_id"'); ?><div id="tsearch">
            <div class="floater rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADVANCED_SEARCH) . '" title="' . TEXT_HELP_ADVANCED_SEARCH . '">' . tep_image(DIR_WS_IMAGES . 'design/asearch.png', BOX_SEARCH_ADVANCED_SEARCH) . '</a>'; ?></div>
            <div class="floater" style="margin-top: 5px;"><?php echo tep_draw_input_field('keywords', 'Search', 'size="10" class="search"') . tep_draw_hidden_field('module', 'quick_search'); ?></div>
          </div></form></div>
        </div>
      </div>
    </div>
  </div>
*/
?>