<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Sitemap strings file
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('NAVBAR_TITLE', 'Sitemap');
define('HEADING_TITLE', 'Sitemap');

define('TEXT_INFO_CATEGORIES', 'Categories');
define('TEXT_INFO_COMPANY', 'Information');
define('TEXT_INFO_CUSTOMER_SERVICE', 'Customer Service');
define('TEXT_INFO_PRODUCTS', 'Best Sellers');
define('TEXT_INFO_BRANDS', 'Brands');
define('TEXT_INFO_TYPES', 'Types');
define('TEXT_INFO_PRICES', 'Prices');

define('TEXT_INFO_CONTACT_US', 'Contact ' . STORE_NAME);
define('TEXT_INFO_ACCOUNT', 'Customer Account');
define('TEXT_INFO_LOGIN', 'Customer Login');
define('TEXT_INFO_SEARCH', 'Search ' . STORE_NAME);
define('TEXT_INFO_ORDERS', 'Your Orders');
define('TEXT_INFO_NOTIFICATIONS', 'Product Notifications');
define('TEXT_INFO_WISHLIST', 'Products Wishlist');
define('TEXT_INFO_SHOPPING_CART', 'Shopping Cart');
define('TEXT_INFO_REVIEWS', TEXT_PRODUCTS_POSTFIX . ' Reviews');
?>