<?php
  define('MODULE_PAYMENT_PAYPAL_EXPRESS_TEXT_TITLE', 'PayPal Express Checkout');
  define('MODULE_PAYMENT_PAYPAL_EXPRESS_TEXT_PUBLIC_TITLE', 'PayPal Express (including Credit Cards and Debit Cards)');
  define('MODULE_PAYMENT_PAYPAL_EXPRESS_TEXT_DESCRIPTION', '<img src="images/icon_popup.png" border="0">&nbsp;<a href="https://www.paypal.com/it/mrb/pal=S53S5NBZGRYF4" target="_blank" style="text-decoration: underline; font-weight: bold;">Visit PayPal Website</a>&nbsp;<a href="javascript:toggleDivBlock(\'paypalExpressInfo\');">(info)</a><span id="paypalExpressInfo" style="display: none;"><br><i>Using the above link to signup at PayPal grants osCommerce a small financial bonus for referring a customer.</i></span><br />Copyright &copy; <a target="_blank" href="http://www.lavoriamopervoi.com">LavoriamoPerVoi</a>');
  define('MODULE_PAYMENT_PAYPAL_EXPRESS_TEXT_BUTTON', 'Checkout with PayPal');
  define('MODULE_PAYMENT_PAYPAL_EXPRESS_TEXT_COMMENTS', 'Comments:');
?>