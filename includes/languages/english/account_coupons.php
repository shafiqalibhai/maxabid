<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account Coupons Page Strings
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Coupons');

define('HEADING_TITLE', 'My Account Coupons');

define('TABLE_HEADING_NAME', 'Coupon Name');
define('TABLE_HEADING_AMOUNT', 'Amount');
define('TABLE_HEADING_TYPE', 'Coupon Type');
define('TABLE_HEADING_USAGE', 'Usage');
define('TABLE_HEADING_DATE_START', 'Starts');
define('TABLE_HEADING_DATE_END', 'Expires');
define('TABLE_HEADING_DATE_ADDED', 'Date Added');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_COUPON_REDEEM_FORM', '</br>');
define('TEXT_INFO_REDEEM_LEGEND', 'Redeem Coupon');
define('TEXT_INFO_ENTER_CODE', 'Enter a coupon code:');
define('TEXT_INFO_REDEEM', 'Redeem');
define('TEXT_INFO_BIDS', '%s Bids');
define('TEXT_INFO_NO_COUPONS', 'You have no coupons redeemed. If you have a coupon code please enter it in the coupons form above.');
define('TEXT_INFO_AUCTION_BIDS_LEFT', 'You have %s bids left');

define('ERROR_COUPON_CODE_EMPTY', 'Please Enter A Valid Coupon Code');
define('ERROR_COUPON_CODE_INVALID', 'Invalid Redemption: The coupon code you entered is either incorrect, expired or already used.');
define('SUCCESS_COUPON_REDEEMED', 'Coupon %s was successfully redeemed');
?>
