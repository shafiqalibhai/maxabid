<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Groups page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('NAVBAR_TITLE', 'Auctions List');
define('HEADING_TITLE', 'Viewing: %s');
define('HEADING_RECENTLY_ENDED', 'Viewing: Recently Ended Auctions');
define('HEADING_FAVORITES', 'Viewing: Favorite Auctions');
define('HEADING_COMING_SOON', 'Viewing: Coming Soon Auctions');

define('TEXT_INFO_GROUP_DEFAULT_NAME', 'There are no Auction Categories');
define('TEXT_INFO_GROUP_DEFAULT_DESC', 'We are sorry but there are no Auction Categories at this point');

define('TEXT_INFO_GROUP_COMING_SOON_NAME', 'Coming Soon');
define('TEXT_INFO_GROUP_COMING_SOON_DESC', 'Displaying upcoming auctions');
define('TEXT_INFO_GROUP_COMING_SOON_EMPTY', 'There are no future auctions at this point');

define('TEXT_INFO_GROUP_ENDED_NAME', 'Recently Ended');
define('TEXT_INFO_GROUP_ENDED_DESC', 'Displaying the most recently ended auctions');
define('TEXT_INFO_GROUP_ENDED_EMPTY', 'There are no recently ended auctions at this point');

define('TEXT_INFO_GROUP_FAVORITES_NAME', 'My Auctions');
define('TEXT_INFO_GROUP_FAVORITES_DESC', 'Your Favorite Auctions');
define('TEXT_INFO_GROUP_FAVORITES_EMPTY', 'You have no favorite auctions assigned yet');

define('TEXT_INFO_AUCTION_PRIZES_COUNT', '%s Items to Win');
define('TEXT_INFO_AUCTION_PRICE', '<b>#1 ITEM RRP</b>: %s');
define('TEXT_INFO_AUCTION_RRP', '<b>RRP</b>: %s');
define('TEXT_INFO_AUCTION_PRICE_TIERS', '<b>%s ITEM RRP:</b> %s');
define('TEXT_INFO_AUCTION_STARTS', 'Starts on: %s');
define('TEXT_INFO_AUCTION_ENDS', 'Ends on: %s');
define('TEXT_INFO_AUCTION_FINAL_PRICE', 'Final Price: <b>%s</b>');
define('TEXT_INFO_LAST_BID', '<b>L.U.B:</b> %s');
define('TEXT_INFO_LAST_LEADER', '<b>Winning:</b> %s');
define('TEXT_INFO_BIDS_ENTERED', '%s Bids Entered');
define('TEXT_INFO_AUCTION_WAIT_FIRST_BID', 'Awaiting First Bid');
define('TEXT_INFO_AUCTION_COMING_SOON', '<b>COMING SOON</b>');
define('TEXT_INFO_AUCTION_BIDS_ENTERED', '%s bids entered');
define('TEXT_INFO_AUCTION_BIDS_SELF_ENTERED', 'You submitted %s bids');
define('TEXT_INFO_AUCTION_BIDS_WINNERS', 'Winners: %s');
define('TEXT_INFO_AUCTION_BIDS_WINNERS_DATA', '%s. %s (%s bids)');
define('TEXT_INFO_AUCTION_ENTER_BID', 'Enter L.U.B:');
define('TEXT_INFO_AUCTION_CURRENT_VALUE', 'Next Bid: &pound;');
define('TEXT_INFO_AUCTION_TIMER_PRESENT', 'Timer: %s');

define('TEXT_INFO_AUCTION_PLACE_BID', 'Submit Bid on %s');
define('TEXT_INFO_AUCTION_LOGIN_FIRST', 'Login to submit bid');
define('TEXT_INFO_AUCTION_LOGIN_FIRST_2', 'blank');
define('TEXT_INFO_AUCTION_LOGIN_DETAILS', 'Login to view details');
define('TEXT_INFO_AUCTION_PAUSED', 'Auction is paused');
define('TEXT_INFO_AUCTION_ENDED', 'Auction ended on:<br /><b>%s</b>');
define('TEXT_INFO_AUCTION_WON', 'Won by %s');
define('TEXT_INFO_AUCTION_NO_WINNER', 'There was no winner');
define('TEXT_INFO_AUCTION_DETAILS', 'Latest Information For Auction:   %s');
define('TEXT_INFO_AUCTION_BIDS_REMAIN', 'BIDS REMAINING: %s');

define('TEXT_INFO_AUCTION_NO_BIDS', 'You have no bids left');
define('TEXT_INFO_AUCTION_BIDS_LEFT', 'You have %s bids left');
define('TEXT_INFO_AUCTION_BIDS_STEP_REQUIRED', '%s bids per turn required to play %s');

define('ERROR_AUCTION_NO_WINNER', 'No Winner');
define('ERROR_AUCTION_RUNNING', 'Auction is Running');
define('ERROR_AUCTION_NOT_STARTED', 'Auction not started yet');
?>
