<?php
/*        
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Callback Page Strings
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('TEXT_INFO_BID_SET', '%s Auction %s');
define('TEXT_INFO_BID_NEW', 'New bid in auction %s by %s');
define('TEXT_INFO_BID_NEW_SELF', 'You entered a bid of %s in auction %s');
define('TEXT_INFO_BID_NEW_PRICE', '%s set in in auction %s by %s');
define('TEXT_INFO_AUCTION_END_WINNER', 'Auction %s won by %s position: [%s]');
define('TEXT_INFO_AUCTION_END_WINNER_LABEL', 'Won by %s');
define('TEXT_INFO_AUCTION_END_NULL', 'Auction %s ended with no winner');
define('TEXT_INFO_AUCTION_END_NULL_LABEL', 'No Winner');
define('TEXT_INFO_AUCTION_ENDED', 'Auction Ended');
define('TEXT_INFO_BIDS_ENTERED', '%s Bids Entered');
define('TEXT_INFO_LAST_BID', '<b>L.U.B:</b> %s');
define('TEXT_INFO_LAST_LEADER', '<b>Leader:</b> %s');
define('TEXT_INFO_AUCTION_EXPIRES', 'Time to End: %s');
define('TEXT_INFO_AUCTION_TIMER_PRESENT', 'Timer: %s');
define('TEXT_INFO_AUCTION_TIMER_PRESENT_X_BID', 'X Bid Timer: %s');
define('TEXT_INFO_AUCTION_TIMER_PRESENT_FIRST_BID', '1st Bid Timer: %s');
define('TEXT_INFO_AUCTION_TIMER_AUTO', 'Ends on: %s');
define('TEXT_INFO_AUCTION_PAUSED', 'Auction Paused');

define('TEXT_INFO_AUCTION_NO_BIDS', 'You have no bids left');
define('TEXT_INFO_AUCTION_BIDS_LEFT', 'You have %s bids left');
define('TEXT_INFO_AUCTION_BIDS_REMAIN', 'BIDS REMAINING: %s');

define('ERROR_INVALID_AUCTION', 'The specified Auction is no longer available.');
define('ERROR_MAXIMUM_BIDS', 'Maximum Bids for auction %s reached - Cannot add more');
define('ERROR_NO_BIDS', 'You have not enough bids left in your account. You need to purchase additional bids');
define('ERROR_INSUFFICIENT_BIDS', 'Insufficient bids. You need at least %s bids to place in auction %s');
define('ERROR_BID_EQUAL', 'You already entered %s in auction %s.');
//define('ERROR_BID_VALUE_ENTRY', 'Auction %s requires a non-zero value to be entered');
define('ERROR_BID_VALUE_ENTRY', 'Auction %s: requires a non-zero amount to be submitted');
define('ERROR_BID_VALUE_BETWEEN', 'Auction %s: submitted bids must be between &pound;%s and &pound;%s');
define('ERROR_BID_VALUE_AT_LEAST', 'A higher bid is present. Bids for Auction %s must be at least %s');
define('ERROR_AUCTION_OVERBID', 'Auction %s does not allow overbids and you are currently winning');
define('ERROR_AUCTION_PAUSED', 'Auction %s is going to restart on: %shrs');
define('ERROR_AUCTION_WILL_START', 'Auction %s Starts at: %s');
define('ERROR_AUCTION_SECS_LABEL', 'Starts in: %s');
define('ERROR_AUCTION_WILL_START_LABEL', 'Starts at: %s');
define('ERROR_AUCTION_PAUSED_LABEL', 'Auction Restarts at: %s');
define('ERROR_CUSTOMER_MUST_LOGIN', 'You must login to place bids');
define('ERROR_AUCTION_SESSION_EXPIRED', 'The session has timed out because of inactivity. Please login again.');
?>
