<?php
/*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

// ---------------------------------------------------------------------------
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// Moved html bottom section to a separate file
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  switch($g_script) {
    case FILENAME_DEFAULT:
?>
          </div>
<?php
      if( !empty($current_category_id) ) {
        include(DIR_WS_MODULES . 'dynamic.php');
      }
/*
      if( tep_session_is_registered('customer_id') ) {
        include(DIR_WS_MODULES . 'promotion.php');
      } else {
        include(DIR_WS_MODULES . 'personal.php');
      }
*/
      break;
    case FILENAME_CHECKOUT_PAYMENT:
    case FILENAME_CHECKOUT_CONFIRMATION:
    case FILENAME_LOGOFF:	    case FILENAME_PASSWORD_FORGOTTEN:	    case 'create_account_org.php':	    case 'login.php':
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'dynamic.php');
      break;    case FILENAME_CREATE_ACCOUNT:
?>		  </div>
          </div>
<?php
      include(DIR_WS_MODULES . 'dynamic.php');
      break;
    case FILENAME_CATEGORIES:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion.php');
      break;
    case FILENAME_PRODUCT_INFO:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_product.php');
      break;
    case FILENAME_SHOPPING_CART:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_cart.php');
      break;
    case FILENAME_ACCOUNT:
    case FILENAME_ACCOUNT_EDIT:
    case FILENAME_ACCOUNT_NEWSLETTERS:
    case FILENAME_ACCOUNT_NOTIFICATIONS:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_account.php');
      break;
    case FILENAME_ADDRESS_BOOK:
    case FILENAME_ADDRESS_BOOK_PROCESS:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_address.php');
      break;
    case FILENAME_CONTACT_US:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_contact.php');
      break;
    case FILENAME_CHECKOUT_SUCCESS:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_contact.php');
      break;

    case FILENAME_GENERIC_PAGES:
?>
          </div>
<?php
      include(DIR_WS_MODULES . 'promotion_info.php');
      break;
    default:
?>
          </div>
<?php
      break;
  }
?>
          <div class="cleaner">
<?php 
  require(DIR_WS_INCLUDES . 'footer.php'); 
?>
          </div>
        </div></div>
        <div class="cleaner totalsize balancer">
          <div class="bgc calign bounder footer_outer"><p>Copyright &copy; 2008 - 2011 <a href="<?php echo tep_href_link('', '', 'NONSSL'); ?>">Maxabid</a><br />All Rights Reserved.</p></div>
        </div>
      </div>
    </div>
  </div>
<?php
  if($g_script == FILENAME_CHECKOUT) {
?>
<!-- dialogs_bof //-->
     <div id="loginBox" title="Log Into My Account" style="display:none;"><table cellpadding="2" cellspacing="0" border="0">
       <tr>
         <td><?php echo ENTRY_EMAIL_ADDRESS;?></td>
         <td><?php echo tep_draw_input_field('email_address');?></td>
       </tr>
       <tr>
         <td><?php echo ENTRY_PASSWORD;?></td>
         <td><?php echo tep_draw_password_field('password');?></td>
       </tr>
       <tr>
         <td colspan="2" align="right"><?php echo tep_image_button('button_login.gif', IMAGE_BUTTON_LOGIN, 'id="loginWindowSubmit"');?></td>
       </tr>
       <tr>
         <td colspan="2"><div class="main" align="center" id="loginStatus" style="display:none">&nbsp;</div></td>
       </tr>
     </table></div>
     <div id="ajaxLoader" title="Processing Checkout Form" style="display:none;"><img src="includes/javascript/jquery/themes/smoothness/images/ajax_load.gif" /><hr /><div id="ajaxMsg" class="main">Updating, please wait...</div></div>
     <div id="addressBook" title="Address Book" style="display:none"></div>
     <div id="newAddress" title="New Address" style="display:none"></div>
<!-- dialogs_eof//-->
<?php
  }
?>
<div><script language="javascript" type="text/javascript">
  var jqWrap = general;
  jqWrap.launch();
</script><script type="text/javascript">    $(window).load(function() {        $('#slider').nivoSlider({    effect: 'slideInLeft',               // Specify sets like: 'fold,fade,sliceDown'    slices: 15,                     // For slice animations    boxCols: 8,                     // For box animations    boxRows: 4,                     // For box animations    animSpeed: 500,                 // Slide transition speed    pauseTime: 9000,                // How long each slide will show    startSlide: 0,                  // Set starting Slide (0 index)    directionNav: true,             // Next & Prev navigation    controlNav: true,               // 1,2,3... navigation    controlNavThumbs: false,        // Use thumbnails for Control Nav    pauseOnHover: true,             // Stop animation while hovering    manualAdvance: false,           // Force manual transitions    prevText: 'Prev',               // Prev directionNav text    nextText: 'Next',               // Next directionNav text    randomStart: false,             // Start on a random slide    beforeChange: function(){},     // Triggers before a slide transition    afterChange: function(){},      // Triggers after a slide transition    slideshowEnd: function(){},     // Triggers after all slides have been shown    lastSlide: function(){},        // Triggers when last slide is shown    afterLoad: function(){}         // Triggers when slider has loaded});    });</script></div>
<?php
  switch($g_script) {
    case FILENAME_DEFAULT:
/*
?>
<div><script language="javascript" type="text/javascript">
  $('#img_content a:has(img)').fancybox({
    'autoScale'     : false,
    'transitionIn'  : 'elastic',
    'transitionOut' : 'elastic'
  });

  var jqWrap = image_index;
  jqWrap.launch();
</script></div>
<?php
*/
      break;
    case FILENAME_AUCTION_GROUPS:
?>
<div><script language="javascript" type="text/javascript">
  var jqWrap = sticky_auctions;
  jqWrap.baseURL = "<?php echo tep_href_link(FILENAME_AUCTION_CALLBACK, '', 'NONSSL', false, false, false); ?>";
  jqWrap.mainURL = "<?php echo tep_href_link(FILENAME_AUCTION_GROUPS, '', 'NONSSL', false, false, false); ?>";
  jqWrap.detailsURL = "<?php echo tep_href_link(FILENAME_AJAX_CONTROL, '', 'NONSSL', false, false, false); ?>";
  jqWrap.launch();
</script></div>
<?php
      break;
    case FILENAME_AUCTION_INFO:
?>
<div><script language="javascript" type="text/javascript">
  var jqWrap = sticky_auctions;
  jqWrap.baseURL = "<?php echo tep_href_link(FILENAME_AUCTION_CALLBACK, '', 'NONSSL', false, false, false); ?>";
  jqWrap.mainURL = "<?php echo tep_href_link(FILENAME_AUCTION_INFO, 'auctions_id=' . $auction_info['auctions_id'], 'NONSSL', false, false, false); ?>";
  jqWrap.detailsURL = "<?php echo tep_href_link(FILENAME_AJAX_CONTROL, '', 'NONSSL', false, false, false); ?>";
  jqWrap.keep_ended = 2;
  jqWrap.launch();
  $('div#fancyx a.pg_link').fancybox({
    'autoScale'     : false,
    'transitionIn'  : 'elastic',
    'transitionOut' : 'elastic'
  });
</script></div>
<?php
      break;
    case FILENAME_PRODUCT_INFO:
?>
<div><script language="javascript" type="text/javascript">
  $('div#fancyx a.pg_link').fancybox({
    'autoScale'     : false,
    'transitionIn'  : 'elastic',
    'transitionOut' : 'elastic'
  });
</script></div>
<?php
/*
?>
<div><script language="javascript" type="text/javascript">
  $('.custsectioncell a').fancybox({
    'autoScale'     : false,
    'transitionIn'  : 'elastic',
    'transitionOut' : 'elastic'
  });
</script></div>
<?php
*/
      break;
    case FILENAME_CREATE_ACCOUNT:
?>
<div><script language="javascript" type="text/javascript">
  var jqWrap = new_account;
  jqWrap.launch();
</script></div>
<?php
      break;
    default:
      break;
  }
?>
<?php
/*
<div><script language="javascript" type="text/javascript">
  $('#quick_search_id input[name="keywords"]').liveSearch({
    'url'           : '<?php echo FILENAME_AJAX_CONTROL; ?>',
    'form_id'       : '#quick_search_id'
  });
</script></div>
*/
?>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>