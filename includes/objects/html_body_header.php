<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Common html body header section
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  switch($g_script) {
    case FILENAME_DEFAULT:
      break;
    default:
      break;
  }

  if( !isset($heading_row) && defined('HEADING_TITLE') ) {
?>
      <div><h1><?php echo HEADING_TITLE; ?></h1></div>
<?php
  }
?>
