<?php
/*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

// ---------------------------------------------------------------------------
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// Moved html bottom section to a separate file
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
</head>
<body>
  <div class="wider balancer" id="wrapper">
    <div class="totalsize balancer" id="mainbody">
      <div class="bg bounder">
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<?php
  if ($messageStack->size() > 0) {
?>
          <div><?php echo $messageStack->output(); ?></div>
<?php
  }
?>
        <div class="mborder"><div id="mcontent" class="highter hider">
<?php
  switch($g_script) {
    case FILENAME_DEFAULT:
      if( !empty($current_category_id) ) {
?>
          <div id="shome_left">
<?php
      } else {
?>
          <div id="hpadder">
<?php
      }
      break;
    case FILENAME_CHECKOUT_PAYMENT:
    case FILENAME_CHECKOUT_CONFIRMATION:
    case FILENAME_LOGOFF:
?>
          <div id="shome_left">
<?php
      break;    
	  case FILENAME_CREATE_ACCOUNT:
?>
          <div id="shome_left" style="width:auto;">
<?php
      break;
    case FILENAME_CATEGORIES:
?>
          <div class="floater" style="width: 730px; border-right: 1px solid #d6d6d6; padding-right: 10px; margin-right: 8px;">
<?php
      break;
    case FILENAME_PRODUCT_INFO:
//      require(DIR_WS_MODULES . 'customer_images.php');
?>
          <div id="product_right">
<?php
      break;
    case FILENAME_ACCOUNT:
    case FILENAME_ACCOUNT_EDIT:
    case FILENAME_ACCOUNT_NEWSLETTERS:
    case FILENAME_ACCOUNT_NOTIFICATIONS:
?>
          <div id="account_right">
<?php
      break;
    case FILENAME_SHOPPING_CART:
?>
          <div id="scart_right">
<?php
      break;
    case FILENAME_ADDRESS_BOOK:
    case FILENAME_ADDRESS_BOOK_PROCESS:
?>
          <div id="address_left">
<?php
      break;
    case FILENAME_CONTACT_US:
?>
          <div id="contact_left">
<?php
      break;
    case FILENAME_CHECKOUT_SUCCESS:
?>
          <div id="contact_left">
<?php
      break;
    case FILENAME_GENERIC_PAGES:
?>
          <div id="info_right">
<?php
      break;
    default:
?>
          <div class="hpadder">
<?php
      break;
  }
?>
