<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Common section for listing products
// $g_products_per_row var must be initialized before invoking this module
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
 $incart = array();
$check_query1 = "select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '9'";
    //$check_array1 = tep_db_fetch_array($check_query1);
    tep_query_to_array($check_query1, $check_array1);
    //var_dump($check_array1);    
?>
  <table border="0" cellspacing="0" cellpadding="0" width="510px" class="list">
<?php
  if( !isset($g_products_per_row) || $g_products_per_row <= 0 ) {
    $g_products_per_row = MAX_DISPLAY_NEW_PER_ROW;
  }
  $db_names = $g_filters->get_filters_db_names();

  if( isset($g_filter_flag) && isset($params_array) && is_array($params_array) && count($params_array) ) {
    foreach($params_array as $key => $value) {
      unset($db_names[$key]);
    }
  }
  
  $error = '<div class="messageStackError" "="">
<img src="images/icons/error.gif" title="Error" alt="Error">&nbsp; RESPONSIBLE PURCHASING: Please Purchase One Bids Package At A Time  <a href="/shopping-cart.html">(VIEW CART)<a>
      </div>';
      
  $products_cart = $cart->get_products();
   //var_dump($total_items);
 /* for( $k=0, $j=count($total_items); $k<=$j; $k++ ) {
    echo $k.'-------';
    //if(array_search($products_cart[$k]['id'], $check_array1)) {
    //echo 'adasd';
    //}
    echo array_search($products_cart[$k]['id'], $check_array1[$k]['products_id']);
    echo $products_cart[$k]['id'];
    echo '<br>'.$check_array1[$k]['products_id'];   
    //var_dump(count($total_items)); 
    echo '<br>--------------------------------------------------------------<br>';
    
    if( $products_cart[$k]['id'] == $check_array1[$k]['products_id']) {

      $error = '<div class="messageStackError" "="">
<img src="images/icons/error.gif" title="Error" alt="Error">&nbsp; RESPONSIBLE PURCHASING: Please Purchase One Bids Package At A Time  <a href="/shopping-cart.html">(VIEW CART)<a>
      </div>';     
      //$incart[$products_cart[$k]['id']] = true;
    }
    else {
    //$incart[$products_cart[$k]['id']] = false;
    }
    } */
    
  //var_dump($incart);
    //echo '<br>--------------------------------------------------------------<br>';
    //var_dump($total_items); 
    
       //echo '<br><br>';
    for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
       
      foreach($products_cart as $cart_item) {
        //var_dump($cart_item['id']);
        if($cart_item['id'] == $total_items[$i]['products_id']){
        $incart = true;
        }
        
        }
    }
  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
    $final_string = $row_string = ''; 
            
    $tmp_image = DIR_WS_IMAGES . $total_items[$i]['products_image'];
    if( !tep_not_null($total_items[$i]['products_image']) || !file_exists($tmp_image) || filesize($tmp_image) <= 0 ) {
    //if( !tep_not_null($tmp_image) ) {
      $total_items[$i]['products_image'] = IMAGE_NOT_AVAILABLE;
    }

    $total_items[$i]['products_name'] = tep_get_products_name($total_items[$i]['products_id']);
    $total_items[$i]['products_name'] = tep_create_safe_string($total_items[$i]['products_name']);

    if( !isset($total_items[$i]['specials_new_products_price']) ) {
      $total_items[$i]['specials_new_products_price'] = tep_get_products_special_price($total_items[$i]['products_id']);
    }
    if( tep_not_null($total_items[$i]['specials_new_products_price'])) {
      $products_price = '<span class="productPrice"><s>' . $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id'])) . '</s></span>&nbsp;';
      $products_price .= '<span class="productSpecialPrice">' . $currencies->display_price($total_items[$i]['specials_new_products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id'])) . '</span>';
    } else {
      $products_price = '<span class="productSpecialPrice">' . $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id'])) . '</span>';
    }

    $row_string .= '<td class="calign cpad"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '" title="' . $total_items[$i]['products_name'] . '">' . tep_image(DIR_WS_IMAGES . $total_items[$i]['products_image'], $total_items[$i]['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>';
    $row_string .= '<td><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '" title="' . $total_items[$i]['products_name'] . '">' . $total_items[$i]['products_name'] . '</a></td>';
    $row_string .= '<td class="ralign">' . $products_price . '</td>';

	if( tep_session_is_registered('customer_id') && !$incart) {
	  $buynow_form = tep_draw_form('buy_now' . $total_items[$i]['products_id'], tep_href_link($g_script, 'action=buy_now'), 'post');
	  }
	  elseif(tep_session_is_registered('customer_id') && $incart) {
	  $buynow_form = tep_draw_form('buy_now' . $total_items[$i]['products_id'], tep_href_link(FILENAME_SHOPPING_CART, '', 'NONSSL'), 'post');
	  }
	  else {
	  $buynow_form = tep_draw_form('buy_now' . $total_items[$i]['products_id'], tep_href_link(FILENAME_LOGIN, '', 'SSL'), 'post');
	  }
      $row_string .= '<td class="calign">' . $buynow_form;
    
    if( $total_items[$i]['products_status'] ) { 
      //$row_string .= tep_draw_hidden_field('products_id', $total_items[$i]['products_id']) . tep_image_submit('button_purchase.png', IMAGE_BUTTON_BUY_NOW . ' ' . $total_items[$i]['products_name']);
      $row_string .= tep_draw_hidden_field('products_id', $total_items[$i]['products_id']);      
      if( tep_session_is_registered('customer_id') && !$incart) {
      $row_string .= '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '" title="' . $total_items[$i]['products_name'] . '" class="tbutton2 bsubmit">' . IMAGE_BUTTON_BUY_NOW . '</a>';
      }
	  elseif(tep_session_is_registered('customer_id') && $incart) {
	  $row_string .= '<a href="' . tep_href_link(FILENAME_SHOPPING_CART, '', 'NONSSL') . '" title="' . $total_items[$i]['products_name'] . '" class="tbutton2 bsubmit">View Cart</a>';
	  }
	  else {
	  $row_string .= '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" title="' . $total_items[$i]['products_name'] . '" class="tbutton2 bsubmit">' . IMAGE_BUTTON_BUY_NOW . '</a>';
	  }
    } else {
      $row_string .= '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id']) . '" rel="nofollow">' . tep_image_button('button_out_of_stock.gif', $total_items[$i]['products_name'] . ' ' . IMAGE_BUTTON_OUT_OF_STOCK);
    }
    $row_string .= '</form></td>' . "\n";
    
    $row_class = ($i%2)?'oddrow':'altrow';
    $final_string .= '<tr class="' . $row_class . '">' . $row_string . '</tr>' . "\n";
    $final_string .= '<tr><td class="sep"><div></div></td></tr>' . "\n";
    echo $final_string;
  }
  if($incart) {
    echo $error;
  }
    
  
  //var_dump($cart->get_products());
?>
  </table>
