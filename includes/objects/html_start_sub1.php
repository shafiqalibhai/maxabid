<?php
/*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

// ---------------------------------------------------------------------------
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// Moved html bottom section to a separate file
// Added META-G
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
// check if the 'install' directory exists, and warn of its existence
  if (WARN_INSTALL_EXISTENCE == 'true') {
    if (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/install')) {
      $messageStack->add('header', WARNING_INSTALL_DIRECTORY_EXISTS, 'warning');
    }
  }

// check if the configure.php file is writeable
  if (WARN_CONFIG_WRITEABLE == 'true') {
    if ( (file_exists(dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php')) && (is_writeable(dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php')) ) {
      $messageStack->add('header', WARNING_CONFIG_FILE_WRITEABLE, 'warning');
    }
  }

// check if the session folder is writeable
  if (WARN_SESSION_DIRECTORY_NOT_WRITEABLE == 'true') {
    if (STORE_SESSIONS == '') {
      if (!is_dir(tep_session_save_path())) {
        $messageStack->add('header', WARNING_SESSION_DIRECTORY_NON_EXISTENT, 'warning');
      } elseif (!is_writeable(tep_session_save_path())) {
        $messageStack->add('header', WARNING_SESSION_DIRECTORY_NOT_WRITEABLE, 'warning');
      }
    }
  }

// check session.auto_start is disabled
  if ( (function_exists('ini_get')) && (WARN_SESSION_AUTO_START == 'true') ) {
    if (ini_get('session.auto_start') == '1') {
      $messageStack->add('header', WARNING_SESSION_AUTO_START, 'warning');
    }
  }

  if ( (WARN_DOWNLOAD_DIRECTORY_NOT_READABLE == 'true') && (DOWNLOAD_ENABLED == 'true') ) {
    if (!is_dir(DIR_FS_DOWNLOAD)) {
      $messageStack->add('header', WARNING_DOWNLOAD_DIRECTORY_NON_EXISTENT, 'warning');
    }
  }

  $g_right_column_margin = 0;
  $g_inner_right_column = 0;
  $g_left_column_width = 0;
  if( isset($category_depth) && $category_depth == 'top' ) {
    //$g_main_section_width = $g_width = TOTAL_WIDTH;
    $g_main_section_width = $g_width = 800;
  } else {

    //$g_width = TOTAL_WIDTH-REMAINDER_WIDTH;
    $g_width = TOTAL_WIDTH;
    //$g_main_section_width = TOTAL_WIDTH-BOX_WIDTH_LEFT-BOX_WIDTH_RIGHT-LEFT_COLUMN_MARGIN-RIGHT_COLUMN_MARGIN-REMAINDER_WIDTH;
    $g_main_section_width = TOTAL_WIDTH-BOX_WIDTH_LEFT-LEFT_COLUMN_MARGIN-REMAINDER_WIDTH;
    $g_left_column_width = BOX_WIDTH_LEFT;
    $g_right_column_width = 0;
  }

  switch($g_script) {
/*
    case FILENAME_SHOP_BY_PRICE:
    case FILENAME_PRODUCTS_NEW:
    case FILENAME_SPECIALS:
    //case FILENAME_PRODUCT_INFO:
      $g_inner_right_column = BOX_WIDTH_RIGHT;
      $g_right_column_margin = RIGHT_COLUMN_MARGIN;
      break;
*/
    default:
      if( isset($category_depth) && $category_depth != 'top' ) {
        $g_inner_right_column = BOX_WIDTH_RIGHT;
        $g_right_column_margin = RIGHT_COLUMN_MARGIN;
      } else {
        $g_main_section_width += REMAINDER_WIDTH;
      }
      break;
  }

// Setup privacy header
  //header('P3P: CP="NOI ADM DEV PSAi COM NAV STP IND"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php echo HTML_PARAMS; ?>>
<head>
<?php
//-MS- META-G Added
  if( file_exists(DIR_WS_CLASSES . 'meta_g.php') && defined('META_DEFAULT_ENABLE') && META_DEFAULT_ENABLE == 'true' ) {
    require(DIR_WS_CLASSES . 'meta_g.php');
    $cMeta = new metaG();
    echo $cMeta->get_meta_tags($_GET);
  } else {
    echo '<title>' . TITLE . '</title>' . "\n";
  }
//-MS- META-G Added
?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<base href="<?php echo $g_relpath; ?>"></base>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<link rel="stylesheet" type="text/css" href="style_menu.css" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/themes/smoothness/ui.all.css" />
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/jquery.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/jquery.dump.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/jquery.base64.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/jquery.ajaxq.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/jquery.form.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/ui/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/general.js"></script>
<?php
  switch($g_script) {
    case FILENAME_DEFAULT:
      //echo '<link rel="stylesheet" type="text/css" href="styleindex.css" />' . "\n";
      //echo '<script type="text/javascript" src="includes/javascript/image_index.js"></script>' . "\n";

      //echo '<script type="text/javascript" src="includes/javascript/niceforms/niceforms.js"></script>' . "\n";
      //echo '<link rel="stylesheet" type="text/css" href="includes/javascript/niceforms/niceforms.css" media="all" />' . "\n";

      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />' . "\n";
      break;
    case FILENAME_CATEGORIES:
      echo '<link rel="stylesheet" type="text/css" href="stylerightcol.css" />' . "\n";
      break;
    case FILENAME_AUCTION_GROUPS:
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/jgrowl/jquery.jgrowl.css" />' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/jgrowl/jquery.jgrowl.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/sticky_auctions.js"></script>' . "\n";
      break;
    case FILENAME_AUCTION_INFO:
      echo '<link rel="stylesheet" type="text/css" href="styleleftcol.css" />' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/jgrowl/jquery.jgrowl.css" />' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/jgrowl/jquery.jgrowl.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/sticky_auctions.js"></script>' . "\n";
      break;
    case FILENAME_PRODUCT_INFO:
      echo '<link rel="stylesheet" type="text/css" href="styleleftcol.css" />' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>' . "\n";
      echo '<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>' . "\n";
      echo '<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />' . "\n";
      break;
    case FILENAME_CREATE_ACCOUNT:
      echo '<script type="text/javascript" src="includes/javascript/new_account.js"></script>' . "\n";
      break;
    default:
      break;
  }
?>
<link rel="stylesheet" href="includes/styles/slider/default/default.css" type="text/css" media="screen" />
<link rel="stylesheet" href="includes/styles/slider/light/light.css" type="text/css" media="screen" />
<link rel="stylesheet" href="includes/styles/slider/dark/dark.css" type="text/css" media="screen" />
<link rel="stylesheet" href="includes/styles/slider/themes/bar/bar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="includes/styles/slider/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="includes/styles/slider/style.css" type="text/css" media="screen" />
<?php
/*
<link rel="stylesheet" type="text/css" href="includes/javascript/livesearch/livesearch.css" />
<script language="javascript" type="text/javascript" src="includes/javascript/livesearch/livesearch.js"></script>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5785335-3");
pageTracker._trackPageview();
</script>
*/
?>
<script type="text/javascript" src="includes/javascript/jquery.nivo.slider.js"></script>