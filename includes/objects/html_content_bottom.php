<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Common html bottom of page
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
        <div class="cleaner buttonsRow vpad vspacer">
<?php
  if( !isset($html_lines_array) || !is_array($html_lines_array) || !count($html_lines_array) ) {
    $html_lines_array = array();
    $html_lines_array[] = '<div class="rspacer ralign"><a href="' . tep_href_link() . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE . '</a></div>' . "\n";
  }
  foreach($html_lines_array as $key => $value) {
    echo $value;
  }
  unset($html_lines_array);
?>
        </div>