<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Generic Pages for Catalog end
// Front End support for the generic text pages
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . $g_script);

  if( empty($current_gtext_id) && empty($current_abstract_id) ) {
    tep_redirect(tep_href_link());
  }

  if( !empty($current_gtext_id) ) {
    $nav_query = tep_db_query("select gtext_id as id, gtext_title as text from " . TABLE_GTEXT . " where gtext_id='" . (int)$current_gtext_id . "' and status='1'");
    $nav_array = tep_db_fetch_array($nav_query);
    $nav_array['param'] = 'gtext_id';
  } else {
    $nav_query = tep_db_query("select abstract_zone_id as id, abstract_zone_name as text from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id='" . (int)$current_abstract_id . "'");
    $nav_array = tep_db_fetch_array($nav_query);
    $nav_array['param'] = 'abz_id';
  }

  $breadcrumb->add($nav_array['text'], tep_href_link($g_script, $nav_array['param'] . '=' . $nav_array['id']));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php 
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
?>
<?php

  if( $current_gtext_id ) {
    $gtext_query = tep_db_query("select gtext_title, gtext_description from " . TABLE_GTEXT . " where gtext_id='" . (int)$current_gtext_id . "' and status='1'");
    $gtext_array = tep_db_fetch_array($gtext_query);
?>
      <div><h1><?php echo strtoupper($gtext_array['gtext_title']); ?></h1></div>
      <div><?php echo $gtext_array['gtext_description']; ?></div>
<?php
    require(DIR_WS_MODULES . 'text_products.php');
  } else {
    $abstract_query = tep_db_query("select abstract_zone_name, abstract_zone_desc from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id='" . (int)$current_abstract_id . "'");
    $abstract_array = tep_db_fetch_array($abstract_query);

    $cText = new gtext_front;
    $zones_array = $cText->get_gtext_entries($current_abstract_id, true, false);
?>
      <div><h1><?php echo strtoupper($abstract_array['abstract_zone_name']); ?></h1></div>
      <div><?php echo $abstract_array['abstract_zone_desc']; ?></div>
      <div class="bounder vspacer">
<?php
    foreach($zones_array as $key => $value ) { 
?>
        <div class="floater quarter">
          <div class="highter rspacer"><?php echo '<a href="' . tep_href_link($g_script, 'gtext_id=' . $key) . '" rel="nofollow">' . strtoupper($value['gtext_title']) . '</a>'; ?></div>
        </div>
<?php
    }
?>
      </div>
<?php
  }
?>
      <div class="buttonsRow vpad tspacer">
        <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link() . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
<?php
/*
        <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link() . '">' . tep_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>'; ?></div>
*/
?>
      </div>
<?php require('includes/objects/html_end.php'); ?>
