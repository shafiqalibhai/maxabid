<?php
/*
  $Id: customer_testimonials.php,v 1.3 2003/12/08 Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Copyright (c) 2000,2001 The Exchange Project

  Contributed by http://www.seen-online.co.uk

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Privacy page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $testimonial_id = (isset($_GET['testimonial_id']) ? $_GET['testimonial_id'] : '');
    
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_TESTIMONIALS);
  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_TESTIMONIALS));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  if ($testimonial_id != '') {
    $heading_row = true;
  }
?>
<?php require('includes/objects/html_body_header.php'); ?>
      <tr>
        <td>
<?php
  if ($testimonial_id != '') {
    $full_testimonial = tep_db_query("select * FROM " . TABLE_TESTIMONIALS . " WHERE status = '1' and testimonials_id = '" . (int)$testimonial_id . "'");
  }
  else {
    $full_testimonial = tep_db_query("select * FROM " . TABLE_TESTIMONIALS . " WHERE status = '1'");
  }
  while ($testimonials = tep_db_fetch_array($full_testimonial)) {
/*
    $testimonial_array[] = array('id' => $testimonials['testimonials_id'],
                                 'author' => $testimonials['testimonials_name'],
                                 'testimonial' => $testimonials['testimonials_html_text'],
                                 'word_count' => tep_word_count($testimonials['testimonials_html_text'], ' '),
                                 'url' => $testimonials['testimonials_url'],
                                 'url_title' => $testimonials['testimonials_url_title']);
*/


    $info_box_contents = array();
    $info_box_contents[] = array('text' => sprintf(TEXT_TESTIMONIAL_BY, '<b>' . $testimonials['testimonials_name'] . '</b>'));
  //  new infoBoxHeading($info_box_contents, false, false, false, 'main');

    $info_box_contents = array();
    $info_box_contents[] = array('text' => $testimonials['testimonials_html_text']);
    if( tep_not_null($testimonials['testimonials_url']) ) {
      $info_box_contents[] = array('text' => '<a href="' . $testimonials['testimonials_url'] . '">' . $testimonials['testimonials_url_title'] . '</a>');
    }
    new contentBox($info_box_contents);
  }
?>
        </td>
      </tr>
<?php
  $html_lines_array = array();
  $html_lines_array[] = '<td align="right"><a href="' . tep_href_link(FILENAME_TESTIMONIALS) . '">' . tep_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a></td>' . "\n";
  require('includes/objects/html_content_bottom.php'); 
?>
<?php require('includes/objects/html_end.php'); ?>
