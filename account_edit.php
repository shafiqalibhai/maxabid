<?php
/*
  $Id: account_edit.php,v 1.65 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account Edit page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_EDIT);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch($action) {
    case 'process':   
      if (ACCOUNT_GENDER == 'true') $gender = tep_db_prepare_input($_POST['gender']);
      $firstname = tep_db_prepare_input($_POST['firstname']);
      $lastname = tep_db_prepare_input($_POST['lastname']);
      $nickname = tep_db_prepare_input($_POST['nickname']);
      if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($_POST['dob']);
      $email_address = tep_db_prepare_input($_POST['email_address']);
      $telephone = tep_db_prepare_input($_POST['telephone']);
      $fax = tep_db_prepare_input($_POST['fax']);

      $error = false;

      $nickname = tep_create_safe_string($nickname);
      $length = strlen($nickname);
      if( $length < 8 || $length > 32 || $nickname != tep_db_prepare_input($_POST['nickname']) ) {
        $error = true;
        $messageStack->add('account_edit', ERROR_NICKNAME_LENGTH);
      }
      $check_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_id != '" . (int)$customer_id . "' and customers_nickname = '" . tep_db_input($nickname) . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( $check_array['total'] ) {
        $error = true;
        $messageStack->add('account_edit', ERROR_NICKNAME_EXISTS);
      }

      if (ACCOUNT_GENDER == 'true') {
        if ( ($gender != 'm') && ($gender != 'f') ) {
          $error = true;

          $messageStack->add('account_edit', ENTRY_GENDER_ERROR);
        }
      }

      if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
      }

      if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
      }

      if (strlen($nickname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add('account_edit', ENTRY_NICK_NAME_ERROR);
      }

      if (ACCOUNT_DOB == 'true') {
        if (!checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4))) {
          $error = true;

          $messageStack->add('account_edit', ENTRY_DATE_OF_BIRTH_ERROR);
        }
      }

      if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR);
      }

      if (!tep_validate_email($email_address)) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
      }

      $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "' and customers_id != '" . (int)$customer_id . "'");
      $check_email = tep_db_fetch_array($check_email_query);
      if ($check_email['total'] > 0) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
      }

      if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
      }

      if( $error == false ) {
        $sticky_poll = isset($_POST['sticky_poll'])?1:0;

        $sql_data_array = array(
          'customers_firstname' => $firstname,
          'customers_lastname' => $lastname,
          'customers_nickname' => $nickname,
          'customers_email_address' => $email_address,
          'customers_telephone' => $telephone,
          'customers_fax' => $fax,
          'sticky_poll' => $sticky_poll,
        );

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

        $sql_data_array = array('entry_firstname' => $firstname,
                                'entry_lastname' => $lastname);

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$customer_default_address_id . "'");

        // reset the session variables
        $customer_first_name = $firstname;

        $messageStack->add_session('account', SUCCESS_ACCOUNT_UPDATED, 'success');

        tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
      }
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
<?php
  $account_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $account = tep_db_fetch_array($account_query);
?>
      <div class="bounder"><?php echo tep_draw_form('addressbook', tep_href_link($g_script,  'action=process', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="bounder vpad bspacer altrow heavy">
          <div class="floatend rspacer"><span class="frequired vpad"><?php echo FORM_REQUIRED_INFORMATION; ?></span></div>
        </div>
        <div class="vpad"><label for="email" class="halfer floater"><?php echo ENTRY_EMAIL_ADDRESS; ?></label><?php echo tep_draw_input_field('email_address', $account['customers_email_address'], 'id="email"', 'text', true, (isset($error_array['firstname'])?$error_array['firstname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="firstname" class="halfer floater"><?php echo ENTRY_FIRST_NAME; ?></label><?php echo tep_draw_input_field('firstname', $account['customers_firstname'], 'id="firstname"', 'text', true, (isset($error_array['firstname'])?$error_array['firstname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="lastname" class="halfer floater"><?php echo ENTRY_LAST_NAME; ?></label><?php echo tep_draw_input_field('lastname', $account['customers_lastname'], 'id="lastname"', 'text', true, (isset($error_array['lastname'])?$error_array['lastname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="nickname" class="halfer floater"><?php echo ENTRY_NICK_NAME; ?></label><?php echo tep_draw_input_field('nickname', $account['customers_nickname'], 'id="nickname"', 'text', true, (isset($error_array['nickname'])?$error_array['nickname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="telephone" class="halfer floater"><?php echo ENTRY_TELEPHONE_NUMBER; ?></label><?php echo tep_draw_input_field('telephone', $account['customers_telephone'], 'id="telephone"', 'text', true, (isset($error_array['telephone'])?$error_array['telephone']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="fax" class="halfer floater"><?php echo ENTRY_FAX_NUMBER; ?></label><?php echo tep_draw_input_field('fax', $account['customers_fax'], 'id="fax"', 'text', true, (isset($error_array['fax'])?$error_array['fax']:false)); ?></div>
        <div class="vpad"><span class="floater rspacer"><?php echo tep_draw_checkbox_field('sticky_poll', 1, $account['sticky_poll']?true:false, 'id="sticky"'); ?></span><label for="sticky"><?php echo TEXT_INFO_ENABLE_STICKY_POLL; ?></label></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_UPDATE . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ADDRESS_BOOK . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
