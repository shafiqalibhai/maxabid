<?php
/*
  $Id: product_info.php,v 1.97 2003/07/01 14:34:54 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Product Info page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  define('INFO_IMAGES_PER_ROW', 4);
  require('includes/application_top.php');

  if( !$current_product_id ) {
    $g_validator->process_error();
  }

  require(DIR_WS_LANGUAGES . $language . '/' . basename($PHP_SELF));
  require(DIR_WS_LANGUAGES . $language . '/' . 'profanity.php');
  require(DIR_WS_LANGUAGES . $language . '/' . 'restricted.php');


  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch($action) {
    case 'process':
      if( !isset($_POST['products_info_review_x']) || !isset($_POST['products_info_review_y']) ||
          !tep_not_null($_POST['products_info_review_x']) || !tep_not_null($_POST['products_info_review_y']) ) {

        tep_redirect(tep_href_link('', '', 'NONSSL', false));
      }

      if( isset($_POST['rating']) )
        $rating = $_POST['rating'];
      else
        $rating = 0;

      if( isset($_POST['review']) )
        $review = $_POST['review'];
       else
        $review = '';

      $error = false;
      if (strlen($review) < REVIEW_TEXT_MIN_LENGTH) {
        $error = true;

        $messageStack->add('products_info_review', JS_REVIEW_TEXT);
      }

      if (($rating < 1) || ($rating > 5)) {
        $error = true;
        $messageStack->add('products_info_review', JS_REVIEW_RATING);
      }

//-MS- Added anti-span traffic utilization
      if($g_cSpam->scan_fields($review) ) {
        $error = true;
        $messageStack->add_session('product_info', TEXT_REVIEW_SUCCESS, 'success');
        tep_redirect(tep_href_link(basename($PHP_SELF), 'products_id=' . $current_product_id));
      }
//-MS- Added anti-span traffic utilization

      if ($error == false) {
//-MS- Added restricted/profanity words
        if( !restricted_check($review, $restricted_words) ) {
          $review = profanity_check($review, $profanity_words);

          if (tep_session_is_registered('customer_id')) {
            $customer_query = tep_db_query("select CONCAT(customers_firstname, ' ', customers_lastname) as name from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
            if( $customer_array = tep_db_fetch_array($customer_query) ) {
              tep_db_query("insert into " . TABLE_REVIEWS . " (products_id, customers_id, customers_name, reviews_rating, date_added) values ('" . (int)$current_product_id . "', '" . (int)$customer_id . "', '" . $customer_array['name'] . "', '" . (int)$rating . "', now())");
            } else {
              tep_db_query("insert into " . TABLE_REVIEWS . " (products_id, customers_name, reviews_rating, date_added) values ('" . (int)$current_product_id . "', 'Guest', '" . (int)$rating . "', now())");
            }
          } else {
            //tep_db_query("insert into " . TABLE_REVIEWS . " (products_id, customers_name, reviews_rating, date_added) values ('" . (int)$current_product_id . "', '" . tep_db_input($_POST['customers_firstname']) . ' ' . tep_db_input($_POST['customers_lastname']) . "', '" . tep_db_input($rating) . "', now())");
            tep_db_query("insert into " . TABLE_REVIEWS . " (products_id, customers_name, reviews_rating, date_added) values ('" . (int)$current_product_id . "', 'Guest', '" . (int)$rating . "', now())");
          }
          $insert_id = tep_db_insert_id();
          tep_db_query("insert into " . TABLE_REVIEWS_DESCRIPTION . " (reviews_id, languages_id, reviews_text) values ('" . (int)$insert_id . "', '" . (int)$languages_id . "', '" . tep_db_input(tep_db_prepare_input($review)) . "')");
          $messageStack->add_session('product_info', TEXT_REVIEW_SUCCESS, 'success');
        }
        tep_redirect(tep_href_link(basename($PHP_SELF), 'products_id=' . $current_product_id));
      }
//-MS- Added restricted/profanity words EOM
      break;
    default:
      break;
  }

  tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1, last_accessed=now() where products_id = '" . (int)$current_product_id . "' and language_id = '" . (int)$languages_id . "'");

//-MS- Filters Added
  $filters_list = $g_filters->get_filters_db_names();
  $filters_string = '';
  foreach($filters_list as $key => $value) {
    $filters_string .= 'p.' . $key . ',';
  }
//-MS- Filters Added EOM

  $product_info_query = tep_db_query("select p.products_id, " . tep_db_input(tep_db_prepare_input($filters_string)) . " p.products_status, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image,p.products_disable_cart, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id, m.manufacturers_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id = pd.products_id) left join " . TABLE_MANUFACTURERS . " m on (p.manufacturers_id=m.manufacturers_id) where p.products_display='1' and p.products_id = '" . (int)$current_product_id . "' and pd.language_id = '" . (int)$languages_id . "'");
     $product_info = tep_db_fetch_array($product_info_query);
  $real_price = $product_info['products_price'];
  $tmp_price = tep_get_products_special_price($product_info['products_id']);
  if( tep_not_null($tmp_price) ) {
    $real_price = $tmp_price;
  }

?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript" type="text/javascript"><!--
function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>

<?php
  if ($messageStack->size('products_info_review') > 0) {
?>
  <div><?php echo $messageStack->output('products_info_review'); ?></div>
<?php
  }

  $product_info['specials_new_products_price'] = tep_get_products_special_price($product_info['products_id']);
  if (tep_not_null($product_info['specials_new_products_price'])) {
    $new_price = '<span class="textPrice">' . TEXT_RETAIL . '</span>&nbsp;<span class="oldPrice">' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span><br>';
    $new_price .= '<span class="textPrice">' . TEXT_SPECIAL_PRICE . '</span>&nbsp;<span class="productSpecialPrice" id="swap_price2">' . $currencies->display_price($product_info['specials_new_products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
  } else {
    $new_price = '<span class="textPrice">' . TEXT_OUR_PRICE . '</span>&nbsp;<span class="productSpecialPrice" id="swap_price2">' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
  }

//    if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
//      $products_price = '<s>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</s> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
//    } else {
//      $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
//    }


//    if (tep_not_null($product_info['products_model'])) {
//      $products_name = strtoupper($product_info['products_name']) . '&nbsp;&raquo;&nbsp;<span class="smallText">' . $product_info['products_model'] . '</span>';
//    } else {
//      $products_name = strtoupper($product_info['products_name']);
//    }
             
  $products_name = strtoupper($product_info['products_name']);
  $script = basename($PHP_SELF);
  $action_params = '&action=add_product';
  if( count($g_group_fields->get_product_groups($current_product_id)) ) {
    $script = FILENAME_PRODUCT_INFO_OPTIONS;
    $action_params = '&action=add_options';
  }   
?>
        <div><?php echo tep_draw_form('cart_quantity', tep_href_link($script, 'products_id=' . $current_product_id . $action_params), 'post','enctype="multipart/form-data"'); ?><div>
<?php
    $category_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$current_product_id . "' limit 1");
    $category_array = tep_db_fetch_array($category_query);
    $category_query = tep_db_query("select cd.categories_name, c.logo_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$category_array['categories_id'] . "' and cd.categories_id = '" . (int)$category_array['categories_id'] . "' and cd.language_id = '" . (int)$languages_id . "'");
    $category_array = tep_db_fetch_array($category_query);
?>
          <div><h1><?php echo strtoupper($category_array['categories_name']) . '<br /><span class="subline">' . $products_name . '</span>'; ?></h1></div>
          <div class="bounder">
<?php
  $display_image = '';
  $display_params = 'pID=' . $product_info['products_id'];
  $display_price = '';
  $display_name = $products_name . '<br />';

  if( !tep_not_null($product_info['products_image']) || !file_exists(DIR_WS_IMAGES . $product_info['products_image']) || filesize(DIR_WS_IMAGES . $product_info['products_image']) <= 0 ) {
    $display_image = IMAGE_NOT_AVAILABLE;
  } else {
    $display_image = $product_info['products_image'];
  }

  $frame_output = '<a href="javascript:void(0)" rel="nofollow" onclick="popupWindow(\'' . tep_href_link(FILENAME_POPUP_IMAGE, 'pID=' . $product_info['products_id']) . '\')" name="swap_src" id="swap_src" class="enlarge_frame">' . tep_image(DIR_WS_IMAGES . $display_image, addslashes($product_info['products_name']), LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, 'id="swap_cell"') . '<br /><span id="swap_name">' . $display_name . '</span><span class="swap_red" id="swap_price">' . $display_price . '</span></a>';
    //$frame_output = '<a href="javascript:void(0)" rel="nofollow" onclick="popupWindow(\'' . tep_href_link(FILENAME_POPUP_IMAGE, 'pID=' . $product_info['products_id']) . '\')" name="swap_src" id="swap_src" class="enlarge_frame">' . tep_image(DIR_WS_IMAGES . $display_image, addslashes($product_info['products_name']), LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, 'id="swap_cell"') . '<br /><span id="swap_name">' . $display_name . '</span></a>';
?>
            <div class="floater calign" style="width: 300px; padding-top: 10px;"><?php echo $frame_output . '<span class="blocker">' . TEXT_CLICK_TO_ENLARGE . '</span>'; ?></div>
            <div class="extend" style="border-left: 2px solid #d6d6d6;"></div>
            <div class="floater" style="width: 340px; padding-left: 16px;">
              <div class="bounder calign">
<?php
  $tmp_fields_array = array();
  if( !tep_has_product_attributes($current_product_id) ) {
    $tmp_fields_array = tep_get_bracket_fields($current_product_id, true);
  }

  $images_array = array();
  $images_info_array = array();
  $image_flag = false;


  $tmp_fields_array = array();
  if( !tep_has_product_attributes($current_product_id) ) {
    $tmp_fields_array = tep_get_bracket_fields($current_product_id, true);
  }

  $images_array = array();
  $images_info_array = array();
  $image_flag = false;

//-MS- Filters added
  $header_set = false;
  foreach($filters_list as $key => $value) {
    $option = $g_filters->get_option_name_from_db_column($key,$product_info[$key]);
    if( tep_not_null($option) ) {
      if( !$header_set ) {
        $header_set = true;
?>
<?php 
        $tmp_name = $product_info['products_name'];
        $tmp_array = explode(' ' , $product_info['products_name']);
        if( is_array($tmp_array) && count($tmp_array) && isset($tmp_array[count($tmp_array)-1]) && strtoupper($tmp_array[count($tmp_array)-1]) == strtoupper(TEXT_PRODUCTS_POSTFIX)) {
          unset($tmp_array[count($tmp_array)-1]);
          $tmp_name = implode(' ', $tmp_array);
          $tmp_name  = htmlspecialchars($tmp_name);
        }
?>
                <div><b><?php echo $tmp_name . ' Features'; ?></b></div>
<?php
      }
?>
                <div><?php echo '<b>' . $g_filters->get_filters_name($value) . ':</b>' . $g_filters->get_option_name_from_db_column($key,$product_info[$key]); ?></b></div>
<?php
    }
  }
//-MS- Filters EOM
?>
<?php
  if( tep_not_null($product_info['manufacturers_name']) ) {
?>
                <div><?php echo '<b>' . TEXT_MANUFACTURER . '</b> ' . $product_info['manufacturers_name']; ?></div>
<?php
  }
?>
<?php
  $products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$current_product_id . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "'");
  $products_attributes = tep_db_fetch_array($products_attributes_query);
  if ($products_attributes['total'] > 0) {
?>
                <div><?php echo TEXT_PRODUCT_OPTIONS; ?></div>
<?php
    $products_options_name_query = tep_db_query("select distinct popt.products_options_id, popt.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int)$current_product_id . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int)$languages_id . "' order by popt.products_options_name");
    while ($products_options_name = tep_db_fetch_array($products_options_name_query)) {
      $products_options_array = array();
      $products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int)$current_product_id . "' and pa.options_id = '" . (int)$products_options_name['products_options_id'] . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . (int)$languages_id . "'");
      while ($products_options = tep_db_fetch_array($products_options_query)) {
        $products_options_array[] = array('id' => $products_options['products_options_values_id'], 'text' => $products_options['products_options_values_name']);
        if ($products_options['options_values_price'] != '0') {
          $products_options_array[sizeof($products_options_array)-1]['text'] .= ' (' . $products_options['price_prefix'] . $currencies->display_price($products_options['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) .') ';
        }
      }

      if (isset($cart->contents[$current_product_id]['attributes'][$products_options_name['products_options_id']])) {
        $selected_attribute = $cart->contents[$current_product_id]['attributes'][$products_options_name['products_options_id']];
      } else {
        $selected_attribute = false;
      }
?>
                <div class="floater"><b><?php echo $products_options_name['products_options_name'] . ':'; ?></b></div>
                <div class="floater"><?php echo tep_draw_pull_down_menu('id[' . $products_options_name['products_options_id'] . ']', $products_options_array, $selected_attribute); ?></div>

<?php
    }
?>
<?php
  }
?>
                <div class="vpad"></div>
                <div class="cleaner">
<?php
  if( $product_info['products_status'] ) {
?>

                  <div style="padding: 14px 0px 6px 0px;"><?php echo $new_price; ?></div>
<?php
  }  
  if( $product_info['products_status'] ) {
    if( count($g_group_fields->get_product_groups($current_product_id)) ) {
      $select_array = array(
                            array('id' => '0', 'text' => TEXT_OPTIONS_SELECT),
                            array('id' => '1', 'text' => TEXT_OPTIONS_ADD_CART),
                            array('id' => '2', 'text' => TEXT_OPTIONS_ADD_MORE),
                           );

      echo '<div class="bounder">' . "\n";
      echo '  <div class="floater">' . tep_draw_pull_down_menu('options_select', $select_array) . '</div>';
      echo '  <div class="floater">' . tep_image_submit('button_continue.gif', 'Additional options are available for ' . $product_info['products_name'] . ', click here to ' . IMAGE_BUTTON_CONTINUE) . '</div>' . "\n"; 
      echo '</div>' . "\n";
    } 
	
    if (!$product_info['products_disable_cart']) {
      echo '<div class="calign">' . '<a class="mbutton2">' . IMAGE_BUTTON_IN_CART_DISABLED . '</a>' . '</div>';
    }
    /*elseif () {
      echo '<div class="calign">' . '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_IN_CART . '</a>' . '</div>';
    }*/
    else {
      echo '<div class="calign">' . '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_IN_CART . '</a>' . '</div>';
    }
  } else {
    //echo '<div>' . tep_image_button('button_out_of_stock.gif', IMAGE_BUTTON_OUT_OF_STOCK) . '</div>';
  }
?>
                </div>
<?php
  if( $product_info['products_status'] ) {
?>
    <div class="vpad calign"><?php echo tep_draw_hidden_field('products_id', $product_info['products_id']); ?></div>
<?php
  }
?>
              </div>
            </div>
          </div>
<?php
  if(DISPLAY_EXTRA_IMAGES == 'true') {
    include (DIR_WS_MODULES . 'products_extra_images.php');
  } 
?>
          <div style="margin-left: -8px; padding-top: 4px; border-bottom: 1px solid #CCC;"></div>
        </div></form></div>
        <div class="vpad"><h3><?php echo strtoupper($product_info['products_name'] . ' ' . HEADING_DESCRIPTION); ?></h3></div>
        <div><?php echo stripslashes($product_info['products_description']); ?></div>
        <div class="vpad"></div>
<?php
  include(DIR_WS_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
  //include(DIR_WS_MODULES . 'product_reviews_info.php');
  //include(DIR_WS_MODULES . FILENAME_PRODUCT_REVIEWS_WRITE);
?>
<?php require('includes/objects/html_end.php'); ?>
