<?php
/*
  $Id: advanced_search_result.php,v 1.72 2003/06/23 06:50:11 project3000 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  // split page-results via post
  require(DIR_WS_CLASSES . 'post_page_results.php');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);

  $adv_array = array();
  $error = false;

  if ( (isset($_POST['keywords']) && empty($_POST['keywords'])) &&
       (isset($_POST['dfrom']) && (empty($_POST['dfrom']) || ($_POST['dfrom'] == DOB_FORMAT_STRING))) &&
       (isset($_POST['dto']) && (empty($_POST['dto']) || ($_POST['dto'] == DOB_FORMAT_STRING))) &&
       (isset($_POST['pfrom']) && !is_numeric($_POST['pfrom'])) &&
       (isset($_POST['pto']) && !is_numeric($_POST['pto'])) ) {
    $error = true;
    $messageStack->add_session('advanced_search', ERROR_AT_LEAST_ONE_INPUT);
  } else {
    $dfrom = '';
    $dto = '';
    $pfrom = '';
    $pto = '';
    $keywords = '';

    if (isset($_POST['dfrom'])) {
      $dfrom = (($_POST['dfrom'] == DOB_FORMAT_STRING) ? '' : tep_db_prepare_input($_POST['dfrom']));
      $adv_array['dfrom'] = $dfrom;
    }

    if (isset($_POST['dto'])) {
      $dto = (($_POST['dto'] == DOB_FORMAT_STRING) ? '' : tep_db_prepare_input($_POST['dto']));
      $adv_array['dto'] = $dto;
    }

    if (isset($_POST['pfrom'])) {
      $pfrom = tep_db_prepare_input($_POST['pfrom']);
      $adv_array['pfrom'] = $pfrom;
    }

    if (isset($_POST['pto'])) {
      $pto = tep_db_prepare_input($_POST['pto']);
      $adv_array['pto'] = $pto;
    }

    if (isset($_POST['keywords'])) {
      $keywords = tep_db_prepare_input($_POST['keywords']);
      $adv_array['keywords'] = $keywords;
    }

    $date_check_error = false;
    if (tep_not_null($dfrom)) {
      if (!tep_checkdate($dfrom, DOB_FORMAT_STRING, $dfrom_array)) {
        $error = true;
        $date_check_error = true;
        $messageStack->add_session('advanced_search', ERROR_INVALID_FROM_DATE);
      }
    }

    if (tep_not_null($dto)) {
      if (!tep_checkdate($dto, DOB_FORMAT_STRING, $dto_array)) {
        $error = true;
        $date_check_error = true;

        $messageStack->add_session('advanced_search', ERROR_INVALID_TO_DATE);
      }
    }

    if (($date_check_error == false) && tep_not_null($dfrom) && tep_not_null($dto)) {
      if (mktime(0, 0, 0, $dfrom_array[1], $dfrom_array[2], $dfrom_array[0]) > mktime(0, 0, 0, $dto_array[1], $dto_array[2], $dto_array[0])) {
        $error = true;

        $messageStack->add_session('advanced_search', ERROR_TO_DATE_LESS_THAN_FROM_DATE);
      }
    }

    $price_check_error = false;
    if (tep_not_null($pfrom)) {
      if (!settype($pfrom, 'double')) {
        $error = true;
        $price_check_error = true;

        $messageStack->add_session('advanced_search', ERROR_PRICE_FROM_MUST_BE_NUM);
      }
    }

    if (tep_not_null($pto)) {
      if (!settype($pto, 'double')) {
        $error = true;
        $price_check_error = true;

        $messageStack->add_session('advanced_search', ERROR_PRICE_TO_MUST_BE_NUM);
      }
    }

    if (($price_check_error == false) && is_float($pfrom) && is_float($pto)) {
      if ($pfrom >= $pto) {
        $error = true;

        $messageStack->add_session('advanced_search', ERROR_PRICE_TO_LESS_THAN_PRICE_FROM);
      }
    }

    if (tep_not_null($keywords)) {
      if (!tep_parse_search_string($keywords, $search_keywords)) {
        $error = true;

        $messageStack->add_session('advanced_search', ERROR_INVALID_KEYWORDS);
      }
    }
  }

  if (empty($dfrom) && empty($dto) && empty($pfrom) && empty($pto) && empty($keywords)) {
    $error = true;

    $messageStack->add_session('advanced_search', ERROR_AT_LEAST_ONE_INPUT);
  }

  if ($error == true) {
    tep_redirect(tep_href_link(FILENAME_ADVANCED_SEARCH));
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ADVANCED_SEARCH));
//  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT));
  header("Cache-Control: public");
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <tr>
        <td class="all_borders"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr class="contentBoxContents">
            <td class="pageHeading"><h1><?php echo HEADING_TITLE_2; ?></h1></td>
<?php
/*
            <td class="pageHeading" align="right"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_browse.gif', HEADING_TITLE_2, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
*/
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td>
<?php
  $select_str = "select pd.products_id, p.products_image, p.products_price, p.products_model, p.products_status, p.products_tax_class_id ";
  $from_str = "from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id)";
  $where_str = " where p.products_display = '1' and pd.language_id = '" . (int)$languages_id . "' ";

  if (isset($_POST['categories_id']) && tep_not_null($_POST['categories_id'])) {
    if (isset($_POST['inc_subcat']) && ($_POST['inc_subcat'] == '1')) {
      $subcategories_array = array();
      tep_get_subcategories($subcategories_array, $_POST['categories_id']);

      $from_str .= " left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (pd.products_id = p2c.products_id)";
      $where_str .= " and (p2c.categories_id = '" . (int)$_POST['categories_id'] . "'";

      for ($i=0, $n=sizeof($subcategories_array); $i<$n; $i++ ) {
        $where_str .= " or p2c.categories_id = '" . (int)$subcategories_array[$i] . "'";
      }

      $where_str .= ")";
    } else {
      $from_str .= " left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (pd.products_id = p2c.products_id)";
      $where_str .= " and p2c.categories_id = '" . (int)$_POST['categories_id'] . "'";
    }
  }



/*
  $select_str = "select distinct p.products_id, p.products_image, m.manufacturers_id, p.products_status, pd.products_name, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price ";

  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto)) ) {
    $select_str .= ", SUM(tr.tax_rate) as tax_rate ";
  }

  $from_str = "from " . TABLE_PRODUCTS . " p left join " . TABLE_MANUFACTURERS . " m using(manufacturers_id) left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id";

  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto)) ) {
    if (!tep_session_is_registered('customer_country_id')) {
      $customer_country_id = STORE_COUNTRY;
      $customer_zone_id = STORE_ZONE;
    }
    $from_str .= " left join " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id left join " . TABLE_ZONES_TO_GEO_ZONES . " gz on tr.tax_zone_id = gz.geo_zone_id and (gz.zone_country_id is null or gz.zone_country_id = '0' or gz.zone_country_id = '" . (int)$customer_country_id . "') and (gz.zone_id is null or gz.zone_id = '0' or gz.zone_id = '" . (int)$customer_zone_id . "')";
  }

  $from_str .= ", " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_CATEGORIES . " c, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c";

  $where_str = " where p.products_display = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p.products_id = p2c.products_id and p2c.categories_id = c.categories_id ";

  if (isset($_POST['categories_id']) && tep_not_null($_POST['categories_id'])) {
    if (isset($_POST['inc_subcat']) && ($_POST['inc_subcat'] == '1')) {
      $subcategories_array = array();
      tep_get_subcategories($subcategories_array, $_POST['categories_id']);

      $where_str .= " and p2c.products_id = p.products_id and p2c.products_id = pd.products_id and (p2c.categories_id = '" . (int)$_POST['categories_id'] . "'";

      for ($i=0, $n=sizeof($subcategories_array); $i<$n; $i++ ) {
        $where_str .= " or p2c.categories_id = '" . (int)$subcategories_array[$i] . "'";
      }

      $where_str .= ")";
    } else {
      $where_str .= " and p2c.products_id = p.products_id and p2c.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and p2c.categories_id = '" . (int)$_POST['categories_id'] . "'";
    }
  }

  if (isset($_POST['manufacturers_id']) && tep_not_null($_POST['manufacturers_id'])) {
    $where_str .= " and m.manufacturers_id = '" . (int)$_POST['manufacturers_id'] . "'";
  }
*/
  $key_str = '';
  $score_array = array();
  if (isset($search_keywords) && (sizeof($search_keywords) > 0)) {
    //$where_str .= " and (";
    $key_str .= " and (";
    for ($i=0, $n=sizeof($search_keywords); $i<$n; $i++ ) {
      switch ($search_keywords[$i]) {
        case '(':
        case ')':
        case 'and':
        case 'or':
          //$where_str .= " " . $search_keywords[$i] . " ";
          $key_str .= " " . $search_keywords[$i] . " ";
          break;
        default:
          $keyword = tep_db_prepare_input($search_keywords[$i]);
          //$where_str .= "(pd.products_name like '%" . tep_db_input($keyword) . "%' or p.products_model like '%" . tep_db_input($keyword) . "%' or m.manufacturers_name like '%" . tep_db_input($keyword) . "%'";
          $key_str .= "(pd.products_name like '%" . tep_db_input($keyword) . "%' or p.products_model like '%" . tep_db_input($keyword) . "%'";
          $tmp_str = "(" . "(pd.products_name like '%" . tep_db_input($keyword) . "%' or p.products_model like '%" . tep_db_input($keyword) . "%'";
          if (isset($_POST['search_in_description']) && ($_POST['search_in_description'] == '1')) {
            //$where_str .= " or pd.products_description like '%" . tep_db_input($keyword) . "%'";
            $key_str .= " or pd.products_description like '%" . tep_db_input($keyword) . "%'";
            $tmp_str .= " or pd.products_description like '%" . tep_db_input($keyword) . "%'";
          }
          //$where_str .= ')';
          $key_str .= ')';
          $tmp_str .= '))';
          $listing_sql = $select_str . $from_str . $where_str . ' and ' . $tmp_str;
          $listing_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
          if( $listing_split->number_of_rows > 0) {
            $score_array[md5($search_keywords[$i])] = $tmp_str;
          }
          break;
      }
    }
    //$where_str .= " )";
    $key_str .= " )";
  }

  if (tep_not_null($dfrom)) {
    $where_str .= " and p.products_date_added >= '" . tep_date_raw($dfrom) . "'";
  }

  if (tep_not_null($dto)) {
    $where_str .= " and p.products_date_added <= '" . tep_date_raw($dto) . "'";
  }

  if (tep_not_null($pfrom)) {
    if ($currencies->is_set($currency)) {
      $rate = $currencies->get_value($currency);

      $pfrom = $pfrom / $rate;
    }
  }

  if (tep_not_null($pto)) {
    if (isset($rate)) {
      $pto = $pto / $rate;
    }
  }
/*
  if (DISPLAY_PRICE_WITH_TAX == 'true') {
    if ($pfrom > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) >= " . (double)$pfrom . ")";
    if ($pto > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) <= " . (double)$pto . ")";
  } else {
    if ($pfrom > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) >= " . (double)$pfrom . ")";
    if ($pto > 0) $where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) <= " . (double)$pto . ")";
  }
*/
  if ($pfrom > 0) $where_str .= " and p.products_price >= " . (double)$pfrom . "";
  if ($pto > 0) $where_str .= " and p.products_price <= " . (double)$pto . "";

  $group_str = '';
/*
  if ( (DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto)) ) {
    $group_str = " group by p.products_id, tr.tax_priority";
  }
*/
  $order_str = ' order by p.products_status desc, pd.products_name';
  //$listing_sql = $select_str . $from_str . $where_str . $order_str;
  $listing_sql = $select_str . $from_str . $where_str . $key_str . $group_str . $order_str;
  $listing_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'pd.products_id');


  if( !($listing_split->number_of_rows) && isset($search_keywords) && count($search_keywords) > 0 ) {
    $max_threshold = 1;
    for($i=0, $n=sizeof($search_keywords); $i<$n; $i++ ) {
      $found_flag = false;
      switch($search_keywords[$i]) {
        case '(':
        case ')':
        case 'and':
        case 'or':
          //$key_str .= " " . $search_keywords[$i] . " ";
          break;
        default:
          if( isset($score_array[md5($search_keywords[$i])]) ) {
            continue;
          }

          $key_len = strlen($search_keywords[$i]);
          if( $key_len <= $max_threshold ) {
            break;
          }

          unset($left_str, $left_count, $listing_left_split, $right_str, $right_count, $listing_right_split);
          for( $left_threshold = $key_len-1; $left_threshold > $max_threshold; $left_threshold-- ) {
            $key_str = " and (";
            $keyword = tep_db_prepare_input($search_keywords[$i]);
            $keyword = substr($keyword, 0, $left_threshold);
            $key_str .= "(pd.products_name like '%" . tep_db_input($keyword) . "%' or p.products_model like '%" . tep_db_input($keyword) . "%'";
            if (isset($_POST['search_in_description']) && ($_POST['search_in_description'] == '1')) {
              $key_str .= " or pd.products_description like '%" . tep_db_input($keyword) . "%'";
            }
            $key_str .= ')';
            $key_str .= " )";
            $listing_sql = $select_str . $from_str . $where_str . $key_str . $group_str . $order_str;
            $listing_left_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');

            if ( $listing_left_split->number_of_rows > 0 ) {
              $left_key = $key_str;
              $left_count = $listing_left_split->number_of_rows;
              $left_str = $listing_sql;
              $found_flag = true;
              break;
            }
          }

          for( $right_threshold = $key_len-1; $right_threshold > $max_threshold; $right_threshold-- ) {
            $key_str = " and (";
            $keyword = tep_db_prepare_input($search_keywords[$i]);
            $keyword = substr($keyword, -$right_threshold);
            $key_str .= "(pd.products_name like '%" . tep_db_input($keyword) . "%' or p.products_model like '%" . tep_db_input($keyword) . "%'";
            if (isset($_POST['search_in_description']) && ($_POST['search_in_description'] == '1')) {
              $key_str .= " or pd.products_description like '%" . tep_db_input($keyword) . "%'";
            }
            $key_str .= ')';
            $key_str .= " )";
            $listing_sql = $select_str . $from_str . $where_str . $key_str . $group_str . $order_str;
            $listing_right_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');

            // If the word was previously processed and results found skip the keyword
            if( $listing_right_split->number_of_rows > 0 ) {
              $right_key = $key_str;
              $right_count = $listing_right_split->number_of_rows;
              $right_str = $listing_sql;
              $found_flag = true;
              break;
            }
          }

          if($found_flag && isset($right_count) && isset($left_count) ) {
            // If both scans are successful, check percentages and promote left scan
            if( $left_threshold >= $right_threshold-1 || $right_threshold <= 1 ) {
              $listing_sql = $select_str . $from_str . $where_str . $left_key;
            } elseif($left_count < $right_count) {
              $listing_sql = $select_str . $from_str . $where_str . $left_key;
            } else {
              $listing_sql = $select_str . $from_str . $where_str . $right_key;
            }
          } elseif($found_flag && isset($right_count))  {
            $listing_sql = $select_str . $from_str . $where_str . $right_key;
          } elseif($found_flag && isset($left_count)) {
            $listing_sql = $select_str . $from_str . $where_str . $left_key;
          }

          if( $found_flag ) {
            $tmp_str = '';
            if( count($score_array) ) {
              $tmp_str = " and " . implode(" and ", $score_array);
            }
            $org_sql = $listing_sql;
            $listing_sql .= $tmp_str;
            $listing_sql .= $group_str . $order_str;
            $listing_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
            if( !$listing_split->number_of_rows) {
              $listing_sql = $org_sql;
              $tmp_str = '';
              if( count($score_array) ) {
                $tmp_str = " and " . implode(" or ", $score_array);
              }
              $listing_sql .= $tmp_str;
              $listing_sql .= $group_str . $order_str;
              $listing_split = new postPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
            } else {
            }

            break 2;
          }
          break;
      }
    }
  }

  if ( ($listing_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
    <td class="smallText" align="right"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $adv_array); ?></td>
  </tr>
</table>
<?php
  }

  // Initialize vars
  $total_items = array();
  tep_query_to_array($listing_split->sql_query, $total_items);

  $j=count($total_items);

  if( $j > 0 ) {
    require('includes/objects/product_listing_common.php');
  } else {
?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_NO_PRODUCTS; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table>
<?php
  }

  if ( ($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) ) {
?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
    <td class="smallText" align="right"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $adv_array); ?></td>
  </tr>
</table>
<?php
  }
?>
        </td>
      </tr>
<?php 
  $html_lines_array = array();
  $html_lines_array[] = '<td class="main"><a href="' . tep_href_link(FILENAME_ADVANCED_SEARCH) . '">' . tep_image_button('button_back.gif', IMAGE_BUTTON_BACK) . '</a></td>' . "\n";
  require('includes/objects/html_content_bottom.php'); 
?>
<?php require('includes/objects/html_end.php'); ?>
