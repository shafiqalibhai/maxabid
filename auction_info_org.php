<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Auction Information Page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . $g_script);

  $auctions_id = isset($_GET['auctions_id'])?(int)$_GET['auctions_id']:0;
  $check_query = tep_db_query("select auctions_id, auctions_name, auctions_image, auctions_description, auctions_type, products_id, status_id, bid_step, date_start, date_expire, start_pause, end_pause, start_price from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
  if( !tep_db_num_rows($check_query) ) {
    $messageStack->add_session(tep_get_script_name(), ERROR_AUCTION_INVALID);
    tep_redirect(tep_href_link(FILENAME_AUCTION_GROUPS, '', 'NONSSL'));
  }
  $auction_info = tep_db_fetch_array($check_query);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  switch($action) {
    default:
      break;
  }

?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div class="bounder">
        <div class="floater"><h1><?php echo $auction_info['auctions_name']; ?></h1></div>
<?php
  $bids_string = '';
  if( tep_session_is_registered('customer_id') ) {
    $customer_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customer_array = tep_db_fetch_array($customer_query);
    $bids_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '"><span class="pad notice">' . sprintf(TEXT_INFO_AUCTION_BIDS_LEFT, '<span id="bids_notice">' . $customer_array['customers_bids'] . '</span>') . '</span></a>';
?>
        <div class="floatend" style="padding-top: 14px; padding-right: 8px;"><?php echo $bids_string; ?></div>
<?php
  }
?>
      </div>
<?php
  $new_price = '<span class="textPrice">' . TEXT_INFO_PRICE . '</span>&nbsp;<span class="productSpecialPrice">' . $currencies->display_price($auction_info['start_price'], 0) . '</span>';

  $auctions_name = strtoupper($auction_info['auctions_name']);
  $action_params = '&action=add_auction';
?>
        <div class="bounder">
<?php
  $display_image = '';

  if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
    $display_image = IMAGE_NOT_AVAILABLE;
  } else {
    $display_image = $auction_info['auctions_image'];
  }
?>
          <div class="floater auction_item hider" style="height: 320px; width: 250px;" id="auction_item_<?php echo $auction_info['auctions_id']; ?>">
            <div class="hpad">
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b1"></div>
              <div id="auction_block_<?php echo $auction_info['auctions_id']; ?>" style="border-left: 6px solid #CCC; border-right: 6px solid #CCC; min-height: 270px;">
                <div class="ralign rpad moduleRowSelected bspacer"><?php echo '<a href="#" attr="' . $auction_info['auctions_id'] . '" class="auction_details" title="' . sprintf(TEXT_INFO_AUCTION_DETAILS, $auction_info['auctions_name']) . '">' . tep_image(DIR_WS_ICONS . 'icon_info.png', sprintf(TEXT_INFO_AUCTION_DETAILS, $auction_info['auctions_name'])) . '</a>'; ?></div>
<?php
  if( $auction_info['status_id' != 2 ) {
    if( $auction['bid_step'] > 1 ) {
      $splash_class = 'splash48';
    } else {
      $splash_class = 'splash48_blue';
    }
?>
                <div class="bounder">
                  <div class="floater tener">
                    <div class="<?php echo $splash_class; ?> heavy" style="margin-left: 6px;" title="<?php echo sprintf(TEXT_INFO_AUCTION_BIDS_STEP_REQUIRED, $auction_info['bid_step'], $auction_info['auctions_name']); ?>"><div style="padding-top: 12px; padding-left: 18px;"><?php echo $auction_info['bid_step'] ?></div></div>
                  </div>
                  <div class="floater nineties calign" id="fancyx"><?php echo '<a href="' . DIR_WS_IMAGES . $display_image . '" class="pg_link" target="_blank">' . tep_image(DIR_WS_IMAGES . $display_image, $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
                </div>
<?php
  } else {
?>
                <div class="calign" id="fancyx"><?php echo '<a href="' . DIR_WS_IMAGES . $display_image . '" class="pg_link" target="_blank">' . tep_image(DIR_WS_IMAGES . $display_image, $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
<?php
  }
?>
                <div class="bspacer calign tiny_text"><?php echo sprintf(TEXT_INFO_AUCTION_PRICE, '<span class="heavy mark_red">' . $currencies->format($auction['start_price']) . '</span>'); ?></div>
<?php
      if( $auction_info['status_id'] == 2 ) {
        $history_array = array();
        $history_query_raw = "select auto_id, customers_id, auctions_id, auctions_name, products_name, started, completed, bid_count, winner_bids, customers_nickname, customer_count from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'";
        tep_query_to_array($history_query_raw, $history_array);
        $auction = $history_array[0];
?>
                <div class="calign vspacer"><?php echo sprintf(TEXT_INFO_AUCTION_ENDED, $auction['completed']); ?></div>
                <div class="heavy tiny_text calign">
<?php 
        echo sprintf(TEXT_INFO_BIDS_ENTERED, $auction['bid_count']);
?>
                </div>
                <div class="heavy tiny_text calign hider">
<?php
        if( tep_session_is_registered('customer_id') ) {
          $winner_flag = false;
          $output = '';
          $output .= '<table class="tabledata">';

          $j = count($history_array);
          for($i=0; $i<$j; $i++) {
            $self_bids = '';
            if( $auction['customers_id'] == $customer_id) {
              $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $auction['winner_bids']) . '</span>';
            }

            $history_array[$i]['customers_nickname'] = tep_truncate_string($history_array[$i]['customers_nickname']);

            if( !empty($auction['customers_nickname']) ) {
              $rclass = ($i%2)?'altrow':'oddrow';
              $output .= '<tr class="' . $rclass . '"><td class="ralign">' . ($i+1) . '.</td><td class="lalign mark_green">' . $history_array[$i]['customers_nickname'] . '</td><td class="lalign">' . $history_array[$i]['winner_bids'] . '</td></tr>';
              if( $history_array[$i]['customers_id'] == $customer_id) {
                $self_bids = '<span class="mark_red">' . sprintf(TEXT_INFO_AUCTION_BIDS_SELF_ENTERED, $history_array[$i]['winner_bids']) . '</span>';
              }
            }
          }

          $output .= '</table>';

          if( !empty($self_bids) ) {
            echo '<div>' . $self_bids . '</div>';
          }

          if( !empty($auction['customers_nickname']) ) {
            echo '<div>' . sprintf(TEXT_INFO_AUCTION_BIDS_WINNERS, $j) . '</div>';
            echo $output;
          } else {
            echo '<div>' . TEXT_INFO_AUCTION_NO_WINNER . '</div>';
          }

        } else {
          echo sprintf(TEXT_INFO_AUCTION_LOGIN_DETAILS);
        }
?>
                </div>
<?php
      } elseif( $auction_info['status_id'] == 1 ) {
        if( !tep_session_is_registered('customer_id') ) {
          $note = TEXT_INFO_AUCTION_LOGIN_FIRST_2;
        } else {
          $check_query = tep_db_query("select customers_id from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");

          if( !tep_db_num_rows($check_query) ) {
            $note = '<span class="heavy mark_red">' . TEXT_INFO_AUCTION_WAIT_FIRST_BID . '</span>';
          } else {
            $check_array = tep_db_fetch_array($check_query);
            $nickname = tep_auction_customer_nickname($check_array['customers_id']);
            $note = '<span class="heavy mark_red">' . sprintf(TEXT_INFO_LAST_BID, tep_truncate_string($nickname)) . '</span>';
          }
        }
?>
                <div class="calign vspacer" id="bid_note<?php echo $auction_info['auctions_id']; ?>"><?php echo $note; ?></div>
<?php
        $bids_query = tep_db_query("select bid_price, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'");
        if( !tep_db_num_rows($bids_query) ) {
          $bids_array = array(
            'bid_price' => '0.00',
            'last_modified' => date('Y-m-d H:i:s'),
          );
        } else {
          $bids_array = tep_db_fetch_array($bids_query);
        }
?>
                <div class="heavy tiny_text bspacer calign" id="auction_entry_bid_<?php echo $auction_info['auctions_id']; ?>">
<?php 
        echo TEXT_INFO_AUCTION_ENTER_BID . '&nbsp;';
        echo tep_draw_input_field('auction_bid[' . $auction_info['auctions_id'] . ']', '0.00', 'size="4" class="bid_inputs" id="auction_input_' . $auction_info['auctions_id'] . '"');
?>
                </div>
                <div class="vpad heavy tiny_text calign">
<?php
        echo tep_draw_hidden_field('hidden_bid[' . $auction_info['auctions_id'] . ']', $bids_array['last_modified'], 'id="backup_bid_' . $auction_info['auctions_id'] . '"');
        if( !tep_session_is_registered('customer_id') ) {
          echo '<a href="' . tep_href_link(FILENAME_LOGIN, '', 'SSL') . '" attr="' . $auction_info['auctions_id'] . '" class="sticky_bid" title="' . TEXT_INFO_AUCTION_LOGIN_FIRST . '">' . tep_image_button('button_auction_login.gif', TEXT_INFO_AUCTION_LOGIN_FIRST) . '</a>';
        } else {
          echo '<a href="#" attr="' . $auction_info['auctions_id'] . '" class="sticky_bid" title="' . sprintf(TEXT_INFO_AUCTION_PLACE_BID, $auction_info['auctions_name']) . '">' . tep_image_button('button_placebid.gif', sprintf(TEXT_INFO_AUCTION_PLACE_BID, $auction_info['auctions_name']) ) . '</a>'; 
        }
?>
                </div>
                <div class="vpad heavy tiny_text calign mark_red" id="bid_note2_<?php echo $auction_info['auctions_id']; ?>"></div>
<?php
      } else {
?>

<?php
      }
?>
              </div>
              <div class="b2" style="margin-left: 1px; margin-right: 1px;"></div>
              <div class="b2" style="margin-left: 2px; margin-right: 2px;"></div>
              <div class="b1" style="margin-left: 4px; margin-right: 4px;"></div>
              <div class="b1" style="margin-left: 6px; margin-right: 6px;"></div>
            </div>
          </div>
          <div class="extend" style="border-left: 2px solid #d6d6d6;"></div>
          <div class="floater" style="width: 670px; padding-left: 16px;">
            <div class="bounder">
<?php
      if( $auction_info['status_id'] < 2 ) {
        $products_name = tep_get_products_name($auction_info['products_id']);
        $row_class = 'oddrow';
?>
              <div class="heavy pad dtrans" style="font-size: 20px"><?php echo TEXT_INFO_AUCTION_PRIZES; ?></div>
              <div class="heavy <?php echo $row_class; ?> vpad lpad auction_item"><?php echo '1. ' . '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction_info['products_id']) . '">' . $products_name . '</a>'; ?></div>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction_info['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $display_image, $auction_info['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
                <div><?php echo $auction_info['auctions_description']; ?></div>
              </div>
<?php
        $tiers_array = array();
        $tiers_query = "select products_id, auctions_name, auctions_image, auctions_description from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . $auction_info['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query, $tiers_array);
        for($i=0, $j=count($tiers_array); $i<$j; $i++) {
          $row_class = ($i%2)?'oddrow':'altrow';
          $products_name = tep_get_products_name($tiers_array[$i]['products_id']);
          if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
            $tier_image = IMAGE_NOT_AVAILABLE;
          } else {
            $tier_image = $auction_info['auctions_image'];
          }

?>
              <div class="heavy <?php echo $row_class; ?> vpad lpad auction_item"><?php echo ($i+2) . '. ' . '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tiers_array[$i]['products_id']) . '">' . $products_name . '</a>'; ?></div>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tiers_array[$i]['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $tier_image, $tiers_array[$i]['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
                <div><?php echo $tiers_array[$i]['auctions_description']; ?></div>
              </div>
<?php
        }
      } elseif( $auction_info['status_id'] == 2 ) {
        $history_array = array();
        $history_query = "select customers_id, customers_nickname, products_id, products_name, auctions_name, winner_bids, bid_count, started, completed from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "'";
        tep_query_to_array($history_query, $history_array);
?>
              <div class="heavy pad dtrans" style="font-size: 20px"><?php echo TEXT_INFO_AUCTION_PRIZES_WON; ?></div>
<?php
        $tiers_array = array();
        $tiers_query_raw = "select auctions_image, auctions_description from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_info['auctions_id'] . "' order by sort_id";
        tep_query_to_array($tiers_query_raw, $tiers_array);

        for($i=0, $j=count($history_array); $i<$j; $i++) {
          $description = $auction_info['auctions_description'];

          $row_class = ($i%2)?'oddrow':'altrow';
          if( $i ) {
            if( !tep_not_null($tiers_array[$i]['auctions_image']) || !file_exists(DIR_WS_IMAGES . $tiers_array[$i]['auctions_image']) || filesize(DIR_WS_IMAGES . $tiers_array[$i]['auctions_image']) <= 0 ) {
              $auction_image = IMAGE_NOT_AVAILABLE;
            } else {
              $auction_image = $tiers_array[$i]['auctions_image'];
            }
            $description = $tiers_array[$i]['auctions_description'];
          } else {
            if( !tep_not_null($auction_info['auctions_image']) || !file_exists(DIR_WS_IMAGES . $auction_info['auctions_image']) || filesize(DIR_WS_IMAGES . $auction_info['auctions_image']) <= 0 ) {
              $auction_image = IMAGE_NOT_AVAILABLE;
            } else {
              $auction_image = $auction_info['auctions_image'];
            }
          }
?>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="lpad heighter">
                  <div class="heavy floater halfer auction_item"><?php echo ($i+1) . '. ' . $history_array[$i]['customers_nickname']; ?></div>
                  <div class="heavy floater halfer auction_item"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $history_array[$i]['products_id']) . '">' . $history_array[$i]['products_name'] . '</a>'; ?></div>
                </div>
              </div>
              <div class="bounder vpad <?php echo $row_class; ?>">
                <div class="floater quarter calign"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $history_array[$i]['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $auction_image, $history_array[$i]['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></div>
                <div><?php echo $description; ?></div>
              </div>
<?php
        }
      }
?>
            </div>
          </div>
        </div>
<?php require('includes/objects/html_end.php'); ?>
