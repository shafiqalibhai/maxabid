<?php
/*
  $Id: quickfind.php,v 1.10 2005/08/04 23:25:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright � 2003 osCommerce

  Released under the GNU General Public License

*/
  require('includes/application_top.php');

  $keywords = (isset($_GET['keywords'])?$_GET['keywords']:'');
  $q = addslashes(preg_replace("%[^0-9a-zA-Z ]%", "", $keywords) );

  $limit = 10;
  if(isset($q) && tep_not_null($q) ) {
    $results = array();
    //$query = tep_db_query("select pd.products_id, pd.products_name, p.products_model from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_PRODUCTS . " p on (p.products_id = pd.products_id) where (pd.products_name like '%" . tep_db_input($q) . "%' or p.products_model like '%" . tep_db_input($q) . "%') and p.products_display = '1' and pd.language_id = '" . (int)$languages_id . "' order by pd.products_name limit " . $limit);
    $query = tep_db_query("select pd.products_id, pd.products_name from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_PRODUCTS . " p on (p.products_id = pd.products_id) where pd.products_name like '%" . tep_db_input($q) . "%' and p.products_display = '1' and pd.language_id = '" . (int)$languages_id . "' order by pd.products_name limit " . $limit);
   
    if ( tep_db_num_rows($query) ) {
      while ( $row = tep_db_fetch_array($query) ) {
	  
//	    if ( isset($row['products_model']) && tep_not_null($row['products_model']) ) {
//		  $model = ' [' . $row['products_model'] . ']';
//		} else {
//		  $model = '';
//		}
	  

        $row['products_name'] = str_replace(TEXT_PRODUCTS_POSTFIX, '', ucwords($row['products_name']));
        $name = $row['products_name'];
        $id = $row['products_id'];
        $url = tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $id);
        $results[] = '<a href="' .  $url . '">' .  $name . '</a>' . "\n";
	    // $results[] = '<a href="' . $url . '">' . $name . $model . '</a>' .  "\n";
      }
    } else {
      //$results[] = '<span class="smallText">There are no results</span>';
      $results[] = '<a href="' .  tep_href_link(FILENAME_ADVANCED_SEARCH, '', 'NONSSL') . '">No results found - See other options</a>';
    }
    echo implode('<br />' . "\n", $results);
  } else {
    //echo "Quick Find Results ...";
  }

  require('includes/application_bottom.php');
?>
