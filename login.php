<?php
/*
  $Id: create_account.php,v 1.65 2003/06/09 23:03:54 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Create Account page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $navigation->remove_current_page();

// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
  if( count($_POST) && $session_started == false) {
    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE, '', 'NONSSL'));
  }

  if (tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  }

  $error_array = array();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $country = isset($_POST['country'])?$_POST['country']:tep_default_country_zone();
  $state = isset($_POST['state'])?$_POST['state']:tep_default_country_zone(1,0,$country);

// Second Address
  $country2 = isset($_POST['country2'])?$_POST['country2']:tep_default_country_zone();
  $state2 = isset($_POST['state2'])?$_POST['state2']:tep_default_country_zone(1,2,$country2);

//  $country = isset($_POST['country'])?$_POST['country']:STORE_COUNTRY;
//  $state = isset($_POST['state'])?$_POST['state']:STORE_ZONE;

/*
  if( isset($_POST['old_country']) && $_POST['old_country'] != $country) {
    $action='change_country';
  } elseif( isset($_POST['old_country2']) && $_POST['old_country2'] != $country2) {
    $action='change_country';
  } elseif( isset($_POST['change_country_x']) || isset($_POST['change_country_y']) || isset($_POST['change_country2_x']) || isset($_POST['change_country2_y']) ) {
    //if( isset($_POST['old_country']) && $_POST['old_country'] != $country) {
    $action='change_country';
    //}
  }
*/

  if( isset($_POST['change_country_x']) || isset($_POST['change_country_y']) || isset($_POST['change_country2_x']) || isset($_POST['change_country2_y']) ) {
    $action='change_country';
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . $g_script);

  $process = false;
  $login = false;
  switch($action) {
    case 'login':
      $error = false;
      $email_address = tep_db_prepare_input($_POST['email_address']);
      $password = tep_db_prepare_input($_POST['password']);

// Check if email exists
      $check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_password, customers_email_address, customers_default_address_id, sticky_poll from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
      if (!tep_db_num_rows($check_customer_query)) {
        $error = true;
      } else {
        $check_customer = tep_db_fetch_array($check_customer_query);
// Check that password is good
        if (!tep_validate_password($password, $check_customer['customers_password'])) {
          $error = true;
        } else {
          tep_session_recreate();

          $check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$check_customer['customers_id'] . "' and address_book_id = '" . (int)$check_customer['customers_default_address_id'] . "'");
          $check_country = tep_db_fetch_array($check_country_query);

          $customer_id = $check_customer['customers_id'];
          $customer_default_address_id = $check_customer['customers_default_address_id'];
          $customer_first_name = $check_customer['customers_firstname'];
          $customer_country_id = $check_country['entry_country_id'];
          $customer_zone_id = $check_country['entry_zone_id'];
          $sticky_poll = $check_customer['sticky_poll'];

          tep_session_register('customer_id');
          tep_session_register('customer_default_address_id');
          tep_session_register('customer_first_name');
          tep_session_register('customer_country_id');
          tep_session_register('customer_zone_id');

          tep_session_register('sticky_poll');


          tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");

// restore cart contents
          $cart->restore_contents();

          if (sizeof($navigation->snapshot) > 0) {
            $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode'], true, false);
            $navigation->clear_snapshot();
            tep_redirect($origin_href);
          } else {
            tep_redirect(tep_href_link(FILENAME_ACCOUNT,'','SSL'));
          }
        }
      }
      if ($error == true) {
        $messageStack->add(tep_get_script_name(), TEXT_LOGIN_ERROR);
      }
      break;
    case 'process':
      $process = true;
      //$same_address  = isset($_POST['same_address'])?1:0;
$same_address = 1;
      if (ACCOUNT_GENDER == 'true') {
        if (isset($_POST['gender'])) {
          $gender = tep_db_prepare_input($_POST['gender']);
        } else {
          $gender = false;
        }

        if( !$same_address && isset($_POST['gender2']) ) {
          $gender2 = tep_db_prepare_input($_POST['gender2']);
        } else {
          $gender2 = false;
        }
      }

      $nickname = tep_db_prepare_input($_POST['nickname']);

      $firstname = tep_db_prepare_input($_POST['firstname']);
      if( !$same_address ) {
        $firstname2 = tep_db_prepare_input($_POST['firstname2']);
      }

      $lastname = tep_db_prepare_input($_POST['lastname']);
      if( !$same_address ) {
        $lastname2 = tep_db_prepare_input($_POST['lastname2']);
      }

      if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($_POST['dob']);
      $email_address = tep_db_prepare_input($_POST['email_address']);

      $company = isset($_POST['company'])?tep_db_prepare_input($_POST['company']):'';
      $street_address = tep_db_prepare_input($_POST['street_address']);
      if( !$same_address ) {
        $street_address2 = tep_db_prepare_input($_POST['street_address2']);
      }


      if (ACCOUNT_SUBURB == 'true') {
        $suburb = tep_db_prepare_input($_POST['suburb']);
        if( !$same_address ) {
          $suburb2 = tep_db_prepare_input($_POST['suburb2']);
        }
      }

      $postcode = tep_db_prepare_input($_POST['postcode']);
      if( !$same_address ) {
        $postcode2 = tep_db_prepare_input($_POST['postcode2']);
      }

      $city = tep_db_prepare_input($_POST['city']);
      if( !$same_address ) {
        $city2 = tep_db_prepare_input($_POST['city2']);
      }

/*
      if (ACCOUNT_STATE == 'true') {
        $state = tep_db_prepare_input($_POST['state']);
        if (isset($_POST['zone_id'])) {
          $zone_id = tep_db_prepare_input($_POST['zone_id']);
        } else {
          $zone_id = false;
        }
      }
*/
      //$country = tep_db_prepare_input($_POST['country']);
      //if( !$same_address ) {
      //  $country2 = tep_db_prepare_input($HTTP_POST_VARS['country2']);
      //}

      $telephone = tep_db_prepare_input($_POST['telephone']);
      $fax = isset($_POST['fax'])?tep_db_prepare_input($_POST['fax']):'';
      if (isset($_POST['newsletter'])) {
        $newsletter = tep_db_prepare_input($_POST['newsletter']);
      } else {
        $newsletter = false;
      }
      $password = tep_db_prepare_input($_POST['password']);
      $confirmation = tep_db_prepare_input($_POST['confirmation']);

      $error = false;

      if (ACCOUNT_GENDER == 'true') {
        if ( ($gender != 'm') && ($gender != 'f') ) {
          $error = true;
          $messageStack->add(tep_get_script_name(), ENTRY_GENDER_ERROR);
        }
        if( !$same_address && $gender2 != 'm' && $gender2 != 'f' ) {
          $error = true;
          $messageStack->add(tep_get_script_name(), ENTRY_GENDER_ERROR);
        }
      }

      $nickname = tep_create_safe_string($nickname);
      $length = strlen($nickname);
      if( $length < 8 || $length > 32 || $nickname != tep_db_prepare_input($_POST['nickname']) ) {
        $error = true;

        $messageStack->add(tep_get_script_name(), ERROR_NICKNAME_LENGTH);
        $error_array['nickname'] = ERROR_NICKNAME_LENGTH;
      }
      $check_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_id != '" . (int)$customer_id . "' and customers_nickname = '" . tep_db_input($nickname) . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( $check_array['total'] ) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ERROR_NICKNAME_EXISTS);
        $error_array['nickname'] = ERROR_NICKNAME_EXISTS;
      }

      if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_FIRST_NAME_ERROR);
        $error_array['firstname'] = ENTRY_FIRST_NAME_ERROR;
      }
      if( !$same_address && strlen($firstname2) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_FIRST_NAME_ERROR);
        $error_array['firstname2'] = ENTRY_FIRST_NAME_ERROR;
      }

      if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_LAST_NAME_ERROR);
        $error_array['lastname'] = ENTRY_LAST_NAME_ERROR;
      }
      if( !$same_address && strlen($lastname2) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_LAST_NAME_ERROR);
        $error_array['lastname2'] = ENTRY_LAST_NAME_ERROR;
      }

      if (ACCOUNT_DOB == 'true') {
        if (checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4)) == false) {
          $error = true;
          $messageStack->add(tep_get_script_name(), ENTRY_DATE_OF_BIRTH_ERROR);
        }
      }

      if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_EMAIL_ADDRESS_ERROR);
        $error_array['email_address'] = ENTRY_EMAIL_ADDRESS_ERROR;
      } elseif (tep_validate_email($email_address) == false) {
        $error = true;

        $messageStack->add(tep_get_script_name(), ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
      } else {
        $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
        $check_email = tep_db_fetch_array($check_email_query);
        if ($check_email['total'] > 0) {
          $error = true;
          $messageStack->add(tep_get_script_name(), ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
          $error_array['email_address'] = ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
        }
      }

      if( strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_STREET_ADDRESS_ERROR);
        $error_array['street_address'] = ENTRY_STREET_ADDRESS_ERROR;
      }
      if( !$same_address && strlen($street_address2) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_STREET_ADDRESS_ERROR);
        $error_array['street_address2'] = ENTRY_STREET_ADDRESS_ERROR;
      }

      if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_POST_CODE_ERROR);
        $error_array['postcode'] = ENTRY_POST_CODE_ERROR;
      }
      if( !$same_address && strlen($postcode2) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_POST_CODE_ERROR);
        $error_array['postcode2'] = ENTRY_POST_CODE_ERROR;
      }

      if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_CITY_ERROR);
        $error_array['city'] = ENTRY_CITY_ERROR;
      }
      if( !$same_address && strlen($city2) < ENTRY_CITY_MIN_LENGTH ) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_CITY_ERROR);
        $error_array['city2'] = ENTRY_CITY_ERROR;
      }

      if (is_numeric($country) == false) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_COUNTRY_ERROR);
        $error_array['country'] = ENTRY_COUNTRY_ERROR;
      }
      if( !$same_address && is_numeric($country2) == false) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_COUNTRY_ERROR);
        $error_array['country2'] = ENTRY_COUNTRY_ERROR;
      }

//-MS- Active Countries Added
      if (ACCOUNT_STATE == 'true') {
        $check_query = tep_db_query("select z.zone_id from " . TABLE_ZONES . " z left join " . TABLE_COUNTRIES . " c on (c.countries_id=z.zone_country_id) where z.status_id='1' and c.status_id='1' and z.pay_status_id = '1' and c.pay_status_id = '1' and z.zone_id = '" . (int)$state . "' and c.countries_id = '" . (int)$country . "'");

        if(!tep_db_num_rows($check_query) ) {
          $messageStack->add(tep_get_script_name(), ENTRY_STATE_ERROR_SELECT);
          $error_array['state'] = ENTRY_STATE_ERROR_SELECT;
          $state = STORE_ZONE;
          $country = STORE_COUNTRY;
          $error = true;
        }

        if( !$same_address ) {
          $check_query = tep_db_query("select z.zone_id from " . TABLE_ZONES . " z left join " . TABLE_COUNTRIES . " c on (c.countries_id=z.zone_country_id) where z.status_id='1' and c.status_id='1' and z.ship_status_id = '1' and c.ship_status_id = '1' and z.zone_id = '" . (int)$state2 . "' and c.countries_id = '" . (int)$country2 . "'");
          if(!tep_db_num_rows($check_query) ) {
            $messageStack->add(tep_get_script_name(), ENTRY_STATE_ERROR_SELECT);
            $error_array['state2'] = ENTRY_STATE_ERROR_SELECT;
            $state2 = STORE_ZONE;
            $country2 = STORE_COUNTRY;
            $error = true;
          }
        }
      }

      $check_query = tep_db_query("select countries_id from " . TABLE_COUNTRIES . " where status_id = '1' and pay_status_id = '1' and countries_id = '" . (int)$country . "'");
      if(!tep_db_num_rows($check_query) ) {
        $messageStack->add(tep_get_script_name(), ENTRY_COUNTRY_ERROR);
        $error_array['country'] = ENTRY_COUNTRY_ERROR;
        unset($sql_entry_array['entry_country_id']);
        $error = true;
        $country = STORE_COUNTRY;
      }
      if( !$same_address ) {
        $check_query = tep_db_query("select countries_id from " . TABLE_COUNTRIES . " where status_id = '1' and ship_status_id = '1' and countries_id = '" . (int)$country2 . "'");
        if(!tep_db_num_rows($check_query) ) {
          $messageStack->add(tep_get_script_name($PHP_SELF), ENTRY_COUNTRY_ERROR);
          $error_array['country2'] = ENTRY_COUNTRY_ERROR;
          unset($sql_entry_array['entry_country_id']);
          $error = true;
          $country2 = STORE_COUNTRY;
        }
      }


//-MS- Active Countries Added EOM

/*
      if (ACCOUNT_STATE == 'true') {
        $zone_id = 0;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
        $check = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check['total'] > 0);
        if ($entry_state_has_zones == true) {
          $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name like '" . tep_db_input($state) . "%' or zone_code like '%" . tep_db_input($state) . "%')");
          if (tep_db_num_rows($zone_query) == 1) {
            $zone = tep_db_fetch_array($zone_query);
            $zone_id = $zone['zone_id'];
          } else {
            $error = true;

            $messageStack->add(tep_get_script_name(), ENTRY_STATE_ERROR_SELECT);
          }
        } else {
          if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
            $error = true;

            $messageStack->add(tep_get_script_name(), ENTRY_STATE_ERROR);
          }
        }
      }
*/



      if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_TELEPHONE_NUMBER_ERROR);
        $error_array['telephone'] = ENTRY_TELEPHONE_NUMBER_ERROR;
      }


      if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_PASSWORD_ERROR);
        $error_array['password'] = ENTRY_PASSWORD_ERROR;
      } elseif ($password != $confirmation) {
        $error = true;
        $messageStack->add(tep_get_script_name(), ENTRY_PASSWORD_ERROR_NOT_MATCHING);
        $error_array['confirmation'] = ENTRY_PASSWORD_ERROR_NOT_MATCHING;
      }

      if ($error == false) {
        $sql_data_array = array(
          'customers_nickname' => $nickname,
          'customers_firstname' => $firstname,
          'customers_lastname' => $lastname,
          'customers_email_address' => $email_address,
          'customers_telephone' => $telephone,
          'customers_fax' => $fax,
          'customers_newsletter' => $newsletter,
          'customers_password' => tep_encrypt_password($password)
        );

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);

        $customer_id = tep_db_insert_id();

        $sql_data_array = array('customers_id' => $customer_id,
                                'entry_firstname' => $firstname,
                                'entry_lastname' => $lastname,
                                'entry_street_address' => $street_address,
                                'entry_postcode' => $postcode,
                                'entry_city' => $city,
                                'entry_country_id' => $country);

        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;

        if (ACCOUNT_STATE == 'true') {
          $sql_data_array['entry_zone_id'] = (int)$state;
          $sql_data_array['entry_state'] = '';
        }

/*
        if (ACCOUNT_STATE == 'true') {
          if ($zone_id > 0) {
            $sql_data_array['entry_zone_id'] = $zone_id;
            $sql_data_array['entry_state'] = '';
          } else {
            $sql_data_array['entry_zone_id'] = '0';
            $sql_data_array['entry_state'] = $state;
          }
        }
*/
        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
        $address_id = tep_db_insert_id();

        if( !$same_address ) {
          $sql_data_array = array('customers_id' => $customer_id,
                                  'entry_firstname' => $firstname2,
                                  'entry_lastname' => $lastname2,
                                  'entry_street_address' => $street_address2,
                                  'entry_postcode' => $postcode2,
                                  'entry_city' => $city2,
                                  'entry_country_id' => $country2);

          if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender2;
          if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company2;
          if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb2;

          if (ACCOUNT_STATE == 'true') {
            $sql_data_array['entry_zone_id'] = (int)$state2;
            $sql_data_array['entry_state'] = '';
          }
          tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
          $address_id2 = tep_db_insert_id();
        }

        tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$address_id . "' where customers_id = '" . (int)$customer_id . "'");

        $sql_data_array = array('customers_info_id' => (int)$customer_id,
                                'customers_info_date_of_last_logon' => 'now()',
                                'customers_info_number_of_logons' => '1',
                                'customers_info_date_account_created' => 'now()',
                                'customers_info_date_account_last_modified' => 'now()',
                               );
        tep_db_perform(TABLE_CUSTOMERS_INFO, $sql_data_array);

        tep_session_recreate();

        $customer_first_name = $firstname;
        $customer_default_address_id = $address_id;
        $customer_country_id = $country;
        $customer_zone_id = $state;
        tep_session_register('customer_id');
        tep_session_register('customer_first_name');
        tep_session_register('customer_default_address_id');
        tep_session_register('customer_country_id');
        tep_session_register('customer_zone_id');

        $sticky_poll = 1;
        tep_session_register('sticky_poll');

        if (!tep_session_is_registered('billto')) tep_session_register('billto');
        if (!tep_session_is_registered('sendto')) tep_session_register('sendto');

        $sendto = $billto = $address_id;
        if( !$same_address ) {
          $sendto = $address_id2;
        }

//// restore cart contents
        $cart->restore_contents();

//// build the message content
        $name = $firstname . ' ' . $lastname;

        if (ACCOUNT_GENDER == 'true') {
           if ($gender == 'm') {
             $email_text = sprintf(EMAIL_GREET_MR, $lastname);
           } else {
             $email_text = sprintf(EMAIL_GREET_MS, $lastname);
           }
        } else {
          $email_text = sprintf(EMAIL_GREET_NONE, $firstname);
        }

//-MS- Email Templates added
        $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='create_account'");
        if( $email_array = tep_db_fetch_array($mail_query) ) {
          $right_col = tep_email_column(DEFAULT_EMAIL_ABSTRACT_ZONE_ID);
          $temp_array = array(
                              'CUSTOMER_NAME' => $name,
                              'CUSTOMER_EMAIL' => $email_address,
                              'CUSTOMER_PASSWORD' => $_POST['password'],
                              'STORE_NAME' => STORE_NAME,
                              'STORE_EMAIL_ADDRESS' => STORE_OWNER_EMAIL_ADDRESS,
                              'RIGHT_COL' => $right_col
                             );
          $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
          tep_mail($customer_name, $email_address, $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
        } else {
//-MS- Email Templates added EOM
          $email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
          tep_mail($name, $email_address, EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
        }

        tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT_SUCCESS, '', 'SSL'));
      }
      break;
    case 'change_country':
      // Standard
      $street_address = tep_db_prepare_input($_POST['street_address']);
      $postcode = tep_db_prepare_input($_POST['postcode']);
      $city = tep_db_prepare_input($_POST['city']);
      $firstname = tep_db_prepare_input($_POST['firstname']);
      $lastname = tep_db_prepare_input($_POST['lastname']);
      $email_address = tep_db_prepare_input($_POST['email_address']);
      // Optional
      $gender = isset($_POST['gender'])?tep_db_prepare_input($_POST['gender']):'';
      $suburb = isset($_POST['suburb'])?tep_db_prepare_input($_POST['suburb']):'';
      $company = isset($_POST['company'])?tep_db_prepare_input($_POST['company']):'';
      $dob = isset($_POST['dob'])?tep_db_prepare_input($_POST['dob']):'';
      $newsletter = isset($_POST['newsletter'])?tep_db_prepare_input($_POST['newsletter']):'';

      // Second
      $street_address2 = isset($_POST['street_address2'])?tep_db_prepare_input($_POST['street_address2']):'';
      $postcode2 = isset($_POST['postcode2'])?tep_db_prepare_input($_POST['postcode2']):'';
      $city2 = isset($_POST['city2'])?tep_db_prepare_input($_POST['city2']):'';
      $firstname2 = isset($_POST['firstname2'])?tep_db_prepare_input($_POST['firstname2']):'';
      $lastname2 = isset($_POST['lastname2'])?tep_db_prepare_input($_POST['lastname2']):'';
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div style="width:60%; float:left;"><h1><?php echo strtoupper(HEADING_TITLE1); ?></h1></div>
      <div style="width:60%; float:left;"><?php echo tep_draw_form('login', tep_href_link($g_script, 'action=login', 'SSL')); ?><fieldset><legend><?php echo TEXT_INFO_LOGIN_INTRO; ?></legend>
        <div class="vpad"><label for="log_email_address" class="quarter floater"><?php echo ENTRY_EMAIL_ADDRESS; ?></label><?php echo tep_draw_input_field('email_address', '', 'id="log_email_address"'); ?></div>
        <div class="vpad"><label for="log_password" class="quarter floater"><?php echo ENTRY_PASSWORD; ?></label><?php echo tep_draw_password_field('password', '', 'id="log_password"'); ?></div>
		</br></br></br><?php echo '<a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . TEXT_PASSWORD_FORGOTTEN . '</a>'; ?>
		<?php echo ' || <a href="/create_account.php">' . HEADER_TITLE_CREATE_ACCOUNT . '</a>'; ?>
		
 
        <div class="wider ralign">
		<?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_LOGIN . '</a>'; ?>
		</div>
      <div class="cleaner"></div></div>
<?php require('includes/objects/html_end.php'); ?>
